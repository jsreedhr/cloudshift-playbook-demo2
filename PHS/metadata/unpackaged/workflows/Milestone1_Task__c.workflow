<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Requirement_Name_Concatenate</fullName>
        <field>Name</field>
        <formula>Text_Area__c + &quot;:&quot; + &quot; &quot; +  Requirement_Title__c</formula>
        <name>Requirement Name Concatenate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Requirement Name</fullName>
        <actions>
            <name>Requirement_Name_Concatenate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
