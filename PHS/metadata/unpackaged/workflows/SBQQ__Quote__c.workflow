<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CPQ_Approved_Quote_Email_Alert</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>CPQ - Approved Quote Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approved_Quote</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Field_Sales_Big_Deal_Alert</fullName>
        <description>CPQ - Field Sales Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Tele_Appointer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>75921@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Greenleaf_Big_Deal_Alert</fullName>
        <description>CPQ - Greenleaf Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>75675@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Inbound_Big_Deal_Alert</fullName>
        <description>CPQ - Inbound Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>52381@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Outbound_Big_Deal_Alert</fullName>
        <description>CPQ - Outbound Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Tele_Appointer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>51934@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Rejected_Quote_Email_Alert</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>CPQ - Rejected Quote Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Rejected_Quote</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Wastekit_1_Big_Deal_Alert</fullName>
        <description>CPQ - Wastekit 1 Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Tele_Appointer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>200270@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>40661@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>CPQ_Wastekit_2_Big_Deal_Alert</fullName>
        <description>CPQ - Wastekit 2 Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Tele_Appointer_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>200270@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>40661@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Big_Deal_Alerts</template>
    </alerts>
    <alerts>
        <fullName>Email_approval_request_confirmation_email_Greenleaf_Mgmt</fullName>
        <description>Email approval request confirmation email (Greenleaf - Mgmt)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approval_Confirmation_Template_GL_Mgmt</template>
    </alerts>
    <alerts>
        <fullName>Email_approval_request_confirmation_email_Greenleaf_RSM</fullName>
        <description>Email approval request confirmation email (Greenleaf - RSM)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approval_Confirmation_Template_GL_RSM</template>
    </alerts>
    <alerts>
        <fullName>Email_approval_request_confirmation_email_Hygiene_HoS</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>Email approval request confirmation email (Hygiene - HoS)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approval_Confirmation_Template_Hygiene_HoS</template>
    </alerts>
    <alerts>
        <fullName>Email_approval_request_confirmation_email_Hygiene_RSM</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>Email approval request confirmation email (Hygiene - RSM)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approval_Confirmation_Template_Hygiene_RSM</template>
    </alerts>
    <alerts>
        <fullName>Email_approval_request_confirmation_email_Wastekit</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>Email approval request confirmation email (Wastekit)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/CPQ_Quote_Approval_Confirmation_Template_Wastekit</template>
    </alerts>
    <fieldUpdates>
        <fullName>CPQ_Does_Not_Require_Mgmt_Approval</fullName>
        <field>Management_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>CPQ - Does Not Require Mgmt Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Does_Not_Require_RSM_Approval</fullName>
        <field>RSM_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>CPQ - Does Not Require RSM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Management_Approval_Required</fullName>
        <field>Management_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>CPQ - Management Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Quote_Quote_Accepted</fullName>
        <description>The Accepted field will be updated  to &apos;TRUE&apos; in order to prevent people editing accepted quotes.</description>
        <field>Accepted__c</field>
        <literalValue>1</literalValue>
        <name>CPQ Quote - Quote Accepted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Quote_Set_Date_Sent</fullName>
        <field>PHSCPQ_Date_Sent__c</field>
        <formula>TODAY()</formula>
        <name>CPQ Quote - Set Date Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Quote_Set_Default_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Default</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CPQ Quote - Set Default Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Requires_RSM_Approval</fullName>
        <field>RSM_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>CPQ - Requires RSM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Set_Quote_Template_Hygiene</fullName>
        <field>SBQQ__QuoteTemplateId__c</field>
        <formula>&apos;a25250000003Wn3&apos;</formula>
        <name>CPQ - Set Quote Template (Hygiene)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Set_Quote_Template_Wastekit</fullName>
        <field>SBQQ__QuoteTemplateId__c</field>
        <formula>&apos;a25250000003Wn8&apos;</formula>
        <name>CPQ - Set Quote Template (Wastekit)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Carriage_Type</fullName>
        <field>Carriage_Charge__c</field>
        <name>Reset Carriage Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_DoC_Type</fullName>
        <field>DOC_Charge__c</field>
        <name>Reset DoC Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_HW_Type</fullName>
        <field>Hazardous__c</field>
        <name>Reset HW Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Install_Charge_Type</fullName>
        <field>Installation_Charge__c</field>
        <name>Reset Install Charge Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Install_Type</fullName>
        <field>Installation_Charge__c</field>
        <name>Reset Install Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Quoted_Carriage</fullName>
        <field>Quoted_Carriage_Total__c</field>
        <name>Reset Quoted Carriage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Quoted_DoC</fullName>
        <field>Quoted_DoC_Total__c</field>
        <name>Reset Quoted DoC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Quoted_HW</fullName>
        <field>Quoted_HW_Total__c</field>
        <name>Reset Quoted HW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Quoted_Install</fullName>
        <field>Quoted_Installation_Total__c</field>
        <name>Reset Quoted Install</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Process_ID_Greenleaf</fullName>
        <field>SBQQ__QuoteProcessId__c</field>
        <formula>&apos;a2425000000u9wH&apos;</formula>
        <name>Set Quote Process ID - Greenleaf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Process_ID_Hygiene</fullName>
        <field>SBQQ__QuoteProcessId__c</field>
        <formula>&apos;a2425000000stO8&apos;</formula>
        <name>Set Quote Process ID - Hygiene</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Process_ID_Wastekit</fullName>
        <field>SBQQ__QuoteProcessId__c</field>
        <formula>&apos;a2425000000u9wM&apos;</formula>
        <name>Set Quote Process ID - Wastekit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Template_ID_Greenleaf</fullName>
        <field>SBQQ__QuoteTemplateId__c</field>
        <formula>&apos;a25250000003WnD&apos;</formula>
        <name>Set Quote Template ID - Greenleaf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Template_ID_Greenleaf_DS</fullName>
        <field>SBQQ__QuoteTemplateId__c</field>
        <formula>&apos;a25250000003d9y&apos;</formula>
        <name>Set Quote Template ID - Greenleaf DS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Type_Greenleaf</fullName>
        <field>Quote_Type__c</field>
        <literalValue>Greenleaf</literalValue>
        <name>Set Quote Type - Greenleaf</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Type_Greenleaf_Design_Space</fullName>
        <field>Quote_Type__c</field>
        <literalValue>Greenlead Design Space</literalValue>
        <name>Set Quote Type - Greenleaf Design Space</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Type_Hygiene</fullName>
        <field>Quote_Type__c</field>
        <literalValue>Hygiene</literalValue>
        <name>Set Quote Type - Hygiene</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quote_Type_Wastekit</fullName>
        <field>Quote_Type__c</field>
        <literalValue>Wastekit</literalValue>
        <name>Set Quote Type - Wastekit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_HoS_Approved</fullName>
        <field>Management_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick HoS Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_HoS_Approval</fullName>
        <field>In_Management_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In HoS Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_RSM_Approval</fullName>
        <field>In_RSM_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In RSM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_RSM_Approved</fullName>
        <field>RSM_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick RSM Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_HoS_Approved</fullName>
        <field>Management_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick HoS Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_HoS_Approval</fullName>
        <field>In_Management_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In HoS Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_RSM_Approval</fullName>
        <field>In_RSM_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In RSM Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_RSM_Approved</fullName>
        <field>RSM_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick RSM Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Carriage_Quoted_Value</fullName>
        <field>Quoted_Carriage_Total__c</field>
        <formula>0</formula>
        <name>Update Carriage Quoted Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DoC_Quoted_Value</fullName>
        <field>Quoted_DoC_Total__c</field>
        <formula>0</formula>
        <name>Update DoC Quoted Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_HW_Quoted_Value</fullName>
        <field>Quoted_HW_Total__c</field>
        <formula>0</formula>
        <name>Update HW Quoted Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quoted_Value</fullName>
        <field>Quoted_Installation_Total__c</field>
        <formula>0</formula>
        <name>Update Quoted Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Awaiting_Approval</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Update Status to Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Draft</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Status to In Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Management_Approved</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Management Approved</literalValue>
        <name>Update Status to Management Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_RSM_Approved</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>RSM Approved</literalValue>
        <name>Update Status to RSM Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CPQ - Does Not Require HoS Approval %28Hygiene%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_Mgmt_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Tele_Appointing&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Field_Sales&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_House&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Key_Accounts&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;,  

Number_of_Below_Minimum_10_Products__c = 0,

Number_of_1_Below_STD_Con_Term_Products__c = 0,

ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;),

OR(
AND(ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &gt;=250),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &gt;=500),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &gt;=2500),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &gt;=1000),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Yearly in Advance&quot;))

)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Does Not Require Management Approval %28Greenleaf%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_Mgmt_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR(
ISPICKVAL(Quote_Type__c, &quot;Greenleaf&quot;),
ISPICKVAL(Quote_Type__c, &quot;Greenleaf Design Space&quot;)),

Number_of_Below_Minimum_Products__c =0,

ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;), 

ISPICKVAL(Method_of_Payment__c , &quot;Yearly in Advance&quot;)

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Does Not Require Management Approval %28Wastekit%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_Mgmt_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(

AND(

OR( 
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;), 

SBQQ__LineItemCount__c &gt;=1,
Number_of_Below_Target_Products__c = 0,
Number_of_Below_Minimum_Products__c = 0,
Number_of_POA_Products__c = 0

),

AND(

Owner:User.UserRole.DeveloperName = &quot;Wastekit&quot;),

AND(

Owner:User.UserRole.DeveloperName = &quot;WK_Management&quot;), 

AND(

Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;, 

SBQQ__LineItemCount__c &gt;=1,
Number_of_POA_Products__c = 0,
Number_of_Below_Target_Products__c = 0,
Number_of_Below_Minimum_Products__c = 0)

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Does Not Require RSM Approval %28Greenleaf%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR( 
ISPICKVAL(Quote_Type__c, &quot;Greenleaf&quot;), 
ISPICKVAL(Quote_Type__c, &quot;Greenleaf Design Space&quot;)),

Number_of_Below_STD_Con_Term_Products__c &lt;1,

Number_of_Bespoke_Products__c =0,

OR(Number_of_Below_Minimum_Products__c =0,Number_of_Below_Target_Products__c =0),
OR(Number_of_Below_Minimum_Products__c &gt;=1 , Number_of_Below_Target_Products__c &gt;=1)

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Does Not Require RSM Approval %28Hygiene%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Tele_Appointing&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Field_Sales&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_House&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Key_Accounts&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;, 

Number_of_Below_STD_Con_Term_Products__c &lt;1,

Number_of_Below_Minimum_Products__c &lt;1,

OR(ISPICKVAL(Hazardous__c, &quot;Full&quot;), ISPICKVAL(Hazardous__c, &quot;&quot;)),

OR(ISPICKVAL(DOC_Charge__c , &quot;Full&quot;), ISPICKVAL(DOC_Charge__c , &quot;&quot;)),

OR(ISPICKVAL(Carriage_Charge__c, &quot;Full&quot;), ISPICKVAL(Carriage_Charge__c, &quot;&quot;)),

OR(ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;), ISPICKVAL(Method_of_Payment__c , &quot;&quot;)),

OR(ISBLANK(Number_of_Trial_Products__c), Number_of_Trial_Products__c = 0), 

NOT(ISPICKVAL(SBQQ__Type__c, &quot;Trial&quot;)),

OR(

AND(ISPICKVAL(Method_of_Payment__c , &quot;Monthly Direct Debit&quot;),
Total_Contract_Value__c&gt;=15000),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &gt;=500),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &gt;=1000),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &gt;=5000),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &gt;=2000),

AND(ISPICKVAL(Method_of_Payment__c , &quot;Yearly in Advance&quot;))

)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Does Not Require RSM Approval %28Wastekit%29</fullName>
        <actions>
            <name>CPQ_Does_Not_Require_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(


OR(
Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;,
Owner:User.UserRole.DeveloperName = &quot;Wastekit&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_Management&quot;), TRUE,


IF(AND(

OR( 
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;), 

Number_of_Below_Target_Products__c =0), TRUE, 

FALSE))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Quote Accepted</fullName>
        <actions>
            <name>CPQ_Quote_Quote_Accepted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will check a hidden checkbox field called &apos;Accepted&apos; when the Quote status is set to &apos;Accepted&apos;.</description>
        <formula>AND(
ISCHANGED(SBQQ__Status__c),
ISPICKVAL(SBQQ__Status__c, &apos;Accepted&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires HoS Approval %28Hygiene%29</fullName>
        <actions>
            <name>CPQ_Management_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Tele_Appointing&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Field_Sales&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_House&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Key_Accounts&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;,  

OR(

Number_of_1_Below_STD_Con_Term_Products__c &gt;=1,

Number_of_Below_Minimum_10_Products__c &gt;=1,

NOT(ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;)),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Monthly Direct Debit&quot;),
Total_Contract_Value__c&lt;10000),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &lt;250),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &lt;500),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &lt;2500),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &lt;1000))

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires Management Approval %28Greenleaf%29</fullName>
        <actions>
            <name>CPQ_Management_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR(
ISPICKVAL(Quote_Type__c, &quot;Greenleaf&quot;),
ISPICKVAL(Quote_Type__c, &quot;Greenleaf Design Space&quot;)),

OR(

Number_of_Below_Minimum_Products__c &gt;=1,

NOT(ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;)), 

ISPICKVAL(Method_of_Payment__c , &quot;Monthly Direct Debit&quot;), 

ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITH a direct debit&quot;), 

ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITHOUT a direct debit&quot;), 

ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITH a direct debit&quot;), 

ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITHOUT a direct debit&quot;)

)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires Management Approval %28Wastekit%29</fullName>
        <actions>
            <name>CPQ_Management_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(

AND(

OR(
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;,  
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;), 

Number_of_Below_Target_Products__c &gt;=1),

AND(

Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;, 
Number_of_Below_Minimum_Products__c &gt;= 1),

AND(

OR(
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;,  
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;), 

Number_of_POA_Products__c &gt;=1),

AND(

OR(
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;,  
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_RSM&quot;), 

Number_of_Below_Min_WK_Consumable_Prods__c &gt;=1)

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires RSM Approval %28Greenleaf%29</fullName>
        <actions>
            <name>CPQ_Requires_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR(
ISPICKVAL(Quote_Type__c, &quot;Greenleaf&quot;),
ISPICKVAL(Quote_Type__c, &quot;Greenleaf Design Space&quot;)),

OR(

Number_of_Below_STD_Con_Term_Products__c &gt;=1,

Number_of_Bespoke_Products__c &gt;=1,

AND(Number_of_Below_Target_Products__c &gt;=1, Number_of_Below_Minimum_Products__c = 0)

)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires RSM Approval %28Hygiene%29</fullName>
        <actions>
            <name>CPQ_Requires_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Consumables&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Engineers&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Tele_Appointing&quot;,
Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;,
Owner:User.UserRole.DeveloperName != &quot;GL_Field_Sales&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_House&quot;, 
Owner:User.UserRole.DeveloperName != &quot;GL_Key_Accounts&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;,

OR(

Number_of_Below_STD_Con_Term_Products__c &gt;=1,

Number_of_Trial_Products__c &gt;=1, 

Number_of_Below_Minimum_Products__c &gt;= 1,

ISPICKVAL(SBQQ__Type__c, &quot;Trial&quot;),

AND(Number_of_Hazardous_Waste_Products__c &gt;=1, NOT(ISPICKVAL(Hazardous__c, &quot;Full&quot;)), NOT(ISPICKVAL(Hazardous__c, &quot;&quot;))),

AND(Number_of_DOC_Products__c &gt;=1, NOT(ISPICKVAL(DOC_Charge__c , &quot;Full&quot;)), NOT(ISPICKVAL(DOC_Charge__c , &quot;&quot;))),

AND(Number_of_Carriage_Charge_Products__c &gt;=1, NOT(ISPICKVAL(Carriage_Charge__c, &quot;Full&quot;)), NOT(ISPICKVAL(Carriage_Charge__c, &quot;&quot;))),

NOT(ISPICKVAL(SBQQ__PaymentTerms__c, &quot;Net 30&quot;)),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Monthly Direct Debit&quot;),
Total_Contract_Value__c&lt;15000),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &lt;500),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &lt;1000),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITH a direct debit&quot;),
SBQQ__NetAmount__c &lt;5000),

AND(
ISPICKVAL(Method_of_Payment__c , &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
SBQQ__NetAmount__c &lt;2000))

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Requires RSM Approval %28Wastekit%29</fullName>
        <actions>
            <name>CPQ_Requires_RSM_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR(
Owner:User.UserRole.DeveloperName = &quot;WK_ASM&quot;, 
Owner:User.UserRole.DeveloperName = &quot;WK_Consumables&quot;,
Owner:User.UserRole.DeveloperName = &quot;WK_Engineers&quot;,  
Owner:User.UserRole.DeveloperName = &quot;WK_Tele_Appointing&quot;
),

OR(

Number_of_Below_Maximum_Products__c &gt;=1, 

Number_of_Below_Target_Products__c &gt;=1

))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Set Default Quote</fullName>
        <actions>
            <name>CPQ_Quote_Set_Default_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(

NOT(ISNEW()),

SBQQ__LineItemCount__c &gt;= 1,

OR(

AND(RSM_Approval_Required__c = TRUE, RSM_Approved__c = FALSE),

AND(Management_Approval_Required__c = TRUE,  Management_Approved__c = FALSE)

)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Set Quote Process %26 Template %28Greenleaf Design Space%29</fullName>
        <actions>
            <name>Set_Quote_Process_ID_Greenleaf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Template_ID_Greenleaf_DS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Type_Greenleaf_Design_Space</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName = &quot;Libby Sprason&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Set Quote Process %26 Template %28Greenleaf%29</fullName>
        <actions>
            <name>Set_Quote_Process_ID_Greenleaf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Template_ID_Greenleaf</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Type_Greenleaf</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
$User.FirstName &amp; &quot; &quot; &amp; $User.LastName != &quot;Libby Sprason&quot;,
OR(
CONTAINS($UserRole.DeveloperName,&quot;Greenleaf&quot;),
CONTAINS($UserRole.DeveloperName,&quot;GL&quot;)
)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Set Quote Process %26 Template %28Hygiene%29</fullName>
        <actions>
            <name>CPQ_Set_Quote_Template_Hygiene</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Process_ID_Hygiene</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Type_Hygiene</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

OR(

AND(
NOT(CONTAINS($UserRole.DeveloperName,&quot;Greenleaf&quot;)),
NOT(CONTAINS($UserRole.DeveloperName,&quot;GL&quot;)),
NOT(CONTAINS($UserRole.DeveloperName,&quot;Wastekit&quot;)),
NOT(CONTAINS($UserRole.DeveloperName,&quot;WK&quot;))),

AND(ISBLANK($UserRole.DeveloperName)))

)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CPQ - Set Quote Process %26 Template %28Wastekit%29</fullName>
        <actions>
            <name>CPQ_Set_Quote_Template_Wastekit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Process_ID_Wastekit</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quote_Type_Wastekit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
CONTAINS($UserRole.DeveloperName,&quot;Wastekit&quot;),
CONTAINS($UserRole.DeveloperName,&quot;WK&quot;)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote - Set Date Sent on Quote</fullName>
        <actions>
            <name>CPQ_Quote_Set_Date_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to stamp the date sent on the quote when the status has been set to &apos;Sent&apos;.</description>
        <formula>AND(
ISBLANK(PHSCPQ_Date_Sent__c),
ISPICKVAL(SBQQ__Status__c, &quot;Sent&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Field Sales Big Deal Alert</fullName>
        <actions>
            <name>CPQ_Field_Sales_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ManagerId</field>
            <operation>equals</operation>
            <value>Caterina Wokoh</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Hygiene</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Greenleaf Big Deal Alert</fullName>
        <actions>
            <name>CPQ_Greenleaf_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Greenleaf,Greenleaf Design Space</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Inbound Big Deal Alert</fullName>
        <actions>
            <name>CPQ_Inbound_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ManagerId</field>
            <operation>equals</operation>
            <value>Jonathan Price</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>2000</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Hygiene</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Outbound Big Deal Alert</fullName>
        <actions>
            <name>CPQ_Outbound_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ManagerId</field>
            <operation>equals</operation>
            <value>Shellie Stevens</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>2000</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Hygiene</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Wastekit Big Deal Alert %28Field Sales%29</fullName>
        <actions>
            <name>CPQ_Wastekit_1_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Wastekit</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>15000</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Created_By_Profile__c</field>
            <operation>equals</operation>
            <value>WK RSM,WK ASM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote - Wastekit Big Deal Alert %28Tele-Appointing%2C Consumables Originated%29</fullName>
        <actions>
            <name>CPQ_Wastekit_2_Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Created_By_Profile__c</field>
            <operation>equals</operation>
            <value>WK Tele-Appointing,WK Consumables</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.SBQQ__NetAmount__c</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__Quote__c.Quote_Type__c</field>
            <operation>equals</operation>
            <value>Wastekit</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - Carriage Reset</fullName>
        <actions>
            <name>Reset_Carriage_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Quoted_Carriage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Calculated_Carriage_Total__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - Carriage Waiver Fee</fullName>
        <actions>
            <name>Update_Carriage_Quoted_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Carriage_Charge__c</field>
            <operation>equals</operation>
            <value>Waiver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - DoC Reset</fullName>
        <actions>
            <name>Reset_DoC_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Quoted_DoC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Number_of_DOC_Product__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - DoC Waiver Fee</fullName>
        <actions>
            <name>Update_DoC_Quoted_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.DOC_Charge__c</field>
            <operation>equals</operation>
            <value>Waiver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - HW Reset</fullName>
        <actions>
            <name>Reset_HW_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Quoted_HW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Number_of_HW_Products__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - HW Waiver Fee</fullName>
        <actions>
            <name>Update_HW_Quoted_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Hazardous__c</field>
            <operation>equals</operation>
            <value>Waiver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - Install Reset</fullName>
        <actions>
            <name>Reset_Install_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_Quoted_Install</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Installation_Total__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supp - Installation Waiver Fee</fullName>
        <actions>
            <name>Update_Quoted_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__Quote__c.Installation_Charge__c</field>
            <operation>equals</operation>
            <value>Waiver</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
