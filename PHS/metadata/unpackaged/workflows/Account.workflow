<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Change_Location_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Master_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Account - Change Location Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DM_Populate_Postal_Code_Action</fullName>
        <field>DM_Postal_Code__c</field>
        <formula>BillingPostalCode</formula>
        <name>DM Populate Postal Code Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Activity_Update_Action</fullName>
        <field>Last_Activity__c</field>
        <formula>NOW()</formula>
        <name>Last Activity Update Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_fix_action</fullName>
        <field>Phone</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)</formula>
        <name>Phone fix action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account - Change Location RT to Normal RT</fullName>
        <actions>
            <name>Account_Change_Location_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DM Populate Postal Code</fullName>
        <actions>
            <name>DM_Populate_Postal_Code_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR(     AND( 	ISNEW(), 	ISBLANK( DM_Postal_Code__c )     ),     ISBLANK( DM_Postal_Code__c )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Last Activity Update</fullName>
        <actions>
            <name>Last_Activity_Update_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT($Setup.Override_Automation__c.Workflow__c),
ISCHANGED( LastModifiedDate )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Substitute Phone Account</fullName>
        <actions>
            <name>Phone_fix_action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
