<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_New_Owner_Of_The_Case</fullName>
        <description>Notify New Owner Of The Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Management_Email_Templates/Task_Assignment_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_field</fullName>
        <field>Task_Date_In_Future__c</field>
        <literalValue>1</literalValue>
        <name>Change field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Date</fullName>
        <field>Closed_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Due_Date_To_Now</fullName>
        <field>Due_Date_and_Time__c</field>
        <formula>NOW()</formula>
        <name>Set Due Date To Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Task_Status</fullName>
        <field>Task_isActive__c</field>
        <literalValue>0</literalValue>
        <name>Set Task Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>New Task Email To Owner</fullName>
        <actions>
            <name>Notify_New_Owner_Of_The_Case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISCHANGED( OwnerId ), LEFT(WhatId, 3) == TEXT(500) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Closed Date</fullName>
        <actions>
            <name>Set_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( Status, &apos;Completed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Due Date To Now When Task Is Closed</fullName>
        <actions>
            <name>Set_Due_Date_To_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISPICKVAL(Status, &apos;Completed&apos;),  NOT(ISPICKVAL(PRIORVALUE(Status), &apos;Completed&apos;)),  $Setup.Glogbal_Settings__c.Can_Edit_Closed_Cases__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Task Status To False</fullName>
        <actions>
            <name>Set_Task_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR (     ISBLANK(Due_Date_and_Time__c),     Due_Date_and_Time__c &lt;= LastModifiedDate )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Task Status To True</fullName>
        <actions>
            <name>Change_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Required to make the validation for not adding tasks to closed cases</description>
        <formula>Due_Date_and_Time__c &gt; LastModifiedDate</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TB Set Task Status To False</fullName>
        <active>false</active>
        <description>Set the task is active field on the task to false once the due date is in the past.</description>
        <formula>NOT(ISBLANK(Due_Date_and_Time__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Task_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Task.Due_Date_and_Time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
