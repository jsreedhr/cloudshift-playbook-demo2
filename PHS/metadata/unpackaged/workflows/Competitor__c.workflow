<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Competitor_Required</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>New Competitor Required</description>
        <protected>false</protected>
        <recipients>
            <recipient>54378@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/New_Competitor_Required</template>
    </alerts>
    <rules>
        <fullName>Competitor Reminder</fullName>
        <active>false</active>
        <formula>Contract_End_Date__c -  TODAY()  &lt;= 90</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Competitor Required</fullName>
        <actions>
            <name>New_Competitor_Required</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( Competitor_Account__r.Name = &apos;Competitor Unknown&apos;,  NOT( ISBLANK( Unknown_Competitor_Details__c ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
