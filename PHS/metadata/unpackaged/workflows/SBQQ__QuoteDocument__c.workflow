<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_CPQ_Quote_Sent_Date</fullName>
        <field>PHSCPQ_Date_Sent__c</field>
        <formula>CreatedDate</formula>
        <name>Update CPQ Quote Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>SBQQ__Quote__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CPQ_Quote_Status</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Accepted</literalValue>
        <name>Update CPQ Quote Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>SBQQ__Quote__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CPQ_Quote_to_Sent</fullName>
        <field>SBQQ__Status__c</field>
        <literalValue>Sent</literalValue>
        <name>Update CPQ Quote to Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>SBQQ__Quote__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Primary_Checkbox</fullName>
        <description>Primary__c on Quote to TRUE to ensure validations will fire</description>
        <field>SBQQ__Primary__c</field>
        <literalValue>1</literalValue>
        <name>Update Primary Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>SBQQ__Quote__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Quote Document - DocuSign Status Completed</fullName>
        <actions>
            <name>Update_CPQ_Quote_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Primary_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__QuoteDocument__c.SBQQ__ElectronicSignature__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__QuoteDocument__c.SBQQ__SignatureStatus__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Document - Updates to Quote</fullName>
        <actions>
            <name>Update_CPQ_Quote_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CPQ_Quote_to_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SBQQ__QuoteDocument__c.SBQQ__ElectronicSignature__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SBQQ__QuoteDocument__c.SBQQ__SignatureStatus__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
