<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Current_Term</fullName>
        <field>Current_Term__c</field>
        <formula>TEXT(Product2.Current_Term__c)</formula>
        <name>Current Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Maximum_Price</fullName>
        <field>Maximum_Price_WF__c</field>
        <formula>Product2.Maximum_Price__c</formula>
        <name>Maximum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Minimum_Price</fullName>
        <field>Minimum_Price_WF__c</field>
        <formula>Product2.Minimum_Price__c</formula>
        <name>Minimum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity</fullName>
        <field>Quantity</field>
        <formula>Quantity__c * IF (Service_Frequency__c &gt; 1, Service_Frequency__c, 1)</formula>
        <name>Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity_TS</fullName>
        <field>Quantity</field>
        <formula>IF( Product2.Contract_Frequency__c = 26 , Quantity__c * 52, Quantity__c * Product2.Contract_Frequency__c )</formula>
        <name>Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bespoke_Product</fullName>
        <field>Bespoke_Product_WF__c</field>
        <formula>Product2.ProductCode</formula>
        <name>Set Bespoke Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Quantity</fullName>
        <field>Quantity</field>
        <formula>Quantity__c * IF (Service_Frequency__c &gt; 1, Service_Frequency__c, 1)</formula>
        <name>Set Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Target_Price</fullName>
        <field>Target_Price_WF__c</field>
        <formula>Product2.Target_Price__c</formula>
        <name>Set Target Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Greenleaf Quote Checks</fullName>
        <actions>
            <name>Maximum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Minimum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Bespoke_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Target_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>QuoteLineItem.Maximum_Price_WF__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>QuoteLineItem.Minimum_Price_WF__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>QuoteLineItem.Target_Price_WF__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>QuoteLineItem.Bespoke_Product_WF__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Greenleaf</value>
        </criteriaItems>
        <description>Copies: 
maximum price from product, to Maximum Price WF; 
minimum price from product, to Minimum Price WF; 
target price from product, to Target Price WF; 
and product name from product, if Bespoke, to Bespoke Product WF.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Current Term</fullName>
        <actions>
            <name>Current_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( 
NOT($Setup.Override_Automation__c.Workflow__c) 
 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Quantity</fullName>
        <actions>
            <name>Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND    (  NOT($Setup.Override_Automation__c.Workflow__c),      OR (  ISNEW(),             ISCHANGED(Quantity__c),              ISCHANGED(Service_Frequency__c)           )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Quantity Treadsmart</fullName>
        <actions>
            <name>Quantity_TS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND    (  NOT($Setup.Override_Automation__c.Workflow__c),      Product2.Division_Name__c  =  &apos;Treadsmart&apos;,      OR (  ISNEW(),             ISCHANGED(Quantity__c)           )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
