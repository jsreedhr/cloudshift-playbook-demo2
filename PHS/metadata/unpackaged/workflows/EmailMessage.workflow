<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_signal_customer_created_from_email</fullName>
        <field>Created_By_Customer_Email__c</field>
        <literalValue>1</literalValue>
        <name>Case signal customer created from email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Awaiting_PHS_Response</fullName>
        <field>Status</field>
        <literalValue>Awaiting PHS Response</literalValue>
        <name>Set Awaiting PHS Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_ID</fullName>
        <field>Email_ID__c</field>
        <formula>Id</formula>
        <name>Update Email ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Populate Email ID</fullName>
        <actions>
            <name>Update_Email_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Awaiting Response</fullName>
        <actions>
            <name>Set_Awaiting_PHS_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( Incoming, Parent.Email_To_Case_First_Email_Received__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Signal case created by customer email</fullName>
        <actions>
            <name>Case_signal_customer_created_from_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Email_To_Case_First_Email_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
