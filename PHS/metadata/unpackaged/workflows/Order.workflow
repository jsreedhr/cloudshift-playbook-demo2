<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Agreement_Approved</fullName>
        <description>Agreement Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/Contract_Management_Approved_Quote</template>
    </alerts>
    <alerts>
        <fullName>Approval_Rejected</fullName>
        <description>Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/Contract_Rejected_Agreement</template>
    </alerts>
    <alerts>
        <fullName>Approved_Contract_Email_Alert_To_Owner</fullName>
        <description>Approved Contract Email Alert To Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Approval_Email_Alert_Approved</template>
    </alerts>
    <alerts>
        <fullName>Contracts_Signature_Approval_Email_Alert</fullName>
        <description>Contracts Signature Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Signature_Approval_Email_Alert_Approved</template>
    </alerts>
    <alerts>
        <fullName>Contracts_Signature_Rejection_Email_Alert</fullName>
        <description>Contracts Signature Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Signature_Approval_Email_Alert_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Order_Send_Contract_Verified_Approval_Email_Alert</fullName>
        <description>Order - Send Contract Verified Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CPQ_Folder/Order_Contracts_Verified_Email_Alert</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Contract_Email_Alert_To_Owner</fullName>
        <description>Rejected Contract Email Alert To Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Approval_Email_Alert_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Agreement_Does_Not_Req_Con_Approval</fullName>
        <field>Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Agreement - Does Not Req Con Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Requires_Contract_Approval</fullName>
        <field>Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Agreement - Requires Contract Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Required</fullName>
        <field>Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Approved_Checklist</fullName>
        <field>Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Remove Approved Checklist</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Status_to_Draft</fullName>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Reset Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Signature_Authorised_Date</fullName>
        <field>Signature_Authorised_Date__c</field>
        <formula>Now()</formula>
        <name>Set Signature Authorised Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Awaiting_Approval</fullName>
        <field>Status</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Status to Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Approval_Required</fullName>
        <field>Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Approval Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agreement_Status</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Agreement Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Box</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Approved Box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Approved_Field</fullName>
        <field>Agreement_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update Contract Approved Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Generated_Field</fullName>
        <field>Agreement_Generated__c</field>
        <literalValue>0</literalValue>
        <name>Update Contract Generated Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signature_Approval_Required_Field</fullName>
        <field>Signature_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>Update Signature Approval Required Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signature_Authorised</fullName>
        <field>Signature_Authorised__c</field>
        <literalValue>1</literalValue>
        <name>Update Signature Authorised</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Agreement - Does Not Requires Contract Approval - Contract Specific</fullName>
        <actions>
            <name>Agreement_Does_Not_Req_Con_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;,  
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;, 
Number_of_Order_Products__c &gt;=1,

OR( 
  
  AND(ISPICKVAL(Original_Method_of_Payment__c, &quot;Yearly in Advance&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;))
  ),
  
  AND( 
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;))
  ),
  
  AND( 
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;))
  ),
  
  AND(ISPICKVAL(Original_Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;))
  ),
  
  AND(
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;))
  ),

  AND(
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Monthly direct debit&quot;),

  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Yearly in Advance&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;)))

  ),

OR(

  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 30&quot;), 

  OR(
  ISPICKVAL(Payment_Terms__c, &quot;Net 30&quot;))
  ),
 
  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 45&quot;), 
 
  OR( 
  ISPICKVAL(Payment_Terms__c, &quot;Net 30&quot;),
  ISPICKVAL(Payment_Terms__c, &quot;Net 45&quot;)) 
  ), 
 
  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 60&quot;), 

  OR( 
  ISPICKVAL(Payment_Terms__c, &quot;Net 30&quot;),
  ISPICKVAL(Payment_Terms__c, &quot;Net 45&quot;),
  ISPICKVAL(Payment_Terms__c, &quot;Net 60&quot;))
  ), 

  AND(
  ISPICKVAL(Original_Payment_Terms__c, &quot;Net 90&quot;),

  OR(
  ISPICKVAL(Payment_Terms__c, &quot;Net 30&quot;),
  ISPICKVAL(Payment_Terms__c, &quot;Net 45&quot;),
  ISPICKVAL(Payment_Terms__c, &quot;Net 60&quot;),
  ISPICKVAL(Original_Payment_Terms__c, &quot;Net 90&quot;))
  )
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Agreement - Requires Contract Approval - Contract Specific</fullName>
        <actions>
            <name>Agreement_Requires_Contract_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(

Owner:User.UserRole.DeveloperName != &quot;WK_RSM&quot;, 
Owner:User.UserRole.DeveloperName != &quot;Wastekit&quot;, 
Owner:User.UserRole.DeveloperName != &quot;WK_Management&quot;,  
Owner:User.UserRole.DeveloperName != &quot;Greenleaf&quot;, 
Number_of_Order_Products__c &gt;=1,

OR( 
  
  AND(ISPICKVAL(Original_Method_of_Payment__c, &quot;Yearly in Advance&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;), 
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;), 
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;))
  ),
  
  AND( 
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Half yearly invoicing WITH a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;), 
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;))
  ),
  
  AND( 
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Half yearly invoicing WITHOUT a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;))
  ),
  
  AND(ISPICKVAL(Original_Method_of_Payment__c, &quot;Quarterly invoicing WITH a direct debit&quot;), 
  
  OR(
  ISPICKVAL(Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;),
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;))
  ),
  
  AND(
  ISPICKVAL(Original_Method_of_Payment__c, &quot;Quarterly invoicing WITHOUT a direct debit&quot;), 
  ISPICKVAL(Method_of_Payment__c, &quot;Monthly Direct Debit&quot;)
  ),

  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 30&quot;), 

  OR(
  ISPICKVAL(Payment_Terms__c, &quot;Net 45&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Net 60&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Net 90&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Other&quot;)) 
  ),
 
  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 45&quot;), 
 
  OR( 
  ISPICKVAL(Payment_Terms__c, &quot;Net 60&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Net 90&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Other&quot;)) 
  ), 
 
  AND(ISPICKVAL(Original_Payment_Terms__c, &quot;Net 60&quot;), 

  OR( 
  ISPICKVAL(Payment_Terms__c, &quot;Net 90&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Other&quot;))
  ), 

  AND( 
  ISPICKVAL(Original_Payment_Terms__c, &quot;Net 90&quot;), 
  ISPICKVAL(Payment_Terms__c, &quot;Other&quot;)) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Order - Contracts Verified Agreement</fullName>
        <actions>
            <name>Order_Send_Contract_Verified_Approval_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Contract Verified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
