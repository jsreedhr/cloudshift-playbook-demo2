<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>First_Response_Violation_Alert</fullName>
        <ccEmails>caseescalation@phs.co.uk</ccEmails>
        <description>First Response Violation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>50014@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>caseescalation@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/SLA_Violation_First_Response</template>
    </alerts>
    <alerts>
        <fullName>NewCaseContactEmail</fullName>
        <description>NewCaseContactEmail</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/Email2CaseEmailResponse</template>
    </alerts>
    <alerts>
        <fullName>Resolution_Violation_Alert</fullName>
        <ccEmails>caseescalation@phs.co.uk</ccEmails>
        <description>Resolution Violation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>50014@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>caseescalation@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/SLA_Violation_Resolution</template>
    </alerts>
    <alerts>
        <fullName>Second_Response_Violation_Alert</fullName>
        <ccEmails>caseescalation@phs.co.uk</ccEmails>
        <description>Second Response Violation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>50014@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>caseescalation@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/SLA_Violation_Second_Response</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_The_Case_Owner</fullName>
        <description>Send Email To The Case Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Management_Email_Templates/Case_Ownership_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_User_Upon_Closure_Action</fullName>
        <description>Send Email To User Upon Closure Action</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>customersupport@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/CaseClosed</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_User_Upon_New_Case_Comment</fullName>
        <description>Send Email To User Upon New Case Comment</description>
        <protected>false</protected>
        <recipients>
            <field>Internal_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>customersupport@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Management_Email_Templates/New_Case_Comment</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Stopped_to_False</fullName>
        <description>Set the case stopped checkbox to false</description>
        <field>IsStopped</field>
        <literalValue>0</literalValue>
        <name>Case Stopped to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Stopped_to_True</fullName>
        <description>Set the case stopped checkbox to true</description>
        <field>IsStopped</field>
        <literalValue>1</literalValue>
        <name>Case Stopped to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Diarised_Date</fullName>
        <description>Clear the case diarised date and time.</description>
        <field>Diarised_Date_Time__c</field>
        <name>Clear Diarised Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_To_Working</fullName>
        <field>Status</field>
        <literalValue>Working</literalValue>
        <name>Set Case To Working</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_case_status_to_Diarised</fullName>
        <description>When a user selects a Diarised Date Time (eg not blank), we auto update the status to &quot;Diarised&quot;</description>
        <field>Status</field>
        <literalValue>Diarised</literalValue>
        <name>Set case status to Diarised</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Store_Prior_Value</fullName>
        <field>Previous_Diarised_Date__c</field>
        <formula>PRIORVALUE( Diarised_Date_Time__c )</formula>
        <name>Store Prior Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Store_initial_origin_on_E2C</fullName>
        <field>Email_to_Case_Origin__c</field>
        <formula>text(Origin)</formula>
        <name>Store initial origin on E2C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Subject_Action</fullName>
        <field>Subject</field>
        <formula>&quot;sf - quick case&quot;</formula>
        <name>Update Case Subject Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto-setting case to Diarised status</fullName>
        <actions>
            <name>Set_case_status_to_Diarised</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Auto-set when a Diarised Date &amp; Time is entered (eg not blank)</description>
        <formula>AND( OR(ISBLANK(PRIORVALUE(Diarised_Date_Time__c)),ISNEW()), NOT(ISPICKVAL(Status, &quot;Diarised&quot;)), NOT(ISBLANK (Diarised_Date_Time__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Work Item Completion notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Work Item</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Automatic email sent to parent case owner when a related work item case status is set to complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed Send Email</fullName>
        <actions>
            <name>Send_Email_To_User_Upon_Closure_Action</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT($Setup.Override_Automation__c.Workflow__c), Suppress_Email__c=False,  ISPICKVAL(Status,&apos;Closed&apos;), !ISPICKVAL(PRIORVALUE(Status), &apos;Closed&apos;), RecordType.DeveloperName != &apos;Quick_Case&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case Ownership Taken</fullName>
        <actions>
            <name>Send_Email_To_The_Case_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Case_To_Working</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (       NOT(CONTAINS(OwnerId , &apos;00G&apos;)),       CONTAINS(PRIORVALUE(OwnerId) , &apos;00G&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Diarised Date on Status Change</fullName>
        <actions>
            <name>Clear_Diarised_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Store_Prior_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear the Diarised Date &amp; Time when the status is changed from a diarised state.</description>
        <formula>AND( ISPICKVAL(PRIORVALUE(Status), &quot;Diarised&quot;), NOT(ISPICKVAL(Status, &quot;Diarised&quot;)), NOT(ISBLANK(Diarised_Date_Time__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Quick Case</fullName>
        <actions>
            <name>Close_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &apos;Quick_Case&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Quick Case Name</fullName>
        <actions>
            <name>Update_Case_Subject_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.DeveloperName = &apos;Quick_Case&apos;, OR( ISCHANGED(Subject), ISNEW() ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Diarised Stop Milestones</fullName>
        <actions>
            <name>Case_Stopped_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Diarised</value>
        </criteriaItems>
        <description>When the case status is set to diarised, update the stopped flag to true so that the case milestones are paused.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EmailContactRegardingCase</fullName>
        <actions>
            <name>NewCaseContactEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email notification when a request is submitted which contains a unique reference number.</description>
        <formula>AND( 	NOT($Setup.Override_Automation__c.Workflow__c),         NOT(Suppress_Email__c),         RecordType.DeveloperName != &apos;Quick_Case&apos;,          OR (             AND (                 ISCHANGED(ContactId),                  ISBLANK(PRIORVALUE(ContactId))             ),             ISNEW()         ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Record Origin on Creation</fullName>
        <actions>
            <name>Store_initial_origin_on_E2C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Rule to record the intermediary Origin on creation. Ideally, this would only execute for email-to-case cases.</description>
        <formula>NOT (ISBLANK(SuppliedEmail))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send email upon case comment update</fullName>
        <actions>
            <name>Send_Email_To_User_Upon_New_Case_Comment</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Case_Comment_Added_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Undiarised Start Milestones</fullName>
        <actions>
            <name>Case_Stopped_to_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Diarised</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When the case status is set from diarised, update the stopped flag to false so that the case milestones are unpaused. Only for cases that haven&apos;t been closed, as these should not reopen milestones.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
