<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_Key_Account_Owner</fullName>
        <description>Alert Key Account Owner</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Alert_Key_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>Big_Deal_Alert</fullName>
        <description>Big Deal Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>brightgen@phs.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Draft/Opportunity_Big_Deal</template>
    </alerts>
    <alerts>
        <fullName>New_Opportunity_Assignment_Email</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>New Opportunity Assignment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/New_Opportunity_Assigned_Alert</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Closed_Won_Email_Alert</fullName>
        <description>Opportunity Closed Won Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Won_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Send_Driver_Observation_Closed_Won_Email</fullName>
        <description>Opportunity - Send Driver Observation &apos;Closed Won&apos; Email</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Opportunity_Driver_Closed_Won</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Send_Driver_Opportunity_Closed_Lost_Email</fullName>
        <description>Opportunity - Send Driver Opportunity &apos;Closed-Lost&apos; Email</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Opportunity_Driver_Closed_Lost</template>
    </alerts>
    <fieldUpdates>
        <fullName>Amount_Transfer_Action</fullName>
        <field>Amount</field>
        <formula>Amount_Temp__c</formula>
        <name>Amount Transfer Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_At_Stage</fullName>
        <field>Closed_At_Stage__c</field>
        <formula>TEXT(PRIORVALUE(StageName))</formula>
        <name>Closed At Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_Date</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_Date_Action</fullName>
        <field>CloseDate</field>
        <formula>Closed_Date_Temp__c</formula>
        <name>Closed Date Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closure_Reason_Lost</fullName>
        <field>Closure_Reason__c</field>
        <literalValue>Lost</literalValue>
        <name>Closure Reason Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closure_Reason_Won</fullName>
        <field>Closure_Reason__c</field>
        <literalValue>Won</literalValue>
        <name>Closure Reason Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Customer_Contacted_Action</fullName>
        <field>Customer_Contacted__c</field>
        <literalValue>1</literalValue>
        <name>Opp Customer Contacted Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Default_Opp_Name</fullName>
        <field>Name</field>
        <formula>IF(ISBLANK(Account.Name), &quot;Opportunity&quot;, Account.Name) &amp;
IF(ISBLANK(Account.BillingPostalCode ) , &quot;&quot;, &quot; - &quot; &amp; Account.BillingPostalCode) &amp;
IF(ISBLANK(TEXT(Priority__c)), &quot;&quot;, &quot; - &quot; &amp; TEXT(Priority__c)) &amp;
IF(ISBLANK(TEXT(DATEVALUE(CreatedDate))), &quot;&quot;, &quot; - &quot; &amp; Text(Day(DATEVALUE(CreatedDate))) + &quot;/&quot; + Text(Month(DATEVALUE(CreatedDate))) + &quot;/&quot; + Text(Year(DATEVALUE(CreatedDate))) )</formula>
        <name>Set Default Opp Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_from_Closure_Reason_Lost</fullName>
        <field>StageName</field>
        <literalValue>Contract Lost</literalValue>
        <name>Set Stage from Closure Reason Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Contract_Won</fullName>
        <field>StageName</field>
        <literalValue>Contract Won</literalValue>
        <name>Set Stage to Contract Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Number_of_Close_Date_Changes</fullName>
        <description>Update &apos;Number of Close Date Changes&apos; everytime the &apos;Close Date&apos; is changed.</description>
        <field>NumberofCloseDateChanges__c</field>
        <formula>IF( 

ISBLANK(NumberofCloseDateChanges__c), 1,

PRIORVALUE( NumberofCloseDateChanges__c ) + 1

)</formula>
        <name>Update Number of Close Date Changes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_To_Proposal_Being_Prepared</fullName>
        <field>StageName</field>
        <literalValue>Proposal Being Prepared</literalValue>
        <name>Update Stage To Proposal Being Prepared</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_To_Quotation</fullName>
        <description>Updates the Opportunity stage to quotation.</description>
        <field>StageName</field>
        <literalValue>Quote Being Prepared</literalValue>
        <name>Update Stage To Quotation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Key Account Owner</fullName>
        <actions>
            <name>Alert_Key_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Key Accounts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Key_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Amount Transfer</fullName>
        <actions>
            <name>Amount_Transfer_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT($Setup.Override_Automation__c.Workflow__c),
ISPICKVAL(StageName, &apos;Opened Lead&apos;)
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change Stage To Proposal Being Prepared %28KAM%29</fullName>
        <actions>
            <name>Update_Stage_To_Proposal_Being_Prepared</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity stage to &apos;Proposal Being Prepared&apos; when a quote is added against a KAM opportunity.</description>
        <formula>AND( NOT($Setup.Override_Automation__c.Workflow__c),  RecordType.DeveloperName = &quot;KAM_Opportunity&quot;, Quote_Roll_Up__c  &gt; 0,  Probability &gt; 0,  Probability &lt; 0.5  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Stage To Quotation</fullName>
        <actions>
            <name>Update_Stage_To_Quotation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity stage to &apos;Quotation&apos; when a quote is added.</description>
        <formula>AND( NOT($Setup.Override_Automation__c.Workflow__c),  RecordType.DeveloperName = &quot;Non_KAM_Record&quot;, Quote_Roll_Up__c  &gt; 0,  Probability &gt; 0,  Probability &lt; 0.5  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed Date Rule</fullName>
        <actions>
            <name>Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Profile.Name  = &apos;Telesales&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won Email</fullName>
        <actions>
            <name>Opportunity_Closed_Won_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISPICKVAL(StageName, &apos;Closed Won&apos;),   NOT(ISBLANK(CloseDate)),   NOT(ISBLANK( Commencement_Date__c )) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contract Won</fullName>
        <actions>
            <name>Big_Deal_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Contract Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Opportunity Name</fullName>
        <actions>
            <name>Set_Default_Opp_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Enforce opportunity naming convention.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp Customer Contacted</fullName>
        <actions>
            <name>Opp_Customer_Contacted_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Last_Visit_Date__c</field>
            <operation>greaterThan</operation>
            <value>1/1/1997</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - New Opp Assignment Email</fullName>
        <actions>
            <name>New_Opportunity_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send an email to the new owner after the &apos;Send to Outbound&apos; and &apos;Send to Field Sales&apos; feature has been used.</description>
        <formula>ISCHANGED(Owner_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Driver Opportunity %27Closed Lost%27 email</fullName>
        <actions>
            <name>Opportunity_Send_Driver_Opportunity_Closed_Lost_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Originating_Employee_User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notify the &apos;Originating Employee&apos; (Driver) when Opportunity &apos;Stage&apos; changes to &apos;Closed - Lost&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Driver Opportunity %27Closed Won%27 email</fullName>
        <actions>
            <name>Opportunity_Send_Driver_Observation_Closed_Won_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Originating_Employee_User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Notify the &apos;Originating Employee&apos; (Driver) when Opportunity &apos;Stage&apos; changes to &apos;Closed - Won&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Closure Reason Lost</fullName>
        <actions>
            <name>Closure_Reason_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT($Setup.Override_Automation__c.Workflow__c),
ISPICKVAL( StageName , &apos;Contract Lost&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Closure Reason Won</fullName>
        <actions>
            <name>Closure_Reason_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
NOT($Setup.Override_Automation__c.Workflow__c),
ISPICKVAL( StageName , &apos;Contract Won&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Previous Stage</fullName>
        <actions>
            <name>Closed_At_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  NOT($Setup.Override_Automation__c.Workflow__c),  ISPICKVAL(StageName, &apos;Contract Lost&apos;),  ISCHANGED(StageName)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage from Closure Reason Lost</fullName>
        <actions>
            <name>Set_Stage_from_Closure_Reason_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
NOT($Setup.Override_Automation__c.Workflow__c), 
ISPICKVAL(  Closure_Reason__c  , &apos;Lost&apos;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Stage from Closure Reason Won</fullName>
        <actions>
            <name>Set_Stage_to_Contract_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 
NOT($Setup.Override_Automation__c.Workflow__c), 
ISPICKVAL(  Closure_Reason__c  , &apos;Won&apos;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opportunity Close Date Changes</fullName>
        <actions>
            <name>Update_Number_of_Close_Date_Changes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Opportunity &apos;Number of Close Date Changes&apos; field everytime the &apos;Close Date&apos; is changed.</description>
        <formula>ISCHANGED(CloseDate)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
