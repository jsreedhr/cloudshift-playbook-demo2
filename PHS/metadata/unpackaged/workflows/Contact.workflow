<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Contact</fullName>
        <description>Email to Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Draft/Default_Approval_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Phone_fix_action</fullName>
        <field>Phone</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)</formula>
        <name>Phone fix action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Substitute Phone Contact</fullName>
        <actions>
            <name>Phone_fix_action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
