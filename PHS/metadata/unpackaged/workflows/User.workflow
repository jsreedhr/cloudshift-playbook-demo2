<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Employee_number_action</fullName>
        <field>EmployeeNumber</field>
        <formula>LEFT( Username , FIND( &quot;@&quot;, Username ) - 1  )</formula>
        <name>Update Employee number action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_custom_employee_number</fullName>
        <field>Employee_Number__c</field>
        <formula>LEFT( Username , FIND( &quot;@&quot;, Username ) - 1 )</formula>
        <name>update custom employee number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Employee Number</fullName>
        <actions>
            <name>Update_Employee_number_action</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_custom_employee_number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
