<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Converted</fullName>
        <description>Lead Converted</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Lead_Converted</template>
    </alerts>
    <alerts>
        <fullName>Lead_Send_Driver_Observation_Archived_Email</fullName>
        <description>Lead - Send Driver Observation &apos;Archived&apos; Email</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Lead_Archived</template>
    </alerts>
    <alerts>
        <fullName>Lead_Send_Driver_Observation_In_Review_Email</fullName>
        <description>Lead - Send Driver Observation &apos;In Review&apos; Email</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Lead_In_Review</template>
    </alerts>
    <alerts>
        <fullName>Lead_Send_Driver_Observation_In_Review_Email2</fullName>
        <description>Lead - Send Driver Observation &apos;In Review&apos; Email</description>
        <protected>false</protected>
        <recipients>
            <field>Originating_Employee_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/Lead_In_Review</template>
    </alerts>
    <alerts>
        <fullName>New_Lead_Assignment_Email</fullName>
        <ccEmails>david@cloudshiftgroup.com</ccEmails>
        <description>New Lead Assignment Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Emails/New_Lead_Assigned_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_Auto_True</fullName>
        <field>Do_Not_Assign_Automatically__c</field>
        <literalValue>1</literalValue>
        <name>Do Not Assign Auto - True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CalloutEligibleTrue</fullName>
        <description>set field value to TRUE</description>
        <field>Callout_Eligible__c</field>
        <literalValue>1</literalValue>
        <name>CalloutEligibleTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Description</fullName>
        <field>Copy_Description__c</field>
        <formula>Description</formula>
        <name>Copy Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Advance_to_Enriched_De_Duped</fullName>
        <field>Status</field>
        <literalValue>Enriched &amp; De-Duplicated</literalValue>
        <name>Lead - Advance to Enriched &amp; De-Duped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Advance_to_Qualified</fullName>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Lead - Advance to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Advance_to_Ready_for_Conversion</fullName>
        <field>Status</field>
        <literalValue>Ready for Conversion</literalValue>
        <name>Lead - Advance to Ready for Conversion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assign_to_GL_Key_Accounts_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Greenleaf_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead - Assign to GL Key Accounts Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assign_to_Key_Accounts_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Key_Account_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Lead - Assign to Key Accounts Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Calculate_Score1</fullName>
        <description>Created as workflow due to formula reaching maximum size.</description>
        <field>Score_1__c</field>
        <formula>IF(AND(
ISBLANK(Email),
NOT(ISBLANK(Phone)),
NOT(ISBLANK(MobilePhone))), 2/3,

IF(AND(
NOT(ISBLANK(Email)),
ISBLANK(Phone),
NOT(ISBLANK(MobilePhone))), 2/3,

IF(AND(
NOT(ISBLANK(Email)),
NOT(ISBLANK(Phone)),
ISBLANK(MobilePhone)), 2/3,

IF(AND(
ISBLANK(Email),
ISBLANK(Phone),
ISBLANK(MobilePhone)), 0,

IF(AND(
NOT(ISBLANK(Email)),
NOT(ISBLANK(Phone)),
NOT(ISBLANK(MobilePhone))), 1, 1/3)))))</formula>
        <name>Lead - Calculate Score1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_In_Progress_Action</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Lead In Progress Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Set_Closed_Date</fullName>
        <field>Closed_Date__c</field>
        <formula>NOW()</formula>
        <name>Lead - Set Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Originating_Employee_Email</fullName>
        <field>Originating_Employee_Email__c</field>
        <formula>Originating_Employee_User__r.Email</formula>
        <name>Originating Employee Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Phone_fix_action</fullName>
        <field>Phone</field>
        <formula>SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Phone, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)</formula>
        <name>Phone fix action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Wildebeest_Ref</fullName>
        <field>Wildebeest_Ref__c</field>
        <formula>External_Customer_ID__c</formula>
        <name>Populate Wildebeest Ref</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Close_Date_Action</fullName>
        <field>Closed_Date__c</field>
        <formula>Now()</formula>
        <name>Set Close Date Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lead - Advance Status to Enriched %26 De-Duplicated</fullName>
        <actions>
            <name>Lead_Advance_to_Enriched_De_Duped</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND( NOT(ISBLANK(PostalCode)), NOT($User.LastName = &quot;Dev Team&quot;), OR( NOT(ISBLANK(MobilePhone)), NOT(ISBLANK(Phone)), NOT(ISBLANK(Email))),  OR(ISBLANK(Title), ISPICKVAL(Timescale__c, &quot;&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Advance Status to Qualified</fullName>
        <actions>
            <name>Lead_Advance_to_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND(  NOT(ISBLANK(PostalCode)),   OR(  NOT(ISBLANK(MobilePhone)),  NOT(ISBLANK(Phone)), NOT(ISBLANK(Email))),   NOT(ISBLANK(Title)),  NOT(ISPICKVAL(Timescale__c, &quot;&quot;)),  OR(ISBLANK(FirstName),  ISBLANK(Street),  ISBLANK(City),  ISBLANK(State),  ISBLANK(PostalCode)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Advance Status to Ready for Conversion</fullName>
        <actions>
            <name>Lead_Advance_to_Ready_for_Conversion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND(  NOT(ISBLANK(PostalCode)),   OR(  NOT(ISBLANK(MobilePhone)),  NOT(ISBLANK(Phone)), NOT(ISBLANK(Email))),   NOT(ISBLANK(Title)),  NOT(ISPICKVAL(Timescale__c, &quot;&quot;)),  NOT(ISPICKVAL(Salutation, &quot;&quot;)), NOT(ISBLANK(FirstName)), NOT(ISBLANK(Street)), NOT(ISBLANK(City)), NOT(ISBLANK(State)), NOT(ISBLANK(PostalCode)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Calculate Score1</fullName>
        <actions>
            <name>Lead_Calculate_Score1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Copy Description</fullName>
        <actions>
            <name>Copy_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>OR( ISNEW(), ISCHANGED( Description ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Reallocate to Greenleaf Queue %28Deprecated%29</fullName>
        <actions>
            <name>Lead_Assign_to_GL_Key_Accounts_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Deprecated - Replaced in trigger as part of: &apos;Lead Management: Key Account Tickbox on Lead&apos;</description>
        <formula>AND( NOT(Owner:Queue.DeveloperName = &quot;Greenleaf_Leads&quot;), NOT(CONTAINS(Owner:User.Profile.Name, &quot;Greenleaf&quot;)), Key_Account__c = FALSE, Greenleaf_Key_Account__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Reallocate to KAM Queue %28Deprecated%29</fullName>
        <actions>
            <name>Lead_Assign_to_Key_Accounts_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Deprecated - Replaced in trigger as part of: &apos;Lead Management: Key Account Tickbox on Lead&apos;</description>
        <formula>AND( NOT(Owner:Queue.DeveloperName = &quot;Lead_Key_Accounts&quot;), 

NOT(CONTAINS(Owner:User.Profile.Name, &quot;Key Account&quot;)), 
NOT(CONTAINS(Owner:User.Profile.Name, &quot;Greenleaf&quot;)), 
NOT(CONTAINS(Owner:User.Profile.Name, &quot;Wastekit Trial&quot;)), 

Key_Account_Type__c  &lt;&gt; &quot;Public Sector&quot; ,
Key_Account__c = TRUE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Send Driver %27Archived%27 Acknowledgement Email</fullName>
        <actions>
            <name>Lead_Send_Driver_Observation_Archived_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Notify the &apos;Originating Employee&apos; (Driver) when Lead &apos;Status&apos; changes to &apos;Archived&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Send Driver Observation %27Converted%27 Email</fullName>
        <actions>
            <name>Lead_Converted</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Converted To Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Send Driver Observation Acknowledgement Email</fullName>
        <actions>
            <name>Lead_Send_Driver_Observation_In_Review_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>In Review</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Send to Outbound%2FField Sales Owner Change</fullName>
        <actions>
            <name>New_Lead_Assignment_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send an email to the new owner after the &apos;Send to Outbound&apos; and &apos;Send to Field Sales&apos; feature has been used.</description>
        <formula>ISCHANGED(Owner_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Set Closure Date</fullName>
        <actions>
            <name>Lead_Set_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND( ISBLANK(Closed_Date__c), OR(ISPICKVAL(Status, &quot;Archived&quot;), NOT(ISPICKVAL(Closure_Reason__c, &quot;&quot;))))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Contract Term Set</fullName>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND(  NOT($Setup.Override_Automation__c.Workflow__c),   Contract_Term__c =  Contract_End_Date__c  -  Contract_Start_Date__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Originating Employee</fullName>
        <actions>
            <name>Originating_Employee_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Originating_Employee_User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Wildebeest Ref</fullName>
        <actions>
            <name>Populate_Wildebeest_Ref</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>AND( ISNUMBER( External_Customer_ID__c ) , ISBLANK( Wildebeest_Ref__c ), BEGINS(  TEXT( LeadSource ) , &apos;Driver&apos;)  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Assign Auto to False</fullName>
        <actions>
            <name>Assign_Auto_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Callout Eligible</fullName>
        <actions>
            <name>CalloutEligibleTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>IF(  OR(ISNEW(),    Wildebeest_Ref__c != PRIORVALUE( Wildebeest_Ref__c)  ) , TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Close Date</fullName>
        <actions>
            <name>Set_Close_Date_Action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT($Setup.Override_Automation__c.Workflow__c),  OR(ISPICKVAL( Status , &apos;Closed Lost&apos;),ISPICKVAL( Status , &apos;Converted To Opportunity&apos;))   )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to In Review</fullName>
        <actions>
            <name>Lead_Send_Driver_Observation_In_Review_Email2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the status to &quot;In Review&quot; when an update is applied against the Lead</description>
        <formula>AND( NOT($Setup.Override_Automation__c.Workflow__c),
NOT($User.LastName = &quot;Dev Team&quot;),
ISPICKVAL(Status, &apos;New&apos;), ISCHANGED(LastModifiedDate)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Substitute Phone Lead</fullName>
        <actions>
            <name>Phone_fix_action</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>DEPRECATED - Functionality moved to Trigger.</description>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
