<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Quantity</fullName>
        <field>Quantity</field>
        <formula>Quantity__c * IF (Service_Frequency__c &gt; 1, Service_Frequency__c, 1)</formula>
        <name>Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quantity_TS2</fullName>
        <field>Quantity</field>
        <formula>IF( Product2.Contract_Frequency__c = 26 , Quantity__c * 52, Quantity__c * Product2.Contract_Frequency__c )</formula>
        <name>Quantity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Quantity</fullName>
        <actions>
            <name>Quantity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND  ( NOT($Setup.Override_Automation__c.Workflow__c),    OR ( ISNEW(),         ISCHANGED(Quantity__c),         ISCHANGED(Service_Frequency__c)        )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Quantity Treadsmart</fullName>
        <actions>
            <name>Quantity_TS2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (       NOT($Setup.Override_Automation__c.Workflow__c),      Product2.Division_Name__c  = &apos;Treadsmart&apos;,      OR ( ISNEW(),            ISCHANGED(Quantity__c),            ISCHANGED(Service_Frequency__c)          )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
