<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Above_Maximum_Price</fullName>
        <field>Below_Maximum__c</field>
        <name>CPQ QLI - Record Above Maximum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Above_Minimum_10</fullName>
        <field>Below_Minimum_Plus_Ten__c</field>
        <name>CPQ QLI - Record Above Minimum + 10%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Above_Minimum_Price</fullName>
        <field>Below_Minimum__c</field>
        <name>CPQ QLI - Record Above Minimum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Above_Standard_Contract</fullName>
        <field>Below_Standard_Contract_Term__c</field>
        <literalValue>0</literalValue>
        <name>CPQ QLI - Record Above Standard Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Above_Target_Price</fullName>
        <field>Below_Target__c</field>
        <name>CPQ QLI - Record Above Target Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Below_Maximum_Price</fullName>
        <field>Below_Maximum__c</field>
        <formula>&apos;Yes&apos;</formula>
        <name>CPQ QLI - Record Below Maximum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Below_Minimum_10</fullName>
        <field>Below_Minimum_Plus_Ten__c</field>
        <formula>&apos;Yes&apos;</formula>
        <name>CPQ QLI - Record Below Minimum + 10%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Below_Minimum_Price</fullName>
        <field>Below_Minimum__c</field>
        <formula>&apos;Yes&apos;</formula>
        <name>CPQ QLI - Record Below Minimum Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Record_Below_Target_Price</fullName>
        <field>Below_Target__c</field>
        <formula>&apos;Yes&apos;</formula>
        <name>CPQ QLI - Record Below Target Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Set_Approval_Flag</fullName>
        <field>Approval_Flag__c</field>
        <literalValue>1</literalValue>
        <name>CPQ QLI - Set Approval Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_QLI_Untick_Approval_Flag</fullName>
        <field>Approval_Flag__c</field>
        <literalValue>0</literalValue>
        <name>CPQ QLI - Untick Approval Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Record_if_1_Below_STD_Con_Term</fullName>
        <field>X1_Year_Below_STD_Contract_Term__c</field>
        <literalValue>1</literalValue>
        <name>CPQ - Record if &gt;1 Below STD Con Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Record_if_Below_STD_Contract_Term</fullName>
        <field>Below_Standard_Contract_Term__c</field>
        <literalValue>1</literalValue>
        <name>CPQ - Record if Below STD Contract Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CPQ_Record_if_Not_1_Below_STD_Con_Term</fullName>
        <field>X1_Year_Below_STD_Contract_Term__c</field>
        <literalValue>0</literalValue>
        <name>CPQ -Record if Not &gt;1 Below STD Con Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QLI_Set_Bespoke_Product</fullName>
        <field>PHSCPQ_Bespoke_Product__c</field>
        <literalValue>1</literalValue>
        <name>QLI - Set Bespoke Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Product_Family_Range_Combination</fullName>
        <field>PHSCPQ_Product_Family_Range_Combination__c</field>
        <formula>SBQQ__Product__r.Product_Family_Range_Combination__c</formula>
        <name>Set Product Family &amp; Range Combination</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CPQ QLI - Does Not Require Hygiene Approval</fullName>
        <actions>
            <name>CPQ_QLI_Untick_Approval_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 

SBQQ__Quote__r.Developer_Role__c != &quot;WK_ASM&quot;, 
SBQQ__Quote__r.Developer_Role__c !=&quot;WK_Consumables&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Engineers&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Tele_Appointing&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_RSM&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;Wastekit&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Management&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;GL_Field_Sales&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;GL_House&quot;, 
SBQQ__Quote__r.Developer_Role__c !=&quot;GL_Key_Accounts&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;Greenleaf&quot;, 

VALUE(Text(PHSCPQ_Standard_Contract_Term__c))-VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c))&gt;=0, 

((SBQQ__NetPrice__c - Minimum_Price__c) &gt;= 0), 

OR(ISPICKVAL(SBQQ__Quote__r.Hazardous__c, &quot;Full&quot;), PHSCPQ_Attracts_HW_Paperwork_Charge__c = FALSE), 

OR(ISPICKVAL(SBQQ__Quote__r.DOC_Charge__c , &quot;Full&quot;), PHSCPQ_Attracts_DoC_Charge__c = FALSE), 

OR(ISPICKVAL(SBQQ__Quote__r.Carriage_Charge__c, &quot;Full&quot;), PHSCPQ_Attracts_Carriage_Charge__c=FALSE)

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Record %3E1 Below Standard Contract Term</fullName>
        <actions>
            <name>CPQ_Record_if_1_Below_STD_Con_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate if the actual contract term selected is less than the standard contract term by more than a year.</description>
        <formula>IF(VALUE(Text(PHSCPQ_Standard_Contract_Term__c))-VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c))&lt;-1, TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Record Above Standard Contract Term</fullName>
        <actions>
            <name>CPQ_QLI_Record_Above_Standard_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate if the actual contract term selected is more than the standard contract term.</description>
        <formula>VALUE(Text(PHSCPQ_Standard_Contract_Term__c))&gt;= VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Record Below Standard Contract Term</fullName>
        <actions>
            <name>CPQ_Record_if_Below_STD_Contract_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate if the actual contract term selected is less than the standard contract term.</description>
        <formula>VALUE(Text(PHSCPQ_Standard_Contract_Term__c))-VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c))&lt;0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Record Not %3E1 Below Standard Contract Term</fullName>
        <actions>
            <name>CPQ_Record_if_Not_1_Below_STD_Con_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate if the actual contract term selected is not less than the standard contract term by more than a year.</description>
        <formula>(VALUE(Text(PHSCPQ_Standard_Contract_Term__c))-VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c)))&gt;=-1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Record Product Range Family Combination</fullName>
        <actions>
            <name>Set_Product_Family_Range_Combination</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate the product family and range combination used in WK approval processes.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ QLI - Requires Hygiene Approval</fullName>
        <actions>
            <name>CPQ_QLI_Set_Approval_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 

SBQQ__Quote__r.Developer_Role__c != &quot;WK_ASM&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Consumables&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Engineers&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Tele_Appointing&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_RSM&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;Wastekit&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;WK_Management&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;GL_Field_Sales&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;GL_House&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;GL_Key_Accounts&quot;, 
SBQQ__Quote__r.Developer_Role__c != &quot;Greenleaf&quot;, 

OR( 

VALUE(Text(PHSCPQ_Standard_Contract_Term__c))-VALUE(TEXT(SBQQ__Product__r.PHSCPQ_Standard_Contract_Term__c))&lt;0, 

((SBQQ__NetPrice__c - Minimum_Price__c) &lt; 0), 

AND(PHSCPQ_Attracts_HW_Paperwork_Charge__c = TRUE, NOT(ISPICKVAL(SBQQ__Quote__r.Hazardous__c, &quot;Full&quot;)), NOT(ISPICKVAL(SBQQ__Quote__r.Hazardous__c, &quot;&quot;))), 

AND(PHSCPQ_Attracts_DoC_Charge__c = TRUE, NOT(ISPICKVAL(SBQQ__Quote__r.DOC_Charge__c , &quot;Full&quot;)), NOT(ISPICKVAL(SBQQ__Quote__r.DOC_Charge__c , &quot;&quot;))), 

AND(PHSCPQ_Attracts_Carriage_Charge__c = TRUE, NOT(ISPICKVAL(SBQQ__Quote__r.Carriage_Charge__c, &quot;Full&quot;)), NOT(ISPICKVAL(SBQQ__Quote__r.Carriage_Charge__c, &quot;&quot;))) 

) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Above Maximum</fullName>
        <actions>
            <name>CPQ_QLI_Record_Above_Maximum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &gt;= Maximum_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Above Minimum %2B 10%25</fullName>
        <actions>
            <name>CPQ_QLI_Record_Above_Minimum_10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(SBQQ__NetPrice__c &gt;= (Minimum_Price__c - (Minimum_Price__c * 0.1)), TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Above Minimum Price</fullName>
        <actions>
            <name>CPQ_QLI_Record_Above_Minimum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &gt;= Minimum_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Above Trade%2FTarget</fullName>
        <actions>
            <name>CPQ_QLI_Record_Above_Target_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &gt;= Target_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Below Maximum</fullName>
        <actions>
            <name>CPQ_QLI_Record_Below_Maximum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &lt; Maximum_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Below Minimum %2B 10%25</fullName>
        <actions>
            <name>CPQ_QLI_Record_Below_Minimum_10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(SBQQ__NetPrice__c &lt; (Minimum_Price__c - (Minimum_Price__c * 0.1)), TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Below Minimum Price</fullName>
        <actions>
            <name>CPQ_QLI_Record_Below_Minimum_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &lt; Minimum_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CPQ Quote Line - Below Trade%2FTarget</fullName>
        <actions>
            <name>CPQ_QLI_Record_Below_Target_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>SBQQ__NetPrice__c &lt; Target_Price__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QLI - Set Bespoke Product</fullName>
        <actions>
            <name>QLI_Set_Bespoke_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
SBQQ__Product__r.ProductCode = &apos;BESPOKE&apos;, 
SBQQ__Product__r.ProductCode = &apos;BESPOKEINT&apos;, 
SBQQ__Product__r.ProductCode = &apos;BESPOKEEXT&apos;, 
SBQQ__Product__r.ProductCode = &apos;BESPOKEGRD&apos;, 
SBQQ__Product__r.ProductCode = &apos;BESPOKEREP&apos;,
SBQQ__Product__r.ProductCode = &apos;BESPOKELWL&apos;,
SBQQ__Product__r.ProductCode = &apos;BESPOKEXMS&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
