<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_case_comment_date</fullName>
        <field>Case_Comment_Added_Date__c</field>
        <formula>NOW()</formula>
        <name>Update case comment date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case comment updated</fullName>
        <actions>
            <name>Update_case_comment_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT($Setup.Override_Automation__c.Workflow__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
