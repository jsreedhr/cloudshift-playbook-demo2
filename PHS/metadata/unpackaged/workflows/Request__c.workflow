<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_on_closure_of_IT_service_request</fullName>
        <description>Send email on closure of IT service request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Request_Object_Emails/Salesforce_Closure_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Send_email_on_receipt_of_new_IT_service_request</fullName>
        <description>Send email to user on receipt of new IT service request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Request_Object_Emails/Salesforce_Request_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_gatekeeper_on_receipt_of_new_IT_service_request</fullName>
        <description>Send email to admin office gatekeeper on receipt of new IT service request</description>
        <protected>false</protected>
        <recipients>
            <recipient>54937@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Request_Object_Emails/Notify_IT_service_queue_members_of_new_request</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_queue_members_on_receipt_of_IT_service_request</fullName>
        <description>Send email to queue members on receipt of IT service request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Request_Object_Emails/Notify_IT_service_queue_members_of_new_request</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_queue_members_on_receipt_of_knowledge_request</fullName>
        <description>Send email to queue members on receipt of knowledge request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>noreply@phs.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Request_Object_Emails/Notify_knowledge_queue_members_of_new_knowledge_request</template>
    </alerts>
</Workflow>
