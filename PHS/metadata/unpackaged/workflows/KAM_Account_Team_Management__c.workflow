<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Master_Account_ID</fullName>
        <field>Master_Account_Id__c</field>
        <formula>Master_Account__r.Id</formula>
        <name>Update Master Account ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Master Account ID</fullName>
        <actions>
            <name>Update_Master_Account_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$User.IsActive</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
