<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Draft/Default_Approved_Approval_Email</template>
    </alerts>
    <alerts>
        <fullName>Approval_Rejected</fullName>
        <description>Approval Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Draft/Default_Reject_Approval_Email</template>
    </alerts>
    <alerts>
        <fullName>Below_Min_Quote_Approved</fullName>
        <description>Below Min Quote Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>Below_Target_Quote_Approved2</fullName>
        <description>Below Target Quote Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>Below_Target_Quote_Rejected</fullName>
        <description>Below Target Quote Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Request_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Bespoke_Quote_Approved</fullName>
        <description>Bespoke Quote Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Request_Approval</template>
    </alerts>
    <alerts>
        <fullName>Bespoke_Request_Rejected</fullName>
        <description>Bespoke Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Request_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Notify_of_Quote_Above_Max_Price</fullName>
        <description>Notify of Quote Above Max Price</description>
        <protected>false</protected>
        <recipients>
            <recipient>75675@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Quote_Above_Max_Price</template>
    </alerts>
    <alerts>
        <fullName>Notify_of_quote_above_5k</fullName>
        <description>Notify of quote above £5k</description>
        <protected>false</protected>
        <recipients>
            <recipient>75675@phs.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Greenleaf_Emails_Auto_Send/Quote_Above_5k</template>
    </alerts>
    <fieldUpdates>
        <fullName>BelowMin_Quote_in_Review</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Quote in Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Below_Target_Quote_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Below Target Quote Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Below_Target_Quote_Record_Type_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Below Target Quote Record Type Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Below_Target_Quote_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Below Target Quote Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Below_Target_Record_Type_Draft</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Draft</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Below Target Record Type Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Quote_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Bespoke Quote Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Quote_Record_Type_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Bespoke Quote Record Type Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Quote_Rejected2</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Bespoke Quote Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Quote_in_Review</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Bespoke Quote in Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Record_Type_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Bespoke Record Type Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bespoke_Record_Type_Draft</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Draft</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Bespoke Record Type Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Quote Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_In_Review</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Quote In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Quote Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Approved</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Draft</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Draft</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Greenleaf Quote Above Max</fullName>
        <actions>
            <name>Notify_of_Quote_Above_Max_Price</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Above_Maximum__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Greenleaf</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Greenleaf Quote Over %C2%A35k</fullName>
        <actions>
            <name>Notify_of_quote_above_5k</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.TotalPrice</field>
            <operation>greaterThan</operation>
            <value>5000</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Greenleaf</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Approval Status</fullName>
        <actions>
            <name>Quote_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Record_Type_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( NOT($Setup.Override_Automation__c.Workflow__c), Ready_For_Approval__c = TRUE, Revised_Term__c = 0 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
