trigger PHSAccountc_Trigger on PHS_Account__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    /*
    whenever a PHS Account is added the related Account needs to have its 
    Wildebeest_Ref__c updated to include the Wildebeest_Ref__c for the new PHS Account.
    
    If a PHS Account is updated and the Account has been changed then 
    remove the phs account from the wildebeest_ref__c of the previous account and add it to the new one.      
    */
    
    // Before Insert       
    if(Trigger.isBefore && Trigger.isInsert) {
      	
    }

    // After Insert
    if(Trigger.isAfter && Trigger.isInsert) {
     phs_PHSAccountc_Helper.addNewPHSAccountToAccount(Trigger.New);
    }

    // Before Update 
    if(Trigger.isBefore && Trigger.isUpdate) {
      	   
    }

    // After Update 
    if(Trigger.isAfter && Trigger.isUpdate) {
     phs_PHSAccountc_Helper.maintainAccountToPHSAccountRelationship(Trigger.oldMap,Trigger.New);
      
    }

    // Before Delete 
    if(Trigger.isBefore && Trigger.isDelete) {


    }

    // After Delete 
    if(Trigger.isAfter && Trigger.isDelete) {
     
            
    }

    // After Undelete
    if(Trigger.isAfter && Trigger.isUndelete) {
     
            
    }
   
}