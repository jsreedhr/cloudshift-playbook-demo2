trigger CPQQuote on SBQQ__Quote__c (before update, after update) {
    if(Trigger.isBefore && CPQQuoteTriggerFunctions.isBeforeUpdate){
        CPQQuoteTriggerFunctions.isBeforeUpdate = false;
        CPQQuoteTriggerFunctions.CreateContractFromAcceptedQuote(Trigger.new, Trigger.old);
    }
    if(Trigger.isAfter){
        if(CPQQuoteTriggerFunctions.isAfterUpdate){
            CPQQuoteTriggerFunctions.isAfterUpdate = false;
            CPQQuoteTriggerFunctions.SendtoHoSApproval(Trigger.new);
            CPQQuoteTriggerFunctions.isAfterUpdate = true;    
        }
    }    
}