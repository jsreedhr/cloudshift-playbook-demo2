/*********************************************************************************
 * bg_Opportunity_bi
 *
 * Before  insert trigger for the Opportunity object
 * 
 * Author: Alex Leslie - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: 22-12-2016 KH - Added setOriginatingEmployee
 * Updated: CC 23-01-2017: Added Trigger Settings
 *  
 * Author : Hormese Ambookken
 * Updated: 31-10-2017: Added Trigger Settings to update Activity
 *********************************************************************************/

trigger bg_Opportunity_bi on Opportunity(before insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}


	if (TS.Id == null || TS.Opportunity_bi_populateOwner__c)
	{
		bg_ObjectOwner_Helper.populateOwner(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME, Trigger.new);
	}

	if (TS.Id == null || TS.Opportunity_bi_setOriginatingEmployee__c)
	{
		bg_OpportunityHelper.setOriginatingEmployee(Trigger.New);
	}
	if ((TS.Id == null || TS.Opportunity_ai_populateFLTFields__c )  & bg_OpportunityHelper.runOnce())
	{
	  	bg_OpportunityHelper.populateFLTFields(Trigger.new);
	}
	
	if(bg_OpportunityHelper.isBeforeInsert)
	{
	    system.debug('---------inside before insert----');
	    bg_OpportunityHelper.isBeforeInsert = false;
    	// To update Campaigns field under Opportunity
    	OpportunityTriggerHandler.updateCampaignsField(Trigger.new);
    	
    	//To update the opportunity default name
    	OpportunityTriggerHandler.updateOppoDefaultName(Trigger.New);
    	bg_OpportunityHelper.isBeforeInsert = true;
	}
}