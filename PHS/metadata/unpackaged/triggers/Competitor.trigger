trigger Competitor on Competitor__c (before insert, before update) {
	CompetitorTriggerFunctions.SetLostOpportunity(Trigger.new);
}