/**********************************************************************
* Name:     TeleTaskTrigger
* Author:   Danny Ayres
* Date:     16 November 2017
* ======================================================
* ======================================================
* Purpose:
* To stamp the Tele-Appointer field on Lead if this is the first Task or Event created and related to the lead.
*
* ======================================================
* ======================================================
* History:
* VERSION   DATE                   NOTES
* 1.0       16 November 2017       Inital Creation
***********************************************************************/
trigger TeleTaskTrigger on Task (before insert) {
    /*
    if(trigger.isInsert){

      // To  populate Task Parent (Opportunity /Lead) feild values
                 TaskTriggerHandler.updateParentFields(trigger.new);
               if(userinfo.getUserRoleId()!=NULL &&userinfo.getUserRoleId().length()>1)
       {
        UserRole u = [SELECT Name FROM UserRole where Id =: userinfo.getUserRoleId()];

        String uname = u.name;
        if((u.name).containsIgnoreCase('Teleappointing')){
            String lead_prefix = Schema.SObjectType.Lead.getKeyPrefix();
            map<Task,Id> mapTaskLead = new Map<Task,Id>();
            for(Task t : Trigger.New){
                if(((String)t.WhoId).startsWith(lead_prefix)){
                    mapTaskLead.put(t,t.WhoId);
                }
            }

             List<Lead> leadList = [Select id, Tele_Appointer__c from Lead where (Tele_Appointer__c = NULL
            OR Tele_Appointer__c = '') AND Id in: mapTaskLead.Values()];

            List<Lead> updateLead = new List<Lead>();

            if(leadList.size() > 0){
                for(Lead l : leadList){
                    l.Tele_Appointer__c = userinfo.getName();
                    updateLead.add(l);
                }
            }

            update updateLead;
        }

        }
    }
    */


}