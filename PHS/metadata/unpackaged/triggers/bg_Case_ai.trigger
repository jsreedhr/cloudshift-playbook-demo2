/*********************************************************************************
 * bg_Case_ai
 *
 * After insert trigger for the Case object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 23-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * Updated: Brightgen - IB 09/03/2018 added statusDiarised method
 *********************************************************************************/

trigger bg_Case_ai on Case(after insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Case_ai_forceAssignEmailToCaseRecords__c)
	{
		bg_Case_Helper.forceAssignmentsForEmailToCaseRecords(Trigger.new);
	}

	if (TS.Id == null || TS.Case_ai_synchronise__c)
	{
		bg_Wildebeest_Helper.synchronise(Trigger.new, Trigger.new);
	}
    
    if(TS.Id == null || TS.Case_ai_Diarised__c)
    {
	bg_Case_Helper.statusDiarised(Trigger.new);
    }
}