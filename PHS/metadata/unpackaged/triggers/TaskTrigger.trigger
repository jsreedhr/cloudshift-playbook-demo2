trigger TaskTrigger on Task (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());

    if (TS.Id == null) {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }

    if (TS.Id == null) {
        TS = Trigger_Settings__c.getOrgDefaults();
    }

    // Before Insert 
       
    if(Trigger.isBefore && Trigger.isInsert) {
            phs_Task_Helper.validateCreatingATask(Trigger.New);
            
        if (TS.Id == null || TS.Task_bi_populateOwner__c)
        {
            bg_ObjectOwner_Helper.populateOwner(bg_Constants.TASK_OBJECTOWNER_NAME, Trigger.new);
        }   
        
        // To  populate Task Parent (Opportunity /Lead) feild values
        TaskTriggerHandler.updateParentFields(trigger.new);
        
        
    }

    // After Insert
    if(Trigger.isAfter && Trigger.isInsert) {
        if (TS.Id == null || TS.Task_ai_synchronise__c)
        {
            bg_Wildebeest_Helper.synchronise(Trigger.new);
        }
    }

    // Before Update 
    if(Trigger.isBefore && Trigger.isUpdate && TaskTriggerHandler.isBeforeUpdate) {
        TaskTriggerHandler.isBeforeUpdate = true;
     phs_Task_Helper.validateCreatingATask(Trigger.New);
     // To  populate Task Parent (Opportunity /Lead) feild values
        TaskTriggerHandler.updateParentFields(trigger.new); 
        TaskTriggerHandler.isBeforeUpdate = false;
    }

    // After Update 
    if(Trigger.isAfter && Trigger.isUpdate && TaskTriggerHandler.isAfterUpdate) {
        TaskTriggerHandler.isAfterUpdate = false;
        if (TS.Id == null || TS.Task_au_synchronise__c)
        {
            bg_Wildebeest_Helper.synchronise(Trigger.new, Trigger.oldMap);
        }
        //Added recirsive check
        if ((TS.Id == null || TS.Task_au_updateAge__c) &   bg_Task_Helper.runOnce())
        {
            bg_Task_Helper.updateAge(Trigger.new, Trigger.oldMap);
        }
        TaskTriggerHandler.isAfterUpdate = true;
    }

    // Before Delete 
    if(Trigger.isBefore && Trigger.isDelete) {

    }

    // After Delete 
    if(Trigger.isAfter && Trigger.isDelete) {
            
    }

    // After Undelete
    if(Trigger.isAfter && Trigger.isUndelete) {
            
    }

}