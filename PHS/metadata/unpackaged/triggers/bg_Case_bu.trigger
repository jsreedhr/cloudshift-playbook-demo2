/**********************************************************************
* bg_Case_bu:
*
* Trigger for the before update actions on the Case object
* Created By: 
* Created Date: 
* Changes: 21-12-2016 KH - Intoducing checkOriginatingEmployee
* Updated: CC 23-01-2017: Added Trigger Settings
* Updated: PHS - AG 08/05/2017: Added validating closing a case call
***********************************************************************/

trigger bg_Case_bu on Case(before update)
{
			bg_Case_Helper.validateClosingACase(Trigger.New,Trigger.OldMap);
			
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Case_bu_populateCasePriority__c)
	{
		bg_Case_Helper.populateCasePriority(Trigger.New);
	}

	if (TS.Id == null || TS.Case_bu_setCaseDefaultEntitlement__c)
	{
		bg_Case_Helper.setCaseDefaultEntitlement(Trigger.New);
	}
	
	if (TS.Id == null || TS.Case_bu_defaultPHSAccount__c)
	{
		bg_Case_Helper.defaultPHSAccount(Trigger.New, Trigger.OldMap);
	}
	
	if (TS.Id == null || TS.Case_bu_checkOriginatingEmployee__c)
	{
		bg_Case_Helper.checkOriginatingEmployee(Trigger.New, Trigger.OldMap);
	}
    
    	if (TS.Id == null || TS.Case_bu_Diarised__c)
	{
		bg_Case_Helper.statusDiarised2(Trigger.New, Trigger.OldMap); 
	}
	

		
}