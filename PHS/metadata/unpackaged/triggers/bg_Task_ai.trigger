/*********************************************************************************
 * bg_Task_ai
 *
 * After insert trigger for the Task object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *********************************************************************************/

trigger bg_Task_ai on Task(after insert)
{
   
}