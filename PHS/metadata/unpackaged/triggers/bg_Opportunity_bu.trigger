/*********************************************************************************
 * bg_Opportunity_bu
 *
 * Before Update trigger for the Opportunity object
 * 
 * Author: Kash Hussain - BrightGen Ltd
 * Created: 22-12-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *  
 * Author : Hormese Ambookken
 * Updated: 31-10-2017: Added Trigger Settings to update Activity
 *********************************************************************************/
trigger bg_Opportunity_bu on Opportunity(before update)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null)
    {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) 
    {
        TS = Trigger_Settings__c.getOrgDefaults();
    }

    if (TS.Id == null || TS.Opportunity_bu_checkOriginatingEmployee__c)
    {
        bg_OpportunityHelper.checkOriginatingEmployee(Trigger.new, Trigger.oldMap);
    }
    
    //Added recirsive check
    if(bg_OpportunityHelper.runOnce())
    {
        if (TS.Id == null || TS.Opportunity_au_populateFLTFields__c) 
        { 
            //bg_OpportunityHelper.populateFLTFields(Trigger.old); 
            bg_OpportunityHelper.populateFLTFields(Trigger.new);
        } 
    }
    if(bg_OpportunityHelper.isBeforeUpdate)
    {
        system.debug('---------inside before update----');
        bg_OpportunityHelper.isBeforeUpdate = false;
        // To update Campaigns field under Opportunity
        OpportunityTriggerHandler.updateDateClosed(Trigger.new,Trigger.oldMap);
        OpportunityTriggerHandler.updateCampaignsField(Trigger.new);
        
        //To update the opportunity default name
        OpportunityTriggerHandler.updateOppoDefaultName(Trigger.New);
        
        bg_OpportunityHelper.isBeforeUpdate = true;
    }
}