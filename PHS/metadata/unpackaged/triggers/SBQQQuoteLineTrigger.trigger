trigger SBQQQuoteLineTrigger on SBQQ__QuoteLine__c (before insert,before update) {
    
    
if(Trigger.isBefore && Trigger.isUpdate )
{
 
 CPQQuoteLineTriggerFunctions.getProductPBEPricing(trigger.new);
 CPQQuoteLineTriggerFunctions.ValidateActualContractLength(trigger.new, Trigger.oldMap);
 CPQQuoteLineTriggerFunctions.ValidateActualServiceFrequency(trigger.new, Trigger.oldMap);
   
}
  if(Trigger.isBefore && Trigger.isInsert )
{

 CPQQuoteLineTriggerFunctions.getProductPBEPricing(trigger.new);   
 CPQQuoteLineTriggerFunctions.ValidateActualContractLength(trigger.new, new Map<Id, SBQQ__QuoteLine__c>());
 CPQQuoteLineTriggerFunctions.ValidateActualServiceFrequency(trigger.new, new Map<Id, SBQQ__QuoteLine__c>());
  
 
}  
    
}