/*********************************************************************************
 * bg_Task_au
 *
 * After update trigger for the Task object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *          TM 10-05-2017: Added updateAge
 *********************************************************************************/
trigger bg_Task_au on Task(after update)
{
}