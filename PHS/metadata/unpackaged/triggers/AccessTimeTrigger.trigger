/**
 *  Trigger Name: AccessTimeTrigger  
 *  Description: This is a Trigger for Access Time.
 *  Company: Standav
 *  CreatedDate:20/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             20/03/2018                 Original version
 */
trigger AccessTimeTrigger on Access_Time__c (after insert, after update, before delete) {
    
    AccessTimeTriggerHandler controller = new AccessTimeTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            controller.sortAccessTimeDelete(Trigger.old);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            controller.sortAccessTime(Trigger.new);
        }
    }
}