/************************************************************************
 * bg_Case_bi
 *
 * Before insert trigger for the Case object
 * 
 * Author: BrightGen Ltd
 * Created: 23-03-2016
 * Updated: 21-12-2016 KH - Adding setOriginatingEmployee
 * Updated: CC 23-01-2017: Added Trigger Settings
 *          KH 14-07-2017: Added assignPHSAccount (Case: 00030968) 
 ***********************************************************************/
trigger bg_Case_bi on Case(before insert)
{   
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null)
    {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) 
    {
        TS = Trigger_Settings__c.getOrgDefaults();
    }

    if (TS.Id == null || TS.Case_bi_setCaseDefaultEntitlement__c)
    {
        bg_Case_Helper.setCaseDefaultEntitlement(Trigger.New);
    }
        if (TS.Id == null || TS.Case_bi_populateCasePriority__c)
    {
        bg_Case_Helper.populateCasePriority(Trigger.New);
    }

    if (TS.Id == null || TS.Case_bi_defaultPHSAccount__c)
    {
        bg_Case_Helper.defaultPHSAccount(Trigger.New);
    }

    if (TS.Id == null || TS.Case_bi_setOriginatingEmployee__c)
    {
        bg_Case_Helper.setOriginatingEmployee(Trigger.new);
    }

    if (TS.Id == null || TS.Case_bi_assignPHSAccount__c)
    {
        bg_Case_Helper.assignPHSAccount(Trigger.new);
    }
    

}