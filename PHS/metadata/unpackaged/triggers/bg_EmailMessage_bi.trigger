/********************************************************************************
* bg_EmailMessage_ai :
*
* After insert Trigger for the Email Message Object
*
* Created By: Daniel Trusler
* Created Date: 18-01-2017
*********************************************************************************/

trigger bg_EmailMessage_bi on EmailMessage (before insert)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

    System.debug(TS);
    System.debug(TS.EmailMessage_bi_preventEmailsFromFLT0__c);
    System.debug(TS.EmailMessage_bi_preventEmailsToSrvcBoxes__c);

	if (TS.Id == null || TS.EmailMessage_bi_preventEmailsFromFLT0__c || TS.EmailMessage_bi_preventEmailsToSrvcBoxes__c)
	{
        List<EmailMessage> outboundEmails = new List<EmailMessage>();
        for (EmailMessage newMessage : trigger.new)
        {
            if (!newMessage.Incoming)
            {
                outboundEmails.add(newMessage);
            }
        }

        if (TS.Id == null || TS.EmailMessage_bi_preventEmailsFromFLT0__c)
        {
            bg_EmailMessage_Helper.addErrorToFLT0Emails(outboundEmails);
        }

        if (TS.Id == null || TS.EmailMessage_bi_preventEmailsToSrvcBoxes__c)
        {
            bg_EmailMessage_Helper.addErrorToServiceBoxDestinationEmails(outboundEmails);
        }
	}

    

}