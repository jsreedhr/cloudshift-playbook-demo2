/*********************************************************************************
 * bg_Lead_au
 *
 * After update trigger for the Lead object
 * 
 * Author: Tom Morris - BrightGen Ltd
 * Created: 24-06-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * 
 * 
 * Author : Hormese Ambookken
 * Updated: 31-10-2017: Added Trigger Settings to update Activity
 *********************************************************************************/

trigger bg_Lead_au on Lead(after update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}
    /*
	if (TS.Id == null || TS.Lead_au_populateFLTFields__c)
	{
		bg_LeadHelper.populateFLTFields(Trigger.new, Trigger.oldMap);
	}
	*/
	if(LeadTriggerFunctions.isAfterUpdate)
	{
	    LeadTriggerFunctions.isAfterUpdate = false;
    	// To update Activity fields under Lead
    	LeadTriggerFunctions.updateActivity(Trigger.new);
    	bg_LeadHelper.mapLeadFieldToOpp(Trigger.New);
    	LeadTriggerFunctions.isAfterUpdate = true;
	}
}