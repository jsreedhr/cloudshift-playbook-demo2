/**********************************************************************
* bg_Agreement_au
*
* Tigger for the After Update of Order
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 02/11/2016
*
* Updated: CC 23-01-2017: Added Trigger Settings
***********************************************************************/

trigger bg_Agreement_au on Order(after update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Agreement_au_updateParentOppCloseDate__c)
	{
		bg_AgreementHelper.updateParentOpportunityCloseDate(Trigger.new, Trigger.oldMap);
	}

}