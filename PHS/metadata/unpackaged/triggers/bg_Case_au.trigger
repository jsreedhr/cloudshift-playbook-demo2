/*********************************************************************************
 * bg_Case_au
 *
 * After update trigger for the Case object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 23-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *********************************************************************************/

trigger bg_Case_au on Case(after update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Case_au_completeMilestones__c)
	{
		bg_Case_Helper.completeMilestones(Trigger.New, Trigger.OldMap);
	}

   if(phs_RecursiveCheck.runOnce())
    {
     system.debug('AG - recursive check - first run');
      if (TS.Id == null || TS.Case_au_synchronise__c)
	 {
	    system.debug('AG - before sync call in after update trigger');
		bg_Wildebeest_Helper.synchronise(Trigger.new, Trigger.oldMap);
		system.debug('AG - after sync call in after update trigger');
	 }   
    }
     else
    {
    system.debug('AG - failed recursive check - not first run');
    }
           
    
}