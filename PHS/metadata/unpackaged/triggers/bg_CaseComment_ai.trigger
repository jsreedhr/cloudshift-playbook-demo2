/*********************************************************************************
 * bg_CaseComment_ai
 *
 * After insert trigger for the CaseComment object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *          KH 08-05-2017: Added completeResponseMilestones
 *********************************************************************************/

trigger bg_CaseComment_ai on CaseComment(after insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.CaseComment_ai_completeResponseMilestone__c)
	{
		bg_CaseCommentHelper.completeResponseMilestones(Trigger.new);
	}

	if (TS.Id == null || TS.CaseComment_ai_synchronise__c)
	{
		bg_Wildebeest_Helper.synchronise(Trigger.newMap);
	}	
}