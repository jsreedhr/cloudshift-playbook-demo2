/*********************************************************************************
 * bg_Event_bi
 *
 * Before insert trigger for the Event object
 * 
 * Author: Alex Leslie - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * Updated: Rizwan 22-11-2017 added logic to populated data from Lead/Opportunity
 *********************************************************************************/

trigger bg_Event_bi on Event(before insert,before update)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null){
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) {
        TS = Trigger_Settings__c.getOrgDefaults();
    }
    if (TS.Id == null || TS.Event_bi_populateOwner__c){
        bg_ObjectOwner_Helper.populateOwner(bg_Constants.EVENT_OBJECTOWNER_NAME, Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isInsert){
        ch_Event_Helper.EventTrigger(Trigger.new);
        ch_Event_Helper.populateLeadOpportunityData(Trigger.new);
    }
    
     if(Trigger.isBefore && Trigger.isUpdate)
    {
         ch_Event_Helper.populateLeadOpportunityData(Trigger.new);
    } 
}