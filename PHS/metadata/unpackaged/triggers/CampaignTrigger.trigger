/********************************************************************** 
* CampaignTrigger: 
* 
* Trigger for Campaign 
* Created By: Hormese Ambookken Created Date:  02/11/2017
***********************************************************************/ 
trigger CampaignTrigger on Campaign (after insert, after update, before delete, after undelete) {
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete){
            CampaignTriggerHandler.updateLeadCampaignsField(Trigger.new);
            CampaignTriggerHandler.updateOpportunityCampaignsField(Trigger.new);
        }
    }
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            CampaignTriggerHandler.updateLeadCampaignsFieldDelete(Trigger.old);
            CampaignTriggerHandler.updateOpportunityCampaignsFieldDelete(Trigger.old); 
        }
    }
}