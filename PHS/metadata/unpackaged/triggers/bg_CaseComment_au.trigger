/*********************************************************************************
 * bg_CaseComment_au
 *
 * After update trigger for the CaseComment object
 * 
 * Author: SA - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 24-01-2017: Added Trigger Settings
 *********************************************************************************/

trigger bg_CaseComment_au on CaseComment(after update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.CaseComment_au_synchronise__c)
	{
		bg_Wildebeest_Helper.synchronise(Trigger.newMap, Trigger.oldMap);
	}
}