trigger phs_CampaignProductTrigger on Campaign_Product__c ( 
    before insert,  
    before update,  
    before delete,  
    after insert,  
    after update,  
    after delete,  
    after undelete) { 
  
         // Before Insert        
    if(Trigger.isBefore && Trigger.isInsert) { 
     
     
    } 
  
    // After Insert 
    if(Trigger.isAfter && Trigger.isInsert) { 
  
        phs_CampaignProductHelper.createLeadProducts(Trigger.new); 
        
    } 
  
    // Before Update  
    if(Trigger.isBefore && Trigger.isUpdate) { 
    
    
    } 
  
    // After Update  
    if(Trigger.isAfter && Trigger.isUpdate) { 
      
     
    } 
  
    // Before Delete  
    if(Trigger.isBefore && Trigger.isDelete) { 
  
  
    } 
  
    // After Delete  
    if(Trigger.isAfter && Trigger.isDelete) { 
      
             
    } 
  
    // After Undelete 
    if(Trigger.isAfter && Trigger.isUndelete) { 
      
             
    } 
    
}