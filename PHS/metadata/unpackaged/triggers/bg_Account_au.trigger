/*********************************************************************************
 * bg_Account_au
 *
 * After update trigger for the Account object
 * 
 * Author: Tom Morris - BrightGen Ltd
 * Created: 24-06-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *********************************************************************************/

trigger bg_Account_au on Account(after update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Account_au_populateFLTFields__c)
	{
		bg_AccountHelper.populateFLTFields(Trigger.new, Trigger.oldMap);
	}
	    
}