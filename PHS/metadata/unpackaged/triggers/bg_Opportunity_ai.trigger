/**********************************************************************
* bg_Opportunity_ai:
*
* Tigger for the After Insert of Opportunity
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 23-01-2017
* Updated: CC 23-01-2017: Added Trigger Settings
*
***********************************************************************/

trigger bg_Opportunity_ai on Opportunity(after insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}
 //Added recirsive check
	if ((TS.Id == null || TS.Opportunity_ai_populateFLTFields__c )  & bg_OpportunityHelper.runOnce())
	{
	  	//bg_OpportunityHelper.populateFLTFields(Trigger.new);
	}
	

}