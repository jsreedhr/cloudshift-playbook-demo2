/**
    * @Name:        CaseTrigger
    * @Description: This Trigger will call framework for all contexts
    *
    * @author:      Jared Watson
    * @version:     1.0
    * Change Log
    *
    * Date          author              Change Description
    * -----------------------------------------------------------------------------------
    * 24/08/2018  	Jared Watson        Created Class
*/

trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    new CaseTriggerHandler().run();

}