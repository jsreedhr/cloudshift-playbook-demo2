/**********************************************************************
* Name:     EventTrigger
* Author:   Danny Ayres 
* Date:     16 November 2017
* ======================================================
* ======================================================
* Purpose:                                                            
* To stamp the Tele-Appointer field on Lead if this is the first Task or Event created and related to the lead.
*                                                            
* ======================================================
* ======================================================
* History:                                                            
* VERSION   DATE                    NOTES 
* 1.0       16 November 2017        Inital Creation      
***********************************************************************/

trigger EventTrigger on Event (before insert) {
    
    ch_Event_Helper.EventTrigger(Trigger.new);
    
}