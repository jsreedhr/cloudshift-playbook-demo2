/***************************************************
* Class Name    : AgreementLocationTrigger
* Date Created  : 13 - Jul - 2018
* Date Modified : 16 - Jul - 2018
* Purpose       : This is the trigger for AgreementLocation  
* *************************************************/
trigger AgreementLocationTrigger on Agreement_Location__c (before insert, after insert, after update, after delete, after undelete) {
    // Before Triggers
    if(Trigger.isBefore){
    	// Insert Triggers
    	if(Trigger.isInsert){
    		AgreementLocationTriggerFunction.createDefaultAccessTime(Trigger.new);
    	}


    }

    // Before Triggers
    if(Trigger.isAfter){
    	// Insert Triggers
    	if(Trigger.isInsert){
    		AgreementLocationTriggerFunction.populateLocationInformation(Trigger.new);
    	
    	}
    	// Update Triggers
    	if(Trigger.isUpdate){
    	
    	}
    	// Delete Triggers
    	if(Trigger.isDelete){
    
    	}
    	// Delete Triggers
    	if(Trigger.isUnDelete){
    	
    	}

    }

    //if(Trigger.isInsert && Trigger.isBefore){
    //	AgreementLocationTriggerFunction.createDefaultAccessTime(Trigger.new);
    //}
    //if(Trigger.isInsert && Trigger.isAfter){
    //	AgreementLocationTriggerFunction.PopulateLocationInformation(Trigger.new);
    //} 
}