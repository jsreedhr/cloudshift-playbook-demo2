/*********************************************************************************
 * bg_Task_bi
 *
 * before insert trigger for the Task object
 *
 * Author: Alex Leslie - BrightGen Ltd
 * Created: 24-03-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * Updated: Danny Ayres 20-11-2017 To stamp the Tele-Appointer field on Lead,
            if this is the first Task or Event created and related to the lead.
 * Updated Rizwan 22-11-2017 added logic to poppulated data from Lead/Opportunity
 ********************************************************************************/

trigger bg_Task_bi on Task(before insert)
{
    if(Trigger.isBefore && Trigger.isInsert){
        bg_Task_Helper.teleappointerTask(Trigger.new);
        bg_Task_Helper.populateLeadOpportunityData(Trigger.new);
    }
}