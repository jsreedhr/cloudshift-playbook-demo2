/**********************************************************************
* Name:     Note
* Author:   Danny Ayres 
* Date:     17 November 2017
* ======================================================
* ======================================================
* Purpose:                                                            
* Update the Last Note Creation Date field when a new note is added.
*                                                            
* ======================================================
* ======================================================
* History:                                                            
* VERSION   DATE                    NOTES 
* 1.0       17 November 2017        Inital Creation      
***********************************************************************/

trigger ContentDocumentLink on ContentDocumentLink (after insert) {

    ContentNoteTriggerFunctions.StampContentNoteDate(Trigger.new);
            
}