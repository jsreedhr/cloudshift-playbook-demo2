/*********************************************************************************
 * Event_bu
 *
 * Before update trigger for the Event object
 * 
 * 
 * Created: 29-11-2017
 * 
 *********************************************************************************/
trigger Event_bu on Event (before update) {
if(Trigger.isBefore && Trigger.isUpdate && ch_Event_Helper.isBeforeUpdate){
        ch_Event_Helper.isBeforeUpdate = false;
       ch_Event_Helper.populateLeadOpportunityData(Trigger.new);
       ch_Event_Helper.isBeforeUpdate = true;
    }
}