/*********************************************************************************
 * bg_ExternalObjectPush
 *
 * Trigger which calls the helper class to push fields into the Lead and Case objects
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 09-03-2016
 *
 *********************************************************************************/

trigger bg_ExternalObjectPush_ai on External__c(after insert)
{
	bg_ExternalHelper.pushInfo(Trigger.new, Trigger.oldMap);
}