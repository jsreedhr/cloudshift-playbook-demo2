/*********************************************************************************
 * bg_Lead_bu
 *
 * Before Update trigger for the Lead object
 * 
 * Author: Kash Hussain - BrightGen Ltd
 * Created: 21-12-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * 
 * 
 * Author : Hormese Ambookken
 * Updated: 31-10-2017: Added Trigger Settings to update Campaigns Field
 *********************************************************************************/

trigger bg_Lead_bu on Lead(before update)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Lead_bu_checkOriginatingEmployee__c)
	{
		bg_LeadHelper.checkOriginatingEmployee(Trigger.new, Trigger.oldMap);
	}
    
   
	if(bg_LeadHelper.isBeforeUpdate)
	{
	    bg_LeadHelper.isBeforeUpdate = false;
        bg_LeadHelper.SetNonCCA(Trigger.new);
        LeadTriggerFunctions.getaccountdataupdate(Trigger.old, Trigger.new);
        LeadTriggerFunctions.ChangeLeadProductsOwner(Trigger.new, Trigger.old);
    
        // To update Campaigns fields under Lead
        LeadTriggerFunctions.updateCampaignsField(Trigger.new);
        //To update activity fileds
        	LeadTriggerFunctions.updateActivity(Trigger.new);
        
        bg_LeadHelper.fieldUpdatesInsertOrUpdate(trigger.New, Trigger.oldMap);
	
        bg_LeadHelper.isBeforeUpdate = true;
        
         if (TS.Id == null || TS.Lead_au_populateFLTFields__c)
	{
		bg_LeadHelper.populateFLTFields(Trigger.new, Trigger.oldMap);
	}
	
	}
    
    
    
	
}