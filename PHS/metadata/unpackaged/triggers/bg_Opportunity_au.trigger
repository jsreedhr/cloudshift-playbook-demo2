/********************************************************************** 
* bg_Opportunity_au: 
* 
* Tigger for the After Update of Opportunity 
* Created By: Tim Hutchings - PHS Group* Created Date:  
* Updated: CC 23-01-2017: Added Trigger Settings  
* 
* Author : Hormese Ambookken
* Updated: 31-10-2017: Added Trigger Settings to update Activity
***********************************************************************/ 
trigger bg_Opportunity_au on Opportunity(after update) 
{ 
  
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId()); 
    if (TS.Id == null) 
    { 
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId()); 
    } 
    if (TS.Id == null)  
    { 
        TS = Trigger_Settings__c.getOrgDefaults(); 
    } 
    
   //Added recirsive check
     // To update Activity fields under Opportunity
     if(bg_OpportunityHelper.runOnce())
          {
        if (TS.Id == null || TS.Opportunity_au_populateFLTFields__c) 
        { 
        // bg_OpportunityHelper.populateFLTFields(Trigger.old); 
        } 
         OpportunityTriggerHandler.updateActivity(Trigger.new);
         system.debug('---------inside after update----');
     }
     
}