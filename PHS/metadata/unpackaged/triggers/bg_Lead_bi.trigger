/*********************************************************************************
 * bg_Lead_bi
 *
 * Before insert trigger for the Lead object
 * 
 * Author: Alex Leslie - BrightGen Ltd
 * Created: 23-03-2016
 * Updated: 21-12-2003 KH - Adding setOriginatingEmployee
 * Updated: CC 23-01-2017: Added Trigger Settings
 * 
 * 
 * Author : Hormese Ambookken
 * Updated: 31-10-2017: Added Trigger Settings to update Campaigns Field
 *********************************************************************************/

trigger bg_Lead_bi on Lead(before insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.Lead_bi_populateOwner__c)
	{
		bg_ObjectOwner_Helper.populateOwner(bg_Constants.LEAD_OBJECTOWNER_NAME, Trigger.new);
	}
	
	if (TS.Id == null || TS.Lead_bi_setOriginatingEmployee__c)
	{
		bg_LeadHelper.setOriginatingEmployee(Trigger.new);
	}
   
    if(bg_LeadHelper.isBeforeInsert)
    {
        bg_LeadHelper.isBeforeInsert = false;
        //Get account data
        LeadTriggerFunctions.getaccountdatainsert(Trigger.new);
        bg_LeadHelper.isBeforeInsert = true;
    }
     if (TS.Id == null || TS.Lead_ai_populateFLTFields__c)
	{
		bg_LeadHelper.populateFLTFields(Trigger.new);
	}
	
    
    bg_LeadHelper.fieldUpdatesInsertOrUpdate(trigger.New, new Map<Id, Lead>());
    bg_LeadHelper.fieldUpdatesInsert(trigger.New);
    
    //Populate the default pricebook on to the lead
    bg_LeadHelper.SetPriceBook(Trigger.new);
    
    //Populate CCA on to the lead
    bg_LeadHelper.SetNonCCA(Trigger.new);
	
	// To update Campaigns fields under Lead
	LeadTriggerFunctions.updateCampaignsField(Trigger.new);
}