/**********************************************************************
* bg_DocusignStatus_ai
*
* Tigger for the After Insert of DocusignStatus
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 02/11/2016
*
* Updated: CC 23-01-2017: Added Trigger Settings
***********************************************************************/

trigger bg_DocusignStatus_ai on dsfs__DocuSign_Status__c(after insert)
{
	Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
	if (TS.Id == null)
	{
		TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
	}
	if (TS.Id == null) 
	{
		TS = Trigger_Settings__c.getOrgDefaults();
	}

	if (TS.Id == null || TS.DocusignStatus_ai_setContractSentDate__c)
	{
		bg_DocuSignHelper.setContractSentDateOnAgreement(Trigger.new);
	}
}