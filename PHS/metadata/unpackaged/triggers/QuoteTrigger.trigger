trigger QuoteTrigger on Quote (after insert,before insert) {

    if(Trigger.isBefore && Trigger.isInsert)
    {
       QuoteTriggerHandler.UpdatePriceBook(trigger.new);
    }
    
    
    if(Trigger.isAfter && Trigger.isInsert)
    {
       QuoteTriggerHandler.UpdateQuoteLineItems(trigger.new);
    }
    
}