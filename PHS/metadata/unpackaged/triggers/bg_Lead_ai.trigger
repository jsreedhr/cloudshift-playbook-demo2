/*********************************************************************************
 * bg_Lead_ai
 *
 * After insert trigger for the Lead object
 * 
 * Author: Tom Morris - BrightGen Ltd
 * Created: 24-06-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 * 
 * Author : Hormese Ambookken 
 * Updated: 31-10-2017: Added Trigger Settings to update Activity
 *********************************************************************************/

trigger bg_Lead_ai on Lead(after insert)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null)
    {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) 
    {
        TS = Trigger_Settings__c.getOrgDefaults();
    }
    
    if (TS.Id == null || TS.Lead_ai_populateFLTFields__c)
    {
        //bg_LeadHelper.populateFLTFields(Trigger.new);
    }
    
    
    // To update Activity fields under Lead
    LeadTriggerFunctions.updateActivity(Trigger.new);
    
    LeadTriggerFunctions.getaccountplanlinksinsert(Trigger.new);
}