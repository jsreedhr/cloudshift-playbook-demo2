/********************************************************************** 
* CampaignMemberTrigger: 
* 
* Trigger for CampaignMember 
* Created By: Hormese Ambookken
* Updated: 02-11-2017: Added Trigger Settings to update Campaigns field in Lead
***********************************************************************/
trigger CampaignMemberTrigger on CampaignMember (after insert, after update, after delete, after undelete) {
    if(Trigger.isInsert){
        CampaignMemberTriggerFunctions.getcampaignproducts(Trigger.new);
        CampaignMemberTriggerFunctions.updateLeadCampaignField(Trigger.new);
    }
    if(Trigger.isUndelete){
        CampaignMemberTriggerFunctions.updateLeadCampaignField(Trigger.new);
    }
    if(Trigger.isDelete){
        CampaignMemberTriggerFunctions.updateLeadCampaignField(Trigger.old);
    }
}