/*********************************************************************************
 * bg_LeadConvertCompetitors_ai
 *
 * After insert/ after update trigger for the Lead object
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 11-01-2016
 * Updated: CC 23-01-2017: Added Trigger Settings
 *********************************************************************************/

trigger bg_LeadConvertCompetitors_ai on Lead (after insert, after update)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null)
    {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) 
    {
        TS = Trigger_Settings__c.getOrgDefaults();
    }

    if (TS.Id == null || TS.LeadConvertCompetitors_ai_convertLead__c)
    {
        if(phs_RecursiveCheck.runOnce())
        bg_LeadHelper.convertLead(Trigger.new, Trigger.oldMap);
    }
}