/********************************************************************************
* bg_EmailMessage_ai :
*
* After insert Trigger for the Email Message Object
*
* Created By: Tom Morris
* Created Date: 17-01-2017 
*
* Updated: CC 24-01-2017: Added Trigger Settings
*          KH 08-05-2017: Added completeResponseMilestones
* Updated: Brightgen - IB 09/03/2018 added secondEmailMessage method
*********************************************************************************/

trigger bg_EmailMessage_ai on EmailMessage(after insert)
{
    Trigger_Settings__c TS = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    if (TS.Id == null)
    {
        TS = Trigger_Settings__c.getInstance(Userinfo.getProfileId());
    }
    if (TS.Id == null) 
    {
        TS = Trigger_Settings__c.getOrgDefaults();
    }

    if (TS.Id == null || TS.EmailMessage_ai_updateCaseFlag__c)
    {
        bg_EmailMessage_Helper.updateCaseFlag(Trigger.new);
    }   

    if (TS.Id == null || TS.EmailMessage_ai_CompleteMilestones__c)
    {
        bg_EmailMessage_Helper.completeResponseMilestones(Trigger.new);
    }  

}