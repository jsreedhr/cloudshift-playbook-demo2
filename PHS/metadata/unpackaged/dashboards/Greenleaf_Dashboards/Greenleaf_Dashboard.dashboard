<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for All GL Sales</footer>
            <gaugeMax>553123.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>All Sales Revenue - this FP</header>
            <indicatorBreakpoint1>452555.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>502839.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_All2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for GL Key Accounts team</footer>
            <gaugeMax>92696.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Key Accounts Revenue - this FP</header>
            <indicatorBreakpoint1>75842.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>84269.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_Key_Accounts2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for All GL Sales</footer>
            <gaugeMax>4143579.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>All Sales Revenue - this FY</header>
            <indicatorBreakpoint1>3390201.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>3766890.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_All2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for GL Key Accounts team</footer>
            <gaugeMax>1033450.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Key Accounts Revenue - this FY</header>
            <indicatorBreakpoint1>845550.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>939500.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_Key_Accounts2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Closed Opportunities Grouped by Lead Source, with Close Date in Current or Last FP</footer>
            <header>Closed Opportunities by Lead Source</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Closed_Opps_by_Lead_Source</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>% of Opportunities Won &amp; Lost, with Close Date in Current or Previous FQ</footer>
            <header>Opportunities Won Rate</header>
            <legendPosition>Right</legendPosition>
            <report>Greenleaf_Reports/GL_Quotes_Conversion_Rate</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Average Days from Open to Closed Opportunity by Owner, with Close Date in Current or Previous FQ</footer>
            <header>Average Days to Close Opportunity</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Time_to_Close_Opportunities</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>TOTAL_PRICE</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for GL Field Sales team</footer>
            <gaugeMax>37950.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Field Sales Revenue - this FP</header>
            <indicatorBreakpoint1>31050.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>34500.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_Field_Sales2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for GL Telesales team</footer>
            <gaugeMax>63250.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Telesales Revenue - this FP</header>
            <indicatorBreakpoint1>51750.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>57500.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_Telesales2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for GL Field Sales team</footer>
            <gaugeMax>411235.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Field Sales Revenue - this FY</header>
            <indicatorBreakpoint1>336465.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>373850.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_Field_Sales2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for GL Telesales team</footer>
            <gaugeMax>690250.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Telesales Revenue - this FY</header>
            <indicatorBreakpoint1>564750.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>627500.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_Telesales2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Opportunities Value grouped by Stage, with a Close Date in Current or Next FQ</footer>
            <header>Pipeline by Stage</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Pipeline_by_Stage</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>true</expandOthers>
            <footer>Lost Opportunities by Reason, with Close Date in Current or Previous FQ</footer>
            <header>Opportunities Lost by Reason</header>
            <legendPosition>Right</legendPosition>
            <report>Greenleaf_Reports/GL_Opportunities_Lost_by_Reason</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>AGE</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Average Age of Open Opportunities by Owner, with Close Date in Current or Next FQ</footer>
            <groupingColumn>FULL_NAME</groupingColumn>
            <header>Age of Opportunities closing in Current or Next FQ</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Open_Opportunities</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for GL House Sales team</footer>
            <gaugeMax>3.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>House Sales Revenue - this FP</header>
            <indicatorBreakpoint1>1.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>2.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_House2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Period Revenue for GL Xmas Sales</footer>
            <gaugeMax>359776.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Xmas Revenue - this FP</header>
            <indicatorBreakpoint1>294362.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>327069.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FP_Xmas2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for GL House Sales team</footer>
            <gaugeMax>332396.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>House Sales Revenue - this FY</header>
            <indicatorBreakpoint1>271960.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>302178.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_House2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Integer</displayUnits>
            <footer>Current Year to Date Revenue for GL Xmas Sales</footer>
            <gaugeMax>1676247.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Xmas Revenue - this FY</header>
            <indicatorBreakpoint1>1371475.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>1523861.0</indicatorBreakpoint2>
            <indicatorHighColor>#5454C2</indicatorHighColor>
            <indicatorLowColor>#548BC2</indicatorLowColor>
            <indicatorMiddleColor>#54799C</indicatorMiddleColor>
            <report>Greenleaf_Reports/GL_Revenue_for_this_FY_Xmas2</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Opportunities Value grouped by Owner, with a Close Date in Current or Next FQ</footer>
            <header>Pipeline by Owner</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Pipeline_by_Owner</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Count of Opened Leads by Owner &amp; Status, created in Current or Previous FP</footer>
            <header>Opened Leads in Current or Last FP</header>
            <legendPosition>Bottom</legendPosition>
            <report>Greenleaf_Reports/GL_Open_Leads</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>true</useReportChart>
        </components>
    </rightSection>
    <runningUser>07223@phs.co.uk</runningUser>
    <textColor>#000000</textColor>
    <title>Greenleaf Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
