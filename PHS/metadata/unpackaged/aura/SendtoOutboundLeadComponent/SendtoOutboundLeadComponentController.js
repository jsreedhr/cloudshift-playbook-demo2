({
    doInit:function(component,event,helper){
        //get the record Id
        var recId=component.get("v.recordId");
        // Set default value to show message
        component.set("v.displaySuccessMessage", false);
        console.log('recordId : ' + recId);
   
        //method re_allocate_Lead from LeadReallocationController
        var action=component.get("c.reallocateOutboundLead");
        action.setParams({
            "leadIdAura":component.get("v.recordId")
        });
        //Register the callback function
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
               	$A.get("e.force:refreshView").fire();
                component.set("v.displaySuccessMessage", true);
                component.set("v.message", response.getReturnValue());
            }
        });
        // Action Call
        $A.enqueueAction(action); 
    }
})