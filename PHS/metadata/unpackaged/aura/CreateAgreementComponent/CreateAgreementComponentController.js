({
    doInit : function(component, event, helper) {
        try{
            //Id of the current quote Record
            var recId = component.get("v.recordId");
            var action = component.get("c.getLoggedInProfile");
            var userStatus = component.get("c.getUserStatus");
            var oppAction = component.get("c.getOpp");
            oppAction.setParams({
                "oppId":component.get("v.recordId")
            });
            var argeementAction = component.get("c.hasAgreements");
            argeementAction.setParams({
                "oppId":component.get("v.recordId")
            });
            var createAgrrAction = component.get("c.createAgreement");
            createAgrrAction.setParams({
                "oppId":component.get("v.recordId")
            }); 
            var profile;
            var userActive;
            var agreementId;
            var quoteRollUp ;
            var hasSyncedQuote ;
            var productCount ;
            var hasOpenAgreements ;
            var isQuickContract ;
            
            // Register the callback function
            
            userStatus.setCallback(this, function(response) {
                var uStatus = response.getState();
                var data = response.getReturnValue();
                if(uStatus){
                    if(data !=null && data !='undefined' && data!=''){
                        userActive = data.ActiveInPilot__c;
                    }
                }
                // Register the callback function
                action.setCallback(this, function(response) {
                    var status = response.getState();
                    var data2 = response.getReturnValue();
                    if(data2 !=null && data2 !='undefined' && data2!=''){
                        profile = data2.Name;
                    }
                    oppAction.setCallback(this, function(response) {
                        var orderState = response.getState();
                        var opp = response.getReturnValue();
                        if(orderState==="SUCCESS") {
                            if(opp!=null && opp!='undefined' && opp!='')
                            {
                                quoteRollUp = opp.Quote_Roll_Up__c;
                                hasSyncedQuote = opp.Has_Synced_Quote__c;
                                productCount = opp.Product_Count__c;
                                isQuickContract = opp.Is_Quick_Contract__c;
                            }
                        }
                        
                        argeementAction.setCallback(this, function(response) {
                            var docState = response.getState();
                            var data1 = response.getReturnValue();
                            if(docState==="SUCCESS") {
                                hasOpenAgreements = response.getReturnValue();
                                // alert(hasOpenAgreements);
                                if(hasOpenAgreements)
                                    hasOpenAgreements =hasOpenAgreements;
                                else
                                    hasOpenAgreements=false;
                                
                            }
                           
                                
                                console.log(quoteRollUp+':quoteRollUp,'+hasSyncedQuote+':hasSyncedQuote,'+productCount+':productCount,'+hasOpenAgreements+':hasOpenAgreements,'+isQuickContract+':isQuickContract,');
                                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                if( (quoteRollUp > 0 && hasSyncedQuote == 1 && productCount > 0 && hasOpenAgreements ==0) || (isQuickContract == 1 && productCount > 0 && hasOpenAgreements==0) )
                                {
                                    
                                    var result = confirm("By pressing OK you will create a new Agreement");
                                    
                                    if(result == true)
                                    {
                                        createAgrrAction.setCallback(this, function(response) {
                                            var docState = response.getState();
                                            var dt = response.getReturnValue();
                                            if(docState==="SUCCESS") {
                                                if(dt!=null && dt!='undefined' && dt!='')
                                                {
                                                    agreementId = dt;
                                                 /*  var navEvt = $A.get("e.force:navigateToURL");
                                                    navEvt.setParams({
                                                        "url": '/'+agreementId,
                                                        "isredirect":false
                                                    });
                                                    navEvt.fire(); */
                                                    window.location.href ='/'+agreementId;
                                                    
                                                    dismissActionPanel.fire();
                                                    
                                                }
                                            }
                                        });
                                        $A.enqueueAction(createAgrrAction);  
                                    }
                                    
                                    else{
                                        dismissActionPanel.fire();
                                    }
                                    
                                    
                                }
                                else if(hasOpenAgreements == 1)
                                {
                                    dismissActionPanel.fire();
                                    
                                    alert("An Opportunity can only have one related open Agreement, if you wish to create a new Agreement close the current related Agreements");
                                    
                                }
                                    else if( quoteRollUp > 0 && hasSyncedQuote == 0)
                                    {
                                        dismissActionPanel.fire();
                                        alert("You must sync a quote if you wish to create a Contract");
                                        
                                    }
                                        else if( quoteRollUp > 0 && hasSyncedQuote == 1 && productCount == 0 )
                                        {
                                            dismissActionPanel.fire();
                                            alert("You must assign products to the opportunity if you wish to create a contract");
                                            
                                        }
                                
                                            else if( quoteRollUp == 0 && isQuickContract == 0)
                                            {
                                                dismissActionPanel.fire();
                                                alert("You must set the Quick Contract field if you are skipping the quotation stage");
                                                
                                            }
                                                else if( isQuickContract == 1 && productCount == 0 )
                                                {
                                                    dismissActionPanel.fire();
                                                    alert("You must assign products to the opportunity if you wish to create a quick contract");
                                                    
                                                }
                                                              
                                                            
                          
                            
                        });
                        $A.enqueueAction(argeementAction);
                    });
                    $A.enqueueAction(oppAction);
                });
                $A.enqueueAction(action);
            });
            $A.enqueueAction(userStatus);
        }
        catch(e){
            throw new Error("Unexpected error occured! Try refreshing the page. If it continues to happen please contact your System Admin");
            console.error(e); 
        }
    }
})