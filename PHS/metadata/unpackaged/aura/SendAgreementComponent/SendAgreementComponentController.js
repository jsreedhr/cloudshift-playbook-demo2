({
    doInit : function(component, event, helper) {
        try{
            //Id of the current quote Record
            var recId = component.get("v.recordId");
            var action = component.get("c.getLoggedInProfile");
            var userStatus = component.get("c.getUserStatus");
            var orderAction = component.get("c.getOrder");
            orderAction.setParams({
                "orderId":component.get("v.recordId")
            });
            var InvokeDocuSignAction = component.get("c.callInvokeDocuSign");
            InvokeDocuSignAction.setParams({
                "agreementId":component.get("v.recordId")
            });
            var ApplyAction = component.get("c.ApplyTemplate");
            ApplyAction.setParams({
                "agreementId":component.get("v.recordId")
            });
            
            var profile;
            var userActive;
            var agreementId;
            var contractApproved;
            var contractGenerated;
            var customerSignatory;
            var signerFirstName 
            var signerLastName 
            var signerEmail 
            var senderFirstName ;
            var senderLastName;
            var senderEmail;
            var signatureAuthorised;
            var signatureApprovalReq ;
            
            // Register the callback function
            
            userStatus.setCallback(this, function(response) {
                var uStatus = response.getState();
                var data = response.getReturnValue();
                if(uStatus){
                    if(data !=null && data !='undefined' && data!=''){
                        userActive = data.ActiveInPilot__c;
                    }
                }
                // Register the callback function
                action.setCallback(this, function(response) {
                    var status = response.getState();
                    var data2 = response.getReturnValue();
                    if(data2 !=null && data2 !='undefined' && data2!=''){
                        profile = data2.Name;
                    }
                    orderAction.setCallback(this, function(response) {
                        var orderState = response.getState();
                        var Order = response.getReturnValue();
                        if(orderState==="SUCCESS") {
                            if(Order!=null && Order!='undefined' && Order!='')
                            {
                                agreementId = Order.Id;
                                contractApproved = Order.Agreement_Approved__c;
                                contractGenerated = Order.Agreement_Generated__c;
                                customerSignatory = Order.Customer_Signatory__c;
                                signerFirstName = Order.Signatory_First_Name__c;
                                signerLastName = Order.Signatory_Last_Name__c;
                                signerEmail = Order.Signatory_Email__c;
                                senderFirstName = Order.Owner.FirstName;
                                senderLastName = Order.Owner.LastName;
                                senderEmail = Order.Owner.Email;
                                signatureAuthorised = Order.Signature_Authorised__c;
                                signatureApprovalReq = Order.Signature_Approval_Required__c;
                            }
                        }
                        
                        InvokeDocuSignAction.setCallback(this, function(response) {
                            var docState = response.getState();
                            var strUrl = response.getReturnValue();
                            var dismissActionPanel = $A.get("e.force:closeQuickAction");
                            
                            if(profile=='System Administrator' || activeInPilot == 1)
                            {
                                
                                if (contractApproved == 1 && customerSignatory != '' && signatureAuthorised == 0 && signatureApprovalReq == 0)
                                {
                                    var result = confirm("By pressing OK you will send the Agreement");
                                    if(result == true)
                                    {
                                        if(docState==="SUCCESS")
                                        {
                                                                                      
                                            dismissActionPanel.fire();
                                              console.log(strUrl);
                                            var urlEvent = $A.get("e.force:navigateToURL");
                                            urlEvent.setParams({
                                                "url":strUrl,
                                                "isredirect": "true"
                                            });
                                            urlEvent.fire();
                                                                                      
                                            ApplyAction.setCallback(this, function(response) {
                            				var applState = response.getState();
                                               });
                                         $A.enqueueAction(ApplyAction);
                                        }
                                    }
                                }
                                else
                                {
                                    dismissActionPanel.fire();
                                    alert("DocuSign can only be used for approved Agreements. Make sure an Agreement has been generated and the Customer Signatory field has been populated");
                                } 
                            }
                            
                            else
                            {
                                dismissActionPanel.fire();
                                alert("You must be a System Administrator or Active in the Pilot to use this button");
                            }
                               
                        });
                        $A.enqueueAction(InvokeDocuSignAction);
                    });
                    $A.enqueueAction(orderAction);
                });
                $A.enqueueAction(action);
            });
            $A.enqueueAction(userStatus);
        }catch(e){
            throw new Error("Unexpected error occured! Try refreshing the page. If it continues to happen please contact your System Admin");
            console.error(e); 
        }
    }
})