({
    doInit : function(component, event, helper) {
        
        
        var currentUrl = window.location.href;
        var data1 ;
        console.log('check1',currentUrl);
        var todays = new Date();
        var userName;
        var conId;
        todays.setDate(todays.getDate() + 30);
        
        var oppURLAction = component.get("c.getOppID");
        var oppAction = component.get("c.getOpp");
        var accAction = component.get("c.getAcc");
        var userAction = component.get("c.getUser");
        var conAction = component.get("c.getContact");
        
        oppURLAction.setParams({
            "url":currentUrl
        });
        
        
        oppURLAction.setCallback(this, function(response) {
            var uStatus1 = response.getState();
            data1 = response.getReturnValue();
            
            oppAction.setParams({
                "oppId":data1
            });
            accAction.setParams({
                "oppId":data1
            });
            conAction.setParams({
                "oppId":data1
            });          
            
            accAction.setCallback(this, function(response) {
                var uStatus3 = response.getState();
                var accdata = response.getReturnValue();
                
                userAction.setCallback(this, function(response) {
                    userName = response.getReturnValue();
                   // conAction.setCallback(this, function(response) {
                     //   conId = response.getReturnValue();
                        
                        oppAction.setCallback(this, function(response) {
                            var uStatus = response.getState();
                            var data = response.getReturnValue();
                            
                            if(uStatus){
                                if(data !=null && data !='undefined' && data!=''){
                                   if(accdata.BillingCity==null || accdata.BillingStreet ==null ||accdata.BillingPostalCode ==null)
                                    { 
                                        $A.get("e.force:closeQuickAction").fire();
                                        alert('Please ensure billing information - Street, City, PostalCode have been recorded on the account.');}
                                    else{ 
                                    var createEvent = $A.get("e.force:createRecord");
                                    createEvent.setParams({
                                        "entityApiName": "SBQQ__Quote__c",
                                        "defaultFieldValues": {
                                            'SBQQ__Opportunity2__c' : data.Id,
                                        'SBQQ__ExpirationDate__c':data.CloseDate,
                                            // 'SBQQ__ExpirationDate__c':todays,
                                            'SBQQ__Account__c': accdata.Id,
                                            'SBQQ__Status__c' :'Draft',
                                            'SBQQ__Primary__c' : true,
                                            'SBQQ_IsSyncing__c': true,
                                            'SBQQ__SalesRep__c' :userName,                         
                                            'SBQQ__BillingCity__c' :accdata.BillingCity,
                                            'SBQQ__BillingStreet__c' :accdata.BillingStreet,
                                            'SBQQ__BillingPostalCode__c':accdata.BillingPostalCode,
                                            'SBQQ__BillingState__c':accdata.BillingState,
                                            'SBQQ__PricebookId__c':data.Pricebook2Id
                                           // 'SBQQ__PriceBook__c':data.Pricebook2.name 
                                            //'SBQQ__PrimaryContact__c':conId
                                        }
                                    });
                                    createEvent.fire();
                                } 
                                }
                            }
                        });
                        $A.enqueueAction(oppAction);
                        
                    });
                    $A.enqueueAction(userAction);
                    
                //});
                //$A.enqueueAction(conAction);
            });
            $A.enqueueAction(accAction);
            
        });
        $A.enqueueAction(oppURLAction);
        
    }
})