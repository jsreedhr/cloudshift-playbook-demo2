<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Case_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Externals</relationshipLabel>
        <relationshipName>Externals</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Case_Description__c</fullName>
        <externalId>false</externalId>
        <label>Case Description</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Case_Origin__c</fullName>
        <externalId>false</externalId>
        <label>Case Origin</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Driver</fullName>
                    <default>false</default>
                    <label>Driver</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case_Priority__c</fullName>
        <externalId>false</externalId>
        <label>Case Priority</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>High</fullName>
                    <default>false</default>
                    <label>High</label>
                </value>
                <value>
                    <fullName>Medium</fullName>
                    <default>false</default>
                    <label>Medium</label>
                </value>
                <value>
                    <fullName>Low</fullName>
                    <default>false</default>
                    <label>Low</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case_Reason__c</fullName>
        <externalId>false</externalId>
        <label>Case Reason</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>User didn&apos;t attend training</fullName>
                    <default>false</default>
                    <label>User didn&apos;t attend training</label>
                </value>
                <value>
                    <fullName>Complex functionality</fullName>
                    <default>false</default>
                    <label>Complex functionality</label>
                </value>
                <value>
                    <fullName>Existing problem</fullName>
                    <default>false</default>
                    <label>Existing problem</label>
                </value>
                <value>
                    <fullName>Instructions not clear</fullName>
                    <default>false</default>
                    <label>Instructions not clear</label>
                </value>
                <value>
                    <fullName>New problem</fullName>
                    <default>false</default>
                    <label>New problem</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case_Status__c</fullName>
        <externalId>false</externalId>
        <label>Case Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>New</fullName>
                    <default>false</default>
                    <label>New</label>
                </value>
                <value>
                    <fullName>Escalated</fullName>
                    <default>false</default>
                    <label>Escalated</label>
                </value>
                <value>
                    <fullName>On Hold</fullName>
                    <default>false</default>
                    <label>On Hold</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Case_Subject__c</fullName>
        <externalId>false</externalId>
        <label>Case Subject</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Type__c</fullName>
        <externalId>false</externalId>
        <label>Case Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Problem</fullName>
                    <default>false</default>
                    <label>Problem</label>
                </value>
                <value>
                    <fullName>Feature Request</fullName>
                    <default>false</default>
                    <label>Feature Request</label>
                </value>
                <value>
                    <fullName>Question</fullName>
                    <default>false</default>
                    <label>Question</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Lead_Company__c</fullName>
        <externalId>false</externalId>
        <label>Lead Company</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_Email__c</fullName>
        <externalId>false</externalId>
        <label>Lead Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_First_Name__c</fullName>
        <externalId>false</externalId>
        <label>Lead First Name</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_Last_Name__c</fullName>
        <externalId>false</externalId>
        <label>Lead Last Name</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lead_Phone__c</fullName>
        <externalId>false</externalId>
        <label>Lead Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Lead_Photo_URL__c</fullName>
        <externalId>false</externalId>
        <label>Lead Photo URL</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Lead_Products_of_Interest__c</fullName>
        <externalId>false</externalId>
        <label>Lead Products of Interest</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>WMO - Sharps Disposal</fullName>
                    <default>false</default>
                    <label>WMO - Sharps Disposal</label>
                </value>
                <value>
                    <fullName>WMO - Nappy Disposal</fullName>
                    <default>false</default>
                    <label>WMO - Nappy Disposal</label>
                </value>
                <value>
                    <fullName>WMO - Medical Disposal</fullName>
                    <default>false</default>
                    <label>WMO - Medical Disposal</label>
                </value>
                <value>
                    <fullName>Water Products</fullName>
                    <default>false</default>
                    <label>Water Products</label>
                </value>
                <value>
                    <fullName>Vends</fullName>
                    <default>false</default>
                    <label>Vends</label>
                </value>
                <value>
                    <fullName>Treadsmart Rolller Towels</fullName>
                    <default>false</default>
                    <label>Treadsmart Rolller Towels</label>
                </value>
                <value>
                    <fullName>Air freshners / cleaners</fullName>
                    <default>false</default>
                    <label>Air freshners / cleaners</label>
                </value>
                <value>
                    <fullName>Baby Changing</fullName>
                    <default>false</default>
                    <label>Baby Changing</label>
                </value>
                <value>
                    <fullName>Disposals</fullName>
                    <default>false</default>
                    <label>Disposals</label>
                </value>
                <value>
                    <fullName>First Aid</fullName>
                    <default>false</default>
                    <label>First Aid</label>
                </value>
                <value>
                    <fullName>Fitted Mats</fullName>
                    <default>false</default>
                    <label>Fitted Mats</label>
                </value>
                <value>
                    <fullName>Hand Dryers</fullName>
                    <default>false</default>
                    <label>Hand Dryers</label>
                </value>
                <value>
                    <fullName>Loose Lay Mats</fullName>
                    <default>false</default>
                    <label>Loose Lay Mats</label>
                </value>
                <value>
                    <fullName>Medical Disposal</fullName>
                    <default>false</default>
                    <label>Medical Disposal</label>
                </value>
                <value>
                    <fullName>Mopping System</fullName>
                    <default>false</default>
                    <label>Mopping System</label>
                </value>
                <value>
                    <fullName>Nappy Disposal</fullName>
                    <default>false</default>
                    <label>Nappy Disposal</label>
                </value>
                <value>
                    <fullName>On-Site floor cleaning</fullName>
                    <default>false</default>
                    <label>On-Site floor cleaning</label>
                </value>
                <value>
                    <fullName>PHS Washrooms Contract</fullName>
                    <default>false</default>
                    <label>PHS Washrooms Contract</label>
                </value>
                <value>
                    <fullName>Roller Towels</fullName>
                    <default>false</default>
                    <label>Roller Towels</label>
                </value>
                <value>
                    <fullName>Sales</fullName>
                    <default>false</default>
                    <label>Sales</label>
                </value>
                <value>
                    <fullName>Sanitisers / soaps</fullName>
                    <default>false</default>
                    <label>Sanitisers / soaps</label>
                </value>
                <value>
                    <fullName>Sharps</fullName>
                    <default>false</default>
                    <label>Sharps</label>
                </value>
                <value>
                    <fullName>Sharps Disposal</fullName>
                    <default>false</default>
                    <label>Sharps Disposal</label>
                </value>
                <value>
                    <fullName>Standard Mats</fullName>
                    <default>false</default>
                    <label>Standard Mats</label>
                </value>
                <value>
                    <fullName>Towel Dispensers</fullName>
                    <default>false</default>
                    <label>Towel Dispensers</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Lead_Salutation__c</fullName>
        <externalId>false</externalId>
        <label>Lead Salutation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Mr.</fullName>
                    <default>false</default>
                    <label>Mr.</label>
                </value>
                <value>
                    <fullName>Mrs.</fullName>
                    <default>false</default>
                    <label>Mrs.</label>
                </value>
                <value>
                    <fullName>Ms.</fullName>
                    <default>false</default>
                    <label>Ms.</label>
                </value>
                <value>
                    <fullName>Miss.</fullName>
                    <default>false</default>
                    <label>Miss.</label>
                </value>
                <value>
                    <fullName>Dr.</fullName>
                    <default>false</default>
                    <label>Dr.</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Lead_Source__c</fullName>
        <externalId>false</externalId>
        <label>Lead Source</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Cross Divisional Lead</fullName>
                    <default>false</default>
                    <label>Cross Divisional Lead</label>
                </value>
                <value>
                    <fullName>Website</fullName>
                    <default>false</default>
                    <label>Website</label>
                </value>
                <value>
                    <fullName>Phone Enquiry</fullName>
                    <default>false</default>
                    <label>Phone Enquiry</label>
                </value>
                <value>
                    <fullName>Calling Campaign upload</fullName>
                    <default>false</default>
                    <label>Calling Campaign upload</label>
                </value>
                <value>
                    <fullName>Email Campaign</fullName>
                    <default>false</default>
                    <label>Email Campaign</label>
                </value>
                <value>
                    <fullName>Direct Mail</fullName>
                    <default>false</default>
                    <label>Direct Mail</label>
                </value>
                <value>
                    <fullName>Google PPC</fullName>
                    <default>false</default>
                    <label>Google PPC</label>
                </value>
                <value>
                    <fullName>Advert</fullName>
                    <default>false</default>
                    <label>Advert</label>
                </value>
                <value>
                    <fullName>Exhibition</fullName>
                    <default>false</default>
                    <label>Exhibition</label>
                </value>
                <value>
                    <fullName>Linked in/Social Media</fullName>
                    <default>false</default>
                    <label>Linked in/Social Media</label>
                </value>
                <value>
                    <fullName>Purchased List</fullName>
                    <default>false</default>
                    <label>Purchased List</label>
                </value>
                <value>
                    <fullName>Partner Referral</fullName>
                    <default>false</default>
                    <label>Partner Referral</label>
                </value>
                <value>
                    <fullName>Other Referral</fullName>
                    <default>false</default>
                    <label>Other Referral</label>
                </value>
                <value>
                    <fullName>Cloned opportunity</fullName>
                    <default>false</default>
                    <label>Cloned opportunity</label>
                </value>
                <value>
                    <fullName>Cold call/self generated</fullName>
                    <default>false</default>
                    <label>Cold call/self generated</label>
                </value>
                <value>
                    <fullName>Customer renewal</fullName>
                    <default>false</default>
                    <label>Customer renewal</label>
                </value>
                <value>
                    <fullName>Customer Upsell</fullName>
                    <default>false</default>
                    <label>Customer Upsell</label>
                </value>
                <value>
                    <fullName>Surveyor Lead</fullName>
                    <default>false</default>
                    <label>Surveyor Lead</label>
                </value>
                <value>
                    <fullName>Driver Observation</fullName>
                    <default>false</default>
                    <label>Driver Observation</label>
                </value>
                <value>
                    <fullName>Driver Request</fullName>
                    <default>false</default>
                    <label>Driver Request</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Record_Type__c</fullName>
        <externalId>false</externalId>
        <label>Record Type</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>External</label>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>LAST_UPDATE</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Case_Subject__c</columns>
        <columns>Lead_Last_Name__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>External Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Externals</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
