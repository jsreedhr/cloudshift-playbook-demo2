<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Logs for any successful or failed callouts to the Wildebeest Integration</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Callout_Type__c</fullName>
        <externalId>false</externalId>
        <label>Callout Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Login</fullName>
                    <default>false</default>
                    <label>Login</label>
                </value>
                <value>
                    <fullName>Send</fullName>
                    <default>false</default>
                    <label>Send</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Created_Time__c</fullName>
        <externalId>false</externalId>
        <formula>LEFT(RIGHT(text( CreatedDate ),9),8)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Created Time</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Exception_Message__c</fullName>
        <description>The actual exception raised</description>
        <externalId>false</externalId>
        <label>Exception Message</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Exception_Stack_Trace__c</fullName>
        <externalId>false</externalId>
        <label>Exception Stack Trace</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Exception_Type__c</fullName>
        <externalId>false</externalId>
        <label>Exception Type</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HTTP_Request_Body__c</fullName>
        <description>The actually HTTP request made from Salesforce (outbound)</description>
        <externalId>false</externalId>
        <label>HTTP Request Body</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>HTTP_Request_Endpoint__c</fullName>
        <externalId>false</externalId>
        <label>HTTP Request Endpoint</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HTTP_Response_Body__c</fullName>
        <description>The actual response received from the callout</description>
        <externalId>false</externalId>
        <label>HTTP Response Body</label>
        <length>131072</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>HTTP_Response_Status_Code__c</fullName>
        <externalId>false</externalId>
        <label>HTTP Response Status Code</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HTTP_Response_Status__c</fullName>
        <externalId>false</externalId>
        <label>HTTP Response Status</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HTTP_Response_Warning__c</fullName>
        <externalId>false</externalId>
        <label>HTTP Response Warning</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Object_Type__c</fullName>
        <description>The type of object the callout relates to</description>
        <externalId>false</externalId>
        <label>Object Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Case</fullName>
                    <default>false</default>
                    <label>Case</label>
                </value>
                <value>
                    <fullName>CaseComment</fullName>
                    <default>false</default>
                    <label>CaseComment</label>
                </value>
                <value>
                    <fullName>Task</fullName>
                    <default>false</default>
                    <label>Task</label>
                </value>
                <value>
                    <fullName>CustomerDetails</fullName>
                    <default>false</default>
                    <label>CustomerDetails</label>
                </value>
                <value>
                    <fullName>BuyerCustomerDetails</fullName>
                    <default>false</default>
                    <label>BuyerCustomerDetails</label>
                </value>
                <value>
                    <fullName>GetBuyerCustomerDetails</fullName>
                    <default>false</default>
                    <label>GetBuyerCustomerDetails</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Parent_Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Parent Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Integration Logs</relationshipLabel>
        <relationshipName>Integration_Logs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Record_ID__c</fullName>
        <description>Id of the record which relates to the callout</description>
        <externalId>false</externalId>
        <label>Record ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Success__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Flag to determine if the callout was completed successfully without any errors</description>
        <externalId>false</externalId>
        <label>Success</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Integration Log</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Object_Type__c</columns>
        <columns>Success__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <columns>HTTP_Response_Status__c</columns>
        <columns>HTTP_Response_Status_Code__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_Failures</fullName>
        <columns>CREATED_DATE</columns>
        <columns>Created_Time__c</columns>
        <columns>NAME</columns>
        <columns>Callout_Type__c</columns>
        <columns>Object_Type__c</columns>
        <columns>Exception_Type__c</columns>
        <columns>HTTP_Response_Status__c</columns>
        <columns>HTTP_Response_Status_Code__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Success__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>All Failures</label>
    </listViews>
    <listViews>
        <fullName>All_Today</fullName>
        <columns>CREATED_DATE</columns>
        <columns>Created_Time__c</columns>
        <columns>NAME</columns>
        <columns>Object_Type__c</columns>
        <columns>Success__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>HTTP_Response_Status__c</columns>
        <columns>HTTP_Response_Status_Code__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>All Today</label>
    </listViews>
    <nameField>
        <displayFormat>IL-{000000}</displayFormat>
        <label>Integration Log Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Integration Logs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
