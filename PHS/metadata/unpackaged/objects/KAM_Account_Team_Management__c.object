<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override updated by Lightning App Builder during activation.</comment>
        <content>KAM_Account_Team_Management_Record_Page1</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Owner__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account Owner</label>
        <referenceTo>User</referenceTo>
        <relationshipName>KAM_Account_Team_Management1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Key_Account_Role_Type__c</fullName>
        <externalId>false</externalId>
        <label>Key Account Role Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Key Account Director</fullName>
                    <default>false</default>
                    <label>Key Account Director</label>
                </value>
                <value>
                    <fullName>Key Account Executive</fullName>
                    <default>false</default>
                    <label>Key Account Executive</label>
                </value>
                <value>
                    <fullName>Key Account Manager</fullName>
                    <default>false</default>
                    <label>Key Account Manager</label>
                </value>
                <value>
                    <fullName>Senior Key Account Manager</fullName>
                    <default>false</default>
                    <label>Senior Key Account Manager</label>
                </value>
                <value>
                    <fullName>Waste Management Key Account Manager</fullName>
                    <default>false</default>
                    <label>Waste Management Key Account Manager</label>
                </value>
                <value>
                    <fullName>Head of Key Accounts</fullName>
                    <default>false</default>
                    <label>Head of Key Accounts</label>
                </value>
                <value>
                    <fullName>Key Account Broker</fullName>
                    <default>false</default>
                    <label>Key Account Broker</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Key_Account_Type__c</fullName>
        <externalId>false</externalId>
        <label>Key Account Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Key</fullName>
                    <default>false</default>
                    <label>Key</label>
                </value>
                <value>
                    <fullName>National</fullName>
                    <default>false</default>
                    <label>National</label>
                </value>
                <value>
                    <fullName>Public Sector</fullName>
                    <default>false</default>
                    <label>Public Sector</label>
                </value>
                <value>
                    <fullName>Tender</fullName>
                    <default>false</default>
                    <label>Tender</label>
                </value>
                <value>
                    <fullName>Waste Management</fullName>
                    <default>false</default>
                    <label>Waste Management</label>
                </value>
                <value>
                    <fullName>Broker</fullName>
                    <default>false</default>
                    <label>Broker</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Master_Account_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>false</externalId>
        <label>Master Account Id</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Master_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Master Account</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1</booleanFilter>
            <filterItems>
                <field>Account.HasNegotiatorResponsibility__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>KAM Account Team Management</relationshipLabel>
        <relationshipName>KAM_Account_Team_Management</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>KAM Account Team Management</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Master_Account__c</columns>
        <columns>Account_Owner__c</columns>
        <columns>Key_Account_Type__c</columns>
        <columns>Key_Account_Role_Type__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>KAM-{000}</displayFormat>
        <label>KAM Account Team Management Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>KAM Account Team Management</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Master_Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account_Owner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Key_Account_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Key_Account_Role_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Master_Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account_Owner__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Key_Account_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Key_Account_Role_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UPDATEDBY_USER</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Master_Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account_Owner__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Key_Account_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Key_Account_Role_Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UPDATEDBY_USER</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Master_Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Account_Owner__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Key_Account_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Key_Account_Role_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>UPDATEDBY_USER</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
