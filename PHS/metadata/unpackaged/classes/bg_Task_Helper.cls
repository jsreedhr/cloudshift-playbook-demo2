/********************************************************************************
* bg_Task_Helper :
*
* Helper Class for the Task trigger methods
*
* Created By: Tom Morris
* Created Date: 17-01-2017
* Changes: 20-11-2017 Danny Ayres, added teleappointerTask method
* Changes: 22-11-2017 Rizwan, added populateLeadOpportunityData method
*********************************************************************************/

public with sharing class bg_Task_Helper {

private static boolean run = true;
     public static boolean runOnce(){
    if(run){
     run=false;
  return true;
    }else{
    return run;
    }
    
    }

	public static void updateAge(List<Task> taskList, Map<Id, Task> oldTasks)
	{
		List<Task> tasksToUpdate = new List<Task>();

		for(Task task : taskList)
		{
			Task oldTask = oldTasks.get(task.Id);
			Boolean completedDateHasChanged = task.Closed_Date__c != oldTask.Closed_Date__c && task.Closed_Date__c != null;

			if(completedDateHasChanged)
			{
				tasksToUpdate.add(task);
			}
		}

		if(!tasksToUpdate.isEmpty())
		{
			updateAge(tasksToUpdate);
		}
	}

	public static void updateAge(List<Task> taskList)
	{
		BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];

		List<Task> tasksToUpdate = new List<Task>();

		for(Task task : taskList)
		{
			if(task.Closed_Date__c != null)
			{
				Long milliseconds = BusinessHours.diff(bh.Id, task.CreatedDate, task.Closed_Date__c);
				Long seconds = (Long)milliseconds/1000;
				Long minutes = (Long)seconds/60;
				Long hours = (Long)minutes/60;
				Long displayMinutes = (Long)Math.mod((Integer)minutes,60);

				Task newTask = new Task();
				newTask.Id = task.Id;
				newTask.Age_Minutes__c = minutes;
				newTask.Age_Display__c = 'Hours: ' + hours + ' Minutes: ' + displayMinutes;

				tasksToUpdate.add(newTask);
			}
		}

		if(!tasksToUpdate.isEmpty())
		{
			update tasksToUpdate;
		}
	}

	public static void validateParentCaseForSync(List<Task> tasks)
	{
		Set<Id> caseIDs = new Set<Id>();
		//Map<Id,Id> caseCommParentCaseMap = new Map<Id,Id>();
		String casePrefix = Schema.SObjectType.Case.getKeyPrefix();
		for (Task newTask : tasks)
		{
			if (((String)newTask.WhatId).startsWith(casePrefix) && newTask.WhatId != null)
			{
				caseIDs.add(newTask.WhatId);
			}
		}

		Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, Wildebeest_Case_Reference__c FROM Case where Id IN : caseIDS]);

		for (Task newTask : tasks)
		{
			Case parentCase = caseMap.get(newTask.WhatId);
			if (parentCase.Wildebeest_Case_Reference__c == null || parentCase.Wildebeest_Case_Reference__c == '')
			{
				system.debug('**Case Comm**: ' + parentCase.Wildebeest_Case_Reference__c);
				newTask.addError('The parent Case is still being synced with Wildebeest, please try again in a few seconds');
			}
		}
	}

	public static void teleappointerTask(List<Task> tasks){
		/*
		// To  populate Task Parent (Opportunity /Lead) feild values
		TaskTriggerHandler.updateParentFields(tasks);
		if(userinfo.getUserRoleId()!=NULL && userinfo.getUserRoleId().length()>1){
			UserRole u = [SELECT Name FROM UserRole where Id =: userinfo.getUserRoleId() Limit 1];

			String uname = u.name;
			if((u.name).containsIgnoreCase('Teleappointing')){
				String lead_prefix = Schema.SObjectType.Lead.getKeyPrefix();
				Set<Id> leadIds = new Set<Id>();
				for(Task t : tasks){
					if(((String)t.WhoId).startsWith(lead_prefix))
						leadIds.add(t.WhoId);
				}

				List<Lead> leadList = [Select id, Tele_Appointer__c from Lead where (Tele_Appointer__c = NULL
				OR Tele_Appointer__c = '') AND Id IN: leadIds];

				if(leadList.size() > 0){
					for(Lead l : leadList){
						l.Tele_Appointer__c = userinfo.getName();
					}
					update leadList;
				}

			}

		}*/
	}
	/* Mathod Name : populateLeadOpportunityData
	*  created By : Md Rizwan
	*  created Date : 22-Nov-2017
	*  Description : updates few fields of Task record based on values in related Lead/Opportunity record
	*/
	public static void populateLeadOpportunityData(List<Task> taskList){

		Set<Id> leadIdSet = new Set<Id>();
		Set<Id> opportunityIdSet = new Set<Id>();
        List<Task> taskUpdateList = new List<Task>();

		String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();
		String opportunityPrefix = Schema.SObjectType.Opportunity.getKeyPrefix();

		for(Task task:taskList){
			if(task.WhoId!=null && ((String)task.WhoId).startsWith(leadPrefix))
				leadIdset.add(task.WhoId);
			else if(task.WhatId!=null && ((String)task.WhatId).startsWith(opportunityPrefix))
				opportunityIdSet.add(task.WhatId);
		}
		// populate field values from related leads and opportunities
		if(leadIdSet.size()>0){
			Map<Id,Lead> leadMap = new Map<Id,Lead>();
			List<Lead> leadList = [Select Id, Lead_Score_Total__c, LeadSource, Campaigns__c, Status, Postal_District__c From Lead Where Id IN: leadIdSet];
			for(Lead lead:leadList){
				if(!leadMap.containsKey(lead.Id))
					leadMap.put(lead.Id, lead);
			}
			for(Task task:taskList){
				if(task.WhoId != NULL && leadMap.containsKey(task.WhoId)){
					Lead lead = leadMap.get(task.WhoId);
					task.Score__c = lead.Lead_Score_Total__c;
					task.Source__c = lead.LeadSource;
					task.Campaigns__c = lead.Campaigns__c;
					task.Status__c = lead.Status;
                    task.Postal_District__c = lead.Postal_District__c;

				}
			}
		}
		if(opportunityIdSet.size()>0){
			Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
			List<Opportunity> opportunityList = [Select Id, LeadSource,StageName, Campaigns__c, Amount, DM_Postal_District__r.Name From Opportunity Where Id IN: opportunityIdSet];
			for(Opportunity opp:opportunityList){
				if(!opportunityMap.containsKey(opp.id))
					opportunityMap.put(opp.Id, opp);
			}
			for(Task task:taskList){
				if(task.WhatId != NULL && opportunityMap.containsKey(task.WhatId)){
					Opportunity opp = opportunityMap.get(task.WhatId);
                    task.Source__c = opp.LeadSource;
					task.Stage__c = opp.StageName;
					task.Amount__c = opp.Amount;
					task.Campaigns__c = opp.Campaigns__c;
                    task.Postal_District__c = opp.DM_Postal_District__r.Name;
				}
			}
		}

	}
}