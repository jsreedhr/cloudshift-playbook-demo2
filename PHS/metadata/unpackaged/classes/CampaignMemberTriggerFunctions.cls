public without sharing class CampaignMemberTriggerFunctions {

    public static void getcampaignproducts(List <CampaignMember> lst_campmembers){
        
		Set <ID> set_campaignids = new Set <ID>();
        Map <ID, ID> map_CampLeadIds = new Map <ID, ID>();
        Map <ID, ID> map_LeadProductIdLeadId = new Map <ID, ID>();
        List <Lead_Product__c> lst_lps = new List <Lead_Product__c>();
        Set <ID> set_leadids = new Set <ID>();
        List <Lead_Product__c> lst_LPstoupdate = new List <Lead_Product__c>();
        
        for(CampaignMember cm: lst_campmembers){
            
            IF(cm.LeadId !=Null){
                set_campaignids.add(cm.CampaignId);
                map_CampLeadIds.put(cm.CampaignId, cm.LeadId);
            }
        }
        
        IF(!set_campaignids.isEmpty()){
            
            List <Campaign_Product__c> lst_campproducts = new List <Campaign_Product__c>([SELECT ID, Campaign__c, Campaign_Name__c, Annual_Cost__c, Floor_Price__c, Trade_Price__c, High_Price__c, Weekly_Cost__c, Service_Frequency__c, Product__c FROM Campaign_Product__c WHERE Campaign__c in: set_campaignids]);
                    
            IF(!lst_campproducts.isEmpty()){
            	for(Campaign_Product__c cp: lst_campproducts){
                    Lead_Product__c lp = new Lead_Product__c();
                    lp.Lead__c = map_CampLeadIds.get(cp.Campaign__c);                                       
                    lp.Product__c = cp.Product__c;
                    lp.Annual_Cost__c = cp.Annual_Cost__c;
                    lp.High_Price__c = cp.High_Price__c;
                    lp.Floor_Price__c = cp.Floor_Price__c;
                    lp.Trade_Price__c = cp.Trade_Price__c;
                    lp.Weekly_Cost__c = cp.Weekly_Cost__c;
                    lp.Service_Frequency__c = cp.Service_Frequency__c;
                    lp.Campaign__c = cp.Campaign_Name__c;
                    lst_lps.add(lp);
           		}
                insert lst_lps; 
                
                //Set the owner of the lead product to be the current owner of the lead
                
                for(Lead_Product__c lp: lst_lps){
                	 set_leadids.add(lp.Lead__c);
                }
                
                IF(!set_leadids.isEmpty()){
                	
                    List <Lead> lst_leads = new List <Lead>([SELECT ID, OwnerId, Owner.Type FROM Lead WHERE id in: set_leadids]);
                    
                    IF(!lst_leads.isEmpty()){
                        
                        for(Lead l: lst_leads){
                            IF(l.Owner.Type == 'User'){
                            map_LeadProductIdLeadId.put(l.id, l.OwnerId);
                            }
                        }
						
						for(Lead_Product__c lp: lst_lps){
                            IF(map_LeadProductIdLeadId.get(lp.Lead__c) != Null){
                        	lp.OwnerId = map_LeadProductIdLeadId.get(lp.Lead__c);
                            lst_LPstoupdate.add(lp);
                            }                            
                        }
                        Update lst_LPstoupdate;
                    }
                }
            }
        }
    } 
    
    // A method to update Campaigns field under CampaignMember after insert, update, delete and undelete.
    public static void updateLeadCampaignField(List<CampaignMember> campaignMemberList){
        
        Map<String, String> mapLeadToCampMember = new Map<String, String>();
        Set<String> setLeadId = new Set<String>();
        for(CampaignMember objCam : campaignMemberList){
            system.debug('mapLeadToCampMember---->' +mapLeadToCampMember);
            if(objCam.LeadId != null && !setLeadId.contains(objCam.LeadId)){
                setLeadId.add(objCam.LeadId);
                system.debug('mapLeadToCampMember---->' +mapLeadToCampMember);
                if(mapLeadToCampMember.containsKey(objCam.LeadId)){
                    String campaignName =mapLeadToCampMember.get(objCam.LeadId);
                    campaignName += objCam.Campaign.Name + ',';   
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
                else{
                    String campaignName = objCam.Campaign.Name + ',';   
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
            }
        }
        
        List<Lead> leadList = new List<Lead>([Select Campaigns__c From Lead Where Id in :setLeadId]);
        for(Lead leadRecord :leadList){
            if(mapLeadToCampMember.containsKey(leadRecord.Id)){
                leadRecord.Campaigns__c = mapLeadToCampMember.get(leadRecord.Id).removeEnd(',');
            }
        }
        update leadList;
    }
}