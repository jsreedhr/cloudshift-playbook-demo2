/*********************************************************************************
 * bg_VFC_AccountController_Tests:
 *
 * Tests for the test VFC Account Controller methods
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 18-01-2016
 *
 *********************************************************************************/

@isTest
private class bg_VFC_AccountController_Tests {
        
    @testSetup
    static void setupData()
    {
        Account act = new Account(Name = 'Test Account', BillingPostalcode = 'LS1 3DD');
        insert act;


        Opportunity opp = new Opportunity(Name = 'Test Opp',
                                          Account = act,
                                          CloseDate = Date.today(),
                                          StageName = 'Contract Lost',
                                          Next_Renewal_Date__c = Date.today(),
                                          Lost_Competitor__c = 'Box It',
                                          Closure_Reason__c = 'Lost on Price',
                                          Amount = 100);
        insert opp;

        Opportunity opp2 = new Opportunity(Name = 'Test Opp 2',
                                          Account = act,
                                          CloseDate = Date.today(),
                                          StageName = 'Contract Won',
                                          Next_Renewal_Date__c = Date.today(),
                                          Closure_Reason__c = 'Lost on Price',
                                          Amount = 100);
        insert opp2;

        Opportunity opp3 = new Opportunity(Name = 'Test Opp 3',
                                          Account = act,
                                          CloseDate = Date.today(),
                                          StageName = 'Opened Lead',
                                          Amount = 100);
        insert opp3;
    }


    static testMethod void testVFC()
    {
        Test.startTest();
   
        Account a = [select Id, Name, Closed_Won_Opptys__c,Closed_Lost_Opptys__c,Open_Oppty_Value__c,Closed_Won_Value__c from Account where Name = 'Test Account' limit 1];
        ApexPages.StandardController controller = new ApexPages.StandardController(a);
        VFC_AccountController extension = new VFC_AccountController(controller);
        Account acct = extension.a;

        extension.getPieData();
        extension.getBar();

        System.assertEquals(a.Id, acct.Id);
        System.assertEquals(a.Closed_Won_Opptys__c, acct.Closed_Won_Opptys__c);
        System.assertEquals(a.Closed_Lost_Opptys__c, acct.Closed_Lost_Opptys__c);
        System.assertEquals(a.Open_Oppty_Value__c, acct.Open_Oppty_Value__c);
        System.assertEquals(a.Closed_Won_Value__c, acct.Closed_Won_Value__c);
        Test.stopTest();
    }
}