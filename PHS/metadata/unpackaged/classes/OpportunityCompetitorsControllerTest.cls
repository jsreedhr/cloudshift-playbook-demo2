/**
 *  @Class Name:    OpportunityCompetitorsControllerTest
 *  @Description:   This is a test class for OpportunityCompetitorsController
 *  @Company:       dQuotient
 *  CreatedDate:    28/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           28/11/2017                  Original Version
 */
@isTest
private class OpportunityCompetitorsControllerTest {

    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        List<Account> accList= new List<Account>();
        Account acc = bg_Test_Data_Utils.createAccount('N');
         acc.Company_Status__c = 'Competitor';
        accList.add(acc);
        Account acc1 = bg_Test_Data_Utils.createAccount('W');
        acc1.Name = Label.Unknown_Competitor_Name;
         acc1.Company_Status__c = 'Competitor';
        accList.add(acc1);
        insert accList;

        Lead lead1 = bg_Test_Data_Utils.createLead('1qqwe');
        lead1.OwnerId = u.id;
        insert lead1;
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp1 = bg_Test_Data_Utils.createOpportunity('Test Opp1', acc.id);
        opp1.OwnerId = u.Id;
        oppList.add(opp1);
        
        Opportunity opp2 = bg_Test_Data_Utils.createOpportunity('Test Opp2', acc.id);
        opp2.OwnerId = u.Id;
        oppList.add(opp2);
        insert oppList;
        
        List<Competitor__c> compList = new List<Competitor__c>();
        Competitor__c competitor1 = bg_Test_Data_Utils.createCompetitor(lead1.Id);
        competitor1.Competitor_Account__c = acc.Id;
        competitor1.Opportunity__c = opp1.Id;
        competitor1.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
        compList.add(competitor1);
        
        Competitor__c competitor2 = bg_Test_Data_Utils.createCompetitor(lead1.Id);
        competitor2.Competitor_Account__c = acc1.Id;
        competitor2.Opportunity__c = opp2.Id;
        competitor2.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
        compList.add(competitor2);
        
        insert compList;
        
    }
    
    // Test createRecords method in OpportunityCompetitorsController
	private static testMethod void testcreateRecords() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name='Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.createRecords();
        }
        test.stopTest();
	}
	
	// Test addRow method in OpportunityCompetitorsController
	private static testMethod void testaddRow() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name='Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.addRow();
        }
        test.stopTest();
	}
	
	// Test selectAll method in OpportunityCompetitorsController
	private static testMethod void testselectAllfalse() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name='Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.selectAll();
        }
        test.stopTest();
	}
	
	// Test addRow method in OpportunityCompetitorsController
	private static testMethod void testdeleteSelectedRows() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name='Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.selectAll = true;
            ext.selectAll();
            ext.deleteSelectedRows();
        }
        test.stopTest();
	}
	
	// Test delRow method in OpportunityCompetitorsController
	private static testMethod void testdelRow() {
	    User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name='Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.RowNo = '0';
            ext.delRow();
        }
        test.stopTest();
	}
	
	// Test checkduplicates method in OpportunityCompetitorsController
	private static testMethod void testcheckduplicates() {
	    User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [select Id from Account where Name= 'Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.OpportunityCompetitor')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.checkduplicates();
        }
        test.stopTest();
	}
	
	// Test saveCompetitor method in OpportunityCompetitorsController
	private static testMethod void testsaveCompetitor() {
        Account acc = [select Id from Account where Name= 'Test Account N'];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where AccountId = :acc.Id Limit 1];
        test.startTest();
            PageReference pageRef = Page.OpportunityCompetitor;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', oppo.id);
            Apexpages.StandardController sc = new Apexpages.StandardController(oppo);
            OpportunityCompetitorsController ext = new  OpportunityCompetitorsController(sc);
            ext.saveCompetitor();
        test.stopTest();
    }
}