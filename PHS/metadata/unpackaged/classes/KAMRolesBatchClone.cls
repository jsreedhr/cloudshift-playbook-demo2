global class KAMRolesBatchClone implements Database.Batchable <sObject>, Database.stateful {
    
    private map<Id, KAM_Account_Team_Management__c> mapAccountIdToKAMTeamMenagment = new map<Id, KAM_Account_Team_Management__c>();
    private List<Account> listAccountToUpdate = new List<Account>();
    public KAMRolesBatchClone(){
        List<KAM_Account_Team_Management__c> listKAMTeamManag = [SELECT Account_Owner__c, Key_Account_Role_Type__c, Key_Account_Type__c,
                                                                                             Master_Account__c 
                                                                   FROM KAM_Account_Team_Management__c 
                                                                  WHERE Account_Owner__c != NULL AND isDeleted = FALSE];
        for(KAM_Account_Team_Management__c objKAM: listKAMTeamManag){
            mapAccountIdToKAMTeamMenagment.put(objKAM.Master_Account__c, objKAM);
        }
    }
    
    public void run(Integer size){
        Database.executeBatch(this, size==null?200: size);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        return Database.getQueryLocator([SELECT Parent_Account__r.Name, Child_Account__r.Name, Child_Account__c, Parent_Account__c FROM PHS_Account_Relationship__c 
        WHERE Parent_Account__c IN: mapAccountIdToKAMTeamMenagment.keySet() AND isDeleted = FALSE]);
    }

    global void execute(Database.BatchableContext bc, List <PHS_Account_Relationship__c> phsAccountRelationship) {
        
        Set<Id> setChildAccountIds = new Set<Id>();
        for(PHS_Account_Relationship__c phs: phsAccountRelationship) {
            setChildAccountIds.add(phs.Child_Account__c);
            if (mapAccountIdToKAMTeamMenagment.containsKey(phs.Parent_Account__c)) {
                KAM_Account_Team_Management__c kam = mapAccountIdToKAMTeamMenagment.get(phs.Parent_Account__c);
                Account parentAccount = new Account( Id = phs.Parent_Account__c,
                                                     Test_Key_Account__c=TRUE,
                                                     Test_Key_Account_Role__c =kam.Key_Account_Role_Type__c,
                                                     Test_Key_Account_Type__c=kam.Key_Account_Type__c);
                system.debug('=====1========parentAccount======'+parentAccount);
                listAccountToUpdate.add(parentAccount);
            }
        }
        fetchHierarchyAccount(setChildAccountIds);
    }
    
    private void fetchHierarchyAccount(Set<id> setChildAccountIds){
        Set<Id> setAnotherChildAccountIds = new Set<Id>();
        for(PHS_Account_Relationship__c phs: [SELECT Id, Child_Account__c, Parent_Account__c FROM PHS_Account_Relationship__c 
                                               WHERE Parent_Account__c IN: setChildAccountIds AND isDeleted = FALSE]) {
            setAnotherChildAccountIds.add(phs.Child_Account__c);
            if (mapAccountIdToKAMTeamMenagment.containsKey(phs.Parent_Account__c)) {
                KAM_Account_Team_Management__c kam = mapAccountIdToKAMTeamMenagment.get(phs.Parent_Account__c);
                Account parentAccount = new Account( Id = phs.Parent_Account__c,
                                                     Test_Key_Account__c=TRUE,
                                                     Test_Key_Account_Role__c =kam.Key_Account_Role_Type__c,
                                                     Test_Key_Account_Type__c=kam.Key_Account_Type__c);
                system.debug('=====2========parentAccount======'+parentAccount);
                listAccountToUpdate.add(parentAccount);
            }
        }
        if(setAnotherChildAccountIds.size()>0){
            fetchHierarchyAccount(setAnotherChildAccountIds);
        }
        else{
            //TODO: update list here 
            
            // update listAccountToUpdate;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
            From AsyncApexJob Where Id =: bc.getJobId()
        ];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {
            a.CreatedBy.Email
        };
        mail.setToAddresses(toAddresses);
        mail.setSubject('KAM Key Roles Batch Status : ' + a.Status);
        mail.setPlainTextBody('Records processed : ' + a.TotalJobItems +
            'with ' + a.NumberOfErrors + ' failures.');
        // Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
        //     mail
        // });
    }
}