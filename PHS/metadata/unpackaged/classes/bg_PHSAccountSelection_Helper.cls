public with sharing class bg_PHSAccountSelection_Helper {

    public static List<SelectOption> generatePHSAccountOptions(Id parentAccountId){
    	List<SelectOption> options = new List<SelectOption>();
        List<PHS_Account__c> phsAccounts = [select Id, Wildebeest_Ref__c, Admin_Team_Name__c, Location_AVI_Value__c from PHS_Account__c where Salesforce_Account__c = :parentAccountId];
        for (PHS_Account__c phsAccount : phsAccounts)
        {
            options.add(new SelectOption(phsAccount.Id, (phsAccount.Wildebeest_Ref__c + ' ' + phsAccount.Admin_Team_Name__c + ' ' + phsAccount.Location_AVI_Value__c).remove('null')));
        }
        return options;
    }

    public static List<String> generateErrorMessages(Case theCase){
    	List<String> errors = new List<String>();
    	if (theCase.AccountId == null)
    	{
    		errors.add(Label.Account_Must_Have_PHS_Accounts);
    	}
    	if (theCase.PHS_Account__c == null)
    	{
            errors.add(Label.PHS_Account_Service_Enabled);
    	}
    	else
    	{
    		PHS_Account__c phsAccount = [select Name, Service_Enabled__c, Wildebeest_Ref__c from PHS_Account__c where Id = :theCase.PHS_Account__c];
	    	if (!phsAccount.Service_Enabled__c)
	    	{
	            errors.add(Label.PHS_Account_Service_Enabled);
	    	}
	    	if (phsAccount.Wildebeest_Ref__c == null)
	    	{
	            errors.add(Label.PHS_Account_Wildebeest_Ref);
	    	}
    	}
    	return errors;
    }

    public static PageReference generateReturnURL(String caseIdOrNumber, Id accountId, String accountName, Id phsAccountId, String phsAccountName, String returnURL, String recordTypeId, Id contactId, String contactName) {

        Glogbal_Settings__c globalSettings = Glogbal_Settings__c.getOrgDefaults();

        String phsAccountFieldId = globalSettings.PHS_Account_Field_Id__c;
        String phsAccountLinkFieldId = globalSettings.PHS_Account_Link_Id__c;

        String url = '/' + caseIdOrNumber + '/e?ent=Case';

        Map<String, String> urlMap = new Map<String, String>{'cas4_lkid' => accountId,
                                                             'cas4' => accountName,
                                                             phsAccountLinkFieldId => phsAccountId,
                                                             phsAccountFieldId => phsAccountName,
                                                             'cas3' => contactName,
                                                             'cas3_lkid' => contactId,
                                                             'retURL' => returnURL
                                                         };

        if(recordTypeId != null)
        {
            urlMap.put('RecordType', recordTypeId);
        }

        PageReference newPageRef = new PageReference(url);
        newPageRef.getParameters().putAll(urlMap);

        return newPageRef;

    }

    public static Case generateComponentStartPoint() {
        Case theCase = new Case();
        Id recordId = ApexPages.currentPage().getParameters().get('id');

        Id contactId = recordId != null && recordId.getSobjectType() == Schema.Contact.getSobjectType().getDescribe().getSobjectType() ? recordId : null;
        Id accountId = recordId != null && recordId.getSobjectType() == Schema.Account.getSobjectType().getDescribe().getSobjectType() ? recordId : null;
        /*
         * Possibly select the case for the update.
         */ 
        Id caseId = recordId != null && recordId.getSobjectType() == Schema.Case.getSobjectType().getDescribe().getSobjectType() ? recordId : null;

        theCase.AccountId = accountId;
        theCase.ContactId = contactId;
        theCase.Id = caseId;

        Contact contactRecord = new Contact();
        if(theCase.ContactId != null)
        {
            List<Contact> contactRecords = [Select Id, AccountId From Contact Where Id = :theCase.ContactId Limit 1];

            if(contactRecords.size() > 0)
            {
                contactRecord = contactRecords[0];
                theCase.AccountId = contactRecord.AccountId;
            }
        }
        System.debug('Brightgen theCase[' + theCase + ']');

        return theCase;
    }
}