/**********************************************************************
* bg_MyNotesController:
*
* Controller for the 'My Notes'.  
* Created By: BrightGen Ltd
* Created Date: 19/04/2016
*
* Changes: 
***********************************************************************/

public with sharing class bg_MyNotesController {
    public void setNotes() {
        myNotepad.note__c = Notes;
        update myNotepad;
    }
    
    My_Note__c myNotepad;
    Integer notesCount;
    public String Notes{ get; set;} 

    public bg_MyNotesController() {
         notesCount = [Select COUNT() from My_Note__c where ownerId = :UserInfo.getUserId()];
    }
    
    public void init() {
         My_Note__c  notesObject;
         if (notesCount == 0) 
         {
             notesObject = new My_Note__c();
             notesObject.ownerId = UserInfo.getUserId();
             insert notesObject;
         }
         else
         {
             notesObject = [Select Note__c from My_Note__c where ownerId = :UserInfo.getUserId() LIMIT 1];
         }
         myNotepad = notesObject;
         Notes = notesObject.note__c;
    }
}