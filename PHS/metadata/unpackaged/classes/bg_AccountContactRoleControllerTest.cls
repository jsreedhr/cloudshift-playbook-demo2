/*********************************************************************************
 * bg_AccountContactRoleControllerTest
 *
 * Test class for the account contact role controller.   
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 18-01-2016
 *
 *********************************************************************************/

@istest
public with sharing class bg_AccountContactRoleControllerTest
{
    @testSetup
    static void setupData()
    {
        Account a = bg_Test_Data_Utils.createAccount('Test Account');
        insert a;

        Contact c = new Contact(FirstName = 'Test', LastName = 'Test');
        c.Account = a;
        insert c;
        
        AccountContactRole acr = new AccountContactRole(AccountId = a.Id, ContactId = c.Id, Role = 'Influencer', IsPrimary = TRUE);
        insert acr;
    }

    static testMethod void testContactRolePositive()
    {
        Test.startTest();
        Contact c = [select Id from Contact where FirstName = 'Test' limit 1];
        ApexPages.StandardController controller = new ApexPages.StandardController(c);
        bg_AccountContactRoleController extension = new bg_AccountContactRoleController(controller);
        List<AccountContactRole> acrs = extension.acrs;
        System.assertEquals(1, extension.acrs.size());
        System.assertEquals('Influencer', acrs[0].Role);
        Test.stopTest();
    }
}