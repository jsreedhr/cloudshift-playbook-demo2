//this class is not referred by any component or the comp referred is not used in anywhere
global class AutoCompleteDemoController 
{
    private static Map<String, Schema.SObjectType> gd =Schema.getGlobalDescribe();

    public  List<fieldWrapper> listWrapper {get;set;}
    public  SObject newSobject {get;set;}
    public Boolean  showModal {get; set;}

    public AutoCompleteDemoController(){
        //createSobject();
        showModal = false;
    }

    public string createSobject() 
    {
        string obj = Apexpages.currentPage().getParameters().get('lookup_obj_api');
        Schema.SObjectType sot = gd.get(obj);
        newSobject = sot.newSObject();
        showModal = true;
        if (sot == null) {
            return null;
        }
        Schema.DescribeSObjectResult DescribeSObjectParentObj = sot.getDescribe();
        Map<String,AutocompleteObjectFieldset__c> mapCustomData = AutocompleteObjectFieldset__c.getAll();
        Schema.FieldSet fieldSetObj = DescribeSObjectParentObj.FieldSets.getMap().get(mapCustomData.get(obj).Fieldset_API__c);
        List<Schema.FieldSetMember> fieldSetFields = new List<Schema.FieldSetMember>();
        if(fieldSetObj != null){
            fieldSetFields = fieldSetObj.getFields();
        }
        listWrapper = new List<fieldWrapper>();
        if(!fieldSetFields.isEmpty()){
            showModal = true;
            for(Schema.FieldSetMember objMember : fieldSetFields){
                fieldWrapper newWrapperObj = new fieldWrapper(objMember.getLabel(),String.valueOf(objMember.getType()),objMember.getDbRequired(),objMember.getFieldPath());
                listWrapper.add(newWrapperObj);
            }
        }
        System.debug('cgeck wrapper-----    '+listWrapper);
        return null;
        
    }

    @RemoteAction
    global static string findSObjects(string obj, string qry ) 
    {
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) 
        {
            return null;
        }
        /* string qry = 'a'; */
        
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT Id,Name';
        
        
        
        /* Adding the object and filter by name to the soql */
        
        soql += ' from ' + obj + ' where name' + filter;
        
        
        
        soql += ' order by Name limit 10';
        
        system.debug('Qry: '+soql);
        
            
        
        List<sObject> L = new List<sObject>();
        
        
        try 
        {           
            L = Database.query(soql);
            system.debug('Qryvalue: '+L);
        }
        catch (QueryException e) 
        {
            system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        List<sObjectWrapper> wrapperList = new List<sObjectWrapper>();
        if(!L.isEmpty()){
            for(sObject sObj:L){
                sObjectWrapper wrapperObj = new sObjectWrapper(String.valueOf(sObj.get('Name')),String.valueOf(sObj.get('Id')));
                wrapperList.add(wrapperObj);
            }
        }
        
            String jsonbody = JSON.serialize(wrapperList).escapeXML();
            System.debug('json file ----->'+wrapperList);
            return jsonbody; 
        
        
   }

   public void save(){
       try{
           insert newSobject;
       }catch(Exception e){
           System.debug('Error save--->'+e.getMessage());
       }
   }
   public class sObjectWrapper{
       public String label{get;set;}
       public String id {get;set;}
        public sObjectWrapper(String label,String id){
            this.label = label;
            this.id = id;
        }
   }
   public class fieldWrapper{
       public String label{get;set;}
       public String type{get;set;}
       public Boolean required{get;set;}
       public String fieldAPI{get;set;}

       public fieldWrapper(String label,string type,Boolean required,String fieldAPI){
           this.label = label;
           this.type = type;
           this.required = required;
           this.fieldAPI = fieldAPI;
       }

   }
}