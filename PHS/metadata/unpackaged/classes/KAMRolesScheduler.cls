/*
*  Class Name:  KAMRolesScheduler
*  Description: This schedulable class executes KAMRolesBatch class on set time intervals
*  Company: dQuotient
*  CreatedDate: 03-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer          Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      03-Nov-2017            Orginal Version
*/
global class KAMRolesScheduler{
    /*
} implements Schedulable{
  global void execute(SchedulableContext sc){
        resetKeyAccounts();
    KAMRolesBatch b = new KAMRolesBatch();
    database.executebatch(b,200);
  }
    global void resetKeyAccounts()
    {
        Set<ID> keyAccounts = new Set<ID>();
        List<Account> childAccounts = new List<Account>();
        for(Account a : [Select Id,Test_Key_Account__c From account Where Test_Key_Account__c = TRUE]){
          
            a.Test_Key_Account__c=FALSE;
            a.Test_Key_Account_Role__c=NULL;
            a.Test_Key_Account_Type__c=NULL;
            childAccounts.add(a);
        }
        if(childAccounts.size() > 0)
            update childAccounts;

    }*/

}