/*
*  Class Name:  ClosedCasesForLeadControllerTest
*  Description: Test class for CasesForLeadController
*  Company: dQuotient
*  CreatedDate: 16-May-2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer          Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  David Kurtanjek    16-May-2018            Original Version
*/
@isTest
private class ClosedCasesForLeadControllerTest {
  static PriceBook2 priceBook;
    static List<Account> accountList;
    static List<Contact> contactList;
    static List<Lead> leadList;
  static List<PHS_Account__c> phsAccountList;
  static List<PHS_Account_Relationship__c> phsList;
  static List<Case> caseList;

  @isTest
  private static void createData(){
        accountList = new List<Account>();
        contactList = new List<Contact>();
    leadList = new List<Lead>();
    caseList = new List<Case>();
    phsList = new List<PHS_Account_Relationship__c>();
    phsAccountList = new List<PHS_Account__c>();
    // create custom setting Wildebeest_Integration__c
    Wildebeest_Integration__c wildBeest = bg_Test_Data_Utils.createCSWildebeestIntegration();
    insert wildBeest;
         
      
        //update standard priceBook
        priceBook = new PriceBook2(
            Id = Test.getStandardPricebookId(),
          Name = 'Standard Price Book',
            Description = 'Some text',
            isActive = TRUE
        );
        update priceBook;
    //Insert Leads
    for(Integer i=0;i<5;i++){
        Lead lead = bg_Test_Data_Utils.createLead(''+i);
        leadList.add(lead);
    }
    //following values will help improve test coverage by covering else/catch blocks
    leadList[0].Wildebeest_Ref__c = '';
    leadList[1].Wildebeest_Ref__c = '12345678';
    insert leadList;

    //Insert Accounts
    accountList = new List<Account>();
        //adding 10 + 2 records, to create multi level parent-child relationship
        for(Integer i=0;i<5;i++){
            Account account = bg_Test_Data_Utils.createAccount(''+i);
      accountList.add(account);
        }
        insert accountList;
        
        for(Integer i=0;i<5;i++){
            Contact con = bg_Test_Data_Utils.createContact('Contact'+i , accountList[i].Id);
      contactList.add(con);
        }
        insert contactList;
      
      //Create PHS account
      	PHS_Account__c phsacc = bg_Test_Data_Utils.createPHSAccount('12345678', accountList[1].Id);
      	insert phsacc; 

        //create Cases
    for(Integer i=0;i<5;i++){
        Case caseObj = bg_Test_Data_Utils.createCase('Case'+i, contactList[i].Id,accountList[1].Id); 
        caseObj.PHS_Account__c = phsacc.id;
        caseObj.Type = 'Contract Processing';
        caseObj.Reason = 'Bespoke mat contracts';
        caseObj.Case_Reason__c = 'Bespoke mat contracts';
        caseList.add(caseObj);
    }
    insert caseList;
      
      for(Case c: caseList){
          c.Status = 'Closed';
          
      }
      update caseList;
  }
  
  @isTest
  private static void coverController(){
        createData();
        Test.startTest();
    CasesForLeadController controller1 = new CasesForLeadController();
    //following four lines help cover catch block
        controller1.setLeadId(null);
    controller1.getLeadId();
        controller1.setLeadId('00Q4E0000099k37');
    controller1.getLeadId();
        
        controller1.setLeadId(leadList[0].Id);
    controller1.getLeadId();
    //since Wildebeest_Ref__c is blank, errorMessage should have content
    System.assertNotEquals(NULL, controller1.errorMessage);

        CasesForLeadController controller2 = new CasesForLeadController();
    controller2.setLeadId(leadList[1].Id);
    controller2.getLeadId();
    //since Wildebeest_Ref__c won't match any value in PHS_Account__c, errorMessage should have content
    System.assertNotEquals(NULL, controller2.errorMessage);

        CasesForLeadController controller3 = new CasesForLeadController();
    controller3.setLeadId(leadList[2].Id);
    controller3.getLeadId();
    //since Wildebeest_Ref__c will be present, errorMessage should be empty
    System.assertEquals('', controller3.errorMessage);
        Test.stopTest();
  }
}