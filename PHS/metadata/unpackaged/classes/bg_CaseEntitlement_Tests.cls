/**********************************************************************
* bg_CaseEntitlement_Tests:
*
* Tests for the Case Entitlements  
* Created By: BrightGen Ltd
* Created Date: 05/04/2016
*
* Changes: 
***********************************************************************/

@isTest
public class bg_CaseEntitlement_Tests {

	@testSetup static void setupDate() {
		//bg_RecordBuilder.generateDefaultGlobalSettings();
	}

	@isTest static void Given_ANewlyCreatedCase_When_AnAccountIsRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheAccount() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		System.debug('ID Of user--'+user.Id);
		Given_ANewlyCreatedCase_When_AnAccountIsRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheAccount_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_AnAccountIsNotRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheDefault() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AnAccountIsNotRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheDefault_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_AnAccountDoesntHaveAnEntitlement_Then_TheCaseEntitlementProcessIsPulledFromTheDefault() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AnAccountDoesntHaveAnEntitlement_Then_TheCaseEntitlementProcessIsPulledFromTheDefault_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_NoEntitlementsArePopulated_Then_TheCaseEntitlementIsNull() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_NoEntitlementsArePopulated_Then_TheCaseEntitlementIsNull_Runner(user);
	}


	public static void Given_ANewlyCreatedCase_When_AnAccountIsRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheAccount_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
        
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
        
   		Account accountRecord = new Account(Name = 'Test', BillingPostalcode = 'LS1 3DD');
    	insert accountRecord;

    	Entitlement entitlementRecord = new Entitlement(Name = 'Test', AccountId = accountRecord.Id);
    	insert entitlementRecord;


		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Entitlement_Id__c = entitlementRecord.Id);
		insert gs;

		Account acct = new Account(Name = 'Test1', BillingPostalcode = 'LS1 3DD');
		insert acct;

		Case caseRecord = new Case(Status = 'New', AccountId = acct.Id);
		insert caseRecord;
		

		Entitlement ent = [Select Id From Entitlement Where AccountId = :acct.Id];		
		test.stopTest();


		caseRecord = [Select Id, EntitlementId From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals(ent.Id, caseRecord.EntitlementId);

        }
	}


	public static void Given_ANewlyCreatedCase_When_AnAccountIsNotRelatedToTheCase_Then_TheCaseEntitlementProcessIsPulledFromTheDefault_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
        
   		Account accountRecord = new Account(Name = 'Test', BillingPostalcode = 'LS1 3DD');
    	insert accountRecord;

    	Entitlement entitlementRecord = new Entitlement(Name = 'Test', AccountId = accountRecord.Id);
    	insert entitlementRecord;


		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Entitlement_Id__c = entitlementRecord.Id);
		insert gs;

		Case caseRecord = new Case(Status = 'New');
		insert caseRecord;
		test.stopTest();

		caseRecord = [Select Id, EntitlementId From Case Where Id = :caseRecord.Id Limit 1];
		gs = [Select Id, Default_Entitlement_Id__c From Glogbal_Settings__c Where Id =:gs.Id Limit 1];
		System.assertEquals(gs.Default_Entitlement_Id__c, caseRecord.EntitlementId);

        }
	}



	public static void Given_ANewlyCreatedCase_When_AnAccountDoesntHaveAnEntitlement_Then_TheCaseEntitlementProcessIsPulledFromTheDefault_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
        
   		Account accountRecord = new Account(Name = 'Test', BillingPostalcode = 'LS1 3DD');
    	insert accountRecord;

    	Entitlement entitlementRecord = new Entitlement(Name = 'Test', AccountId = accountRecord.Id);
    	insert entitlementRecord;


		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Entitlement_Id__c = entitlementRecord.Id);
		insert gs;

		Account acct = new Account(Name = 'Test1', BillingPostalcode = 'LS1 3DD');
		insert acct;

		Entitlement ent = [Select Id From Entitlement Where AccountId = :acct.Id];
		delete ent;

		Case caseRecord = new Case(Status = 'New', AccountId = acct.Id);
		insert caseRecord;
		test.stopTest();

		caseRecord = [Select Id, EntitlementId From Case Where Id = :caseRecord.Id Limit 1];
		gs = [Select Id, Default_Entitlement_Id__c From Glogbal_Settings__c Where Id =:gs.Id Limit 1];
		System.assertEquals(gs.Default_Entitlement_Id__c, caseRecord.EntitlementId);

        }
	}


	public static void Given_ANewlyCreatedCase_When_NoEntitlementsArePopulated_Then_TheCaseEntitlementIsNull_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

        test.startTest();
        
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
        
        Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Entitlement_Id__c = null);
		insert gs;

		Case caseRecord = new Case(Status = 'New');
		insert caseRecord;
		test.stopTest();

		caseRecord = [Select Id, EntitlementId From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals(null, caseRecord.EntitlementId);

        }
	}
    
}