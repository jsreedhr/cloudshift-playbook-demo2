/*********************************************************************************
 * bg_PHSAccountSelectionUpdateController
 *
 * Controler class for the PHS account selection vf page.
 * Contains methods to build a list of PHS Accounts based on a parent account id,
 * and a method to pass the selected information over to the creation of a new case.
 *
 * Author: Jamie Wooley
 * Created: 06-05-2016
 *
 *********************************************************************************/

public with sharing class bg_PHSAccountSelectionUpdateController {
	public Case theCase {get; set;}
    public PageReference cancelURL {get; set;}

    public bg_PHSAccountSelectionUpdateController(ApexPages.StandardController stdController) {
        cancelURL = stdController.cancel();
        List<string> fields = new List<String>();
        if (!Test.isRunningTest())
        {
			fields.add('AccountId');
	        fields.add('PHS_Account__c');
            fields.add('ContactId');
	        stdController.addFields(fields);
        }
        theCase = (Case)stdController.getRecord();
        if (theCase.AccountId != null || theCase.PHS_Account__c != null)
        {
        	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, Label.PHS_Account_Select_First_Time);
            ApexPages.addMessage(myMsg);
        }
    }
}