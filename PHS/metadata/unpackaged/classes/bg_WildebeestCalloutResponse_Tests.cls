@isTest
global class bg_WildebeestCalloutResponse_Tests {
    
	global class MockHttpResponseGenerator implements HttpCalloutMock {
		  String rBody;
		  Integer rCode;

		  global MockHttpResponseGenerator(String rBody, Integer rCode) {
			   this.rBody = rBody;
			   this.rCode = rCode;
		  }

	     global HTTPResponse respond(HTTPRequest req) {

	        System.debug('mock req: ' + req);
	        
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(rBody);
	        res.setStatusCode(rCode);
	        return res;
	    }
	}


	@testSetup static void setup() {
      String res = '{"EntityClassRef":"Case","EntityId":null,"Message":"Unable to PHS Mapping of Case Classification for combination of Reason : A temporary contract is required and Type Account Administration","SalesforceId":"5004E0000011HgRQAU","Success":true}';
      MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
      Test.setMock(HttpCalloutMock.class, mock);

		  Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

      Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
      if(integrationSetting == null) {
          integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
          integrationSetting.Http_Callout_Endpoint__c = 'test';
          integrationSetting.Http_Callout_Query__c = 'test';
          insert integrationSetting;
      }


      Account act = new Account(Name = 'Test Account', Wildebeest_Ref__c = '01', BillingPostalcode = 'LS1 3DD');
      insert act;

      Case c = createCase();
      insert c;

	}

	static Case createCase()
  {
        Account act = new Account(Name = 'Test Account 1', Wildebeest_Ref__c = '01', BillingPostalcode = 'LS1 3DD');
        insert act;

        PHS_Account__c phsAct = new PHS_Account__c(Wildebeest_Ref__c = '01', Service_Enabled__c=true, Salesforce_Account__c = act.Id);
        insert phsAct;

        Case c = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, PHS_Account__c=phsAct.Id, Origin='01', Type='Cancellation', Case_Reason__c='01', Case_Ref__c = '1234', AccountID = act.Id);
        return c;
  }


    static testMethod void caseReferenceBlanking() {


    	User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);

    	System.runAs(user)
    	{


    		test.startTest();

	      String res = '{"EntityClassRef":"Case","EntityId":null,"Message":"Unable to PHS Mapping of Case Classification for combination of Reason : A temporary contract is required and Type Account Administration","SalesforceId":"5004E0000011HgRQAU","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
    		insert c;

    		c.Type = 'Account Administration';
	    	c.Case_Reason__c = 'A temporary contract is required';
	    	update c;
	    	System.debug('**** c.Case_Ref__c' + c.Case_Ref__c);

        test.stopTest();

        Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];
        System.debug('**** case ref: ' + cu.Case_Ref__c);
        System.debug('**** sync message: ' + cu.Sync_Wildebeest_Message__c);
			  System.assertNotEquals(' ', cu.Case_Ref__c);
	    	System.assertNotEquals(null, cu.Case_Ref__c);
	    	System.assertNotEquals(null, cu.Sync_Wildebeest_Message__c);

    	}
        
    }
}