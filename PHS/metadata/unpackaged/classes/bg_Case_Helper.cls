/*********************************************************************************
 * bg_Case_Helper
 *
 * A helper class for the case object to perform the following actions
 * -  Prevent closing a case if it has inflight tasks
 * -  Default the case priority
 * -  Default the case entitlement
 * -  Default the PHS account
 * -  Force assignments on email to case records
 * -  Populate key case milestone information on the case
 * 
 * Author: SA - BrightGen Ltd
 * Created: 23-03-2016
 * Updated: 21-12-2016 KH - Updated to add checkOriginatingEmployee and setOriginatingEmployee
 *          06-06-2017 KH - Updated completeMilestones to include new Closed status
 *          10-07-2017 KH - Updated defaultPHSAccount() and added assignPHSAccount()
 *                          as part of Case 00030968.
 * Updated: Brightgen - IB 09/03/2018 added statusDiarised and statusDiarised2 method
 *********************************************************************************/
public with sharing class bg_Case_Helper
{
    /*
        Method used to set the Orginating Employee On the Case (before insert). The employee number on the Case
        is used to get the associated User record.
    */
    public static void setOriginatingEmployee(List<Case> caseRecords)
    {
        Set<String> employeeNumbers = new Set<String>();

        for(Case caseRecord : caseRecords)
        {
            if (caseRecord.Originating_Employee__c != null)
            {
                employeeNumbers.add(caseRecord.Originating_Employee__c);
            }
        }

        if (!employeeNumbers.isEmpty())
        {
            Map<String, User> employeeNumberUserMap =  bg_UserHelper.getEmployeeUserMap(employeeNumbers);

            for (Case caseRecord : caseRecords)
            {
                if (employeeNumberUserMap.containsKey(caseRecord.Originating_Employee__c))
                {
                    caseRecord.Originating_Employee_User__c = employeeNumberUserMap.get(caseRecord.Originating_Employee__c).Id;
                    caseRecord.Originating_Employee_Role__c = employeeNumberUserMap.get(caseRecord.Originating_Employee__c).UserRole.Name;
                }
            }
        }
    }

    /*
        Method used to set the Orginating Employee On the Case (before update). The employee number on the Case
        is used to get the associated User record. A check is made to see if the Employee Number on the Case has changed.
    */
    public static void checkOriginatingEmployee(List<Case> caseRecords, Map<Id, Case> oldCaseRecords)
    {
        List<Case> casesToProcess = new List<Case>();

        for (Case caseRecord : caseRecords)
        {
            Case oldCaseRecord = oldCaseRecords.get(caseRecord.Id);

            if (caseRecord.Originating_Employee__c != null && caseRecord.Originating_Employee__c != oldCaseRecord.Originating_Employee__c)
            {
                casesToProcess.add(caseRecord);
            }
        }

        if (!casesToProcess.isEmpty())
        {
            setOriginatingEmployee(casesToProcess);
        }
    }

    /*
     * Method to be used before insert to prevent cases being closed with inflight tasks or events
     */
    public static void validateClosingACase(List<Case> caseRecords) {
    	Map<Id, Case> caseRecordsWithActivities = new Map<Id, Case>([Select Id, 
    																		(Select Id From Tasks  Where IsClosed = false ),
    																		(Select Id From Events  Where Event_isActive__c = true )
    																		From Case Where Id IN : caseRecords 
    																		AND Case_Record_isActive__c = true]);
    	for(Case caseRecord : caseRecords)
    	{
    		Boolean isClosed = caseRecord.Status.Contains(bg_Constants.CLOSED_CASE_STATUS);
    		Boolean caseFound = caseRecordsWithActivities != null && caseRecordsWithActivities.containsKey(caseRecord.Id);
    		Boolean hasInflightActivities = caseFound && 
                                            ((caseRecordsWithActivities.get(caseRecord.Id).Tasks != null && caseRecordsWithActivities.get(caseRecord.Id).Tasks.size() > 0) || 
                                            (caseRecordsWithActivities.get(caseRecord.Id).Events != null && caseRecordsWithActivities.get(caseRecord.Id).Events.size() > 0));
            
            if(isClosed && hasInflightActivities)
    		{
    			caseRecord.addError(Label.Close_Case_Error);
    		}
    	}
    }

    /*
     * Method to be used before update to prevent cases being closed with inflight tasks or events
     */
    public static void validateClosingACase(List<Case> caseRecords, Map<Id, Case> oldCaseRecords) {
    	List<Case> caseRecordsToVerify = new List<Case>();

    	for(Case caseRecord : caseRecords)
    	{
    		Boolean isChangedToClosed = caseRecord.Status.Contains(bg_Constants.CLOSED_CASE_STATUS) && !oldCaseRecords.get(caseRecord.Id).Status.Contains(bg_Constants.CLOSED_CASE_STATUS);

    		if(isChangedToClosed)
    		{
    			caseRecordsToVerify.add(caseRecord);
    		}
    	}

    	if(!caseRecordsToVerify.isEmpty())
    	{
    		validateClosingACase(caseRecordsToVerify);
    	}
    }

    /*
     * A method to run before insert to default the case priority to the priority on the account if one isn't specified.
     */
    public static void populateCasePriority(List<Case> caseRecords) {
        /*
         * Get a set of account Ids
         */
        List<Case> caseRecordToDefault = new List<Case>();
        Set<Id> accountIds = new Set<Id>();
        for(Case caseRecord : caseRecords)
        {
            Boolean casePriorityIsSet = caseRecord.Priority__c != null;

            if(!casePriorityIsSet)
            {
                caseRecordToDefault.add(caseRecord);
                accountIds.add(caseRecord.AccountId);
            }
        }

        /*
         * Generate a map of accounts that relate to the cases without priorities.
         */
        Map<Id, Account> accountRecordsMap = new Map<Id, Account>();
        if(!accountIds.isEmpty())
        {
            accountRecordsMap = new Map<Id, Account>([Select Id, Default_Case_Priority__c From Account Where Id IN : accountIds]);
        }

        /*
         * Update the priority to the the value on the account or default it.
         */
        for(Case caseRecord : caseRecordToDefault)
        {
            Boolean hasAccountRecord = accountRecordsMap.containsKey(caseRecord.AccountId);
            
            if(hasAccountRecord)
            {
                caseRecord.Priority__c = accountRecordsMap.get(caseRecord.AccountId).Default_Case_Priority__c;
            }
            else
            {
                Glogbal_Settings__c globalSettings = Glogbal_Settings__c.getInstance(UserInfo.getUserId());
                Boolean globalSettingPriorityHasBeenPopulated = globalSettings != null && globalSettings.Default_Case_Priority__c != null;
                if(globalSettingPriorityHasBeenPopulated)
                {
                    caseRecord.Priority__c = globalSettings.Default_Case_Priority__c;
                }
            }
        }
    }

    /*
     * A method to run before insert to populate the default entitlement on the case
     */
    public static void setCaseDefaultEntitlement(List<Case> caseRecords) {
        /*
         * Get default entitlement
         */
        Glogbal_Settings__c globalSettings = Glogbal_Settings__c.getInstance(UserInfo.getUserId());
        Boolean globalSettingPriorityHasBeenPopulated = globalSettings != null && globalSettings.Default_Entitlement_Id__c != null;
        ID QCRECORDTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Quick Case').getRecordTypeId();

        /*
         * Get the account id from the case or default the entitlement
         */
        Set<Id> accountIds = new Set<Id>();
        List<Case> caseRecordsToFurtherProcess = new List<Case>();
        for(Case caseRecord : caseRecords)
        {
            Boolean caseRecordHasAnAccount = caseRecord.AccountId != null;
            Boolean caseIsNotAQuickCase = caseRecord.RecordTypeId != QCRECORDTYPEID;

            if(caseRecordHasAnAccount && caseIsNotAQuickCase)
            {
                accountIds.add(caseRecord.AccountId);
                caseRecordsToFurtherProcess.add(caseRecord);
            }
            else if(globalSettingPriorityHasBeenPopulated && caseIsNotAQuickCase)
            {
                caseRecord.EntitlementId = globalSettings.Default_Entitlement_Id__c;
            }
        }

        /*
         * Get the default entitlment from the account or default it.
         */
        Map<id, Account> accountRecordsMap = new Map<Id, Account>();
        if(!accountIds.isEmpty())
        {
            accountRecordsMap = new Map<Id, Account>([Select Id, (Select Id From Entitlements Limit 1) From Account Where Id IN : accountIds]);
        }

        for(Case caseRecord : caseRecordsToFurtherProcess)
        {
            Boolean accountHasAnEntitlement = accountRecordsMap.containsKey(caseRecord.AccountId) && accountRecordsMap.get(caseRecord.AccountId).Entitlements.size() == 1;

            if(accountHasAnEntitlement)
            {
                caseRecord.EntitlementId = accountRecordsMap.get(caseRecord.AccountId).Entitlements[0].Id;
            }
            else if(globalSettingPriorityHasBeenPopulated)
            {
                caseRecord.EntitlementId = globalSettings.Default_Entitlement_Id__c;
            }
        }
    }

    /*
     * Run this method before insert to default the PHS account on the case if the account only has one child record.
     */
    public static void defaultPHSAccount(List<Case> caseRecords) {
        /*
         * Get a list of account ids and check if a PHS Account has been specified.
         */
        Set<Case> caseRecordsToUpdate = new Set<Case>();
        Set<Id> accountIds = new Set<Id>();
        for(Case caseRecord : caseRecords)
        {
            Boolean phsAccountHasBeenSpecified = caseRecord.PHS_Account__c != null;
            Boolean accountIdHasBeenSpecified = caseRecord.AccountId != null;

            if(!phsAccountHasBeenSpecified && accountIdHasBeenSpecified)
            {
                caseRecordsToUpdate.add(caseRecord);
                accountIds.add(caseRecord.AccountId);
            }
        }

        /*
         * Loop through the accounts and default the PHS account if there is only one
         */
        Map<Id, Account> accountIdToAccountMap = new Map<Id, Account>([Select Id, (Select Id From PHS_Accounts__r Limit 2) From Account Where Id = :accountIds]);
        for(Case caseRecord : caseRecordsToUpdate)
        {
            Boolean caseHasAMatchingAccount = accountIdToAccountMap != null && accountIdToAccountMap.containsKey(caseRecord.AccountId);
            Boolean accountOnlyHasOnePHSAccount = caseHasAMatchingAccount && accountIdToAccountMap.get(caseRecord.AccountId).PHS_Accounts__r.size() == 1;
            if(accountOnlyHasOnePHSAccount)
            {
                caseRecord.PHS_Account__c = accountIdToAccountMap.get(caseRecord.AccountId).PHS_Accounts__r[0].Id;
            }
        }
    }

    /*
        Method to run on the Before Insert of a Case. It uses the Wildebeest Ref on the Case (populated by external process)
        and then looks for a PHS Account within Salesforce with that same Ref (it assumes it's always 1:1) and it then also
        assigns the Parent Salesforce Account of the PHS Account to the Case. Cases which have been through this process do not
        need to go through defaultPHSAccount().
    */
    public static void assignPHSAccount(List<Case> caserecords)
    {
        Set<Case> caseRecordsToUpdate = new Set<Case>();
        Set<String> phsWildebeestRefs = new Set<String>();
        Map<String, PHS_Account__c> phsAccountRefMap = new Map<String, PHS_Account__c>();

        for (Case caseRecord : caseRecords)
        {
            if (caseRecord.Wildebeest_Ref__c != null && caseRecord.Wildebeest_Ref__c != '')
            {
                caseRecordsToUpdate.add(caseRecord);
                phsWildebeestRefs.add(caseRecord.Wildebeest_Ref__c);
            }
        }

        if (!phsWildebeestRefs.isEmpty())
        {
            List<PHS_Account__c> phsAccounts = new List<PHS_Account__c>([SELECT Id, Wildebeest_Ref__c, Salesforce_Account__c FROM PHS_Account__c WHERE Wildebeest_Ref__c IN : phsWildebeestRefs]);
            for (PHS_Account__c phsAcc : phsAccounts)
            {
                phsAccountRefMap.put(phsAcc.Wildebeest_Ref__c, phsAcc);
            }
        }

        if (!phsAccountRefMap.isEmpty())
        {
            for (Case caseRecord : caseRecordsToUpdate)
            {
                PHS_Account__c phsAcc = phsAccountRefMap.get(caseRecord.Wildebeest_Ref__c);
                caseRecord.AccountId = phsAcc.Salesforce_Account__c;
                caseRecord.PHS_Account__c = phsAcc.Id;
            }
        }
    }
       

    /*
     * Run this method before update to default the PHS account on the case if the account only has one child record.
     */
    public static void defaultPHSAccount(List<Case> caseRecords, Map<Id, Case> oldCaseRecords) {
        /*
         * Check if any of the case fields have changed to prompt the PHS account update
         */
        List<Case> caseRecordsToUpdate = new List<Case>();
        for(Case caseRecord : caseRecords)
        {
            Boolean accountRecordHasChanged = caseRecord.AccountId != null && caseRecord.AccountId != oldCaseRecords.get(caseRecord.Id).AccountId;
            Boolean doesntHavePHSAccount = caseRecord.PHS_Account__c == null;
            
            if(accountRecordHasChanged && doesntHavePHSAccount)
            {
                caseRecordsToUpdate.add(caseRecord);
            }
        }

        if(!caseRecordsToUpdate.isEmpty())
        {
            defaultPHSAccount(caseRecordsToUpdate);
        }
    }

    /*
     * Run this method adter insert to force the assignments to run on an email to case record.
     */
    public static void forceAssignmentsForEmailToCaseRecords(List<Case> caseRecords) {
        List<Case> caseRecordsToUpdate = new List<Case>();

        /*
         * Get a set of email to case origins
         */
        Glogbal_Settings__c globalSettings = Glogbal_Settings__c.getInstance(UserInfo.getUserId());
        Boolean globalSettingsAreValid = globalSettings != null && globalSettings.Email_To_Case_Origin__c != null;
        Set<String> emailToCaseOrigins = globalSettingsAreValid ? new Set<String>(globalSettings.Email_To_Case_Origin__c.split(';')) : new Set<String>();

        for(Case caseRecord : caseRecords)
        {
            Boolean caseHasComeFromEmailToCase = emailToCaseOrigins.contains(caseRecord.Origin);
            
            if(caseHasComeFromEmailToCase)
            {
                caseRecordsToUpdate.add(caseRecord.clone(true, true, true, true));
            }
        }

        /*
         * Update the case using the default assignment rule in the dml options
         */
        if(!caseRecordsToUpdate.isEmpty())
        {
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.assignmentRuleHeader.useDefaultRule = true;
            Database.update(caseRecordsToUpdate, dml);
        }
    }

    private static Boolean shouldUpdateCheckBeRun = true; 
    
    public static Map<Id, Case> caseMilestoneDataUpdate(List<Case> caseRecords)
    {
        List<Id> caseIdList = new List<Id>();
        for(Case c : caseRecords) {
            caseIdList.add(c.Id);
        }

        Boolean caseRecordsPresent = !caseIdList.isEmpty();

        /*
         * Populate the milestone fields on the case
         */
        if(caseRecordsPresent)
        {
            return caseMilestoneDataUpdate(caseIdList); 
        } 

        return null;
    }
   
   
    public static Map<Id, Case> caseMilestoneDataUpdate(List<Id> caseIds)
    {
        List<CaseMilestone> CaseMilestoneRecords = new List<CaseMilestone>([Select Id, MilestoneType.Name, CaseId, TargetResponseInMins, TargetDate, IsCompleted, CompletionDate From CaseMilestone Where CaseId In :caseIds]); 
        
        /*
         * Generate a map of case id to a list of milestone records
         */
        Map<Id, List<CaseMilestone>> IdToCaseMilestoneMap = new Map<Id, List<CaseMilestone>>(); 
        for(CaseMilestone CaseMilestoneRecord : CaseMilestoneRecords)
        {
            if(IdToCaseMilestoneMap.containsKey(CaseMilestoneRecord.CaseId))
            {
                List<CaseMilestone> tmp = IdToCaseMilestoneMap.get(CaseMilestoneRecord.CaseId); 
                tmp.add(CaseMilestoneRecord); 
                IdToCaseMilestoneMap.put(CaseMilestoneRecord.CaseId, tmp); 
            }
            else
            { 
                List<CaseMilestone> tmp = new List<CaseMilestone>(); 
                tmp.add(CaseMilestoneRecord); 
                IdToCaseMilestoneMap.put(CaseMilestoneRecord.CaseId, tmp); 
            } 
        } 
     
        /* 
         * Update the case records with the correct values 
        */ 
        List<Case> caseRecordsList = [Select Status, First_Response_Target_Date__c, Case_Resolution_Target_Date__c, Second_Response_Target_Date__c, IsStopped, 
                                      First_Response_Completed_DateTime__c, Second_Response_Completed_DateTime__c, Case_Resolution_Completed_DateTime__c,
                                      TargetResponseInMins__c From Case Where Id In :caseIds]; 
     
        Map<Id, Case> caseRecordsToUpdateMap = new Map<Id, Case>(); 


        for(Case caseRecord : caseRecordsList)
        { 
            Boolean hasBeenUpdated = false; 
            
            if(IdToCaseMilestoneMap.containsKey(caseRecord.Id) && caseRecord.IsStopped == false && caseRecord.Status != bg_Constants.CLOSED_CASE_STATUS)
            { 
                /*
                 * Loop through each of the milestones and populate the respective dates.
                 */

                for(CaseMilestone caseMilestoneRecord : IdToCaseMilestoneMap.get(caseRecord.Id))
                { 
                    if(caseMilestoneRecord.MilestoneType.Name == 'Resolution') 
                    {
                        DateTime resolutionTargetDate = caseMilestoneRecord.IsCompleted == false ? caseMilestoneRecord.TargetDate: null;
                        DateTime resolutionCompletedDate = caseMilestoneRecord.IsCompleted == true ? caseMilestoneRecord.CompletionDate: null;

                        Boolean valueNeedsToChange = caseRecord.Case_Resolution_Target_Date__c != resolutionTargetDate ||
                                                     caseRecord.Case_Resolution_Completed_DateTime__c != resolutionCompletedDate;


                        if(valueNeedsToChange)
                        {
                            caseRecord.Case_Resolution_Target_Date__c = resolutionTargetDate; 
                            caseRecord.Case_Resolution_Completed_DateTime__c = resolutionCompletedDate; 
                            caseRecord.TargetResponseInMins__c = caseMilestoneRecord.TargetResponseInMins; 
                            hasBeenUpdated = true;
                        }
                        
                    }
                    else if(caseMilestoneRecord.MilestoneType.Name == 'First Response')
                    { 
                        DateTime firstResponseTargetDate = caseMilestoneRecord.IsCompleted == false ? caseMilestoneRecord.TargetDate: null;
                        DateTime firstResponseCompletedDate = caseMilestoneRecord.IsCompleted == true ? caseMilestoneRecord.CompletionDate: null;

                        Boolean valueNeedsToChange = caseRecord.First_Response_Target_Date__c != firstResponseTargetDate ||
                                                     caseRecord.First_Response_Completed_DateTime__c != firstResponseCompletedDate;


                        if(valueNeedsToChange)
                        {
                            caseRecord.First_Response_Target_Date__c = caseMilestoneRecord.IsCompleted == false ? caseMilestoneRecord.TargetDate : null; 
                            caseRecord.First_Response_Completed_DateTime__c = caseMilestoneRecord.IsCompleted == true ? caseMilestoneRecord.CompletionDate : null; 
                            caseRecord.TargetResponseInMins__c = caseMilestoneRecord.TargetResponseInMins; 
                            hasBeenUpdated = true; 
                        }
                    } 
                    else if(caseMilestoneRecord.MilestoneType.Name == 'Second Response')
                    { 
                        DateTime secondResponseTargetDate = caseMilestoneRecord.IsCompleted == false ? caseMilestoneRecord.TargetDate: null;
                        DateTime secondResponseCompletedDate = caseMilestoneRecord.IsCompleted == true ? caseMilestoneRecord.CompletionDate: null;

                        Boolean valueNeedsToChange = caseRecord.Second_Response_Target_Date__c != secondResponseTargetDate ||
                                                     caseRecord.Second_Response_Completed_DateTime__c != secondResponseCompletedDate;


                        if(valueNeedsToChange)
                        {
                            caseRecord.Second_Response_Target_Date__c = caseMilestoneRecord.IsCompleted == false ? caseMilestoneRecord.TargetDate : null; 
                            caseRecord.Second_Response_Completed_DateTime__c = caseMilestoneRecord.IsCompleted == true ? caseMilestoneRecord.CompletionDate : null; 
                            caseRecord.TargetResponseInMins__c = caseMilestoneRecord.TargetResponseInMins; 
                            hasBeenUpdated = true;
                        } 
                    } 
                } 
             
            }
            else
            { 
                                /* 
                 * Make the end date null if no entitlement is attached to the case 
                 */ 
                if(caseRecord.First_Response_Target_Date__c != null || caseRecord.Case_Resolution_Target_Date__c != null || caseRecord.Second_Response_Target_Date__c != null)
                { 
                    caseRecord.First_Response_Target_Date__c = null; 
                    caseRecord.Case_Resolution_Target_Date__c = null;
                    caseRecord.Second_Response_Target_Date__c = null; 
                    hasBeenUpdated = true; 
                } 
            } 

            if(hasBeenUpdated)
            { 
                caseRecordsToUpdateMap.put(caseRecord.Id, caseRecord); 
            } 
        }

        return caseRecordsToUpdateMap;
    }

    /*
        Method to complete all outstanding Milestones related to the Case if it was closed
    */
    public static void completeMilestones(List<Case> caseRecords, Map<Id, Case> oldCaseRecords)
    {
        Set<Id> caseFirstResponseToComplete = new Set<Id>();
        Set<Id> caseSecondResponseToComplete = new Set<Id>();
        Set<Id> caseResolutionResponseToComplete = new Set<Id>();

        for(Case caseRecord : caseRecords)
        {   
            Boolean resolution = (caseRecord.Status == bg_Constants.CLOSED_CASE_STATUS || caseRecord.Status == 'Closed (Raised in error)') && (oldCaseRecords.get(caseRecord.Id).Status != bg_Constants.CLOSED_CASE_STATUS || oldCaseRecords.get(caseRecord.Id).Status != 'Closed (Raised in error)');
            
            if(resolution)
            {
                caseResolutionResponseToComplete.add(caseRecord.Id);
            }
        }

        Set<Id> allCaseIds = caseResolutionResponseToComplete;
        List<CaseMilestone> caseMilestoneToUpdate = new List<CaseMilestone>();
        for(CaseMilestone caseMilestone : [SELECT Id, completionDate, MilestoneType.Name, CaseId FROM CaseMilestone WHERE caseId IN :allCaseIds and completionDate = null])
        {
            Boolean isFirstResponse = caseMilestone.MilestoneType.Name == 'First Response';
            Boolean isSecondResponse = caseMilestone.MilestoneType.Name == 'Second Response';
            Boolean isResolution = caseMilestone.MilestoneType.Name == 'Resolution';
            if(isFirstResponse || isSecondResponse || isResolution)
            {
                caseMilestone.CompletionDate = datetime.now();
                caseMilestoneToUpdate.add(caseMilestone);
            }
        }

        if(!caseMilestoneToUpdate.isEmpty())
        {
            update caseMilestoneToUpdate;
        }
    }

    public static void statusDiarised(List<Case> caseList){

        List<Case> toUpdate = new List<Case>();

        List<Case> cases = [SELECT Id, Status, Diarised_Date_Time__c FROM Case WHERE Id IN: caseList];

        for(Case c : cases)
        {
            if(c.Status != 'Diarised' && c.Diarised_Date_Time__c != null)
            {
                c.Status = 'Diarised';
                toUpdate.add(c);
            }

        }

        update toUpdate;

    }

    public static void statusDiarised2(List<Case> caseList, Map<Id,Case>oldRecords){

        List<Case> toUpdate = new List<Case>();
        
        for(Case c : caseList)
        {
            Case oldCase = oldRecords.get(c.Id);
            Boolean change = c.Diarised_Date_Time__c != oldCase.Diarised_Date_Time__c;

            if(oldCase.Diarised_Date_Time__c == null && change && c.Status != 'Diarised' )
            {
                c.Status = 'Diarised';
            }

        }

    }
}