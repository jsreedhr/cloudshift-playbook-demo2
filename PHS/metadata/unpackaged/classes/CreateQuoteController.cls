/**
 *  @Class Name:    CreateQuoteController
 *  @Description:   This is a controller for CreateQuotePage
 *  @Company:       dQuotient
 *  CreatedDate:    06/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Hormese                           				Original Version
 */
public class CreateQuoteController {
    public Opportunity opp{get;set;}
    public string quotename{get;set;}
    public string expirydate{get;set;}
    public ID quoterecordid{get;set;}
   
    public  CreateQuoteController()
    {
        system.debug('UiTheme=='+UserInfo.getUiTheme());
        String oppId=ApexPages.currentPage().getParameters().get('oppId');
         opp= [SELECT Product_Count__c ,Account.Postal_District_Cut__c, Account.Name FROM Opportunity WHERE id =: oppId];
         
       
        //quotename= opp.Account.Postal_District_Cut__c+ '- ' + opp.Account.Name + ' - ' + String.valueOf(Date.today().day()) + '/' + Date.today().month() + '/' + Date.today().year();
        quotename= opp.Account.Postal_District_Cut__c+ '- ' + string.escapeSingleQuotes(opp.Account.Name) + ' - ' + String.valueOf(Date.today().day()) + '/' + Date.today().month() + '/' + Date.today().year();
        Date expirydateVar=Date.today().adddays(30);
       expirydate= String.valueOf(expirydateVar.day()) + '/' + expirydateVar.month() + '/' + expirydateVar.year();
       quoterecordid = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Draft').getRecordTypeId();
    }
    


}