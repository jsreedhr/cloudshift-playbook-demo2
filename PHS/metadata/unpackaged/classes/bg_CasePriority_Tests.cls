/**********************************************************************
* bg_CasePriority_Tests:
*
* Tests for the Case Priority.  
* Created By: BrightGen Ltd
* Created Date: 05/04/2016
*
* Changes: 
***********************************************************************/

@isTest
public class bg_CasePriority_Tests {

	@testSetup static void setupDate() {
		//bg_RecordBuilder.generateDefaultGlobalSettings();
	}

	@isTest static void Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithARelatedAccount_Then_ThePrioityShouldBeSetToTheAccountPriority() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithARelatedAccount_Then_ThePrioityShouldBeSetToTheAccountPriority_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithoutARelatedAccount_Then_ThePrioityShouldBeSetToTheDefaultPriority() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithoutARelatedAccount_Then_ThePrioityShouldBeSetToTheDefaultPriority_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_AUserInputsAPriorityWithARelatedAccount_Then_ThePrioityShouldNotTakeTheAccountPriority() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AUserInputsAPriorityWithARelatedAccount_Then_ThePrioityShouldNotTakeTheAccountPriority_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_ThePriorityIsNullOnAllPriorityFields_Then_ThePrioityShouldBeNull() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_ThePriorityIsNullOnAllPriorityFields_Then_ThePrioityShouldBeNull_Runner(user);
	}

	@isTest static void Given_ANewlyCreatedCase_When_AUserIputsPriorityAndAGlobalSettingOfNullWithoutAnAccount_Then_PrioityShouldBeTheInpuValue() {
		User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
		Given_ANewlyCreatedCase_When_AUserIputsPriorityAndAGlobalSettingOfNullWithoutAnAccount_Then_PrioityShouldBeTheInpuValue_Runner(user);
	}


	public static void Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithARelatedAccount_Then_ThePrioityShouldBeSetToTheAccountPriority_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Case_Priority__c = '1');
		insert gs;

		Account acct = new Account(Name = 'Test1', Default_Case_Priority__c  = '4', BillingPostalcode = 'LS1 3DD');
		insert acct;

		Case caseRecord = new Case(Status = 'New', AccountId = acct.Id);
		insert caseRecord;

		test.stopTest();

        caseRecord = [Select Id, Priority__c From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals('4', caseRecord.Priority__c, 'The Priority was not pulled from the Account correctly');

        }
	}


	public static void Given_ANewlyCreatedCase_When_AUserLeavesThePriorityBlankWithoutARelatedAccount_Then_ThePrioityShouldBeSetToTheDefaultPriority_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Case_Priority__c = '1');
		insert gs;

		Case caseRecord = new Case(Status = 'New');
		insert caseRecord;

		test.stopTest();

		caseRecord = [Select Id, Priority__c From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals('1', caseRecord.Priority__c, 'The Priority was not populated with the default custom setting.');

        }
	}


	public static void Given_ANewlyCreatedCase_When_AUserInputsAPriorityWithARelatedAccount_Then_ThePrioityShouldNotTakeTheAccountPriority_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Case_Priority__c = '1');
		insert gs;

		Account acct = new Account(Name = 'Test1', Default_Case_Priority__c  = '4', BillingPostalcode = 'LS1 3DD');
		insert acct;

		Case caseRecord = new Case(Status = 'New', AccountId = acct.Id, Priority__c = '3');
		insert caseRecord;

		test.stopTest();

        caseRecord = [Select Id, Priority__c From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals('3', caseRecord.Priority__c, 'The Priority did not populate correctly');

        }
	}
	

	public static void Given_ANewlyCreatedCase_When_ThePriorityIsNullOnAllPriorityFields_Then_ThePrioityShouldBeNull_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Case_Priority__c = null);
		insert gs;

		Account acct = new Account(Name = 'Test1', Default_Case_Priority__c  = null, BillingPostalcode = 'LS1 3DD');
		insert acct;

		Case caseRecord = new Case(Status = 'New', AccountId = acct.Id, Priority__c = null);
		insert caseRecord;

		test.stopTest();

        caseRecord = [Select Id, Priority__c From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals(null, caseRecord.Priority__c, 'The Priority was not null.');

        }
	}


	public static void Given_ANewlyCreatedCase_When_AUserIputsPriorityAndAGlobalSettingOfNullWithoutAnAccount_Then_PrioityShouldBeTheInpuValue_Runner(User userToRunAs) {
        System.runAs(userToRunAs)
        {

		test.startTest();
		Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }
		Glogbal_Settings__c gs = new Glogbal_Settings__c(Default_Case_Priority__c = null);
		insert gs;

		Case caseRecord = new Case(Status = 'New', Priority__c = '5');
		insert caseRecord;

		test.stopTest();

		caseRecord = [Select Id, Priority__c From Case Where Id = :caseRecord.Id Limit 1];
		System.assertEquals('5', caseRecord.Priority__c, 'The Priority was not the input value.');

        }
	}
}