public class QuoteTriggerHandler {
    @TestVisible static Boolean isInSalesforce1 = UserInfo.getUiThemeDisplayed() == 'Theme4t';
    public static void UpdatePriceBook(List<Quote> quotes)
    {
        set<id> oId = new set<id>();
        for(Quote q : quotes)
        {
            oId.add(q.opportunityid);
            for (OpportunityLineItem olt: [select id ,opportunity.Pricebook2Id, PricebookEntryId,opportunityid ,Product2.name,Product2Id,Quantity,UnitPrice,Description from OpportunityLineItem where opportunityid in : oId])
            {
               
               
               System.debug('Test Theme from Sales console:::::'+UserInfo.getUiTheme());
                 System.debug('Test Theme from Sales console-----'+UserInfo.getUiThemeDisplayed());
                if(UserInfo.getUiTheme()=='Theme4d' || isInSalesforce1 || UserInfo.getUiTheme()=='Theme4u' ){
                    System.debug('DEADLOCK');
                    
               q.Pricebook2Id   = olt.opportunity.Pricebook2Id;
                System.debug('DEADLOCK AFTER');
                }
            }
            
            for(Opportunity opp: [Select id ,accountid  from opportunity  where id in :oId])
                            {
                for(Contact c : [Select name,email ,phone,AccountId,account.name, MailingCity,MailingCountry,MailingPostalCode,MailingStreet,MailingState from Contact where AccountId = :opp.accountid])
                {
                    q.Contactid = c.id;
                    q.Email = c.Email;
                    q.BillingName = c.account.name;
                    q.ShippingName = c.account.name;
                    if(c.Phone!=NULL && c.Phone!='')
                        q.Phone = c.Phone;
                    if(c.MailingCity!=NULL && c.MailingCity!='')
                    q.BillingCity = c.MailingCity;
                    if(c.MailingCountry!=NULL && c.MailingCountry!='')
                        q.BillingCountry = c.MailingCountry;
                    if(c.MailingPostalCode!=NULL && c.MailingPostalCode!='')
                        q.BillingPostalCode = c.MailingPostalCode;
                    if(c.MailingStreet!=NULL && c.MailingStreet!='')
                        q.BillingStreet= c.MailingStreet;
                    if(c.MailingState!=NULL && c.MailingState!='')
                        q.BillingState= c.MailingState; 
                }
            }
            
        }
    }
    public static void UpdateQuoteLineItems(List<Quote> quotes)
    {
        set<id> oId = new set<id>();
        List<QuoteLineItem> qlist = new List<QuoteLineItem>();
        for(Quote q : quotes)
        {
            oId.add(q.opportunityid);
            for (OpportunityLineItem olt: [select id , PricebookEntryId,Quantity__c  ,opportunityid ,Product2.name,Product2Id,Quantity,UnitPrice,Description from OpportunityLineItem where opportunityid in : oId])
            {
             
                if(UserInfo.getUiTheme()=='Theme4d' || isInSalesforce1 || UserInfo.getUiTheme()=='Theme4u')
                {
                    QuoteLineItem ql = new QuoteLineItem();
                    ql.QuoteId=q.Id;
                    ql.Product2 = olt.Product2;
                    if(olt.Quantity != null)
                        ql.Quantity= olt.Quantity;
                    if(olt.Quantity__c != null)
                        ql.Quantity__c = olt.Quantity__c;
                    if(olt.UnitPrice!=null)
                        ql.UnitPrice = olt.UnitPrice;
                    if(olt.Description!=null && olt.Description=='')
                        ql.Description = olt.Description;
                    ql.Product2Id = olt.Product2Id;
                   ql.PricebookEntryId   = olt.PricebookEntryId;
                    qlist.add(ql);
                }
            }
            insert qlist;
            System.debug(qlist);
            
            
        }
    }
}