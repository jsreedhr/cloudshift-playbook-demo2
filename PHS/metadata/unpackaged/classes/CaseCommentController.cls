/**
 * Created by   : jaredwatson on 22/08/2018.
 * Purpose      : Return an ordered list of CaseComment objects to the requesting page
 *
 * *******************************************************************
 * CHANGE HISTORY
 *
 * USER                 DATE        US-REF      CHANGE DETAIL
 * -------------------------------------------------------------------
 * Jared Watson         22/08/2018  REQ-0558    Initial Version
 */

public with sharing class CaseCommentController {

    public Case oCase {get;set;}
    public List<CaseComment> lstDirectCaseComments {get;set;}
    public List<CaseComment> lstChildCaseComments {get;set;}


    //constructor
    public CaseCommentController(ApexPages.StandardController con){
        oCase = (Case)con.getRecord();
        //fetch comments for the current case as direct
        lstDirectCaseComments = getCaseComments(new List<ID>{oCase.Id});

        //fetch comments for the child cases this case is direct parent of
        Map<Id,Case> mChildCases =  new Map<Id,Case>([SELECT id from Case where ParentId = :oCase.Id OR Parent_Case__c = :oCase.Id]);
        lstChildCaseComments = getCaseComments(new List<Id>(mChildCases.keySet()));

    }

    private List<CaseComment> getCaseComments(List<Id> caseIds){

        List<CaseComment> results = new List<CaseComment>();

        results = [SELECT id, ParentId, Parent.CaseNumber, CreatedDate, CreatedBy.Name,  CommentBody, IsPublished
                        FROM CaseComment
                        WHERE ParentId in :caseIds
                        ORDER BY CreatedDate DESC];

        return results;
    }

}