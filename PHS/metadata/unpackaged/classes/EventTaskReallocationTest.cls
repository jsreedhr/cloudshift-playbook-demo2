/**
*  @Class Name:    EventTaskReallocationTest
*  @Description:   This is a test class for EventTaskReallocation
*  @Company:       dQuotient
*  CreatedDate:    26/12/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Swetha TG                                   Original Version
*/
@isTest
private class EventTaskReallocationTest {

  @testSetup
    static void createData() {
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
       
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
      
        List < Task > tList = new List < Task > ();
        Task t = bg_Test_Data_Utils.createTask('Task', acc.Id);
         t.OwnerId = u.id;
        t.Status = 'Completed';
        t.Description = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        
        Task t2 = bg_Test_Data_Utils.createTask('Task2', acc.Id);
         t2.OwnerId = u.id;
        t2.Status = 'In Progress';
        t2.Description = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        tList.add(t);
        tList.add(t2);
        insert tList;
        
        
        List < Event > eList = new List < Event > ();
        Event e = new Event();
      e.subject='event1';
        e.OwnerId = u.id;
        e.ActivityDateTime = System.today().addDays(1);
        e.StartDateTime = System.today().addDays(1);
        e.EndDateTime = System.today().addDays(1);
        e.Description = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        
        Event e2 =new Event();
        e2.OwnerId = u.id;
        e2.ActivityDateTime = System.today().addDays(1);
        e2.StartDateTime =  System.today().addDays(1);
        e2.EndDateTime = System.today().addDays(1);
        
        Event et = bg_Test_Data_Utils.createEvent('Event6', acc.Id);
             et.OwnerId = u.id;
        et.ActivityDateTime = System.today().addDays(1);
        et.StartDateTime = System.today().addDays(1);
        et.EndDateTime = System.today().addDays(1);
        eList.add(e);
        eList.add(e2);
        eList.add(et);
        
        insert eList;
        
    }
    
       private static testMethod void testMethoda() {
        User u = [select id from user limit 1];
        
         Task acc = new Task();
        acc.WhatId = null;
        acc.OwnerId = u.id;
        acc.Status = 'In Progress';
        acc.Description = 'desctritjgbuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttu';
        insert acc;
        
        Task acc3 =  new Task();
             acc3.OwnerId = u.id;
        acc3.Status = 'In Progress';
        acc3.Description = 'desctritjgbuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuttttttttttttttttttttttttttttttttttttttttttttttttttttttttuuuuuu';
        DateTime dT2 = DateTime.now();
        acc3.ActivityDate = System.today();//date.newinstance(dT2.year(), dT2.month(), dT2.day());
        insert acc3;
        
        
        Task acc2 =  new Task();
        acc2.OwnerId = u.id;
        acc2.Status = 'In Progress';
        acc2.Description ='desctritjgbuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuutttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttuuuuuuuu';
        DateTime dT = DateTime.now();
        acc2.ActivityDate = System.today();//date.newinstance(dT.year(), dT.month(), dT.day());
        
        insert acc2;
        
         PageReference pageRef = Page.EventTaskReallocationPage;
        EventTaskReallocation controller = new EventTaskReallocation();
        
        controller.setStatus('Open');
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        Test.startTest();
        controller.Results();
        
        controller.setStatus('Closed');
        controller.assignedUser = acc3;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open');
        controller.assignedUser = acc3;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open & Closed');
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open');
        controller.assignedUser = acc2;
        controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Closed');
        controller.assignedUser = acc2;
        controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open & Closed');
        controller.assignedUser = acc2;
        controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open & Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus(NULL);
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open');
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Open & Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        //controller.Results();
        
        controller.setStatus('Open');
        controller.assignedUser = acc;
        //controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        controller.Results();
        
        controller.setStatus('Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc;
        controller.setTaskEvent('Events Only');
        controller.Results();
        
        controller.setStatus('Open');
        controller.assignedUser = acc;
        controller.assignToUser = acc;
        controller.setTaskEvent('Tasks Only');
        controller.Results();
        
        controller.setStatus('Open & Closed');
        controller.assignedUser = acc;
        controller.assignToUser = acc;
        controller.setTaskEvent('Task & Events');
        
        controller.Results();
        controller.ReAllocate();
        
        List < SelectOption > options1 = controller.getStatusItems();
        List < SelectOption > options = controller.getItems();
        String s = controller.getStatus();
        String te = controller.getTaskEvent();
       
        controller.setStatus('Open');
        controller.assignedUser = acc3;
        controller.assignToUser = acc2;
        controller.setTaskEvent('Events Only');
        controller.ReAllocate();
        controller.Results();
        controller.ChkAll();
       }
       


}