/**
 *  @Class Name:    LeadTriggerFunctionsTest
 *  @Description:   This is a test class for LeadTriggerFunctions
 *  @Company:       dQuotient
 *  CreatedDate:    07/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           07/11/2017                  Original Version
 */
@isTest
private class LeadTriggerFunctionsTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        lead.FLT_Postal_Code__c =flt.id;
        insert lead;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        CampaignMember campMem = bg_Test_Data_Utils.createCampaignMember(lead.id, camp.id);
        insert campMem;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        
        Task t = bg_Test_Data_Utils.createTask('Task', acc.Id);
        t.WhatId = null;
        t.WhoId = lead.id;
        t.OwnerId = u.id;
        insert t;
    
        Event e = bg_Test_Data_Utils.createEvent('Event', acc.Id);
        e.WhatId = null;
        e.WhoId = lead.id;
        e.OwnerId = u.id;
        insert e;
    }
    
    private static testMethod void testupdateActivity() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];        
        Event e = [Select Score__c From Event Where WhoId = :testLead.Id];
        Task t = [Select Score__c From Task Where WhoId = :testLead.Id];
        List<Lead> leadList = new List<Lead>();
        leadList.add(testLead);
        test.startTest();
            LeadTriggerFunctions.updateActivity(leadList);
            System.assertEquals(e.Score__c, t.Score__c);
        test.stopTest();
	}
	
	private static testMethod void testupdateCampaignsField() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, Wildebeest_Ref__c, LeadSource from Lead where OwnerId= :u.Id];        
        List<Lead> leadList = new List<Lead>();
        leadList.add(testLead);
        test.startTest();
            LeadTriggerFunctions.updateCampaignsField(leadList);
            System.assertNotEquals(leadList[0].Campaigns__c, null);
        test.stopTest();
	}
	
	private static testMethod void testgetaccountdatainsert() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, Wildebeest_Ref__c, LeadSource from Lead where OwnerId= :u.Id];        
        List<Lead> oldleadList = new List<Lead>();
        oldleadList.add(testLead);
        test.startTest();
            LeadTriggerFunctions.getaccountdatainsert(oldleadList); 
            System.assertEquals(oldleadList[0].Wildebeest_Ref__c, '00034421');
        test.stopTest();
	}
	
	/*private static testMethod void testgetaccountdatainsertcomma() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, Wildebeest_Ref__c, LeadSource from Lead where OwnerId= :u.Id];        
        List<Lead> oldleadList = new List<Lead>();
        testLead.Wildebeest_Ref__c ='swq12333,333';
        update testLead;
        oldleadList.add(testLead);
        test.startTest();
            LeadTriggerFunctions.getaccountdatainsert(oldleadList); 
            System.assertEquals(oldleadList[0].Wildebeest_Ref__c, 'swq12333,333');
        test.stopTest();
	}
	
	private static testMethod void testgetaccountdataupdatecomma() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [Select Name From Account Where Name = 'Test Account N'];
         
        acc.Wildebeest_Ref__c ='swq12333';
        update acc;
       test.startTest();
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, Wildebeest_Ref__c, LeadSource from Lead where OwnerId= :u.Id];        
                List<Lead> oldleadList = new List<Lead>();
        oldleadList.add(testLead);
        List<Lead> newleadList = new List<Lead>();
        testLead.Wildebeest_Ref__c ='swq12333,333';
                    update testLead;
            newleadList.add(testLead);
            LeadTriggerFunctions.getaccountdataupdate(oldleadList, newleadList); 
            System.assertEquals(oldleadList[0].Wildebeest_Ref__c, 'swq12333,333');
        test.stopTest();
	}*/
	
	private static testMethod void testgetaccountdataupdate() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];        
        List<Lead> oldleadList = new List<Lead>();
        oldleadList.add(testLead);
        test.startTest();
        List<Lead> newleadList = new List<Lead>();
        testLead.Wildebeest_Ref__c ='00034421';
        bg_LeadHelper.isBeforeUpdate = true;
        update testLead;
        testLead.Wildebeest_Ref__c ='1222233';
        bg_LeadHelper.isBeforeUpdate = true;
        update testLead;
        
        test.stopTest();
	}
}