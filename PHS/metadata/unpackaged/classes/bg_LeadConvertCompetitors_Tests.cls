/*********************************************************************************
 * bg_LeadConvertCompetitors_Test
 *
 * Test class to verify the insertion and conversion of leads. 
 * Also tests the reparenting of the Competitors__c custom object  
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 11-01-2016
 *
 *********************************************************************************/

@isTest
private class bg_LeadConvertCompetitors_Tests {
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                                          Id = Test.getStandardPricebookId(),
                                          IsActive = true,
                                          Price_Book_Type__c='Standard'
                                        );
        Update standardPricebook;
        
        Lead leadRecord = bg_Test_Data_Utils.createLead('Last Name 1');
        insert leadRecord;
        
		Competitor__c compRecord = bg_Test_Data_Utils.createCompetitor(leadRecord.Id);
		insert compRecord;
    }
    
	private static testMethod void createLead(){
	    
        Lead leadRecord = [SELECT LastName, FirstName, Status, LeadSource, Company, Phone, MobilePhone, Email FROM Lead WHERE LastName = 'Last Name 1'];
		// Asserts
		System.assertEquals('Last Name 1', leadRecord.LastName);
		System.assertEquals('Test Co. Last Name 1', leadRecord.Company);
		System.assertEquals('01245564735', leadRecord.Phone);
		System.assertEquals('test@test.com', leadRecord.Email);
		
	}

	private static testMethod void createCompetitor(){
	    
		Lead leadRecord = [SELECT Id FROM Lead WHERE LastName = 'Last Name 1'];
		Competitor__c compRecord = [SELECT Lead__c, Strengths__c, Weaknesses__c FROM Competitor__c WHERE Lead__c =:leadRecord.id];
		// Asserts
		System.assertEquals(leadRecord.id, compRecord.Lead__c);
		System.assertEquals('Test Strength', compRecord.Strengths__c);
		System.assertEquals('Test Weakness', compRecord.Weaknesses__c);
	}

	private static testMethod void convertLead(){

		Lead leadRecord = [SELECT Id FROM Lead WHERE LastName = 'Last Name 1'];
		test.startTest();
    	Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(leadRecord.id);
        LeadStatus convertStatus = [Select Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        // Asserts
        System.assert(lcr.isSuccess());
        test.stopTest();
	}

	private static testMethod void checkCompetitorOppId(){
	    
        Lead leadRecord = [SELECT Id FROM Lead WHERE LastName = 'Last Name 1'];
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(leadRecord.id);
		LeadStatus convertStatus = [Select Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus('Converted To Opportunity');
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        Lead convertedLead = [SELECT Id, convertedOpportunityId FROM Lead WHERE IsConverted=true LIMIT 1];
		Competitor__c compRecord = [SELECT Lead__c, Opportunity__c FROM Competitor__c WHERE Lead__c =:leadRecord.id LIMIT 1];
		compRecord.Opportunity__c = convertedLead.convertedOpportunityId;
		update compRecord;
		// Asserts
        System.assertEquals(convertedLead.convertedOpportunityId, compRecord.Opportunity__c);
	}
}