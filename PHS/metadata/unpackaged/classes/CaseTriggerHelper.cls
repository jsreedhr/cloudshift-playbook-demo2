/**
    * @Name:        CaseTriggerHelper
    * @Description: This class will implement all methods from the handler
    *
    * @author:      Jared Watson
    * @version:     1.0
    * Change Log
    *
    * Date          author              Change Description
    * -----------------------------------------------------------------------------------
    * 24/08/2018  	Jared Watson        Created Class
*/

public with sharing class CaseTriggerHelper {

    /**
    *  Method Name: autoCreateContactFromEmailToCase
    *  Description:  method to  1)Auto Create Contact From Email To Case.
    *                           2)Preventing Creation of Duplicate Cancellation Cases.
    *  Param:  List<Case> listNewCases -> Trigger.New
    *  Return: NA
    **/
    public static void autoCreateContactFromEmailToCase(List<Case> listNewCases){
        if(listNewCases != null && !listNewCases.isEmpty()){
            Map<Id,Case> mapPhsIdCase = new Map<ID,Case>();
            Map<String, Contact> mapMatchingContact =new Map<String,Contact>();
            List<Contact> listcontact = new List<contact>();
            Set<Id> SetContactAccIds = new Set<Id>();
            Map<ID,ID> mapaccPhsID = new Map<Id,Id>(); //Map of Acc Ids and Phs-Acc Ids
            set<String> setCaseSuppliedEmail = new set<String>();
            List<Contact> listContactToInsert = new List<Contact>();
            for(Case caseObj: listNewCases){
                //For Preventing Creation of Duplicate Cancellation Cases
                if( caseObj.type =='Cancellation' && caseObj.PHS_Account__c !=Null )
                    mapPhsIdCase.put(caseObj.PHS_Account__c,caseObj);
                // Collection of Case supplied Emails
                setCaseSuppliedEmail.add(caseObj.SuppliedEmail);
            }

            //Prevent Creation of Duplicate Cancellation Cases
            if(mapPhsIdCase.values().size()>0){
                List<Case> listCasesWithSamePhs= [Select Id,CaseNumber,PHS_Account__c from Case where PHS_Account__c in :mapPhsIdCase.keySet() and type='Cancellation'];
                Map<Id,String> CasesUrlMap = new Map<Id,String>();
                String CaseUrl;
                //To Make the error message with link to all existing Cases in the trigger error message
                for(Case caseObj1: listCasesWithSamePhs)
                {
                    if(UserInfo.getUiTheme()=='Theme4d'  || UserInfo.getUiTheme()=='Theme4u' || UserInfo.getUiThemeDisplayed() == 'Theme4t' ){
                        if(CasesUrlMap.containsKey(caseObj1.PHS_Account__c))
                        {

                            CaseUrl = CasesUrlMap.get(caseObj1.PHS_Account__c)+','+caseObj1.CaseNumber;
                            CasesUrlMap.remove(caseObj1.PHS_Account__c) ;
                            CasesUrlMap.put(caseObj1.PHS_Account__c,CaseUrl);
                        }
                        else{
                            CaseUrl =caseObj1.CaseNumber ;
                            CasesUrlMap.put(caseObj1.PHS_Account__c,CaseUrl);
                        }

                    }
                    else
                    {
                        if(CasesUrlMap.containsKey(caseObj1.PHS_Account__c))
                        {
                            CaseUrl = CasesUrlMap.get(caseObj1.PHS_Account__c)+','+'<a href='+Label.Duplication_Cacellation_CaseLink+caseObj1.id+'>'+caseObj1.CaseNumber +'</a>';
                            CasesUrlMap.remove(caseObj1.PHS_Account__c) ;
                            CasesUrlMap.put(caseObj1.PHS_Account__c,CaseUrl);
                        }
                        else{
                            CaseUrl ='<a href='+Label.Duplication_Cacellation_CaseLink+caseObj1.id+'>'+caseObj1.CaseNumber +'</a>';
                            CasesUrlMap.put(caseObj1.PHS_Account__c,CaseUrl);
                        }
                    }


                }
                for(Case caseObj: listCasesWithSamePhs)
                {
                    if( caseObj.PHS_Account__c!=null
                            &&mapPhsIdCase.containsKey(caseObj.PHS_Account__c)
                            && mapPhsIdCase.get(caseObj.PHS_Account__c).type =='Cancellation')
                        (mapPhsIdCase.get(caseObj.PHS_Account__c)).addError('A Cancellations case has already been created for this PHS account, please see the following Cases: '+CasesUrlMap.get(caseObj.PHS_Account__c),false);
                }
            }
            //Get email matching contact list
            listcontact =  [select id, accountId, email from Contact where email IN : setCaseSuppliedEmail ];
            if(!listcontact.isEmpty()){
                for(Contact contactObj : listcontact)
                {
                    mapMatchingContact.put(contactObj.email, contactObj);
                    if(contactObj.accountId!=null)
                        SetContactAccIds.add(contactObj.accountId);
                }

                //Get Phs account Id if the number of Phs accounts are < 1 on the related account
                for(Account acc : [Select id,  (select id from PHS_Accounts__r ) From Account where ID in :SetContactAccIds ])
                {
                    if(acc.PHS_Accounts__r.size()==1)
                        mapaccPhsID.put(acc.id,acc.PHS_Accounts__r[0].id);
                }
            }
            //If no matching contact found
            //and email address doesn't end with suffix available in E2C_Contact_Creation_Exclusion__mdt
            if(mapMatchingContact.isEmpty())
            {
                Set<String> SetCutomObjSuffix = new Set<String>();
                List<E2C_Contact_Creation_Exclusion__mdt> listMetaDataSuffix = [SELECT Email_Suffix__c FROM E2C_Contact_Creation_Exclusion__mdt];
                for(E2C_Contact_Creation_Exclusion__mdt suffObj : listMetaDataSuffix)
                {
                    String lowerSuff = suffObj.Email_Suffix__c.tolowerCase();
                    SetCutomObjSuffix.add(lowerSuff);
                }

                for(Case caseObj: listNewCases){
                    if(caseObj.SuppliedEmail!=null){
                        String strEmailSuffix = '@'+caseObj.SuppliedEmail.substringafter('@');

                        if(!SetCutomObjSuffix.contains(strEmailSuffix.tolowerCase())) {
                            Contact newContactObj = new Contact(

                                    LastName=caseObj.SuppliedName.substringbefore(','),
                                    FirstName=caseObj.SuppliedName.substringAfter(','),
                                    Phone=caseObj.SuppliedPhone,
                                    Email = caseObj.SuppliedEmail
                            );

                            listContactToInsert.add(newContactObj);
                        }
                    }
                }
            }

            //If matching contacts found
            if(!mapMatchingContact.isEmpty())
            {

                for(Case caseObj: listNewCases){
                    if(!setCaseSuppliedEmail.isEmpty()
                            && mapMatchingContact.containsKey(caseObj.SuppliedEmail)
                            && mapMatchingContact.get(caseObj.SuppliedEmail).email != Null
                            && setCaseSuppliedEmail.contains(mapMatchingContact.get(caseObj.SuppliedEmail).email)){
                        //Assign the existing contact to Case
                        caseObj.ContactId = mapMatchingContact.get(caseObj.SuppliedEmail).id;

                        if(mapMatchingContact.get(caseObj.SuppliedEmail).accountId != Null){
                            //Populate Account Id on Case
                            caseObj.AccountId = mapMatchingContact.get(caseObj.SuppliedEmail).accountId;
                            if(!mapaccPhsID.isEmpty()
                                    && mapaccPhsID.containsKey(mapMatchingContact.get(caseObj.SuppliedEmail).accountId)
                                    && mapaccPhsID.get(mapMatchingContact.get(caseObj.SuppliedEmail).accountId)!=null)
                                //Populate PHS Account on Case if Account has only 1 Phs account related
                                caseObj.PHS_Account__c=mapaccPhsID.get(mapMatchingContact.get(caseObj.SuppliedEmail).accountId);
                        }
                    }
                }
            }



            try{
                insert listContactToInsert;
            }
            catch(Exception ex){
                System.debug('Error inserting Contact'+ex);
            }

        }
    }


    /**
    *  Method Name: validateCaseBeforeClosing
    *  Description:  method to  validate case before closing to verify Origin and Contact field values
    *  Param:  Map<Id,Case> mapNewCases -> Trigger.NewMap
    *  Param:  Map<Id,Case> oldNewCases -> Trigger.OldMap
    *  Return: NA
    */
    public static void validateCaseBeforeClosing(Map<Id,sObject> newMap, Map<Id,sObject> oldMap){

        Map<Id, Case> mapNewCases = (Map<Id, Case>) newMap;
        Map<Id, Case> oldNewCases = (Map<Id, Case>) oldMap;

        Set<Id> setCaseContactIds = new Set<Id>();

        for(Case caseObj : mapNewCases.values()){
            if(oldNewCases.get(caseObj.id).Status != caseObj.Status && caseObj.Status=='Closed' && caseObj.ContactId !=null )
                setCaseContactIds.add(caseObj.ContactId);
            //will not be possible to close a case where the Origin = Email and 'Contact' is blank
            if(oldNewCases.get(caseObj.id).Status != caseObj.Status
                    && (caseObj.Origin =='Email' || caseObj.ContactId ==null )
                    && caseObj.Status=='Closed' )
                caseObj.addError('Case cannot be closed, please make sure Origin is not Email and Contact is not blank.' );

        }

        //It will not be possible to close a case where the contact is not linked to a SFDC account
        Map<Id,Contact> mapConact= new Map<Id,Contact>( [Select Id , accountid from contact where id in  :setCaseContactIds and accountId = NULL ]);
        if(mapConact.values().size()>0){
            for(Case caseObj : mapNewCases.values()){
                {
                    if(oldNewCases.get(caseObj.id).Status != caseObj.Status && caseObj.Status=='Closed' && mapConact.containsKey(caseObj.ContactId))
                        caseObj.addError('Case cannot be closed,contact is not linked to a SFDC account.');
                }
            }
        }
    }

    /**
    *  Method Name: createDeletedWIonDelete
    *  Description:  method to  1)IDentify each case with recordtype wWrkItem
    *                           2)Create Deleted WI record with fields
    *  Param:  List<Case> listNewCases -> Trigger.New
    *  Return: NA
    **/
    public static void createDeletedWIonDelete(List<Case> listNewCases){

        List<Work_Item_Deletion_History__c> lstWorkItemDH = new List<Work_Item_Deletion_History__c>();

        for(Case c: [SELECT id, subject, Type, Status, Owner.Name, Diarised_Date_Time__c, Due_Date__c, CreatedDate
                        FROM Case
                        WHERE id in :listNewCases
                        AND RecordType.DeveloperName = :Utils_Constants.CASE_WORK_TYPE_RT_NAME]){
            lstWorkItemDH.add(createWIDeletionHistory(c));
        }

        insert lstWorkItemDH;

    }


    private static  Work_Item_Deletion_History__c createWIDeletionHistory(Case c){
        Work_Item_Deletion_History__c widh = new Work_Item_Deletion_History__c();
        widh.Deleted_WI_Id__c = c.Id;
        widh.Subject__c = c.Subject;
        widh.Type__c = c.Type;
        widh.Status__c = c.Status;
        widh.Deleted_WI_Owner__c = c.Owner.Name;
        widh.Diarised_Date__c = c.Diarised_Date_Time__c;
        widh.Due_Date__c = c.Due_Date__c;
        widh.Deleted_By__c = UserInfo.getName();
        widh.Deleted_WI_Created_Date__c = c.CreatedDate;
        widh.Deletion_Date__c = system.now();

        return widh;
    }
    
    
   
}