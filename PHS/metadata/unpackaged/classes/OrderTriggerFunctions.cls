public without sharing class OrderTriggerFunctions {
    
    public static void AssignContractoContractsTeam(List <Order> lstneworders, List <Order> lstoldorders){
        
        //Declare the variables
        Set <ID> set_Oppids = new Set <ID>();
        Set <ID> set_Agreementids = new Set <ID>();
        Map <Id, Id> mapOppIdConId = new Map <ID,ID>();
        Map <Id, Id> mapQuoteIdAccountId = new Map <ID, ID>();
        Map <Id, Opportunity> mapOppRecord = new Map <Id, Opportunity>();
        List <Opportunity> lst_opps = new List <Opportunity>();
        List <Opportunity> lst_oppupdates = new List <Opportunity>();
        List <Case> lst_casestocreate = new List <Case>();
        Set <ID> set_AgrAccounts = new Set <ID>();
        List <Account> lst_Accts = new List <Account>();
        List <Account> lst_AgreementAccs = new List <Account>();
        List <Account> lst_Locstoupdate = new List <Account>();
        List <Account> lst_Acctstoupdate = new List <Account>();
        
        for(Order no: lstneworders){
            set_Agreementids.add(no.id);
        }
        
        for(Order no: lstneworders){
            IF(no.OpportunityId != Null){
                set_Oppids.add(no.OpportunityId);
                mapOppIdConId.put(no.id, no.OpportunityId);
                mapQuoteIdAccountId.put(no.id, no.AccountId);
            }
        }
        
        IF(!set_Oppids.isEmpty())lst_opps = [SELECT ID, StageName FROM Opportunity WHERE Id in: set_Oppids];

        IF(!set_Oppids.isEmpty())lst_Accts = [SELECT ID, Most_Recent_Method_of_Payment__c, Most_Recent_Payment_Terms__c FROM Account WHERE Id in: mapQuoteIdAccountId.values()];
        
        if(!lst_opps.isEmpty() && lst_opps != null){
            for(Opportunity opp :lst_opps){
                mapOppRecord.put(opp.Id, opp);
            }
        }
        
        List <Agreement_Location__c> lst_agreementlocs = new List <Agreement_Location__c>([SELECT ID, Location_Salesforce_Account__c, Agreement__c FROM Agreement_Location__c WHERE Agreement__c in: set_Agreementids]);
        
        If(!lst_agreementlocs.isEmpty()){
                
            for(Agreement_Location__c agrloc: lst_agreementlocs){
                IF(agrloc.Location_Salesforce_Account__c != Null){
                set_AgrAccounts.add(agrloc.Location_Salesforce_Account__c);
                }       
            }
            
            lst_AgreementAccs = [SELECT ID, Company_Status__c FROM Account WHERE ID in: set_AgrAccounts];
            
        }
        
        for(Order no: lstneworders){
            for(Order oo: lstoldorders){
                IF(no.id == oo.id && no.Status == 'Accepted' && oo.Status != 'Accepted'){
                    
                    Case objcase = new Case();
                    objcase.AccountId = no.AccountId;
                    objcase.ContactId  = no.Buyer__c;
                    objcase.PHS_Account__c = no.Buyer_PHS_Account__c;
                    objcase.Subject = 'New Contract to Process - ' + no.OrderNumber;
                    objcase.Contract_Owner__c = no.OwnerId;
                    objcase.Status = 'Open';
                    objcase.Type = 'Contract Processing';
                    //Difficult for us to ascertain reason
                    objcase.Description = 'New contract to process via CPQ process';
                    objcase.Origin = 'CPQ';
                    objcase.Agreement__c = no.id;
                                        
                    IF(no.Contract_Type__c!='Wastekit'){
                        objcase.OwnerId = System.Label.ContractsProcessingTeamQueueId;
                    }
                    IF(no.Contract_Type__c=='Wastekit' && no.Contract_Type__c == 'Prospect'){
                        objcase.OwnerId = System.Label.WKNewBusinessContractsProcessingTeamQueueId;
                    }
                    IF(no.Contract_Type__c=='Wastekit' && no.Contract_Type__c != 'Prospect'){
                        objcase.OwnerId = System.Label.ContractsProcessingTeamQueueId;
                    }
                    
                    lst_casestocreate.add(objcase);
                    
                    IF(mapOppIdConId.containsKey(no.id) && mapOppIdConId != null){
                        mapOppRecord.get(mapOppIdConId.get(no.id)).StageName = 'Closed Won';
                        lst_oppupdates.add(mapOppRecord.get(mapOppIdConId.get(no.id)));
                    }
                    System.debug('no.OwnerId----->'+no.OwnerId);
                    
                    If(!lst_AgreementAccs.isEmpty()){
                        for(Account acc: lst_AgreementAccs){
                            acc.Company_Status__c = 'Customer';
                            lst_Locstoupdate.add(acc);
                        }                                               
                    }
                    
                    If(!lst_Accts.isEmpty()){
                        for(Account acc: lst_Accts){
                            acc.Most_Recent_Method_of_Payment__c = no.Method_of_Payment__c;
                            acc.Most_Recent_Payment_Terms__c = no.Payment_Terms__c;
                            lst_Acctstoupdate.add(acc);
                        }                                               
                    }
                }
            }
        }
        insert lst_casestocreate;
        Update lst_oppupdates;
        Update lst_Locstoupdate;
        Update lst_Acctstoupdate;
    }
}