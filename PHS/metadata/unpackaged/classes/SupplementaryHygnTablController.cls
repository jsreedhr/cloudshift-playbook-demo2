public class SupplementaryHygnTablController {
public List<SBQQ__Quote__c> quoteList{get;set;}
    
   
    protected Id quoteId;

    public SupplementaryHygnTablController(){
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        System.debug('quoteId: '+quoteId);
        
        if(quoteId!=NULL){
            quoteList = [SELECT Id,Installation_Document_Total__c,HW_Document_Amount__c,DoC_Document_Total__c,Carriage_Document_Amount__c FROM SBQQ__Quote__c WHERE id =:quoteId];
            System.debug('quoteList: '+quoteList); 
          
           
        }
    }
}