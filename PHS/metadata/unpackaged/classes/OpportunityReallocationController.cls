/**
 *  Class Name: OpportunityReallocationController  
 *  Description: This is a Class on Opportunity to reallocate the owner.
 *  Company: Standav
 *  CreatedDate:27/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             27-10-2017                 Original version
 */

public class OpportunityReallocationController {
    
    public static String oppoId{
        get;
        set;
    }
    public static String message{
        get;
        set;
    }
    
    public OpportunityReallocationController(){
        oppoId = ApexPages.currentPage().getParameters().get('id');
    }
    
    //Send to Sales Field for Aura Component SendtoFieldSalesOpportunityComponent
    @auraEnabled
    public static String reallocateSalesOpportunity(String oppoIdAura){
        oppoId = oppoIdAura;
        System.debug('check message---'+oppoId);
        opportunitySendtoSales();
        return message;
    }
    
    //Send to Outbound for Aura Component SendtoOutboundOpportunityComponent
    @auraEnabled
    public static String reallocateOutboundOpportunity(String oppoIdAura){
        oppoId = oppoIdAura;
        System.debug('check message---'+oppoId);
        opportunitySendtoOutbound();
        return message;
    }
    
    // Transfers the Lead ownerid to field sales 
    public static void opportunitySendtoSales(){
        oppoId = String.escapeSingleQuotes(oppoId);
        Opportunity oppoRecord = [SELECT Id, OwnerId, DM_Postal_District__c, DM_Postal_District__r.Name, DM_Postal_District__r.Sales_Rep__c, DM_Postal_District__r.SalesRSM__c, DM_Postal_District__r.Sales_Rep__r.Off_Sick_Annual_Leave__c FROM Opportunity WHERE Id =:oppoId];
        
        
        if(oppoRecord.DM_Postal_District__c != null){
            if(oppoRecord.DM_Postal_District__r.Name == null){
                if(ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationPostCodeError));
                message = Label.OpportunityReallocationPostCodeError;  
            }
            else if(oppoRecord.DM_Postal_District__r.Sales_Rep__c == null && oppoRecord.DM_Postal_District__r.SalesRSM__c != null){
            	oppoRecord.OwnerId = oppoRecord.DM_Postal_District__r.SalesRSM__c;
            	oppoRecord.Owner_Name__c = oppoRecord.DM_Postal_District__r.SalesRSM__c;
            	if(ApexPages.currentPage() != null)
            	    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
                message = Label.OpportunityReallocationSuccess; 
            	
            }
            else if (oppoRecord.DM_Postal_District__r.Sales_Rep__c == null && oppoRecord.DM_Postal_District__r.SalesRSM__c == null){
                if(ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationFieldSalesSRMError));           
                message = Label.OpportunityReallocationFieldSalesSRMError; 
            }
            else if(oppoRecord.DM_Postal_District__r.Sales_Rep__c != null){
                if(oppoRecord.DM_Postal_District__r.Sales_Rep__r.Off_Sick_Annual_Leave__c != true){
                    oppoRecord.OwnerId = oppoRecord.DM_Postal_District__r.Sales_Rep__c;
                    oppoRecord.Owner_Name__c=oppoRecord.DM_Postal_District__r.Sales_Rep__c;
                    if(ApexPages.currentPage() != null)
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
                    message = Label.OpportunityReallocationSuccess;
                }
                else if(oppoRecord.DM_Postal_District__r.Sales_Rep__r.Off_Sick_Annual_Leave__c == true){
                    if(oppoRecord.DM_Postal_District__r.SalesRSM__c != null){
                        oppoRecord.OwnerId = oppoRecord.DM_Postal_District__r.SalesRSM__c;
                        oppoRecord.Owner_Name__c=oppoRecord.DM_Postal_District__r.SalesRSM__c;
                        if(ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
                        message = Label.OpportunityReallocationSuccess;
                    }
                    else
                    {
                        if(ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.ContactAdministrator));        
                        message = Label.ContactAdministrator;
                    }
                }
            }
        }

        if(oppoRecord.DM_Postal_District__c == null){
            if(ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationNullPostCodeError));
            message =Label.OpportunityReallocationNullPostCodeError;
        }
        
        try{
            update oppoRecord;
            System.debug('check message---'+'Success');
        }   
        catch(System.DMLException e){
            System.debug('check message---'+e.getDmlMessage(0));
        }
    }
    
    // Transfers the Opportunity ownerid to Outbound 
    public static void opportunitySendtoOutbound(){
        
        oppoId = String.escapeSingleQuotes(oppoId);
        Opportunity oppRecord = [SELECT Id, OwnerId, DM_Postal_District__c, DM_Postal_District__r.Name, DM_Postal_District__r.OutboundTSAgent__c, DM_Postal_District__r.OutboundTSTeamLeader__c, DM_Postal_District__r.Sales_Rep__c, DM_Postal_District__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c FROM Opportunity WHERE Id =:oppoId];
        
        if(oppRecord.DM_Postal_District__c != null){
            if(oppRecord.DM_Postal_District__r.Name == null){
                if(ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationPostCodeError));
                message =Label.OpportunityReallocationPostCodeError;
            }
            else if(oppRecord.DM_Postal_District__r.OutboundTSAgent__c == null && oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c != null){
            	oppRecord.OwnerId = oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c;
            	oppRecord.Owner_Name__c=oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c;
            	if(ApexPages.currentPage() != null)
            	    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
            	message =Label.OpportunityReallocationSuccess;
            }
            else if (oppRecord.DM_Postal_District__r.OutboundTSAgent__c == null && oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c == null){
                if(ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationOutboundTSError));       
                message =Label.OpportunityReallocationOutboundTSError;
            }
            else if(oppRecord.DM_Postal_District__r.OutboundTSAgent__c != null){
                if(oppRecord.DM_Postal_District__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c != true){
                    
                    oppRecord.OwnerId = oppRecord.DM_Postal_District__r.OutboundTSAgent__c;
                    oppRecord.Owner_Name__c=oppRecord.DM_Postal_District__r.OutboundTSAgent__c;
                    if(ApexPages.currentPage() != null)
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
                    message =Label.OpportunityReallocationSuccess;
                }
                else if(oppRecord.DM_Postal_District__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c == true){
                    if(oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c!=null)
                    {
                        oppRecord.OwnerId = oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c;
                        oppRecord.Owner_Name__c=oppRecord.DM_Postal_District__r.OutboundTSTeamLeader__c;
                        if(ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,Label.OpportunityReallocationSuccess));
                        message =Label.OpportunityReallocationSuccess;
                    }
                    else
                    {
                        if(ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.ContactAdministrator));        
                        message =Label.ContactAdministrator;
                    }
                }
            }
        }
        
        if(oppRecord.DM_Postal_District__c == null){
            if(ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.OpportunityReallocationNullPostCodeError));
            message =Label.OpportunityReallocationNullPostCodeError;
        }
        
        try{
            update oppRecord;
            System.debug('check message---'+'Success');
        }   
        catch(System.DMLException e){
            System.debug('check message---'+e.getDmlMessage(0));
        }
    }

}