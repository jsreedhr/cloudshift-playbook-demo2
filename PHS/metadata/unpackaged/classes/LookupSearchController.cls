/**
 *  Class Name: LookupSearchController  
 *  Description: This is a Controller for LookupSearch page.
 *  Company: Standav
 *  CreatedDate:22/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             22/03/2018                 Original version
 */
public class LookupSearchController {
    
    public list<DisplayObjectWrapper> displayObjWrapperList {get;set;}
    public String searchLookupApi {get;set;}
    public String searchTerm;
    public String getSearchTerm() {
        return this.searchTerm;
    }
    public void setSearchTerm (String searchTerm) {
        this.searchTerm = searchTerm;
    }
    public Boolean showSearchList {get; set;}
    public String uniqueId {get; set;}

    public LookupSearchController() {
        System.debug('seachTerm.......>'+getSearchTerm());
        showSearchList = false;
        searchTerm = '';
        getLookupSearchList();
    }
    
    //method to get the records
    public void getLookupSearchList() {
        uniqueId = Apexpages.currentPage().getParameters().get('uniqueId');
        System.debug('uniqueId param..........>'+uniqueId);
        searchTerm = Apexpages.currentPage().getParameters().get('searchTerm');
        system.debug('enter search method with ----->'+searchTerm);
        // uniqueId ='80125000000RCDA';
        // searchTerm = 'we';
        String searchquery='FIND \''+searchTerm+'\' IN ALL FIELDS RETURNING PHS_Account__c(Id, Account_Name__c, Billing_Country__c, Phone__c, Type_of_Business__c)';
        List<List<sObject>> sObjectList = Search.query(searchQuery);
        this.displayObjWrapperList = New List<DisplayObjectWrapper>();
        for(List<sObject> sObjList : sObjectList)
        {
            system.debug('sObjList---->'+sObjList);
            for(sObject sObjectVar : sObjList)
            {
                DisplayObjectWrapper objWrapper = New DisplayObjectWrapper();
                objWrapper.objId = String.valueOf(sObjectVar.get('Id'));
                objWrapper.Name = String.valueOf(sObjectVar.get('Account_Name__c'));
                objWrapper.Country = String.valueOf(sObjectVar.get('Billing_Country__c'));
                objWrapper.Phone = String.valueOf(sObjectVar.get('Phone__c'));
                objWrapper.BusinessType = String.valueOf(sObjectVar.get('Type_of_Business__c'));
                displayObjWrapperList.add(objWrapper);
            }    
        }
        showSearchList = true;
        system.debug('displayObjWrapperList---->'+displayObjWrapperList);
        return;
    }
    
    // Wrapper for the records
    public class DisplayObjectWrapper
    {
        public String objId{get;set;}
        public String Name{get;set;}
        public String Country{get;set;}
        public String Phone{get;set;}
        public String BusinessType{get;set;}
    }
    
}