/*********************************************************************************
 * bg_PHSAccountSelectionController_Tests
 *
 * Test methods for the bg_PHSAccountSelectionController class
 *
 * Author: Jamie Wooley
 * Created: 05-05-2016
 *
 *********************************************************************************/

@isTest
private class bg_PHSAccountSelectionController_Tests {

    static testMethod void test_NoAccount() {

    	List<Case> cases = new List<Case>();

    	PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assert(checkApexMessages(Label.Account_Must_Have_PHS_Accounts));

    }

    static testMethod void test_ForceRefresh() {

    	List<Case> cases = new List<Case>();

    	PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.forceRefresh();

    }

    static testMethod void test_Cancel() {
        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
        Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.cancel();
    }

    static testMethod void test_PHSAccountOptions(){
		Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        List<SelectOption> phsAccounts = phsAccountSelection_Controller.PHSAccounts;

    }

    static testMethod void test_AccountWithoutPHSAccounts() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assert(checkApexMessages(Label.PHS_Account_Service_Enabled));

    }

    static testMethod void test_AccountWithPHSAccountNoService() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Service_Enabled__c = false;
        insert phsAccount;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assert(checkApexMessages(Label.PHS_Account_Service_Enabled));

    }

    static testMethod void test_AccountWithPHSAccountNoRef() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Wildebeest_Ref__c = null;
        insert phsAccount;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assert(checkApexMessages(Label.PHS_Account_Wildebeest_Ref));

    }

    static testMethod void test_AccountWithPHSAccount_Positive() {

        Glogbal_Settings__c globalSettings = new Glogbal_Settings__c();
        globalSettings.PHS_Account_Field_Id__c = 'testfieldId';
        globalSettings.PHS_Account_Link_Id__c = 'testlinkid';
        insert globalSettings;

        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        insert phsAccount;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
    	Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a.Id);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        PageReference casePageRef = phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assertNotEquals(null, casePageRef);

    }

    static testMethod void test_ContactWithAccountWithPHSAccount_Positive() {

        Glogbal_Settings__c globalSettings = new Glogbal_Settings__c();
        globalSettings.PHS_Account_Field_Id__c = 'testfieldId';
        globalSettings.PHS_Account_Link_Id__c = 'testlinkid';
        insert globalSettings;

        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        Contact c = bg_Test_Data_Utils.createContact('test', a.Id);
        insert c;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        insert phsAccount;

        List<Case> cases = new List<Case>();

        PageReference pageRef = Page.bg_PHSAccountSelection;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', c.Id);

        bg_PHSAccountSelectionController phsAccountSelectionController = new bg_PHSAccountSelectionController(new ApexPages.StandardSetController(cases));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.contactId = c.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        bg_PHSAccountSelection_Controller phsAccountSelection_Controller = new bg_PHSAccountSelection_Controller();
        phsAccountSelection_Controller.theCase = phsAccountSelectionController.theCase;
        phsAccountSelection_Controller.cancelURL = phsAccountSelectionController.cancelURL;

        PageReference casePageRef = phsAccountSelection_Controller.goToCaseCreationScreen();

        system.assertNotEquals(null, casePageRef);

    }

    private static Boolean checkApexMessages(String message) {
	    for(Apexpages.Message msg : ApexPages.getMessages())
	    {
	        if (msg.getDetail().contains(message)) {
	            return true;
			}
		}
		return false;
	}

}