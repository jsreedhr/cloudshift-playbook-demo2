/**
 *  @Class Name:    CreateAgreementCompController
 *  @Description:   This is a controller for CreateAgreementComponent
 *  @Company:       dQuotient
 *  CreatedDate:    09/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha                11/12/2017           		Original Version
 */
public class CreateAgreementCompController {
    @AuraEnabled
    public static Profile getLoggedInProfile() {
        Profile loggedinUser = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1];
        return loggedinUser;
    }
    
    @AuraEnabled
    public static User getUserStatus() {
        User loggedinUser = [select ActiveInPilot__c, APIPartnerServerURL290__c,ApiSessionID_F__c from USER where Id = :UserInfo.getUserId() LIMIT 1];
        return loggedinUser;
    }
    
    @AuraEnabled
    public static Opportunity getOpp(Id oppId)
        
    {
        Opportunity opp = [SELECT Quote_Roll_Up__c ,Has_Synced_Quote__c,Product_Count__c,Is_Quick_Contract__c FROM Opportunity WHERE id =: oppId];
        return opp;
    }    
    
    @AuraEnabled
    public static  Boolean hasAgreements(Id oppId)
    {
        Boolean hasOpenAgreements =  bg_AgreementHelper.returnResultOfOpenAgreement(oppId);
        System.debug('hasOpenAgreements:'+hasOpenAgreements);
        return hasOpenAgreements;
    }
    
    @AuraEnabled
    public static Id createAgreement(Id oppId)
    {
        Opportunity op = [SELECT Product_Count__c FROM Opportunity WHERE id =: oppId];
        Id agreementID =  bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(oppId,op.Product_Count__c.intValue());
        Date dt=Date.today();
        op.Agreement_Created_Date__c  = dt;
        try{
             update op;
             System.debug('opp:'+op);
        }
        catch(Exception e)
        {
             System.debug('Exception---:'+e);
        }
              
        return agreementID;
    }
    
}