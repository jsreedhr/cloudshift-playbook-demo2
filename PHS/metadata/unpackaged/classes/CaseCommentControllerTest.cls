/*
*  Class Name:  CasesCommentControllerTest
*  Description: Test class for CasesCommentController
*  Company: Cloudshift
*  CreatedDate: 23-Aug-2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer      		Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Jared Watson         23-Aug-2017            Orginal Version
*/
@isTest
private class CaseCommentControllerTest {

    static Account tAccount;
    static Contact tContact;
    static Case tCaseParent;
    static Case tCaseChild;
    static Case tCaseWorkOrder;
    static CaseComment tCaseComParent;
    static CaseComment tCaseComChild;
    static CaseComment tCaseComWorkOrder;

    @isTest
    private static void createData(){

        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        globalSettings.Can_Edit_Closed_Cases__c = TRUE;
        update globalSettings;

        tAccount = bg_Test_Data_Utils.createAccount('testAccountCCCT');
        insert tAccount;

        tContact = bg_Test_Data_Utils.createContact('testContactCCCT', tAccount.id);
        insert tContact;

        tCaseParent = bg_Test_Data_Utils.createCase('testCaseParent', tContact.id, tAccount.id);
        tCaseChild = bg_Test_Data_Utils.createCase('testCaseChild', tContact.id, tAccount.id);
        tCaseWorkOrder = bg_Test_Data_Utils.createCase('testCaseWorkOrder', tContact.id, tAccount.id);

        insert tCaseParent;

        tCaseChild.ParentId = tCaseParent.Id;
        insert tCaseChild;

        tCaseWorkOrder.Parent_Case__c = tCaseParent.Id;
        insert tCaseWorkOrder;

        tCaseComParent = bg_Test_Data_Utils.createCaseComment('CaseCommParent', tCaseParent.id);
        tCaseComParent.CommentBody = 'Parent Comment';
        insert tCaseComParent;

        tCaseComChild = bg_Test_Data_Utils.createCaseComment('CaseCommChild', tCaseChild.id);
        tCaseComChild.CommentBody = 'Child Comment';
        insert tCaseComChild;

        tCaseComWorkOrder = bg_Test_Data_Utils.createCaseComment('CaseCommWorkOrder', tCaseWorkOrder.id);
        tCaseComWorkOrder.CommentBody = 'Work Order Comment';
        insert tCaseComWorkOrder;

    }

    @isTest
    private static void coverController(){
        createData();
        
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'Customer Services' Limit 1].Id);

        System.runAs(userToRunAs) {


            Test.startTest();
            PageReference pageRef = Page.CaseCommentsInline;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', tCaseParent.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(tCaseParent);

            CaseCommentController cct = new CaseCommentController(sc);

            Test.stopTest();

            System.assertEquals(1, cct.lstDirectCaseComments.size());
            System.assertEquals(2, cct.lstChildCaseComments.size());
        }

    }
}