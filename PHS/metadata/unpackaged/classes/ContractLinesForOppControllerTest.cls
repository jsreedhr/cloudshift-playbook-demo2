/*
*  Class Name:  ContractLinesForOppControllerTest
*  Description: Test class for ContractLinesForOppController
*  Company: CloudShift
*  CreatedDate: 28-Jan-2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer      		Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  David Kurtanjek      28-Jan-2018            Original Version
*/
@isTest
private class ContractLinesForOppControllerTest {
	//sObject2 denotes odata in correct format, SObject1 denotes incomplete data that will cover else/catch blocks
	static Account account;
	static Contact contact;
    static List<PHS_Account__c> phsAccountList;
    static Opportunity opp1;
	static Opportunity opp2;
    static PriceBook2 priceBook;

    @isTest
    private static void createData(){
        phsAccountList = new List<PHS_Account__c>();
		// create custom setting Wildebeest_Integration__c
		Wildebeest_Integration__c wildBeest = bg_Test_Data_Utils.createCSWildebeestIntegration();
		insert wildBeest;
		//Insert Account
		account = bg_Test_Data_Utils.createAccount('Test Account');
		account.Wildebeest_Ref__c = '4813792';
		insert account;
		//Insert Contact
		contact = bg_Test_Data_Utils.createContact('Test Contact' , account.Id);
		insert contact;
		//Insert PHS_Account__c
		for(Integer i=0;i<3;i++){
			PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('1234', account.Id);
			phsAccountList.add(phsAccount);
		}
		//following three Wildebeest_Ref__c values yields results, don't change
		phsAccountList[0].Wildebeest_Ref__c = '4813792';
		phsAccountList[1].Wildebeest_Ref__c = '3073109';
		phsAccountList[2].Wildebeest_Ref__c = '4505501';
		insert phsAccountList;
        //updating standardPriceBook
        PriceBook2 priceBook = new PriceBook2(
        	Id = Test.getStandardPricebookId(),
            Name = 'Standard Price Book',
            Description = 'Some Text',
            isActive = TRUE,
            Price_Book_Type__c='Standard'
        );
        update priceBook;
		//Insert Opportunity
		opp1 = bg_Test_Data_Utils.createOpportunity('Test Opportunity 1', account.id);
		opp2 = bg_Test_Data_Utils.createOpportunity('Test Opportunity 2', account.id);
		insert new List<Opportunity>{opp1,opp2};
    }
    @isTest
    private static void contractLinesTest(){
		createData();
        ContractLinesForOpportunityController controller1 = new ContractLinesForOpportunityController();
		ContractLinesForOpportunityController controller2 = new ContractLinesForOpportunityController();

		Test.startTest();
		// covers some errors
		controller1.populateContractLines();
		controller1.setOppId(opp1.Id);
		Id tempId = controller1.getOppId();
		System.assertEquals(tempId,controller1.oppId);
		controller1.populateContractLines();
		System.assertEquals('stop'+tempId, controller1.str+controller1.oppId);

		//gives positive results
		controller2.setOppId(opp2.Id);
		tempId = controller2.getoppId();
		System.assertEquals(tempId,controller2.oppId);
		controller2.populateContractLines();
		System.assertEquals(tempId, controller2.oppId);
		System.assertNotEquals(NULL, controller2.contractLineList);
        //if response is saved in opportunity Object then showResponseFromOpportunity() will get covered
        controller2.populateContractLines();

		//changing values to cover else/catch blocks
		phsAccountList[0].Wildebeest_Ref__c = '123456789';
		phsAccountList[1].Wildebeest_Ref__c = '12345678';
		phsAccountList[2].Wildebeest_Ref__c = '1234567';
		update phsAccountList;
		controller2.populateContractLines();
        System.assertNOTEquals(NULL, controller2.contractLineList);

		for(PHS_Account__c phsAccount : phsAccountList)
            phsAccount.Wildebeest_Ref__c = '';
		update phsAccountList;
		controller2.populateContractLines();
        System.assertEquals(new List<bg_ContractLine>(), controller2.contractLineList);

		account.Wildebeest_Ref__c = '123456789';
		update account;
		controller2.populateContractLines();
        System.assertEquals(new List<bg_ContractLine>(), controller2.contractLineList);

		account.Wildebeest_Ref__c = '';
		update account;
		controller2.populateContractLines();
        System.assertEquals(0, controller2.contractLineList.size());

		Test.stopTest();

    }
    
    @isTest
    public static void testmethod1()
    {
        bg_ContractLineCallout bgc = new bg_ContractLineCallout();
    }
}