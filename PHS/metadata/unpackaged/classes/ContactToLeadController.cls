/*Controller Extension for ContactToLeadPage
  Created on 23/10/2017
*/
public class ContactToLeadController 
{
    public Contact contactObj{get; set;}
    public String responseMessage{get; set;}
    public Boolean redirectToLead{get; set;}
    public Id leadId{get; set;}
    public ContactToLeadController(ApexPages.StandardController stdController)
    {
        contactObj = (Contact)stdController.getRecord();
    }
     public void  createLead()
   //  11/10/2017 to redirect to lead
   // public PageReference  createLead()
    {
        responseMessage = 'Unable to Create Lead';
        redirectToLead = false;
        if(contactObj!=NULL)
        {
            contactObj = [Select id, Account.Name, Account.Wildebeest_Ref__c, Account.SIC, Account.Website, Account.Industry, Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.Entity_Type__c, Description, DoNotCall, Decision_Maker__c, Email, Fax, FirstName, HasOptedOutOfEmail, Account.Key_Account__c, Account.Greenleaf_Key_Account__c, Account.Company_Reg_No__c,
                          LastName, LeadSource, MailingAddress, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, MobilePhone, Name, Phone, Secondary_Contact__c,
                          Salutation, Title
                          from Contact 
                          where id =:contactObj.id LIMIT 1];
 
            if(contactObj!=NULL)
            {
                Lead newLeadObj = New Lead();

                if(contactObj.Account.Name!=NULL)
                    newLeadObj.Company = contactObj.Account.Name;
                if(contactObj.Account.Company_Reg_No__c!=NULL)
                    newLeadObj.Company_Reg_No__c = contactObj.Account.Company_Reg_No__c;
                if(contactObj.Account.Sic!=NULL)
                    newLeadObj.SIC_Code__c = contactObj.Account.Sic;
                if(contactObj.Decision_Maker__c!=NULL)
                    newLeadObj.Decision_Maker__c = contactObj.Decision_Maker__c;
                if(contactObj.Description!=NULL)
                    newLeadObj.Description = contactObj.Description;
                if(contactObj.Email!=NULL)
                    newLeadObj.Email = contactObj.Email;
                if(contactObj.Fax!=NULL)
                    newLeadObj.Fax = contactObj.Fax;
                if(contactObj.FirstName!=Null)
                    newLeadObj.FirstName = contactObj.FirstName;
                if(contactObj.HasOptedOutOfEmail!=NULL)
                    newLeadObj.HasOptedOutOfEmail = contactObj.HasOptedOutOfEmail;
                if(contactObj.DoNotCall!=NULL)
                    newLeadObj.DoNotCall = contactObj.DoNotCall;
                if(contactObj.LastName!=NULL)
                    newLeadObj.LastName = contactObj.LastName;
                if(contactObj.Account.BillingStreet!=NULL)
                    newLeadObj.Street = contactObj.Account.BillingStreet;
                    newLeadObj.City = contactObj.Account.BillingCity;
                    newLeadObj.State = contactObj.Account.BillingState;
                    newLeadObj.PostalCode = contactObj.Account.BillingPostalCode;            
                if(contactObj.Account.Key_Account__c!=NULL)
                    newLeadObj.Key_Account__c = contactObj.Account.Key_Account__c;
                if(contactObj.Account.Greenleaf_Key_Account__c!=NULL)
                    newLeadObj.Greenleaf_Key_Account__c = contactObj.Account.Greenleaf_Key_Account__c;
                if(contactObj.Account.Entity_Type__c!=NULL)
                    newLeadObj.Entity_Type__c = contactObj.Account.Entity_Type__c;
                if(contactObj.MobilePhone!=NULL)
                    newLeadObj.MobilePhone = contactObj.MobilePhone;
                if(contactObj.Phone!=NULL)
                    newLeadObj.Phone = contactObj.Phone;
                if(contactObj.Salutation!=NULL)
                    newLeadObj.Salutation = contactObj.Salutation;
                if(contactObj.Secondary_Contact__c!=NULL)
                    newLeadObj.Secondary_Contact__c = contactObj.Secondary_Contact__c;
                if(contactObj.Title!=NULL)
                    newLeadObj.Title = contactObj.Title;
                If(contactObj.Account.Wildebeest_Ref__c!=Null){
                    newLeadObj.Wildebeest_Ref__c = contactObj.Account.Wildebeest_Ref__c;
                    newLeadObj.Company_Status__c = 'Customer';
                }
                If(contactObj.Account.Industry!=Null)
                    newLeadObj.Industry = contactObj.Account.Industry;
                If(contactObj.Account.Website!=Null)
                    newLeadObj.Website = contactObj.Account.Website;
                newLeadObj.Existing_Contact__c = contactObj.Id;
                newLeadObj.Created_From_Contact__c=true;

                try{
                    insert newLeadObj;
                }catch(DmlException e)
                {
                    System.Debug('Error on Lead Insert '+e);
                    responseMessage+=' '+e;
                }
                if(newLeadObj.id!=NULL)
                {   
                    responseMessage = 'Lead created successfully, Redirecting you to Lead record....';
                    redirectToLead = true;
                    leadId = newLeadObj.id;
                    
                }
            }
        }
         //  11/10/2017 to redirect to lead
      // return new PageReference ('/' + leadId+ '?nooverride=1');
      
          
    }
}