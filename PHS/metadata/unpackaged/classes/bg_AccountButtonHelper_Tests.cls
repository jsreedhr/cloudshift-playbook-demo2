@isTest
private class bg_AccountButtonHelper_Tests {
    static testMethod void testPHSIdIsReturned() {

    	Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;

        Integer listSize = bg_AccountButtonHelper.returnPHSAccountCount(a.Id);

        System.assertEquals(1, listSize);
        
    }
}