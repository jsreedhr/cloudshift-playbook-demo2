/*********************************************************************************
 * bg_ExternalHelper
 *
 * Helper method for the 'External' custom object
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 09-03-2016
 *
 *********************************************************************************/


public with sharing class bg_ExternalHelper
{
	public static void pushInfo(List<External__c> externals, Map<id, External__c> oldExternals)
	{
		
		List<Lead> newLeads = new List<Lead>();
		List<Case> newCases = new List<Case>();
		Lead myLead;
		Case myCase;

		for(External__c ext : externals)
		{
			if(ext.Record_Type__c == 'Lead')
			{
				myLead = new Lead();
				myLead.Company = ext.Lead_Company__c;
				myLead.Email = ext.Lead_Email__c;
				myLead.FirstName = ext.Lead_First_Name__c;
				myLead.LastName = ext.Lead_Last_Name__c;
				myLead.Phone = ext.Lead_Phone__c;
				myLead.Photo_URL__c = ext.Lead_Photo_URL__c;
				myLead.Products_Of_Interest__c = ext.Lead_Products_of_Interest__c;
				myLead.Salutation = ext.Lead_Salutation__c;
				myLead.LeadSource = ext.Lead_Source__c;
				newLeads.add(myLead);					
			}


			// else if(ext.Record_Type__c == 'Case')
			// {
			// 	myCase = new Case();
			// 	myCase.AccountId = ext.Case_Account__c;
			// 	System.debug('**** case account' + ext.Case_Account__c);
			// 	myCase.Description = ext.Case_Description__c;
			// 	myCase.Origin = ext.Case_Origin__c;
			// 	myCase.Priority = ext.Case_Priority__c;
			// 	myCase.Reason = ext.Case_Reason__c;
			// 	myCase.Subject = ext.Case_Subject__c;
			// 	myCase.Status = ext.Case_Status__c;
			// 	myCase.Type = ext.Case_Type__c;
			// 	newCases.add(myCase);
			// }

		}

		if(!newLeads.IsEmpty())
		{
			insert newLeads;
		}

		// if(!newCases.isEmpty())
		// {
		// 	insert newCases;
		// }

	}
}