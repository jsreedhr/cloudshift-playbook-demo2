/********************************************************************************
* bg_EmailMessage_Helper :
*
* Helper Class for the Email Message trigger methods
*
* Created By: Tom Morris
* Created Date: 17-01-2017 
* Changes:      16-02-2017 KH - added completeResponseMilestones
                01-02-2017 TM - TM - Added resetUnreadEmailFlag()
                18-01-2018 DT - added addErrorToFLT0Emails() and addErrorToServiceBoxDestinationEmails()
				04-04-2018 IB - Changes to 'updateCaseFlag' method to also update case status
*********************************************************************************/

global with sharing class bg_EmailMessage_Helper
{
    public static Set<Id> caseSet = new Set<Id>();
    private static final String NEW_STATUS = '0';
    static List<Service_Mailbox__mdt> serviceMailboxes = [SELECT Email__c FROM Service_Mailbox__mdt];
    /*
        Method to flag the Case as having an unread email.
    */
    public static void updateCaseFlag(List<EmailMessage> newEmails)
    {

    	List<Id> caseIds = new List<Id>();

        for (EmailMessage em : newEmails)
        {
            if (em.Incoming)
            {
                caseIds.add(em.ParentId);
            }
        }

         List<Case> caseToUpdate = new List<Case>();
        
    //    List<Case> parentCases = [SELECT Id FROM Case WHERE Id IN: caseIds];
        List <Case> caseList = [SELECT Id, Status, (SELECT Id, Incoming, Status, ParentId FROM EmailMessages)
                                FROM Case WHERE Id IN:caseIds];

        for (Case newCase : caseList)
        {
            newCase.Unread_Emails__c = TRUE;
            newCase.Last_Email_Received_Date_Time__c = System.NOW();
            
            Boolean notFirst = newCase.EmailMessages.size() > 1 ;
                if (notFirst){

                newCase.Status = 'Awaiting PHS Response';
                if (!caseToUpdate.contains(newCase))
                {
                    caseToUpdate.add(newCase);

                }    
        }
        }

        if (!caseToUpdate.isEmpty())
        {
            update caseToUpdate;
        }
    }

    /*
        Method to check if the Case has any unread emails (invoked by custom button on Case)
    */
    webservice static String hasUnreadEmails(Id caseId)
    {
        
        List<EmailMessage> emailsRelatedToParentCase = [SELECT Id, Status FROM EmailMessage WHERE ParentId=:caseId AND Incoming = TRUE];

        for (EmailMessage email : emailsRelatedToParentCase)
        {
            if (email.Status=='0')
            {
                return 'TRUE';
            }
        }

        return 'FALSE';
    }

    /*
        Method used in a scheduled job to clear flags on Cases with no unread emails if the flag is still set
    */

    public static void resetUnreadEmailFlag(List<Case> newCases ){

        Set<Id> caseIds = new Set<Id>();

        for(Case caseToOperate : newCases)
        {
            caseIds.add(caseToOperate.id);
        }

        List<EmailMessage> unreadEmailsRelatedToParentCase = [SELECT Id, Status, ParentId
                                                              FROM EmailMessage 
                                                              WHERE ParentId 
                                                              IN:caseIds AND Incoming = TRUE AND Status='0'];


        Set<Id> casesWithUnreadEmails = new Set<Id>();

        for(EmailMessage em : unreadEmailsRelatedToParentCase)
        {
            if(!casesWithUnreadEmails.contains(em.ParentId))
            {
                casesWithUnreadEmails.add(em.ParentId);
            }
        }


        for(Id unreadEmailCaseId : casesWithUnreadEmails)
        {
            if(caseIds.contains(unreadEmailCaseId))
            {
                caseIds.remove(unreadEmailCaseId);
            }
        }


        List<Case> casesToResetFlag = [SELECT Id, Unread_Emails__c FROM Case WHERE Id IN: caseIds];


        List<Case> casesToUpdate = new List<Case>();

        for(Case caseToOperate : casesToResetFlag)
        {
            Case newCase = new Case();
            newCase.Id = caseToOperate.Id;
            newCase.Unread_Emails__c = FALSE;
            casesToUpdate.add(newCase);
        }


        if(!casesToUpdate.isEmpty())
        {
            Database.SaveResult[] srList = Database.update(casesToUpdate, FALSE);

            if (!Test.isRunningTest() && !srList.isEmpty())
            {
                Messaging.SingleEmailMessage emailToSend = generateErrorEmail(srList);
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
            }
        }
        else
        {
            Messaging.SingleEmailMessage emailToSend = generateNoResultsEmail();
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {emailToSend});
        }
    }

    private static Messaging.SingleEmailMessage generateErrorEmail(Database.SaveResult[] saveResults){

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(UserInfo.getUserEmail());
        mail.setToAddresses(sendTo);
        mail.setReplyTo('info@emea.salesforce.com');
        mail.setSenderDisplayName('PHS');
        mail.setSubject('Error: UnreadEmail Job ' + DateTime.Now());
        String body = '';
        Integer successNumber = 0;
        Integer errorNumber = 0;
        Integer totalNumber = 0;

        for(Database.SaveResult sr : saveResults)
        {
            if(sr.isSuccess())
            {
                successNumber++;
            }
            else
            {
                errorNumber++;
            }

            totalNumber++;
        }

        body += 'Total: ' + totalNumber + ', Errors: ' + errorNumber + ', Successes: ' + successNumber + '.';

        mail.setHtmlBody(body);
        return mail;
    }

    private static Messaging.SingleEmailMessage generateNoResultsEmail(){

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(UserInfo.getUserEmail());
        mail.setToAddresses(sendTo);
        mail.setReplyTo('info@emea.salesforce.com');
        mail.setSenderDisplayName('PHS');
        mail.setSubject('Error: UnreadEmail Job ' + DateTime.Now());
        String body;

        body = 'No results were returned';

        mail.setHtmlBody(body);
        return mail;
    }

    /*
        Method to mark the First Response and Second Response Milestones as Completed
        if they are still open, when Outbound Emails are sent from the Case.
    */
        
    public static void completeResponseMilestones(List<EmailMessage> emailMessages)
    {
        DateTime completionDate = System.now();

        Set<Id> caseIDs = new Set<Id>();
        
        for (EmailMessage em : emailMessages)
        {
            if (!em.Incoming)
            {
                caseIDs.add(em.ParentId);
            }
        }

        if (!caseIDs.isEmpty())
        {
            List<Case> caseList = [SELECT Id, ContactId, Contact.Email, SlaStartDate, Status 
                                  FROM Case WHERE Id IN :caseIDs AND EntitlementId != Null AND SlaStartDate != Null AND SlaExitDate = null AND Status != 'Closed']; 
            
            if (!caseList.isEmpty())
            {
                List<Id> updateCases = new List<Id>();
            
                for (Case caseObj:caseList)
                {
                    if (caseObj.SlaStartDate <= completionDate)
                    {
                        updateCases.add(caseObj.Id);
                    }
                    if (!updateCases.isEmpty())
                    {
                        bg_MilestoneHelper.completeResponseMilestones(updateCases, completionDate);
                    }
                }
            }                             
        }
    }

    /*
        Method to add errors to emails that come from address that contain 'FLT0'
    */

    public static void addErrorToFLT0Emails(List<EmailMessage> emailMessages)
    {
      //  System.debug('adding error to flt0 emails');
        //System.debug(Label.FLT0.toLowerCase());
        //System.debug('message  ' +emailMessages);
        //System.debug('outbound  ' +emailMessages[0].FromAddress);
        
        for (EmailMessage outboundEmailMessage : emailMessages)
        {
            if (outboundEmailMessage.FromAddress.toLowerCase().contains(Label.FLT0.toLowerCase()))
            {
                outboundEmailMessage.addError(Label.Invalid_Email_To_Send_From);
            }
        }
    }

    /*
        Method to add errors to emails being sent to PHS' service boxes
    */

    public static void addErrorToServiceBoxDestinationEmails(List<EmailMessage> emailMessages)
    {
        System.debug('adding error to service box emails');
        Set<String> serviceMailboxAddresses = new Set<String>();

        for (Service_Mailbox__mdt serviceMailbox : serviceMailboxes)
        {
            serviceMailboxAddresses.add(serviceMailbox.Email__c);
        }

        for (EmailMessage outboundEmailMessage : emailMessages)
        {
            list<String> addresses = new list<String>();
            if (outboundEmailMessage.ToAddress != null)
            {
                addresses.addAll(outboundEmailMessage.ToAddress.split('; '));
            }

            if (outboundEmailMessage.CcAddress != null)
            {   
                addresses.addAll(outboundEmailMessage.CcAddress.split('; '));
            }   

            if (outboundEmailMessage.BccAddress != null)
            {   
                addresses.addAll(outboundEmailMessage.BccAddress.split('; '));
            }

            for (String address : addresses)
            {
                if (serviceMailboxAddresses.contains(address))
                {
                    outboundEmailMessage.addError(Label.Email_Sent_To_Service_Mailbox);
                    break;
                }
            }
        }
    }

}