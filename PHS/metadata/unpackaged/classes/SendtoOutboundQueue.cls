public without sharing class SendtoOutboundQueue{
    public boolean redirectBoolean {get;set;}
    Lead lead;
    public SendtoOutboundQueue(ApexPages.StandardController stdController)
    {
         lead= (Lead)stdController.getRecord();
        
    }
   public void loadMessages()
   {
          
     QueueSobject q = [Select Id, q.Queue.Name,q.Queue.ID from QueueSobject q WHERE q.Queue.Name = 'Outbound Telesales' Limit 1];
       
     System.debug(q.Queue.id);
     System.debug(q.Queue.name);
       
     try
     {
     if(q!=null)
     {
       lead.Ownerid = q.Queue.id;
       update lead;
        redirectBoolean=true;
     }
     }
     catch(Exception e)
     {
          redirectBoolean=false;
         
     }
   }

}