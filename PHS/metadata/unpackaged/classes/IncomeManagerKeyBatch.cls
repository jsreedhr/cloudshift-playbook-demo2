public  class IncomeManagerKeyBatch{
    
}
/*
global class IncomeManagerKeyBatch implements Database.Batchable < sObject >  {
   // Map < Id, Account > accountMap = new Map < Id, Account > ();
    Map < Id, Set < Id >> phsChildrenMap = new Map < Id, Set < Id >> ();
    
    Map<String, KAM_Batch_Record__c> accountMap = new Map<String, KAM_Batch_Record__c>();
   // Integer updatedAccountsCount = 0;
   // Integer updatedIncomeAccountsCount = 0;
    global IncomeManagerKeyBatch(Map<String, KAM_Batch_Record__c> accountMap) {
        this.accountMap = accountMap;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set < String > accountIds = new Set < String > (accountMap.keySet());
        String strIncome = 'IncomeManagerLocation';
        String query = 'Select Child_Account__c, Parent_Account__c From PHS_Account_Relationship__c Where isDeleted = FALSE and Relationship_Type__c =:strIncome and Parent_Account__c NOT IN :accountIds ';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List < PHS_Account_Relationship__c > phsIncomeList) {
        Set < Id > exploredAccountSet = new Set < Id > ();
        Set < Account > accountUpdateSet = new Set < Account > ();
        if (accountMap == NULL || accountMap.values().size() == 0)
            return;

       // System.debug('--Income--' + phsIncomeList);
        for (PHS_Account_Relationship__c phs: phsIncomeList) {
            if (!exploredAccountSet.contains(phs.Parent_Account__c)) {

                if (accountMap.containsKey(phs.Child_Account__c)) {
                    KAM_Batch_Record__c accKamValue = accountMap.get(phs.Child_Account__c);
                    Account parentAccount = new Account(
                        Id = phs.Parent_Account__c,
                        Test_Key_Account__c = TRUE,
                        Test_Key_Account_Role__c = accKamValue.Key_Account_Role__c,
                        Test_Key_Account_Type__c = accKamValue.Key_Account_Type__c
                    );
                    accountUpdateSet.add(parentAccount);
                    exploredAccountSet.add(parentAccount.id);
                }

            }
        }

        //Update 
        try {
            List < Account > accountList = new List < Account > (accountUpdateSet);
            update accountList;
           // updatedIncomeAccountsCount += accountList.size();
          //  System.debug('--Updated account count--' + updatedIncomeAccountsCount);

        } catch (Exception ex) {
            System.debug('Error while updating account data in Income-Manager-Key-Batch : ' + ex);
        }

    }
    global void finish(Database.BatchableContext bc) {

    }
}
*/