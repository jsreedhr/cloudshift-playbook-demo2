/**********************************************************************
* bg_QueueableJob:
*
* Class to make ivove a queueable job
* Created By: SA
* Created Date: 22-03-2016
*
* Changes: 
***********************************************************************/
public with sharing class bg_QueueableJob {

    public class QueueableJobClassException extends Exception {}

    public class QueueableCalloutJob {

        String type;
		Map<Id, SObject> soMap;
        Map<Id, SObject> soId2relatedSoMap;

	    public QueueableCalloutJob(String type, Map<Id, SObject> soMap, Map<Id, SObject> soId2relatedSoMap) {
            this.type = type;
	        this.soMap = soMap;
            this.soId2relatedSoMap = soId2relatedSoMap;
	    }
    }

    public class QueueableUpdateJob {

		List<Case> caseRecordsToUpdate;

	    public QueueableUpdateJob(List<Case> caseRecordsToUpdate) {
	        this.caseRecordsToUpdate = caseRecordsToUpdate;
	    }
    }
    

	public class QueueableJobClass implements Queueable,  Database.AllowsCallouts {

		QueueableCalloutJob cjob;
		QueueableUpdateJob uJob;
        List<String> errorMessages = new List<String>();

        public QueueableJobClass(QueueableCalloutJob cjob, QueueableUpdateJob uJob) {
            this.cjob = cjob;
            this.uJob = uJob;
        }

        public void execute(QueueableContext context) {

			Map<Id, Case> caseRecordsToUpdate;

            /*
             * get Milestone data to update
             */
        	if(uJob != null && !uJob.caseRecordsToUpdate.isEmpty())
            {
             	caseRecordsToUpdate  = bg_Case_Helper.caseMilestoneDataUpdate(uJob.caseRecordsToUpdate);
            }
         	else
            {
         		caseRecordsToUpdate = new Map<Id, Case>();
            }

            /*
             * Update case comment latest date
             */
            Map<Id, Case> rCaseRecordsToUpdate = new Map<Id, Case>();


            /*
             * dispatch callouts & merge results into updateMap
             */
            Map<Id, sObject> serviceCaseRecordsToUpdate = new Map<Id, sObject>();
            serviceCaseRecordsToUpdate = bg_WildebeestCallout.processWebServiceUpdates(cJob.type, cJob.soMap, cJob.soId2relatedSoMap, new Map<Id, Case>(caseRecordsToUpdate)/*, errorMessages*/);
            

            /*
             * Update any effected case records
             */
            if(serviceCaseRecordsToUpdate != null && !serviceCaseRecordsToUpdate.isEmpty())
            {   
                update serviceCaseRecordsToUpdate.values(); 
            }       
        }

	}	


    public static Id enqueueJob(QueueableJobClass job) {

		return System.enqueueJob(job);
    }


}