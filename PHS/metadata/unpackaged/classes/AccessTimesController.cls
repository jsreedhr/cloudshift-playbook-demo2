public without sharing class AccessTimesController {
          
    public string errOnPage {
        get;
        set;
    }
    public list <Access_Time__c> listPortLinks {
        get;
        set;
    }
    public list <Account> listAccount {
        get;
        set;
    }
    public string rowNo {
        get;
        set;
    }

    public List<SelectOption> timeList { 
        get;
        set;
    }

    public string fromTime {
        get;
        set;
    }

    public string toTime {
        get;
        set;
    }

    Public String AccountId {
        get;
        set;
    }
    
    Public String AgrLocId {
        get;
        set;
    }
    public integer noOfRows {
        get;
        set;
    }
    public List < booleanProjectWrapper > lstOfbooleanProjectWrapper {
        get;
        set;
    }
    public boolean selectAll {
        get;
        set;
    }
    public Boolean showerror {
        get;
        set;
    }
    public integer errorPos {
        get;
        set;
    }

    public String defaultComments{
        get;
        set;
    }

    public List<String> daysList;

   public List<String> selectedDefaultPickListValues {
        get; 
        set;
    }
    //constructor to get the records
    
    public AccessTimesController(apexpages.standardController stdController) {
        daysList = new List<String>{'1. Monday', '2. Tuesday', '3. Wednesday', '4. Thursday', '5. Friday'};
        errOnPage = '';
        selectedDefaultPickListValues = new List<String>();
        listAccount = new list <Account> ();
        timeList = new List<SelectOption>();
        listPortLinks = new list <Access_Time__c> ();
        lstOfbooleanProjectWrapper = new List < booleanProjectWrapper > ();
        selectAll = false;
        noOfRows = 1;
        AgrLocId = apexpages.currentpage().getparameters().get('Id');
        List <Agreement_Location__c> listAgreementLocation = [select Id,Location_Salesforce_Account__c from Agreement_Location__c where id=:AgrLocId];
       
       if( listAgreementLocation.size()>0)
        {
         AccountId=listAgreementLocation[0].Location_Salesforce_Account__c;
            System.debug('----ac--'+AccountId);
        listAccount = [select ID, Name from Account where id =:AccountId limit 1];
                System.debug('----listAccount--'+listAccount);
        listPortLinks = [select Id, Account__c, Agreement__c, Agreement__r.Contract_Type__c, Day__c, Division__c, From__c, To__c, PHS_Account__c,PHS_Account__r.Account_Name__c, Product_Line_Item__c, Comments__c From Access_Time__c where Account__c =:AccountId ORDER BY Day__c limit 4999];
        }
        system.debug('listPortLinks----->'+listPortLinks);
        for (Access_Time__c obj_portlink: listPortLinks) {
            booleanProjectWrapper newBoolWrapper = new booleanProjectWrapper(false, obj_portlink);
            if(obj_portlink.Division__c != null) {
            String division = String.valueOf(obj_portlink.Division__c);
            newBoolWrapper.selectedPickListValues = division.split(';');
            }
            lstOfbooleanProjectWrapper.add(newBoolWrapper);
        }

        for(integer i = 0; i < 24; i++){
            string hours = '';
            if(i < 10) {
                hours = '0' + String.valueOf(i);
            }
            else {
                hours = String.valueOf(i);
            }
            for(integer j = 0; j < 50; j+=15) {
                
                String mins = '';
                if(j < 10) {
                    mins = '0' + String.valueOf(j);
                }
                else {
                    mins = String.valueOf(j);
                }
                
                String timeValue = hours + ':' + mins;
                timeList.add(new SelectOption(timeValue, timeValue));
                
            }
        }
    }

    // To Create a new access time record on click of Add row button

    public void defaultFromTime(){
        for(booleanProjectWrapper bool: lstOfbooleanProjectWrapper){
            bool.objport.From__c = fromTime;
        }
    }

    public void defaultDivisionsPicked(){
        for(booleanProjectWrapper bool: lstOfbooleanProjectWrapper) {
            bool.selectedPickListValues = selectedDefaultPickListValues;
            //bool.selectedPickListValues.addAll(selectedDefaultPickListValue);

        }
    }

    public void defaultToTime(){
        for(booleanProjectWrapper bool: lstOfbooleanProjectWrapper){
            bool.objport.To__c = toTime;
        }
    }

    public void defaultCommentsSave(){
        for(booleanProjectWrapper bool: lstOfbooleanProjectWrapper){
            bool.objport.Comments__c = defaultComments;
        }
    }

    public List<SelectOption> getPickListOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        List<String> pickListValuesList = getPicklistValues('Access_Time__c', 'Division__c');
        for(String pick : pickListValuesList){
            Options.add(new SelectOption(pick,pick));
        }
        return Options;
    }

    public Void addRow() {
        selectAll = false;
        for (integer i = 0; i < 1; i++) {
            Access_Time__c objportlink = new Access_Time__c();
            objportlink.Account__c = AccountId;
            objportlink.From__c = '06:00';
            objportlink.To__c = '18:00';

            listPortLinks.add(objportlink);
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));
        }
    }

    public Void addFiveRows() {
        selectAll = false;
        for (integer i = 0; i < 5; i++) {
            Access_Time__c objportlink = new Access_Time__c();
            objportlink.Account__c = AccountId;
            objportlink.From__c = '06:00';
            objportlink.To__c = '18:00';
            objportlink.Day__c = daysList[i];

            listPortLinks.add(objportlink);
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));
        }
    }
    
    public void selectAll() {

        if (selectAll == true) {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = false;
            }
        }
    }
    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name) { 

        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        for(Schema.PicklistEntry a : pick_list_values) 
        { //for all values in the picklist list
          lstPickvals.add(a.getValue());//add the value  to our final list
        }

        return lstPickvals;
    }

    Public void deleteSelectedRows(){
            // selectAll = false;
            list <Access_Time__c> toDeleteRows = new list <Access_Time__c> ();
            for (integer j = (lstOfbooleanProjectWrapper.size() - 1); j >= 0; j--) {
                if (lstOfbooleanProjectWrapper[j].isSelected == true) {
                    if (lstOfbooleanProjectWrapper[j].objport.id != null) {
                        toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                    }
                    lstOfbooleanProjectWrapper.remove(j);
                }
            }
            delete toDeleteRows;
        }
        /**
        *   Method Name:    DelRow
        *   Description:    To delete the record by passing the row no.
        *   Param:  RowNo

        */

    public Void delRow() {

            selectAll = false;
            system.debug('--------' + RowNo);
            list <Access_Time__c> todelete = new list <Access_Time__c> ();

            if (lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null) {
                todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);

            }


            lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
            delete todelete;


        }
        /**
         *   Method Name:    saveAccess_Time__c
         *   Description:    To save the records and then redirect it to respective opportunity
         */

    public PageReference saveAccessTime() {
        if (showerror != TRUE) {
            List <Access_Time__c> listToUpsert = new List <Access_Time__c> ();
            system.debug('---listtoupsert-->' +listToUpsert);
            Boolean isBlank = FALSE;

            for (booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper) {
                if (objportlink.objport.Account__c == NULL || objportlink.objport.Day__c == NULL){
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'Please ensure the fields Account & Day have been completed'));
                        return null;

                } else {
                    errOnPage = '';
                    if(!objportlink.selectedPickListValues.isEmpty()) {           
                        objportlink.objport.Division__c = String.join(objportlink.selectedPickListValues, ';');
                    }
                    listToUpsert.add(objportlink.objport);
                }
            }
            if (listToUpsert.size() > 0) {
                try {
                    Upsert listToUpsert;
                   // return new PageReference('/' + AccountId);
                   return new PageReference('/' + AgrLocId);
                   


                } catch (Exception e) {
                    e.getMessage();
                    return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'No records found'));
                return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'There is missing information on the access time records.'));
             return null;
        }

    }


    public class booleanProjectWrapper {
        public Boolean isSelected {
            get;
            set;
        }
        public Access_Time__c objport {
            get;
            set;
        }
        public integer ValuetoList {
            get;
            set;
        }

        public List<String> selectedPickListValues{
            get;
            set;
        }


        public booleanProjectWrapper(boolean isSelect, Access_Time__c objports) {
            objport = objports;
            isSelected = isSelect;
            ValuetoList = 1;
            selectedPickListValues = new List<String>();

        }
    }

}