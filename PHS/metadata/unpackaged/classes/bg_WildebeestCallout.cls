/**********************************************************************
* bg_WildebeestCallout:
*
* Class to make Wildebeest callouts
* Created By: SA
* Created Date: 22-03-2016
*
* Changes: KH 16-11-16 Updates to include Integration Logging
*          KH 05-12-16 Updated createIntegrationLog to be @future
*          KH 29-12-16 Merged with CPQ Project Functionality
*          KH 19-05-17 Updates to createIntegrationLog 
*          KH 07-06-17 Updates to createIntegrationLog
***********************************************************************/

public with sharing class bg_WildebeestCallout
{
    public class WildeBeestClassException extends Exception {}

    public static Boolean loggingEnabled; // To allow Integration Logs to be turned off

    static public Boolean settingAreValid(Map<Id, SObject> soMap)
    {
        Boolean isValid = true;

        if(Wildebeest_Integration__c.getValues('Host') == null) 
        {
            isValid = false;
        }

        Glogbal_Settings__c globalSettings = Glogbal_Settings__c.getInstance(UserInfo.getUserId());
        Boolean isSFAPIUser = globalSettings != null && globalSettings.Is_SF_API_User__c == True;
        Boolean isDisable = Boolean.valueOf(Wildebeest_Integration__c.getValues('Host').get('Http_Callout_Disable__c') == True);
        loggingEnabled = Boolean.valueOf(Wildebeest_Integration__c.getValues('Host').get('Enable_Logging__c') == True);

        if(isSFAPIUser || isDisable) 
        {
            isValid = false;
        }

        Integer maxLimit = Integer.valueOf(Wildebeest_Integration__c.getValues('Host').get('Http_Callout_Max_Limit__c'));
        Integer maxCallouts = Limits.getLimitCallouts();

        if(soMap != null && (soMap.size() > maxLimit || soMap.size() > maxCallouts) && !Test.isRunningTest()) 
        {
            isValid = false;
        }

        return isValid;
    }

    static public Map<Id, SObject> processWebServiceUpdates(String type, Map<Id, SObject> soMap, Map<Id, SObject> soId2relatedSoMap, Map<Id, Case> updateMap)
    {    
        try 
        {
            Boolean recordsAreReadyToProcess = soMap != null && !soMap.isEmpty();
            Boolean calloutsLessThanLimit = Limits.getCallouts() <= Limits.getLimitCallouts();

            if(settingAreValid(soMap) && recordsAreReadyToProcess && Limits.getCallouts() <= Limits.getLimitCallouts() || Test.isRunningTest())
            {
                String token  = login();
                Boolean loginHasWorked = token != null;
                if(loginHasWorked && recordsAreReadyToProcess || Test.isRunningTest()) 
                {
                    Map<String, String> resMap = new Map<String,String>();

                    Integer maxLimit = Integer.valueOf(Wildebeest_Integration__c.getValues('Host').get('Http_Callout_Max_Limit__c'));
                    for(Id id : soMap.keySet()) 
                    {
                        Boolean maxLimitExceeded = Limits.getCallouts() <= maxLimit;
                        Boolean salesforceMaxLimitExceeded = Limits.getCallouts() <= Limits.getLimitCallouts();
                        String wildebeestId;
                        String caseID;

                        if(maxLimitExceeded && salesforceMaxLimitExceeded || Test.isRunningTest())
                        {   
                            system.debug('**ID**: ' + id);
                            system.debug('**soId2relatedSoMap**: ' + soId2relatedSoMap);
                            system.debug('**soMap**: ' + soMap);
                            //String wildebeestId = type == 'Case' ? (String)soMap.get(id).get(bg_Constants.CASE_REF_FIELD) : (String)soId2relatedSoMap.get(id).get(bg_Constants.CASE_REF_FIELD);
                            if (type == 'Case')
                            {
                                wildebeestId = (String)soMap.get(id).get(bg_Constants.CASE_REF_FIELD);
                                caseID = id;
                            }
                            else 
                            {
                                wildebeestId = (String)soId2relatedSoMap.get(id).get(bg_Constants.CASE_REF_FIELD);
                                caseID = soId2relatedSoMap.get(id).Id;
                            }
                            resMap.put(id, send(type, wildebeestId, bg_Serializer.toJsonStr(soMap.get(id), type), token, id, caseID));
                        }
                        else
                        {
                            if(!Test.isRunningTest())
                            {
                                throw new WildeBeestClassException('Http Callout Max Limit');
                            }
                        }
                    }

                    return processResults(resMap, soId2relatedSoMap, updateMap);
                }
            }
        } 
        catch(Exception e) 
        {
            System.debug('HttpQueueableClass error: ' + e.getMessage());
            throw e;
        }

        return updateMap;
    }

    /*
        Method to take the SObject ID and and return the type of Object it is
    */
    static public String getType(Id soId)
    {
        try 
        {
            return soId.getSObjectType().getDescribe().getName();
        } 
        catch(System.SObjectException e) 
        {

        }

        return '';
    }

    /*
        Method to return the CaseID from an SObject Map
    */
    static public Id getCaseId(Id soId, Map<Id, SObject> soId2relatedSoMap)
    {

        try 
        {
            if(soId.getSObjectType().getDescribe().getName() == Case.getSObjectType().getDescribe().getName()) 
            {
                return soId;
            }
        } 
        catch(System.SObjectException e) 
        {
            // ignore, fails to recognise CaseComment Id as CaseComment is an instnace of templace CaseObjectComment
        }

        if(soId2relatedSoMap.containsKey(soId)) 
        {
            return soId2relatedSoMap.get(soId).Id;
        }

        return null;
    }

    /*
        Method to return a list of Case IDs for a given map of IDs and SObjects
    */
    static public List<Id> getCaseIdList(Map<String, String> resMap, Map<Id, SObject> soId2relatedSoMap) {

        List<Id> idList = new List<Id>();
        for(String id :resMap.keySet()) 
        {
            idList.add(getCaseId(id, soId2relatedSoMap));
        }

        return idList;
    }

    
    static public Map<Id, SObject> getCurrentValueMap(Map<String, String> resMap, Map<Id, SObject> soId2relatedSoMap) {

        List<Id> caseIdList = getCaseIdList(resMap, soId2relatedSoMap);

        String soql = String.format(bg_Constants.INTEGRATION_CASE_QUERY_TEMPLATE, new List<String> {String.join(caseIdList, '\',\'')});
        System.debug(soql);

        List<SObject> soList = Database.query(soql);
        Map<Id, SObject> soMap = new Map<Id, SObject>();
        for(SObject so : soList) 
        {
            soMap.put(so.Id, so);
        }

        return soMap;
    }
    
    static public boolean updateIfRequired(SObject so1, SObject so2) {

        boolean soUpdate = false;

        Boolean newValueIsntBlank = so2.get(bg_Constants.CASE_REF_FIELD) != null && so2.get(bg_Constants.CASE_REF_FIELD) != '';
        Boolean caseRefNeedsUpdating = newValueIsntBlank && so1.get(bg_Constants.CASE_REF_FIELD) != so2.get(bg_Constants.CASE_REF_FIELD);

        if(caseRefNeedsUpdating) 
        {
            so1.put(bg_Constants.CASE_REF_FIELD, so2.get(bg_Constants.CASE_REF_FIELD));
            soUpdate = true;
        }

        if(so1.get(bg_Constants.WILDEBEEST_MESSAGE_FIELD) != so2.get(bg_Constants.WILDEBEEST_MESSAGE_FIELD)) 
        {
            so1.put(bg_Constants.WILDEBEEST_MESSAGE_FIELD, so2.get(bg_Constants.WILDEBEEST_MESSAGE_FIELD));
            soUpdate = true;
        }

        return soUpdate;
    }

    static public boolean updateIfRequired(SObject so, bg_ResponseWrapper rw)
    {
        boolean soUpdate = false;

        Boolean newValueIsntBlank = rw.EntityId != null && rw.EntityId != '';

        if(so.get(bg_Constants.CASE_REF_FIELD) != rw.EntityId && newValueIsntBlank) 
        {
            so.put(bg_Constants.CASE_REF_FIELD, rw.EntityId);
            soUpdate = true;   
        }

        if(so.get(bg_Constants.WILDEBEEST_MESSAGE_FIELD) != rw.Message) 
        {
            so.put(bg_Constants.WILDEBEEST_MESSAGE_FIELD, rw.Message);
            soUpdate = true;
        }

        return soUpdate;
    }

    static public boolean updateIfRequired(SObject so, String msg)
    {
        boolean soUpdate = false;

        if(so.get(bg_Constants.WILDEBEEST_MESSAGE_FIELD) != msg) 
        {
            so.put(bg_Constants.WILDEBEEST_MESSAGE_FIELD, msg);
            soUpdate = true;
        }

        return soUpdate;
    }

    static public boolean upsertMap(Map<Id, SObject> updateMap, Case caseRecord)
    {
        SObject uCaseRecord = updateMap.get(caseRecord.Id);
        if(uCaseRecord != null) {
            updateIfRequired(uCaseRecord, caseRecord);
            return false;
        }
        else {
            updateMap.put(caseRecord.Id, caseRecord);
            return true;
        }

    }


    static public Map<Id, SObject> updateMap(Map<String, String> resMap, Map<Id, SObject> soId2relatedSoMap, Map<Id, SObject> updateMap)
    {
        Map<Id, SObject> currentValueMap = getCurrentValueMap(resMap, soId2relatedSoMap);

        for(String id :resMap.keySet()) 
        {
            Id caseId = getCaseId(id, soId2relatedSoMap);
            Case caseRecord = (Case) currentValueMap.get(caseId);
            try 
            {
                bg_ResponseWrapper r = parse(resMap.get(id));

                if(updateIfRequired(caseRecord, r)) 
                {
                    upsertMap(updateMap, caseRecord);
                }

            } 
            catch (Exception e) 
            {
                String error = 'Unexpected server response. Please pass this message to an administrator [' + e.getMessage() + ']';
                if(updateIfRequired(caseRecord, error)) 
                {
                    upsertMap(updateMap, caseRecord);
                }
            }

        }

        return updateMap;
    }


    static public Map<Id, SObject> processResults(Map<String, String> resMap, Map<Id, SObject> soId2relatedSoMap, Map<Id, SObject> updateMap)
    {
        Map<Id, SObject> rUpdateMap = updateMap(resMap, soId2relatedSoMap, updateMap);

        return rUpdateMap;
    }

    /*
        Parsing the response for Existing Contract Lines from Wildebeest
    */
    public static bg_phsAccountInfoResponse parsePHSAccountInfo(String response)
    {
        bg_phsAccountInfoResponse phsAccountInfo = (bg_phsAccountInfoResponse)JSON.deserialize(response, bg_phsAccountInfoResponse.class);
        return phsAccountInfo;
    }

    /*
        Parsing the response for inserts and updates of Cases, Case Comments and Tasks into Wildebeest
    */
    static private bg_ResponseWrapper parse(String response)
    {
        bg_ResponseWrapper r = (bg_ResponseWrapper)JSON.deserializeStrict(response, bg_ResponseWrapper.class);        
        return r;
    } 
 

    /*
        Method to authenticate with Wildebeest to acquire a security token which is then used for any further callouts (remains valid for approximately 90 mins)
    */
    static public String login()
    {
        Wildebeest_Integration__c integrationSettings = Wildebeest_Integration__c.getInstance('Host');

        String username = integrationSettings.Http_Callout_Username__c;
        String password = integrationSettings.Http_Callout_Password__c;
        String endpoint = integrationSettings.Http_Callout_Endpoint__c;
        String query = integrationSettings.Http_Callout_Query__c;

        Http http = new Http();   
        HttpRequest req = getHttpRequest(endpoint+'/'+query+'/Authorise', 'POST', null);
        HttpResponse res;
        
        req.setBody('{"Username": "' + username + '","Password" : "' + password + '"}');

        try 
        {
            res = http.send(req);
            return res.getBody().replaceAll('"', '');
        }
        catch (Exception e) 
        {
            if (loggingEnabled)
            {
                //createIntegrationLog(null, false, null, 'Login', req.getBody(), req.getEndpoint(), res.getBody(), res.getStatus(), res.getStatusCode(), res.getHeader('Warning'), e.getMessage(), e.getStackTraceString(), e.getTypeName());
                createIntegrationLog(null, null, false, null, 'Login', req, res, e);
            }
        }

        return null;
    }

    /*
        Method to make the HTTP Callout to Wildebeest for the various integrations (Cases, Case Comments, Tasks and Existing Contract Lines)
    */
    static public String send(String type, String id, String jsonStr, String token, Id recordID, Id parentCaseID)
    {
        HttpResponse res;
        Exception exc;
        Boolean success = false; 

        Http http = new Http();
        String endpoint = String.valueOf(Wildebeest_Integration__c.getValues('Host').get('Http_Callout_Endpoint__c'));
        String query = String.valueOf(Wildebeest_Integration__c.getValues('Host').get('Http_Callout_Query__c'));

        String method = 'POST';
        endpoint += '/'+query+'/'+type;
        if(id != null) 
        {
            endpoint += '/'+id;
            method = 'PUT';
        }

        HttpRequest req = getHttpRequest(endpoint, method, token);
        req.setBody(jsonStr);
        
        try 
        {
            res = http.send(req);
            system.debug('**res**: ' + res);
            Map<String, Object> response = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            if (response.containsKey('Success'))
            {
                success = (Boolean)response.get('Success');
            }
            return res.getBody();
        }
        catch (Exception e) 
        {
            exc = e;
            system.debug('**exc**: ' + exc);
        }
        finally 
        {
            system.debug('**finally**: ' + loggingEnabled);
            system.debug('**Req**: ' + req);
            system.debug('**Res**: ' + res);
            system.debug('**Exc**: ' + exc);
            if (loggingEnabled)
            {                  
                /*
                if (req != null && res != null && exc != null)
                {
                    createIntegrationLog(recordID, success, type, 'Send', req.getBody(), req.getEndpoint(), res.getBody(), res.getStatus(), res.getStatusCode(), res.getHeader('Warning'), exc.getMessage(), exc.getStackTraceString(), exc.getTypeName());
                }
                else if (req != null && res != null)
                {
                    createIntegrationLog(recordID, success, type, 'Send', req.getBody(), req.getEndpoint(), res.getBody(), res.getStatus(), res.getStatusCode(), res.getHeader('Warning'), null, null, null);
                }
                else if (req != null)
                {
                    createIntegrationLog(recordID, success, type, 'Send', req.getBody(), req.getEndpoint(), null, null, null, null, null, null, null);
                }
                else
                {
                    createIntegrationLog(recordID, success, type, 'Send', null, null, null, null, null, null, null, null, null);
                }
                */
                createIntegrationLog(recordID, parentCaseID, success, type, 'Send', req, res, exc);
            }
        }
        return null;
    }

    /*
        Method to create an Integration Log record for Wildebeest Callouts (this is to help idenitfy any errors that may occur with the integrations). Records
        can be found in the UI via the Integration Tab and is only available to System Admins. 
    */
    /*
    @future
    public static void createIntegrationLog(Id recordID, Boolean success, String type, String calloutType, String reqBody, String reqEndpoint, String resBody, String resStatus, Integer resStatusCode, String resWarning, String excMessage, String excTrace, String excType)
    {
        system.debug('**@future**');
        Integration_Log__c log = new Integration_Log__c();

        log.Record_ID__c = recordID;        
        log.Success__c = success;    
        log.Object_Type__c = type;
        log.Callout_Type__c = calloutType;
        log.HTTP_Request_Body__c = reqBody;
        log.HTTP_Request_Endpoint__c = reqEndpoint;
        log.HTTP_Response_Body__c = resBody;
        log.HTTP_Response_Status__c = resStatus;
        log.HTTP_Response_Status_Code__c = resStatusCode;
        log.HTTP_Response_Warning__c = resWarning;
        log.Exception_Message__c = excMessage;
        log.Exception_Stack_Trace__c = excTrace;
        log.Exception_Type__c = excType;
        
        insert log;
    }
    */

    public static void createIntegrationLog(Id recordID, String parentCaseID, Boolean success, String type, String calloutType, HttpRequest req, HttpResponse res, Exception exc)
    {
        system.debug('**@future**');
        Integration_Log__c log = new Integration_Log__c();

        log.Record_ID__c = recordID;        
        log.Parent_Case__c = parentCaseID;
        log.Success__c = success;    
        log.Object_Type__c = type;
        log.Callout_Type__c = calloutType;
        if (req != null)
        {
            log.HTTP_Request_Body__c = req.getBody();
            log.HTTP_Request_Endpoint__c = req.getEndpoint();
        }
        if (res != null)
        {
            log.HTTP_Response_Body__c = res.getBody();
            log.HTTP_Response_Status__c = res.getStatus();
            log.HTTP_Response_Status_Code__c = res.getStatusCode();
            log.HTTP_Response_Warning__c = res.getHeader('Warning');
        }
        if (exc != null)
        {
            log.Exception_Message__c = exc.getMessage();
            log.Exception_Stack_Trace__c = exc.getStackTraceString();
            log.Exception_Type__c = exc.getTypeName();
        }
        // insert log; //commented because the response size is more then 131,072. 
    }
    
    static private HttpRequest getHttpRequest(String endpoint, String method, String token)
    {
        HttpRequest req = new HttpRequest();

        req.setEndpoint(endpoint);
        req.setMethod(method);
        req.setHeader(bg_Constants.CONTENT_TYPE_HEADER, bg_Constants.CONTENT_TYPE_VALUE);

        if(token != null) 
        {
            req.setHeader(bg_Constants.AUTH_TOKEN_HEADER, token);
        }

        return req;
    }
}