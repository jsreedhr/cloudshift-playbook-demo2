public class TestKAMHelperBatch{
    
}
/* COMMENTING FOR TEST COVERAGE
global class TestKAMHelperBatch implements Database.Batchable<sObject>{
    Map<String, KAM_Batch_Record__c> accountMap = new Map<String, KAM_Batch_Record__c>();
   // Integer updatedAccountsCount = 0;
    global TestKAMHelperBatch(Map<String, KAM_Batch_Record__c> accountMap){
        this.accountMap = accountMap;
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
        Set<String> accountIds = new Set<String>(accountMap.keySet());
         String queryString = 'Select Id, Name From Account Where Id IN: accountIds';
        return Database.getQueryLocator(queryString);
    }
    global void execute(Database.BatchableContext bc, List<Account> accountList){
        List<Account> accountUpdateList = new List<Account>();
         List<KAM_Batch_Record__c> KamTempaccountUpdateList = new List<KAM_Batch_Record__c>();
            for(Account account:accountList){
           //accountUpdateList.add(accountMap.get(account.Id));
           
           Account acc = new Account (
               Id=account.id,
               Test_Key_Account__c=accountMap.get(account.Id).Key_Account__c,
               Test_Key_Account_Role__c =accountMap.get(account.Id).Key_Account_Role__c,
               Test_Key_Account_Type__c = accountMap.get(account.Id).Key_Account_Type__c
               
               );
               
               
           accountUpdateList.add(acc);
           KAM_Batch_Record__c kamTemp = new KAM_Batch_Record__c(
               id = accountMap.get(account.Id).id
            );
           
           KamTempaccountUpdateList.add(kamTemp);
        }
        
        
        
        //System.debug('list size : ' + accountUpdateList.size());
          try{
            update accountUpdateList;
            delete KamTempaccountUpdateList;//accountMap.values();
           // updatedAccountsCount += accountUpdateList.size();
        }catch(Exception ex){
            System.debug('--Le Exception'+ex.getMessage());
        }
    }
    global void finish(Database.BatchableContext bc){
       
      
        
         IncomeManagerKeyBatch incomeBatch = new IncomeManagerKeyBatch(accountMap);
          Database.executeBatch(incomeBatch);
       
    }
}
*/