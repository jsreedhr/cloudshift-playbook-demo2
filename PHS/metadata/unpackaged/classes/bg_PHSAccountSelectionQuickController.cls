public with sharing class bg_PHSAccountSelectionQuickController {
	public Case theCase {get; set;}
	public List<String> errors {get; set;}
	public Boolean isSuccess {get; set;}
	public Id successCaseId {get; set;}


    public bg_PHSAccountSelectionQuickController(ApexPages.StandardController controller) {
    	if (theCase == null )
    	{
    		System.debug('Brightgen set case variables');
    		theCase = new Case(AccountId = controller.getId(),
    			               RecordTypeId = bg_RecordBuilder.getRecordType('Quick_Case', 'Case').Id);
    	}
        
        errors = new List<String>();
    }

    public List<SelectOption> PHSAccounts{
    	get {
    		System.debug('Brightgen bg_PHSAccountSelection_Helper.generatePHSAccountOptions(theCase.AccountId) [' + bg_PHSAccountSelection_Helper.generatePHSAccountOptions(theCase.AccountId) + ']');
	        return bg_PHSAccountSelection_Helper.generatePHSAccountOptions(theCase.AccountId);
	    }
	    set;
    }

    public pageReference goToCaseCreationScreen(){
    	System.debug('Brightgen theCase.PHS_Account__c [' + theCase.PHS_Account__c + ']');
    	System.debug('Brightgen theCase [' + theCase + ']');
    	errors = bg_PHSAccountSelection_Helper.generateErrorMessages(theCase);
    	PageReference newCasePageRef;
    	if(errors.isEmpty())
    	{
    		PHS_Account__c phsAccount = [select Name, Service_Enabled__c, Wildebeest_Ref__c from PHS_Account__c where Id = :theCase.PHS_Account__c];

	    	System.debug('Brightgen phsAccount [' + phsAccount + ']');

	    	try
	    	{
	    		insert theCase;
	    		isSuccess = true;
	    		successCaseId = theCase.Id;
	    		theCase = new Case(AccountId = theCase.AccountId);
	    	}
	    	catch (Exception ex)
	    	{
	    		System.debug('Brightgen ex.getTypeName() [' + ex.getTypeName() + ']');
	    		String message = ex.getMessage();
	    		if(message.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) 
	    		{
					String messageReason = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
					Integer indexOf = message.indexOf(messageReason);
					Integer actualStart = indexOf + messageReason.length() + 2;//2 removes the leading comma

	    			message = message.substring(actualStart, message.length());
	    			errors.add(message);
	    		}
	    		else
	    		{
	    			errors.add(ex.getMessage());
	    		}
	    	}
    	}
    	
    	return newCasePageRef;
    }

    public pageReference forceRefresh(){
    	return null;
    }
}