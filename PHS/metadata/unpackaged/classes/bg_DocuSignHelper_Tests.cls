/*************************************************
bg_DocuSignHelper_Tests

Test class for the DocuSign Processes

Author: Kash Hussain (BrightGen)
Created Date: 02/11/2016
Updates: TM - Added methods: checkContractSentDateIsUpdated
                             checkSignatureDateAndSignatureApprovalRequiredAreUpdated
         KH - Added method: checkDocuSignInvokeButton
**************************************************/
@isTest
private class bg_DocuSignHelper_Tests
{
    /*
        Tests that the Contract Sent Date is set when the Docusign status object is set to 'Sent'
    */
    static testMethod void checkContractSentDateIsUpdated()
    {

    	Account acct = bg_Test_Data_Utils.createAccount('Test Account');
	    insert acct;
        
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test Opp', acct.Id);
        testOpp.StageName = 'Open';
        testOpp.CloseDate = System.today();
        insert testOpp;

        Order testAgreement = bg_Test_Data_Utils.createAgreement(acct.Id, testOpp.Id, System.today(), 'Draft');
        insert testAgreement;

    	dsfs__DocuSign_Status__c testDocusignStatus = bg_Test_Data_Utils.createDocuSignStatus(testAgreement.Id, 'Sent');
    	insert testDocusignStatus;

    	testAgreement = [SELECT Id, Agreement_Sent_Date__c FROM Order WHERE Id =: testAgreement.Id];

    	System.assertEquals(System.today(), testAgreement.Agreement_Sent_Date__c);
    }

    /*
        Tests that the Contract Sent Date is set when the Docusign status object is set to 'Sent'
    */
    static testMethod void checkContractSentDateIsUpdatedNegative()
    {

        Account acct = bg_Test_Data_Utils.createAccount('Test Account');
        insert acct;
        
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test Opp', acct.Id);
        testOpp.StageName = 'Open';
        testOpp.CloseDate = System.today();
        insert testOpp;

        Order testAgreement = bg_Test_Data_Utils.createAgreement(acct.Id, testOpp.Id, System.today(), 'Draft');
        insert testAgreement;

        dsfs__DocuSign_Status__c testDocusignStatus = bg_Test_Data_Utils.createDocuSignStatus(testAgreement.Id, 'Completed');
        insert testDocusignStatus;

        testAgreement = [SELECT Id, Agreement_Sent_Date__c FROM Order WHERE Id =: testAgreement.Id];

        System.assertNotEquals(System.today(), testAgreement.Agreement_Sent_Date__c);
    }

    /*
        Tests that the Contract Sent Date is set when the Docusign status object is set to 'Sent'
    */
    static testMethod void checkContractSentDateIsUpdatedBulk()
    {

        Account acct = bg_Test_Data_Utils.createAccount('Test Account');
        insert acct;

        List<Opportunity> oppList = new List<Opportunity>();

        for(Integer i = 0; i<20; i++)
        {
            oppList.add(bg_Test_Data_Utils.createOpportunity('Test Opp' + i, acct.Id));
            oppList[i].StageName = 'Open';
            oppList[i].CloseDate = System.today();
        }

        insert oppList;

        List<Order> agreementList = new List<Order>();

        for(Integer i = 0; i<20; i++)
        {
            agreementList.add(bg_Test_Data_Utils.createAgreement(acct.Id, oppList[i].Id, System.today(), 'Draft'));
        }

        insert agreementList;

        List<dsfs__DocuSign_Status__c> docList = new List<dsfs__DocuSign_Status__c>();

        for(Integer i = 0; i<20; i++)
        {
            docList.add(bg_Test_Data_Utils.createDocuSignStatus(agreementList[i].Id, 'Sent'));
        }

        insert docList;

        System.assertEquals(20, [SELECT Id FROM dsfs__DocuSign_Status__c].size());

        List<Order> checkAgreementList = [SELECT Id, Agreement_Sent_Date__c FROM Order];

        for(Order agreement : checkAgreementList)
        {
            System.assertEquals(System.today(), agreement.Agreement_Sent_Date__c);            
        }
    }

    /*
        Tests that the Signature Date and Approval Required checkbox are set when the DocuSign Status record is set to 'Completed' 
    */
    static testMethod void checkSignatureDateAndSignatureApprovalRequiredAreUpdated()
    {

    	Account acct = bg_Test_Data_Utils.createAccount('Test Account');
	    insert acct;

        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test Opp', acct.Id);
        testOpp.StageName = 'Open';
        testOpp.CloseDate = System.today();
        insert testOpp;

    	Order testAgreement = bg_Test_Data_Utils.createAgreement(acct.Id, testOpp.Id, System.today(), 'Draft');
        insert testAgreement;

    	dsfs__DocuSign_Status__c testDocusignStatus = bg_Test_Data_Utils.createDocuSignStatus(testAgreement.Id, 'Sent');
    	insert testDocusignStatus;

    	testDocusignStatus.dsfs__Envelope_Status__c = 'Completed';
    	update testDocusignStatus;

    	testAgreement = [SELECT Id, Signature_Date__c, Signature_Approval_Required__c FROM Order WHERE Id =: testAgreement.Id];

    	System.assertEquals(System.today(), testAgreement.Signature_Date__c);
    	System.assertEquals(TRUE, testAgreement.Signature_Approval_Required__c);
    }

    /*
        Test to verify that the InvokeButton returns a URL
    */
    static testMethod void checkDocuSignInvokeButton()
    {
        Account acct = bg_Test_Data_Utils.createAccount('Test Account');
        insert acct;

        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test Opp', acct.Id);
        testOpp.StageName = 'Open';
        testOpp.CloseDate = System.today();
        insert testOpp;

        Order testAgreement = bg_Test_Data_Utils.createAgreement(acct.Id, testOpp.Id, System.today(), 'Draft');
        insert testAgreement;

        String dsInvokeURL = bg_DocuSignHelper.invokeDocuSign(testAgreement.Id, 'Sender FName', 'Sender LName', 'sender@mail.com', 'Signer FName', 'Signer LName', 'signer@mail.com');
        system.assertNotEquals(null, dsInvokeURL);
    }
}