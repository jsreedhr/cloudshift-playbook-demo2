public without sharing class CompetitorTriggerFunctions {
    
    public static void SetLostOpportunity(List <Competitor__c> lst_newcomps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Competitor__c n: lst_newcomps){
            IF(n.Lost_To__c == TRUE){
                IF(n.Opportunity__c !=null){
                    set_Oppids.add(n.Opportunity__c);
                }
            }
        }
        
        List <Opportunity> lst_opps = new List <Opportunity>([SELECT ID, Name, StageName, Sub_Closure_Reason__c FROM Opportunity WHERE ID in: set_Oppids]);
        
        IF(!lst_opps.isEmpty()){
            
            for(Opportunity opp: lst_opps){
                opp.StageName = 'Closed Lost';
                opp.Sub_Closure_Reason__c = 'Lost to Competitor';
                opp.Number_of_Lost_To_Competitors__c = 1;
            }
            update lst_opps;
        }
    }
}