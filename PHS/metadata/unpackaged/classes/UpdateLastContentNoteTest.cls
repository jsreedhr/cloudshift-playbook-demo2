/**
*  @Class Name:    UpdateLastContentNoteTest
*  @Description:   This is a test class for ContentNoteTriggerFunctions
*  @Company:       
*  CreatedDate:    01/11/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*                      01/11/2017                  Original Version
* Nitha T S            09/11/2017                  Last Modified
*/
@isTest
private class UpdateLastContentNoteTest{
    
    @isTest
    static void testcreatecontentnote(){
        
        List <String> lst_CDids = new List <String>();
         ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1'; 
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
        insert content;

      
        
        List <ContentVersion> lst_cds = new List <ContentVersion>([SELECT ID, ContentDocumentId FROM ContentVersion LIMIT 1]);
        System.debug(lst_cds.size());
            
            for(ContentVersion cd: lst_cds){
                lst_CDids.add(cd.ContentDocumentId);
            }
            
            String cdid = new List<String>(lst_CDids)[0];
        
        Test.startTest();
            
            Lead l = bg_Test_Data_Utils.createLead('string');
            insert l;
                   
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = l.id;
            cdl.ContentDocumentId = cdid;
             cdl.ShareType= 'V';
            insert cdl;
        
        Test.stopTest();
    }
}