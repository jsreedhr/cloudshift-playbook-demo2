/**
 *  Class Name: AccessTimeTriggerHandler  
 *  Description: This is a TriggerHandler for Access Time.
 *  Company: Standav
 *  CreatedDate:20/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             20/03/2018                 Original version
 */
public class AccessTimeTriggerHandler {
    
    // Sort a list of access time records after insert or update.
    public void sortAccessTime(List<Access_Time__c> accessTimeList){
        Set<Id> orderIdSet = new Set<Id>();
        Set<Id> accessTimeIdSet = new Set<Id>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        List<Access_Time__c> finalSortedChildList = new List<Access_Time__c>();
        for(Access_Time__c accessTime :accessTimeList){
            orderIdSet.add(accessTime.Agreement__c);
            accessTimeIdSet.add(accessTime.Id);
            
        }
        
        Map<Id, List<Access_Time__c>> parentChildAccessMap = new Map<Id, List<Access_Time__c>>();
        List<Access_Time__c> childAccessList = [Select Id, Name, Agreement__c, Comments__c, CustomSortOrder__c, Day__c, From__c, PHS_Account__c, PHS_Account__r.Name, 
                                                Product_Line_Item__r.OrderItemNumber, To__c From Access_Time__c Where Agreement__c IN :orderIdSet];

        for(Access_Time__c accessTime :childAccessList) 
        {
            if(parentChildAccessMap.containsKey(accessTime.Agreement__c))
            {
                parentChildAccessMap.get(accessTime.Agreement__c).add(accessTime);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                // accessTime.CustomSortOrder__c = 0;
                accessChildren.add(accessTime);
                parentChildAccessMap.put(accessTime.Agreement__c, accessChildren);
            }
        }

        for(Id orderRecordId :parentChildAccessMap.keySet()) {
            
            if(!parentChildAccessMap.get(orderRecordId).isEmpty() && parentChildAccessMap.get(orderRecordId) != null){
                sortedChildList = sortbyLocation(parentChildAccessMap.get(orderRecordId));
                parentChildAccessMap.put(orderRecordId, sortedChildList); 
                for ( Integer i= 0;i < sortedChildList.size(); i++){
                    sortedChildList[i].CustomSortOrder__c = i;
                    
                }
                finalSortedChildList.addAll(sortedChildList);
            }
        }
        
        if(phs_RecursiveCheck.runOnce())
        {
            //update the access time records
            try{
                update finalSortedChildList;
            }catch(Exception e){
                e.getMessage();
                system.debug('error----->'+ e.getMessage());
            }
        }
    }
    
    public void sortAccessTimeDelete(List<Access_Time__c> accessTimeList){
        
        Set<Id> orderIdSet = new Set<Id>();
        Set<Id> accessTimeIdSet = new Set<Id>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        List<Access_Time__c> finalSortedChildList = new List<Access_Time__c>();
        for(Access_Time__c accessTime :accessTimeList){
            orderIdSet.add(accessTime.Agreement__c);
            accessTimeIdSet.add(accessTime.Id);
            
        }
        
        Map<Id, List<Access_Time__c>> parentChildAccessMap = new Map<Id, List<Access_Time__c>>();
        List<Access_Time__c> childAccessList = [Select Id, Name, Agreement__c, Comments__c, CustomSortOrder__c, Day__c, From__c, PHS_Account__c, PHS_Account__r.Name, 
                                                Product_Line_Item__r.OrderItemNumber, To__c From Access_Time__c Where Agreement__c IN :orderIdSet AND Id NOT IN :accessTimeIdSet];

        for(Access_Time__c accessTime :childAccessList) 
        {
            if(parentChildAccessMap.containsKey(accessTime.Agreement__c))
            {
                parentChildAccessMap.get(accessTime.Agreement__c).add(accessTime);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                accessChildren.add(accessTime);
                parentChildAccessMap.put(accessTime.Agreement__c, accessChildren);
            }
        }
        
        for(Id orderRecordId :parentChildAccessMap.keySet()) {
            if(!parentChildAccessMap.get(orderRecordId).isEmpty() && parentChildAccessMap.get(orderRecordId) != null){
                
                sortedChildList = sortbyLocation(parentChildAccessMap.get(orderRecordId));
                parentChildAccessMap.put(orderRecordId, sortedChildList);  
                for ( Integer i= 0;i < sortedChildList.size(); i++){
                    sortedChildList[i].CustomSortOrder__c = i;
                    
                }
                finalSortedChildList.addAll(sortedChildList);
            }
        }
        
        if(phs_RecursiveCheck.runOnce())
        {
            //update the access time records
            try{
                update finalSortedChildList;
            }catch(Exception e){
                e.getMessage();
                system.debug('error----->'+ e.getMessage());
            }
        }
    }
    
    // Sort a list of access time records according to the location.
    public List<Access_Time__c> sortbyLocation(List<Access_Time__c> accessTimeList){
        
        Map<String,List<Access_Time__c>> accessFamilyMap = new Map<String,List<Access_Time__c>>();
        List<String> orderingList = new List<String>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        
        for(Access_Time__c access :accessTimeList) 
        {
            if(accessFamilyMap.containsKey(access.PHS_Account__r.Name))
            {
                accessFamilyMap.get(access.PHS_Account__r.Name).add(access);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                accessChildren.add(access);
                accessFamilyMap.put(access.PHS_Account__r.Name, accessChildren);
            }
        }
        
        
        //Add all elements of Set into a list
        orderingList.addAll(accessFamilyMap.keyset());
        //Sort the List
        orderingList.sort();
        
        for ( Integer i= 0;i < orderingList.size(); i++){
            sortedChildList.addall(sortbyProduct(accessFamilyMap.get(orderingList[i])));
            
        }

        //Get the sorted list
        return sortedChildList;
    }
    
    // Sort a list of access time records according to the product.
    public List<Access_Time__c> sortbyProduct(List<Access_Time__c> accessTimeList){
        
        Map<String,List<Access_Time__c>> accessFamilyMap = new Map<String,List<Access_Time__c>>();
        List<String> orderingList = new List<String>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        
        for(Access_Time__c access :accessTimeList) 
        {
            if(accessFamilyMap.containsKey(access.Product_Line_Item__r.OrderItemNumber))
            {
                accessFamilyMap.get(access.Product_Line_Item__r.OrderItemNumber).add(access);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                accessChildren.add(access);
                accessFamilyMap.put(access.Product_Line_Item__r.OrderItemNumber, accessChildren);
            }
        }
        
        
        //Add all elements of Set into a list
        orderingList.addAll(accessFamilyMap.keyset());
        //Sort the List
        orderingList.sort();
        
        for ( Integer i= 0;i < orderingList.size(); i++){
            sortedChildList.addall(sortbyDay(accessFamilyMap.get(orderingList[i])));
            
        }
 
        //Get the sorted list
        return sortedChildList;
    }
    
    // Sort a list of access time records according to the day.
    public List<Access_Time__c> sortbyDay(List<Access_Time__c> accessTimeList){
        
        Map<String,List<Access_Time__c>> accessFamilyMap = new Map<String,List<Access_Time__c>>();
        // Map<String,List<Access_Time__c>> accessFamilySortedMap = new Map<String,List<Access_Time__c>>();
        List<String> orderingList = new List<String>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        
        for(Access_Time__c access :accessTimeList) 
        {
            if(accessFamilyMap.containsKey(access.Day__c))
            {
                accessFamilyMap.get(access.Day__c).add(access);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                accessChildren.add(access);
                accessFamilyMap.put(access.Day__c, accessChildren);
            }
        }
        
        
        //Add all elements of Set into a list
        orderingList.addAll(accessFamilyMap.keyset());
        //Sort the List
        orderingList.sort();
        
        for ( Integer i= 0;i < orderingList.size(); i++){
            sortedChildList.addall(sortbyFromTime(accessFamilyMap.get(orderingList[i])));
            
        }
 
        //Get the sorted list
        return sortedChildList;
    }
    
    // Sort a list of access time records according to the from time.
    public List<Access_Time__c> sortbyFromTime(List<Access_Time__c> accessTimeList){
        
        Map<String,List<Access_Time__c>> accessFamilyMap = new Map<String,List<Access_Time__c>>();
        List<String> orderingList = new List<String>();
        List<Access_Time__c> sortedChildList = new List<Access_Time__c>();
        
        for(Access_Time__c access :accessTimeList) 
        {
            if(accessFamilyMap.containsKey(access.From__c))
            {
                accessFamilyMap.get(access.From__c).add(access);
            }
            else
            {
                List<Access_Time__c> accessChildren = new List<Access_Time__c>();
                accessChildren.add(access);
                accessFamilyMap.put(access.From__c, accessChildren);
            }
        }
        
        //Add all elements of Set into a list
        orderingList.addAll(accessFamilyMap.keyset());
        //Sort the List
        orderingList.sort();
        
        for ( Integer i= 0;i < orderingList.size(); i++){
            sortedChildList.addall(accessFamilyMap.get(orderingList[i]));
            
        }
 
        //Get the sorted list
        return sortedChildList;
    }
    
}