@isTest
private class phs_RecursiveCheck_Test {
    static testMethod void testRecursive() {
    	
    	Boolean testValue = phs_RecursiveCheck.runOnce();
        System.assertEquals(True, testValue);
       
        testValue = phs_RecursiveCheck.runOnce();
        System.assertEquals(False, testValue);   	
    	
    }
}