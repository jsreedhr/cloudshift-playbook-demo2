/********************************************************************************
* bg_ScheduledJob :
*
* Sceduled job for the unread emails flags
*
* Created By: Tom Morris
* Created Date: 01-04-2017
*
* Changes: 
*********************************************************************************/

global class bg_ScheduledJob implements Schedulable {

    global void execute(SchedulableContext sc) {

        runJob();
    }

    public static void runJob(){

        /*
        Section below removed so that the job is scheduled by a chron expression instead of within the job itself, this keeps the scheduled jobs list clean.

        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String minute = string.valueOf(system.now().minute() + 10);
        String second = string.valueOf(system.now().second());
        String year = string.valueOf(system.now().year());
        Integer minInt;
        Integer hourInt;
        Integer dayInt;
        Integer yearInt;
        
        if(Integer.valueOf(minute)>59)
        {
            minInt=Integer.valueOf(minute)-59;
            minute = String.valueOf(minInt);
            hourInt=Integer.valueOf(hour)+1;
            hour = String.valueOf(hourInt);
        }

        if(Integer.valueOf(hour)>23)
        {
            hourInt=Integer.valueOf(hour)-23;
            hour = String.valueOf(hourInt);
            dayInt=Integer.valueOf(day)+1;
            day = String.valueOf(dayInt);
        }

        if(Integer.valueOf(day)>364)
        {
            dayInt=Integer.valueOf(day)-364;
            day = String.valueOf(dayInt);
            yearInt=Integer.valueOf(year)+1;
            year = String.valueOf(yearInt);
        }

        String strJobName = 'Job-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        System.schedule(strJobName, strSchedule, new bg_ScheduledJob());

        */
        bg_EmailMessage_Helper.resetUnreadEmailFlag(generateQuery());
    }

    global bg_ScheduledJob() {}

    public static List<Case> generateQuery() {

    	return [Select Id From Case Where Unread_Emails__c=TRUE];
    }
}