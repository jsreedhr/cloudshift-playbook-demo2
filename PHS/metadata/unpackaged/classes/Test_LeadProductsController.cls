@isTest
public class Test_LeadProductsController {
/**
*  @Class Name:    Test_LeadProductsController 
*  @Description:   This is a test class for the LeadProductsController
*  @Company: CloudShift
*  CreatedDate: 16/11/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  David Kurtanjek     16/11/2017                  Original Version
*
*/
    
    private static testMethod void Test_LeadProductsController(){
        
        List<Lead> plotList = new List<Lead>();  
        
          Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook; 
        
         //Create a product
        Product2 p = bg_Test_Data_Utils.createaProduct2('New Product');
p.IsActive=true;
p.Target_Price__c=10.0;
        insert p;
                
        
        //Create Lead
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('3');
        insert u;
        
        
        //Pricebook
         /* PricebookEntry pbe = bg_Test_Data_Utils.createPricebookEntry(p.Id, standardPricebook.Id);
        pbe.IsActive = true;
        insert pbe;*/
        
        
        Lead objLead = bg_Test_Data_Utils.createLead('3');
        objLead.OwnerId = u.id;
        objLead.Price_Book__c = standardPricebook.id;
        insert objLead;
        
       
        //Create Lead Product
        Lead_Product__c objLP = new Lead_Product__c();
        objLP.Lead__c = objLead.id;
        //objLP.Quantity__c = 1;
        objLP.Product__c = p.id;
        objLP.CreatedDate = DateTime.newInstance(2017, 06, 17, 14, 30, 0);
        insert objLP;
        
        PageReference pageRef = Page.LeadProduct;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', objLead.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objLead);
        LeadProductsController classSC = new LeadProductsController(sc);
        classSC.checkduplicates();
        classSC.saveLeadProduct();
       // classSC.createRecords();
        classSC.addRow();
        classSC.checkduplicates();
        classSC.saveLeadProduct();
        classSC.selectAll();
        classSC.deleteSelectedRows();
        classSC.rowNo ='1';
        classSC.delRow();
        classSC.saveLeadProduct();
    }
    
    
}