global class FieldSetUtility {
    private static Map<String, Schema.SObjectType> gd;
    static {
        gd = Schema.getGlobalDescribe();
        System.debug('gd..........>'+gd);
    }
    
    public static List<FieldSetMemberWrapper> getFieldSetDetails(String objectApi, String fieldSetName) {
        
        fieldSetName = 'PHS_Account_Autocomplete';
        objectApi = 'PHS_Account__c';
        Schema.SObjectType sot = gd.get(objectApi);
        System.debug('sot lookup..........>'+sot);

        if(sot == null) return null;

        Schema.DescribeSObjectResult dsr = sot.getDescribe();
        Schema.FieldSet fs = dsr.FieldSets.getMap().get(fieldSetName);
        System.debug('fieldset fs;;;;;;;;>'+fs);
        System.debug('fieldsetMember fs;;;;;;;;>'+fs.getFields());
        System.debug('fieldPath fs;;;;;;;;>'+fs.getFields()[0].getFieldPath());
        List<FieldSetMemberWrapper> fieldSetDetails = new List<FieldSetMemberWrapper>();
        for(Schema.FieldSetMember fsm : fs.getFields()) {
            fieldSetDetails.add(new FieldSetMemberWrapper(String.valueOf(fsm.getDbRequired()), fsm.getFieldPath(), fsm.getLabel(), String.valueOf(fsm.getRequired())));
        }
        return fieldSetDetails;
    }
    
    global class FieldSetMemberWrapper {
        String dbRequired {get;set;}
        String fieldPath {get;set;}
        String label {get;set;}
        String required {get;set;}
        
        FieldSetMemberWrapper(String dbRequired, String fieldPath, String label, String required) {
            this.dbRequired = dbRequired;
            this.fieldPath = fieldPath;
            this.label  = label;
            this.required = required;
        }
    }
}