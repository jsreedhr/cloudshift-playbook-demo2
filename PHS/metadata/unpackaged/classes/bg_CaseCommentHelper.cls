/*************************************************
* bg_CaseCommentHelper
* 
* Helper class for the Case Comments
*
* Author: Kash Hussain (BrightGen)
* Created Date: 04/02/2017
* Updates: 
**************************************************/
public with sharing class bg_CaseCommentHelper
{
	/*
		Method to mark the First Response and Second Response Milestones as Completed
		if they are still open, when Case Comments are added to the Case.
	*/
	public static void completeResponseMilestones(List<CaseComment> newCaseComments)
	{
		Set<Id> caseIDS = new Set<Id>();
		DateTime completionDate = DateTime.now();

		for (CaseComment cc : newCaseComments)
		{
			caseIDS.add(cc.ParentId);
		}

		if (!caseIDS.isEmpty())
		{
			List<Case> ccCases = [SELECT Id, SlaStartDate, Status FROM Case WHERE Id IN :caseIds AND EntitlementId != Null AND SlaStartDate != Null AND SlaExitDate = null];
			
			if (!ccCases.isEmpty())
			{
				
				List<Id> updateCases = new List<Id>();

				for (Case ccCase : ccCases)
				{
					if ((ccCase.Status != 'Closed' || ccCase.Status == 'Closed (Spam Email)') && ccCase.SlaStartDate <= completionDate)
					{
						updateCases.add(ccCase.Id);
					}
				}

				if (!updateCases.isEmpty())
				{
					bg_MilestoneHelper.completeResponseMilestones(updateCases, completionDate);
				}
			}
		}
	}  
}