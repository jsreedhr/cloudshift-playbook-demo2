/*********************************************************************************
 * bg_caseControllerTest
 *
 * Test class for the case controller.   
 * 
 * Author: Ismail Basser- BrightGen Ltd
 * Created: 06-02-2016
 * 
 * Edited: 26-02-2018 -Ismail Basser - BrightGen Ltd
 * Added new test method 'casePageRefTestCM'
 *********************************************************************************/
@istest
public class bg_caseControllerTest
{
    
    	@testSetup static void setupDate()
    {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        globalSettings.Can_Edit_Closed_Cases__c = TRUE;
        update globalSettings;
	}

    //test to ensure that the to close checkbox is ticked and
    //navigates to correct page
    static testMethod void casePageRefTest()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
       
        System.runAs(userToRunAs)
        {
            Account testAcct = bg_Test_Data_Utils.createAccount('TestK');
            insert testAcct;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('K1', testAcct.Id);
            testPHSAccount.Wildebeest_Ref__c = '9876';
            insert testPHSAccount;
            
            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test',Subject = 'BG Test', Origin = 'Email', Wildebeest_Ref__c = '9876');
            insert caseRecord;
            
            EmailMessage emailMessageRecord = new EmailMessage(ParentId = caseRecord.Id, Status = '0', Incoming = true);
            insert emailMessageRecord;
            
            Case c = [Select Id,Email_ID__c, To_Close__c From Case Where Id = :caseRecord.Id];
      
        ApexPages.StandardController controller = new ApexPages.StandardController(c);
        bg_caseController extension = new bg_caseController(controller);

        Id cID = extension.caseId;
        Id eID = extension.emailId;
        String blankVal = '';
        PageReference result = extension.caseToClose();
            test.stopTest();
        System.assertEquals('/_ui/core/email/author/EmailAuthor?email_id='+eID+'&forward=1&p24=remittance%40phs.co.uk&p5=&retURL='+cID, result.getUrl());
		System.debug('***BGurl'+result.getUrl());
    	caseRecord = [SELECT Id, AccountId, To_Close__c, PHS_Account__c, PHS_Wildebeest_Ref__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];
		System.assertEquals(true, caseRecord.To_Close__c);
            }
    }
    
    static testMethod void casePageRefTestCM()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
       
        System.runAs(userToRunAs)
        {
            Account testAcct = bg_Test_Data_Utils.createAccount('TestK');
            insert testAcct;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('K1', testAcct.Id);
            testPHSAccount.Wildebeest_Ref__c = '9875';
            insert testPHSAccount;
            
            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test',Subject = 'BG Test', Origin = 'Email', Wildebeest_Ref__c = '9875');
            insert caseRecord;
            
            EmailMessage emailMessageRecord = new EmailMessage(ParentId = caseRecord.Id, Status = '0', Incoming = true);
            insert emailMessageRecord;
            
            Case c = [Select Id,Email_ID__c, To_Close__c From Case Where Id = :caseRecord.Id];
      
        ApexPages.StandardController controller = new ApexPages.StandardController(c);
        bg_caseController extension = new bg_caseController(controller);

        Id cID = extension.caseId;
        Id eID = extension.emailId;
        String blankVal = '';
        PageReference result = extension.CreditManagementClose();
            test.stopTest();
        System.assertEquals('/_ui/core/email/author/EmailAuthor?email_id='+eID+'&forward=1&p24=creditmanagement%40phs.co.uk&p5=&retURL='+cID, result.getUrl());
		System.debug('***BGurl'+result.getUrl());
    	caseRecord = [SELECT Id, AccountId, To_Close__c, PHS_Account__c, PHS_Wildebeest_Ref__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];
		System.assertEquals(true, caseRecord.To_Close__c);
            }
    }

}