public class HygienePageController {
    
    public List<SBQQ__QuoteLine__c> corequoteLineList{get;set;}
    public List<SBQQ__QuoteLine__c> trialProductLineList{get;set;}
    List<SBQQ__QuoteLine__c> quoteLineList2 = new  List<SBQQ__QuoteLine__c>();
    List<SBQQ__QuoteLine__c> quoteLineList3 = new  List<SBQQ__QuoteLine__c>();
    public decimal totalAmountcoreproducts{get;set;}
    public decimal totalAmounttrialproducts{get;set;}
    protected Id quoteId;

    public HygienePageController(){
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        System.debug('quoteId: '+quoteId);
        
        if(quoteId!=NULL){
            
           for(SBQQ__QuoteLine__c ql :[SELECT Id, Effective_Unit_Price__c, SBQQ__ProductName__c, PHSCPQ_Standard_Contract_Term__c, Standard_Service_Frequency_Formula__c, PHSCPQ_Actual_vs_Seasonal_Formula__c,  
            Agreement_Type__c, SBQQ__ListPrice__c, SBQQ__Quantity__c, SBQQ__NetTotal__c, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__NetAmount__c, Target_Price__c, Trial_Product__c, Trial_Length__c 
            FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:quoteId ORDER BY SBQQ__ProductName__c ASC]){
                
                if(ql.SBQQ__ListPrice__c!=null){
                    ql.SBQQ__ListPrice__c = ql.SBQQ__ListPrice__c.setScale(2,System.RoundingMode.UP);
                }
                
                IF(ql.Trial_Product__c == FALSE){
                    quoteLineList2.add(ql);                    
                }
                
                IF(ql.Trial_Product__c == TRUE){
                    quoteLineList3.add(ql);                    
                }                
            }
            
            if(!quoteLineList2.isEmpty()){
            corequoteLineList =quoteLineList2;
            totalAmountcoreproducts = corequoteLineList[0].SBQQ__Quote__r.SBQQ__NetAmount__c;
            }
            
            if(!quoteLineList3.isEmpty()){
            trialProductLineList =quoteLineList3;
            totalAmounttrialproducts = trialProductLineList[0].SBQQ__Quote__r.SBQQ__NetAmount__c;
            }
        }
    }
}