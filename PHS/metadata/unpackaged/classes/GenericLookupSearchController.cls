/**
 *  Class Name: GenericLookupSearchController  
 *  Description: This is a Controller for GenericLookupSearch page.
 *  Company: Standav
 *  CreatedDate:27/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             27/03/2018                 Original version
 */
public class GenericLookupSearchController {
    
    public String searchTerm;
    public String uniqueId {get; set;}
    public String objectLabel {get; set;}
    public List<DisplayObjectWrapper> displayObjWrapperList {get;set;}
    public List<String> fieldLabelList {get;set;}
    public String searchLookupApi {get;set;}
    public String CUSTOM_SETTINGS_NEW_LOOKUP = 'AutocompleteObjectFieldset__c';
    
    public String getSearchTerm() {
        return this.searchTerm;
    }
    public void setSearchTerm (String searchTerm) {
        this.searchTerm = searchTerm;
    }
    public Boolean showSearchList {get; set;}

    public GenericLookupSearchController() {
        showSearchList = false;
        searchTerm = '';
        getLookupSearchList('Account','Autocomplete_Account');
    }
    
    /* Method to query records using SOSL*/
    // @AuraEnabled
    public void getLookupSearchList(String objectAPIName, String fieldSetName){
    // public void getLookupSearchList(String objectAPIName){

        objectAPIName = String.escapeSingleQuotes(objectAPIName);
        
        /*CUSTOM_SETTINGS_NEW_LOOKUP = String.escapeSingleQuotes(CUSTOM_SETTINGS_NEW_LOOKUP);
        Map<String, Schema.SObjectType> GlobalDescribeMapCS = Schema.getGlobalDescribe(); 
        Schema.SObjectType CustomSettingType = GlobalDescribeMapCS.get(CUSTOM_SETTINGS_NEW_LOOKUP);
        Schema.DescribeSObjectResult DescribeCustomSettingResult = CustomSettingType.getDescribe();
        system.debug('DescribeCustomSettingResult fields---->'+DescribeCustomSettingResult.fields);*/
        

        
        // uniqueId = Apexpages.currentPage().getParameters().get('uniqueId');
        uniqueId ='80125000000RCDA';
        // searchTerm = Apexpages.currentPage().getParameters().get('searchText');
        searchTerm = 'we';

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectAPIName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        objectLabel = '';
        objectLabel = DescribeSObjectResultObj.getLabel();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        
        this.fieldLabelList = New List<String>();
        String queryString = '(Id';
        for(Schema.FieldSetMember fld :fieldSetObj.getFields()) {
         queryString += ', ' + fld.getFieldPath();
         fieldLabelList.add(fld.getLabel());
        }
        queryString = queryString+')';
        
        String searchQuery = 'FIND \'' + searchTerm + '*\' IN ALL FIELDS RETURNING ' + objectAPIName + queryString ;
        List<List<sObject>> sObjectList = Search.query(searchQuery);
        this.displayObjWrapperList = New List<DisplayObjectWrapper>();
        

        for(List<sObject> sObjList : sObjectList)
        {
            for(sObject sObjectVar : sObjList)
            {
                Integer i = 0;
                Integer size = fieldSetObj.getFields().size();
                DisplayObjectWrapper objWrapper = New DisplayObjectWrapper();
                objWrapper.objId = String.valueOf(sObjectVar.get('Id'));
                if((size -i)>0){
                    objWrapper.Field1 = String.valueOf(sObjectVar.get(fieldSetObj.getFields()[0].getFieldPath()));
                    i++;
                }
                if((size -i)>0){
                    objWrapper.Field2 = String.valueOf(sObjectVar.get(fieldSetObj.getFields()[1].getFieldPath()));
                    i++;
                }
                if((size -i)>0){
                    objWrapper.Field3 = String.valueOf(sObjectVar.get(fieldSetObj.getFields()[2].getFieldPath()));
                    i++;
                }
                if((size - i)>0){
                    objWrapper.Field4 = String.valueOf(sObjectVar.get(fieldSetObj.getFields()[3].getFieldPath()));
                    i++;
                }
                displayObjWrapperList.add(objWrapper);
            }    
        }
        showSearchList = true;
        system.debug('objectLabel---->'+objectLabel);
        system.debug('fieldLabelList---->'+fieldLabelList);
        system.debug('displayObjWrapperList---->'+displayObjWrapperList);
        return;
        
    }
    
    // Wrapper for the records
    public class DisplayObjectWrapper
    {
        public String objId{get;set;}
        public String Field1{get;set;}
        public String Field2{get;set;}
        public String Field3{get;set;}
        public String Field4{get;set;}
    }
}