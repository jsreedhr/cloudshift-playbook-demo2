/**
 * Description : Test CLass for bg_MilestoneHelper
 * Author : Swetha 
 * Date : 12-01-2018
 * Change Log :
 */
@isTest
private class bg_MilestoneHelper_Tests {
    // Create Test Data
    @testSetup static void setup() {
        
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        integrationSetting.Integration_Log_Purge_Limit__c = 5;
        if(integrationSetting == null)
        {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            insert integrationSetting;
        }
        
        Account testAcct = bg_Test_Data_Utils.createAccount('Test');
        insert testAcct;

        Contact testContact = bg_Test_Data_Utils.createContact('Test C', testAcct.Id);
        testContact.Email = 'test@milestone.com';
        insert testContact;

        Case testCase = new Case(AccountId = testAcct.Id, ContactId = testContact.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
        insert testCase;
        
        
    }
	private static testMethod void testcompleteAllMilestones() {
	    Case caseRec = [select id from case Limit 1];
	    Set<Id> caseIds = new Set<Id>{caseRec.Id};
        bg_MilestoneHelper.completeAllMilestones(caseIds);
	}

}