/*Test Class for ContactToLeadController
  Created on 23/10/2017
*/
@isTest 
private class Test_ContactToLeadController
{
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
        Contact contact = bg_Test_Data_Utils.createContact('1', acc.Id);
        insert contact;
    }
    
    @isTest
    static void testMethod1()
    {

      Contact contactObj = [Select LastName, AccountId, Description, Email, FirstName, HasOptedOutOfEmail, LeadSource, MobilePhone, Phone, Salutation, Title From Contact Where LastName = 'Test Contact 1'];
      Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController( contactObj );
        ContactToLeadController controller = New ContactToLeadController(sc);
        controller.contactObj = contactObj;
        controller.createLead();
      Test.stopTest();
    }
}