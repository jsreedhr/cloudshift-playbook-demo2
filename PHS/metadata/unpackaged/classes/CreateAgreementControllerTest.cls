/**
 *  @Class Name:    CreateAgreementControllerTest
 *  @Description:   This is a test class for CreateAgreementController
 *  @Company:       dQuotient
 *  CreatedDate:    11/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           11/12/2017                  Original Version
 */
@isTest
private class CreateAgreementControllerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        opp.Is_Quick_Contract__c = true;
        insert opp;
        
        Quote testQuote = bg_Test_Data_Utils.createQuote('Test Quote');
        testQuote.OpportunityId = opp.Id;
        insert testQuote;
        
        Product2 productObj = bg_Test_Data_Utils.createaProduct2('Test Product');
        insert productObj;
        
        PricebookEntry pbe = bg_Test_Data_Utils.createPricebookEntry(productObj.Id, standardPricebook.Id);
        pbe.IsActive = true;
        insert pbe;
        
        OpportunityLineItem testOppProduct = bg_Test_Data_Utils.createOpportunityLineItem(opp);
        testOppProduct.Product2Id = productObj.Id;
        testOppProduct.OpportunityId = opp.Id;
        testOppProduct.PricebookEntryId = pbe.Id;
        testOppProduct.Quantity__c = 2;
        testOppProduct.UnitPrice = 20;
        testOppProduct.Description = 'Test Description';
        insert testOppProduct;
    }
    
    private static testMethod void testReadyToConvertLead() {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Quote testQuote = [Select Id, OpportunityId From Quote Where Name = 'Test Quote' Limit 1];
        Opportunity oppo = [Select Id, Quote_Roll_Up__c ,Has_Synced_Quote__c,Product_Count__c,Is_Quick_Contract__c from Opportunity where OwnerId= :u.Id];
        system.debug('Oppo---->' +oppo);
        ApexPages.currentPage().getParameters().put('oppId', oppo.Id);

        test.startTest();
        system.runAs(u){
            CreateAgreementController sc = new CreateAgreementController(); 
            sc.loadAction();
        }
        test.stopTest();
	}
}