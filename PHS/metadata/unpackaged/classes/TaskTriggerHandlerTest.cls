/**
 *  @Class Name:    TaskTriggerHandlerTest
 *  @Description:   This is a test class for TaskTriggerHandler
 *  @Company:       dQuotient
 *  CreatedDate:    20/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           20/11/2017                  Original Version
 */
@isTest
private class TaskTriggerHandlerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        u.Username = Integer.valueOf(math.rint(math.random()*1000000)) +'qwert@org.com';
        insert u;
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('24');
        lead.OwnerId = u.id;
        lead.FLT_Postal_Code__c =flt.id;
        insert lead;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        CampaignMember campMem = bg_Test_Data_Utils.createCampaignMember(lead.id, camp.id);
        insert campMem;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        
        /*List<Task> taskList = new List<Task>();
        Task t1 = bg_Test_Data_Utils.createTask('Task 1', acc.Id);
        t1.WhoId = lead.id;
        t1.WhatId = null;
        t1.OwnerId = u.id;
        taskList.add(t1);
        
        Task t2 = bg_Test_Data_Utils.createTask('Task 2', acc.Id);
        t2.WhatId = opp.id;
        t2.WhoId = null;
        t2.OwnerId = u.id;
        taskList.add(t2);
        
        insert taskList;
    
        Event e = bg_Test_Data_Utils.createEvent('Event', acc.Id);
        e.WhatId = null;
        e.WhoId = lead.id;
        e.OwnerId = u.id;
        insert e;*/
    }
    
    private static testMethod void testinsertTask() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Account acc = [Select Name From Account Where Name = 'Test Account N'];
        Lead testLead = [Select Id from Lead where OwnerId= :u.Id];        
        Opportunity testOpp = [Select Id from Opportunity where AccountId= :acc.Id];
        List<Task> taskList = new List<Task>();
        
        Task t1 = bg_Test_Data_Utils.createTask('Task 1', acc.Id);
        t1.WhoId = testLead.id;
        t1.WhatId = null;
        t1.OwnerId = u.id;
        taskList.add(t1);
        
        Task t2 = bg_Test_Data_Utils.createTask('Task 2', acc.Id);
        t2.WhatId = testOpp.id;
        t2.WhoId = null;
        t2.OwnerId = u.id;
        taskList.add(t2);
        
        test.startTest();
            insert taskList;
            system.assertNotEquals(taskList, null);
        test.stopTest();
	}
}