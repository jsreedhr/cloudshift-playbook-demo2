public class GreenLeafController {
    
    public List<SBQQ__QuoteLine__c> quoteLineList{get;set;}
    
    public String productFamily{get;set;}
    public Decimal totalAmount{get;set;}
    protected Id quoteId;

    public GreenLeafController(){
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        System.debug('quoteId: '+quoteId);
        
        quoteLineList = [SELECT Id, Effective_Unit_Price__c, SBQQ__Product__c, SBQQ__Product__r.Name, SBQQ__Product__r.Product_Image_URL__c, 
        SBQQ__Product__r.Product_USP__c, SBQQ__Product__r.Family, SBQQ__Quote__c, SBQQ__Quote__r.SBQQ__NetAmount__c, 
        PHSCPQ_Standard_Contract_Term__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__NetPrice__c, SBQQ__Description__c
        FROM SBQQ__QuoteLine__c 
        WHERE SBQQ__Quote__c =:quoteId];
        System.debug('quoteLineList IMGURL: '+quoteLineList[0].SBQQ__Product__r.Product_Image_URL__c);
        
        
        if(!quoteLineList.isEmpty()){
            
            if(quoteLineList[0].SBQQ__Product__r.Family != NULL){
                productFamily = quoteLineList[0].SBQQ__Product__r.Family;
            }
            
            if(quoteLineList[0].SBQQ__Quote__r.SBQQ__NetAmount__c != NULL){
                totalAmount = quoteLineList[0].SBQQ__Quote__r.SBQQ__NetAmount__c;
            }
        }
        
    }
}