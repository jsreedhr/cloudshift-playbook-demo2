/*
*  Class Name:  ContractLinesForOpportunityController
*  Description: This class performs the logic and passes the Contract Line records
        to be displayed on ContractLinesForOpportunity Visulaforce component
*  Company: CloudShift
*  CreatedDate: 28-Jan-2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer          Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  David Kurtanjek      28-Jan-2018          Original Version
*/
public class ContractLinesForOpportunityController {
    public Boolean uiLMode{get; set;}
    public Boolean uiCMode{get; set;}
    public Id OppId;
    public List<bg_ContractLine> contractLineList{get;set;}
    public Boolean showComponent{get;set;}
    public String str{get;set;}

    Boolean calloutMade;
    String wildebeestRef;
    Opportunity opportunity;
    Account account;
    bg_phsAccountInfoResponse response;
    Set<String> wildebeestReferences;
    List<PHS_Account__c> phsAccountList;

    public ContractLinesForOpportunityController(){
        
        showComponent = calloutMade = FALSE;
        contractLineList = new List<bg_ContractLine>();
        wildebeestRef = str = '';
        opportunity = new Opportunity();
        account = new Account();
        phsAccountList = new List<PHS_Account__c>();
        wildebeestReferences = new Set<String>();
        
        if(UserInfo.getUIThemeDisplayed()=='Theme4d' || UserInfo.getUiTheme()=='Theme4t')
        uiLMode = true;
        else
        uiCMode = true;
    }
    public void setOppId(Id id){
        OppId = id;
    }
    public Id getOppId(){
        return OppId;
    }
    public void populateContractLines(){
        str = 'stop';
        if(OppId == NULL)
            return;
    
        try{
            opportunity = [Select Id, Wildebeest_Ref__c,Last_Callout_Date__c, Callout_Eligible__c, Callout_Response__c From Opportunity Where Id =: oppId];
            if(String.isBlank(opportunity.Wildebeest_Ref__c)){
            }
            else{
                if(isCalloutEligible()== TRUE){
                    makeCallout();
                }
                else{
                    showResponseFromOpportunity();
                }
            }
        }catch(Exception ex){
            System.debug('Opportunity data not found, error : ' + ex);
        }
    }
    private Boolean isCalloutEligible(){
        if(opportunity.Callout_Eligible__c == TRUE || String.isBlank(opportunity.Callout_Response__c) || opportunity.Last_Callout_Date__c == NULL || (System.Now()- 1) > opportunity.Last_Callout_Date__c)
            return TRUE;

        return FALSE;
    }
    private void makeCallout(){
        wildebeestRef = opportunity.Wildebeest_Ref__c.substringBefore(',');
        try{
            
            List<Account> acclist= [Select Id From Account Where Wildebeest_Ref__c = :wildebeestRef OR Wildebeest_Ref__c LIKE :wildebeestRef+',%'
                    OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef+',%'];
            if(acclist!=null && !acclist.isEmpty())
            account = acclist[0];
            else
            {
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No matching accounts refs found'));
                   return;
            }

            if(account != NULL && account.Id != NULL){
                phsAccountList = [Select Wildebeest_Ref__c From PHS_Account__c Where Salesforce_Account__c = :account.Id AND Wildebeest_Ref__c != NULL];

                if(phsAccountList!=NULL && phsAccountList.size() >0){
                    for(PHS_Account__c phsAccount : phsAccountList)
                        wildebeestReferences.add(phsAccount.Wildebeest_Ref__c);

                    if(wildebeestReferences.size()>0){
                        calloutMade = TRUE;
                        response = bg_Wildebeest_Helper.getExistingContractLines(account.Id, wildebeestReferences, true, false, false, false, 'CustomerDetails');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'to be sent to wildebeest system ->' +String.valueOf(wildebeestReferences)));
                        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, '22 ->' +String.valueOf(response)));
                        if(response != NULL && response.ResultSize>0){
                            contractLineList = response.contractLines;
                            showComponent = TRUE;
                        }
                        else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.ContractLinesForOpportunities_NoResponse));
                        }
                         // following condition is to be checked irrespective of the above if code
                        if(response != NULL){
                            opportunity.Callout_Eligible__c = FALSE;
                            opportunity.Callout_Response__c = JSON.serialize(contractLineList);
                            opportunity.Last_Callout_Date__c = System.now();
                            update opportunity;
                        }
                    }
                }
            }
            // ContractLinesForOpportunity_IncompleteData - > Matching data not found in Account/PHS Account, No Callout made
            if(calloutMade == FALSE)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.ContractLinesForOpportunities_IncompleteData));
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error : ' + ex.getMessage()));
        }
    }
    private void showResponseFromOpportunity(){
        contractLineList = (List<bg_ContractLine>)JSON.deserialize(opportunity.Callout_Response__c, List<bg_ContractLine>.class);
        System.debug('contractLineList:-'+ contractLineList);
        
        if(contractLineList.size()>0)
            showComponent = TRUE;
        else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.ContractLinesForOpportunities_NoResponse));
        }
    }
}