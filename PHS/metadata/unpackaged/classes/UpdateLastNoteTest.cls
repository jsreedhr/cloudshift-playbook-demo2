@isTest//(seeAllData=true)
public class UpdateLastNoteTest {
    
static testMethod void createNewNoteRecordLead(){
    
      User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
      u.Email = 'dannytest@cloudshiftgroup.com';
        insert u;
        
      Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
//        lead.FLT_Postal_Code__c =flt.id;
        insert lead;
        
    List<Lead> RelatedToId = [select id from Lead limit 1];
    
    
        
        
   // List<User> NoteOwner = [select Id from User where Email like 'dannytest@cloudshiftgroup.com' limit 1];
    
        List<User> NoteOwner = [select Id from User where Email like 'dannytest@cloudshiftgroup.com' limit 1]; 
        Note Nt = new Note(
        ParentId = RelatedToId[0].id,
        Title = 'Test',
        OwnerId = NoteOwner[0].id);
            
        database.insert(Nt);
}

static testMethod void createNewNoteRecordOpp(){
    
    
      User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
      u.Email = 'dannytest@cloudshiftgroup.com';
        insert u;
        
     Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
     Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
     insert opp;
     
    List<Opportunity> RelatedToId = [select Id from Opportunity limit 1];
    List<User> NoteOwner = [select Id from User where Email like 'dannytest@cloudshiftgroup.com' limit 1];
    
        
        Note Nt = new Note(
        ParentId = RelatedToId[0].id,
        Title = 'Test',
        OwnerId = NoteOwner[0].id);
            


        database.insert(Nt);    
}
    
}