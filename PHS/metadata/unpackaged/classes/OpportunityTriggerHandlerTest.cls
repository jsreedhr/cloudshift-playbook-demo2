/**
 *  @Class Name:    OpportunityTriggerHandlerTest
 *  @Description:   This is a test class for OpportunityTriggerHandler
 *  @Company:       dQuotient
 *  CreatedDate:    07/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           07/11/2017                  Original Version.
 */
@isTest
private class OpportunityTriggerHandlerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        opp.CampaignId = camp.Id;
        
         
        if (Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('KAM Opportunity')!=null)
        {
          Id devRecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('KAM Opportunity').getRecordTypeId();
        opp.RecordTypeId = devRecordTypeId;
       }
        opp.DM_Postal_District__c = flt.ID;
        insert opp;
        
        Task t = bg_Test_Data_Utils.createTask('Task', acc.Id);
        t.WhoId = null;
        t.WhatId = opp.id;
        t.OwnerId = u.id;
        insert t;
    
        Event e = bg_Test_Data_Utils.createEvent('Event', acc.Id);
        e.WhoId = null;
        e.WhatId = opp.id;
        e.OwnerId = u.id;
        insert e;
    }
    
    private static testMethod void testupdateActivity() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId, LeadSource, StageName, Type, CloseDate, Amount, Campaigns__c from Opportunity where OwnerId= :u.Id];
        List<Opportunity> oppoList = new List<Opportunity>();
        oppoList.add(oppo);
        Event e = [Select Score__c From Event Where WhatId = :oppoList[0].Id ];
        Task t = [Select Score__c From Task Where WhatId = :oppoList[0].Id];
        test.startTest();
            OpportunityTriggerHandler.updateActivity(oppoList);
            System.assertEquals(e.Score__c, t.Score__c);
        test.stopTest();
	}
	
	/*private static testMethod void testupdateCampaignsField() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId, LeadSource, StageName, Type, CloseDate, Amount, Campaigns__c, CampaignId from Opportunity where OwnerId= :u.Id Limit 1];
        List<Opportunity> oppoList = new List<Opportunity>();
        oppoList.add(oppo);
        test.startTest();
            OpportunityTriggerHandler.updateCampaignsField(oppoList);
            system.debug('Campaigns---->' +oppoList[0].Campaigns__c);
            System.assertNotEquals(oppoList[0].Campaigns__c, null);
        test.stopTest();
	}*/
	
	/*private static testMethod void testupdateCampaignsField() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, Wildebeest_Ref__c, LeadSource from Lead where OwnerId= :u.Id];        
        List<Lead> leadList = new List<Lead>();
        leadList.add(testLead);
        test.startTest();
            LeadTriggerFunctions.updateCampaignsField(leadList);
            System.assertNotEquals(leadList[0].Campaigns__c, null);
        test.stopTest();
	}*/
}