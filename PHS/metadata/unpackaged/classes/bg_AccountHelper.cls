/*********************************************************************************
 * bg_AccountHelper
 *
 * A helper class for the Account object.
 * 
 * Author: Tom Morris - BrightGen Ltd
 * Created: 24-06-2016
 *          04-07-2016 KH - Updated to use the Name field rather than Front Line Team Name.
 *
 *********************************************************************************/

public with sharing class bg_AccountHelper
{
    /*
        Method to check if the Account record requires an update, based on if the postcode changed. 
    */
    public static void populateFLTFields(List<Account> acctRecords, Map<Id, Account> oldAcctRecords)
    {    
        List<Account> acctRecordsToUpdate = new List<Account>();

        for(Account acctRecord : acctRecords)
        {
            
            Boolean recordHasChanged = acctRecord.BillingPostalCode != oldAcctRecords.get(acctRecord.Id).BillingPostalCode;
            Boolean fltRequired = acctRecord.FLT_Update_Needed__c != oldAcctRecords.get(acctRecord.Id).FLT_Update_Needed__c && acctRecord.FLT_Update_Needed__c == TRUE;
                    
            if(recordHasChanged || fltRequired)
            {
                acctRecordsToUpdate.add(acctRecord);
            }
        }
    
        if(!acctRecordsToUpdate.isEmpty())
        {
            populateFLTFields(acctRecordsToUpdate);
        }   
    }

    /*
        Method to find the FLT record related to the Account record based on it's postcode/outcode. 
    */
    public static void populateFLTFields(List<Account> acctRecords)
    {
        List<String> outcodes = new List<String>();
        List<Account> acctRecordsToUpdate = new List<Account>();
        Boolean postcodeValid = FALSE;

        for(Account acctRecord : acctRecords)
        {
            if(acctRecord.BillingPostalCode != null)
            {
                outcodes.add(bg_FLTHelper.determineOutcode(acctRecord.BillingPostalCode.touppercase()));
            }
        }

        List<Front_Line_Team__c> fltRecords = [SELECT Id, Name, Region_Name__c, FLTNo__c 
                                               FROM Front_Line_Team__c 
                                               WHERE Name 
                                               IN :outcodes];

        system.debug('**fltRecords**: ' + fltRecords);
        for(Account acctRecord: acctRecords)
        {
            for(Front_Line_Team__c fltRecord : fltRecords)
            {
                if(acctRecord.BillingPostalCode != null 
                                                 && fltRecord.Name == bg_FLTHelper.determineOutcode(acctRecord.BillingPostalCode.touppercase()))
                {
                    Account acct = new Account();
                    acct.FLT_Postal_District__c = fltRecord.Id;
                    acct.Id = acctRecord.Id;
                    acctRecordsToUpdate.add(acct);
                    postcodeValid = TRUE;
                }
            }

            if(acctRecord.BillingPostalCode == null || postcodeValid == FALSE)
            {
                Account acct = new Account();
                acct.FLT_Postal_District__c = null;
                acct.Id = acctRecord.Id;
                acctRecordsToUpdate.add(acct);
            }

            postcodeValid = FALSE;

        }

        if(!acctRecordsToUpdate.isEmpty())
        {
            update acctRecordsToUpdate;
        }
    }
}