/**********************************************************************
* bg_AccountHelper_Tests:
*
* Tests for the Account object
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 24/06/2016
*
* Changes: 
***********************************************************************/

@isTest
public class bg_AccountHelper_Tests
{
    static testMethod void testFLTAndRegionOnInsert()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{

    		Front_Line_Team__c flt = new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02');
	        insert flt;

	        test.startTest();

	        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	        insert acct;

	        test.stopTest();

	        acct = [SELECT Id, FLT_Number__c, FLT_Region_Name__c, BillingPostalCode, Name FROM Account WHERE Id = :acct.Id LIMIT 1];

	        System.assertNotEquals(null, acct.FLT_Number__c);
	        System.assertNotEquals(null, acct.FLT_Region_Name__c);
	        System.assertEquals('Scotland & NI', acct.FLT_Region_Name__c);
	        System.assertEquals('FLT 02', acct.FLT_Number__c);
    	}
    }

    static testMethod void testFLTAndRegionOnUpdate()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{
    		List<Front_Line_Team__c> flt = new List<Front_Line_Team__c>();
    		flt.add(new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02'));
    		flt.add(new Front_Line_Team__c(Name = 'AL1', Region_Name__c = 'North London', FLTNo__c = 'FLT 12'));
	        insert flt;

	        test.startTest();

	        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	        insert acct;

	        acct.BillingPostalCode = 'AL1 1AA';
	        update acct;


	        test.stopTest();

	        acct = [SELECT Id, FLT_Number__c, FLT_Region_Name__c FROM Account WHERE Id = :acct.Id LIMIT 1];

	        System.assertNotEquals(null, acct.FLT_Number__c);
	        System.assertNotEquals(null, acct.FLT_Region_Name__c);
	        System.assertEquals('North London', acct.FLT_Region_Name__c);
	        System.assertEquals('FLT 12', acct.FLT_Number__c);
    	}
    }
}