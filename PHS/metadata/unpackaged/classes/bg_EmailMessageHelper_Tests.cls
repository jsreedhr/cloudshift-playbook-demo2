/********************************************************************************
* bg_EmailMessageHelper_Tests :
*
* Class to test the Email Helper trigger methods
*
* Created By: Tom Morris
* Created Date: 17-01-2017 
*
* Changes: DT - 18 - 01 - 2017 Added tests for new features added to bg_EmailMessage_Helper
* Updated: Brightgen - IB 09/03/2018 added secondEmailMessage method
*********************************************************************************/

@isTest
private class bg_EmailMessageHelper_Tests
{
    static List<Service_Mailbox__mdt> serviceMailboxes = [SELECT Email__c FROM Service_Mailbox__mdt];

    @testSetup
    static void testSetup()
    {
    	Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        integrationSetting.Integration_Log_Purge_Limit__c = 5;

        Trigger_Settings__c trigSettings = new Trigger_Settings__c();
        trigSettings.EmailMessage_bi_preventEmailsFromFLT0__c = TRUE;
        trigSettings.EmailMessage_bi_preventEmailsToSrvcBoxes__c = TRUE;
        insert trigSettings;
    }
    // static testMethod void checkUnreadEmailsFlagIsSetToTrue()
    // {
    // 	Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

    //     Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
    //     integrationSetting.Integration_Log_Purge_Limit__c = 5;
    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    // 	Account testAcct = bg_Test_Data_Utils.createAccount('Test');
    // 	insert testAcct;

    //     Case testCase = new Case(AccountId = testAcct.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
    //     insert testCase;

    //     EmailMessage testEmailMessage = new EmailMessage();
    //     testEmailMessage.Status = '0';
    //     testEmailMessage.ParentId = testCase.Id;
    //     testEmailMessage.Incoming = TRUE;
    //     insert testEmailMessage;

    //     testCase = [SELECT Id, Unread_Emails__C FROM Case WHERE Id=:testCase.Id];

    //     System.assertEquals(TRUE, testCase.Unread_Emails__c);
    // }


    // static testMethod void checkButtonPressMethodReturnsTrue()
    // {
    // 	Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

    //     Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
    //     integrationSetting.Integration_Log_Purge_Limit__c = 5;
    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    // 	Account testAcct = bg_Test_Data_Utils.createAccount('Test');
    // 	insert testAcct;

    //     Case testCase = new Case(AccountId = testAcct.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
    //     insert testCase;

    //     EmailMessage testEmailMessage = new EmailMessage();
    //     testEmailMessage.Status = '0';
    //     testEmailMessage.ParentId = testCase.Id;
    //     testEmailMessage.Incoming = TRUE;
    //     insert testEmailMessage;

    //     testCase = [SELECT Id, Unread_Emails__C FROM Case WHERE Id=:testCase.Id];

    //     System.assertEquals(TRUE, testCase.Unread_Emails__c);

    //     String hasUnreadEmails = bg_EmailMessage_Helper.hasUnreadEmails(testCase.Id);

    //     System.assertEquals('TRUE', hasUnreadEmails);
    // }

    // static testMethod void checkButtonPressMethodReturnsFalse()
    // {

    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    // 	Account testAcct = bg_Test_Data_Utils.createAccount('Test');
    // 	insert testAcct;

    //     Case testCase = new Case(AccountId = testAcct.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
    //     insert testCase;

    //     EmailMessage testEmailMessage = new EmailMessage();
    //     testEmailMessage.Status = '2';
    //     testEmailMessage.ParentId = testCase.Id;
    //     testEmailMessage.Incoming = TRUE;
    //     insert testEmailMessage;

    //     testCase = [SELECT Id, Unread_Emails__C FROM Case WHERE Id=:testCase.Id];

    //     String hasUnreadEmails = bg_EmailMessage_Helper.hasUnreadEmails(testCase.Id);

    //     System.assertEquals('FALSE', hasUnreadEmails);
    // }

    // static testMethod void checkFirstEmailSetsFirstResponseMilestone()
    // {
    //     Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
    //     Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
    //     integrationSetting.Integration_Log_Purge_Limit__c = 5;
        
    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    //     Account testAcct = bg_Test_Data_Utils.createAccount('Test');
    //     insert testAcct;

    //     Contact testContact = bg_Test_Data_Utils.createContact('Test C', testAcct.Id);
    //     testContact.Email = 'test@milestone.com';
    //     insert testContact;

    //     Case testCase = new Case(AccountId = testAcct.Id, ContactId = testContact.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
    //     insert testCase;

    //     List<CaseMilestone> cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];
        
    //     for (CaseMilestone cMilestone : cMilestones)
    //     {
    //         system.assertEquals(FALSE, cMilestone.IsCompleted);
    //     }

    //     EmailMessage testEmailMessage = new EmailMessage();
    //     testEmailMessage.Status = '0';
    //     testEmailMessage.ParentId = testCase.Id;
    //     testEmailMessage.Incoming = FALSE;
    //     testEmailMessage.ToAddress = 'test@milestone.com';
    //     insert testEmailMessage;

    //     cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];

    //     for (CaseMilestone cMilestone : cMilestones)
    //     {
    //         if (cMilestone.MilestoneType.Name == 'First Response')
    //         {
    //             system.assertEquals(TRUE, cMilestone.IsCompleted);
    //         }
    //         else 
    //         {
    //             system.assertEquals(FALSE, cMilestone.IsCompleted);
    //         }
    //     }
    // }

    // static testMethod void checkSecondEmailSetsSecondResponseMilestone()
    // {
    //     Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
    //     Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
    //     integrationSetting.Integration_Log_Purge_Limit__c = 5;
        
    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    //     Account testAcct = bg_Test_Data_Utils.createAccount('Test');
    //     insert testAcct;

    //     Contact testContact = bg_Test_Data_Utils.createContact('Test C', testAcct.Id);
    //     testContact.Email = 'test@milestone.com';
    //     insert testContact;

    //     Case testCase = new Case(AccountId = testAcct.Id, ContactId = testContact.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
    //     insert testCase;

    //     List<CaseMilestone> cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];
        
    //     for (CaseMilestone cMilestone : cMilestones)
    //     {
    //         system.assertEquals(FALSE, cMilestone.IsCompleted);
    //     }

    //     EmailMessage testEmailMessage1 = new EmailMessage();
    //     testEmailMessage1.Status = '0';
    //     testEmailMessage1.ParentId = testCase.Id;
    //     testEmailMessage1.Incoming = FALSE;
    //     testEmailMessage1.ToAddress = 'test@milestone.com';
    //     insert testEmailMessage1;

    //     EmailMessage testEmailMessage2 = new EmailMessage();
    //     testEmailMessage2.Status = '0';
    //     testEmailMessage2.ParentId = testCase.Id;
    //     testEmailMessage2.Incoming = FALSE;
    //     testEmailMessage2.ToAddress = 'test@milestone.com';
    //     insert testEmailMessage2;        

    //     cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];

    //     for (CaseMilestone cMilestone : cMilestones)
    //     {
    //         if (cMilestone.MilestoneType.Name == 'First Response')
    //         {
    //             system.assertEquals(TRUE, cMilestone.IsCompleted);
    //         }
    //         else if (cMilestone.MilestoneType.Name == 'Second Response')
    //         {
    //             system.assertEquals(TRUE, cMilestone.IsCompleted);   
    //         }
    //         else 
    //         {
    //             system.assertEquals(FALSE, cMilestone.IsCompleted);
    //         }
    //     }
    // }

    static testMethod void checkCaseEmailFlagIsResetPositive()
    {

        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        Case testCase2 = new Case();
        testCase2.Unread_Emails__c=TRUE;
        insert testCase2;

        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.Incoming = TRUE;
        newEmailMessage.Status = '0';
        insert newEmailMessage;

        EmailMessage newEmailMessage1 = new EmailMessage();
        newEmailMessage1.ParentId = testCase.Id;
        newEmailMessage1.Incoming = TRUE;
        newEmailMessage1.Status = '0';
        insert newEmailMessage1;


        EmailMessage newEmailMessage2 = new EmailMessage();
        newEmailMessage2.ParentId = testCase2.Id;
        newEmailMessage2.Incoming = TRUE;
        newEmailMessage2.Status = '1';
        insert newEmailMessage2;

        EmailMessage newEmailMessage3 = new EmailMessage();
        newEmailMessage3.ParentId = testCase2.Id;
        newEmailMessage3.Incoming = TRUE;
        newEmailMessage3.Status = '2';
        insert newEmailMessage3;

        List<Case> caseList = new List<Case>();

        caseList.add(testCase);
        caseList.add(testCase2);

        bg_EmailMessage_Helper.resetUnreadEmailFlag(caseList);

        testCase = [SELECT Id, Unread_Emails__c FROM Case WHERE Id=:testCase.Id];
        testCase2 = [SELECT Id, Unread_Emails__c FROM Case WHERE Id=:testCase2.Id];

        System.assertEquals(TRUE, testCase.Unread_Emails__c);
        System.assertEquals(FALSE, testCase2.Unread_Emails__c);
    }

    @isTest
    static void checkAppropraiteEmailSent()
    {
        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.FromAddress = 'testSend@test.com';
        newEmailMessage.Incoming = FALSE;
        newEmailMessage.ToAddress = 'testSend@test.com';
        insert newEmailMessage;

        list<EmailMessage> testEmail = [SELECT Subject FROM EmailMessage];
        System.assertEquals(1, testEmail.size(),'Email not inserted properly');
    }
    
    @isTest 
    static void checkFromFLT0Reject()
    {   
        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        Boolean sendRejected = false;
        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.FromAddress = 'testSend'+Label.FLT0+'@test.com';
        newEmailMessage.Incoming = FALSE;
        newEmailMessage.ToAddress = 'testSend@test.com';
        try
        {
            insert newEmailMessage;
        }
        catch (Exception e)
        {
            System.debug('catch!');
            sendRejected = true;
        }

        System.assert(sendRejected, 'email sent when it should not have been');
    }

    @isTest
    static void checkToServiceMailbox()
    {
        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        Boolean sendRejected = false;
        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.FromAddress = 'testSend@test.com';
        newEmailMessage.Incoming = FALSE;
        newEmailMessage.ToAddress = serviceMailboxes[0].Email__c;
        try
        {
            insert newEmailMessage;
        }
        catch (Exception e)
        {
            sendRejected = true;
        }

        System.assert(sendRejected, 'email sent when it should not have been');
    }

    @isTest
    static void checkCCServiceMailbox()
    {
        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        Boolean sendRejected = false;
        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.FromAddress = 'testSend@test.com';
        newEmailMessage.Incoming = FALSE;
        newEmailMessage.ToAddress = 'testReceive@test.com';
        newEmailMessage.CcAddress = serviceMailboxes[0].Email__c;
        try
        {
            insert newEmailMessage;
        }
        catch (Exception e)
        {
            sendRejected = true;
        }

        System.assert(sendRejected, 'email sent when it should not have been');
    }

    @isTest
    static void checkBCCServiceMailbox()
    {
        Case testCase = new Case();
        testCase.Unread_Emails__c=TRUE;
        insert testCase;

        Boolean sendRejected = false;
        EmailMessage newEmailMessage = new EmailMessage();
        newEmailMessage.ParentId = testCase.Id;
        newEmailMessage.FromAddress = 'testSend@test.com';
        newEmailMessage.Incoming = FALSE;
        newEmailMessage.ToAddress = 'testReceive@test.com';
        newEmailMessage.BccAddress = serviceMailboxes[0].Email__c;
        try
        {
            insert newEmailMessage;
        }
        catch (Exception e)
        {
            sendRejected = true;
        }

        System.assert(sendRejected, 'email sent when it should not have been');
    }
    
    @isTest static void secondEmailMessage(){
            Account accountRecord = new Account(Name = 'Test', BillingPostalCode = 'ab12 3cd');
            insert accountRecord;

            Case caseRecord = new Case(AccountId = accountRecord.Id, Status = 'New');
            insert caseRecord;
        
            EmailMessage emailMessageRecord = new EmailMessage(ParentId = caseRecord.Id, Incoming = true, Status = '1', FromAddress = 'from@test.com');
            insert emailMessageRecord;
        
        Case cc = [SELECT Id, Status FROM Case WHERE Id =: caseRecord.Id Limit 1];
        System.assertEquals('New', cc.Status);
        
        	EmailMessage emailMessageRecord2 = new EmailMessage(ParentId = caseRecord.Id, Incoming = true, Status = '0', FromAddress = 'from2@test.com');
            insert emailMessageRecord2;
        
        Case c = [SELECT Id, Status FROM Case WHERE Id =: caseRecord.Id Limit 1];
        System.assertEquals('Awaiting PHS Response', c.Status);
    }

    // static testMethod void checkCaseEmailFlagIsResetNegative()
    // {

    //     Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
    //     Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
    //     integrationSetting.Integration_Log_Purge_Limit__c = 5;
        
    //     if(integrationSetting == null)
    //     {
    //         integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
    //         integrationSetting.Http_Callout_Endpoint__c = 'test';
    //         integrationSetting.Http_Callout_Query__c = 'test';
    //         insert integrationSetting;
    //     }

    //     Account testAcct = new Account();
    //     testAcct.Name = 'TestAccount';
    //     testAcct.BillingPostalCode = 'LS1 1AA';
    //     insert testAcct;

    //     PHS_Account__c testPHSAccount = new PHS_Account__c();
    //     testPHSAccount.Salesforce_Account__c = testAcct.Id;
    //     insert testPHSAccount;

    //     Case testCase = new Case();
    //     testCase.Unread_Emails__c=TRUE;
    //     testCase.AccountId = testAcct.Id;
    //     testCase.PHS_Account__c = testPHSAccount.Id;
    //     testCase.Type = 'Test';
    //     testCase.Case_Reason__c = 'Test';
    //     testCase.Status = 'Closed';
    //     insert testCase;

    //     Case testCase2 = new Case();
    //     testCase2.Unread_Emails__c=TRUE;
    //     insert testCase2;

    //     EmailMessage newEmailMessage = new EmailMessage();
    //     newEmailMessage.ParentId = testCase.Id;
    //     newEmailMessage.Incoming = TRUE;
    //     newEmailMessage.Status = '0';
    //     insert newEmailMessage;

    //     EmailMessage newEmailMessage1 = new EmailMessage();
    //     newEmailMessage1.ParentId = testCase.Id;
    //     newEmailMessage1.Incoming = TRUE;
    //     newEmailMessage1.Status = '0';
    //     insert newEmailMessage1;


    //     EmailMessage newEmailMessage2 = new EmailMessage();
    //     newEmailMessage2.ParentId = testCase2.Id;
    //     newEmailMessage2.Incoming = TRUE;
    //     newEmailMessage2.Status = '1';
    //     insert newEmailMessage2;

    //     EmailMessage newEmailMessage3 = new EmailMessage();
    //     newEmailMessage3.ParentId = testCase2.Id;
    //     newEmailMessage3.Incoming = TRUE;
    //     newEmailMessage3.Status = '0';
    //     insert newEmailMessage3;

    //     List<Case> caseList = new List<Case>();

    //     caseList.add(testCase);
    //     caseList.add(testCase2);

    //     bg_EmailMessage_Helper.resetUnreadEmailFlag(caseList);

    //     testCase = [SELECT Id, Unread_Emails__c FROM Case WHERE Id=:testCase.Id];
    //     testCase2 = [SELECT Id, Unread_Emails__c FROM Case WHERE Id=:testCase2.Id];

    //     System.assertEquals(TRUE, testCase.Unread_Emails__c);
    //     System.assertNotEquals(FALSE, testCase2.Unread_Emails__c);
    // }
}