/**********************************************************************
* bg_OpportunityHelper_Tests:
*
* Tests for the Opportunity object
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 01/08/2016
*
* Changes: KH 27/09/16 - Additional unit test for overriding the DM FLT on Opportunity
***********************************************************************/

@isTest
private class bg_OpportunityHelper_Tests
{
	static testMethod void testFLTAndRegionOnInsert()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{

    		Front_Line_Team__c flt = new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02');
	        insert flt;

	        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	        insert acct;

	        test.startTest();

	        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
	        insert opp;

	        test.stopTest();

	        opp = [SELECT Id, DM_FLT_Name__c, DM_Region__c, DM_Postal_District__c, Name FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertNotEquals(null, opp.DM_Postal_District__c);
	        System.assertEquals('Scotland & NI', opp.DM_Region__c);
	        System.assertEquals('FLT 02', opp.DM_FLT_Name__c);
    	}
    }

    static testMethod void testFLTAndRegionOnInsertWithOverirde()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{
    		List<Front_Line_Team__c> fltRecords = new List<Front_Line_Team__c>();
    		Front_Line_Team__c flt1 = new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02');
    		Front_Line_Team__c flt2 = new Front_Line_Team__c(Name = 'AB2', Region_Name__c = 'North', FLTNo__c = 'FLT 04');
    		fltRecords.add(flt1);
    		fltRecords.add(flt2);
    		insert fltRecords;

	        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	        insert acct;

	        test.startTest();

	        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
	        opp.DM_Postal_District__c = flt2.Id;
	        insert opp;

	        test.stopTest();

	        opp = [SELECT Id, DM_FLT_Name__c, DM_Region__c, DM_Postal_District__c, Name FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertNotEquals(null, opp.DM_Postal_District__c);
	        System.assertEquals('North', opp.DM_Region__c);
	        System.assertEquals('FLT 04', opp.DM_FLT_Name__c);
    	}
    }


    static testMethod void testFLTAndRegionWithNullPostcode()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{

    		List<Front_Line_Team__c> flt = new List<Front_Line_Team__c>();
    		flt.add(new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02'));
    		flt.add(new Front_Line_Team__c(Name = 'AL1', Region_Name__c = 'North London', FLTNo__c = 'FLT 12'));
	        insert flt;

	        Account acct = new Account(Name = 'Test Account', BillingPostalcode = 'LS1 3DD');
	        insert acct;

	        test.startTest();

	        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
	        insert opp;

	        test.stopTest();

	        opp = [SELECT Id, DM_FLT_Name__c, DM_Region__c, DM_Postal_District__c, Name FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertEquals(null, opp.DM_FLT_Name__c);
	        System.assertEquals(null, opp.DM_Region__c);
	        
    	}
    }


    static testMethod void testOrginatingEmployeeOnInsert()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	User employee;
    	System.runAs(userToRunAs)
    	{
    		employee = bg_RecordBuilder.createUser('EA', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    		employee.Username = '112233@phs.co.uk';
    		insert employee;

	        Account acct = new Account(Name = 'Test Account', BillingPostalcode = 'LS1 3DD');
	        insert acct;

	        test.startTest();

	        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
	        opp.Originating_Employee__c = '112233';
	        insert opp;

	        test.stopTest();

	        opp = [SELECT Id, Originating_Employee_User__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertEquals(employee.Id, opp.Originating_Employee_User__c);
    	}
    }
    
    static testMethod void testOrginatingEmployeeOnUpdate()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{
    		List<User> usersToInsert = new List<User>();

    		User employeeA = bg_RecordBuilder.createUser('EA', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    		employeeA.Username = '112233@phs.co.uk';
    		usersToInsert.add(employeeA);

    		User employeeB = bg_RecordBuilder.createUser('EB', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    		employeeB.Username = '999111@phs.co.uk';
    		usersToInsert.add(employeeB);
			
    		insert usersToInsert;

    		Account acct = new Account(Name = 'Test Account', BillingPostalcode = 'LS1 3DD');
	        insert acct;

			test.startTest();

	        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
	        opp.Originating_Employee__c = '112233';
	        insert opp;

	        test.stopTest();

	        opp = [SELECT Id, Originating_Employee_User__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertEquals(employeeA.Id, opp.Originating_Employee_User__c);

	        opp.Originating_Employee__c = '999111';
	        update opp;

	        opp = [SELECT Id, Originating_Employee_User__c FROM Opportunity WHERE Id = :opp.Id LIMIT 1];

	        System.assertEquals(employeeB.Id, opp.Originating_Employee_User__c);
    	}
    }
}