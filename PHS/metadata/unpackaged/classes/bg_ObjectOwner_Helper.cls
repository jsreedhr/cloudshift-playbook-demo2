/*********************************************************************************
 * bg_ObjectOwner_Helper
 *
 * Helper class to change the owner of objects based on a field within the system
 *  currently used for Opportunity, Lead, Activity 
 * Author: 
 * Created: 11-01-2016
 *
 *********************************************************************************/

public with sharing class bg_ObjectOwner_Helper {
	/*
		Generic Method to populate the Owner of the record if the
		Employee number has been set.
	*/
    public static void populateOwner(String objectType, List<SObject> newObjectList) {
    	List<String> employeeNumbers = new List<String>();
    	Map<String, List<SObject>> objToUpdate = new Map<String, List<SObject>>();
    	Object_Owner_Mapping__c mapSettings = Object_Owner_Mapping__c.getValues(objectType);
    	
        if (mapSettings != null){
            for (SObject obj : newObjectList){
                String empNo = String.valueOf(obj.get(mapSettings.Employee_Number_Field__c));

                if (!String.isBlank(empNo)) {
                    // Add the employee number to the list
                    // using the custom setting to get the name of the field
                    employeeNumbers.add(empNo);

                    if (objToUpdate.containsKey(empNo)){
                        List<SObject> objList = objToUpdate.get(empNo);
                        objList.add(obj);
                        objToUpdate.put(empNo, objList);
                    } else {
                        objToUpdate.put(empNo, new List<SObject> { obj });
                    }
                }
            }
            
            if (!employeeNumbers.isEmpty()) {
                /*
                List<User> employeeUsers = [SELECT Id, EmployeeNumber
                                            FROM User
                                            WHERE EmployeeNumber IN :employeeNumbers];
                
                for (String empNo : objToUpdate.keySet()){
                    for (User employeeUser : employeeUsers){
                        if (employeeUser.EmployeeNumber == empNo) {
                            for (SObject obj : objToUpdate.get(empNo)){
                                obj.put(mapSettings.Target_Owner_Field_Name__c, employeeUser.Id);
                            }
                        }
                    }
                }*/
                
                map<String, User> mapEmployee = new map<String, User>();
                for(User objUser: [SELECT Id, EmployeeNumber
                                            FROM User
                                            WHERE EmployeeNumber IN :employeeNumbers])
                {
                    mapEmployee.put(objUser.EmployeeNumber, objUser);
                }
                for (String empNo : objToUpdate.keySet())
                {
                    if (mapEmployee.containsKey(empNo)) 
                    {
                        for (SObject obj : objToUpdate.get(empNo))
                        {
                            obj.put(mapSettings.Target_Owner_Field_Name__c, mapEmployee.get(empNo).Id);
                        }
                    }
                }
            }        
        }
    }
}