public class RentalPageController {
    
    public List<SBQQ__QuoteLine__c> quoteLineList{get;set;}
    public List<SBQQ__QuoteLine__c> RentalquoteLineList{get;set;}
    public List<SBQQ__QuoteLine__c> SalequoteLineList{get;set;}
    public List<SBQQ__QuoteLine__c> MaintenancequoteLineList{get;set;}
    
      List<SBQQ__QuoteLine__c> RentalquoteLineListtmp = new  List<SBQQ__QuoteLine__c>();
     List<SBQQ__QuoteLine__c> SalequoteLineListtmp = new  List<SBQQ__QuoteLine__c>();
     List<SBQQ__QuoteLine__c> MaintenancequoteLineListtmp = new  List<SBQQ__QuoteLine__c>();
    public boolean isRental{get;set;}
    public boolean isSale{get;set;}
    public boolean isMaintenance{get;set;}
    
    protected Id quoteId;
    
    public RentalPageController(){
        isRental = false;
        isSale = false;
        isMaintenance = false;
        
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        System.debug('quoteId: '+quoteId);
        
        if(quoteId!=NULL){
            quoteLineList = [SELECT Id, SBQQ__Product__c, SBQQ__Product__r.Agreement_Type__c, SBQQ__Product__r.Name, PHSCPQ_Standard_Contract_Term__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__NetPrice__c, PHSCPQ_Bundle__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:quoteId AND PHSCPQ_Bundle__c = FALSE];
            System.debug('quoteLineList: '+quoteLineList);    
        }
        
        if(!quoteLineList.isEmpty()){
            for(SBQQ__QuoteLine__c quote : quoteLineList){
                if(quote.SBQQ__Product__r.Agreement_Type__c == 'Rental'){
                  //  RentalquoteLineList.add(quote);
                    RentalquoteLineListtmp.add(quote);
                }
                if(quote.SBQQ__Product__r.Agreement_Type__c == 'Sale'){
                   // SalequoteLineList.add(quote);
                    SalequoteLineListtmp.add(quote);
                }
                if(quote.SBQQ__Product__r.Agreement_Type__c == 'Maintenance'){
                    //MaintenancequoteLineList.add(quote);
                    MaintenancequoteLineListtmp.add(quote);
                }
            }
          /*  IF(RentalquoteLineList.size()>0){
                isRental = True;
            }
            IF(MaintenancequoteLineList.size()>0){
                isMaintenance = True;
            }
            IF(SalequoteLineList.size()>0){
                isSale = True;
            }  */
            IF(RentalquoteLineListtmp.size()>0){
              RentalquoteLineList=  RentalquoteLineListtmp;
                isRental = True;
            }
            IF(MaintenancequoteLineListtmp.size()>0){
                isMaintenance = True;
               MaintenancequoteLineList= MaintenancequoteLineListtmp;
            }
            IF(SalequoteLineListtmp.size()>0){
               SalequoteLineList= SalequoteLineListtmp;
                isSale = True;
            } 
            
            
        }
    }
}