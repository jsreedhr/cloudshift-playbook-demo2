@isTest
public class Test_SendtoOutboundQueue{
    
static testMethod void SendtoOutboundQueue(){
     
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'New';
       
        insert l;
             
    PageReference Pageref = page.SendtoOutboundQueue;
        Pageref.getParameters().put('id',l.id);

     test.setCurrentPage(Pageref);
   
    
    ApexPages.StandardController sc = new ApexPages.standardController(l);

     SendtoOutboundQueue rec  = new SendtoOutboundQueue(sc);
    rec.loadMessages();
}
}