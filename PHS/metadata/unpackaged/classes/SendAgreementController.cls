/**
 *  @Class Name:    SendAgreementController
 *  @Description:   This is a controller for the generate agreement page
 *  @Company:       CloudShift
 *  CreatedDate:    13/06/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  David Kurtanjek     13/06/2018                  Original Version
 */
public class SendAgreementController {

    public Order ld {
        get;
        set;
    }
    public Order orderRec {
        get;
        set;
    }
    public String errorMsg {
        get;
        set;
    }
    public Boolean redirectToPage {
        get;
        set;
    }
    
    public SendAgreementController(ApexPages.StandardController stdController) {
    ld = new Order();
        OrderRec = (Order) stdController.getRecord();
        if (OrderRec != null) {
            OrderRec = [select id, Number_of_Order_Products__c, OwnerId, Buyer__c, Buyer_Title__c, Method__c, Delivery_Email__c, Entity_Type__c, Approval_Required__c, Approved__c, Order_Product_Copy_Quote_Line_Difference__c from Order where id =: OrderRec.id Limit 1];
        }
    }

    public void loadAction() {

        errorMsg = '';
        redirectToPage = false;
        try {
            
            if (orderRec.Number_of_Order_Products__c < 1) {
                errorMsg = 'Please specify where your products are being sent before attempting to generate the agreement. Do this by using the Manage Contract Lines button.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Order_Product_Copy_Quote_Line_Difference__c > 0.01) {
                errorMsg = 'What was sold does not match what has been allocated. Please ensure the full quantity of all sold products have been allocated within the "Manage Contract Line Items Feature"';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Entity_Type__c == NULL) {
                errorMsg = 'Please ensure the entity type has been specified on the account.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Method__c == 'Portal' && orderRec.Delivery_Email__c == Null) {
                errorMsg = 'Please ensure a delivery email for the Portal has been specified.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Buyer__c == NULL) {
                errorMsg = 'Please record who the buyer is.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Buyer_Title__c == NULL) {
                errorMsg = 'Please record the job title of the buyer on their contact record. Please ensure this person is of appropriate seniority to sign this agreement.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }
            
            if (orderRec.Approval_Required__c == TRUE && orderRec.Approved__c == FALSE) {
                errorMsg = 'Please ensure the agreement is approved before trying to generate the document.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            }           
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: ' + e.getMessage()));
        }
        if (errorMsg == '') {
            try {
                update orderRec;
                redirectToPage = true;
            } catch (Exception e) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: ' + e.getMessage()));
            }
        }
    }
}