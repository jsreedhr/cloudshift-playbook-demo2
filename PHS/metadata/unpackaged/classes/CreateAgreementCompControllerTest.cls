/**
 *  @Class Name:    CreateAgreementCompControllerTest
 *  @Description:   This is a test class for CreateAgreementComponent
 *  @Company:       dQuotient
 *  CreatedDate:    11/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aishwarya SK        11/12/2017                  Original Version
 */
 
 @isTest
 public class CreateAgreementCompControllerTest {
     
     /**
    * Description: This method is to create Data.
    */
     @testSetup
     static void createData(){
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Contact contact = bg_Test_Data_Utils.createContact('1', acc.Id);
        insert contact;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        insert opp;
        
        Order agr = bg_Test_Data_Utils.createAgreement(acc.id, opp.id, date.Valueof('2017-12-11'),'Draft');
        insert agr;
     }
     
     private static testMethod void testgetLoggedInProfile(){
        User u = [Select id from User where Alias = 'standt']; 
            
            test.startTest();
            System.runAs(u){
                CreateAgreementCompController.getLoggedInProfile();
            }
            test.stopTest();
     }
     private static testMethod void testgetUserStatus(){
        User u = [Select id from User where Alias = 'standt']; 
            
            test.startTest();
            System.runAs(u){
                CreateAgreementCompController.getUserStatus();
            }
            test.stopTest();
     }
     private static testMethod void testgetOpp(){
        User u = [Select id from User where Alias = 'standt'];
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
            
            test.startTest();
            System.runAs(u){
                CreateAgreementCompController.getOpp(opp.Id);
            }
            test.stopTest();
     }
     private static testMethod void testHasAgreements(){
        User u = [Select id from User where Alias = 'standt']; 
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
        Order agr =[Select id from Order where OpportunityId =:opp.Id];
            
            test.startTest();
            System.runAs(u){
                CreateAgreementCompController.hasAgreements(opp.id);
            }
            test.stopTest();
    }
    private static testMethod void testcreateAgreement(){
        User u = [Select id from User where Alias = 'standt']; 
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
            
            test.startTest();
            System.runAs(u){
                CreateAgreementCompController.createAgreement(opp.id);
            }
            test.stopTest();
}
    
}