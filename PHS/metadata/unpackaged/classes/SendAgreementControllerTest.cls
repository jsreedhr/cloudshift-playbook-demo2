/**
 *  @Class Name:    SendAgreementControllerTest
 *  @Description:   This is a test class for SendAgreementComp
 *  @Company:       dQuotient
 *  CreatedDate:    08/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha                           				Original Version
 */
 
 @isTest
 public class SendAgreementControllerTest {
  /**
    * Description: This method is to create Data.
    */
     @testSetup
     static void createData(){
       
        
       
        
        
     }
     
    /* private static testMethod void testgetLoggedInProfile(){
        User u = [Select id from User where Alias = 'standt']; 
            
            test.startTest();
            System.runAs(u){
               // SendAgreementController.getLoggedInProfile();
            }
            test.stopTest();
     }*/
     private static testMethod void testgetUserStatus(){
         
          User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
          Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Contact contact = bg_Test_Data_Utils.createContact('1', acc.Id);
        insert contact;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        insert opp;
        
       // User u = [Select id from User where Alias = 'standt']; 
            Order agr = bg_Test_Data_Utils.createAgreement(acc.id, opp.id, date.Valueof('2017-12-11'),'Draft');
        insert agr;
            test.startTest();
            System.runAs(u){
               // SendAgreementController.getUserStatus();
               ApexPages.StandardController sc = new ApexPages.StandardController(agr);
               SendAgreementController sObj = new SendAgreementController(sc);
               sObj.loadAction();
            }
            test.stopTest();
     }
    /* private static testMethod void testgetOrder(){
        User u = [Select id from User where Alias = 'standt']; 
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
        Order agr =[Select id from Order where OpportunityId =:opp.Id];    
            
            test.startTest();
            System.runAs(u){
              //  SendAgreementController.getOrder(agr.id);
                //SendAgreementController.ApplyTemplate(agr.id);
            }
            test.stopTest();
     }
     private static testMethod void testcallInvokeDocuSign(){
        User u = [Select id from User where Alias = 'standt']; 
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
        Order agr =[Select id from Order where OpportunityId =:opp.Id];    
            
            test.startTest();
            System.runAs(u){
               // SendAgreementController.callInvokeDocuSign(agr.id);
            }
            test.stopTest();
     }*/
     
     
     
     
 
}