public with sharing class Add_LocationForCopyQuoteLinesItems
{
    public Integer numMultipleLocation                  {get; set;}
    public List<wrapQuoteLine> wrapQuoteLineItemList    {get; set;}
    public List<wrapOrderItem> wrapOrderProductList     {get; set;}
    private Set<Id> productIdSet;
    private map<Id, Copy_Quote_Line_Item__c> mapCopyQLineItem;
    public Boolean disableCheckBoxes                    {get; set;}
    private Order objOrder;
    public Boolean isSelectAll                          {get; set;}
    public Boolean showError                            {get; set;}
    public String accessLocationErrorString             {get; set;}
    public Map<String, Set<String>> locationProdMap     {get; set;}
    public List<LocationDetails> locationDetailsList    {get; set;} 
    public Decimal changedQuantitiy                     {get; set;}
    public Integer changeIndex                           {get; set;}
    private Map<Id, Decimal> mapCQLMaxQuant;
    private Map<String, Decimal> mapPreviousCLiQuant;
    private integer indexCount;
    public Map<String, Integer> mapProdIdToWQLIWrap;
    public Map<Id, Decimal> mapQuantityUsedUp;
   public string errOnPage {
        get;
        set;
    }
    public Add_LocationForCopyQuoteLinesItems(ApexPages.standardController stndCtrl)
    {
        errOnPage='false';
        changeIndex = 0;
        System.debug('changeIndex : ' + changeIndex);
        
        if(!Test.isRunningTest()) stndCtrl.addFields(new list<String>{'Pricebook2Id'});
        objOrder = (Order) stndCtrl.getRecord();
        objOrder = [Select Id, Pricebook2Id From Order Where Id = :objOrder.Id];
        isSelectAll = false;
        showError = false;
        init(); //call init method
        //if(showError == true){

        //}
    }
    
    public void init()
    {   
        disableCheckBoxes = true;
        mapProdIdToWQLIWrap = new Map<String, Integer>();
        mapQuantityUsedUp = new Map<Id, Decimal>();
        //changeIndex = 0;
        mapCQLMaxQuant = new Map<Id, Decimal>();
        mapPreviousCLIQuant = new Map<String, Decimal>();
        locationProdMap = new Map<String, Set<String>>();
        wrapQuoteLineItemList = new list<wrapQuoteLine>();
        locationDetailsList = new List<LocationDetails>();
        wrapOrderProductList = new List<wrapOrderItem>();
        accessLocationErrorString = '';
        mapCopyQLineItem = new map<Id, Copy_Quote_Line_Item__c>();
        integer ct = 0;
        for(Copy_Quote_Line_Item__c qlItem :[Select id, Product__c, Product__r.Name, Product_Code__c, Quantity__c, Minimum_Price__c, Minimum_Total__c, Maximum_Price__c, Maximum_Total__c, Net_Price__c, Net_Total__c, Target_Price__c, Target_Total__c, Product_Range__c, Contract_Term__c, Agreement_Type__c, Service_Frequency__c, Location__c, Agreement__c, Business_Type__c, Proof_of_Service__c, Proof_of_Service_Email__c, Service_Instructions__c, Installation_Required__c, Installation_Instructions__c, Installation_Date__c, Siting__c, Trial_Product__c, Trial_Days__c, Weight__c, Serial_No__c, Light_Level__c, Plant_Height__c, Overall_Height__c, Container_Code__c, Container_Name__c, Colour__c, Measurements__c, Finish__c, Top_Dressings__c, Display_Description_Plant_Variety__c, Revamp_Required__c, Maintenance_Agreement__c  
                                              From Copy_Quote_Line_Item__c Where Agreement__c = :objOrder.Id])
        {
            mapCopyQLineItem.put(qlItem.Product__c, qlItem);
            mapCQLMaxQuant.put(qlItem.Product__c, qlItem.Quantity__c);
            mapProdIdToWQLIWrap.put(String.valueOf(qlItem.Product__c), ct);
            ct++;
            //mapCQLWrapQLI.put()
            wrapQuoteLineItemList.add(new wrapQuoteLine(qlItem));
        }

        system.debug('wrapQuoteLineItemList-->' + wrapQuoteLineItemList);
        for(OrderItem oItem :[Select Product2Id, Location_Salesforce_Account__c, Location_Salesforce_Account__r.name, Location_PHS_Account__r.name, Order_Product_Location__c, Quantity, Minimum_Price__c, Minimum_Total__c, Maximum_Price__c, Maximum_Total__c, Net_Price__c, Net_Total__c, Target_Price__c, Target_Total__c, Product_Range__c, Contract_Term_Years__c, Agreement_Type__c, Service_Frequency__c, Business_Type__c, Proof_of_Service__c, Proof_of_Service_Email__c, Location_PHS_Account__c,Location_PHS_Account__r.Account_Name__c, Service_Instructions__c, Installation_Required__c, Installation_Instructions__c, Installation_Date__c, Siting__c, Trial_Product__c, Trial_Days__c, Weight__c, Serial_No__c, Light_Level__c, Plant_Height__c, Overall_Height__c, Container_Code__c, Container_Name__c, Colour__c, Measurements__c, Finish__c, Top_Dressings__c, Display_Description_Plant_Variety__c, Revamp_Required__c, Maintenance_Agreement__c, OrderId, UnitPrice, PricebookEntryId 
                              From OrderItem Where OrderId = :objOrder.Id])
        {   
            if(mapQuantityUsedUp.containsKey(oItem.Product2Id)){
                mapQuantityUsedUp.put(oItem.Product2Id, mapQuantityUsedUp.get(oItem.Product2Id) + oItem.Quantity);
            }
            else{
                mapQuantityUsedUp.put(oItem.Product2Id, oItem.Quantity);
            }

            wrapOrderItem newWrap = new wrapOrderItem(oItem);
            newWrap.locName = oItem.Order_Product_Location__c;
            newWrap.phsName = oItem.Location_PHS_Account__r.name;

            wrapOrderProductList.add(newWrap);
            if(locationProdMap == null || !locationProdMap.containskey(oItem.Location_Salesforce_Account__c)){
                Set<String> setLocIds = new Set<String>();
                setLocIds.add(String.valueOf(oItem.Product2Id));
                locationProdMap.put(oItem.Location_Salesforce_Account__c, setLocIds);

            }
            else{
                locationProdMap.get(oItem.Location_Salesforce_Account__c).add(String.valueOf(oItem.Product2Id));
            }
            //locationProdMap.put(Location_Salesforce_Acount__c)
            //decimal qty = mapCQLMaxQuant.get(oItem.Product2Id) + oItem.Quantity;        
            //mapCQLMaxQuant.put(oItem.Product2Id, qty);
            mapPreviousCLIQuant.put(String.valueOf(oItem.Product2Id) + String.valueOf(oItem.Location_Salesforce_Account__c), oItem.Quantity);             
        }

        for(Id prodId: mapQuantityUsedUp.keySet()){

            wrapQuoteLineItemList[mapProdIdToWQLIWrap.get(prodId)].remainingQuantity -= mapQuantityUsedUp.get(prodId);
        }
        validationCheck();


    }

   /*public void onChangeThirdPanelQuant(){
        system.debug('-->changeIndex-->' + changeIndex + '-->' + wrapOrderProductList);
        system.debug('check-->' + Apexpages.currentPage().getParameters().get('testParam'));

        // + wrapOrderProductList[changedIndex].orderItem.Product2Id);
        Decimal totalQuantity = 0;
        for(wrapOrderItem wrapObj : wrapOrderProductList) {
            if(wrapObj.orderItem.Product2Id == wrapOrderProductList[changeIndex].orderItem.Product2Id){
                totalQuantity += wrapObj.orderItem.Quantity;
            }
        }

        for(wrapQuoteLine wQLi : wrapQuoteLineItemList) {
            if(wQLi.quoteLineItem.Product__c == wrapOrderProductList[changeIndex].orderItem.Product2Id){
                if(totalQuantity > mapCQLMaxQuant.get(wQLi.quoteLineItem.Product__c)){
                    wrapOrderProductList[changeIndex].orderItem.Quantity = mapPreviousCLIQuant.get(String.valueOf(wrapOrderProductList[changeIndex].orderItem.Product2Id) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Acount__c));
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Quantity Exceeded. Please Review'));
                    return;
                }
                Decimal quantDif = wrapOrderProductList[changeIndex].orderItem.Quantity - mapPreviousCLIQuant.get(String.valueOf(wQLi.quoteLineItem.Product__c) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Acount__c));
                system.debug('quantDif-->' + quantDif);
                wQLi.remainingQuantity = wQLi.remainingQuantity - quantDif;

                //else 
                    //wQLi.quoteLineItem.Quantity__c = wQLi.quoteLineItem.Quantity__c - quantDif;
                mapPreviousCLIQuant.put(String.valueOf(wQLi.quoteLineItem.Product__c) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Acount__c), wrapOrderProductList[changeIndex].orderItem.Quantity);
            }
        }

    } */
    
    
    public void onChangeThirdPanelQuant(){
        system.debug('-->changeIndex-->' + changeIndex + '-->' + wrapOrderProductList);
        system.debug('check-->' + Apexpages.currentPage().getParameters().get('testParam'));

        // + wrapOrderProductList[changedIndex].orderItem.Product2Id);
        Decimal totalQuantity = 0;
        for(wrapOrderItem wrapObj : wrapOrderProductList) {
            if(wrapObj.orderItem.Product2Id == wrapOrderProductList[changeIndex].orderItem.Product2Id){
                totalQuantity += wrapObj.orderItem.Quantity;
            }
        }

        for(wrapQuoteLine wQLi : wrapQuoteLineItemList) {
            if(wQLi.quoteLineItem.Product__c == wrapOrderProductList[changeIndex].orderItem.Product2Id){
              if(totalQuantity > mapCQLMaxQuant.get(wQLi.quoteLineItem.Product__c)){
                    wrapOrderProductList[changeIndex].orderItem.Quantity = totalQuantity;//mapPreviousCLIQuant.get(String.valueOf(wrapOrderProductList[changeIndex].orderItem.Product2Id) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Acount__c));
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Quantity Exceeded. Please Review'));
                    errOnPage='true';
                   // return;
               }
               else
               errOnPage='false';
               
                Decimal quantDif =  mapPreviousCLIQuant.get(String.valueOf(wQLi.quoteLineItem.Product__c) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Account__c))-wrapOrderProductList[changeIndex].orderItem.Quantity;
                system.debug('quantDif-->' + quantDif);
                wQLi.remainingQuantity = mapCQLMaxQuant.get(wQLi.quoteLineItem.Product__c)-totalQuantity;

                //else 
                    //wQLi.quoteLineItem.Quantity__c = wQLi.quoteLineItem.Quantity__c - quantDif;
                   mapPreviousCLIQuant.put(String.valueOf(wQLi.quoteLineItem.Product__c) + String.valueOf(wrapOrderProductList[changeIndex].orderItem.Location_Salesforce_Account__c), wrapOrderProductList[changeIndex].orderItem.Quantity);
            }
        }

    }

    public void validationCheck() {
        // Location_Salesforce_Account__r.BillingCountry, Location_Salesforce_Account__r.billingstate,
        List<Agreement_Location__c> agreementLocationsList = [ Select Id, Name, Location_Salesforce_Account__c, Location_Salesforce_Account__r.Name, Location_Display__c,
                                                               Location_Salesforce_Account__r.billingstreet, Location_PHS_Account__c,
                                                               Location_PHS_Account__r.Account_Name__c, Location_Salesforce_Account__r.billingcity, 
                                                               Location_Salesforce_Account__r.billingPostalCode
                                                               From Agreement_Location__c Where Agreement__c =: objOrder.id]; 
        Set<String> accountIdSet = new Set<String>();
        //Map<String, String> agreementLocationMap = new Map<String, String>();
        Set<String> accessTimeAccountIds = new Set<String>();

        if(agreementLocationsList != null && agreementLocationsList.size()> 0) {

            accessLocationErrorString = 'Not all of the agreement locations have had Access Times defined. The following Agreement Locations don\'t have any Access Times: \n';
            for(Agreement_Location__c agree: agreementLocationsList) {
                accountIdSet.add(String.valueOf(agree.Location_Salesforce_Account__c));

            }

            List<Access_Time__c> accessTimeList = [Select id, Account__c from Access_Time__c where Account__c in: accountIdSet];
            for(Access_Time__c accessTime : accessTimeList){
                accessTimeAccountIds.add(accessTime.Account__c);
            }

            integer i = 0;
            for(Agreement_Location__c agree: agreementLocationsList){
                if(!accessTimeAccountIds.contains(agree.Location_Salesforce_Account__c)){
                        accessLocationErrorString += '\n' + agree.Name + ' - ' +agree.Location_Salesforce_Account__r.Name;
                    
                    showError = true;
                }
                i++;
            }
        }

        else{
            showError = true;
            accessLocationErrorString = 'There are no Agreement Locations available for this Agreement';
        }

        if(showError != true) {
            for(Agreement_Location__c agree: agreementLocationsList) {
                String shippingAddress = agree.Location_Display__c;
                
                LocationDetails locDetails = new LocationDetails(String.valueOf(agree.id), string.valueOf(agree.Location_Salesforce_Account__c), String.valueOf(agree.Location_PHS_Account__c), agree.name, agree.Location_Salesforce_Account__r.Name, agree.Location_PHS_Account__r.Account_Name__c, shippingAddress);
                locationDetailsList.add(locDetails);
            }
        }
        //accountList = [Select Id From Account Where Id = :SalesforceAccount];

    }

    public void setDefaultBooleanTrue(){
        disableCheckBoxes = true;
    }

    public void setDefaultBooleanFalse(){
        disableCheckBoxes = false;
    }

    public PageReference goBack() {
        // Do lots of interesting stuff
        //update account;
        PageReference nextPage = new PageReference('/' + objOrder.Id);
        return nextPage;
    }
    
    public void selectAllCopyQLItem()
    {
        for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
        {
            wrapobj.isSelected = isSelectAll;
        }
    }

    /*public void associateAgreementLoc(){
        String locationAccountId = '';
        f
        for(wrapQuoteLine wrapobj : wrapQuoteLineItemList){
            if(wrapobj.isSelected){

            }
        }
    }*/
    
    // ADD LOCATION BY THIS METHOD
    public void addLocationToSelected()
    {   
        String locationAccountId = '';
        String agreeLocId = '';
        String locationName = '';
        String phsLocId = '';
        String phsAccName = '';
        List<Copy_Quote_Line_Item__c> toBeUpdatedCQLI = new List<Copy_Quote_Line_Item__c>();
        //wrapOrderProductList
        for(LocationDetails locDet : locationDetailsList){
            if(locDet.isSelected!= null && locDet.isSelected == true){
                agreeLocId = locDet.locId;
                phsAccName = locDet.phsAccountName;
                locationAccountId = locDet.locAccountId;
                locationName = locDet.locationAccountName;
                phsLocId = locDet.phsAccountId;
            }
        }

        set<Id> productIdSet = new Set<Id>();
        Boolean noneSelected = true;
        Map<Id, Id> priceBookProductMap = new Map<Id, Id>();
        for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
        {
            if(wrapobj.isSelected) {
                noneSelected = false;
                productIdSet.add(wrapobj.quoteLineItem.Product__c);
                if(wrapobj.remainingQuantity == 0){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'This product has been fully allocated across locations. Please delete or update an existing line before trying to allocate addition quantity of the selected product.'));
                    return;
                }
            }
        }
        if(noneSelected){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please select a sold quote product before pressing "Add Product".'));
            return;
        }
        else{
            for(Pricebookentry entry : [SELECT Id, unitprice, Product2Id FROM PricebookEntry 
                                         WHERE Product2Id IN :productIdSet And isActive = true AND Pricebook2Id =: objOrder.Pricebook2Id ])
            {
                priceBookProductMap.put(entry.Product2Id, entry.Id);
                
            }

            for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
            {
                System.debug('wrap object : '+wrapobj);
                System.debug('priceBookProductMap.containsKey(wrapobj.quoteLineItem.Product__c)>>>>' + priceBookProductMap.containsKey(wrapobj.quoteLineItem.Product__c));
                System.debug('priceBookProductMap >>>'+priceBookProductMap);
                
                if(wrapobj.isSelected){
                    if(locationProdMap.containsKey(locationAccountId) && locationProdMap.get(locationAccountId).contains(wrapobj.quoteLineItem.Product__c)){
                        continue;
                    }
                if(priceBookProductMap.containsKey(wrapobj.quoteLineItem.Product__c))
                {   //system
                    OrderItem objOrderItem = new OrderItem();
                    objOrderItem.OrderId = wrapobj.quoteLineItem.Agreement__c;
                    objOrderItem.Product2Id = wrapobj.quoteLineItem.Product__c;
                    objOrderItem.Quantity = wrapobj.remainingQuantity;
                    objOrderItem.Minimum_Price__c = wrapobj.quoteLineItem.Minimum_Price__c;
                    objOrderItem.Minimum_Total__c = wrapobj.quoteLineItem.Minimum_Total__c;
                    objOrderItem.Maximum_Price__c = wrapobj.quoteLineItem.Maximum_Price__c;
                    objOrderItem.Maximum_Total__c = wrapobj.quoteLineItem.Maximum_Total__c;
                    objOrderItem.Net_Price__c = wrapobj.quoteLineItem.Net_Price__c;
                    objOrderItem.Net_Total__c = wrapobj.quoteLineItem.Net_Total__c;
                    objOrderItem.Target_Price__c = wrapobj.quoteLineItem.Target_Price__c;
                    objOrderItem.Target_Total__c = wrapobj.quoteLineItem.Target_Total__c;
                    objOrderItem.Product_Range__c = wrapobj.quoteLineItem.Product_Range__c;
                    objOrderItem.Contract_Term_Years__c = wrapobj.quoteLineItem.Contract_Term__c;
                    objOrderItem.Agreement_Type__c = wrapobj.quoteLineItem.Agreement_Type__c;
                    objOrderItem.Service_Frequency__c = (wrapobj.quoteLineItem.Service_Frequency__c != null ? Double.valueOf(wrapobj.quoteLineItem.Service_Frequency__c):0);
                    objOrderItem.Business_Type__c = wrapobj.quoteLineItem.Business_Type__c;              
                    objOrderItem.Location_Salesforce_Account__c = locationAccountId;
                    objOrderItem.Location_PHS_Account__c  = phsLocId;
                    objOrderItem.Agreement_Location__c = agreeLocId;
                    objOrderItem.Proof_of_Service__c  = wrapobj.quoteLineItem.Proof_of_Service__c;
                    objOrderItem.Proof_of_Service_Email__c  = wrapobj.quoteLineItem.Proof_of_Service_Email__c;
                    objOrderItem.Service_Instructions__c  = wrapobj.quoteLineItem.Service_Instructions__c;
                    objOrderItem.Installation_Required__c  = wrapobj.quoteLineItem.Installation_Required__c;
                    objOrderItem.Installation_Instructions__c  = wrapobj.quoteLineItem.Installation_Instructions__c;
                    objOrderItem.Installation_Date__c  = wrapobj.quoteLineItem.Installation_Date__c;
                    objOrderItem.Siting__c  = wrapobj.quoteLineItem.Siting__c;
                    objOrderItem.Trial_Product__c  = wrapobj.quoteLineItem.Trial_Product__c;
                    objOrderItem.Trial_Days__c  = wrapobj.quoteLineItem.Trial_Days__c;
                    objOrderItem.UnitPrice = wrapobj.quoteLineItem.Net_Price__c;
                    objOrderItem.PricebookEntryId = priceBookProductMap.get(wrapobj.quoteLineItem.Product__c);
                    objOrderItem.Weight__c = wrapobj.quoteLineItem.Weight__c;
                    objOrderItem.Light_Level__c = wrapobj.quoteLineItem.Light_Level__c;
                    objOrderItem.Plant_Height__c = wrapobj.quoteLineItem.Plant_Height__c;
                    objOrderItem.Overall_Height__c = wrapobj.quoteLineItem.Overall_Height__c;
                    objOrderItem.Container_Code__c = wrapobj.quoteLineItem.Container_Code__c;
                    objOrderItem.Container_Name__c = wrapobj.quoteLineItem.Container_Name__c;
                    objOrderItem.Colour__c = wrapobj.quoteLineItem.Colour__c;
                    objOrderItem.Measurements__c = wrapobj.quoteLineItem.Measurements__c;
                    objOrderItem.Finish__c = wrapobj.quoteLineItem.Finish__c;
                    objOrderItem.Top_Dressings__c = wrapobj.quoteLineItem.Top_Dressings__c;
                    objOrderItem.Display_Description_Plant_Variety__c = wrapobj.quoteLineItem.Display_Description_Plant_Variety__c;
                    objOrderItem.Revamp_Required__c = wrapobj.quoteLineItem.Revamp_Required__c;
                    objOrderItem.Maintenance_Agreement__c = wrapobj.quoteLineItem.Maintenance_Agreement__c;
                    objOrderItem.Serial_No__c = wrapobj.quoteLineItem.Serial_No__c;
                    wrapobj.remainingQuantity = 0;
                    system.debug('wrapobj.quoteLineItem.Product__c-->' + wrapobj.quoteLineItem.Product__c);
                    system.debug('String.valueOf(locationAccountId)' + String.valueOf(locationAccountId));
                    mapPreviousCLIQuant.put(String.valueOf(wrapobj.quoteLineItem.Product__c) + String.valueOf(locationAccountId), objOrderItem.Quantity);
                    //Copy_Quote_Line_Item__c qLI = new Copy_Quote_Line_Item__c(id = wrapobj.quoteLineItem.id, Quantity__c = 0);
                    //toBeUpdatedCQLI.add(qLI);
                    wrapOrderItem newWrap = new wrapOrderItem(objOrderItem); 
                    newWrap.locName = locationName;
                    newWrap.phsName = phsAccName;

                    wrapOrderProductList.add(newWrap);
                    if(locationProdMap == null || !locationProdMap.containskey(locationAccountId)){
                        Set<String> setLocIds = new Set<String>();
                        setLocIds.add(String.valueOf(wrapobj.quoteLineItem.Product__c));
                        locationProdMap.put(locationAccountId, setLocIds);

                    }
                    else{
                        locationProdMap.get(locationAccountId).add(String.valueOf(wrapobj.quoteLineItem.Product__c));
                    }
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Your order record does not have a pricebook associated to it.'));
                    return;
                }
                }
            }
        } 
        //if( toBeUpdatedCQLI != null && toBeUpdatedCQLI.size() > 0)
            //update toBeUpdatedCQLI;
    }
    
    // ADD MULTIPLE LOCATIONS BY THIS METHOD
    /*public void addMultipleLocationToSelected()
    {
        Boolean noneSelected = true;
        if(numMultipleLocation<1){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter the number of locations required before pressing "Add Multiple Locations".'));
            return;
        }
        set<Id> productIdSet = new Set<Id>();
        Map<Id, Id> priceBookProductMap = new Map<Id, Id>();
        system.debug('wrapQuoteLineItemList---->'+wrapQuoteLineItemList);
        for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
        {
            if(wrapobj.isSelected) {
                productIdSet.add(wrapobj.quoteLineItem.Product__c);
                noneSelected = false;
            }
        }
        system.debug('noneSelected---->'+noneSelected);
        if(noneSelected){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please select a record'));
            return;
        }
        
        else{
            
            for(Pricebookentry entry : [SELECT Id, unitprice, Product2Id FROM PricebookEntry 
                                         WHERE Product2Id IN :productIdSet And isActive = true AND Pricebook2Id =: objOrder.Pricebook2Id])
            {
                priceBookProductMap.put(entry.Product2Id, entry.Id);
            }
       
            for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
            {   
                if(wrapobj.isSelected && priceBookProductMap.containsKey(wrapobj.quoteLineItem.Product__c))
                {
                    for(Integer i =1; i <= numMultipleLocation ; i++)
                    {
                        OrderItem objOrderItem = new OrderItem();
                        objOrderItem.OrderId = wrapobj.quoteLineItem.Agreement__c;
                        objOrderItem.Product2Id = wrapobj.quoteLineItem.Product__c;
                        objOrderItem.Quantity = wrapobj.quoteLineItem.Quantity__c/numMultipleLocation;
                        objOrderItem.Minimum_Price__c = wrapobj.quoteLineItem.Minimum_Price__c;
                        objOrderItem.Minimum_Total__c = wrapobj.quoteLineItem.Minimum_Total__c/numMultipleLocation;
                        objOrderItem.Maximum_Price__c = wrapobj.quoteLineItem.Maximum_Price__c;
                        objOrderItem.Maximum_Total__c = wrapobj.quoteLineItem.Maximum_Total__c/numMultipleLocation;
                        objOrderItem.Net_Price__c = wrapobj.quoteLineItem.Net_Price__c; 
                        objOrderItem.Net_Total__c = wrapobj.quoteLineItem.Net_Total__c/numMultipleLocation;
                        objOrderItem.Product_Range__c = wrapobj.quoteLineItem.Product_Range__c;
                        objOrderItem.Contract_Term_Years__c = wrapobj.quoteLineItem.Contract_Term__c;
                        objOrderItem.Agreement_Type__c = wrapobj.quoteLineItem.Agreement_Type__c;
                        objOrderItem.Service_Frequency__c = (wrapobj.quoteLineItem.Service_Frequency__c != null ? Double.valueOf(wrapobj.quoteLineItem.Service_Frequency__c):0);
                        objOrderItem.Business_Type__c = wrapobj.quoteLineItem.Business_Type__c;  
                        
                        objOrderItem.Location_PHS_Account__c  = wrapobj.quoteLineItem.Location__c;
                        objOrderItem.Proof_of_Service__c  = wrapobj.quoteLineItem.Proof_of_Service__c;
                        objOrderItem.Proof_of_Service_Email__c  = wrapobj.quoteLineItem.Proof_of_Service_Email__c;
                        objOrderItem.Service_Instructions__c  = wrapobj.quoteLineItem.Service_Instructions__c;
                        objOrderItem.Installation_Required__c  = wrapobj.quoteLineItem.Installation_Required__c;
                        objOrderItem.Installation_Instructions__c  = wrapobj.quoteLineItem.Installation_Instructions__c;
                        objOrderItem.Installation_Date__c  = wrapobj.quoteLineItem.Installation_Date__c;
                        objOrderItem.Siting__c  = wrapobj.quoteLineItem.Siting__c;
                        objOrderItem.Trial_Product__c  = wrapobj.quoteLineItem.Trial_Product__c;
                        objOrderItem.Trial_Days__c  = wrapobj.quoteLineItem.Trial_Days__c;
                        objOrderItem.UnitPrice = wrapobj.quoteLineItem.Net_Price__c;
                        objOrderItem.PricebookEntryId = priceBookProductMap.get(wrapobj.quoteLineItem.Product__c);
                        objOrderItem.Weight__c = wrapobj.quoteLineItem.Weight__c;
                        objOrderItem.Serial_No__c = wrapobj.quoteLineItem.Serial_No__c;
                        System.debug('objOrderItem--->'+objOrderItem);

                        wrapOrderProductList.add(new wrapOrderItem(objOrderItem));
                    }
                    System.debug('wrapOrderProductList--->'+wrapOrderProductList);
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Your order record does not have a pricebook associated to it.'));
                    return;
                }
            }
        }
    }*/
    
    // SAVE ORDER PRODUCTS BY THIS METHOD 
   // public pageReference save()
    public void save()
    {
        
       
        List<OrderItem> orderItemList = new List<OrderItem>();
        //List<Copy_Quote_Line_Item__c> toBeUpdatedCQLI = new List<Copy_Quote_Line_Item__c>();
        map<Id, wrapQuoteQuantityAndPrice> mapProductIdToWrap = new map<Id, wrapQuoteQuantityAndPrice>();
         if(errOnPage!='error'){
        for(wrapOrderItem wrapobj :wrapOrderProductList)
        {
            
            wrapobj.orderItem.Net_Total__c = wrapobj.orderItem.Quantity * (wrapobj.orderItem.Net_Price__c != null ?wrapobj.orderItem.Net_Price__c :0) ;
            if(!mapProductIdToWrap.containsKey(wrapobj.orderItem.Product2Id))
            {
                wrapQuoteQuantityAndPrice objWrap =  new wrapQuoteQuantityAndPrice();
                objWrap.decQuantity = wrapobj.orderItem.Quantity;
                objWrap.decTotalPrice = wrapobj.orderItem.Net_Total__c;
                mapProductIdToWrap.put(wrapobj.orderItem.Product2Id, objWrap);
            }
            else
            {
                wrapQuoteQuantityAndPrice objWrap =  mapProductIdToWrap.get(wrapobj.orderItem.Product2Id);
                objWrap.decQuantity += (wrapobj.orderItem.Quantity != null ? wrapobj.orderItem.Quantity :0) ;
                objWrap.decTotalPrice += (wrapobj.orderItem.Net_Total__c != null ? wrapobj.orderItem.Net_Total__c :0);
                mapProductIdToWrap.put(wrapobj.orderItem.Product2Id, objWrap);
            }
            orderItemList.add(wrapobj.orderItem);
        }

        //for(wrapQuoteLine wrapobj :wrapQuoteLineItemList)
        //{
            
        //    toBeUpdatedCQLI.add(wrapobj.quoteLineItem);
        //}

        for(Id objProduct2Id: mapProductIdToWrap.keySet())
        {
            wrapQuoteQuantityAndPrice objWrap =  mapProductIdToWrap.get(objProduct2Id);
            Copy_Quote_Line_Item__c objCopyQuoteLineItem = mapCopyQLineItem.get(objProduct2Id);
            system.debug('objCopyQuoteLineItem----->'+objCopyQuoteLineItem);
            if(objCopyQuoteLineItem != null){
            /*if(objCopyQuoteLineItem.Quantity__c != objWrap.decQuantity) 
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The contract line item quantities need to match the sold quote product quantities. Please ensure they match.'));
                    return null;
                }*/
                /*if(objCopyQuoteLineItem.Net_Total__c != objWrap.decTotalPrice)
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The contract line item pricing needs to match the pricing on the sold products. Please ensure they match.'));
                    return null;
                }*/
            }
        }
        
    } //if only no error
    
    else{
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Quantity Exceeded. Please Review'));
    }
        try{
            upsert orderItemList;
            //update toBeUpdatedCQLI;
           // return new pageReference('/'+objOrder.Id);
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
           // return null;
        }
        
    
    }
    public pageReference cancel()
    {
        return new pageReference('/'+objOrder.Id);
    }
    
    // DELETE ORDER PRODUCTS BY THIS METHOD 
    public void deleteRecords()
    {
        List<OrderItem> orderItemList = new List<OrderItem>();
        //List<Copy_Quote_Line_Item__c> toBeUpdatedCQLI = new List<Copy_Quote_Line_Item__c>();
        
        Integer sz = wrapOrderProductList.size() - 1;
        for (Integer i = sz; i >= 0; i--) 
        {
            if(wrapOrderProductList[i].isSelected)
            {   for(wrapQuoteLine wQLi: wrapQuoteLineItemList){
                    if(wQLi.quoteLineItem.Product__c == wrapOrderProductList[i].orderItem.Product2Id){
                        wQLi.remainingQuantity += wrapOrderProductList[i].orderItem.Quantity;
                        //toBeUpdatedCQLI.add(wQLi.quoteLineItem);
                    }
                } 
                if(wrapOrderProductList[i].orderItem.Id != null) orderItemList.add(wrapOrderProductList[i].orderItem);
				
             	
                if(locationProdMap.containsKey(String.valueOf(wrapOrderProductList[i].orderItem.Location_Salesforce_Account__c)) && locationProdMap.get(String.valueOf(wrapOrderProductList[i].orderItem.Location_Salesforce_Account__c)).contains(wrapOrderProductList[i].orderItem.Product2Id)) {
                    Set<String> locIds = new Set<String>();
                    locIds = locationProdMap.get(String.valueOf(wrapOrderProductList[i].orderItem.Location_Salesforce_Account__c));
                    locIds.remove(wrapOrderProductList[i].orderItem.Product2Id);
                    if(locIds == null){
                        locationProdMap.remove(String.valueOf(wrapOrderProductList[i].orderItem.Location_Salesforce_Account__c));
                    }
                    else{
                        locationProdMap.put(String.valueOf(wrapOrderProductList[i].orderItem.Location_Salesforce_Account__c), locIds);
                    }
                }

                wrapOrderProductList.remove(i);
                
            }
        }


        try{
            delete orderItemList;
            //update toBeUpdatedCQLI;
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, ex.getMessage()));
        }
    }
    
    public class wrapQuoteQuantityAndPrice
    {
        public Decimal decQuantity = 0; 
        public Decimal decTotalPrice = 0 ;
    }
    // THIS IS QUOTE WRAPPER CLASS
    public class wrapQuoteLine
    {
        public Copy_Quote_Line_Item__c quoteLineItem    {get;set;}
        public Decimal remainingQuantity                {get;set;}
        public boolean isSelected                       {get;set;}
        
        public wrapQuoteLine(Copy_Quote_Line_Item__c qlt)
        {
            quoteLineItem = qlt;
            remainingQuantity = qlt.Quantity__c;
            isselected=false;
       }
    }
    
    // THIS IS PRODUCT WRAPPER CLASS
    public class wrapOrderItem
    {
        public OrderItem orderItem          {get;set;}
        public String locName               {get;set;}
        public String phsName               {get;set;}
        public boolean isSelected           {get;set;}
        
        public wrapOrderItem(OrderItem oItem)
        {
            orderItem = oItem;
            isselected=false;
       }
    }

    public class LocationDetails {
        public String locid                     {get; set;}

        public String locAccountId              {get; set;}
        public String phsAccountId              {get; set;}
        public String agreementLocName          {get; set;}
        public String locationAccountName       {get; set;}
        public String phsAccountName            {get; set;}
        public String locationAccountAddress    {get; set;}
        public Boolean isSelected               {get; set;}
        public LocationDetails(String locationId, string locationAccId, String phsAccId, String agreementLocationName, String locAccountName, String phsAccName, String locAccountAddress){
            locid = locationId;
            locAccountId =  locationAccId;
            phsAccountId = phsAccId;
            agreementLocName = agreementLocationName;
            locationAccountName = locAccountName;
            phsAccountName = phsAccName;
            locationAccountAddress = locAccountAddress;
        }

    }

}