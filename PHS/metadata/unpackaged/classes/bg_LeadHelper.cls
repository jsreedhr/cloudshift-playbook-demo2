/*********************************************************************************
* bg_LeadHelper
*
* Helper method to reparent the custom Competitors__c object from a lead on convert
* 
* Author: Tom Morris- BrightGen Ltd
* Created: 11-01-2016
*          04-07-2016 KH - Updated to use the Name field rather than Front Line Team Name.         
*          21-12-2016 KH - Updated to add checkOriginatingEmployee and setOriginatingEmployee
*********************************************************************************/

public with sharing class bg_LeadHelper
{    
    public static Boolean isAfterInsert = true;
    public static Boolean isBeforeInsert = true;
    public static Boolean isAfterUpdate = true;
    public static Boolean isBeforeUpdate = true;
    //Sets the standard price book upon lead creation
    public static void SetPriceBook(List<Lead> leads){
        
        List <PriceBook2> lst_pricebooks = ([SELECT Id FROM PriceBook2 WHERE isStandard = TRUE LIMIT 1]);
        
        IF(!lst_pricebooks.isEmpty()){
            for(Lead l: leads){
                for(PriceBook2 p: lst_pricebooks){
                    l.Price_Book__c = p.id;
                }
            }
        }        
    }
    
    //Sets 'Non-CCA' for Greenleaf & Wastekit Leads
    public static void SetNonCCA(List<Lead> leads){
        
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProfileName = PROFILE[0].Name;
        
        IF(MyProfileName.contains('Greenleaf') || MyProfileName.contains('Wastekit')){
            for(Lead l: leads){
                l.Entity_Type__C = 'Non-CCA';
            }
        }        
    }
    
    //Method used to carry over products creating opportunity line items lead products and competitors
    public static void convertLead(List<Lead> leads, Map<Id, Lead> oldLeads){
        Map<Id, Id> leadMap = new Map<Id, Id>();
        //Set of Lead Product Ids to query price book values
        Set <ID> set_LPProdIds = new Set <ID>();
        //Map of lead ids and product ids
        Map <ID, ID> Map_LeadProds = new Map <ID, ID>();
        //Map of products to price book entries       
        Map <ID, ID> Map_Prod_PBEs = new Map <ID, ID>();
        //Map of Leads and Price Book IDs
        Map <ID, ID> Map_Leads_PBIds = new Map <ID,ID>();
        //Map of products to price book entries
        Map <ID, Double> Map_Prod_Price = new Map <ID, Double>();
        //Map of products to price book contract terms
        Map <ID, String> Map_Prod_ConTerm = new Map <ID, String>();  
        Map <ID, String> Map_Prod_allowdSerFreq = new Map <ID, String>();  
        
        //List of Opportunity Line Items to Insert
        List <OpportunityLineItem> lst_OLIs = new List <OpportunityLineItem>();
        //List of Quote Line Items
      //  List <SBQQ__QuoteLine__c> lst_QLIs = new List <SBQQ__QuoteLine__c>(); for now
        //Map of Opp Ids and Lead WDB Ids
        Map <ID, String> Map_OppidWBCodes = new Map <ID, String>();
        //List of opp to updaet New Business type
        List <Opportunity> lst_Opp_toUpdate = new List <Opportunity>();
        ID runninguserid = UserInfo.getUserId();
        User runninguser = [SELECT Id, CPQ_Exempt__c from User where Id = :runninguserid];
        
        Boolean CPQExempt;
        IF(runninguser.CPQ_Exempt__c == TRUE){
            CPQExempt = TRUE;
        }
        else if(runninguser.CPQ_Exempt__c == FALSE){
            CPQExempt = FALSE;
        }           
        
        for(Lead l : leads)
        {
            if(l.IsConverted && l.convertedOpportunityId != null)
            {
                leadMap.put(l.id, l.convertedOpportunityId);
                Map_OppidWBCodes.put(l.convertedOpportunityId, l.Wildebeest_Ref__c);
            }
        }
        
        List<Competitor__c> compList = [SELECT Id, Opportunity__c, Lead__c FROM Competitor__c WHERE Lead__c IN :leadMap.keySet()];
        
        IF(!compList.isEmpty()){
            for(Competitor__c comp : compList){        
                comp.Opportunity__c = leadMap.get(comp.Lead__c);  
            }
        }        
        
        List<Lead_Product__c> prodList = [SELECT Id, Lead__c, Product__c, High_Price__c, Trade_Price__c, Floor_Price__c, Quantity__c, Service_Frequency__c, Contract_Type__c FROM Lead_Product__c WHERE Lead__c IN :leadMap.keySet()];
        
   //Execute Only if on conversion
    if(leadMap != NULL && !leadMap.isEmpty() ) {
        system.debug('True very much');
        
        //When zero lead product - create only one quote
        IF(prodList.isEmpty()){
         List <Opportunity> lst_opps = new List <Opportunity>([SELECT ID, AccountId, CloseDate FROM Opportunity WHERE ID in: leadMap.values()]);
            IF(CPQExempt==False){
                
                for(Lead l : leads){
                    List <SBQQ__Quote__c> lst_cpqquotes = new List <SBQQ__Quote__c>();
                    String Currentuserid = UserInfo.getUserId();
                    SBQQ__Quote__c cpqquote = new SBQQ__Quote__c();
                    cpqquote.SBQQ__Opportunity2__c = leadMap.get(l.id);
                    cpqquote.SBQQ__SalesRep__c = Currentuserid;
                    cpqquote.SBQQ__Status__c = 'Draft';
                    cpqquote.SBQQ__PaymentTerms__c = 'Net 30';
                    cpqquote.Method_of_Payment__c = 'Yearly in Advance';    
                    cpqquote.SBQQ__PriceBook__c = Map_Leads_PBIds.get(l.id);
                    cpqquote.SBQQ__PricebookId__c = Map_Leads_PBIds.get(l.id);
                    cpqquote.SBQQ__StartDate__c = System.Today();
                    cpqquote.Tele_Appointer__c = l.Tele_Appointer__c;
                    cpqquote.Tele_Appointer_Email__c = l.Tele_Appointer_Email__c;
                    cpqquote.SBQQ__Primary__c = true; //a1558000004MKnj - Tckt
                    lst_cpqquotes.add(cpqquote);
                    insert lst_cpqquotes;
                    
                    Map <Id, Id> mapOppQuotes = new Map <Id, Id>();
                    for(SBQQ__Quote__c cpq: lst_cpqquotes){
                        mapOppQuotes.put(cpq.SBQQ__Opportunity2__c, cpq.id);  
                    }
                    
                    Map <ID, Date> mapoppidclosedates = new Map <ID, Date>();
                
                    for(Opportunity o: lst_opps){
                     mapoppidclosedates.put(o.id, o.CloseDate -30);
                       IF(Map_OppidWBCodes.get(o.id) == Null){
                          o.New_Business_Type__c = 'Prospect';
                            lst_Opp_toUpdate.add(o);
                                }
                        else{
                            o.New_Business_Type__c = 'Existing'; 
                             lst_Opp_toUpdate.add(o);
                             
                        }
                    }
                }
            }
        }
        
        
        
        //When  lead product > 0 - create only one quote
        IF(!prodList.isEmpty()){
            system.debug('True very very very much');
            for(Lead_Product__c lp : prodList){
                set_LPProdIds.add(lp.Product__c);
                Map_LeadProds.put(lp.Lead__c, lp.Product__c);
            }
            
            for(Lead l: leads){
                IF(l.Price_Book__c != Null)           
                    Map_Leads_PBIds.put(l.id, l.Price_Book__c);
            }
            
            List <PriceBook2> lst_PB2s = new List <Pricebook2>([SELECT ID, Name, isActive FROM Pricebook2 WHERE ID in: Map_Leads_PBIds.values()]);
            
            
            IF(!lst_PB2s.isEmpty()){     
                
                List <PricebookEntry> lst_Pbes = new List <PricebookEntry>([SELECT ID, Pricebook2Id, Product2Id, Product2.PHSCPQ_Standard_Contract_Term__c,Product2.PHSCPQ_Allowed_Service_Frequency__c, UnitPrice, Target_Price__c FROM PricebookEntry WHERE Pricebook2Id In: Map_Leads_PBIds.values()]);
                system.debug('lst_Pbes-->' + lst_Pbes);
                IF(!lst_Pbes.isEmpty()){
                    
                    for(PricebookEntry pbe: lst_Pbes){
                        Map_Prod_PBEs.put(pbe.Product2Id, pbe.id);
                        IF(pbe.Target_Price__c == Null){
                            Map_Prod_Price.put(pbe.Product2Id, pbe.UnitPrice);
                        }
                        IF(pbe.Target_Price__c != Null){
                            Map_Prod_Price.put(pbe.Product2Id, pbe.Target_Price__c);
                        }
                        Map_Prod_ConTerm.put(pbe.Product2Id, pbe.Product2.PHSCPQ_Standard_Contract_Term__c);
                        Map_Prod_allowdSerFreq.put(pbe.Product2Id, pbe.Product2.PHSCPQ_Allowed_Service_Frequency__c);
                    }
                    //Set<Id> setLeadId = new Set<Id>();
                    map<Id, lead> mapNewLead = new map<Id, Lead>(leads);
                     List <SBQQ__Quote__c> lst_cpqquotes = new List <SBQQ__Quote__c>();
                 
                    //Quote Creation
                     IF(CPQExempt==False){
                      for(Lead l: leads){
                                   
                                    String Currentuserid = UserInfo.getUserId();
                                    SBQQ__Quote__c cpqquote = new SBQQ__Quote__c();
                                    cpqquote.SBQQ__Opportunity2__c = leadMap.get(l.id);
                                    cpqquote.SBQQ__SalesRep__c = Currentuserid;
                                    cpqquote.SBQQ__Status__c = 'Draft';
                                    cpqquote.SBQQ__PaymentTerms__c = 'Net 30';
                                    cpqquote.Method_of_Payment__c = 'Yearly in Advance';    
                                    cpqquote.SBQQ__PriceBook__c = Map_Leads_PBIds.get(l.id);
                                    cpqquote.SBQQ__PricebookId__c = Map_Leads_PBIds.get(l.id);
                                    cpqquote.SBQQ__StartDate__c = System.Today();
                                    cpqquote.Tele_Appointer__c = l.Tele_Appointer__c;
                                    cpqquote.Tele_Appointer_Email__c = l.Tele_Appointer_Email__c;
                                    cpqquote.SBQQ__Primary__c = true; 
                                    lst_cpqquotes.add(cpqquote);
                                    
                      }
                                  if(!lst_cpqquotes.isEmpty())
                                  {
                                   insert lst_cpqquotes;
                                  }
                     }
                    
                    for(Lead_Product__c lp : prodList){
                        
                        if(lp.Lead__c != null && mapNewLead.containsKey(lp.Lead__c)){
                            Lead l = mapNewLead.get(lp.Lead__c);
                            IF(Map_Prod_PBEs.get(lp.Product__c) == NULL){
                                l.AddError('There is at least one product that is not in your selected price book. Please contact your administrator or choose another price book.');
                            }
                            
                            IF(Map_Prod_PBEs.get(lp.Product__c) != NULL){
                                
                                List <Opportunity> lst_opps = new List <Opportunity>([SELECT ID, AccountId, CloseDate FROM Opportunity WHERE ID in: leadMap.values()]);
                                
                                //Creaet opp lineitem and quote lineitem
                                IF(CPQExempt==False){
                                    //Opp product creation
                                    OpportunityLineItem op = new OpportunityLineItem();
                                    op.PricebookEntryId = Map_Prod_PBEs.get(lp.Product__c);
                                    op.OpportunityId = leadMap.get(lp.Lead__c);
                                    op.Product2Id = lp.Product__c;
                                    op.Contract_Type__c = lp.Contract_Type__c;
                                    op.UnitPrice = Map_Prod_Price.get(lp.Product__c);                                     
                                    IF(lp.Service_Frequency__c == Null){
                                        op.Service_Frequency__c = NULL;
                                    }
                                    IF(lp.Service_Frequency__c !=Null){
                                        op.Service_Frequency__c = integer.ValueOf(lp.Service_Frequency__c);
                                    }
                                    op.ServiceDate = Date.today();
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)!=Null && Map_Prod_ConTerm.get(lp.Product__c)!= '1'){
                                        op.Revised_Term__c = Map_Prod_ConTerm.get(lp.Product__c) + ' Years';   
                                    }
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)== '1'){
                                        op.Revised_Term__c = Map_Prod_ConTerm.get(lp.Product__c) + ' Year';   
                                    }
                                    else if(Map_Prod_ConTerm.get(lp.Product__c)==Null){
                                        op.Revised_Term__c = '3 Years'; 
                                    }
                                    op.Quantity = lp.Quantity__c;
                                    op.Quantity__c = lp.Quantity__c;
                                    lst_OLIs.add(op);
                                     
                                    //List of Quote Line Items
                                    List <SBQQ__QuoteLine__c> lst_QLIs = new List <SBQQ__QuoteLine__c>();
                                    
                                    Map <Id, Id> mapOppQuotes = new Map <Id, Id>();
                                    for(SBQQ__Quote__c cpq: lst_cpqquotes){
                                        mapOppQuotes.put(cpq.SBQQ__Opportunity2__c, cpq.id);  
                                    }
                                    
                                    Map <ID, Date> mapoppidclosedates = new Map <ID, Date>();
                                    
                                    for(Opportunity o: lst_opps){
                                        mapoppidclosedates.put(o.id, o.CloseDate -30);
                                        IF(Map_OppidWBCodes.get(o.id) == Null){
                            			o.New_Business_Type__c = 'Prospect';
                            			 lst_Opp_toUpdate.add(o);
                                        }
                                        else{
                                            o.New_Business_Type__c = 'Existing';  
                                             lst_Opp_toUpdate.add(o);
                                        }
                                    }
                                    
                                    SBQQ__QuoteLine__c qli = new SBQQ__QuoteLine__c();
                                    qli.SBQQ__Quote__c = mapOppQuotes.get(leadMap.get(lp.Lead__c));
                                    qli.SBQQ__Product__c = lp.Product__c;
                                    qli.Contract_Type__c = lp.Contract_Type__c;
                                    qli.PHSCPQ_Standard_Service_Frequency__c = lp.Service_Frequency__c;
                                    system.debug('Map_Prod_Price.get(lp.Product__c)-->' + lp.Product__c + '-->' + Map_Prod_Price.get(lp.Product__c)); 
									qli.SBQQ__ListPrice__c = Map_Prod_Price.get(lp.Product__c);
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)==Null){
                                        qli.PHSCPQ_Standard_Contract_Term__c = NULL;   
                                    }
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)!= Null){
                                        qli.PHSCPQ_Standard_Contract_Term__c = Map_Prod_ConTerm.get(lp.Product__c);   
                                    }
                                    
                                    if(Map_Prod_allowdSerFreq.get(lp.Product__c) != NULL)
                                    {
                                         qli.PHSCPQ_Allowed_Service_Frequency__c=Map_Prod_allowdSerFreq.get(lp.Product__c);
                                    }
                                    qli.SBQQ__Quantity__c = lp.Quantity__c;
                                    lst_QLIs.add(qli);
                                    System.debug('=================lst_QLIs==============='+lst_QLIs);
                                     insert lst_QLIs;
                                     system.debug('lst_QLIs-->' + lst_QLIs);
                                    
                                    for(SBQQ__Quote__c cpqquote2: lst_cpqquotes){
                                        cpqquote2.SBQQ__ExpirationDate__c = mapoppidclosedates.get(cpqquote2.SBQQ__Opportunity2__c);
                                        cpqquote2.SBQQ__Primary__c = TRUE;
                                    }
                                    Update lst_cpqquotes;
                                  }
                                
                                //create only opp lineitem for every lead product
                                IF(CPQExempt==TRUE){
                                    system.debug('Entered True-->');
                                    OpportunityLineItem op = new OpportunityLineItem();
                                    op.PricebookEntryId = Map_Prod_PBEs.get(lp.Product__c);
                                    op.OpportunityId = leadMap.get(lp.Lead__c);
                                    op.Product2Id = lp.Product__c;
                                    op.Contract_Type__c = lp.Contract_Type__c;
                                    op.UnitPrice = Map_Prod_Price.get(lp.Product__c);
                                    IF(lp.Service_Frequency__c == Null){
                                        op.Service_Frequency__c = NULL;
                                    }
                                    IF(lp.Service_Frequency__c !=Null){
                                        op.Service_Frequency__c = integer.ValueOf(lp.Service_Frequency__c);
                                    }
                                    op.ServiceDate = Date.today();
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)!=Null && Map_Prod_ConTerm.get(lp.Product__c)!= '1'){
                                        op.Revised_Term__c = Map_Prod_ConTerm.get(lp.Product__c) + ' Years';   
                                    }
                                    IF(Map_Prod_ConTerm.get(lp.Product__c)== '1'){
                                        op.Revised_Term__c = Map_Prod_ConTerm.get(lp.Product__c) + ' Year';   
                                    }
                                    else if(Map_Prod_ConTerm.get(lp.Product__c)==Null){
                                        op.Revised_Term__c = '3 Years'; 
                                    }
                                    op.Quantity = lp.Quantity__c;
                                    op.Quantity__c = lp.Quantity__c;
                                    
                                    lst_OLIs.add(op);
                                     
                     
                    
                                    
                                }
                            }
                        }
                    }
                    
                    IF(!lst_OLIs.isEmpty()){
                      System.debug('===================lst_OLIs.============='+ lst_OLIs);   
                        insert lst_OLIs;
                    }
          }
            }
        }
       
        IF(!lst_Opp_toUpdate.isEmpty()){
                        update lst_Opp_toUpdate[0];
                    }
                   
        update compList; 
  
    } 
    }    
    
    /*
Method used to set the Orginating Employee On the Lead (before insert). The employee number on the Lead
is used to get the associated User record. A check is made to see if the Employee Number has changed on the Lead.
*/
    public static void setOriginatingEmployee(List<Lead> leadRecords)
    {
        system.debug('**Lead Records: ' + leadRecords);
        Set<String> employeeNumbers = new Set<String>();
        
        for(Lead leadRecord : leadRecords)
        {
            if (leadRecord.Originating_Employee__c != null)
            {
                employeeNumbers.add(leadRecord.Originating_Employee__c);
            }
        }
        
        if (!employeeNumbers.isEmpty())
        {
            Map<String, User> employeeNumberUserMap = bg_UserHelper.getEmployeeUserMap(employeeNumbers);
            for (Lead leadRecord : leadRecords)
            {
                if (employeeNumberUserMap.containsKey(leadRecord.Originating_Employee__c))
                {
                    leadRecord.Originating_Employee_User__c = employeeNumberUserMap.get(leadRecord.Originating_Employee__c).Id;
                    leadRecord.Originating_Employee_Role__c = employeeNumberUserMap.get(leadRecord.Originating_Employee__c).UserRole.Name;
                }
            }
        }
    }
    
    
    /*
Method used to set the Orginating Employee On the Lead (before update). The employee number on the Lead
is used to get the associated User record.
*/
    public static void checkOriginatingEmployee(List<Lead> leadRecords, Map<Id, Lead> oldleadRecords)
    {
        List<Lead> leadsToProcess = new List<Lead>();
        
        for (Lead leadRecord : leadRecords)
        {
            Lead oldleadRecord = oldleadRecords.get(leadRecord.Id);
            
            if (leadRecord.Originating_Employee__c != null && leadRecord.Originating_Employee__c != oldleadRecord.Originating_Employee__c)
            {
                leadsToProcess.add(leadRecord);
            }
        }
        
        if (!leadsToProcess.isEmpty())
        {
            setOriginatingEmployee(leadsToProcess);
        }
    }
    
    
    
    /*
Method to check if the Lead record requires an update, based on if the postcode changed. 
*/
    public static void populateFLTFields(List<Lead> leadRecords, Map<Id, Lead> oldleadRecords)
    {    
        List<Lead> leadRecordsToUpdate = new List<Lead>();
        
        for(Lead leadRecord : leadRecords)
        {
            
            Boolean recordHasChanged = leadRecord.PostalCode != oldleadRecords.get(leadRecord.Id).PostalCode;
            Boolean fltRequired = leadRecord.FLT_Update_Needed__c != oldleadRecords.get(leadRecord.Id).FLT_Update_Needed__c && leadRecord.FLT_Update_Needed__c == TRUE;
            
            if(recordHasChanged || fltRequired)
            {
                
                leadRecordsToUpdate.add(leadRecord);
            }
        }
        
        if(!leadRecordsToUpdate.isEmpty())
        {
            populateFLTFields(leadRecordsToUpdate);
        }
        
    }
    
    /*
Method to find the FLT record related to the Lead record based on it's postcode/outcode. 
*/
    /*
public static void populateFLTFields(List<Lead> leadRecords)
{
List<String> outcodes = new List<String>();
List<Lead> leadRecordsToUpdate = new List<Lead>();
Boolean postcodeValid = FALSE;

for(Lead leadRecord : leadRecords)
{
if(leadRecord.PostalCode != null)
{
outcodes.add(bg_FLTHelper.determineOutcode(leadRecord.PostalCode.touppercase()));
}
}

List<Front_Line_Team__c> fltRecords = [SELECT Id, Name, Region_Name__c, FLTNo__c 
FROM Front_Line_Team__c 
WHERE Name 
IN :outcodes];

for(Lead leadRecord: leadRecords)
{
for(Front_Line_Team__c fltRecord : fltRecords)
{
if(leadRecord.PostalCode != null 
&& fltRecord.Name == bg_FLTHelper.determineOutcode(leadRecord.PostalCode.touppercase()))
{
Lead lead = new Lead();
lead.Id = leadRecord.Id;
lead.FLT_Postal_Code__c = fltRecord.Id;
leadRecordsToUpdate.add(lead);
postcodeValid = TRUE;
}
}

if(leadRecord.PostalCode == null || postcodeValid == FALSE)
{
Lead lead = new Lead();
lead.Id = leadRecord.Id;
lead.FLT_Postal_Code__c = null;
leadRecordsToUpdate.add(lead);
}

postcodeValid = FALSE;
}

if(!leadRecordsToUpdate.isEmpty())
{
update leadRecordsToUpdate;
}
}
*/
    
    public static void populateFLTFields(List<Lead> leadRecords)
    {
        List<String> outcodes = new List<String>();
        List<Lead> leadRecordsToUpdate = new List<Lead>();
        Boolean postcodeValid = FALSE;
        
        for(Lead leadRecord : leadRecords)
        {
            if(leadRecord.PostalCode != null)
            {
                outcodes.add(bg_FLTHelper.determineOutcode(leadRecord.PostalCode.touppercase()));
            }
        }
        
        List<Front_Line_Team__c> fltRecords = [SELECT Id, Name, Region_Name__c, FLTNo__c 
                                               FROM Front_Line_Team__c 
                                               WHERE Name 
                                               IN :outcodes];
        map<String, Front_Line_Team__c> mapNameToFrontLineItem = new map<String, Front_Line_Team__c>();                                       
        for(Front_Line_Team__c objFrontLineItem: fltRecords)
        {
            mapNameToFrontLineItem.put(objFrontLineItem.Name , objFrontLineItem);
        }
        
        for(Lead leadRecord: leadRecords)
        {
            if(leadRecord.PostalCode != null)
            {
                String strPostalCode = bg_FLTHelper.determineOutcode(leadRecord.PostalCode.touppercase());
                leadRecord.FLT_Postal_Code__c = mapNameToFrontLineItem.containsKey(strPostalCode) ? mapNameToFrontLineItem.get(strPostalCode).id : null;
            }
            else
            {
                leadRecord.FLT_Postal_Code__c = null;
            }
        }
    }
    
    public static void fieldUpdatesInsert(List<Lead> lstLead)
    {
        for(Lead leadRecord: lstLead)
        {
            //Populate Wildebeest Ref
            if(leadRecord.External_Customer_ID__c != null && leadRecord.External_Customer_ID__c.isNumeric() && 
               String.isBlank(leadRecord.Wildebeest_Ref__c) && 
               String.isNotBlank(leadRecord.LeadSource) &&
               leadRecord.LeadSource.startsWith('Driver'))
            {
                leadRecord.Wildebeest_Ref__c = leadRecord.External_Customer_ID__c;
            }
            
            //Lead_Assigned_Date__c for new leads or leads advances straight to a more advanced status 
            if(leadRecord.OwnerId.getSobjectType() != Group.SobjectType )
            {
                leadRecord.Lead_Assigned_Date__c = Date.today();
            } 
            
        }
    }
    public static void fieldUpdatesInsertOrUpdate(List<Lead> lstLead, map<Id, Lead> mapOldLead)
    {
        String strCurrentUserLastname = UserInfo.getLastName();
        set<Id> setUserId = new set<Id>();
        Override_Automation__c objCustomSettingOverrideAuto = Override_Automation__c.getInstance();
        
        for(Lead leadRecord: lstLead)
        {
            //Lead - Advance Status to Enriched & De-Duplicated
            if(leadRecord.PostalCode != null && strCurrentUserLastname != 'Dev Team' 
               && (String.isNotBlank(leadRecord.MobilePhone) || String.isNotBlank(leadRecord.Phone) || String.isNotBlank(leadRecord.Email))
               && (String.isBlank(leadRecord.Title) || String.isBlank(leadRecord.Timescale__c) )
               && Trigger.isUpdate && (leadRecord.PostalCode != mapOldLead.get(leadRecord.id).PostalCode 
                                       || leadRecord.MobilePhone != mapOldLead.get(leadRecord.id).MobilePhone
                                       || leadRecord.Phone != mapOldLead.get(leadRecord.id).Phone
                                       || leadRecord.Email != mapOldLead.get(leadRecord.id).Email
                                       || leadRecord.Title != mapOldLead.get(leadRecord.id).Title
                                       || leadRecord.Timescale__c != mapOldLead.get(leadRecord.id).Timescale__c))
            {
                leadRecord.Status = 'Enriched & De-Duplicated';
            }
            
            //Lead - Set Closure Date
            if(leadRecord.Closed_Date__c == null && (leadRecord.Status == 'Archived' || String.isNotBlank(leadRecord.Closure_Reason__c))
               && (Trigger.isInsert || (!Trigger.isInsert && (leadRecord.Closed_Date__c != mapOldLead.get(leadRecord.id).Closed_Date__c 
                                                              || leadRecord.Status != mapOldLead.get(leadRecord.id).Status 
                                                              || leadRecord.Closure_Reason__c != mapOldLead.get(leadRecord.id).Closure_Reason__c ))))
            {
                leadRecord.Closed_Date__c = dateTime.now();
            }
            
            //Lead - Advance Status to Qualified
            if(leadRecord.PostalCode != null 
               && (String.isNotBlank(leadRecord.MobilePhone) || String.isNotBlank(leadRecord.Phone) || String.isNotBlank(leadRecord.Email))
               && (String.isNotBlank(leadRecord.Title) || String.isNotBlank(leadRecord.Timescale__c) )
               && (String.isBlank(leadRecord.FirstName) || String.isBlank(leadRecord.Street) || String.isBlank(leadRecord.City) || String.isBlank(leadRecord.State)|| String.isBlank(leadRecord.PostalCode))
               && Trigger.isUpdate && (leadRecord.PostalCode != mapOldLead.get(leadRecord.Id).PostalCode 
                                       || leadRecord.MobilePhone != mapOldLead.get(leadRecord.Id).MobilePhone 
                                       || leadRecord.Phone != mapOldLead.get(leadRecord.Id).Phone
                                       || leadRecord.Email != mapOldLead.get(leadRecord.Id).Email
                                       || leadRecord.Title != mapOldLead.get(leadRecord.Id).Title
                                       || leadRecord.Timescale__c != mapOldLead.get(leadRecord.Id).Timescale__c
                                       || leadRecord.FirstName != mapOldLead.get(leadRecord.Id).FirstName
                                       || leadRecord.Street != mapOldLead.get(leadRecord.Id).Street
                                       || leadRecord.City != mapOldLead.get(leadRecord.Id).City
                                       || leadRecord.State != mapOldLead.get(leadRecord.Id).State))
            {
                leadRecord.Status = 'Qualified';
            }
            
            //Lead - Advance Status to Ready for Conversion
            if(leadRecord.PostalCode != null 
               && (String.isNotBlank(leadRecord.MobilePhone) || String.isNotBlank(leadRecord.Phone) || String.isNotBlank(leadRecord.Email))
               && String.isNotBlank(leadRecord.Title) 
               && String.isNotBlank(leadRecord.Timescale__c) 
               && String.isNotBlank(leadRecord.Salutation) 
               && String.isNotBlank(leadRecord.FirstName) 
               && String.isNotBlank(leadRecord.Street) 
               && String.isNotBlank(leadRecord.City) 
               && String.isNotBlank(leadRecord.State) 
               && String.isNotBlank(leadRecord.PostalCode)
               && String.isNotBlank(leadRecord.Entity_Type__c)
               && Trigger.isUpdate 
               &&( leadRecord.MobilePhone != mapOldLead.get(leadRecord.id).MobilePhone 
                  || leadRecord.Phone != mapOldLead.get(leadRecord.id).Phone 
                  || leadRecord.Email != mapOldLead.get(leadRecord.id).Email 
                  || leadRecord.Title != mapOldLead.get(leadRecord.id).Title 
                  || leadRecord.Timescale__c != mapOldLead.get(leadRecord.id).Timescale__c 
                  || leadRecord.Salutation != mapOldLead.get(leadRecord.id).Salutation 
                  || leadRecord.FirstName != mapOldLead.get(leadRecord.id).FirstName 
                  || leadRecord.Street != mapOldLead.get(leadRecord.id).Street 
                  || leadRecord.City != mapOldLead.get(leadRecord.id).City 
                  || leadRecord.State != mapOldLead.get(leadRecord.id).State
                  || leadRecord.PostalCode != mapOldLead.get(leadRecord.id).PostalCode
                  || leadRecord.PostalCode != mapOldLead.get(leadRecord.id).Entity_Type__c))
            {
                leadRecord.Status = 'Ready for Conversion';
            }
            
            //Lead - Copy Description
            if(Trigger.isInsert || (!Trigger.isInsert && leadRecord.Description != mapOldLead.get(leadRecord.Id).Description))
            {
                leadRecord.Copy_Description__c = leadRecord.Description;
            }
            if(leadRecord.Originating_Employee_User__c != null && (trigger.isInsert || (!Trigger.isInsert && leadRecord.Originating_Employee_User__c != mapOldLead.get(leadRecord.Id).Originating_Employee_User__c)) )
            {
                setUserId.add(leadRecord.Originating_Employee_User__c );
            }
            
            //Lead - Calculate Score1
            leadRecord.Score_1__c = (String.isBlank(leadRecord.Email) && String.isNotBlank(leadRecord.Phone) && String.isNotBlank(leadRecord.MobilePhone) ) ? 2/3 : 
            (String.isNotBlank(leadRecord.Email) && String.isBlank(leadRecord.Phone) && String.isNotBlank(leadRecord.MobilePhone) ) ? 2/3 : 
            (String.isNotBlank(leadRecord.Email) && String.isNotBlank(leadRecord.Phone) && String.isBlank(leadRecord.MobilePhone) ) ? 2/3 : 
            (String.isBlank(leadRecord.Email) && String.isBlank(leadRecord.Phone) && String.isBlank(leadRecord.MobilePhone) ) ? 0 :
            (String.isNotBlank(leadRecord.Email) && String.isNotBlank(leadRecord.Phone) && String.isNotBlank(leadRecord.MobilePhone) ) ? 1:
            1/3 ;
            
            //Set Callout Eligible
            if(Trigger.isInsert || (!Trigger.isInsert && leadRecord.Wildebeest_Ref__c != mapOldLead.get(leadRecord.id).Wildebeest_Ref__c))
            {
                leadRecord.Callout_Eligible__c = true;
            }
            //Set Assign Auto to False
            if(!objCustomSettingOverrideAuto.Workflow__c && Trigger.isUpdate)
            {
                leadRecord.Do_Not_Assign_Automatically__c = true;
            }
            
            if(!objCustomSettingOverrideAuto.Workflow__c)
            {
                //Substitute Phone Lead 
                if(leadRecord.Phone != null)
                {
                    leadRecord.Phone = leadRecord.Phone.replace('(', '').replace(')', '').replace(' ', '').replace('-', '');
                }
            }
        }
        //Originating Employee
        map<Id, User> mapUser = new map<Id, User>([SELECT Id, Email FROM User WHERE id IN:setUserId]);
        Lead_Assigned_Date__c objCustomSettingLead_Assigned_Date = Lead_Assigned_Date__c.getInstance();
        for(Lead leadRecord: lstLead)
        {
            if(leadRecord.Originating_Employee_User__c != null && mapUser.containsKey(leadRecord.Originating_Employee_User__c) &&
               (trigger.isInsert || (!Trigger.isInsert && leadRecord.Originating_Employee_User__c != mapOldLead.get(leadRecord.Id).Originating_Employee_User__c)) )
            {
                leadRecord.Originating_Employee_Email__c = mapUser.get(leadRecord.Originating_Employee_User__c).Email;
            }
            
            //Update Lead_Assigned_Date__c for lead in In-Review and New and owner is not Queue
            if( leadRecord.createdDate > objCustomSettingLead_Assigned_Date.Date__c &&
               Trigger.isUpdate && 
               leadRecord.Status=='In Review' &&
               leadRecord.Lead_Assigned_Date__c == null 
               && leadRecord.Status != mapOldLead.get(leadRecord.Id).Status 
               && leadRecord.OwnerId.getSobjectType() != Group.SobjectType)
            {
                leadRecord.Lead_Assigned_Date__c = Date.today();
            }
            else if( Trigger.isUpdate && 
                    leadRecord.Lead_Assigned_Date__c == null 
                    && leadRecord.Status != mapOldLead.get(leadRecord.Id).Status 
                    && mapOldLead.get(leadRecord.Id).Status == 'New'
                    && leadRecord.OwnerId.getSobjectType() != Group.SobjectType)
            {
                leadRecord.Lead_Assigned_Date__c = Date.today();
            }
            
            
        }
    }
    public static void mapLeadFieldToOpp(List<Lead> lstNewLead)
    {
        List<Opportunity> lstOppToUpdate = new List<Opportunity>();
        for(Lead objLead:lstNewLead)
        {
            if(objLead.IsConverted && objLead.ConvertedOpportunityId != null)
            {
                /*Integer leadAge;
//  Date cDate = Date.newInstance(objLead.CreatedDate);
if( objLead.Status=='Archived')
leadAge=System.today().daysBetween(objLead.Lead_Assigned_Date__c);
else
leadAge=Date.newInstance(objLead.Closed_Date__c.year(), objLead.Closed_Date__c.month(), objLead.Closed_Date__c.day()).daysBetween(Date.newInstance(objLead.CreatedDate.year(), objLead.CreatedDate.month(), objLead.CreatedDate.day())); 
System.debug('leadAge=='+leadAge);
//lstOppToUpdate.add(new Opportunity(id = objLead.ConvertedOpportunityId, Lead_Age_at_Conversion__c = objLead.Lead_Age_at_Conversion__c)); */
                lstOppToUpdate.add(new Opportunity(id = objLead.ConvertedOpportunityId, Lead_Age_at_Conversion__c = objLead.Lead_age__c));
            }
        }
        update lstOppToUpdate;
    }
}