/********************************************************************************
* bg_TaskHelper_Tests :
*
* Test Class for the Task trigger methods
*
* Created By: Tom Morris
* Created Date: 17-01-2017 
* Changes:
*********************************************************************************/

@isTest
private class bg_TaskHelper_Tests {
    static testMethod void checkAgeFieldIsPopulated() {

    	Task newTask = new Task();
    	newTask.Status = 'In Progress';
    	insert newTask;

    	newTask.Status = 'Completed';
    	update newTask;
    	
    	newTask = [SELECT Id, Age_Minutes__c, Closed_Date__c FROM Task WHERE Id=:newTask.Id];

    	System.assertNotEquals(null, newTask.Age_Minutes__c);

    }

    static testMethod void checkAgeFieldIsNotPopulated() {

    	Task newTask = new Task();
    	newTask.Status = 'In Progress';
    	insert newTask;

    	newTask.Status = 'Not Started';
    	update newTask;
    	
    	newTask = [SELECT Id, Age_Minutes__c, Closed_Date__c FROM Task WHERE Id=:newTask.Id];

    	System.assertEquals(null, newTask.Age_Minutes__c);
    }
}