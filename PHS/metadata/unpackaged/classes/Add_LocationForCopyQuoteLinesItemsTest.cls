/**
 *  @Class Name:    Add_LocationForCopyQuoteLinesItemsTest
 *  @Description:   This is a test class for Add_LocationForCopyQuoteLinesItems for Add_LocationForCopyQuoteLinesItems vf page
 *  @Company:       dQuotient
 *  CreatedDate:    14/03/2018  
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           14/03/2018                  Original Version
 */
@isTest
private class Add_LocationForCopyQuoteLinesItemsTest {
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        system.assertNotEquals(acc.Id, null);
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        system.assertNotEquals(opp.Id, null);
        
        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('00034421', acc.Id);
        insert phsAccount;
        system.assertNotEquals(phsAccount.Id, null);
        
        Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true, Price_Book_Type__c='Standard' );
        update standardPricebook;
        
        Product2 prodObj =  bg_Test_Data_Utils.createaProduct2('Test Product');
        insert prodObj;
        system.assertNotEquals(prodObj.Id, null);
        
        PricebookEntry priceBookEntry = bg_Test_Data_Utils.createPricebookEntry(prodObj.Id, standardPricebook.Id);
        priceBookEntry.IsActive = true;
        insert priceBookEntry;
        system.assertNotEquals(priceBookEntry.Id, null);
        
        Order orderObj = bg_Test_Data_Utils.createAgreement(acc.Id, opp.Id, system.today(), 'draft');
        orderObj.Pricebook2Id = standardPricebook.Id;
        insert orderObj;
        system.assertNotEquals(orderObj.Id, null);
        
        Agreement_Location__c agreementLocationObj = bg_Test_Data_Utils.createAgreementLocation(acc.id, orderObj.id);
        insert agreementLocationObj;
        
        Access_Time__c accessTimeObj =  bg_Test_Data_Utils.createAccessTime(orderObj.id, phsAccount.Id, null);
        insert accessTimeObj;
        
        OrderItem orderItemObj = bg_Test_Data_Utils.createOrderItem(phsAccount.Id, orderObj.Id, priceBookEntry.Id, agreementLocationObj.id);
        insert orderItemObj;
        system.assertNotEquals(orderItemObj.Id, null);
        
        Copy_Quote_Line_Item__c quoteLineItem = bg_Test_Data_Utils.createCopyQuoteLineItem(prodObj.Id, phsAccount.Id, orderObj.Id);
        insert quoteLineItem;
        system.assertNotEquals(quoteLineItem.Id, null);

    }
    
    private static testMethod void testQuantityNoMatch() {
        
        Test.startTest();
            Order orderObj = [Select Id From Order Where Status = 'draft'];
            ApexPages.StandardController sc = new ApexPages.StandardController(orderObj);
            Add_LocationForCopyQuoteLinesItems testAddLocation = new Add_LocationForCopyQuoteLinesItems(sc);
            
            PageReference pageRef = Page.ManageContractLineItemsHygiene;
            pageRef.getParameters().put('id', String.valueOf(orderObj.Id));
            Test.setCurrentPage(pageRef);
            testAddLocation.locationDetailsList[0].isSelected = true;
            testAddLocation.wrapQuoteLineItemList[0].remainingQuantity = 2;
            system.debug('testAddLocation-->' + testAddLocation.wrapQuoteLineItemList);
            testAddLocation.wrapQuoteLineItemList[0].isSelected = true;
            
            
            //testAddLocation.isSelectAll = true;
            //testAddLocation.selectAllCopyQLItem();
            testAddLocation.addLocationToSelected();
            testAddLocation.changeIndex = 0;
            testAddLocation.wrapOrderProductList[testAddLocation.changeIndex].orderItem.Quantity = 1;
            testAddLocation.onChangeThirdPanelQuant();
            //testAddLocation.numMultipleLocation = 3;
            //testAddLocation.addMultipleLocationToSelected();
            testAddLocation.save();
            Test.stopTest();
    }
    
    private static testMethod void testsave() {
        Test.startTest();
            Order orderObj = [Select Id From Order Where Status = 'draft'];
            ApexPages.StandardController sc = new ApexPages.StandardController(orderObj);
            Add_LocationForCopyQuoteLinesItems testAddLocation = new Add_LocationForCopyQuoteLinesItems(sc);
            
            PageReference pageRef = Page.ManageContractLineItemsHygiene;
            pageRef.getParameters().put('id', String.valueOf(orderObj.Id));
            Test.setCurrentPage(pageRef);
            
            testAddLocation.isSelectAll = true;
            testAddLocation.selectAllCopyQLItem();
            testAddLocation.save();
        Test.stopTest();
    }
    
    private static testMethod void testdeleteRecordsAndCancel() {
        Test.startTest();
            Order orderObj = [Select Id From Order Where Status = 'draft'];
            ApexPages.StandardController sc = new ApexPages.StandardController(orderObj);
            Add_LocationForCopyQuoteLinesItems testAddLocation = new Add_LocationForCopyQuoteLinesItems(sc);
            
            PageReference pageRef = Page.ManageContractLineItemsHygiene;
            pageRef.getParameters().put('id', String.valueOf(orderObj.Id));
            Test.setCurrentPage(pageRef);
            
            testAddLocation.wrapOrderProductList[0].isSelected = true;
            testAddLocation.deleteRecords();
            testAddLocation.cancel();
        Test.stopTest();
    }

}