/**********************************************************************
* bg_LeadHelper_Tests:
*
* Tests for the Lead object
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 24/06/2016
*
* Changes: 
***********************************************************************/

@isTest
private class bg_LeadHelper_Tests
{
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB 10');
        insert flt;
        
       /* Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook; */
        
         Pricebook2 spb = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                             ); 
        update spb;
        
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        insert lead;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
     //   Product2 productObj = bg_Test_Data_Utils.createaProduct2('Test Product');
       // insert productObj;
       
        Product2 productObj = bg_Test_Data_Utils.createaProduct2('Test Product');
        productObj.IsActive = true;
        productObj.Target_Price__c =12;        
        insert productObj;
        
        Campaign_Product__c campaignproductObj = bg_Test_Data_Utils.createCampaignProduct(camp.Id, productObj.Id);
        insert campaignproductObj;
        
        Lead_Product__c leadproduct= bg_Test_Data_Utils.createLeadProduct(camp.Id, productObj.Id, lead.Id);
        insert leadproduct;
        
      /*  PricebookEntry price = bg_Test_Data_Utils.createPricebookEntry(productObj.Id, standardPricebook.Id);
        price.IsActive = true;
        insert price; */
        
        Competitor__c competitor = bg_Test_Data_Utils.createCompetitor(lead.Id);
        insert competitor;

    }
    
    static testMethod void testFLTAndRegionOnUpdate()
    {
    	User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Id from Front_Line_Team__c where Name = 'AB 10'];
        testLead.FLT_Postal_Code__c = flt.Id;
    	System.runAs(u) 
    	{
    	    test.startTest();
    	    update testLead;
            System.assertNotEquals(null, testLead.FLT_Postal_Code__c);
            test.stopTest();
    	}
    }

    static testMethod void testFLTAndRegionWithNullPostcode()
    {
    	User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
        testLead.FLT_Postal_Code__c = null;
    	System.runAs(u) 
    	{
    	    test.startTest();
    	    update testLead;
            System.assertEquals(null, testLead.FLT_Postal_Code__c);
            test.stopTest();
    	}
    }

    static testMethod void testOrginatingEmployeeOnInsert()
    {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Id from Front_Line_Team__c where Name = 'AB 10'];
        testLead.FLT_Postal_Code__c = flt.Id;
        testLead.Originating_Employee__c = '112233';
        testLead.Status = 'Ready for Conversion';
    	System.runAs(u) 
    	{
    	    test.startTest();
    	    update testLead;
            System.assertNotEquals(null, testLead.Originating_Employee__c);
            test.stopTest();
    	}
    }
    
    private static testMethod void myUnitTest() {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
    	test.startTest();
	    Database.LeadConvert lc = new Database.LeadConvert();
        leadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setLeadId(testLead.id);
	    Database.LeadConvertResult lcr = Database.convertLead(lc);
        //System.assertEquals('Converted To Opportunity', testLead.Status);
        test.stopTest();
    	
    }
    static testmethod void testInsertWorkflowUpdates()
    {
        Pricebook2 spb = new Pricebook2(Id = Test.getStandardPricebookId(),IsActive = true); 
        update spb;
        
        Product2 producto =new Product2();
        producto.Name='test';
        producto.productCode='1234';
        producto.isActive = true;
        producto.Target_Price__c = 50;
        insert producto;
        
       
	    
        Lead lead = bg_Test_Data_Utils.createLead('Test Lead');
        lead.OwnerId = userinfo.getUserId();
        lead.PostalCode = 'CF11 8RR';
        lead.MobilePhone = '121233434';
        lead.External_Customer_ID__c = '2876867868';
        lead.Wildebeest_Ref__c = null;
        lead.LeadSource = 'Inbound Email';
        lead.Email = 'test1234@test.com';
        lead.Title =  null;
        //lead.Price_Book__c = spb.id;
        insert lead;
        phs_RecursiveCheck.run = true;
        lead.Closed_Date__c = null;
        lead.Status = 'Archived';
        
           Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        Campaign_Product__c campaignproductObj = bg_Test_Data_Utils.createCampaignProduct(camp.Id, producto.Id);
        insert campaignproductObj;
        
        Lead_Product__c leadproduct= bg_Test_Data_Utils.createLeadProduct(camp.Id, producto.Id, lead.Id);
        insert leadproduct;
        Competitor__c competitor = bg_Test_Data_Utils.createCompetitor(lead.Id);
        insert competitor;
        
        system.debug('last update========');
        update lead;
        
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;

        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert opp;
	        
        // lead.IsConverted = true;
        // lead.convertedOpportunityId = opp.Id;
        // bg_LeadHelper.convertLead(new List<Lead>{lead}, new map<id, Lead>{lead.Id => lead});
         
    }
    
    static testmethod void testInsertWorkflowUpdates1()
    {
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = userinfo.getUserId();
        lead.PostalCode = 'CF11 8RR';
        lead.MobilePhone = '121233434';
        lead.External_Customer_ID__c = '2876867868';
        lead.Wildebeest_Ref__c = null;
        lead.LeadSource = 'Inbound Email';
        lead.Email = 'test12@test.com';
        lead.Title = 'Title';
        lead.FirstName = null;
        lead.Closed_Date__c =  null;
        lead.Status = 'Archived';
        lead.Closure_Reason__c = 'test reason';
        insert lead;
        lead.Status = 'Archived';
        lead.Email = 'tttest12@test.com';
        lead.Title = 'Title12';
        lead.Street = 'test Street';
        lead.City = 'test city';
        update lead;
    }
    static testmethod void convertMethod()
    {
        Pricebook2 spb = new Pricebook2(Id = Test.getStandardPricebookId(),IsActive = true); 
        update spb;
        
        Product2 newprod =new Product2();
        newprod.Name='Top of the Range';
        newprod.productCode='1234';
        newprod.isActive = true;
        newprod.Target_Price__c = 50;
        insert newprod;
       
	    
        
         Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        Campaign_Product__c campaignproductObj = bg_Test_Data_Utils.createCampaignProduct(camp.Id, newprod.Id);
        insert campaignproductObj;
        
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;

        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert opp;
        
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead lead  = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
    	
    	Lead_Product__c leadproduct= bg_Test_Data_Utils.createLeadProduct(camp.Id, newprod.Id, lead.Id);
        insert leadproduct;
        Competitor__c competitor = bg_Test_Data_Utils.createCompetitor(lead.Id);
        insert competitor;
        
    	test.startTest();
	    Database.LeadConvert lc = new Database.LeadConvert();
        leadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setLeadId(lead.id);
        lc.setOpportunityName('test opp');
	    Database.LeadConvertResult lcr = Database.convertLead(lc);
        //System.assertEquals('Converted To Opportunity', testLead.Status);
        test.stopTest();
    }
}