/**********************************************************************
* bg_WildebeestCallout_Test:
*
* Class to test object bg_WildebeestCallout_Test used to send 
* synchro messages to Wildebeest 
* Created By: SA
* Created Date: 24-03-2016
*
* Changes: 
* BrightGen - Ismail Basser 14/03/2018
*			  testCasePHSaccountUpdate test method added
***********************************************************************/

@isTest
global class bg_WildebeestCallout_Test
{
	global class MockHttpResponseGenerator implements HttpCalloutMock
    {
		  String rBody;
		  Integer rCode;

		  global MockHttpResponseGenerator(String rBody, Integer rCode)
          {
			   this.rBody = rBody;
			   this.rCode = rCode;
		  }

	     global HTTPResponse respond(HTTPRequest req)
         {
	        System.debug('mock req: ' + req);
	        
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody(rBody);
	        res.setStatusCode(rCode);
	        return res;
	    }
	}


    static Case createCase()
    {
        Account act = new Account(Name = 'Test Account 1', Wildebeest_Ref__c = '01', BillingPostalcode = 'LS1 3DD');
        insert act;

        PHS_Account__c phsAct = new PHS_Account__c(Wildebeest_Ref__c = '01', Service_Enabled__c=true, Salesforce_Account__c = act.Id);
        insert phsAct;

        Case c = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, PHS_Account__c=phsAct.Id, Origin='01', Type='Cancellation', Case_Reason__c='01', Case_Ref__c = '1234', AccountID = act.Id);
        return c;
    }


    @testSetup static void setup()
    {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        integrationSetting.Integration_Log_Purge_Limit__c = 5;
        if(integrationSetting == null)
        {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            insert integrationSetting;
        }
    }
 

    @isTest static void testCaseInsert()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
		insert c;

        test.stopTest();

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assertEquals('1234567', cu.Case_Ref__c);
    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }

    
    @isTest static void testCaseUpdate()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
        insert c;

        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        
        c.Description = 'test update';
        c.Status = 'Open';
        c.ReadOnly__c = false;
        update c;

        test.stopTest();

        Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assertEquals('1234567', cu.Case_Ref__c);
    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }    

    @isTest static void testCasePHSaccountUpdate()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Origin='01', Type='Cancellation', Case_Reason__c='01', Case_Ref__c = '1234');
        insert c;

        Account act = new Account(Name = 'Test Account 1', Wildebeest_Ref__c = '01', BillingPostalcode = 'LS1 3DD');
        insert act;

        PHS_Account__c phsAct = new PHS_Account__c(Wildebeest_Ref__c = '01', Service_Enabled__c=true, Salesforce_Account__c = act.Id);
        insert phsAct;

        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        
        c.ReadOnly__c = false;
        c.AccountID = act.Id;
        c.PHS_Account__c=phsAct.Id;
        update c;

        test.stopTest();

        Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

        System.assertEquals('1234567', cu.Case_Ref__c);
        System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }    


    @isTest static void testCaseCommentInsert()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
        insert c;

        test.stopTest();

        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        System.debug('testCaseCommentInsert: '+ c);

		CaseComment cc = new CaseComment(ParentId = c.Id);
		insert cc;          

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assertEquals('1234567', cu.Case_Ref__c);
    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }


    @isTest static void testCaseCommentUpdate()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
        insert c;
        
        test.stopTest();
        
        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        System.debug('testCaseCommentUpdate: '+ c);
        
        CaseComment cc = new CaseComment(ParentId = c.Id);
        insert cc;

        cc = [select Id, CommentBody from CaseComment where ParentId =: c.Id limit 1];
        System.debug('testCaseCommentUpdate: '+ cc);
        
        cc.CommentBody = 'test';
		update cc;

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c, Case_Comment_Added_Date__c from Case where Id =: c.Id limit 1];
                    
    	System.assertEquals('1234567', cu.Case_Ref__c);
       
        System.assert(!cu.Sync_Wildebeest_Message__c.contains('Unexpected server response'), 'Sync message is incorrect. Found [' + cu.Sync_Wildebeest_Message__c + ']');
        System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }


    @isTest static void testTaskInsert()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
        insert c;

        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        System.debug('testCaseUpdate: '+ c);

		Task t = new Task(WhatId = c.Id, Due_Date_and_Time__c = datetime.now().addDays(1), Task_IsActive__c = true);
		insert t;

        test.stopTest();

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assertEquals('1234567', cu.Case_Ref__c);
    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }


    @isTest static void testTaskUpdate()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
        insert c;

        c = [select Id, Status, ReadOnly__c, Case_Ref__c, Sync_Wildebeest_Message__c from Case limit 1];
        System.debug('testCaseUpdate: '+ c);

        Task t = new Task(WhatId = c.Id, Due_Date_and_Time__c = datetime.now().addDays(1), Task_IsActive__c = true);
        insert t;

        t = [select Id from Task where WhatId =: c.Id limit 1];
        System.debug('testTaskUpdate: '+ t);

        t.Description = 'test';
		update t;

        test.stopTest();

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assertEquals('1234567', cu.Case_Ref__c);
    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
    }

    
    @isTest static void testUnexpectedServerResponse()
    {
        test.startTest();

        String res = '{}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
		insert c;

        test.stopTest();

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];
    	System.assertEquals(null, cu.Sync_Wildebeest_Message__c);
    }


    @isTest static void testUnexpectedResponseField()
    {
        test.startTest();

        String res = '{"t1":"t1",EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        Case c = createCase();
		insert c;

        test.stopTest();

    	Case cu = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id =: c.Id limit 1];

    	System.assert(cu.Sync_Wildebeest_Message__c.contains('Unexpected server response'));
    }


    @isTest static void testBulkCaseInsert()
    {
        test.startTest();

        String res = '{"EntityClassRef":"Case","EntityId":1234567,"Message":"7654321","SalesforceId":"01","Success":true}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        List<Case> caseList = new List<Case>();

        for (Integer i=0; i<10; ++i)
        {
            Case c = createCase();
    		caseList.add(c);
        }

        insert caseList;

        test.stopTest();

        List<Id> caseIdList = new List<Id>();
        for(Case c : caseList)
        {
        	caseIdList.add(c.Id);
    	}

        List<Case> cul = [select Id, Case_Ref__c, Sync_Wildebeest_Message__c from Case where Id in : caseIdList];

        for(Case cu : cul) {
	    	System.assertEquals('1234567', cu.Case_Ref__c);
	    	System.assertEquals('7654321', cu.Sync_Wildebeest_Message__c);
	    }
    }
    

    @isTest static void testViewExistingContractLines()
    {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        phsAccount.Wildebeest_Ref__c = '288';
        insert phsAccount;

        test.startTest();
        String res = '{"ContractLines": [{"CommencementDate": "2013-02-01T00:00:00", "AgreementTypeName": "Service", "ContractLineValue": 27.04, "ProductName": "Eclipse Sanitary Disposal Grey Base Grey Lid", "ServiceFrequencyName": "026", "UnitPrice": 27.04, "ContractTermName": "3 Years", "CurrencySymbol": "\u00a3", "PHSAccountReference": 288, "ContractLineId": 83900, "Quantity": 1, "ProductRef": "16403812", "PHSAccountName": "Buckinghamshire Care Ltd", "ExpiryDate": "2016-01-31T00:00:00", "ContractFrequency": 26}], "ResultSize": 27, "Visits": null, "RenegotiationDate": null, "Success": true, "Message": null, "DebtProfile": null, "SaleOrderLines": null}';
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(res, 200);
        Test.setMock(HttpCalloutMock.class, mock);

        bg_ViewContractLineController viewContractController = new bg_ViewContractLineController(new ApexPages.StandardController(a));
        viewContractController.isContractLine = true;
        viewContractController.setUserinput();

        test.stopTest();

        system.assertEquals(27, viewContractController.response.ResultSize);
    }  
}