/*************************************************
bg_AgreementHelper

Helper class for the Order object. Includes methods to be fired off a Javascript button & a trigger.

Author: Tom Morris (BrightGen)
Created Date: 02/11/2016
Updates: KH 21/11/2016 - Update to createAgreementAndLineItemsFromButtonPress to check for Contact Role
**************************************************/

global class bg_AgreementHelper
{
    /*
        - This method is to be called from the Javascript button ('Create Contract') on the Opportunity page layout. 
        - It creates a new instance of an Agreement and Agreement Items with values copied from the Opportunity and Opportunity Products.
    */
    webservice static String createAgreementAndLineItemsFromButtonPress(Id opportunityId, Integer productCount)
    {
        if(productCount > 0)
        {

            List<OrderItem> agreementItemsToInsert = new List<OrderItem>();

            List<OpportunityLineItem> oppLineItemsToProcess = [SELECT Id, Name, TotalPrice, UnitPrice, ListPrice, Quantity__c, PricebookEntryId, Agreement_Type__c,
                                                                      Description, Service_Frequency__c, Siting__c, ServiceDate, Revised_Term__c,
                                                                      Opportunity.Id, Opportunity.Amount, Opportunity.CloseDate, Opportunity.Name,
                                                                      Opportunity.AccountId, Opportunity.ContractId, Opportunity.StageName, Opportunity.Pricebook2Id, 
                                                                      Opportunity.SyncedQuote.Id
                                                                      FROM OpportunityLineItem WHERE opportunityId =: opportunityId];

            List<OpportunityContactRole> oppContactRoles = [SELECT Id, ContactId FROM OpportunityContactRole WHERE OpportunityId = :opportunityId AND IsPrimary = True LIMIT 1];

            OpportunityLineItem oppLineItemToProcess = oppLineItemsToProcess[0];

            // Create the new Agreement and copy the Opportunity fields across
            Order newAgreement = new Order();
            newAgreement.Name = oppLineItemToProcess.Opportunity.Name + 'Agreement';
            newAgreement.Status = 'Draft';
            newAgreement.QuoteId = oppLineItemToProcess.Opportunity.SyncedQuote.Id;
            newAgreement.AccountId = oppLineItemToProcess.Opportunity.AccountId;
            newAgreement.ContractId = returnContractId(oppLineItemToProcess.Opportunity.AccountId);
            newAgreement.Pricebook2Id = oppLineItemToProcess.Opportunity.Pricebook2Id;
            newAgreement.OpportunityId = oppLineItemToProcess.Opportunity.Id;
            newAgreement.EffectiveDate = System.today();

            if (!oppContactRoles.isEmpty())
            {
                newAgreement.Customer_Signatory__c = oppContactRoles[0].ContactId;
            }

            insert newAgreement;
            
            Agreement_Location__c agrloc = new Agreement_Location__c(Agreement__c = newAgreement.id, Location_Salesforce_Account__c = newAgreement.AccountId = oppLineItemToProcess.Opportunity.AccountId);
            insert agrloc;

            // Loop around the Products related to the Opportunity and create Agreement Items from them
            for(OpportunityLineItem oli : oppLineItemsToProcess)
            {
                OrderItem newAgreementItem = new OrderItem();
                newAgreementItem.Agreement_Location__c = agrloc.id;
                newAgreementItem.Term__c = oli.Revised_Term__c;
                newAgreementItem.OrderId = newAgreement.Id;
                newAgreementItem.Quantity = oli.Quantity__c;
                newAgreementItem.Siting__c = oli.Siting__c;
                newAgreementItem.UnitPrice = oli.UnitPrice;
              //  newAgreementItem.ServiceDate = oli.ServiceDate;
                newAgreementItem.Description = oli.Description;
                newAgreementItem.PricebookEntryId = oli.PricebookEntryId;
              //newAgreementItem.Business_Type__c = oli.Business_Type__c;
                newAgreementItem.Agreement_Type__c = oli.Agreement_Type__c;
                newAgreementItem.Service_Frequency__c = oli.Service_Frequency__c;
                newAgreementItem.Unit_Price_PA__c = (oli.TotalPrice / oli.Quantity__c);
                newAgreementItem.Total_Price__c = oli.TotalPrice;

                agreementItemsToInsert.add(newAgreementItem);
            }

            if(!agreementItemsToInsert.isEmpty())
            {
                insert agreementItemsToInsert;
            }

            return String.valueOf(newAgreement.Id);
        }

        return null;
    }

    /*
        Method that takes an Account Id and returns the Id of a Contract that relates to it  
    */
    public static Id returnContractId(Id acctId)
    {

        List<Contract> contractIds = [SELECT Id FROM Contract WHERE AccountId =: acctId AND Status = 'Activated'];

        if(!contractIds.isEmpty())
        {
            return contractIds[0].Id;
        }
        else
        {
            return null;
        }
    }


    /*
        - Method called from a Javascript button
        - Returns TRUE if there are any Open Agreements found that relate to the Opportunity
    */
    webservice static Boolean returnResultOfOpenAgreement(Id opportunityId)
    {

        List<Order> relatedAgreements = [SELECT Id FROM Order WHERE OpportunityId =: opportunityId AND Status != 'Closed'];

        if( relatedAgreements.size() > 0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /*
        - To be fired from an after update trigger
        - Checks if the Signature_Authorised__c checkbox has changed from FALSE to TRUE
        - If so, passes the Agreement to the next method for operation
    */
    public static void updateParentOpportunityCloseDate(List<Order> newAgreements, Map<Id, Order> oldAgreements)
    {

        List<order> agreementsToUpdate = new List<Order>();

        for(Order agreement : newAgreements)
        {
            Order oldAgreement = oldAgreements.get(agreement.Id);

            // Check if the Signature_Authorised__c checkbox has changed from FALSE to TRUE
            Boolean signatureAuthorisedHasChanged = oldAgreement.Signature_Authorised__c == FALSE && agreement.Signature_Authorised__c != oldAgreement.Signature_Authorised__c;

            if(signatureAuthorisedHasChanged)
            {
                agreementsToUpdate.add(agreement);
            }
        }

        if(!agreementsToUpdate.isEmpty())
        {
            updateParentOpportunityCloseDate(agreementsToUpdate);
        }
    }

    /*
        Takes an Agreement as an input and updates the parent Opportunity
    */
    public static void updateParentOpportunityCloseDate(List<Order> newAgreements)
    {

        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        // Loop around any Agreements and update their parent Opportunities
        for(Order agreement : newAgreements)
        {
            Opportunity newOpp = new Opportunity();
            newOpp.Id = agreement.OpportunityId;
            newOpp.StageName = 'Contract Won';
            newOpp.Closure_Reason__c = 'Won';
            newOpp.Next_Renewal_Date__c = System.today().addDays(30);
            newOpp.CloseDate = System.today();
            newOpp.Commencement_Date__c = agreement.EffectiveDate;
            oppsToUpdate.add(newOpp);
        }

        if(!oppsToUpdate.isEmpty())
        {
            update oppsToUpdate;
        }
    }
}