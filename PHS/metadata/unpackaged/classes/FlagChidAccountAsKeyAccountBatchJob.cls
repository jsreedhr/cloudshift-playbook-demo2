global class FlagChidAccountAsKeyAccountBatchJob implements Database.Batchable<sObject>,Database.Stateful
{
    //global Integer recordsProcessed = 0;

    /*
    global FlagChidAccountAsKeyAccountBatchJob(Set<ID> ids ){
        query= 'select Id from account where id in: idsstr';
        idsstr=ids;
    }
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select Id, Parent_Account__c, Child_Account__c From PHS_Account_Relationship__c' +
            ' Where Parent_Account__r.Key_Account__c = TRUE AND Parent_Account__r.HasNegotiatorResponsibility__c = TRUE AND isDeleted = FALSE';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<PHS_Account_Relationship__c> phsParentList){
        if(phsParentList.size()<=0)
            return;

        Set<Id> parentIdSet = new Set<Id>();
        List<Account> accountUpdateList = new List<Account>();
        List<PHS_Account_Relationship__c> phsChildList = new List<PHS_Account_Relationship__c>();
        Map<Id, PHS_Account_Relationship__c> phsChildMap = new Map<Id, PHS_Account_Relationship__c>();
        Map<Id, PHS_Account_Relationship__c> phsExploredMap = new Map<Id,PHS_Account_Relationship__c>();

        try{
            
            phsChildList = [Select Id, Parent_Account__c, Child_Account__c From PHS_Account_Relationship__c Where Parent_Account__c NOT IN :parentIdSet
             AND Parent_Account__r.Key_Account__c = FALSE AND Parent_Account__r.HasNegotiatorResponsibility__c = FALSE AND isDeleted = FALSE];
        }catch(Exception ex){
            System.debug('Failed to get PHS_Account_Relationship__c records : ' + ex.getMessage());
        }

        for(PHS_Account_Relationship__c phs : phsParentList){
            parentIdSet.add(phs.Parent_Account__c);
            Account account = new Account(
                Id = phs.Child_Account__c,
                Key_Account__c = TRUE,
                HasNegotiatorResponsibility__c = FALSE
            );
        }

        if(phsChildList.size() > 0){
            for(PHS_Account_Relationship__c phs : phsChildList){
                if(!phsChildMap.containsKey(phs.Parent_Account__c))
                    phsChildMap.put(phs.Parent_Account__c, phs);
            }
            Id pId;
            //strating from first child, we go to the depth to find all indirect children for every parent Account in pshParentList
            for(PHS_Account_Relationship__c phs : phsParentList){
                pId = phs.Child_Account__c;
                while(phsChildMap.containsKey(pId) && !phsExploredMap.containsKey(pId)){
                    PHS_Account_Relationship__c phsChild = phsChildMap.get(pId);
                    Account account = new Account(
                        Id = phsChild.Child_Account__c,
                        Key_Account__c = TRUE,
                        HasNegotiatorResponsibility__c = FALSE
                    );
                    accountUpdateList.add(account);
                    phsExploredMap.put(phsChild.Parent_Account__c, phsChild);
                }
            }
        }

        try{
            system.debug('Hormese');
            system.debug(accountUpdateList);
            update accountUpdateList;
        }catch(Exception ex){
            System.debug('Failed to update account records : ' + ex.getMessage());
        }

    }

    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
        	From AsyncApexJob Where Id =: bc.getJobId()];

	    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    String[] toAddresses = new String[] {a.CreatedBy.Email};
	    mail.setToAddresses(toAddresses);
	    mail.setSubject('KAM Key Roles Batch Status : ' + a.Status);
	    mail.setPlainTextBody('Records processed : ' + a.TotalJobItems +
	   		'with '+ a.NumberOfErrors + ' failures.');
	    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });


    }
}