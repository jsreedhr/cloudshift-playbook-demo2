global  class ScheduleFlgChildAccountBatchJob implements schedulable {

    global void execute(SchedulableContext SC) {
        queryToBatch();
        FlagChidAccountAsKeyAccountBatchJob b = new FlagChidAccountAsKeyAccountBatchJob();
        database.executebatch(b);
    }
    public void queryToBatch()
    {
        Set<ID> keyAccounts = new Set<ID>();
        List<Account> childAccounts = new List<Account>();
        for(Account a : [Select Id,Key_Account__c From account Where key_account__c = TRUE and HasNegotiatorResponsibility__c = FALSE]){
            a.Key_Account__c = FALSE;
            childAccounts.add(a);
        }
        if(childAccounts.size() > 0)
            update childAccounts;

    }

}