public class RedirectToAccessTimesPage {
    
    public Boolean redirectVariable     {get; set;}
    public String redirectURL           {get; set;}
    public String recordId              {get; set;}
    
    public RedirectToAccessTimesPage(ApexPages.StandardController controller){
    recordId = controller.getId();
    //List<account> newAccounts = [select id, Name, CreatedDate from Account where createdDate <:DateTime.now() AND createdDate >:Datetime.now().addMinutes(-15) AND id:= ];
    Agreement_Location__c agreeLoc = [ Select Id, Location_Salesforce_Account__c, Location_Checked__c from Agreement_Location__c where Location_Salesforce_Account__r.createdDate <:DateTime.now() and Location_Salesforce_Account__r.createdDate >:Datetime.now().addMinutes(-15) and Location_Checked__c !=true and id =: RecordId limit 1];
    redirectVariable = false;
    if(agreeLoc != null){
        redirectVariable = true;
        redirectURL = 'https://phssales--uat--c.cs80.visual.force.com/apex/AccessTime?id=' + RecordId;
        
        
    }
    }
    
    public void updateAgreementLocation(){
        if(redirectVariable){
            Agreement_Location__c agreeLoc = new Agreement_Location__c( id= recordId, Location_Checked__c = true);
            update agreeLoc;
        }
    }
    
    
    
    
    

}