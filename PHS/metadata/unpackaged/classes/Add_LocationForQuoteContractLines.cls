public with sharing class Add_LocationForQuoteContractLines 
{
    private Contract objContract {get;set;}
    public Add_LocationForQuoteContractLines(ApexPages.standardController stndCtrl)
    {
        objContract = (Contract) stndCtrl.getRecord();
    }
    public void saveBtn()
    {
        
    }
    public pageReference cancelBtn()
    {
        return new pageReference('/'+objContract.id);
    }
}