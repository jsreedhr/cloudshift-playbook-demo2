public without sharing class OpportunityCompetitorsController {
    public string errOnPage {
        get;
        set;
    }
    public list < Competitor__c > listPortLinks {
        get;
        set;
    }
    public list < Opportunity > listOpportunity {
        get;
        set;
    }
    public string rowNo {
        get;
        set;
    }
    Public String OppId;
    Public Account CompAccount;
    public integer noOfRows {
        get;
        set;
    }
    //public integer projectDuration{get; set;}
    //public map<string,integer> mapMonthNo;
    //public map<integer,string> mapNoMonth;
    public List < booleanProjectWrapper > lstOfbooleanProjectWrapper {
        get;
        set;
    }
    public boolean selectAll {
        get;
        set;
    }
    public Boolean showerror {
        get;
        set;
    }
    public integer errorPos {
        get;
        set;
    }
    //constructor to grt the records

    public OpportunityCompetitorsController(apexpages.standardController stdController) {
 errOnPage = '';
        listOpportunity = new list < Opportunity > ();
        listPortLinks = new list < Competitor__c > ();
        lstOfbooleanProjectWrapper = new List < booleanProjectWrapper > ();
        selectAll = false;
        noOfRows = 1;
        OppId = apexpages.currentpage().getparameters().get('id');
        listOpportunity = [select Id, Name, StageName, Amount from Opportunity where id =: OppId limit 1];
        CompAccount = [SELECT ID, Name FROM Account WHERE Name =: Label.Unknown_Competitor_Name];
        listPortLinks = [select Id, Name, Lead__c, Opportunity__c, Product_Offering__c, Contract_Start_Date__c, Contract_End_Date__c, Lost_To__c, Competitor_Account__c, Unknown_Competitor_Details__c, Estimate__c, Calculate_Expiry_Days__c, is_Primary_Competitor__c, is_Current_Supplier__c, is_Self_Supplied__c, is_PHS_Supplied__c from Competitor__c where Opportunity__c =: OppId limit 4999];

        for (Competitor__c obj_portlink: listPortLinks) {
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, obj_portlink));
        }
    }


    public Void createRecords() {
        selectAll = false;
        system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if (lstOfbooleanProjectWrapper.size() > 0)

        {
            integer l = lstOfbooleanProjectWrapper.size() - 1;
            for (integer i = 1; i <= lstOfbooleanProjectWrapper[0].ValuetoList; i++) {
                Competitor__c objportlink = new Competitor__c();
                system.debug('######------------++++' + l);

                objportlink.Opportunity__c = OppId;

                listPortLinks.add(objportlink);
                lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));

            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                'You need atleast 1 record'));
        }

    }



    // To Create a new competitor record on click of Add row button


    public Void addRow() {
        selectAll = false;
        for (integer i = 0; i < noOfRows; i++) {
            Competitor__c objportlink = new Competitor__c();
            objportlink.Opportunity__c = OppId;

            listPortLinks.add(objportlink);
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));
        }
    }

    public void selectAll() {

        if (selectAll == true) {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = false;
            }
        }
    }

    Public void deleteSelectedRows() {
            selectAll = false;
            list < Competitor__c > toDeleteRows = new list < Competitor__c > ();
            for (integer j = (lstOfbooleanProjectWrapper.size() - 1); j >= 0; j--) {
                if (lstOfbooleanProjectWrapper[j].isSelected == true) {
                    if (lstOfbooleanProjectWrapper[j].objport.id != null) {
                        toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                    }

                    lstOfbooleanProjectWrapper.remove(j);

                }


            }
            delete toDeleteRows;
        }
        /**
        *   Method Name:    DelRow
        *   Description:    To delete the record by passing the row no.
        *   Param:  RowNo

        */

    public Void delRow() {

            selectAll = false;
            system.debug('--------' + RowNo);
            list < Competitor__c > todelete = new list < Competitor__c > ();

            if (lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null) {
                todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);

            }


            lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
            delete todelete;


        }
        /**
         *   Method Name:    saveCompetitor
         *   Description:    To save the records and then redirect it to respective opportunity
         */

    public void saveCompetitor() {
        errOnPage = '';
        if (showerror != true) {
            list < Competitor__c > listToUpsert = new list < Competitor__c > ();
            boolean isBlank = false;
            //system.debug('@@@@@@@@@@'+listProjRev.size());

            for (booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper) {

                if (objportlink.objport.Competitor_Account__c == null && objportlink.objport.Product_Offering__c == null) {
                    errOnPage = 'error';
                    // errorOnPage=true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'Competitor Account: You must enter a value <br> Product Offering: You must enter a value'));

                } else if (objportlink.objport.Competitor_Account__c == null && objportlink.objport.Is_Self_Supplied__c == FALSE) {
                    errOnPage = 'error';
                    // errorOnPage=true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'Competitor Account: You must enter a value'));

                } else if (objportlink.objport.Product_Offering__c == null) {
                    errOnPage = 'error';
                    // errorOnPage=true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                        'Product Offering: You must enter a value'));
                } else if (objportlink.objport.Competitor_Account__c == CompAccount.id && objportlink.objport.Unknown_Competitor_Details__c == Null) {
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'Please specify competitor details in the Unknown Competitor Details Field e.g. name and address so that a new competitor can be setup'));

                } else
                {
                     errOnPage = '';
                    listToUpsert.add(objportlink.objport);
                }

            }
            if (listToUpsert.size() > 0 && errOnPage != 'error') {
                try {
                    Upsert ListtoUpsert;
                    /* PageReference acctPage = new PageReference('/' + OppId);
                     acctPage.setRedirect(true);
                     return acctPage; */

                } catch (Exception e) {
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'There is missing information on either the lead or opportunity, please edit and save the lead record to identify the missing information.'));
                    // return null;
                }
            }
        }
        //  return null;
    }


    public void checkduplicates() {
        Set < String > dupcheckset = new Set < String > ();
        showerror = false;
        errorPos = 0;
        Integer i = 0;
        for (booleanProjectWrapper bwrap: lstOfbooleanProjectWrapper) {
            String str = String.valueOf(bwrap.objport.Competitor_Account__c) + String.valueOf(bwrap.objport.Product_Offering__c);
            /*
            if(bwrap.objport.Competitor_Account__c !=null &&  !dupcheckset.contains(bwrap.objport.Competitor_Account__c))
            {
                dupcheckset.add(bwrap.objport.Competitor_Account__c);
            }
            else if(bwrap.objport.Competitor_Account__c !=null && dupcheckset.contains(bwrap.objport.Competitor_Account__c))
            {
                showerror =true;
                errorPos=i;
                return;

            }
            */

            if (bwrap.objport.Competitor_Account__c != null && bwrap.objport.Competitor_Account__c != CompAccount.id && bwrap.objport.Product_Offering__c != null && !dupcheckset.contains(str)) {
                dupcheckset.add(str);
            } else if (bwrap.objport.Competitor_Account__c != null && bwrap.objport.Competitor_Account__c != CompAccount.id && bwrap.objport.Product_Offering__c != null && dupcheckset.contains(str)) {
                showerror = true;
                errorPos = i;
                return;

            }
            i++;


        }

    }
    public class booleanProjectWrapper {
        public Boolean isSelected {
            get;
            set;
        }
        public Competitor__c objport {
            get;
            set;
        }
        public integer ValuetoList {
            get;
            set;
        }

        public booleanProjectWrapper(boolean isSelect, Competitor__c objports) {
            objport = objports;
            isSelected = isSelect;
            ValuetoList = 1;

        }
    }

}