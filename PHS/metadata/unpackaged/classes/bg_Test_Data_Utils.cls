/*********************************************************************************
 * bg_Test_Data_Utils:
 *
 * Class that contains methods for creating new records to be used in tests.
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 12-01-2016
 *          22-12-2016 AL - Added setUpWildebeestDates and createCaseComment
 *          29-12-2016 KH - Merged with CPQ Project and added createAgreement and createDocuSignStatus                    
 *********************************************************************************/
@isTest
public without sharing class bg_Test_Data_Utils {
    private static Profile stdUserProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
    public static Lead createLead(string i) 
    {
        String leadStatus = SObjectType.Lead.Fields.Status.PicklistValues[0].getValue();
        String leadSource = SObjectType.Lead.Fields.LeadSource.PicklistValues[0].getValue();
        String leadEntityType = SObjectType.Lead.Fields.Entity_Type__c.PicklistValues[0].getValue();
        String companyStatus = SObjectType.Lead.Fields.Company_Status__c.PicklistValues[0].getValue();
        Lead testLead = new Lead(LastName = i,
                                FirstName =  i,
                                Company = 'Test Co. ' + i,
                                Status = 'New',
                                Entity_Type__c = leadEntityType,
                                LeadSource = leadSource,
                                Company_Status__c = companyStatus,
                                Phone = '01245 564735',
                                Email = 'test@test.com',
                                Salutation = 'Mr.',
                                PostalCode = 'TS1 3ST',
                                //Score_1__c = 30,
                                Wildebeest_Ref__c = '0003442'+i,
                                Title = 'Lead Title',
                                Timescale__c = 'Immediately',
                                Street = 'Test street',
                                City = 'Blah',
                                State = 'Arizona'
                        );
        return testLead;
    }

    public static Object_Owner_Mapping__c createObjectOwnerMapping(string name) 
    {
        Object_Owner_Mapping__c mapping = new Object_Owner_Mapping__c(
            Employee_Number_Field__c = 'Employee_Number__c',
            Target_Owner_Field_Name__c = 'OwnerId',
            Name = name);
        return mapping;
    }

    public static User createUserWithEmployeeNumber(String employeeNumber) {
        User u = new User(Alias = 'standt', 
            Email='standard@org.com', 
            EmailEncodingKey='UTF-8', 
            LastName='Testing' + employeeNumber, 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = stdUserProfile.Id,
            TimeZoneSidKey='Europe/London',
            UserName=employeeNumber + 'standard@testorg.com');
        return u;
    }

    public static Account createAccount(String name) {
        Account acc = new Account(
                Name = 'Test Account ' + name,
                Billingstreet = 'test street',
                BillingCity = 'test city',
                BillingPostalcode = 'LS1 3DD',
                BillingCountry = 'testcountry',
                Wildebeest_Ref__c ='swq',
                BillingState ='test state',
                Key_Account__c =true,
                Greenleaf_Key_Account__c = true,
                Industry ='Apparel',
                Company_Reg_No__c ='QWER1'+name,
                Company_Status__c ='Prospect',
                Website ='test@org.com',
                HasNegotiatorResponsibility__c = TRUE,
                Phone = '01273464629'
            );
            
        return acc;
    }

    public static Contact createContact(String name, Id accountId) {
        Contact cont = new Contact(
                LastName = 'Test Contact ' + name,
                AccountId = accountId,
                Description = 'Test',
                Email = 'Test@equals.com',
                FirstName = 'Michael',
                HasOptedOutOfEmail = false,
                LeadSource = 'Phone Enquiry',
                MobilePhone = '07765 564371',
                Phone = '01234 453122',
                Salutation = 'Mr',
                Title = 'Head of Procurement',
                Mailingstreet = 'test street',
                MailingCity = 'test city',
                MailingPostalcode = 'LS1 3DD',
                MailingCountry = 'testcountry',
                MailingState ='test state'
                );

        return cont;
    }

    public static PHS_Account__c createPHSAccount(String wildebeestRef, Id parentAccountId){
        PHS_Account__c phsAccount = new PHS_Account__c(
                Service_Enabled__c = true,
                Wildebeest_Ref__c = wildebeestRef,
                Salesforce_Account__c = parentAccountId
            );
        return phsAccount;
    }
    
    public static PHS_Account_Relationship__c createcreatePHSAccountRelation(String parentacctId, String childacctId, String parentPHSAccount, String childPHSAccount){
        String relationshipType = SObjectType.PHS_Account_Relationship__c.Fields.Relationship_Type__c.PicklistValues[0].getValue();
        PHS_Account_Relationship__c phs = new PHS_Account_Relationship__c(
                Relationship_Type__c = relationshipType,
                Parent_Account__c = parentacctId,
                Child_Account__c = childacctId,
                Parent_PHS_Account__c = parentPHSAccount,
                Child_PHS_Account__c = childPHSAccount
            );
        return phs;
    }
    
    public static Opportunity createOpportunity(String name, Id accountId) {
        Opportunity opp = new Opportunity(
                Name = name,
                StageName = 'Negotiation',
                AccountId = accountId,
                CloseDate = Date.Today(),
                Type = 'New Business'
            );

        return opp;
    }

    public static Event createEvent(String name, Id accountId) {
        Event evt = new Event(
                Subject = name,
                StartDateTime = Datetime.now(),
                EndDateTime =  Datetime.now().addDays(1),
                WhatId = accountId
            );

        return evt;
    }

     public static Task createTask(String name, Id accountId) {
        Task tsk = new Task(
                Subject = name,
                WhatId = accountId,
                Status='Not Started',
                Priority='Normal'
            );

        return tsk;
    }

    public static Case createCase(String title, Id conId, Id accId){
        String caseRecordTypeId = [Select Id From RecordType Where SobjectType = 'Case' and Name = 'Standard'].Id;
        String caseOrigin = SObjectType.Case.Fields.Origin.PicklistValues[0].getValue();
        Case theCase = new Case(
                Subject = title,
                ContactId = conID,
                AccountId = accId,
                Status = 'Working',
                Origin = caseOrigin
            );

        return theCase;
    }
    
    public static CaseComment createCaseComment(String title, Id caseId){
        CaseComment caseCom = new CaseComment(
                ParentId = caseId,
                CommentBody = title
            );

        return caseCom;
    }

    public static Wildebeest_Integration__c setUpWildebeestDates(String format){
        Wildebeest_Integration__c integrationSettings = new Wildebeest_Integration__c(
                Name='Host',
                Wildebeest_Date_Format__c = 'dd/MM/yyyy HH:mm:ss',
                Integration_Log_Purge_Limit__c = 50
            );

        return integrationSettings;
    }

    public static Order createAgreement(Id accountId, Id opportunityId, Date effectiveDate, String status) {
        Order agreement = new Order(
                AccountId = accountId,
                OpportunityId = opportunityId,
                EffectiveDate =  effectiveDate,
                Status = status
            );

        return agreement;
    }

    public static dsfs__DocuSign_Status__c createDocuSignStatus(Id agreementId, String status) {
        dsfs__DocuSign_Status__c docStatus = new dsfs__DocuSign_Status__c(
                Agreement__c = agreementId,
                dsfs__Envelope_Status__c = status
            );

        return docStatus;
    }
    
    public static Front_Line_Team__c createFLT(String name){
        Front_Line_Team__c flt = new Front_Line_Team__c(Name = name);
        return flt;
    }
    
    public Static Agreement_Location__c createAgreementLocation(String accountId, String agreementId){
        Agreement_Location__c agL = new Agreement_Location__c(Location_Salesforce_Account__c = AccountId, Agreement__c = agreementId);
        return agL;
        
    }
    
    public static Campaign createCampaign(String name){
        Campaign camp = new Campaign(Name = name);
        return camp;
    }
    
    public static CampaignMember createCampaignMember(String leadId, String campId){
        CampaignMember campMem = new CampaignMember(LeadId = leadId, 
                                                    CampaignId = campId
                                                    );
        return campMem;
    }
    
    public static Product2 createaProduct2(String name){
        Product2 productObj = new Product2(Name = name
                                            //Agreement_Type__c = 'Sale';
                                );
      return productObj;
    }
    
    public static Campaign_Product__c createCampaignProduct(String campId, String prodId){
        Campaign_Product__c campaignproductObj = New Campaign_Product__c(
                                                      Product__c = prodId,
                                                      Annual_Cost__c = 100,
                                                      Floor_Price__c = 150,
                                                      Add_to_Existing_Open_Leads__c = TRUE,
                                                      Campaign__c = campId
                                                      );
      return campaignproductObj;  
    }

    public static Lead_Product__c createLeadProduct(String campId, String prodId, String leadId){
        Lead_Product__c leadproduct = new Lead_Product__c(
                                          Campaign__c = campId,
                                          Lead__c = leadId,
                                          Product__c = prodId
                                          );
        return leadproduct;
    }
    
    public static Competitor__c createCompetitor(String leadId){
        Competitor__c competitor = new Competitor__c(
                                         Lead__c = leadId,
                                         Strengths__c = 'Test Strength', 
                                         Weaknesses__c = 'Test Weakness'
                                         );
        return competitor;
    }
    
    public static PricebookEntry createPricebookEntry(String prodId, String pricebookId){
        PricebookEntry price = new PricebookEntry(
                                Pricebook2Id = pricebookId, 
                                Product2Id = prodId, 
                                UnitPrice = 20
                                );
        return price;
    }
    
    public static Quote createQuote(String name){
        Quote testQuote = new Quote(
                                Name = name
                                );
        return testQuote;
    }
    
    public static OpportunityLineItem createOpportunityLineItem(Opportunity opp){
        OpportunityLineItem testOppProduct = new OpportunityLineItem(
                                opportunityid = opp.Id,
                                Quantity = 2,
                                UnitPrice = 20
                                );
        return testOppProduct;
    }
    
    public static Copy_Quote_Line_Item__c createCopyQuoteLineItem(Id prodId, Id phsAccountId, Id agreementId){
       Copy_Quote_Line_Item__c quoteLineItem = new Copy_Quote_Line_Item__c(
                                    Product__c = prodId, 
                                    Quantity__c = 3, 
                                    Net_Price__c = 20, 
                                    Net_Total__c = 60, 
                                    Product_Range__c = 'Christmas', 
                                    Location__c = phsAccountId, 
                                    Agreement__c = agreementId,
                                    Minimum_Price__c = 0,
                                    Minimum_Total__c = 60,
                                    Maximum_Price__c = 100,
                                    Maximum_Total__c = 100,
                                    Contract_Term__c = '1',
                                    Agreement_Type__c = 'Despatch',
                                    Business_Type__c = 'Renewal',
                                    Proof_of_Service_Email__c = 'test@gmail.com',
                                    Siting__c = 'copy quote line item siting',
                                    Trial_Days__c = '30 days',
                                    Weight__c = 'copy quote line item weight',
                                    Serial_No__c = 'copy quote line item serial'
                                );
        return quoteLineItem;
    }
    
    public static OrderItem createOrderItem(Id phsAccountId, Id agreementId, Id priceBookEntryId, Id agreementLocationId){
        OrderItem oItem = new OrderItem(
                            Quantity = 3, 
                            Net_Price__c = 20, 
                            Net_Total__c = 60, 
                            Product_Range__c = 'Picklist TBC', 
                            Location_PHS_Account__c = phsAccountId, 
                            OrderId =  agreementId, 
                            UnitPrice = 30, 
                            PricebookEntryId = priceBookEntryId,
                            Agreement_Location__c = agreementLocationId
                        );
        return oItem;
    }
    
    public static Wildebeest_Integration__c createCSWildebeestIntegration(){
        Wildebeest_Integration__c wildBeest = new Wildebeest_Integration__c(
                                                    Name = 'Host',
                                                    Enable_Logging__c = true,
                                                    Http_Callout_Disable__c = false,
                                                    Http_Callout_Endpoint__c = 'https://sfintegration.phs.co.uk:9355',
                                                    Http_Callout_Max_Limit__c = 100,
                                                    Http_Callout_Password__c = 'Wildeb33st',
                                                    Http_Callout_Query__c = 'Receive.svc',
                                                    Http_Callout_Username__c = 'SalesforceReceiver',
                                                    Integration_Log_Purge_Limit__c = 30,
                                                    Wildebeest_Date_Format__c = null
                                                    );
        return wildBeest;
    }
    
    public static Access_Time__c createAccessTime(Id agreementId, Id accountId, Id productLineItem){
        Access_Time__c accessTime = new Access_Time__c(
                                        Agreement__c = agreementId,
                                        PHS_Account__c = accountId,
                                        Product_Line_Item__c = productLineItem,
                                        Day__c = '1. Monday',
                                        From__c = ' 11:00',
                                        To__c = '17:00'
                                        );
        return accessTime;
    }
}