public class GlobalConstants {
    
    public final static Map<String,AutocompleteObjectFieldset__c> CUSTOM_SETTINGS_NEW_LOOKUP_MAP = AutocompleteObjectFieldset__c.getAll();
    public final static Map<String,SearchLookupList__c> CUSTOM_SETTINGS_SEARCH_LOOKUP_MAP = SearchLookupList__c.getAll();

}