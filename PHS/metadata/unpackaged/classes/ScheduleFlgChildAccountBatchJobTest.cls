/**
 *  @Class Name:    ScheduleFlgChildAccountBatchJobTest
 *  @Description:   This is a test class for ScheduleFlgChildAccountBatchJob
 *  @Company:       dQuotient
 *  CreatedDate:    26/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           26/10/2017                  Original Version
 */
@isTest
private class ScheduleFlgChildAccountBatchJobTest {

	public static testMethod void testschedule() {
        
        Test.StartTest();
        ScheduleFlgChildAccountBatchJob sh = new ScheduleFlgChildAccountBatchJob();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh); 
        Test.stopTest(); 
	}
	
	/**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        Account a = bg_Test_Data_Utils.createAccount('test');
        a.key_account__c = TRUE;
        a.HasNegotiatorResponsibility__c = true;
        insert a;
        Account b = bg_Test_Data_Utils.createAccount('test2');
        b.key_account__c = TRUE;
        b.HasNegotiatorResponsibility__c = false;
        insert b;
        
        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;
        
        PHS_Account_Relationship__c acctRel = new PHS_Account_Relationship__c();
        acctRel.Relationship_Type__c = 'BuyerLocation';
        acctRel.Child_account__c = a.Id;
        acctRel.parent_account__c = b.Id;
        acctRel.Parent_PHS_Account__c = phsAccount.Id;
        acctRel.Child_PHS_Account__c = phsAccount.Id;
        insert acctRel;
    
    }

}