@isTest
public class AccessTimesControllerTest {
    @testSetup static void createData(){
	    
	    Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        system.assertNotEquals(acc.Id, null);
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        system.assertNotEquals(opp.Id, null);
        
        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('00034421', acc.Id);
        phsAccount.Account_Name__c = 'test Account_Name__c';
        insert phsAccount;
        system.assertNotEquals(phsAccount.Id, null);
        
        Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true,Price_Book_Type__c='Standard' );
        update standardPricebook;
        
        Product2 prodObj =  bg_Test_Data_Utils.createaProduct2('Test Product');
        insert prodObj;
        system.assertNotEquals(prodObj.Id, null);
        
        PricebookEntry priceBookEntry = bg_Test_Data_Utils.createPricebookEntry(prodObj.Id, standardPricebook.Id);
        priceBookEntry.IsActive = true;
        insert priceBookEntry;
        system.assertNotEquals(priceBookEntry.Id, null);
        
        
        
        Order orderObj = bg_Test_Data_Utils.createAgreement(acc.Id, opp.Id, system.today(), 'draft');
        orderObj.Pricebook2Id = standardPricebook.Id;
        insert orderObj;
        system.assertNotEquals(orderObj.Id, null);
        
        Agreement_Location__c agreementLocationObj = bg_Test_Data_Utils.createAgreementLocation(acc.id, orderObj.id);
        insert agreementLocationObj;
        
        OrderItem orderItemObj = bg_Test_Data_Utils.createOrderItem(phsAccount.Id, orderObj.Id, priceBookEntry.Id, agreementLocationObj.Id);
        insert orderItemObj;
        system.assertNotEquals(orderItemObj.Id, null);
        
        List<Access_Time__c> accessTimeList = new List<Access_Time__c>();
        for(Integer i=0; i<5; i++){
            Access_Time__c accessTime = bg_Test_Data_Utils.createAccessTime(orderObj.Id, phsAccount.Id, orderItemObj.Id);
            accessTimeList.add(accessTime);
        
        }
        accessTimeList[0].Day__c = '1. Monday';
        accessTimeList[1].Day__c = '2. Tuesday';
        accessTimeList[2].Day__c = '3. Wednesday';
        accessTimeList[3].Day__c = '4. Thursday';
        accessTimeList[4].Day__c = '5. Friday';
        insert accessTimeList;
        system.assertNotEquals(accessTimeList, null);
	}
	
    
    public static testMethod void testAddRow() {
	    
	    Account acc = [Select Id from Account];
        
        Order orderId = [Select Id From Order];
        system.debug('--orderid-->' +orderId);
        
        Agreement_Location__c agreementLocationObj = [select Id from  Agreement_Location__c];
        Test.startTest();
        PageReference pageRef = Page.AccessTime;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('id',orderId.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(orderId);
        AccessTimesController controller = new AccessTimesController(sc);
        AccessTimesAtLocationController controller1 = new AccessTimesAtLocationController();
       
          controller1.setAgreementLocationId(agreementLocationObj.id);
          controller1.getAgreementLocationId();
          controller1.getAccessTimesForAccount();
        ////////////////////////////////
             AccessTimesControllerT1 controllerT1 = new AccessTimesControllerT1(sc);
             controllerT1.selectAll = true;
             controllerT1.selectAll();
             
                
        controllerT1.addRow();
        controllerT1.saveAccessTime();
        controllerT1.lstOfbooleanProjectWrapper[0].objport.Account__c = acc.id;
        controllerT1.lstOfbooleanProjectWrapper[0].objport.Day__c = '1. Monday';
       // controllerT1.lstOfbooleanProjectWrapper[0].selectedPickListValues = new List<String>{'Greenleaf'};
        controllerT1.saveAccessTime();
       controllerT1.addLocation();
                      
        //controller.addRow();
        controller.selectAll = true;
        controller.selectAll();
        LisT<SelectOption> newOptions = controller.getPickListOptions();
        controller.addRow();
        controller.saveAccessTime();
        controller.lstOfbooleanProjectWrapper[0].objport.Account__c = acc.id;
        controller.lstOfbooleanProjectWrapper[0].objport.Day__c = '1. Monday';
        controller.lstOfbooleanProjectWrapper[0].selectedPickListValues = new List<String>{'Greenleaf'};
        controller.saveAccessTime();
        
        controller.addFiveRows();
        //controller.addLocation();
        //controller.addLocation();
        
        
        controller.fromTime = '06:00';
        controller.toTime = '12:00';
        controller.selectedDefaultPickListValues = new List<String>{'Greenleaf', 'Wastekit'};
        controller.defaultComments = 'test';
        controller.defaultCommentsSave();
        controller.defaultFromTime();
        controller.defaultToTime();
        controller.defaultDivisionsPicked();
        controller.RowNo = '1';
        controller.delRow();
        controller.selectAll = true;
        controller.selectAll();
        controller.deleteSelectedRows();
        controller.saveAccessTime();
        Test.stopTest();
    }

}