/***************************************************
* Class Name    : AgreementLocationTriggerTest
* Date Created  : 04 - Aug - 2018
* Date Modified : 04 - Aug - 2018
* Purpose       : This is a test class for AgreementLocationTrigger
* *************************************************/
@isTest
private class AgreementLocationTriggerTest {
	
	/**
     *  Method Name: createTestData
     *  Description:  method to create test data for testing AgreementLocationTriggerF
     *  Param:  NA
     *  Return: NA
     */
	@testSetup static void createTestData(){
		// Creation of Accounts
		List<Account> lstTestAccounts = new List<Account>();
		for(integer i=1; i<= 4; i++){
			Account testAccount = bg_Test_Data_Utils.createAccount(''+i);
			lstTestAccounts.add(testAccount);
		}
		insert lstTestAccounts;
		// Creation of Opportunity
		Opportunity testOpportunity = bg_Test_Data_Utils.createOpportunity('Test Opp', lstTestAccounts[0].id);
        insert testOpportunity;
        // Creation of Order
        Order testAgreement = bg_Test_Data_Utils.createAgreement(lstTestAccounts[0].Id, testOpportunity.Id, system.today(), 'Draft');
        insert testAgreement;
	}
	/**
     *  Method Name: testPopulateSalesforceAccountNamesonAgreement
     *  Description:  method to testing functionality of populating Account Names on Agreement
     *  Param:  NA
     *  Return: NA
     */
  /*  private static testMethod void testPopulateSalesforceAccountNamesonAgreement(){
    	List<Account> lstTestAccounts = new List<Account>();
    	lstTestAccounts = [Select id, Name From Account order by Name asc Limit 4];
    	List<Order> lstAgreements = new List<Order>();
    	lstAgreements = [Select id From Order Limit 1];
    	String expectedAccountNames = '';
    	Test.startTest();
    		// Creation of Agreement Locations
	        List<Agreement_Location__c> lstAgreementLocations = new List<Agreement_Location__c>();
	        List<Order> lstAgreementEval = new List<Order>();
			for(integer i=1; i<= 3; i++){
				Agreement_Location__c agreementLocation = bg_Test_Data_Utils.createAgreementLocation(lstTestAccounts[i].id, lstAgreements[0].id);
				lstAgreementLocations.add(agreementLocation);
			}
			/// Test insert Use Case
			insert lstAgreementLocations;
			expectedAccountNames = createExpectedAccountNames(lstAgreementLocations);
    		lstAgreementEval = [Select id, Salesforce_Account_Names__c From Order Limit 1];
    		System.assertEquals(lstAgreementEval[0].Salesforce_Account_Names__c, expectedAccountNames);
    		// Testing Update Use Case
    		lstAgreementLocations[0].Location_Salesforce_Account__c = lstTestAccounts[0].id;	
    		update lstAgreementLocations;
    		expectedAccountNames = createExpectedAccountNames(lstAgreementLocations);
    		lstAgreementEval = [Select id, Salesforce_Account_Names__c From Order Limit 1];
    		System.assertEquals(lstAgreementEval[0].Salesforce_Account_Names__c, expectedAccountNames);
    		// Testting Delete Use
    		List<Agreement_Location__c> lstAgreementLocationsEval = new List<Agreement_Location__c>();
    		delete lstAgreementLocations[0];
    		lstAgreementLocationsEval = [Select id, Agreement__c, Location_Salesforce_Account__c From Agreement_Location__c limit 2];
    		expectedAccountNames = createExpectedAccountNames(lstAgreementLocationsEval);
    		lstAgreementEval = [Select id, Salesforce_Account_Names__c From Order Limit 1];
    		System.assertEquals(lstAgreementEval[0].Salesforce_Account_Names__c, expectedAccountNames);
    		// Testing Undelete
    		undelete lstAgreementLocations[0];
    		lstAgreementLocationsEval = [Select id, Agreement__c, Location_Salesforce_Account__c From Agreement_Location__c limit 3];
    		expectedAccountNames = createExpectedAccountNames(lstAgreementLocationsEval);
    		lstAgreementEval = [Select id, Salesforce_Account_Names__c From Order Limit 1];
    		System.assertEquals(lstAgreementEval[0].Salesforce_Account_Names__c, expectedAccountNames);
    	Test.stopTest();
    }*/
    /**
     *  Method Name: createExpectedAccountNames
     *  Description:  method that creates a expected Account Name
     *  Param: List<Agreement_Location__c> lstAgreementLocations --> List of Agreement locations for which Account Names need to be need to combined
     *  Return: String
     */
  /*  private static String createExpectedAccountNames(List<Agreement_Location__c> lstAgreementLocations){
    	set<id> setAccountIds = new set<id>();
    	for(Agreement_Location__c agreementLocation: lstAgreementLocations){
    		setAccountIds.add(agreementLocation.Location_Salesforce_Account__c);
    	}
    	List<Account> lstTestAccounts = new List<Account>();
    	lstTestAccounts = [Select id, Name From Account where id in: setAccountIds order by Name asc Limit 3];
    	String expectedAccountNames ='';
    	// Constructing the Account Names in the Alphabetical order
    	for(Account account : lstTestAccounts){
    		expectedAccountNames += '\''+account.id + '\', ';
    	}
    	expectedAccountNames = expectedAccountNames.removeEnd(', ');
    	return expectedAccountNames;
    }*/
	
}