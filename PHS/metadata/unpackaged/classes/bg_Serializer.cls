/**********************************************************************
* bg_Serializer:
*
* Class to serialise an SObject
* Created By: SA
* Created Date: 22-03-2016
* Changes: 19-07-2016 JW: Updated to replace all instances of the Zero Width No-break space with nothing.
*          22-12-2016 AL: Updated to serialize CaseComments using CaseCommentWrapper
*          29-12-2016 KH: Merged with CPQ Project and added toJsonStr for bg_phsAccountInfoRequest
*                          and remove the update defined above from JW (no longer relevant)
*          18-01-2017 AL: Added task wrapper to serialiser in order to format dates
***********************************************************************/

public with sharing class bg_Serializer
{
    public static String toJsonStr(SObject so, String objType)
    {
        String jsonStr = '';
        System.debug('toJsonStr so: ' + so);
        System.debug('toJsonStr objType: ' + objType);
        
        try
        {
            if (objType == 'CaseComment')
            {
                System.debug('toJsonStr ccWrap: start');
                CaseCommentWrapper ccWrap = new CaseCommentWrapper(so, bg_Serializer.Users);
                System.debug('toJsonStr ccWrap: ' + ccWrap);
                jsonStr = JSON.serialize(ccWrap);
            }
            else if (objType == 'Task')
            {
                System.debug('toJsonStr taskWrapper: start');
                TaskWrapper tskWrapper = new TaskWrapper(so);
                System.debug('toJsonStr tskWrapper: ' + tskWrapper);
                jsonStr = JSON.serialize(tskWrapper.WrapperMap);
            } 
            else
            {
                System.debug('toJsonStr normal obj: ');
                jsonStr = JSON.serialize(so);
            }          
        }
        catch(Exception e)
        {
            System.debug('toJsonStr error: ' + e.getMessage());
        }

        System.debug('serialiser: so: ' + so);
        System.debug('serialiser: json: ' + jsonStr);

        return jsonStr;
    }   

    private static Map<id, User> userMap;
    public static Map<id, User> Users
    {
        get
        {
            if (userMap == null)
            {
                userMap = new Map<ID, User>([SELECT Id, Name, EmployeeNumber, UserRole.Name FROM User]);
            }
            return userMap;
        }
    }

    /*
        Convert the request class into JSON, this needs to be moved to bg_Searilizer
    */
    public static String toJsonStr(bg_phsAccountInfoRequest rD)    
    {
        String jsonStr = null;

        try 
        {
            jsonStr = JSON.serialize(rD);
        }
        catch(Exception e)
        {
            System.debug('**toJsonStr error**: ' + e.getMessage());   
        }
        return jsonStr;
    }
}