/**
 *  @Class Name:    LeadReallocationControllerTest
 *  @Description:   This is a test class for LeadReallocationController
 *  @Company:       dQuotient
 *  CreatedDate:    30/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           30/10/2017                  Original Version
 */
@isTest
private class LeadReallocationControllerTest {
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
              
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        List <Profile> OutboundTSProfiles = new List <Profile>([SELECT ID, Name FROM PROFILE WHERE Name = 'Telesales' LIMIT 1]);
       
        List <Profile> Salesrepprofiles = new List <Profile>([SELECT ID, Name FROM PROFILE WHERE Name = 'Field Sales' LIMIT 1]);
        
        User OutboundAgent = bg_Test_Data_Utils.createUserWithEmployeeNumber('2');

        OutboundAgent.ProfileId = OutboundTSProfiles[0].id;
        OutboundAgent.Alias = 'OBAgent';
        insert OutboundAgent;
        
        User OutboundTeamLeader = bg_Test_Data_Utils.createUserWithEmployeeNumber('3');
        OutboundTeamLeader.ProfileId = OutboundTSProfiles[0].id;
        OutboundTeamLeader.Alias = 'OBTL';
        insert OutboundTeamLeader;
        
        User SalesRep = bg_Test_Data_Utils.createUserWithEmployeeNumber('4');
        SalesRep.ProfileId = Salesrepprofiles[0].id;
        SalesRep.Alias = 'Salesrep';
        insert SalesRep; 
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        lead.FLT_Postal_Code__c =flt.id;
        insert lead;
    }
    
    // Test reallocateLead method in LeadReallocationController
    private static testMethod void testreallocateLead() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        User OutboundAgent = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBAgent'];
        User OutboundTeamLeader = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBTL'];
        User Salesrep = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'SalesRep'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        system.debug('testLead--->' +testLead);
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.SalesRSM__c = SalesRep.id;
        flt.OutboundTSAgent__c = OutboundAgent.id;
        flt.OutboundTSTeamLeader__c = OutboundTeamLeader.id;
        testLead.FLT_Postal_Code__c =flt.id;
        update flt;
        update testLead;
        system.debug('flt--->' +flt);

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.id);
        }
        test.stopTest();
	}
	
	// Test reallocateOutboundLead method in LeadReallocationController
    private static testMethod void testreallocateOutboundLead() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
		User OutboundAgent = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBAgent'];
        User OutboundTeamLeader = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBTL'];
        User Salesrep = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'SalesRep'];
        system.debug('user---->' +u);
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        system.debug('testLead--->' +testLead);
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = Salesrep.id;
        flt.SalesRSM__c = Salesrep.id;
        flt.OutboundTSAgent__c = OutboundAgent.id;
        flt.OutboundTSTeamLeader__c = OutboundTeamLeader.id;
        testLead.FLT_Postal_Code__c =flt.id;
        update flt;
        update testLead;
        system.debug('flt--->' +flt);

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.id);
        }
        test.stopTest();
	}
	
	// Test transfer methods when Front_Line_Team__c is null for a lead
	private static testMethod void testleadnullFLT() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        system.debug('user---->' +u);
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        system.debug('flt--->' +flt);
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}
	
	// Test transfer methods when postalcode is null
	/*private static testMethod void testleadnullPostalcode() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        system.debug('user---->' +u);
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        system.debug('flt--->' +flt);
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        testLead.postalcode =null;
        update testLead;

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}*/

    // Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are null and Off_Sick_Annual_Leave__c is false;
	private static testMethod void testleadnull() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        testLead.FLT_Postal_Code__c = flt.id;
        update testLead;
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}

    // Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are null and Off_Sick_Annual_Leave__c is false;
	private static testMethod void testleadnullfalse() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        testLead.FLT_Postal_Code__c = flt.id;
        update testLead;
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c not null , Off_Sick_Annual_Leave__c is false and SalesRSM__c, OutboundTSTeamLeader__c not null;
	private static testMethod void testleadnullSales() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        User OutboundAgent = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBAgent'];
        User OutboundTeamLeader = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBTL'];
        User Salesrep = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'SalesRep'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = Salesrep.id;
        flt.SalesRSM__c = Salesrep.id;
        flt.OutboundTSAgent__c = OutboundAgent.id;
        flt.OutboundTSTeamLeader__c = OutboundTeamLeader.id;
        update flt;
        testLead.FLT_Postal_Code__c = flt.id;
        update testLead;
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c are null , Off_Sick_Annual_Leave__c is false and SalesRSM__c, OutboundTSTeamLeader__c not null;
	private static testMethod void testleadnullcheckFLTfields() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        User OutboundAgent = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBAgent'];
        User OutboundTeamLeader = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBTL'];
        User Salesrep = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'SalesRep'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.SalesRSM__c = Salesrep.id;
        flt.OutboundTSTeamLeader__c = OutboundTeamLeader.id;
        update flt;
        testLead.FLT_Postal_Code__c = flt.id;
        update testLead;
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c not null , Off_Sick_Annual_Leave__c is true and SalesRSM__c, OutboundTSTeamLeader__c are null;
	private static testMethod void testleadcheckFLTfieldsnull() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        User OutboundAgent = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBAgent'];
        User OutboundTeamLeader = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'OBTL'];
        User Salesrep = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'SalesRep'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = Salesrep.id;
        flt.OutboundTSAgent__c = OutboundAgent.id;
        update flt;
        testLead.FLT_Postal_Code__c = flt.id;
        update testLead;
        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateLead(testLead.Id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundLead')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            LeadReallocationController.reallocateOutboundLead(testLead.Id);
        }
        test.stopTest();
	}
}