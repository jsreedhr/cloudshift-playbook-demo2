@isTest
private class RedirectToAccessTimesPageTest
{

	private static testMethod void updateAgreementLocationTest() {
	    
	    Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        
        Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true );
        update standardPricebook;
        
        Order orderObj = bg_Test_Data_Utils.createAgreement(acc.Id, opp.Id, system.today(), 'draft');
        orderObj.Pricebook2Id = standardPricebook.Id;
        insert orderObj;
        
        Agreement_Location__c agreementLocationObj = bg_Test_Data_Utils.createAgreementLocation(acc.id, orderObj.id);
        insert agreementLocationObj;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(agreementLocationObj);
        RedirectToAccessTimesPage testRedirect = new RedirectToAccessTimesPage(sc);
        testRedirect.updateAgreementLocation();
        

	}

}