/**********************************************************************
* bg_PopulateCaseEntitlementDate_Tests:
*
* Tests for the Entitlement population on the Case.  
* Created By: BrightGen Ltd
* Created Date: 05/04/2016
*
* Changes: 
***********************************************************************/

@isTest 
public class bg_PopulateCaseEntitlementDate_Tests{ 
	public static Id setup() { 

	    Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
	    
	    Account parentAccountRecord = (Account)Account.sObjectType.newSObject(null, true);  
	    parentAccountRecord.Name='Entitlement Parent Account Two';
	    parentAccountRecord.BillingPostalcode = 'AB1 1AA'; 
	    insert parentAccountRecord;  
	     
	    Account accountRecord = (Account)Account.sObjectType.newSObject(null, true); 
	    accountRecord.Name='Entitlement Account Two'; 
	    accountRecord.ParentId = parentAccountRecord.Id; 
	    accountRecord.BillingPostalcode = 'AB1 1AA'; 
	    insert accountRecord;  
	     
	    Contact contactRecord = (Contact)Contact.sObjectType.newSObject(null, true); 
	    contactRecord.Lastname='LastName'; 
	    contactRecord.AccountId = accountRecord.Id; 
	    insert contactRecord;  
	     
	    /* 
	     * TODO - Move RAM in to custom setting 
	     */ 
	     
	    Entitlement entitlementRecord = new Entitlement(Name = 'Test Entitlement', AccountId = accountRecord.Id, StartDate = date.today(), EndDate = date.today() + 12); 
	    insert entitlementRecord; 
	     
	    return accountRecord.Id; 
	}

 	static testMethod void entitlementEndDateTest() {

	    Id AccountId = setup(); 
	    Test.startTest();     
	     
	    Case caseRecord = (Case)Case.sObjectType.newSObject(null, true); 
	    caseRecord.Priority = '1'; 
	    caseRecord.Origin = 'CSEvents'; 
	    caseRecord.Type = 'Enquiry'; 
	    caseRecord.RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id;
	    caseRecord.AccountId = AccountId;

	    insert caseRecord; 
	     
	    caseRecord = [Select Id, Entitlement.Id From Case Where Id = :caseRecord.Id limit 1];
	    Entitlement ent = [Select Id, StartDate, EndDate From Entitlement Where Id = :caseRecord.EntitlementId];

	    Test.stopTest(); 
	     
	    caseRecord = [Select Id, First_Response_Target_Date__c, Case_Resolution_Target_Date__c From Case WHERE Id = :caseRecord.Id limit 1]; 
	     
	     
	    System.assertNotEquals(null, caseRecord.Case_Resolution_Target_Date__c); 
  	} 
   
    static testMethod void entitlementUpdateTest() {

	    Id AccountId = setup(); 
	    Case caseRecord = (Case)Case.sObjectType.newSObject(null, true); 
	    caseRecord.Priority = '1'; 
	    caseRecord.Origin = 'CSEvents'; 
	    caseRecord.Type = 'Enquiry'; 
	    caseRecord.AccountId = AccountId;
	    caseRecord.RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id; 
	    insert caseRecord; 
	     
	    Test.startTest();   
	    update caseRecord; 
	    Test.stopTest(); 
	     
	    caseRecord = [Select Id, First_Response_Target_Date__c, Case_Resolution_Target_Date__c From Case WHERE Id = :caseRecord.Id limit 1]; 
	     
	    System.assertNotEquals(null, caseRecord.Case_Resolution_Target_Date__c); 
    } 
   
}