/**
 *  @Class Name:    CampaignTriggerHandlerTest
 *  @Description:   This is a test class for CampaignTriggerHandler
 *  @Company:       dQuotient
 *  CreatedDate:    07/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           07/11/2017                  Original Version
 */
@isTest
private class CampaignTriggerHandlerTest {

    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        insert lead;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        CampaignMember campMem = bg_Test_Data_Utils.createCampaignMember(lead.id, camp.id);
        insert campMem;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
    }
    
	// Test updateLeadCampaignsField method in CampaignTriggerHandler
    private static testMethod void testupdateLeadCampaigns() {

        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead lead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c from Lead where OwnerId= :u.Id];
        Campaign camp = [Select Id, Name, IsActive From Campaign Where Name = 'Test Campaign'];

        test.startTest();
            camp.IsActive = true;
            update camp;
            system.assertNotEquals(lead.Campaigns__c, null);
        test.stopTest();

	}
	
	// Test updateLeadCampaignsFieldDelete method in CampaignTriggerHandler
	private static testMethod void testupdateLeadCampaignsDelete() {

        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c from Lead where OwnerId= :u.Id];
        Campaign camp = [Select Id, Name From Campaign Where Name = 'Test Campaign'];
        
        test.startTest();
            delete camp;
            System.assertEquals(0,[Select Id From Campaign].size());
        test.stopTest();
	}
	
	// Test updateOpportunityCampaignsField method in CampaignTriggerHandler
	private static testMethod void testupdateOpportunityCampaigns() {

        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign C');
        insert camp; 
        Account acc = [Select Name From Account Where Name = 'Test Account N'];
        Opportunity opp = [Select Name, StageName, AccountId, Type, OwnerId, CampaignId, Campaigns__c From Opportunity Where AccountId = :acc.id];
        opp.CampaignId = camp.Id;
        update opp;
        
        test.startTest();
            camp.IsActive = true;
            update camp;
            system.assertEquals(camp.IsActive, true);
        test.stopTest();
	}
	
	// Test updateOpportunityCampaignsFieldDelete method in CampaignTriggerHandler
	private static testMethod void testupdateOpportunityCampaignsDelete() {

        Campaign camp = [Select Id, Name From Campaign Where Name = 'Test Campaign'];
        Account acc = [Select Name From Account Where Name = 'Test Account N'];
        Opportunity opp = [Select Name, StageName, AccountId, Type, OwnerId,CampaignId, Campaigns__c From Opportunity Where AccountId = :acc.id];
        opp.CampaignId = camp.Id;
        update opp;
        
        test.startTest();
            delete camp;
            system.assertEquals(opp.Campaigns__c, null);
        test.stopTest();
	}
}