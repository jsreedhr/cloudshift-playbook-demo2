global class bg_ContractLineCallout
{
    // /*
    //     Method to query existing data from the Wildebeest System
    //     // Test PHS Account References: 288, 13015, 4353727, 129527
    // */
    // static public bg_WildebeestCallout.phsAccountInfoResponse getExistingContractLines(ID accountID, Set<String> phsAccountReferences, Boolean contractLines)
    // {
    //     try 
    //     {
    //         Boolean calloutsLessThanLimit = Limits.getCallouts() <= Limits.getLimitCallouts();

    //         if (bg_WildebeestCallout.settingAreValid(null) & calloutsLessThanLimit)
    //         {   
    //             String token = bg_WildebeestCallout.login();
    //             Boolean loginHasWorked = token != null;

    //             System.debug('**** token' + token);
    //             System.debug('**** loginHasWorked' + loginHasWorked);

    //             if (loginHasWorked)
    //             {
    //                 bg_WildebeestCallout.phsAccountInfoRequest newRequest = new bg_WildebeestCallout.phsAccountInfoRequest();
    //                 newRequest.PHSAccountIDS = phsAccountReferences;
    //                 newRequest.ContractLines = contractLines;
    //                 newRequest.SaleOrderLines = false;
    //                 newRequest.Visits = false;
    //                 newRequest.DebtProfile = false;

    //                 System.debug('**** bg_Serializer.toJsonStr(newRequest)' + bg_Serializer.toJsonStr(newRequest));

    //                 String response = bg_WildebeestCallout.send('CustomerDetails', null, bg_Serializer.toJsonStr(newRequest), token, accountID);
    //                 system.debug('**response**: ' + response);
    //                 bg_WildebeestCallout.phsAccountInfoResponse phsAccountInfo = bg_WildebeestCallout.parsePHSAccountInfo(response);

    //                 return phsAccountInfo;
    //             }
    //         }
    //     }
    //     catch (Exception e)
    //     {
    //         throw e;
    //     }
    //     return null;
    // }
}