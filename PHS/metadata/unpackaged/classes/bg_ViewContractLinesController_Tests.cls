@isTest
private class bg_ViewContractLinesController_Tests {
    static testMethod void testMethod1() {

    	// Create Account
    	Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	    insert acct;

	    PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', acct.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;

	    ApexPages.StandardController sc = new ApexPages.StandardController(acct);

        bg_ViewContractLineController controller = new bg_ViewContractLineController(sc);

        Integer phsAccountCount = controller.getPHSAccountCount();

        System.assertEquals(1, phsAccountCount);
    }
}