/**********************************************************************
* bg_CaseCommentsWrapper_Tests:
*
* Test class for the CaseCommentsWrapper class
*
* Created By: BrightGen Ltd (AL)
* Created Date: 18/01/2016
*
* Changes: 
***********************************************************************/

@isTest
private class bg_CaseCommentsWrapper_Tests {
    static testMethod void testCaseCommentsParsed() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;

    	Account acc = bg_Test_Data_Utils.createAccount('brightGen');
    	insert acc;

    	Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
    	insert cont;

    	Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
    	insert theCase;

    	CaseComment caseComms = bg_Test_Data_Utils.createCaseComment('Hello! how are you?', theCase.Id);
        insert caseComms;

        test.startTest();
        caseComms = [SELECT ParentId, LastModifiedDate, CommentBody, CreatedDate, Id, CreatedById, LastModifiedById
                        FROM CaseComment
        				WHERE Id =: caseComms.Id];

        CaseCommentWrapper wrapper = new CaseCommentWrapper((SObject)caseComms, bg_Serializer.Users);
        test.stopTest();

        system.debug('*** serialized: **** : ' + bg_Serializer.toJsonStr(caseComms, 'CaseComment'));

        system.assertEquals('Hello! how are you?', wrapper.CommentBody);
    }

    static testMethod void testCaseDateCorrectFormat() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;
    	
    	Account acc = bg_Test_Data_Utils.createAccount('brightGen');
    	insert acc;

    	Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
    	insert cont;

    	Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
    	insert theCase;

    	CaseComment caseComms = bg_Test_Data_Utils.createCaseComment('Hello! how are you?', theCase.Id);
        insert caseComms;

        caseComms.CommentBody = 'New comment body';
        update caseComms;

        test.startTest();
        caseComms = [SELECT ParentId, LastModifiedDate, CommentBody, CreatedDate, Id, CreatedById, LastModifiedById
                     FROM CaseComment
        			 WHERE Id =: caseComms.Id];

        CaseCommentWrapper wrapper = new CaseCommentWrapper((SObject)caseComms, bg_Serializer.Users);

        test.stopTest();

        system.assertEquals('New comment body', wrapper.CommentBody);

        //check the date format is correct
        Pattern pat = Pattern.compile('([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})');
        Matcher match = pat.Matcher(wrapper.CreatedDate);
        system.assert(match.matches(), 'Created date format does not match');

        match = pat.Matcher(wrapper.LastModifiedDate);
        system.assert(match.matches(), 'Modified date format does not match');
    }

    static testMethod void testNoneCaseCommentsIgnored() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;
    	
        Case theCase = new Case();
        
       	CaseCommentWrapper wrapper = new CaseCommentWrapper((SObject)theCase, bg_Serializer.Users);

       	System.assertEquals(null, wrapper.Id);
    }

    static testMethod void bulkTestBgSerializer(){
        Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
        insert integrationSettings;
        
        Account acc = bg_Test_Data_Utils.createAccount('brightGen');
        insert acc;

        Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
        insert cont;

        Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
        insert theCase;

        List<CaseComment> caseComms = new List<CaseComment>();
        

        for (Integer i=0; i < 250; i++){
            CaseComment caseComm = bg_Test_Data_Utils.createCaseComment('Hello! how are you?' + i, theCase.Id);
            caseComms.add(caseComm);
        }

        insert caseComms;


        for(CaseComment caseComm : [SELECT ParentId, LastModifiedDate, LastModifiedById, CreatedById, CommentBody, CreatedDate, Id
                                        FROM CaseComment]) 
        {
            System.assert(String.isNotBlank(bg_Serializer.toJsonStr(caseComm, 'CaseComment')), 'bg_Serializer.toJsonStr returned a blank string');
        }
    }
}