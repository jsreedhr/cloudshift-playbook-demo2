/*
*  Class Name:  AccessTimesAtLocationController
*  Description: This controller class shows the access times for a given location
*  Company: CloudShift
*  CreatedDate: 16.7.2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer          Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  David Kurtanjek    16-Jul-2018            Original Version
*/
public with sharing class AccessTimesAtLocationController{
    public Id AgreementLocationId;
    public Boolean showAccessTimesComponent{get;set;}
    public List <Access_Time__c> AccessTimesList{get;set;}
    public String errorMessage{get;set;}



    Agreement_Location__c AgreementLocation;
    
	List<Account> accountList;

    public AccessTimesAtLocationController(){
        showAccessTimesComponent = FALSE;
        errorMessage = '';
        AgreementLocation = new Agreement_Location__c();
        AccessTimesList = new List<Access_Time__c>();
        accountList = new List<Account>();

    }
    public void setAgreementLocationId(Id id){
        AgreementLocationId = id;

    }
    public Id getAgreementLocationId(){
        try{
            AgreementLocation = [Select Id, Location_Salesforce_Account__c From Agreement_Location__c Where Id =:AgreementLocationid limit 1];
        }catch(Exception ex){
        }
		if(String.isEmpty(AgreementLocation.Location_Salesforce_Account__c)){

            Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Please specify an account in the Location Salesforce Account field to see any existing access times.'));
        }
        else
                getAccessTimesForAccount();
            return AgreementLocationId;
        }

    public void getAccessTimesForAccount(){

        String SalesforceAccount = String.valueOf(AgreementLocation.Location_Salesforce_Account__c);
        try{
            accountList = [Select Id From Account Where Id = :SalesforceAccount];

            AccessTimesList = [SELECT ID, Account__c, Account__r.Name, From__c, To__c, Day__c, Division__c, Comments__c, CustomSortOrder__c, CreatedDate
                From Access_Time__c Where Account__c IN : accountList ORDER BY CustomSortOrder__c ASC];
            if(AccessTimesList!=NULL && AccessTimesList.size()>0)
                showAccessTimesComponent = TRUE;
            else{
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Access Times Exist for this Location'));
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Access Times Exist for this Location'));
        }

    }
}