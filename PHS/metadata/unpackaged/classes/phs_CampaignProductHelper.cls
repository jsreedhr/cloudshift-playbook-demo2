public with sharing class phs_CampaignProductHelper { 
    
    
    public static void createLeadProducts(List<Campaign_Product__c> campaignProducts) 
    { 
        // List<String> leadIds = new List<String>(); 
        
        // for (Campaign_Product__c campaignProd: campaignProducts){ 
            
        //     IF(campaignProd.Add_to_Existing_Open_Leads__c == TRUE){
        //         String campaignId = campaignProd.Campaign__c;
                
                
        //         //Get Product with Id 
        //         Product2 product = [SELECT Id, Name
        //                             FROM Product2 
        //                             WHERE Id = :campaignProd.Product__c]; 
                
        //         List<CampaignMember> campaignMembers = [SELECT Id, LeadId 
        //                                                 FROM CampaignMember 
        //                                                 WHERE campaignId = :campaignId]; 
                
        //         if (campaignMembers.size() >= 0) { 
        //             List<Lead_Product__c> leadListInsert = new List<Lead_Product__c>();
        //             for (CampaignMember myCampaignMember : campaignMembers) {        
        //                 leadIds.add(myCampaignMember.LeadId); 
        //             } 
                    
        //             for (String myLeadId : leadIds) { 
        //                 Lead_Product__c leadProduct = new Lead_Product__c(); 
        //                 leadProduct.Lead__c = myLeadId; 
        //                 leadProduct.Product__c = product.Id; 
        //                 leadProduct.Annual_Cost__c = campaignProd.Annual_Cost__c;
        //                 leadProduct.High_Price__c = campaignProd.High_Price__c;
        //                 leadProduct.Floor_Price__c = campaignProd.Floor_Price__c; 
        //                 leadProduct.Service_Frequency__c = campaignProd.Service_Frequency__c; 
        //                 leadProduct.Trade_Price__c = campaignProd.Trade_Price__c; 
        //                 leadProduct.Weekly_Cost__c = campaignProd.Weekly_Cost__c;
        //                 leadProduct.Campaign__c = campaignProd.Campaign_Name__c;
        //                 leadListInsert.add(leadProduct);
                        
                        
        //             } 
        //             if(!leadListInsert.isEmpty()){
        //                 try{
        //                     insert leadListInsert;
        //                 }catch(Exception e){
                            
        //                 }
                         
        //             }
        //         } 
        //     }
        // }
        
        List<String> leadIds = new List<String>(); 
        set<id> setcampaignids = new set<Id>();
        for (Campaign_Product__c campaignProd: campaignProducts)
        { 
            if(campaignProd.Add_to_Existing_Open_Leads__c == TRUE && campaignProd.Campaign__c != null)
            {
                setcampaignids.add(campaignProd.Campaign__c);
            }
        }
        
        map<Id, List<CampaignMember>> mapCampaignIdToMembers = new map<Id, List<CampaignMember>>();
        for(CampaignMember objCampaingMember:  [SELECT Id, LeadId, campaignId
                                                        FROM CampaignMember 
                                                        WHERE campaignId IN :setcampaignids])
        {
            if(!mapCampaignIdToMembers.containskey(objCampaingMember.campaignId))
                mapCampaignIdToMembers.put(objCampaingMember.campaignId, new List<CampaignMember>());
            mapCampaignIdToMembers.get(objCampaingMember.campaignId).add(objCampaingMember);
        }
        List<Lead_Product__c> leadListInsert = new List<Lead_Product__c>();
        for (Campaign_Product__c campaignProd: campaignProducts)
        {
            IF(campaignProd.Add_to_Existing_Open_Leads__c == TRUE)
            {
                String campaignId = campaignProd.Campaign__c;
                if (mapCampaignIdToMembers.containskey(campaignId) && mapCampaignIdToMembers.get(campaignId).size() >= 0) 
                { 
                    for (CampaignMember myCampaignMember : mapCampaignIdToMembers.get(campaignId)) 
                    { 
                        Lead_Product__c leadProduct = new Lead_Product__c(); 
                        leadProduct.Lead__c = myCampaignMember.LeadId; 
                        leadProduct.Product__c = campaignProd.Product__c; 
                        leadProduct.Annual_Cost__c = campaignProd.Annual_Cost__c;
                        leadProduct.High_Price__c = campaignProd.High_Price__c;
                        leadProduct.Floor_Price__c = campaignProd.Floor_Price__c; 
                        leadProduct.Service_Frequency__c = campaignProd.Service_Frequency__c; 
                        leadProduct.Trade_Price__c = campaignProd.Trade_Price__c; 
                        leadProduct.Weekly_Cost__c = campaignProd.Weekly_Cost__c;
                        leadProduct.Campaign__c = campaignProd.Campaign_Name__c;
                        leadListInsert.add(leadProduct);
                    } 
                } 
            }
        }
        try{
            insert leadListInsert;
        }
        catch(Exception e){
        }
    } 
}