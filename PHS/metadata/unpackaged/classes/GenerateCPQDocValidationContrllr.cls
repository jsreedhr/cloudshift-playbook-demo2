public class GenerateCPQDocValidationContrllr {
   //public  String quoteId;
      public string quoteId {
        get;
        set;
    }
    public SBQQ__Quote__c quote {
        get;
        set;
    }
     public String errorMsg {
        get;
        set;
    }
     public Boolean redirectToLead {
        get;
        set;
    }
 public GenerateCPQDocValidationContrllr(ApexPages.StandardController stdController) {
     
      quoteId = ApexPages.currentPage().getParameters().get('id');
      if (quoteId != null){
            quote = [select id, RSM_Approval_Required__c, RSM_Approved__c, Management_Approval_Required__c, Management_Approved__c from SBQQ__Quote__c where id =: quoteId Limit 1];
            system.debug('--quote--'+quote);
         }
 }
    
   
   
   public void loadAction() {
            errorMsg = '';
            redirectToLead = false;
        if ((quote.RSM_Approval_Required__c == true && quote.RSM_Approved__c == false) || (quote.Management_Approval_Required__c== true && quote.Management_Approved__c== false)){
            errorMsg = 'The quote requires approval before being able to generate the document.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
        }
        else
        {
             redirectToLead = true;
        }
       
   }
}