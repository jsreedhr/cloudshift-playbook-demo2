/*********************************************************************************
 * bg_caseController
 *
 * Controller class for case. Primarily used when 'Remittance' or 'Credit Management'
 * button is clicked on case   
 * 
 * Author: Ismail Basser - BrightGen Ltd
 * Created: 05-02-2018
 * 
 * Edited: 26-02-2018 -Ismail Basser - BrightGen Ltd
 * Added new method 'CreditManagementClose'
 *********************************************************************************/

public class bg_caseController {


	public Id emailId {get; set;}
    public Id caseId { get; set; }
    public Case cas { get; set; }
    public boolean close { get; set; }

    //constructor to get the Case record
    public bg_caseController(ApexPages.StandardController controller) {
    cas =  (Case) controller.getRecord();
    caseId = cas.Id;
    System.debug('The case record: ' + cas);
    emailId = cas.Email_ID__c;
    close = cas.To_Close__c;
    }

    //Method that can is called from the Visual Force page action attribute
    //updates case by checking the checkbox and then goes to send email page
    //if there's an error then in will show on the page
    public PageReference caseToClose() {
        
        try{
		Case casetoUp = [SELECT Id, To_Close__c FROM Case WHERE Id =:caseId Limit 1];
    	casetoUp.To_Close__c = true;

       update casetoUp;
       String blankVal = '';
        PageReference pageRef = new PageReference( '/_ui/core/email/author/EmailAuthor?email_id='+emailId+'&forward=1&p24=remittance@phs.co.uk&p5='+blankVal+'&retURL='+caseId);
        pageRef.setRedirect(true);
            System.debug('***BGIsmail'+pageRef);
            return pageRef; //Returns to the case page
       	} catch(DmlException e) {
          	ApexPages.addMessages(e);
         //   PageReference pageRef = ApexPages.currentPage();
        //	pageRef.setRedirect(true);
        	return null;

    	}
	}
    
    public PageReference CreditManagementClose() {
        
        try{
		Case casetoUp = [SELECT Id, To_Close__c FROM Case WHERE Id =:caseId Limit 1];
    	casetoUp.To_Close__c = true;

       update casetoUp;
       String blankVal = '';
        PageReference pageRef = new PageReference( '/_ui/core/email/author/EmailAuthor?email_id='+emailId+'&forward=1&p24=creditmanagement@phs.co.uk&p5='+blankVal+'&retURL='+caseId);
        pageRef.setRedirect(true);
            System.debug('***BGIsmail2'+pageRef);
            return pageRef; //Returns to the case page
       	} catch(DmlException e) {
          	ApexPages.addMessages(e);
         //   PageReference pageRef = ApexPages.currentPage();
        //	pageRef.setRedirect(true);
        	return null;

    	}
	}
    
    //If there's an error then the VF page will show the error
    //The back button will take the user back to the case
    public PageReference back() {
        
        PageReference pageRef = new PageReference( '/'+caseId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}