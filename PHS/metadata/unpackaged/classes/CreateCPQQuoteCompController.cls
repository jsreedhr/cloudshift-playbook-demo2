public class CreateCPQQuoteCompController {

    
    @AuraEnabled
    public static Opportunity getOpp(string oppId)
        
    {
        System.debug('oppId'+oppId);
        Opportunity opp = [SELECT CloseDate, id,name, Product_Count__c, account.BillingCity, account.BillingCountry, account.BillingPostalCode, Account.name, accountid, account.BillingState, Pricebook2Id,Pricebook_Id__c,Pricebook_Name__c, Pricebook2.name FROM Opportunity WHERE id =: oppId ];
        opp.CloseDate=opp.CloseDate.addDays(-30);
        System.debug('opprec'+opp);
        return opp;
    }   
    
    @AuraEnabled
    public static string getOppID(string url)
        
    {  
        System.debug('url==='+url);
       string tem = url.substringAfter('sObject/').substringBefore('/view');
       return tem;
    }   
    
        @AuraEnabled
    public static Account getAcc(string oppId)
        
    {
      
       Opportunity opp = [SELECT accountid FROM Opportunity WHERE id =: oppId ];
       Account acc = [select  id, BillingCity, BillingStreet,BillingCountry, BillingPostalCode, name,  BillingState  FROM Account WHERE id =: opp.accountid  limit 	1];
       if(String.isNotBlank(acc.BillingCity))
        acc.BillingCity = string.escapeSingleQuotes(acc.BillingCity);
        if(String.isNotBlank(acc.BillingStreet))
        acc.Billingstreet = string.escapeSingleQuotes(acc.Billingstreet);
         if(String.isNotBlank(acc.BillingPostalCode))
        acc.BillingPostalCode = string.escapeSingleQuotes(acc.BillingPostalCode);
        
        
         System.debug('accrec'+acc.Billingstreet);
        return acc;
    }   
    
    
     @AuraEnabled
    public static string getUser()
        
    {
         string loggedinUser = UserInfo.getUserId();
             return loggedinUser;
    }   

 @AuraEnabled
    public static Id getContact(Id oppId)
        
    {
       OpportunityContactRole primaryContact = [select ContactId from OpportunityContactRole where IsPrimary = true and opportunityid =: oppId limit 1];
        return primaryContact.ContactId;
           
    }       
      
    
    
    
    
}