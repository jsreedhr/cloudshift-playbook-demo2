/*
    Class to define the structure of the JSON Request to be sent to Wildebeest for Additional Information
    for the the PHS Accounts
*/
public class bg_phsAccountInfoRequest
{
    public Set<String> PHSAccountIDS;
    public Boolean ContractLines;
    public Boolean SaleOrderLines;
    public Boolean Visits;
    public Boolean DebtProfile;
    public Integer ContractLineLimit;
}