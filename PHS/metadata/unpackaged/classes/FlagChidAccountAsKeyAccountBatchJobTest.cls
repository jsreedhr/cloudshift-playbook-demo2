@isTest
public class FlagChidAccountAsKeyAccountBatchJobTest {
    static List<Account> accountList;
    static List<PHS_Account__c> phsAccountList;
    static List<PHS_Account_Relationship__c> phsList;
    @testsetup
    static void createData(){
		//Insert Accounts
		accountList = new List<Account>();
        //adding 5 + 7 records, to create multi level parent-child relationship
		 
		for(Integer i=0;i<4;i++){
			Account account = bg_Test_Data_Utils.createAccount(''+i);
			accountList.add(account);
		}
		system.debug('AccountList____before---->' +accountList);
        //HasNegotiatorResponsibility__c should be FALSE for child accounts
        for(Integer i=5;i<12;i++){
            Account account = bg_Test_Data_Utils.createAccount(''+i);
            account.Key_Account__c = FALSE;
			account.HasNegotiatorResponsibility__c = FALSE;
			accountList.add(account);
        }
        system.debug('AccountList____after---->' +accountList);
		insert accountList;

		//Insert PHS_Account__c records
		phsAccountList = new List<PHS_Account__c>();
		for(Account account:accountList){
			PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('swq1', account.Id); 
			phsAccountList.add(phsAccount);
		}
		insert phsAccountList;

		//Insert PHS_Account_Relationship__c records
		phsList = new List<PHS_Account_Relationship__c>();
		//size of phsList to be half of accountList size
		for(Integer i=0;i<5;i++){
			PHS_Account_Relationship__c phs = bg_Test_Data_Utils.createcreatePHSAccountRelation(accountList[i].Id, accountList[i+5].Id, phsAccountList[i].Id, phsAccountList[i+5].Id);
			phsList.add(phs);
		}
        //creating two additional records to create multi level parent-child relationship
        //(account[0] -> account[5] -> account[10], account[1] -> account[6] -> account[11])
        for(Integer i=5;i<7;i++){
			PHS_Account_Relationship__c phs = bg_Test_Data_Utils.createcreatePHSAccountRelation(accountList[i].Id, accountList[i+4].Id, phsAccountList[i].Id, phsAccountList[i+4].Id);
			phsList.add(phs);
        }
		insert phsList;
	}
    static testMethod void testMethod1()
    {
        //  createData();
        Test.startTest();
        FlagChidAccountAsKeyAccountBatchJob obj = new FlagChidAccountAsKeyAccountBatchJob();
        DataBase.executeBatch(obj);
        Test.stopTest();

        Account account = [Select Key_Account__c, HasNegotiatorResponsibility__c From Account Where Name = 'Test Account 11' Limit 1];
        System.assertEquals(TRUE, account.Key_Account__c);
        System.assertEquals(FALSE, account.HasNegotiatorResponsibility__c);
    }
}