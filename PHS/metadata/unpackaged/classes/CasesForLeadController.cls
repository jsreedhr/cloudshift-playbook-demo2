/*
*  Class Name:  CasesForLeadController
*  Description: This controller class performs the logic and passes the Case records to be displayed on CasesForLead Visulaforce component
*  Company: dQuotient
*  CreatedDate: 07-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer            Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      07-Nov-2017            Orginal Version
*/
public with sharing class CasesForLeadController{
    public Id leadId;
    public Boolean showCaseComponent{get;set;}
    public List<Case> caseList{get;set;}
    public String errorMessage{get;set;}



    Lead lead;
    List<Account> accountList;

    public CasesForLeadController(){
        showCaseComponent = FALSE;
        errorMessage = '';
        lead = new Lead();
        caseList = new List<Case>();
        accountList = new List<Account>();

    }
    public void setLeadId(Id id){
        leadId = id;

    }
    public Id getLeadId(){
        try{
            lead = [Select Id, Wildebeest_Ref__c From Lead Where Id =:leadId limit 1];
        }catch(Exception ex){
            //casesForLead_Error_Message1 = Error on getting Lead record data
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.casesForLead_Error_Message1));
        }
        if(lead == NULL || lead.Id == NULL){
            // casesForLead_Error_Message2 = Error on getting Lead record data
            Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, Label.casesForLead_Error_Message2));
        }
        else if(String.isEmpty(lead.Wildebeest_Ref__c)){
            // casesForLead_Error_Message3 = Blank wildebeest reference field
            Apexpages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, Label.casesForLead_Error_Message3));
        }
        else
            getCasesForLead();
        return leadId;
    }

    public void getCasesForLead(){

        String wildebeestRef = String.valueOf(lead.Wildebeest_Ref__c);
        wildebeestRef = wildebeestRef.substringBefore(',');
        try{
            accountList = [Select Id From Account Where Wildebeest_Ref__c = :wildebeestRef OR Wildebeest_Ref__c LIKE :wildebeestRef+',%'
                            OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef+',%'];
            //now we have set of all accountIds which are directly or indirectly related to the particular lead (using Wildebeest_Ref__c values)
            caseList = [Select Id, isClosed, Account.Name, CaseNumber, Type, Case_Reason__c, Subject, Status, CreatedDate, PHS_Wildebeest_Ref__c, Owner.Name, Contact.Name, SuppliedEmail
                From Case Where AccountId IN : accountList AND isClosed = FALSE ORDER BY createdDate DESC];
            if(caseList!=NULL && caseList.size()>0)
                showCaseComponent = TRUE;
            else{
              // casesForLead_Error_Message4 = No Open Cases found for this Lead
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.casesForLead_Error_Message4));
            }
        }catch(Exception ex){
            // casesForLead_Error_Message5 = Incomplete info, No records found

            System.debug('Error in getCasesForLead method : ' + ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.casesForLead_Error_Message5));
        }

    }
}