/**
 *  @Class Name:    AccessTimeTriggerTest
 *  @Description:   This is a test class for sorting Access Time records on the related lists tab on Orders
 *  @Company:       dQuotient
 *  CreatedDate:    26/03/2018  
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           26/03/2018                  Original Version
 */
@isTest
private class AccessTimeTriggerTest {

    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = '00034421';
        insert acc;
        system.assertNotEquals(acc.Id, null);
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        system.assertNotEquals(opp.Id, null);
        
        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('00034421', acc.Id);
        insert phsAccount;
        system.assertNotEquals(phsAccount.Id, null);
        
        Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true,Price_Book_Type__c='Standard' );
        update standardPricebook;
        
        Product2 prodObj =  bg_Test_Data_Utils.createaProduct2('Test Product');
        insert prodObj;
        system.assertNotEquals(prodObj.Id, null);
        
        PricebookEntry priceBookEntry = bg_Test_Data_Utils.createPricebookEntry(prodObj.Id, standardPricebook.Id);
        priceBookEntry.IsActive = true;
        insert priceBookEntry;
        system.assertNotEquals(priceBookEntry.Id, null);
        
        Order orderObj = bg_Test_Data_Utils.createAgreement(acc.Id, opp.Id, system.today(), 'draft');
        orderObj.Pricebook2Id = standardPricebook.Id;
        insert orderObj;
        system.assertNotEquals(orderObj.Id, null);
              
      
        
        Agreement_Location__c agL = new Agreement_Location__c(Location_Salesforce_Account__c = acc.id, Agreement__c = orderObj.id);
        insert agL;
               
        
        
        OrderItem orderItemObj = bg_Test_Data_Utils.createOrderItem(phsAccount.Id, orderObj.Id, priceBookEntry.Id,agL.Id);
        insert orderItemObj;
        system.assertNotEquals(orderItemObj.Id, null);
        
        List<Access_Time__c> accessTimeList = new List<Access_Time__c>();
        for(Integer i=0; i<5; i++){
            Access_Time__c accessTime = bg_Test_Data_Utils.createAccessTime(orderObj.Id, phsAccount.Id, orderItemObj.Id);
            accessTimeList.add(accessTime);
        }
        insert accessTimeList;
        system.assertNotEquals(accessTimeList, null);
    }
    
    private static testMethod void testUpdateRecord() {
        
        Access_Time__c accessTime = [Select Id, Name, Agreement__c, Comments__c, CustomSortOrder__c, Day__c, From__c, PHS_Account__c, PHS_Account__r.Name, 
                                                Product_Line_Item__r.OrderItemNumber, To__c From Access_Time__c Limit 1];
        Test.startTest();
            //accessTime.From__c = '12:00';
            update accessTime;
        Test.stopTest();
    }

    private static testMethod void testDeleteRecord() {
        
        Access_Time__c accessTime = [Select Id, Name, Agreement__c, Comments__c, CustomSortOrder__c, Day__c, From__c, PHS_Account__c, PHS_Account__r.Name, 
                                                Product_Line_Item__r.OrderItemNumber, To__c From Access_Time__c Limit 1];
        Test.startTest();
            delete accessTime;
        Test.stopTest();
    }

}