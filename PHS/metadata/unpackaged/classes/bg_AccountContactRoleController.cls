/*********************************************************************************
 * bg_AccountContactRoleController
 *
 * Controller for the account contact role object.
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 18-01-2016
 *
 *********************************************************************************/

public with sharing class bg_AccountContactRoleController
{
    private final Id contactId;
    public List<AccountContactRole> acrs { get; set; }
    public bg_AccountContactRoleController(ApexPages.StandardController stdController)
    {
        contactId = stdController.getId();
        acrs = [select AccountId
                              ,Account.Name
                              ,Role
                              ,IsPrimary
                              ,CreatedBy.Name
                              ,CreatedDate
                     from AccountContactRole
                   where ContactId =:contactId
               order by Account.Name ASC];             
    }
}