/**
 *  Class Name: LeadMapController  
 *  Description: This is a Controller Class for LeadMap.
 *  Company: Standav
 *  CreatedDate:06/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             06-11-2017                 Original version
 */
public class LeadMapController {
    
    public String address{
        get;
        set;
    }
     public String streetAddress{
        get;
        set;
    }
    private final Lead leadrecord;

    public LeadMapController(ApexPages.StandardController stdController){
        this.leadrecord = (Lead)stdController.getRecord();
        leadAddressAllocate();
    }
    
    public void leadAddressAllocate(){
         address='';
        streetAddress='';
        if(leadrecord.PostalCode != NULL)
            address = leadrecord.PostalCode;
        else
            address = leadrecord.Street + ' ' + leadrecord.City;
        system.debug('Address----->'+address);
        if(leadrecord.Street!=null)
        {
        streetAddress=leadrecord.Street.replace('\n', ' ');
        streetAddress=streetAddress.replace('\r', '  ');
        }
        address =address.replace('\r', '  ').replace('\n', ' ');
        
    }
}