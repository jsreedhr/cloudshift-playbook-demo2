/**********************************************************************
* bg_DeleteIntegrationLogs:
*
* Scheduled Batch job to delete Integration Logs older than N days
* Created By: KH
* Created Date: 11/11/16
*
* Changes: 
***********************************************************************/

global class bg_DeleteIntegrationLogs implements Schedulable, Database.Batchable<SObject>
{    
    /*
        - This can be used to run the job using Apex Anonymous.

        public static String CRON_EXP = '0 50 09 * * ?';
        System.schedule('ScheduleApexClassTest', CRON_EXP, new bg_DeleteIntegrationLogs());
    */

    global String query;

    global void execute(SchedulableContext sc)
    {
        Wildebeest_Integration__c iSettings =  Wildebeest_Integration__c.getValues('Host');
        query = 'SELECT Id FROM Integration_Log__c WHERE CreatedDate != LAST_N_DAYS:' + Integer.valueOf(iSettings.Integration_Log_Purge_Limit__c);
        Database.executeBatch(new bg_DeleteIntegrationLogs(query), 100);
    }

    global bg_DeleteIntegrationLogs() {}

    /*private static void scheduleDelete(String query)
    {
        system.scheduleBatch(new bg_DeleteIntegrationLogs(query), 'Integration Log Cleardown' + Math.random() * 5, 0, 100);
    }*/
    
    global bg_DeleteIntegrationLogs(String query)
    {
        this.query = query;
    }    

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<Integration_Log__c> scope)
    {
        delete scope;
    }

    global void finish(Database.BatchableContext BC){}
}