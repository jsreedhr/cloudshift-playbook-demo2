/*********************************************************************************
 * phs_Task_Helper
 *
 * A helper class for the task object to perform the following actions
 * -  Prevent creating a task on a closed case
 * 
 * Author: AG - PHS
 * Created: 12-07-2017
 *********************************************************************************/
public with sharing class phs_Task_Helper {
    
	public static void validateCreatingATask(List<Task> taskRecords) {
    	for(Task t : taskRecords){	
			if(t.TaskSubtype == 'Task'){
        		if(t.WhatId != null){
	        		If(t.whatId.getsObjectType() == Case.sObjectType){
        				String id  = t.WhatId;
          				Case c = [Select Id, CaseNumber, Subject, Description, IsClosed from Case where Id = :id];        
           	 			if(c.IsClosed){
	    					t.addError(Label.Create_TaskClosedCase_Error);
    					}
    				}
        		}      
   			}
		}
    }
}