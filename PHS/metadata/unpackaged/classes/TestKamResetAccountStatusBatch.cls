public class TestKamResetAccountStatusBatch{
    
}

/*
global class TestKamResetAccountStatusBatch  implements Database.Batchable < sObject > , Database.Stateful {
 Integer resetAccountsCount = 0;
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select Id,Test_Key_Account__c From account Where Test_Key_Account__c = TRUE';
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext bc, List < Account > accList) {
     
          Set<ID> keyAccounts = new Set<ID>();
        List<Account> childAccounts = new List<Account>();
        for(Account a : accList){
           a.Test_Key_Account__c=FALSE;
            a.Test_Key_Account_Role__c=NULL;
            a.Test_Key_Account_Type__c=NULL;
            childAccounts.add(a);
        }
         
         try{
              if(childAccounts.size() > 0)
              update childAccounts;
             resetAccountsCount += childAccounts.size();
         }
         catch(Exception ex)
         {
             System.debug('Error resetting/updating account status');
         }
       

     }
   global void finish(Database.BatchableContext bc) {
        System.debug('Updated Accounts Count : ' + resetAccountsCount);
       
       //Main batch called from here
      TestKAMRolesBatch kamBatch = new TestKAMRolesBatch();
        Database.executeBatch(kamBatch,2000);
        
    }

    
}*/