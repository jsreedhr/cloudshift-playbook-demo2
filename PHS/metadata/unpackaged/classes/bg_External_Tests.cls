@isTest
private class bg_External_Tests {
    
	static testMethod void createExternalTestLead(){
		
		External__c testExternal = new External__c();
		testExternal.Record_Type__c = 'Lead';
        testExternal.Lead_Company__c = 'test co';
		testExternal.Lead_Email__c = 'test@test.com';
		testExternal.Lead_First_Name__c = 'testFirstName';
		testExternal.Lead_Last_Name__c = 'testLastName';
		testExternal.Lead_Phone__c = '1234';

		insert testExternal;

		testExternal = [SELECT Lead_Company__c, Lead_Email__c, Lead_First_Name__c, Lead_Last_Name__c, Lead_Phone__c FROM External__c WHERE Id =: testExternal.Id];

		Lead leadList = [SELECT Company, Email, FirstName, LastName, Phone FROM Lead LIMIT 1];


		// Asserts
		// System.assertEquals('test co', testExternal.Lead_Company__c);
		// System.assertEquals('test@test.com', testExternal.Lead_Email__c);
		// System.assertEquals('testFirstName', testExternal.Lead_First_Name__c);
		// System.assertEquals('testLastName', testExternal.Lead_Last_Name__c);
		// System.assertEquals('1234', testExternal.Lead_Phone__c);

		System.assertEquals('test co', leadList.Company);
		System.assertEquals('test@test.com', leadList.Email);
		System.assertEquals('testFirstName', leadList.FirstName);
		System.assertEquals('testLastName', leadList.LastName);
		System.assertEquals('1234', leadList.Phone);
		
	}

	// static testMethod void createExternalTestCase(){
		
	// 	External__c testExternal = new External__c();
	// 	testExternal.Record_Type__c = 'Case';
	// 	testExternal.Case_Description__c = 'test desc';
	// 	testExternal.Case_Origin__c = 'test origin';
	// 	testExternal.Case_Priority__c = 'test priority';

	// 	insert testExternal;

	// 	testExternal = [SELECT Case_Account__c, Case_Description__c, Case_Origin__c, Case_Priority__c FROM External__c WHERE Id =: testExternal.Id];

	// 	Case caseList = [SELECT Description, Origin, Priority FROM Case LIMIT 1];


	// 	System.assertEquals('test desc', caseList.Description);
	// 	System.assertEquals('test origin', caseList.Origin);
	// 	System.assertEquals('test priority', caseList.Priority);
				
	// }



}