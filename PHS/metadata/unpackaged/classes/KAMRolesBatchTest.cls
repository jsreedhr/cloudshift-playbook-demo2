/*
*  Class Name:  KAMRolesBatchTest
*  Description: This class covers KAMRolesBatch class
*  Company: dQuotient
*  CreatedDate: 06-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer            Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      06-Nov-2017            Orginal Version
*/
@isTest
private class KAMRolesBatchTest{
    static User user;
    static List<Account> accountList;
    static List<PHS_Account__c> phsAccountList;
    static List<PHS_Account_Relationship__c> phsList;
    static List<KAM_Account_Team_Management__c> kamList;
    @isTest
    private static void createData(){
        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1];
       // Insert Test user record as current user
        System.runAs(currentUser){
            user = new User(
                firstName = 'Test',
                lastName = 'User'+Math.random()*10000,
                email = 'sometest.user123@dquotient.com',
                Username = 'sometest.user123@dquotient.com',
                Alias = 'dfghj',
                EmailEncodingKey = 'ISO-8859-1',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                ProfileId = profile.Id
            );
            insert user;
        }
        //Insert Accounts
        accountList = new List<Account>();
        //adding 10 + 2 records, to create multi level parent-child relationship
        for(Integer i=0;i<12;i++){
            Account account = new Account(
                Name = 'Business Entity ' + i,
                BillingPostalCode = 'CF83 1XH',
                Key_Account__c = TRUE,
                Test_Key_Account__c=TRUE,
                HasNegotiatorResponsibility__c = TRUE,
                Phone = '01273464629'
            );
            accountList.add(account);
        }
        insert accountList;

        //Insert PHS_Account__c records
        phsAccountList = new List<PHS_Account__c>();
        for(Account account:accountList){
            PHS_Account__c phsAccount = new PHS_Account__c(
                Salesforce_Account__c = account.Id
            );
            phsAccountList.add(phsAccount);
        }
        insert phsAccountList;

        //Insert PHS_Account_Relationship__c records
        phsList = new List<PHS_Account_Relationship__c>();
        String relationshipType = SObjectType.PHS_Account_Relationship__c.Fields.Relationship_Type__c.PicklistValues[0].getValue();
        //size of phsList to be half of accountList size
        for(Integer i=0;i<5;i++){
            PHS_Account_Relationship__c phs = new PHS_Account_Relationship__c(
                Relationship_Type__c = relationshipType,
                Parent_Account__c = accountList[i].Id,
                Child_Account__c = accountList[i+5].Id,
                Parent_PHS_Account__c = phsAccountList[i].Id,
                Child_PHS_Account__c = phsAccountList[i+5].Id
            );
            phsList.add(phs);
        }
        //creating two additional records to create multi level parent-child relationship
        //(account[0] -> account[5] -> account[10], account[1] -> account[6] -> account[11])
        for(Integer i=5;i<7;i++){
            PHS_Account_Relationship__c phs = new PHS_Account_Relationship__c(
                Relationship_Type__c = relationshipType,
                Parent_Account__c = accountList[i].Id,
                Child_Account__c = accountList[i+5].Id,
                Parent_PHS_Account__c = phsAccountList[i].Id,
                Child_PHS_Account__c = phsAccountList[i+5].Id
            );
            phsList.add(phs);
        }
        insert phsList;

        //Insert KAM_Account_Team_Management__c records
        kamList = new List<KAM_Account_Team_Management__c>();
        String keyAccountRole = SObjectType.KAM_Account_Team_Management__c.Fields.Key_Account_Role_Type__c.PicklistValues[0].getValue();
        String keyAccountType = SObjectType.KAM_Account_Team_Management__c.Fields.Key_Account_Type__c.PicklistValues[0].getValue(); 
    /*  String keyAccountRole = SObjectType.KAM_Account_Team_Management__c.Fields.Test_Key_Account_Role__c.PicklistValues[0].getValue();
        String keyAccountType = SObjectType.KAM_Account_Team_Management__c.Fields.Test_Key_Account_Type__c.PicklistValues[0].getValue(); */
        for(Integer i=0;i<5;i++){
            KAM_Account_Team_Management__c kam = new KAM_Account_Team_Management__c(
                Account_Owner__c = user.Id,
                Master_Account__c = accountList[i].Id,
                    Key_Account_Role_Type__c = keyAccountRole,
                Key_Account_Type__c = keyAccountType
            );
            kamList.add(kam);
        }
        insert kamList;
    }
    @isTest
    private static void KAMRolesBatchTest(){
        createData();
       /* Test.StartTest();
       // KAMRolesBatch batch = new KAMRolesBatch();
        //Database.executeBatch(batch);
        
        
         
        Test.stopTest();
        
        Account account = [Select Id, OwnerId, Test_Key_Account_Role__c From Account Where Name = 'Business Entity 11' Limit 1];
        System.assertEquals(kamList[0].Key_Account_Role_Type__c, account.Test_Key_Account_Role__c);*/
       // System.assertEquals(user.Id, account.ownerId);
    }
    
    
     
}