@isTest
public class Test_CompetitorsController {
/**
*  @Class Name:    Test_CompetitorsController 
*  @Description:   This is a test class for the CompetitorsController
*  @Company: CloudShift
*  CreatedDate: 16/11/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  David Kurtanjek     16/11/2017                  Original Version
*
*/
    
    private static testMethod void Test_LeadProductsController(){
        
        List<Lead> plotList = new List<Lead>();  
        
        //Create Lead
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('3');
        insert u;
        Lead objLead = bg_Test_Data_Utils.createLead('3');
        objLead.OwnerId = u.id;
        insert objLead;
        
        //Create an account
        Account a = bg_Test_Data_Utils.createAccount('New Account');
           a.Company_Status__c = 'Competitor';
        insert a;
                
        //Create Competitor
        Competitor__c c = bg_Test_Data_Utils.createCompetitor(objLead.id);
        c.Competitor_Account__c = a.id;
        insert c;
        
        //Create Competitor
        Competitor__c d = bg_Test_Data_Utils.createCompetitor(objLead.id);
        d.Competitor_Account__c = a.id;
        insert d;
        
        PageReference pageRef = Page.LeadCompetitor;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', objLead.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objLead);
        LeadCompetitorsController classSC = new LeadCompetitorsController(sc);
        classSC.saveCompetitor();
        classSC.createRecords();
        classSC.addRow();
        classSC.saveCompetitor();
        classSC.selectAll();
        classSC.deleteSelectedRows();
        classSC.rowNo ='1';
        classSC.delRow();
        classSC.saveCompetitor();
    }
}