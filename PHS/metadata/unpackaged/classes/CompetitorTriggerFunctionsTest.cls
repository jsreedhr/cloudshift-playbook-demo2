/**
 *  @Class Name:    CompetitorTriggerFunctionsTest
 *  @Description:   This is a test class for CompetitorTriggerFunctions
 *  @Company:       dQuotient
 *  CreatedDate:    22/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           22/11/2017                  Original Version
 */
@isTest
private class CompetitorTriggerFunctionsTest {

    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true ,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        insert lead;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        opp.Closure_Reason__c = 'Other';
        opp.Next_Renewal_Date__c = Date.today().adddays(30);
        insert opp;
        
        Competitor__c competitor = bg_Test_Data_Utils.createCompetitor(lead.Id);
        insert competitor;

    }
    
	private static testMethod void testSetLostOpportunity() {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];
        Opportunity oppo = [Select Id, OwnerId from Opportunity where OwnerId= :u.Id];
        Competitor__c competitor = [Select Lead__c, Strengths__c, Weaknesses__c from Competitor__c where Lead__c = :testLead.Id];
        system.debug('competitor--->' +competitor);
        competitor.Opportunity__c =oppo.id;
        competitor.Lost_To__c = TRUE;
        test.startTest();
            update competitor;
            system.assertEquals(competitor.Opportunity__c, oppo.Id);
        test.stopTest();
	}

}