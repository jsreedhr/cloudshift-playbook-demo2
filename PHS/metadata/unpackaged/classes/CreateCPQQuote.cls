/**
 *  @Class Name:    CreateCPQQuote
 *  @Description:   This is a controller for CreateCPQQuotePage
 *  @Company:       dQuotient
 *  CreatedDate:    01/15/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *                                      
 */
public class CreateCPQQuote {
    public Opportunity opp {
        get;
        set;
    }
    public string quotename {
        get;
        set;
    }
    public Date expirydateVar2 {
        get;
        set;
    }
    public string expirydate {
        get;
        set;
    }
    public string Status {
        get;
        set;
    }
    public string TeleAppointer {
        get;
        set;
    }
    public string TeleAppointerEmail {
        get;
        set;
    }
    public string PaymentTerms {
        get;
        set;
    }
    //  public string primary {        get;set;    }
    public string oppName {
        get;
        set;
    }
    public string accName {
        get;
        set;
    }
    public string loggedinUser {
        get;
        set;
    }
    public string MethodofPayment {
        get;
        set;
    }
    public string loggedinUserNm {
        get;
        set;
    }
    public OpportunityContactRole primaryContact {
        get;
        set;
    }
    public Contact primaryContNm {
        get;
        set;
    }
    public string priceBookIDClUrl {
        get;
        set;
    }
    public string fromOpp {
        get;
        set;
    }

   public string priceBookIDClUrlNm {
        get;
        set;
    }

    // public CreateCPQQuote() {
    public CreateCPQQuote() {

        primaryContact = new OpportunityContactRole();
        // String oppId = ApexPages.currentPage().getParameters().get('oppId');
        System.debug('chkURL : ' + Apexpages.currentPage().getUrl());

        string oppId1 = Apexpages.currentPage().getUrl().substringAfter('006').substringBefore('&clc=');
        Integer indexint = Apexpages.currentPage().getUrl().indexOf('oppid=006');
        if(indexint>0)
        oppId1 =  Apexpages.currentPage().getUrl().substring(indexint+6,indexint+21);
        // string oppId2 = Apexpages.currentPage().getUrl().substringAfter('&retURL=%2F').substringBefore('&scontrolCaching');
       // string oppId2 = Apexpages.currentPage().getUrl().substringAfter('&retURL=%2F');
         string oppId2 = Apexpages.currentPage().getUrl().substringAfter('oppid=');
        
        List <PriceBook2> lst_standardpricebook = new List <PriceBook2>([SELECT ID, isActive, isStandard FROM PriceBook2 WHERE isActive = TRUE AND isStandard = TRUE LIMIT 1]);
        
        if (oppId2.length() >= 15)
            oppId2 = oppId2.substring(0, 15);

        System.debug('Opp Id -1 :' + oppId1);
        System.debug('Opp Id -1 :' + oppId1.length());
        System.debug('Opp Id -2:' + oppId2);
        System.debug('UserInfo.getUiTheme()' + UserInfo.getUiTheme());

        if ((string.isNotBlank(oppId1) && oppId1.length() == 15) || (string.isNotBlank(oppId2) && oppId2.startsWith('006'))) {
            fromOpp = 'yes';
            if (UserInfo.getUiTheme() == 'Theme4d' || UserInfo.getUiTheme() == 'Theme4u' || UserInfo.getUiThemeDisplayed() == 'Theme4t') {
            
              opp = [SELECT name, Product_Count__c, closedate, Account.BillingCity, account.BillingStreet, account.BillingPostalCode, Pricebook_Id__c, Account.name, accountid, account.BillingState, Pricebook2.Id, Pricebook2.name, Tele_Appointer__c, Tele_Appointer_Email__c FROM Opportunity WHERE id =: oppId1 limit 1];
            } else {
              opp = [SELECT name, Product_Count__c, closedate, Account.BillingCity, account.BillingStreet, account.BillingPostalCode, Pricebook_Id__c, Account.name, accountid, account.BillingState, Pricebook2.Id, Pricebook2.name, Tele_Appointer__c, Tele_Appointer_Email__c FROM Opportunity WHERE id =: oppId2 limit 1];
            }

            if (String.isNotBlank(opp.Account.BillingCity))
                opp.Account.BillingCity = string.escapeSingleQuotes(opp.Account.BillingCity);
            if (String.isNotBlank(opp.Account.BillingStreet)) {
                opp.Account.Billingstreet = string.escapeSingleQuotes(opp.Account.Billingstreet);
                opp.Account.Billingstreet = opp.Account.Billingstreet.replaceAll('\\s+', '');
            }
            if (String.isNotBlank(opp.Account.BillingPostalCode))
                opp.Account.BillingPostalCode = string.escapeSingleQuotes(opp.Account.BillingPostalCode);


            try {
                if (UserInfo.getUiTheme() == 'Theme4d' || UserInfo.getUiTheme() == 'Theme4u' || UserInfo.getUiThemeDisplayed() == 'Theme4t')
                    primaryContact = [select ContactId from OpportunityContactRole where IsPrimary = true and opportunityid =: oppId1 limit 1];
                else
                    primaryContact = [select ContactId from OpportunityContactRole where IsPrimary = true and opportunityid =: oppId2 limit 1];
                primaryContNm = [select name from contact where id =: primaryContact.ContactId limit 1];

            } catch (Exception ex) {
                System.debug('Exception fetching primary contactrole or name');
            }
            Date expirydateVar = Date.today().adddays(30);

            expirydateVar2 = expirydateVar;
            expirydate = String.valueOf(expirydateVar2.day()) + '/' + expirydateVar2.month() + '/' + expirydateVar2.year();
            Status = 'Draft';
            PaymentTerms = 'Net 30';
            MethodofPayment = 'Yearly in Advance';
            TeleAppointer = opp.Tele_Appointer__c;
            TeleAppointerEmail = opp.Tele_Appointer_Email__c;
            oppName = string.escapeSingleQuotes(opp.Name);
            accName = string.escapeSingleQuotes(opp.Account.Name);
            loggedinUserNm = UserInfo.getName();
            loggedinUser = UserInfo.getUserId();
        	If(opp.Pricebook2.Id != NULL){
		    priceBookIDClUrl = opp.Pricebook2.Id;
            priceBookIDClUrlNm =opp.Pricebook2.Name;
             
            }          
            else {
                priceBookIDClUrl = lst_standardpricebook[0].id;
               priceBookIDClUrlNm =opp.Pricebook2.Name;
            }                  
        	} else {
            fromOpp = 'no';
            System.debug('Not from opp page');
        }
    }

}