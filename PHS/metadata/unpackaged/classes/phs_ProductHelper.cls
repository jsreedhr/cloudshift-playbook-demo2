/*********************************************************************************
 * phs_ProductHelper
 *
 * A helper class for the product object to perform the following actions
 * -  Create price book entry for non-standard price books when new product created
 * 
 * Author: AG - PHS
 * Created: 24-07-2017
 *********************************************************************************/
public with sharing class phs_ProductHelper {
    public static void createNonStandardPriceBookEntriesForNewProduct(List<Product2> products) {
    	for(Product2 p : products){
    		if(p.IsActive == true){   		
	    		List<pricebookEntry> listForInsert = new List<pricebookEntry>();	
	    		        		
	    		pricebook2 SPB = [SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true];	
	    		
	    		PricebookEntry priceBookEntry1 = new PricebookEntry(IsActive = true, 
				Product2Id = p.id, 
				UnitPrice = p.Target_Price__c, 
				Pricebook2Id = SPB.Id,
				UseStandardPrice = false);  
	    		
	    		listForInsert.add(priceBookEntry1);
	    	
	       		for(pricebook2 nspb : [SELECT Id FROM Pricebook2 WHERE IsStandard = false]){
	   		  		
					PricebookEntry priceBookEntry = new PricebookEntry(IsActive = true, 
					Product2Id = p.id, 
					UnitPrice = p.Target_Price__c, 
					Pricebook2Id = nspb.Id,
					UseStandardPrice = false);  
							  				 	 
					listForInsert.add(priceBookEntry);
	   		  	
	       		}
				insert listForInsert;
    		}
		}
	}
	
public static void syncPriceBooksOnProductUpdate(List<Product2> products, Map<Id,Product2> oldMap) {
   	for(Product2 p : products){
    	if(p.IsActive == true){  
    		Product2 oldProd = oldMap.get(p.Id);
    		
    		List<PricebookEntry> check = [SELECT Id FROM PricebookEntry WHERE IsActive = true and Product2Id =: p.Id
    		 							  AND priceBook2Id IN (SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true)];	    		
    		if(check.size() < 1){
    			createNonStandardPriceBookEntriesForNewProduct(products);
    		}
    		else if(p.Target_Price__c != oldProd.Target_Price__c)
    		{
    			check[0].UnitPrice = p.Target_Price__c;
    			update check[0];
    			updateRelatedCustomPriceBookEntries(p);
    		}
    		 		
    	}
	}
}

public static void updateRelatedCustomPriceBookEntries(Product2 p) {
    List<pricebookEntry> listForEntryUpdate = new List<pricebookEntry>();

	List<pricebookEntry> Matches = [SELECT Id, Name, Pricebook2Id, UnitPrice, Product2Id, IsActive, Is_Negotiated__c FROM PricebookEntry 
	                                where product2id =: p.Id 
	                                and isactive = true 
	                                and Is_Negotiated__c = false
	                                AND priceBook2Id IN (SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = false)];
			        		 
	for(pricebookEntry aMatch : Matches){		
		aMatch.UnitPrice = p.Target_Price__c; 
		listForEntryUpdate.add(aMatch);
	}
		          	    			    	 					       	   				
	update listForEntryUpdate;

}
}