/*
 *  Class Name:  TestKAMRolesBatch
 *  Description: This batch class updates Owner, Key_Account_Role & Key_Account_Type Fields
 *               in "Account" object records based on corresponding values in KAM_Account_Team_Management__c object record
 *  Company: dQuotient
 *  CreatedDate: 14-Feb-2018
 *
 *  Modification Log
 *  ----------------------------------------------------------------------------------
 *  Developer          Modification Date      Comments
 *  ----------------------------------------------------------------------------------
 *  Mohammed Rizwan      14-Feb-2018            Orginal Version
 *  Swetha TG            22-May-2018            Modified 
 */
 
 public class TestKAMRolesBatch{
     
 }
/* Commenting for test coverage
global class TestKAMRolesBatch implements Database.Batchable < sObject > {
    //map<Account Id, KAM Record>
    Map < Id, KAM_Account_Team_Management__c > kamMap = new Map < Id, KAM_Account_Team_Management__c > ();
    //map<Account Id, Set<Id of all Child Accounts>>
    // Map < Id, Set < Id >> phsChildrenMap = new Map < Id, Set < Id >> ();
    Map < Id, List < PHS_Account_Relationship__c >> phsChildrenMap = new Map < Id, List < PHS_Account_Relationship__c >> ();
    Map < String, String > phsRelationshipMap = new Map < String, String > ();
   // Integer updatedAccountsCount = 0, phsRecordsCount = 0, kamRecordsCount = 0;
    Map<String, KAM_Batch_Record__c> MapKamBatchRec = new Map<String,KAM_Batch_Record__c>();
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String strIncome = 'IncomeManagerLocation';
        String query = 'Select Id, Child_Account__c, Parent_Account__c ,Relationship_Type__c From PHS_Account_Relationship__c Where isDeleted = FALSE and Relationship_Type__c != :strIncome   ';
        return Database.getQueryLocator(query);
    }
    
   

    global void execute(Database.BatchableContext bc, List < PHS_Account_Relationship__c > phsList) {
        //populate kamMap only once
        if (kamMap == NULL || kamMap.values().size() == 0) {
            List < KAM_Account_Team_Management__c > kamList = new List < KAM_Account_Team_Management__c > ();
            try {
               // kamList = [SELECT Account_Owner__c, Key_Account_Role_Type__c, Key_Account_Type__c, Master_Account__c From KAM_Account_Team_Management__c Where Account_Owner__c != NULL AND isDeleted = FALSE LIMIT 49999];
                kamList = [SELECT Account_Owner__c, Key_Account_Role_Type__c, Key_Account_Type__c, Master_Account__c From KAM_Account_Team_Management__c Where Account_Owner__c != NULL AND isDeleted = FALSE LIMIT 49999];
               

              //  kamRecordsCount = kamList.size();

                if (kamList.size() == 49999) {
                    System.debug('WARNING : KAM_Account_Team_Management__c may have more than 50k records');
                }
            } catch (Exception ex) {
                System.debug('KAM_Account_Team_Management__c records couldnt be fetched, no records to be updated, Error : ' + ex.getMessage());
            }
            if (kamList != NULL && kamList.size() > 0) {
                for (KAM_Account_Team_Management__c kam: kamList) {
                    if (!kamMap.containsKey(kam.Master_Account__c)) {
                        kamMap.put(kam.Master_Account__c, kam);
                    }
                }
            }
        }
        //following if block to execute independently of the "if block" written above, don't club these two together
      //  System.debug('inside execute, phs size till now : ' + phsRecordsCount);

        if (kamMap != NULL && phsList.size() > 0) {
          //  phsRecordsCount += phsList.size();

            for (PHS_Account_Relationship__c phs: phsList) {
                //avoid direct cyclical relationship
                if (phs.Parent_Account__c != phs.Child_Account__c) {
                    if(!phsChildrenMap.containsKey(phs.Parent_Account__c)) 
                    phsChildrenMap.put(phs.Parent_Account__c, new List < PHS_Account_Relationship__c > ());
                    phsChildrenMap.get(phs.Parent_Account__c).add(phs);
                  
                    
                }
                
                
            }
            
            
             updateAccountRecordValues(kamMap,phsChildrenMap);
        }
       
    }
    global void finish(Database.BatchableContext bc) {
       // updateAccountRecordValues();
        //second batch called from updateAccountRecordValues method, passing Map<AccountId, Account>
        
       List<KAM_Batch_Record__c> ListKamBatchRec = [Select  Key_AccountId__c,Key_Account__c,Key_Account_Role__c,Key_Account_Type__c,Id from KAM_Batch_Record__c limit 49999];
        
        for(KAM_Batch_Record__c obj: ListKamBatchRec){
            MapKamBatchRec.put(obj.Key_AccountId__c,obj);
        }
        TestKAMHelperBatch helperBatch = new TestKAMHelperBatch(MapKamBatchRec);
        Database.executeBatch(helperBatch);
    }

    //helper methods
    void updateAccountRecordValues( Map < Id, KAM_Account_Team_Management__c > kamMap , Map < Id, List < PHS_Account_Relationship__c >> phsChildrenMap) {
        if (kamMap == NULL || kamMap.values().size() == 0 || phsChildrenMap == NULL || phsChildrenMap.values().size() == 0)
                    return;

        
        
        Set < Id > exploredAccountSet = new Set < Id > ();
        Map < Id, Account > accountMap = new Map < Id, Account > ();

        List < KAM_Account_Team_Management__c > kamList = kamMap.values();
        for (KAM_Account_Team_Management__c kam: kamList) {

            //if master account has not been explored, else ignore this record
            if (!exploredAccountSet.contains(kam.Master_Account__c)) {
                exploredAccountSet.add(kam.Master_Account__c);

                // add parent account and direct children using phsChildrenMap in accountUpdateList
                if (phsChildrenMap.containsKey(kam.Master_Account__c)) {
                    Account parentAccount = new Account(
                        Id = kam.Master_Account__c,
                        Test_Key_Account__c = TRUE,
                        Test_Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                        Test_Key_Account_Type__c = kam.Key_Account_Type__c
                        
                    );
                    
                    
                        //Custom Object Rec
                        KAM_Batch_Record__c tempObj = new KAM_Batch_Record__c (
                            Key_AccountId__c = kam.Master_Account__c,
                            Key_Account__c = TRUE,
                            Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                            Key_Account_Type__c = kam.Key_Account_Type__c
                            );
                    
                    MapKamBatchRec.put(tempObj.Key_AccountId__c,tempObj);
                    
                    if (!accountMap.containsKey(parentAccount.Id))
                        accountMap.put(parentAccount.Id, parentAccount);

                    for (PHS_Account_Relationship__c objRel: phsChildrenMap.get(kam.Master_Account__c)) {
                        Account account = new Account(
                            Id = objRel.Child_Account__c,
                            Test_Key_Account__c = TRUE,
                            Test_Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                            Test_Key_Account_Type__c = kam.Key_Account_Type__c
                        );
                        if (!accountMap.containsKey(account.Id))
                            accountMap.put(account.Id, account);
                            
                            //Custom Object Rec
                        KAM_Batch_Record__c tempObjChild = new KAM_Batch_Record__c (
                            Key_AccountId__c =  objRel.Child_Account__c,
                            Key_Account__c = TRUE,
                            Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                            Key_Account_Type__c = kam.Key_Account_Type__c
                            );
                    
                    MapKamBatchRec.put(tempObjChild.Key_AccountId__c,tempObjChild);
                            
                      //  if (objRel.Relationship_Type__c == 'BuyerLocation') {
                        //    continue;
                        //}
                        
                         if (objRel.Relationship_Type__c != 'BuyerLocation')
                            getAllChildren(objRel.Child_Account__c, exploredAccountSet, accountMap, kam,MapKamBatchRec);

                    }
                }

            
                
           } 
        }
        
      //  System.debug('account records count : ' + accountMap.values().size());

        //Get the Key accounts that has no relationship records
        for(KAM_Account_Team_Management__c kam: kamList)
        {
            
                 if (!accountMap.containsKey(kam.Master_Account__c)) {
                       Account parentAccount = new Account(
                        Id = kam.Master_Account__c,
                        Test_Key_Account__c = TRUE,
                        Test_Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                        Test_Key_Account_Type__c = kam.Key_Account_Type__c
                    );
                     
                      if (!accountMap.containsKey(parentAccount.Id))
                      {
                        accountMap.put(parentAccount.Id, parentAccount);
                      }
                        
                        
                    //Custom Object Rec
                        KAM_Batch_Record__c tempObjChild = new KAM_Batch_Record__c (
                            Key_AccountId__c = kam.Master_Account__c,
                            Key_Account__c = TRUE,
                            Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                            Key_Account_Type__c = kam.Key_Account_Type__c
                            );
                    
                    MapKamBatchRec.put(tempObjChild.Key_AccountId__c,tempObjChild);
                                

                     
                 }
             
        }

    // System.debug('====accountMap==='+accountMap);        
        //call helper batch from here
        
        // Insert the map into  custom Object to avoid stateful limit exception
        
     
        try{
            if(MapKamBatchRec.values().size()>0 && MapKamBatchRec.values().size()<999)
            insert MapKamBatchRec.values();
        }
        catch(Exception e)
        {
            
        }
        
        
        
    }
    void getAllChildren(Id parentId, Set < Id > exploredAccountSet, Map < Id, Account > accountMap, KAM_Account_Team_Management__c kam,Map<String,KAM_Batch_Record__c> tempMap) {
        if (!phsChildrenMap.containsKey(parentId))
            return;
        //at any level, parent account is updated in previous iteration, only child Account to be updated here
        //following logic used to get children from second level onwards
        if (!exploredAccountSet.contains(parentId)) {
           exploredAccountSet.add(parentId);
            for (PHS_Account_Relationship__c objRel: phsChildrenMap.get(parentId)) {

                Account account = new Account(
                    Id = objRel.Child_Account__c,
                    Test_Key_Account__c = TRUE,
                    Test_Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                    Test_Key_Account_Type__c = kam.Key_Account_Type__c
                );
                if (!accountMap.containsKey(account.Id))
                    accountMap.put(account.Id, account);
                    
                    //Custom Object Rec
                        KAM_Batch_Record__c tempObjChild = new KAM_Batch_Record__c (
                            Key_AccountId__c = objRel.Child_Account__c,
                            Key_Account__c = TRUE,
                            Key_Account_Role__c = kam.Key_Account_Role_Type__c,
                            Key_Account_Type__c = kam.Key_Account_Type__c
                            );
                    
                    MapKamBatchRec.put(tempObjChild.Key_AccountId__c,tempObjChild);
                                
                    
                //if (objRel.Relationship_Type__c == 'BuyerLocation') {
                  //  continue;
                //}
               if (objRel.Relationship_Type__c != 'BuyerLocation')
                    getAllChildren(objRel.Child_Account__c, exploredAccountSet, accountMap, kam,tempMap);
            }
        }
    }
}
*/