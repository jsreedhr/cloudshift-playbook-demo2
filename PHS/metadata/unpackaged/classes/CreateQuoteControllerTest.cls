/**
 *  @Class Name:    CreateQuoteControllerTest
 *  @Description:   This is a test for CreateQuotePage
 *  @Company:       dQuotient
 *  CreatedDate:    11/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aishwarya SK        11/12/2017                 	Original Version
 */
 
 @isTest
 public class CreateQuoteControllerTest{
  
     /**
    * Description: This method is to create Data.
    */
     @testSetup
     static void createData(){
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        insert opp;
     }   
     private static testMethod void testCreateQuoteController(){
        User u = [Select id from User where Alias = 'standt']; 
        Opportunity opp = [Select id from Opportunity where OwnerId=:u.Id];
        ApexPages.currentPage().getParameters().put('oppId', opp.Id);
            
            test.startTest();
            System.runAs(u){
            CreateQuoteController sc = new CreateQuoteController();
            }
            test.stopTest();
     }

}