@isTest
private class LeadMapControllerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;    
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        lead.FLT_Postal_Code__c =flt.id;
        insert lead;
    }
    
    private static testMethod void testleadAddressAllocate() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, Street, City, PostalCode, FLT_Postal_Code__c, OwnerId, Campaigns__c, Status, Lead_Score_Total__c, LeadSource, Wildebeest_Ref__c from Lead where OwnerId= :u.Id];        
        test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
            LeadMapController testLeadMap = new LeadMapController(sc);
            PageReference pageRef = Page.LeadMap;
            pageRef.getParameters().put('id', String.valueOf(testLead.Id));
            Test.setCurrentPage(pageRef);
            testLeadMap.leadAddressAllocate();
        test.stopTest();
	}
}