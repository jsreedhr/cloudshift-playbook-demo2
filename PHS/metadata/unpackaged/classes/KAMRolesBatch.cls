/*
*  Class Name:  KAMRolesBatch
*  Description: This batch class updates Owner, Key_Account_Role & Key_Account_Type Fields
				in "Account" object records based on corresponding values in KAM_Account_Team_Management__c object record
*  Company: dQuotient
*  CreatedDate: 03-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer      		Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      03-Nov-2017            Orginal Version
*/


global class KAMRolesBatch {
  /*  
}
implements Database.Batchable < sObject > {
    Map < id,
    Set < PHS_Account_Relationship__c >> phsSubChildMap;
    Map < Id,
    KAM_Account_Team_Management__c > kamMap;
    Set < Account > accountUpdateSet2;
    //Map<Id,Id> visitedNodes;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Account_Owner__c, Key_Account_Role_Type__c, Key_Account_Type__c, Master_Account__c From KAM_Account_Team_Management__c Where Account_Owner__c != NULL AND isDeleted = FALSE';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List < KAM_Account_Team_Management__c > kamList) {
        if (kamList.size() <= 0)
            return;
        Map < Id, KAM_Account_Team_Management__c > chilIds = new Map < Id, KAM_Account_Team_Management__c > ();


        Set < Id > accountIdSet = new Set < Id > ();
        List < PHS_Account_Relationship__c > phsParentList = new List < PHS_Account_Relationship__c > ();
        List < PHS_Account_Relationship__c > phsChildList = new List < PHS_Account_Relationship__c > ();
        Set < Account > accountUpdateSet1 = new Set < Account > ();
        accountUpdateSet2 = new Set < Account > ();
        //Map<Kam_Master_Account_Id, KAM record>
        kamMap = new Map < Id, KAM_Account_Team_Management__c > ();
        //Map<phs_parent_account_id, phs_parent_account_id>
        Map < Id, PHS_Account_Relationship__c > phsExploredMap = new Map < Id, PHS_Account_Relationship__c > ();
        //Map<parent_Account_id, PHS record>
        Map < Id, PHS_Account_Relationship__c > phsChildMap = new Map < Id, PHS_Account_Relationship__c > ();
        // using set to eardicate duplicate entries, it prevents inconsistencies & infinte loop during recursion
        phsSubChildMap = new Map < Id, Set < PHS_Account_Relationship__c >> ();

        for (KAM_Account_Team_Management__c kam: kamList) {
            if (!kamMap.containsKey(kam.Master_Account__c))
                kamMap.put(kam.Master_Account__c, kam);
        }
        accountIdSet = kamMap.keySet();
        try {
            phsParentList = [Select Parent_Account__r.Name, Child_Account__r.Name, Child_Account__c, Parent_Account__c From PHS_Account_Relationship__c Where Parent_Account__c IN: accountIdSet AND isDeleted = FALSE];
            phsChildList = [Select Parent_Account__r.Name, Child_Account__r.Name, Child_Account__c, Parent_Account__c From PHS_Account_Relationship__c Where Parent_Account__c NOT IN: accountIdSet AND isDeleted = FALSE];
        } catch (Exception ex) {
            System.debug('Error while fetching data from PHS Account Relationship object : ' + ex.getMessage());
        }
        if (phsParentList.size() > 0) {
            
            for (PHS_Account_Relationship__c phs: phsParentList) {
                KAM_Account_Team_Management__c kam = kamMap.get(phs.Parent_Account__c);
                chilIds.put(phs.Child_Account__c, kam);
                if (!phsExploredMap.containsKey(phs.Parent_Account__c)) {
                    Account parentAccount = new Account(
                        Id = phs.Parent_Account__c,
                        
                           
                            Test_Key_Account__c=TRUE,
                Test_Key_Account_Role__c =kam.Key_Account_Role_Type__c,
                Test_Key_Account_Type__c=kam.Key_Account_Type__c
                    );
                    accountUpdateSet1.add(parentAccount);
                    phsExploredMap.put(phs.Parent_Account__c, phs);
                }

                Account childAccount = new Account(
                    Id = phs.Child_Account__c,
                    
                        Test_Key_Account__c=TRUE,
                Test_Key_Account_Role__c =kam.Key_Account_Role_Type__c,
                Test_Key_Account_Type__c=kam.Key_Account_Type__c
                );
                accountUpdateSet1.add(childAccount);


            }

        }


        
        try {

            Set < Id > chilMapIds = new Set < Id > ();
            chilMapIds = chilIds.keySet();
            KAM_Account_Team_Management__c kamList11 = chilIds.values().get(0);


            List < PHS_Account_Relationship__c > incomeAccList = new List < PHS_Account_Relationship__c > ();
            incomeAccList = [select Parent_Account__r.Name, Parent_Account__c, Child_Account__c from PHS_Account_Relationship__c where Relationship_Type__c = 'IncomeManagerLocation'
                and isDeleted = FALSE and Child_Account__c in: chilMapIds
            ];

            if (incomeAccList.size() > 0) {

                for (PHS_Account_Relationship__c phs1: incomeAccList) {
                    if (!phsExploredMap.containsKey(phs1.Parent_Account__c)) {
                        Account parentAccount1 = new Account(
                            Id = phs1.Parent_Account__c,
                            
                               Test_Key_Account__c=TRUE,
                Test_Key_Account_Role__c =kamList11.Key_Account_Role_Type__c,
                Test_Key_Account_Type__c=kamList11.Key_Account_Type__c
                        );
                        accountUpdateSet1.add(parentAccount1);
                        phsExploredMap.put(phs1.Parent_Account__c, phs1);
                    }
                    

                }

            }

        } catch (Exception ex) {
            System.debug('Error while fetching income manager account from PHS Account Relationship object : ' + ex.getMessage());
        }







        //Now we have to check whether the Child_Account__c in PHS list has  children & subchildren (upto n levels)
        // if subsequent children & subchildren are found, add them to the list accountUpdateSet.
        //Using exploredMap to prevent infinite loop & duplicates
        if (phsChildList.size() > 0) {
            //using phsChildList, make a map of each phs.Parent_Account__c & Set of all its children
            for (PHS_Account_Relationship__c phs: phsChildList) {
                if (phs.Parent_Account__c != phs.Child_Account__c) {
                    if (!phsSubChildMap.containsKey(phs.Parent_Account__c)) {
                        Set < PHS_Account_Relationship__c > phsSet = new Set < PHS_Account_Relationship__c > ();
                        phsSet.add(phs);
                        phsSubChildMap.put(phs.Parent_Account__c, phsSet);
                    } else {
                        Set < PHS_Account_Relationship__c > phsSet = phsSubChildMap.get(phs.Parent_Account__c);
                        phsSet.add(phs);
                        phsSubChildMap.put(phs.Parent_Account__c, phsSet);
                    }
                }
            }

            //iterating over phsParentList and find all its subchildren and assign sAme values as PHS_Parent_Account
            for (PHS_Account_Relationship__c phs: phsParentList) {
                // calling recursive function to reach all children & subChildren corresponding to each entry in phsParentList
                // as each parent can have multiple children, each of these children can have multiple sub children and so on....
                // visited Nodes is used to store parent_Id, parent_Id to check & stop recursion
                Map < Id, Id > visitedNodes = new Map < Id, Id > ();
                visitedNodes.put(phs.Parent_Account__c, phs.Parent_Account__c);
                recursiveFunction(phs.Parent_Account__c, phs.Child_Account__c, visitedNodes);
            }

        }
        try {
            List < Account > accountList1 = new List < Account > (accountUpdateSet1);
            List < Account > accountList2 = new List < Account > (accountUpdateSet2);
            update accountList1;
            update accountList2;
        } catch (Exception ex) {
            System.debug('Error while updating account data in Batch : ' + ex);
        }
    }
    global void recursiveFunction(Id ancestorId, Id parentId, Map < Id, Id > visitedNodes) {
        // ancestorId remains same throughout the operation
        // It's Parent_Account__c of each record from phsParentList
        List < PHS_Account_Relationship__c > incomeAccList2 = new List < PHS_Account_Relationship__c > ();
        Map < Id, KAM_Account_Team_Management__c > chilIds2 = new Map < Id, KAM_Account_Team_Management__c > ();
        Map < Id, PHS_Account_Relationship__c > phsExploredMap2 = new Map < Id, PHS_Account_Relationship__c > ();
        if (!phsSubChildMap.containsKey(parentId) || visitedNodes.containsKey(parentId))
            return;

        visitedNodes.put(parentId, parentId);
        Set < PHS_Account_Relationship__c > tempSet = phsSubChildMap.get(parentId);
        KAM_Account_Team_Management__c kam = kamMap.get(ancestorId);
        chilIds2.put(parentId, kam);
        for (PHS_Account_Relationship__c phs: tempSet) {
            chilIds2.put(phs.Child_Account__c, kam);
            Account account = new Account(
                Id = phs.Child_Account__c,
                   
                
                Test_Key_Account__c=TRUE,
                Test_Key_Account_Role__c =kam.Key_Account_Role_Type__c,
                Test_Key_Account_Type__c=kam.Key_Account_Type__c
            );
            accountUpdateSet2.add(account);
            recursiveFunction(ancestorId, phs.Child_Account__c, visitedNodes);
        }

        //try

        try {

            Set < Id > chilMapIds = new Set < Id > ();
            chilMapIds = chilIds2.keySet();
            KAM_Account_Team_Management__c kamList11 = chilIds2.values().get(0);
            List < PHS_Account_Relationship__c > incomeAccList = new List < PHS_Account_Relationship__c > ();
            incomeAccList = [select Parent_Account__r.Name, Parent_Account__c, Child_Account__c from PHS_Account_Relationship__c where Relationship_Type__c = 'IncomeManagerLocation'
                and isDeleted = FALSE and Child_Account__c in: chilMapIds
            ];

            Set < Id > incomeChilds = new Set < Id > ();

            if (incomeAccList.size() > 0) {

                for (PHS_Account_Relationship__c phs1: incomeAccList) {
                    if (!phsExploredMap2.containsKey(phs1.Parent_Account__c)) {
                        incomeChilds.add(phs1.Parent_Account__c);
                        Account parentAccount1 = new Account(
                            Id = phs1.Parent_Account__c,
                          
                                Test_Key_Account__c=TRUE,
                Test_Key_Account_Role__c =kamList11.Key_Account_Role_Type__c,
                Test_Key_Account_Type__c=kamList11.Key_Account_Type__c
                        );
                        accountUpdateSet2.add(parentAccount1);
                        phsExploredMap2.put(phs1.Parent_Account__c, phs1);
                    }
                }

              


            }

        } catch (Exception ex) {
            System.debug('Error while fetching income manager account from PHS Account Relationship object : ' + ex.getMessage());
        }

        //End Try



    }
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
            From AsyncApexJob Where Id =: bc.getJobId()
        ];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {
            a.CreatedBy.Email
        };
        mail.setToAddresses(toAddresses);
        mail.setSubject('KAM Key Roles Batch Status : ' + a.Status);
        mail.setPlainTextBody('Records processed : ' + a.TotalJobItems +
            'with ' + a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });
    }
    */
}