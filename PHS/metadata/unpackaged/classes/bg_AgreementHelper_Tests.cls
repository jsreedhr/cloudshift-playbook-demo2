/*************************************************
bg_AgreementHelper_Tests

Test class for the Order object.

Author: Tom Morris (BrightGen)
Created Date: 02/11/2016
Updates:

**************************************************/

@isTest(SeeAllData=true)
private class bg_AgreementHelper_Tests {

    // Tests if Agreements and Agreement Items are created when forcing the creation method to run. A Contract is auto linked to the Agreement. 
    static testMethod void testAgreementAreCreatedWithRelatedContract()
    {
        // Create Account
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;
        
        // Create Location
        Account location = new Account(Name = 'Test Location Account', BillingPostalCode = 'RH10 7NJ');
        insert location;

        // Create Agreement and relate to Account
        Order testContract = new Order(Name='test',Status = 'Draft', EffectiveDate = System.Today(), AccountId = acct.Id);
        insert testContract;
        
        //Create Agreement Location
        Agreement_Location__c agr = new Agreement_Location__c(Agreement__c = testContract.id, Location_Salesforce_Account__c = location.id);
		Insert agr;
        
        // Get the standard Pricebook Id
        Id standardPriceBookId = Test.getStandardPricebookId();

        // Create the Opp related to the Account
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert testOpp;
        // Assert it has been successfully created
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :testOpp.Id];
        System.assertNotEquals(opp, null);

        Wildebeest_Product__c wbProd = new Wildebeest_Product__c(Name = 'Test WB Prod', Product_Ref__c = '1234', Product_Id__c = 4321.0);
        insert wbProd;

        // Create a Product
        Product2 p2 = new Product2(Name='Test Product',isActive=true,Target_Price__c = 10.00, Wildebeest_Product__c = wbProd.Id);
        insert p2;
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
        System.assertEquals('Test Product', p2ex.Name);

        // Associate the Product and Pricebook to a Pricebook Entry
        List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
                                            where product2id =: p2.Id and Pricebook2Id=: standardPriceBookId]; 
        PricebookEntry pbe;                                 
        if (results.size() < 1) {
             pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
             insert pbe;    
        }
         else
            {
             pbe = results[0];
        }
               
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);

        // Create the Opp Line Item with required fields
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=testOpp.Id, Quantity=1, TotalPrice=99, Quantity__c=1);
        insert oli;
        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbe.Id, oliex.PriceBookEntryId); 

        opp = [SELECT Name, Product_Count__c FROM Opportunity WHERE Id = :testOpp.Id];

        // Force the button press method top run
        String agreementId = bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(testOpp.Id, Integer.valueOf(opp.Product_Count__c));

        // Assert it doesn't return null for the Agreement Id
        System.assertNotEquals(agreementId, null);

        // Assert that the Agreement Items have been created
        List<OrderItem> testAgreementItems = [SELECT Id FROM OrderItem where Orderid IN (SELECT Id from Order where OpportunityId =: testOpp.Id)];
        System.assertEquals(testAgreementItems.size(), 1);

        // Run the method which checks if there are open Agreements related to the Opp
        Boolean hasOpenAgreements = bg_AgreementHelper.returnResultOfOpenAgreement(testOpp.Id);

        // Assert that it does have open Agreements
        System.assertEquals(hasOpenAgreements, TRUE);
    }


    // Tests if Agreements and Agreement Items are created when forcing the creation method to run. A Contract is not auto linked to the Agreement. 
    static testMethod void testAgreementAreCreatedWithoutRelatedContract()
    {
        // Create Account
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;

        // Get the standard Pricebook Id
        Id standardPriceBookId = Test.getStandardPricebookId();

        // Create the Opp related to the Account
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert testOpp;
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :testOpp.Id];
        System.assertNotEquals(opp, null);

        Wildebeest_Product__c wbProd = new Wildebeest_Product__c(Name = 'Test WB Prod', Product_Ref__c = '1234', Product_Id__c = 4321.0);
        insert wbProd;

        // Create a Product
        Product2 p2 = new Product2(Name='Test Product',isActive=true,Target_Price__c = 10.00, Wildebeest_Product__c = wbProd.Id);
        insert p2;
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
        System.assertEquals('Test Product', p2ex.Name);

              // Associate the Product and Pricebook to a Pricebook Entry
        List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
                                            where product2id =: p2.Id and Pricebook2Id=: standardPriceBookId]; 
        PricebookEntry pbe;                                 
        if (results.size() < 1) {
             pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
             insert pbe;    
        }
         else
            {
             pbe = results[0];
        }
            
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);

        // Create the Opp Line Item with required fields
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=testOpp.Id, Quantity=1, TotalPrice=99, Quantity__c=1);
        insert oli;
        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbe.Id, oliex.PriceBookEntryId);

        opp = [SELECT Name, Product_Count__c FROM Opportunity WHERE Id = :testOpp.Id];

        // Force the button press method top run
        String agreementId = bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(testOpp.Id, Integer.valueOf(opp.Product_Count__c));

        // Assert it doesn't return null for the Agreement Id
        System.assertNotEquals(agreementId, null);

        // Assert that the Agreement Items have been created
        List<OrderItem> testAgreementItems = [SELECT Id FROM OrderItem where Orderid IN (SELECT Id from Order where OpportunityId =: testOpp.Id)];
        System.assertEquals(testAgreementItems.size(), 1);
    }

    // Tests that no Agreement is created as there are no Products assiciated to the Opportunity
    static testMethod void testAgreementAreNotCreated()
    {
        // Create Account
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;

        // Get the standard Pricebook Id
        Id standardPriceBookId = Test.getStandardPricebookId();

        // Create the Opp related to the Account
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert testOpp;
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :testOpp.Id];
        System.assertNotEquals(opp, null);

        opp = [SELECT Name, Product_Count__c FROM Opportunity WHERE Id = :testOpp.Id];

        // Force the button press method top run
        String agreementId = bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(testOpp.Id, Integer.valueOf(opp.Product_Count__c));

        // Assert it returns null for the Agreement Id
        System.assertEquals(agreementId, null);

        // Assert that the Agreement Items have not been created
        List<OrderItem> testAgreementItems = [SELECT Id FROM OrderItem where Orderid IN (SELECT Id from Order where OpportunityId =: testOpp.Id)];
        System.assertEquals(testAgreementItems.size(), 0);

        // Run the method which checks if there are open Agreements related to the Opp
        Boolean hasOpenAgreements = bg_AgreementHelper.returnResultOfOpenAgreement(testOpp.Id);

        // Assert that it does not have open Agreements
        System.assertEquals(hasOpenAgreements, FALSE);
    }

    // Tests that the parent Opportunity is updated on update of the Agreement Signature_Authorised__c field
    static testMethod void testOpportunityIsClosedAndDatesUpdated()
    {
        // Create Account
        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
        insert acct;

        // Get the standard Pricebook Id
        Id standardPriceBookId = Test.getStandardPricebookId();

        // Create the Opp related to the Account
        Opportunity testOpp = bg_Test_Data_Utils.createOpportunity('Test', acct.Id);
        insert testOpp;
        Opportunity opp = [SELECT Name FROM Opportunity WHERE Id = :testOpp.Id];
        System.assertNotEquals(opp, null);

        Wildebeest_Product__c wbProd = new Wildebeest_Product__c(Name = 'Test WB Prod', Product_Ref__c = '1234', Product_Id__c = 4321.0);
        insert wbProd;

        // Create a Product
        Product2 p2 = new Product2(Name='Test Product',isActive=true,Target_Price__c = 10.00, Wildebeest_Product__c = wbProd.Id);
        insert p2;
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
        System.assertEquals('Test Product', p2ex.Name);

             // Associate the Product and Pricebook to a Pricebook Entry
        List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
                                            where product2id =: p2.Id and Pricebook2Id=: standardPriceBookId]; 
        PricebookEntry pbe;                                 
        if (results.size() < 1) {
             pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
             insert pbe;    
        }
         else
            {
             pbe = results[0];
        }
            
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);

        // Create the Opp Line Item with required fields
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=testOpp.Id, Quantity=1, TotalPrice=99, Quantity__c=1);
        insert oli;
        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbe.Id, oliex.PriceBookEntryId);

        opp = [SELECT Name, Product_Count__c FROM Opportunity WHERE Id = :testOpp.Id];

        // Force the button press method top run
        String agreementId = bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(testOpp.Id, Integer.valueOf(opp.Product_Count__c));

        // Assert it doesn't return null for the Agreement Id
        System.assertNotEquals(agreementId, null);
                
        Order testAgreement = [SELECT Id, EffectiveDate, Signature_Authorised__c FROM Order Where OpportunityID =: testOpp.Id Limit 1];
        // Assert that an Agreement has been created
        System.assertNotEquals(testAgreement, null);

        // Update the Agreement values to trigger the parent Opp to update
        testAgreement.Signature_Authorised__c = TRUE;
        testAgreement.EffectiveDate = System.today();
        testAgreement.AccountId = acct.Id;
        update testAgreement;

        testOpp = [SELECT Id, StageName, CloseDate, Commencement_Date__c FROM Opportunity WHERE Id = :testOpp.Id LIMIT 1];

        // Assert that the parent Opp has been closed and has had date fields set
        System.assertEquals(testOpp.StageName, 'Contract Won');
        System.assertEquals(testOpp.CloseDate, System.today());
        System.assertEquals(testOpp.Commencement_Date__c, testAgreement.EffectiveDate);
    }
}