/**
 *  Class Name: LeadReallocationController  
 *  Description: This is a Class on LeadReallocation.
 *  Company: Standav
 *  CreatedDate:27/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             27-10-2017                 Original version
 */
public class LeadReallocationController {

    public static String leadId {
        get;
        set;
    }
    public static String message {
        get;
        set;
    }
     public LeadReallocationController(){
        leadId = ApexPages.currentPage().getParameters().get('id');
    }

    //Send to Sales Field for Aura Component SendtoFieldSalesLeadComponent
    @auraEnabled
    public static String reallocateLead(String leadIdAura) {

        leadId = leadIdAura;
        System.debug('check message---' + leadId);
        leadSendtoSales();
        return message;

    }

    //Send to Outbound for Aura Component SendtoOutboundLeadComponent
    @auraEnabled
    public static String reallocateOutboundLead(String leadIdAura) {

        leadId = leadIdAura;
        System.debug('check message---' + leadId);
        leadSendtoOutbound();
        return message;

    }

    //Transfers the Lead ownerid to Field Sales 
    public static void leadSendtoSales() {
        leadId = String.escapeSingleQuotes(leadId);
        Lead leadRecord = [SELECT Id, OwnerId, FLT_Postal_Code__c, postalcode, FLT_Postal_Code__r.Name, FLT_Postal_Code__r.Sales_Rep__c, FLT_Postal_Code__r.SalesRSM__c, FLT_Postal_Code__r.Sales_Rep__r.Off_Sick_Annual_Leave__c FROM Lead WHERE Id =: leadId];


        if (leadRecord.FLT_Postal_Code__c != null) {
            if (leadRecord.FLT_Postal_Code__r.Name == null) {
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationPostCodeError));
                message = Label.LeadReallocationPostCodeError;
            } else if (leadRecord.FLT_Postal_Code__r.Sales_Rep__c == null && leadRecord.FLT_Postal_Code__r.SalesRSM__c != null) {
                leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.SalesRSM__c;
                leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.SalesRSM__c;
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                message = Label.LeadReallocationSuccess;

            } else if (leadRecord.FLT_Postal_Code__r.Sales_Rep__c == null && leadRecord.FLT_Postal_Code__r.SalesRSM__c == null) {
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationFieldSalesSRMError));

                message = Label.LeadReallocationFieldSalesSRMError;
            } else if (leadRecord.FLT_Postal_Code__r.Sales_Rep__c != null) {
                if (leadRecord.FLT_Postal_Code__r.Sales_Rep__r.Off_Sick_Annual_Leave__c != true) {
                    leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.Sales_Rep__c;
                    leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.Sales_Rep__c;
                    if (ApexPages.currentPage() != null)
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                    message = Label.LeadReallocationSuccess;
                } else if (leadRecord.FLT_Postal_Code__r.Sales_Rep__r.Off_Sick_Annual_Leave__c == true) {
                    if (leadRecord.FLT_Postal_Code__r.SalesRSM__c != null) {
                        leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.SalesRSM__c;
                        leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.SalesRSM__c;
                        if (ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                        message = Label.LeadReallocationSuccess;
                    } else {
                        if (ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ContactAdministrator));
                        message = Label.ContactAdministrator;
                    }
                }
            }
        }

        if (leadRecord.FLT_Postal_Code__c == null && leadRecord.postalcode == null) {
            if (ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationNullPostCodeError));
            message = Label.LeadReallocationNullPostCodeError;
        } else if (leadRecord.FLT_Postal_Code__c == null) {
            if (ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationPostCodeError));

            message = Label.LeadReallocationPostCodeError;
        }
        try {
            update leadRecord;
            System.debug('check message---' + 'Success');
        } catch (System.DMLException e) {
            System.debug('check message---' + e.getDmlMessage(0));
        }

    }

    //Transfers the Lead ownerid to Outbound 
    public static void leadSendtoOutbound() {
        leadId = String.escapeSingleQuotes(leadId);
        system.debug('leadId----->' + leadId);
        Lead leadRecord = [SELECT Id, OwnerId, postalcode, FLT_Postal_Code__c, FLT_Postal_Code__r.OutboundTSAgent__c, FLT_Postal_Code__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c, FLT_Postal_Code__r.Name, FLT_Postal_Code__r.OutboundTSTeamLeader__c, FLT_Postal_Code__r.Sales_Rep__c, FLT_Postal_Code__r.OutboundTSTeamLeader__r.Off_Sick_Annual_Leave__c FROM Lead WHERE Id =: leadId];


        if (leadRecord.FLT_Postal_Code__c != null) {
            if (leadRecord.FLT_Postal_Code__r.Name == null) {
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationPostCodeError));

                message = Label.LeadReallocationPostCodeError;
            } else if (leadRecord.FLT_Postal_Code__r.OutboundTSAgent__c == null && leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c != null) {
                leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c;
                leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c;
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                message = Label.LeadReallocationSuccess;
            } else if (leadRecord.FLT_Postal_Code__r.OutboundTSAgent__c == null && leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c == null) {
                if (ApexPages.currentPage() != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationOutboundTSError));
                message = Label.LeadReallocationOutboundTSError;
            } else if (leadRecord.FLT_Postal_Code__r.OutboundTSAgent__c != null) {
                if (leadRecord.FLT_Postal_Code__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c != true) {
                    leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.OutboundTSAgent__c;
                    leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.OutboundTSAgent__c;
                    if (ApexPages.currentPage() != null)
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                    message = Label.LeadReallocationSuccess;
                } else if (leadRecord.FLT_Postal_Code__r.OutboundTSAgent__r.Off_Sick_Annual_Leave__c == true) {
                    if (leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c != null) {
                        leadRecord.OwnerId = leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c;
                        leadRecord.Owner_Name__c = leadRecord.FLT_Postal_Code__r.OutboundTSTeamLeader__c;
                        if (ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, Label.LeadReallocationSuccess));
                        message = Label.LeadReallocationSuccess;
                    } else {
                        if (ApexPages.currentPage() != null)
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.ContactAdministrator));
                        message = Label.ContactAdministrator;
                    }
                }
            }
        }
        if (leadRecord.FLT_Postal_Code__c == null && leadRecord.postalcode == null) {
            if (ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationNullPostCodeError));
            message = Label.LeadReallocationNullPostCodeError;
        } else if (leadRecord.FLT_Postal_Code__c == null) {
            if (ApexPages.currentPage() != null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Label.LeadReallocationPostCodeError));
            message = Label.LeadReallocationPostCodeError;

        }
        try {
            update leadRecord;
            System.debug('check message---' + 'Success');
        } catch (System.DMLException e) {
            System.debug('check message---' + e.getDmlMessage(0));
        }
    }
}