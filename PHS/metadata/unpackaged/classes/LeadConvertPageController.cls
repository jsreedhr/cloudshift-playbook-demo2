/**
 *  @Class Name:    CreateQuoteController
 *  @Description:   This is a controller for CreateQuotePage
 *  @Company:       dQuotient
 *  CreatedDate:    06/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha                08/12/2017           		Original Version
 */
public class LeadConvertPageController {

    public Boolean sendEmail {
        get;
        set;
    }
    public Contact cont {
        get;
        set;
    }
    
      public Contact cont2 {
        get;
        set;
    }
    public Account accnt {
        get;
        set;
    }
    public Lead ld {
        get;
        set;
    }
    public User usr {
        get;
        set;
    }
    public String convertedStatus {
        get;
        set;
    }
    public Lead leadRec {
        get;
        set;
    }
    public String errorMsg {
        get;
        set;
    }
    public Boolean redirectToLead {
        get;
        set;
    }
    public Boolean redirectToContact {
        get;
        set;
    }
    public String oppName {
        get;
        set;
    }
    public string prevStatus;
    public LeadConvertPageController(ApexPages.StandardController stdController) {
        ld = new Lead();
        accnt = new Account();
        usr = new User();
        leadRec = (Lead) stdController.getRecord();
        cont2 = new Contact();
        if (leadRec != null) {
            sendEmail = true;
            leadRec = [select id, Existing_Contact__c, status, OwnerId, company, postalcode from Lead where id =: leadRec.id Limit 1];
            oppName = leadRec.company + ' - ' + leadRec.postalcode;
            //oppName = leadRec.company + ' - ' + System.today().format();
            prevStatus = leadRec.status;
            System.debug(leadRec.OwnerId);
               accnt.OwnerId = leadRec.OwnerId;
            if (accnt.OwnerId == NULL || accnt.OwnerId.getsobjecttype() == Group.SObjectType)
                accnt.OwnerId = Userinfo.getUserid();
            else
                accnt.OwnerId = leadRec.OwnerId;
            //  ld.OwnerId = leadRec.OwnerId;
            if (leadRec.Existing_Contact__c != null) {
                usr.contactid = leadRec.Existing_Contact__c;
                cont = [Select id, name, OwnerId, accountid, lastname from contact where id =: usr.contactid];
               //To Populate the account fld in convert page
                cont2.accountid = cont.accountid;
            }
        }
    }

    public void loadAction() {

        errorMsg = '';
        redirectToLead = false;
        try {
            if (leadRec.status != 'Ready for Conversion') {
                errorMsg = 'The lead must be in a status of Ready For Conversion prior to being able to convert.';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, errorMsg));
            } else {
                leadRec.status = 'Converted To Opportunity';
                update leadRec;
                System.debug('lead converted-----' + leadRec);

            }

        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: ' + e.getMessage()));
        }
        if (errorMsg == '') {
            try {
                leadRec.status = prevStatus;
                update leadRec;
                redirectToLead = true;
            } catch (Exception e) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: ' + e.getMessage()));
            }
        }
    }
    public PageReference convertLead() {
        try {
            /* 
                 
            Database.LeadConvert lc = new Database.LeadConvert();
                        lc.setLeadId(leadRec.id);
                        if(leadrec.Existing_Contact__c!=null)
                            lc.setContactId(leadrec.Existing_Contact__c);
                        if(cont!=null && cont.accountid!=null)
                            lc.setAccountId(cont.accountid);
                      lc.setConvertedStatus(convertedStatus);
                       // if(oppName==null || oppName=='')
                         //   lc.setDoNotCreateOpportunity(true);
                      //  else
                       oppName = leadRec.company +' - '+ System.today().format();
                            lc.setOpportunityName(oppName);
                     lc.setOwnerId(Userinfo.getUserid());
            */
            
            

            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(leadRec.id);
            // System.debug('chk1:'+leadRec.id);

            if (usr.contactid != NULL) {
                lc.setContactId(usr.contactid);
                cont = [Select accountid from contact where id =: usr.contactid];
            }
            
          //  System.debug(cont2.accountid);
            if (cont2.accountid != null)
                lc.setAccountId(cont2.accountid);
                
               // if(cont.accountid != null)

            /*  if(leadrec.Existing_Contact__c!=null)
        lc.setContactId(leadrec.Existing_Contact__c);
    if(cont!=null && cont.accountid!=null)
        lc.setAccountId(cont.accountid);
   lc.setConvertedStatus(convertedStatus); */



            if (oppName == null || oppName == '')
                lc.setDoNotCreateOpportunity(true);
            else
                lc.setOpportunityName(oppName);
            lc.setOwnerId(accnt.OwnerId);

            //  if (ld.OwnerId == NULL || ld.OwnerId.getsobjecttype() == Group.SObjectType)
            //  lc.setOwnerId(Userinfo.getUserid());

            // else
            //  lc.setOwnerId(ld.OwnerId);

            lc.setSendNotificationEmail(sendEmail);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            Lead leadConverted = [SELECT Name, Existing_Contact__c, ConvertedOpportunityId, ConvertedContactId,convertedaccountid   FROM Lead WHERE ConvertedContactId != null and id =: leadRec.id];
            System.debug('leadConverted::' + leadConverted);
            PageReference pageRef;
            System.debug(lc.getAccountId());
            if (oppName == null || oppName == '') {
                if (lc.getAccountId() != null)
                    pageRef = new PageReference('/' + lc.getAccountId());
                else
                pageRef = new PageReference('/' + leadConverted.convertedaccountid);
            }
            else {
                Id OpportunityId = leadConverted.ConvertedOpportunityId;
                pageRef = new PageReference('/' + OpportunityId);
            }
            return pageRef;

        } catch (Exception e) {
            errorMsg = e.getMessage();
            if (errorMsg.contains('REQUIRED_FIELD_MISSING') || errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') || errorMsg.contains('INVALID_ID_FIELD')) {
                if (errorMsg.contains('REQUIRED_FIELD_MISSING'))
                    errorMsg = errorMsg.substringBetween('REQUIRED_FIELD_MISSING, ', ': ');
                if (errorMsg.contains('INVALID_ID_FIELD'))
                    errorMsg = errorMsg.substringBetween('INVALID_ID_FIELD, ', '[]');
                if (errorMsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    errorMsg = errorMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ', '[]');
                   
                    
            }
             if (errorMsg.contains('DUPLICATE_VALUE'))
                    errorMsg = 'There is already an account available with the same Company Reg No. Accounts cannot have duplicate Company Reg No' ;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ' ' + errorMsg));
            System.debug(' Error ------------'+ errorMsg);
            return null;
        }
    }

    public PageReference cancelaction() {
        PageReference pageRef = new PageReference('/' + leadRec.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}