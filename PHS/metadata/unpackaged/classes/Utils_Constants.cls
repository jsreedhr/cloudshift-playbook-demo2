/**
 * Created by jaredwatson on 24/08/2018.
 */

public with sharing class Utils_Constants {

    //**************************** Record Type Names ***************************
    public static final String CASE_WORK_TYPE_RT_NAME = 'Work_Item';
    public static final String CASE_STANDARD_RT_NAME = 'Standard';


}