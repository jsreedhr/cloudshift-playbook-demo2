/*********************************************************************************
 * bg_PHSAccountSelection_Controller
 *
 * Controler class for the PHS account selection vf page.
 * Contains methods to build a list of PHS Accounts based on a parent account id,
 * and a method to pass the selected information over to the creation of a new case.
 *
 * Author: Cameron Stewart
 * Created: 05-05-2016
 *
 *********************************************************************************/
public with sharing class bg_PHSAccountSelection_Controller {
	public Case theCase {get; set;}
	public PageReference cancelURL {get; set;}

	public List<SelectOption> PHSAccounts{
        get{
            return bg_PHSAccountSelection_Helper.generatePHSAccountOptions(theCase.AccountId);
        }
        set;
    }

    public pageReference goToCaseCreationScreen(){
    	/*
    	 * Add any error messages to the page
    	 */
        List<String> errors = bg_PHSAccountSelection_Helper.generateErrorMessages(theCase);
        if (!errors.isEmpty())
        {
            for (String error : errors)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error);
                ApexPages.addMessage(myMsg);
            }
            return null;
        }

        PHS_Account__c phsAccount = [select Name, Service_Enabled__c, Wildebeest_Ref__c from PHS_Account__c where Id = :theCase.PHS_Account__c];
        String accountName = [select Name from Account where Id = :theCase.AccountId].Name;
        Contact contactRecord = new Contact();
        if (theCase.contactId != null)
        {
            contactRecord = [select Id, Name from Contact where Id = :theCase.contactId];
        }

        String caseId = theCase.Id == null ? '500' : theCase.Id;
        Id recordTypeId = theCase.Id == null ? theCase.RecordTypeId : null;

        PageReference newCasePageRef = bg_PHSAccountSelection_Helper.generateReturnURL(caseId, theCase.AccountId, accountName, theCase.PHS_Account__c, phsAccount.Name, caseId, recordTypeId, contactRecord.Id, contactRecord.Name);
        system.debug('newCasePageRef: ' + newCasePageRef);
        return newCasePageRef;
    }

    public pageReference forceRefresh(){
        return null;
    }

    public pageReference cancel() {
    	return cancelURL;
    }
}