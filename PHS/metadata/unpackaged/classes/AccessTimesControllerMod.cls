public without sharing class AccessTimesControllerMod {
    public string errOnPage {
        get;
        set;
    }
    public list <Access_Time__c> listPortLinks {
        get;
        set;
    }
    public list <Order> listOrder {
        get;
        set;
    }
    public string rowNo {
        get;
        set;
    }
    Public String OrderId;
    public integer noOfRows {
        get;
        set;
    }
    public List < booleanProjectWrapper > lstOfbooleanProjectWrapper {
        get;
        set;
    }
    public boolean selectAll {
        get;
        set;
    }
    public Boolean showerror {
        get;
        set;
    }
    public integer errorPos {
        get;
        set;
    }
    //constructor to grt the records

    public AccessTimesControllerMod(apexpages.standardController stdController) {
        errOnPage = '';
        listOrder = new list <Order> ();
        listPortLinks = new list <Access_Time__c> ();
        lstOfbooleanProjectWrapper = new List < booleanProjectWrapper > ();
        selectAll = false;
        noOfRows = 1;
        OrderId = apexpages.currentpage().getparameters().get('id');
        listOrder = [select ID, Opportunityid, Opportunity.Name from Order where id =: OrderId limit 1];
        listPortLinks = [select Id, Agreement__c, Day__c, From__c, To__c, PHS_Account__c,PHS_Account__r.Account_Name__c, Product_Line_Item__c, Comments__c From Access_Time__c where Agreement__c =: OrderId ORDER BY Day__c limit 4999];
        
        for (Access_Time__c obj_portlink: listPortLinks) {
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, obj_portlink));
        }
    }

    // To Create a new access time record on click of Add row button


    public Void addRow() {
        selectAll = false;
        for (integer i = 0; i < noOfRows; i++) {
            Access_Time__c objportlink = new Access_Time__c();
            objportlink.Agreement__c = OrderId;
            objportlink.From__c = '06:00';
            objportlink.To__c = '18:00';

            listPortLinks.add(objportlink);
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));
        }
    }
    
        public Void addLocation() {
        selectAll = false;
            Access_Time__c ATMon = new Access_Time__c();
            ATMon.Day__c = '1. Monday';
            ATMon.Agreement__c = OrderId;
            ATMon.From__c = '06:00';
            ATMon.To__c = '18:00';
            listPortLinks.add(ATMon);
            Access_Time__c ATTue = new Access_Time__c();
            ATTue.Day__c = '2. Tuesday';
            ATTue.Agreement__c = OrderId;
            ATTue.From__c = '06:00';
            ATTue.To__c = '18:00';
            listPortLinks.add(ATTue);
            Access_Time__c ATWed = new Access_Time__c();
            ATWed.Day__c = '3. Wednesday';
            ATWed.Agreement__c = OrderId;
            ATWed.From__c = '06:00';
            ATWed.To__c = '18:00';
            listPortLinks.add(ATWed);
            Access_Time__c ATThu = new Access_Time__c();
            ATThu.Day__c = '4. Thursday';
            ATThu.Agreement__c = OrderId;
            ATThu.From__c = '06:00';
            ATThu.To__c = '18:00';
            listPortLinks.add(ATThu);
            Access_Time__c ATFri = new Access_Time__c();
            ATFri.Day__c = '5. Friday';
            ATFri.Agreement__c = OrderId;
            ATFri.From__c = '06:00';
            ATFri.To__c = '18:00';
            listPortLinks.add(ATFri);     
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, ATMon));
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, ATTue));
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, ATWed));
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, ATThu));
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, ATFri));
        }

    public void selectAll() {

        if (selectAll == true) {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = false;
            }
        }
    }

    Public void deleteSelectedRows(){
            // selectAll = false;
            list <Access_Time__c> toDeleteRows = new list <Access_Time__c> ();
            for (integer j = (lstOfbooleanProjectWrapper.size() - 1); j >= 0; j--) {
                if (lstOfbooleanProjectWrapper[j].isSelected == true) {
                    if (lstOfbooleanProjectWrapper[j].objport.id != null) {
                        toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                    }
                    lstOfbooleanProjectWrapper.remove(j);
                }
            }
            delete toDeleteRows;
        }
        /**
        *   Method Name:    DelRow
        *   Description:    To delete the record by passing the row no.
        *   Param:  RowNo

        */

    public Void delRow() {

            selectAll = false;
            system.debug('--------' + RowNo);
            list <Access_Time__c> todelete = new list <Access_Time__c> ();

            if (lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null) {
                todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);

            }


            lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
            delete todelete;


        }
        /**
         *   Method Name:    saveAccess_Time__c
         *   Description:    To save the records and then redirect it to respective opportunity
         */

    public PageReference saveAccessTime() {
        if (showerror != TRUE) {
            List <Access_Time__c> listToUpsert = new List <Access_Time__c> ();
            Boolean isBlank = FALSE;

            for (booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper) {
                if (objportlink.objport.Agreement__c == NULL || objportlink.objport.Day__c == NULL){
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'Please ensure the fields Agreement & Day have been completed'));
                        return null;

                } else {
                    errOnPage = '';
                    listToUpsert.add(objportlink.objport);
                }
            }
            if (listToUpsert.size() > 0) {
                try {
                    Upsert listToUpsert;
                    return new PageReference('/' + OrderId);


                } catch (Exception e) {
                    e.getMessage();
                    return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'No records found'));
                return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'There is missing information on the access time records.'));
             return null;
        }

    }


    public class booleanProjectWrapper {
        public Boolean isSelected {
            get;
            set;
        }
        public Access_Time__c objport {
            get;
            set;
        }
        public integer ValuetoList {
            get;
            set;
        }

        public booleanProjectWrapper(boolean isSelect, Access_Time__c objports) {
            objport = objports;
            isSelected = isSelect;
            ValuetoList = 1;

        }
    }

}