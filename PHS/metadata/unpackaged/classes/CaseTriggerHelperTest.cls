/*
*  Class Name:  CaseTriggerHelperTest
*  Description: Test class for CaseTriggerHelper
*  Company: Standav
*  CreatedDate: 24-Aug-2018
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  24-Aug-2017            Orginal Version
*/
@isTest
public class CaseTriggerHelperTest {
    
    static Account testAccount;
    static Contact testContact;
    static Contact testContactWithNoAcc;
    static PHS_Account__c testPHSAccount;
    static Case testCaseWithPhs1;
    static Case testCaseWithPhs2;
    static final RecordType RT_WORK_ITEM = [select id from RecordType where DeveloperName = :Utils_Constants.CASE_WORK_TYPE_RT_NAME];
    static final RecordType RT_CASE = [select id from RecordType where DeveloperName = :Utils_Constants.CASE_STANDARD_RT_NAME];
    
    @isTest
    private static void createData(){
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        globalSettings.Can_Edit_Closed_Cases__c = TRUE;
        update globalSettings;
        
        testAccount = bg_Test_Data_Utils.createAccount('testAccountCCCT');
        insert testAccount;
        
        testContact = bg_Test_Data_Utils.createContact('testContactCCCT', testAccount.id);
        testContact.email='test123@e2ctest3.com';
        insert testContact;
        
        testContactWithNoAcc = new Contact(LastName='WithNoAcc', 
                                           email='test123@No.email.com');
        insert testContactWithNoAcc;
        
        
        testPHSAccount = bg_Test_Data_Utils.createPHSAccount('1234', testAccount.Id);
        insert testPHSAccount;
          
        testCaseWithPhs1 = bg_Test_Data_Utils.createCase('CaseTest', testContact.Id,testAccount.Id);
        testCaseWithPhs1.Status = 'Open';  
        testCaseWithPhs1.PHS_Account__c=testPHSAccount.Id;
        testCaseWithPhs1.Type = 'Cancellation';
        testCaseWithPhs1.SuppliedEmail='test123@e2ctest3.com';
        insert testCaseWithPhs1;
        testCaseWithPhs2 = bg_Test_Data_Utils.createCase('CaseTest', testContactWithNoAcc.Id,testAccount.Id);
        testCaseWithPhs2.Status = 'Open';  
        testCaseWithPhs2.PHS_Account__c=testPHSAccount.Id;
        insert testCaseWithPhs2;
        testCaseWithPhs2.Type = 'Cancellation';
        update testCaseWithPhs2;
        
    }
    
    
    @isTest
    private static void deleteCaseWithWorkTypeRT(){
        createData();
        
        Case caseWT = bg_Test_Data_Utils.createCase('CaseWT', testContact.Id,testAccount.Id);
        caseWT.Status = 'Open';
        caseWT.Subject = 'test work type subject';
        caseWT.PHS_Account__c = testPHSAccount.Id;
        caseWT.RecordTypeId = RT_WORK_ITEM.Id;
        insert caseWT;
        
        Test.startTest();
        
        List<Work_Item_Deletion_History__c> lstWTdh = [SELECT id from Work_Item_Deletion_History__c];
        System.assertEquals(0, lstWTdh.size());
        
        delete caseWT;
        
        Test.stopTest();
        
        List<Work_Item_Deletion_History__c> lstWTdh2 = [SELECT id, Status__c, Subject__c from Work_Item_Deletion_History__c];
        System.assertEquals(1, lstWTdh2.size());
        System.assertEquals('Open', lstWTdh2[0].Status__c);
        
        
        
    }
    
    @isTest
    private static void deleteCaseWithoutWorkTypeRT(){
        createData();
        
        Case caseWT = bg_Test_Data_Utils.createCase('CaseWT', testContact.Id,testAccount.Id);
        caseWT.Status = 'Open';
        caseWT.Subject = 'test work type subject';
        caseWT.PHS_Account__c = testPHSAccount.Id;
        caseWT.RecordTypeId = RT_CASE.Id;
        insert caseWT;
        
        Test.startTest();
        
        List<Work_Item_Deletion_History__c> lstWTdh = [SELECT id from Work_Item_Deletion_History__c];
        System.assertEquals(0, lstWTdh.size());
        
        delete caseWT;
        
        Test.stopTest();
        
        List<Work_Item_Deletion_History__c> lstWTdh2 = [SELECT id, Status__c, Subject__c from Work_Item_Deletion_History__c];
        System.assertEquals(0, lstWTdh2.size());
        
    }
    
    @isTest
    private static void autoCreateContFrmEmailandValidateCaseCloseLightning(){
        createData();
        CaseTriggerHelper.isInSalesforce1 = true;
        Case testCaseWithNoAcc = bg_Test_Data_Utils.createCase('CaseTest', testContactWithNoAcc.Id,testAccount.Id);
        testCaseWithNoAcc.Status = 'Open';  
        insert testCaseWithNoAcc;
        
        
        Case testCaseDuplicate = bg_Test_Data_Utils.createCase('CaseTest', testContact.Id,testAccount.Id);
        testCaseDuplicate.Status = 'Open'; 
        testCaseDuplicate.SuppliedEmail='test123@e2ctest4.com';
        testCaseDuplicate.PHS_Account__c=testPHSAccount.Id;
        testCaseDuplicate.Type = 'Cancellation';
        testCaseDuplicate.SuppliedName = 'tLastNm,tFirstNm';
        testCaseDuplicate.SuppliedPhone ='98989898';
        
        testCaseWithPhs1.Origin='Email';
        testCaseWithPhs1.status='Closed';
        testCaseWithPhs1.SuppliedName = 'tLastNm,tFirstNm';
        testCaseWithPhs1.SuppliedPhone ='98989898';
        
        testCaseWithNoAcc.status='Closed';
        Test.startTest();
        try{
            update testCaseWithNoAcc;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Case cannot be closed,') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
        try{
            update testCaseWithPhs1;
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Case cannot be closed,') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        } 
        try{
            insert testCaseDuplicate;            
        }catch(Exception e){
            System.assertNotEquals(null, e);
        }
        Test.stopTest();
    }
    
    @isTest
    private static void autoCreateContFrmEmailandValidateCaseCloseClassic(){
        createData();
        Case testCaseDuplicate = bg_Test_Data_Utils.createCase('CaseTest', testContact.Id,testAccount.Id);
        testCaseDuplicate.Status = 'Open'; 
        testCaseDuplicate.SuppliedEmail='test123@e2ctest4.com';
        testCaseDuplicate.PHS_Account__c=testPHSAccount.Id;
        testCaseDuplicate.Type = 'Cancellation';
        testCaseDuplicate.SuppliedName = 'tLastNm,tFirstNm';
        testCaseDuplicate.SuppliedPhone ='98989898';
        Test.startTest();
        try{
            insert testCaseDuplicate;            
        }catch(Exception e){
            System.assertNotEquals(null, e);
        }
        Test.stopTest();
        
    }
    
    
    
    
    
}