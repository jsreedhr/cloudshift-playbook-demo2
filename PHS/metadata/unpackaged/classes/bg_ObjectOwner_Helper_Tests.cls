/*********************************************************************************
 * bg_ObjectOwner_Helper_Tests
 *
 * Test class to verify that the ObjectOwner Helper class is working correctly
 * Tests changing the Owner for Lead, Opportunity, Event, Task
 * 
 * Author: Alex Leslie- BrightGen Ltd
 * Created: 23-03-2016
 *
 *********************************************************************************/

@isTest
private class bg_ObjectOwner_Helper_Tests {
	private static Integer bulkFactor = 100;

	/*
	 *  Test that when a lead is created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createLeadNoEmployeeTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.LEAD_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Lead leadRecord = bg_Test_Data_Utils.createLead('1');
		leadRecord.OwnerId = UserInfo.getUserId();
		insert leadRecord;

		leadRecord = [SELECT OwnerId FROM Lead WHERE Id =: leadRecord.Id];

		// Asserts
		System.assertEquals(UserInfo.getUserId(), leadRecord.OwnerId);
	}

	/*
	 *  Test that when a lead is created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createLeadEmployeeTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.LEAD_OBJECTOWNER_NAME);
		insert objOwnerMap;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];


		Lead leadRecord = bg_Test_Data_Utils.createLead('1');
		leadRecord.OwnerId = UserInfo.getUserId();
		leadRecord.Employee_Number__c = usr.Employee_Number__c;
		insert leadRecord;

		leadRecord = [SELECT OwnerId FROM Lead WHERE Id =: leadRecord.Id];

		// Asserts
		System.assertEquals(usr.Id, leadRecord.OwnerId);
	}

	/*
	 *  Test that when bulk leads are created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createLeadNoEmployeeBulkTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.LEAD_OBJECTOWNER_NAME);
		insert objOwnerMap;
		List<Lead> leadsToInsert = new List<Lead>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Lead leadRecord = bg_Test_Data_Utils.createLead(String.valueOf(i));
			leadRecord.OwnerId = UserInfo.getUserId();
			leadsToInsert.add(leadRecord);
		}
		
		insert leadsToInsert;

		List<Lead> leadRecords = [SELECT OwnerId FROM Lead WHERE OwnerId =: UserInfo.getUserId()];

		// Asserts
		System.assertEquals(bulkFactor, leadRecords.size());
	}

	/*
	 *  Test that when bulk leads are created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createLeadEmployeeBulkTestSameEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.LEAD_OBJECTOWNER_NAME);
		insert objOwnerMap;
		
		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('2345');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];


		List<Lead> leadsToInsert = new List<Lead>();

		for (Integer i = 0; i < bulkFactor; i++){
			Lead leadRecord = bg_Test_Data_Utils.createLead(String.valueOf(i));
			leadRecord.OwnerId = UserInfo.getUserId();
			leadRecord.Employee_Number__c = usr.Employee_Number__c;
			leadsToInsert.add(leadRecord);
		}

		insert leadsToInsert;

		List<Lead> leadRecords = [SELECT Id FROM Lead WHERE OwnerId =: usr.Id];

		// Asserts
		System.assertEquals(bulkFactor, leadRecords.size());
	}

	/*
	 *  Test that when bulk leads are created and an employee number is specified
	 * that the owner changes to the specified employee
	 */
    static testMethod void createLeadEmployeeBulkTestDifferentEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.LEAD_OBJECTOWNER_NAME);
		insert objOwnerMap;

		List<Lead> leadsToInsert = new List<Lead>();
		List<User> usersToInsert = new List<User>();
		List<Id> userIds = new List<Id>();

		for (Integer i =0; i < bulkFactor; i++){
			User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber(String.valueOf(i));
			usersToInsert.add(usr);
		}


		insert usersToInsert;

		for (User usr : usersToInsert){
			userIds.add(usr.Id);
		}

		usersToInsert = [SELECT Id, Employee_Number__c FROM User WHERE Id IN:userIds];
		Integer i=0;
		for (User usr : usersToInsert){
			Lead leadRecord = bg_Test_Data_Utils.createLead(String.valueOf(i));
			leadRecord.OwnerId = UserInfo.getUserId();
			leadRecord.Employee_Number__c = usr.Employee_Number__c;
			leadsToInsert.add(leadRecord);
			i++;
		}

		insert leadsToInsert;

		
		System.debug('**** BG **** userIds:' + userIds);

		List<Lead> leadRecords = [SELECT Id FROM Lead WHERE OwnerId IN :userIds];

		// Asserts
		System.assertEquals(bulkFactor, leadRecords.size());
	}

	/*
	 *  Test that when a lead is created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createOpportunityNoEmployeeTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('1');
		insert accRecord;

		Opportunity oppRecord = bg_Test_Data_Utils.createOpportunity('1', accRecord.Id);
		oppRecord.OwnerId = UserInfo.getUserId();
		insert oppRecord;

		oppRecord = [SELECT OwnerId FROM Opportunity WHERE Id =: oppRecord.Id];

		// Asserts
		System.assertEquals(UserInfo.getUserId(), oppRecord.OwnerId);
	}

	/*
	 *  Test that when a lead is created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createOpportunityEmployeeTest(){

    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME);
		insert objOwnerMap;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		Opportunity oppRecord = bg_Test_Data_Utils.createOpportunity('23456', accRecord.Id);
		oppRecord.OwnerId = UserInfo.getUserId();
		oppRecord.Employee_Number__c = usr.Employee_Number__c;
		insert oppRecord;

		oppRecord = [SELECT OwnerId FROM Opportunity WHERE Id =: oppRecord.Id];

		// Asserts
		System.assertEquals(usr.Id, oppRecord.OwnerId);
	}

	/*
	 *  Test that when bulk opportunity are created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createOpportunityNoEmployeeBulkTest(){
		
    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		List<Opportunity> oppsToInsert = new List<Opportunity>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Opportunity oppRecord = bg_Test_Data_Utils.createOpportunity('23456', accRecord.Id);
			oppRecord.OwnerId = UserInfo.getUserId();
			oppsToInsert.add(oppRecord);
		}
		
		insert oppsToInsert;

		List<Opportunity> oppsRecords = [SELECT OwnerId FROM Opportunity WHERE OwnerId =: UserInfo.getUserId()];

		// Asserts
		System.assertEquals(bulkFactor, oppsRecords.size());
	}

	/*
	 *  Test that when bulk opportunity are created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createOpportunityEmployeeBulkTestSameEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;
		
		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('2345');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];


		List<Opportunity> oppsToInsert = new List<Opportunity>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Opportunity oppRecord = bg_Test_Data_Utils.createOpportunity('23456', accRecord.Id);
			oppRecord.OwnerId = UserInfo.getUserId();
			oppRecord.Employee_Number__c = usr.Employee_Number__c;
			oppsToInsert.add(oppRecord);
		}

		insert oppsToInsert;

		List<Opportunity> oppsRecords = [SELECT Id FROM Opportunity WHERE OwnerId =: usr.Id];

		// Asserts
		System.assertEquals(bulkFactor, oppsRecords.size());
	}

	/*
	 *  Test that when bulk opportunities are created and an employee number is specified
	 * that the owner changes to the specified employee
	 */
    static testMethod void createOpportunityEmployeeBulkTestDifferentEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.OPPORTUNITY_OBJECTOWNER_NAME);
		insert objOwnerMap;

		List<Opportunity> oppsToInsert = new List<Opportunity>();
		List<User> usersToInsert = new List<User>();
		List<Id> userIds = new List<Id>();

		for (Integer i =0; i < bulkFactor; i++){
			User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber(String.valueOf(i));
			usersToInsert.add(usr);
		}

		insert usersToInsert;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		for (User usr : usersToInsert){
			userIds.add(usr.Id);
		}

		usersToInsert = [SELECT Id, Employee_Number__c FROM User WHERE Id IN:userIds];
		Integer i=0;

		for (User usr : usersToInsert){
			Opportunity oppRecord = bg_Test_Data_Utils.createOpportunity('23456', accRecord.Id);
			oppRecord.OwnerId = UserInfo.getUserId();
			oppRecord.Employee_Number__c = usr.Employee_Number__c;
			oppsToInsert.add(oppRecord);
			i++;
		}

		insert oppsToInsert;

		
		System.debug('**** BG **** userIds:' + userIds);

		List<Opportunity> oppsRecords = [SELECT Id FROM Opportunity WHERE OwnerId IN :userIds];

		// Asserts
		System.assertEquals(bulkFactor, oppsRecords.size());
	}

	/*
	 *  Test that when a event is created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createEventNoEmployeeTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.EVENT_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		Event evtRecord = bg_Test_Data_Utils.createEvent('Test', accRecord.Id);
		evtRecord.OwnerId = UserInfo.getUserId();
		insert evtRecord;

		evtRecord = [SELECT OwnerId FROM Event WHERE Id =: evtRecord.Id];

		// Asserts
		System.assertEquals(UserInfo.getUserId(), evtRecord.OwnerId);
	}

	/*
	 *  Test that when a event is created and an employee number is specified
	 * that the owner changes to the employee
	 */
    static testMethod void createEventEmployeeTest(){

    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.EVENT_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];

		Event evtRecord = bg_Test_Data_Utils.createEvent('Test', accRecord.Id);
		evtRecord.OwnerId = UserInfo.getUserId();
		evtRecord.Employee_Number__c = usr.Employee_Number__c;
		insert evtRecord;

		evtRecord = [SELECT OwnerId FROM Event WHERE Id =: evtRecord.Id];

		// Asserts
		System.assertEquals(usr.Id, evtRecord.OwnerId);

	}

	/*
	 *  Test that when bulk event are created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createEventNoEmployeeBulkTest(){
		
    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.EVENT_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		List<Event> evtsToInsert = new List<Event>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Event evtRecord = bg_Test_Data_Utils.createEvent('Test', accRecord.Id);
			evtRecord.OwnerId = UserInfo.getUserId();
			evtsToInsert.add(evtRecord);
		}
		
		insert evtsToInsert;

		List<Event> eventRecords = [SELECT OwnerId FROM Event WHERE OwnerId =: UserInfo.getUserId()];

		// Asserts
		System.assertEquals(bulkFactor, eventRecords.size());
	}

	/*
	 *  Test that when bulk event are created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createEventEmployeeBulkTestSameEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.EVENT_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];


		List<Event> evtsToInsert = new List<Event>();

		for (Integer i = 0; i < bulkFactor; i++){
			Event evtRecord = bg_Test_Data_Utils.createEvent('Test', accRecord.Id);
			evtRecord.OwnerId = UserInfo.getUserId();
			evtRecord.Employee_Number__c = usr.Employee_Number__c;
			evtsToInsert.add(evtRecord);
		}

		insert evtsToInsert;

		List<Event> eventRecords = [SELECT OwnerId FROM Event WHERE OwnerId =: usr.Id];
		// Asserts
		System.assertEquals(bulkFactor, eventRecords.size());
	}

	/*
	 *  Test that when bulk events are created and an employee number is specified
	 * that the owner changes to the specified employee
	 */
    static testMethod void createEventEmployeeBulkTestDifferentEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.EVENT_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		List<Event> evtsToInsert = new List<Event>();
		List<User> usersToInsert = new List<User>();
		List<Id> userIds = new List<Id>();

		for (Integer i =0; i < bulkFactor; i++){
			User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber(String.valueOf(i));
			usersToInsert.add(usr);
		}

		insert usersToInsert;

		for (User usr : usersToInsert){
			userIds.add(usr.Id);
		}

		usersToInsert = [SELECT Id, Employee_Number__c FROM User WHERE Id IN:userIds];
		Integer i=0;

		for (User usr : usersToInsert){
			Event evtRecord = bg_Test_Data_Utils.createEvent('Test', accRecord.Id);
			evtRecord.OwnerId = UserInfo.getUserId();
			evtRecord.Employee_Number__c = usr.Employee_Number__c;
			evtsToInsert.add(evtRecord);
			i++;
		}

		insert evtsToInsert;

		
		System.debug('**** BG **** userIds:' + userIds);

		List<Event> evtsRecords = [SELECT Id FROM Event WHERE OwnerId IN :userIds];

		// Asserts
		System.assertEquals(bulkFactor, evtsRecords.size());
	}

	/*
	 *  Test that when a task is created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createTaskNoEmployeeTest(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.TASK_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		Task tskRecord = bg_Test_Data_Utils.createTask('Test', accRecord.Id);
		tskRecord.OwnerId = UserInfo.getUserId();
		insert tskRecord;

		tskRecord = [SELECT OwnerId FROM Task WHERE Id =: tskRecord.Id];

		// Asserts
		System.assertEquals(UserInfo.getUserId(), tskRecord.OwnerId);
	}

	/*
	 *  Test that when a task is created and an employee number is specified
	 * that the owner changes to the employee
	 */
    static testMethod void createTaskEmployeeTest(){

    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.TASK_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];

		Task tskRecord = bg_Test_Data_Utils.createTask('Test', accRecord.Id);
		tskRecord.OwnerId = UserInfo.getUserId();
		tskRecord.Employee_Number__c = usr.Employee_Number__c;
		insert tskRecord;

		tskRecord = [SELECT OwnerId FROM Task WHERE Id =: tskRecord.Id];

		// Asserts
		System.assertEquals(usr.Id, tskRecord.OwnerId);

	}

	/*
	 *  Test that when bulk tasks are created without specifying an employee number
	 * that the owner reamins the same
	 */
    static testMethod void createTaskNoEmployeeBulkTest(){
		
    	Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.TASK_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		List<Task> tsksToInsert = new List<Task>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Task tskRecord = bg_Test_Data_Utils.createTask('Test', accRecord.Id);
			tskRecord.OwnerId = UserInfo.getUserId();
			tsksToInsert.add(tskRecord);
		}
		
		insert tsksToInsert;

		List<Task> eventRecords = [SELECT OwnerId FROM Task WHERE OwnerId =: UserInfo.getUserId()];

		// Asserts
		System.assertEquals(bulkFactor, eventRecords.size());
	}

	/*
	 *  Test that when bulk task are created and an employee number is specified
	 * that the owner rchanges to the employee
	 */
    static testMethod void createTaskEmployeeBulkTestSameEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.TASK_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber('23456');
		insert usr;

		usr = [SELECT Id, Employee_Number__c FROM User WHERE Id =: usr.Id];

		List<Task> tsksToInsert = new List<Task>();
		
		for (Integer i = 0; i < bulkFactor; i++){
			Task tskRecord = bg_Test_Data_Utils.createTask('Test', accRecord.Id);
			tskRecord.OwnerId = UserInfo.getUserId();
			tskRecord.Employee_Number__c = usr.Employee_Number__c;
			tsksToInsert.add(tskRecord);
		}

		insert tsksToInsert;

		List<Task> tskRecords = [SELECT OwnerId FROM Task WHERE OwnerId =: usr.Id];
		// Asserts
		System.assertEquals(bulkFactor, tskRecords.size());
	}

	/*
	 *  Test that when bulk events are created and an employee number is specified
	 * that the owner changes to the specified employee
	 */
    static testMethod void createTaskEmployeeBulkTestDifferentEmployee(){
		Object_Owner_Mapping__c objOwnerMap = bg_Test_Data_Utils.createObjectOwnerMapping(bg_Constants.TASK_OBJECTOWNER_NAME);
		insert objOwnerMap;

		Account accRecord = bg_Test_Data_Utils.createAccount('23456');
		insert accRecord;

		List<Task> tsksToInsert = new List<Task>();
		List<User> usersToInsert = new List<User>();
		List<Id> userIds = new List<Id>();

		for (Integer i =0; i < bulkFactor; i++){
			User usr = bg_Test_Data_Utils.createUserWithEmployeeNumber(String.valueOf(i));
			usersToInsert.add(usr);
		}

		insert usersToInsert;

		for (User usr : usersToInsert){
			userIds.add(usr.Id);
		}

		usersToInsert = [SELECT Id, Employee_Number__c FROM User WHERE Id IN:userIds];
		Integer i=0;

		for (User usr : usersToInsert){
			Task tskRecord = bg_Test_Data_Utils.createTask('Test', accRecord.Id);
			tskRecord.OwnerId = UserInfo.getUserId();
			tskRecord.Employee_Number__c = usr.Employee_Number__c;
			tsksToInsert.add(tskRecord);
			i++;
		}

		insert tsksToInsert;

		
		System.debug('**** BG **** userIds:' + userIds);

		List<Task> tskRecords = [SELECT Id FROM Task WHERE OwnerId IN :userIds];

		// Asserts
		System.assertEquals(bulkFactor, tskRecords.size());
	}
}