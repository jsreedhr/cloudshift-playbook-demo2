/**********************************************************************
* bg_CaseCommentHelper_Tests:
*
* Helper Class for the Case Comments
*
* Created By: Kash Hussain (BrightGen)
* Created Date: 13-02-2017 
* Changes: 
***********************************************************************/
@isTest
private class bg_CaseCommentHelper_Tests
{
    static testMethod void testFirstCaseCommentCompletesFirstResponseMileSTone()
    {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        integrationSetting.Integration_Log_Purge_Limit__c = 5;
        
        if(integrationSetting == null)
        {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            insert integrationSetting;
        }

        Account testAcct = bg_Test_Data_Utils.createAccount('Test');
        insert testAcct;

        Contact testContact = bg_Test_Data_Utils.createContact('Test C', testAcct.Id);
        testContact.Email = 'test@milestone.com';
        insert testContact;

        Case testCase = new Case(AccountId = testAcct.Id, ContactId = testContact.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
        insert testCase;

        List<CaseMilestone> cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];
        
        for (CaseMilestone cMilestone : cMilestones)
        {
            system.assertEquals(FALSE, cMilestone.IsCompleted);
        }

        CaseComment caseCom = bg_Test_Data_Utils.createCaseComment('Test Comment', testCase.Id);
        insert caseCom;        

        cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];

        for (CaseMilestone cMilestone : cMilestones)
        {
            if (cMilestone.MilestoneType.Name == 'First Response')
            {
                system.assertEquals(TRUE, cMilestone.IsCompleted);
            }
            else 
            {
                system.assertEquals(FALSE, cMilestone.IsCompleted);
            }
        }
    }

    static testMethod void testSecondCaseCommentCompletesSecondResponseMileStone()
    {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        integrationSetting.Integration_Log_Purge_Limit__c = 5;
        
        if(integrationSetting == null)
        {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            insert integrationSetting;
        }

        Account testAcct = bg_Test_Data_Utils.createAccount('Test');
        insert testAcct;

        Contact testContact = bg_Test_Data_Utils.createContact('Test C', testAcct.Id);
        testContact.Email = 'test@milestone.com';
        insert testContact;

        Case testCase = new Case(AccountId = testAcct.Id, ContactId = testContact.Id, Subject = 'Test', Status = 'Open', Origin = 'Email');
        insert testCase;

        List<CaseMilestone> cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];
        
        for (CaseMilestone cMilestone : cMilestones)
        {
            system.assertEquals(FALSE, cMilestone.IsCompleted);
        }

        CaseComment caseComA = bg_Test_Data_Utils.createCaseComment('Test Comment', testCase.Id);
        insert caseComA;        

        caseComA = [SELECT Id, CreatedDate FROM CaseComment WHERE Id = :caseComA.Id];

        CaseComment caseComB = bg_Test_Data_Utils.createCaseComment('Test Comment2', testCase.Id);
        insert caseComB;        

        cMilestones = [SELECT Id, MilestoneType.Name, CaseId, CompletionDate, IsCompleted FROM CaseMilestone WHERE CaseId = : testCase.Id];

        for (CaseMilestone cMilestone : cMilestones)
        {
            if (cMilestone.MilestoneType.Name == 'First Response')
            {
                system.assertEquals(TRUE, cMilestone.IsCompleted);
                system.assertEquals(caseComA.CreatedDate, cMilestone.CompletionDate);
            }
            else if (cMilestone.MilestoneType.Name == 'Second Response')
            {
                system.assertEquals(TRUE, cMilestone.IsCompleted);   
            }
            else 
            {
                system.assertEquals(FALSE, cMilestone.IsCompleted);
            }
        }
    }
}