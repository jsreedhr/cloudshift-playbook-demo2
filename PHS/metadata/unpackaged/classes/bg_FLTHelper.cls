/*********************************************************************************
 * bg_FLTHelper
 *
 * A helper class for the FLT object.
 * 
 * Author: Kash Hussain (BrightGen Ltd)
 * Created: 04-07-2016
 *
 *********************************************************************************/

public with sharing class bg_FLTHelper
{   
    /*
        Method to determine the Outcode from a given Postcode.
    */
    public static String determineOutcode(String postcode)
    {    
        if(postcode != null && postcode.contains(' '))
        {
			postcode = 	postcode.replaceAll('\\s+', '');
            String district = postcode.substring(0, postcode.length() - 3);        
            return district;
        }    
        else
        {
            String district = postcode.substring(0, postcode.length() - 3);        
          	return district;   
        }
    }
}