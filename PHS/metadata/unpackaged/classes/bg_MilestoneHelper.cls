/*************************************************
* bg_MilestoneHelper
*
* Helper class for the Case Milestones
*
* Author: Kash Hussain (BrightGen)
* Created Date: 04/02/2017
* Updates: 
**************************************************/
public with sharing class bg_MilestoneHelper
{
	/*
		Method to mark the First and Second Response Milestones as Completed (driven by
		Case Comments and Email Messages)
	*/
	public static void completeResponseMilestones(List<Id> caseIds, DateTime complDate)
	{
		List<CaseMilestone> cmsToUpdate = [SELECT Id, CompletionDate FROM CaseMilestone WHERE CaseId IN :caseIds AND (MilestoneType.Name = 'First Response' OR MilestoneType.Name = 'Second Response') AND CompletionDate = null LIMIT 1];
		
		if (!cmsToUpdate.isEmpty())
		{
			List<CaseMilestone> milestonesToUpdate = new List<CaseMilestone>();

			for (CaseMilestone cm : cmsToUpdate)
			{
				CaseMilestone cMilestone = new CaseMilestone();
				cMilestone.Id = cm.Id;
				cMilestone.CompletionDate = complDate;
				milestonesToUpdate.add(cMilestone);
			}	
			
			if (!milestonesToUpdate.isEmpty())
			{
				update milestonesToUpdate;
			}
		}
	}

	/*
		Method to mark all outstanding milestones (First Response, Second Response, Resolution)
		as completed if not all ready completed
	*/
	public static void completeAllMilestones(Set<Id> caseIDs)
	{
        List<CaseMilestone> caseMilestoneToUpdate = new List<CaseMilestone>();

        for (CaseMilestone caseMilestone : [SELECT Id, completionDate, MilestoneType.Name, CaseId FROM CaseMilestone WHERE caseId IN :caseIDs and completionDate = null])
        {
            Boolean isFirstResponse = caseMilestone.MilestoneType.Name == 'First Response';
            Boolean isSecondResponse = caseMilestone.MilestoneType.Name == 'Second Response';
            Boolean isResolution = caseMilestone.MilestoneType.Name == 'Resolution';
            
            if (isFirstResponse || isSecondResponse || isResolution)
            {
                caseMilestone.CompletionDate = datetime.now();
                caseMilestoneToUpdate.add(caseMilestone);
            }
        }

        if (!caseMilestoneToUpdate.isEmpty())
        {
            update caseMilestoneToUpdate;
        }
	}
}