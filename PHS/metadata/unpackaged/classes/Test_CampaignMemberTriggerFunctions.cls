/**
 *  @Class Name:    Test_CampaignMemberTriggerFunctions
 *  @Description:   This is a test class for CampaignMemberTriggerFunctions
 *  @Company:       
 *  CreatedDate:    01/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *                      01/11/2017                  Original Version
 * Nitha T S            09/ 11/ 2017                Last Modified
 */
@isTest 
private class Test_CampaignMemberTriggerFunctions{
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
       Pricebook2 spb = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true
                             ); 
        update spb;
       // insert standardPricebook;   
      // pricebook2 SPB = [SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true];
        
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        insert lead;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
        
        Product2 productObj = bg_Test_Data_Utils.createaProduct2('Test Product');
        productObj.IsActive = true;
        productObj.Target_Price__c =12;
        insert productObj;
        
         /*PricebookEntry standardPricebook = new PricebookEntry(
                 // Id = Test.getStandardPricebookId(),
                 Product2Id = productObj.id, 
                 Pricebook2Id = spb.id,
                  IsActive = true,
                 UnitPrice=10
                  );
        //update
        insert standardPricebook;  */
        
        
        //PricebookEntry priceBE = bg_Test_Data_Utils.createPricebookEntry(productObj.Id, standardPricebook.Id);
        //insert priceBE;
        
        Campaign_Product__c campaignproductObj = bg_Test_Data_Utils.createCampaignProduct(camp.Id, productObj.Id);
        insert campaignproductObj;
        
        Lead_Product__c leadproduct= bg_Test_Data_Utils.createLeadProduct(camp.Id, productObj.Id, lead.Id);
        insert leadproduct;
        
    }
    
    @isTest
    static void testCampaignMember()
    {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead lead = [Select Id, FLT_Postal_Code__c, OwnerId, Campaigns__c from Lead where OwnerId= :u.Id];
        Campaign camp = [Select Id, Name, IsActive From Campaign Where Name = 'Test Campaign'];
        Campaign_Product__c campaignproductObj = [Select Id, Campaign__c From Campaign_Product__c Where Campaign__c = :camp.Id];
        Lead_Product__c leadproduct= [Select Id, Campaign__c From Lead_Product__c Where Campaign__c = :camp.Id];        
        CampaignMember objCampaignMember = bg_Test_Data_Utils.createCampaignMember(lead.id, camp.id);
        
        Test.startTest();     
            insert objCampaignMember;
            system.assertEquals(leadproduct.Campaign__c, campaignproductObj.Campaign__c);
            delete objCampaignMember;
            System.assertEquals(0,[Select Id From CampaignMember].size());
        Test.stopTest();
    }
}