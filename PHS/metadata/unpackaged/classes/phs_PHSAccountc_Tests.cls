@isTest
private class phs_PHSAccountc_Tests {

    static testMethod void testAddNewPHSAccountToAccount() {
        // TO DO: implement unit test       
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;
          
    }
    
    static testMethod void testAddNewPHSAccountToAccountWithExistingRef() {
        // TO DO: implement unit test       
        Account a = bg_Test_Data_Utils.createAccount('test');
        a.Wildebeest_Ref__c = '999';
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;
          
    }
    
    static testMethod void testMaintainAccountToPHSAccountRelationship() {
        // TO DO: implement unit test
        test.StartTest();
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;
        Account b = bg_Test_Data_Utils.createAccount('test2');
        insert b;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;
        
        phsAccount.Salesforce_Account__c = b.Id;
        update phsAccount;
            
        test.stoptest();
    }
    
    static testMethod void testMaintainAccountToPHSAccountRelationshipWithExistingRef() {
        // TO DO: implement unit test
        test.StartTest();
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;
        Account b = bg_Test_Data_Utils.createAccount('test2');
        b.Wildebeest_ref__c = '999';
        insert b;
        
        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;
        
        phsAccount.Salesforce_Account__c = b.Id;
        update phsAccount;
            
        test.stoptest();
    }
}