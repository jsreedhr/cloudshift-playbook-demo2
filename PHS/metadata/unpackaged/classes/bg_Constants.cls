/**********************************************************************
* bg_Constants:
*
* BrightGen's constants for use in other classes.  
* Created By: BrightGen Ltd
* Created Date: 19/04/2016
*
* Changes: 
***********************************************************************/

public with sharing class bg_Constants {
	/*
	* Field Limits
	*/
	public static Integer ACCOUNT_WILDE_REF_MAXLENGTH = 255;
	
	/*
	 * Case statuses
	 */
    public static String CLOSED_CASE_STATUS = 'Closed';

    /*
     * Record Type Constants
     */
    public static String CASE_OBJECT_TYPE = 'Case';
    public static String STANDARD_CASE = 'Standard';
    public static String QUICK_CASE = 'Quick_Case';

    /*
     * Record Types
     */
    public static Map<String, RecordType> recordTypeNameToRecordMap;
    public static RecordType getRecordType(String sObjectType, String developerName) {
    	if(recordTypeNameToRecordMap == null)
    	{
    		recordTypeNameToRecordMap = new Map<String, RecordType>();
    		for(RecordType recordType : [Select Id, sObjectType, DeveloperName From RecordType])
	    	{
	    		recordTypeNameToRecordMap.put(recordType.sObjectType + recordType.DeveloperName, recordType);
	    	}
    	}
    	
    	if(recordTypeNameToRecordMap != null && recordTypeNameToRecordMap.containsKey(sObjectType + developerName))
    	{
    		return recordTypeNameToRecordMap.get(sObjectType + developerName);
    	}
    	
    	return null;
    }

    /*
     *  Object Owner Mapping Names
     */
    public static String LEAD_OBJECTOWNER_NAME = 'Lead';
    public static String OPPORTUNITY_OBJECTOWNER_NAME = 'Opportunity';
    public static String TASK_OBJECTOWNER_NAME = 'Task';
    public static String EVENT_OBJECTOWNER_NAME = 'Event';

    /*
     * Itegration Constants
     */
    public static final String INTEGRATION_CASE_QUERY_TEMPLATE =  'SELECT Case_Ref__c, Sync_Wildebeest_Message__c FROM Case WHERE id IN (\'\'{0}\'\')';
    public static final String CASE_REF_FIELD = 'Case_Ref__c';
    public static final String WILDEBEEST_MESSAGE_FIELD = 'Sync_Wildebeest_Message__c';
    public static final String AUTH_TOKEN_HEADER = 'Authentication_Token';
    public static final String CONTENT_TYPE_HEADER = 'Content-Type';
    public static final String CONTENT_TYPE_VALUE = 'application/json';

    /*
     * Docusign Constants
     */

    public static final String SENT = 'Sent';
    public static final String COMPLETED     = 'Completed';


}