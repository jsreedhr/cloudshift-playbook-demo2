/**********************************************************************
* bg_RecordBuilder:
*
* A class to create records for testing purposes.  
* Created By: BrightGen Ltd
* Created Date: 19/04/2016
*
* Changes: 
***********************************************************************/

public with sharing class bg_RecordBuilder {
    public static User createUser(String randomVal, String profileId)
    {
        String alias = 'tuser' + randomVal;
        alias = alias.length() > 8 ? alias.right(8) : alias;
        return new User(Username= 'test.user' + randomVal + '@harpercollins.com', Email = 'test.user' + randomVal + '@harpercollins.com', Lastname = 'user' + randomVal, Firstname = 'test' + randomVal, Alias = alias, 
                                   CommunityNickname = 'tuser0' + randomVal, ProfileId = profileId, TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', 
                                   UserPermissionsMobileUser = false, isActive = true);
    }

    public static Glogbal_Settings__c generateDefaultGlobalSettings() {
    	Account accountRecord = new Account(Name = 'Test', BillingPostalcode = 'LS1 3DD');
    	insert accountRecord;

    	Entitlement entitlementRecord = new Entitlement(Name = 'Test', AccountId = accountRecord.Id);
    	insert entitlementRecord;

    	/*
    	 * Check if chatter groups can be seen in tests and set the default service one.
    	 */

    	Glogbal_Settings__c globalSettings = new Glogbal_Settings__c();
    	globalSettings.Default_Case_Priority__c = '3';
    	globalSettings.Default_Entitlement_Id__c = entitlementRecord.Id;
    	globalSettings.Service_Chatter_Id__c = '';
        globalSettings.Can_Edit_Case_Description__c  = TRUE;

    	insert globalSettings;


        Wildebeest_Integration__c integrationSetting = Wildebeest_Integration__c.getValues('Host');
        if(integrationSetting == null) {
            integrationSetting = new Wildebeest_Integration__c(Name = 'Host');
            integrationSetting.Http_Callout_Endpoint__c = 'test';
            integrationSetting.Http_Callout_Query__c = 'test';
            integrationSetting.Integration_Log_Purge_Limit__c = 5;
            insert integrationSetting;
        }

    	return globalSettings;
    }

    public static Map<String, RecordType> recordTypeMap;
    public static RecordType getRecordType(String developerName, String sObjectType) {
        if(recordTypeMap == null)
        {
            recordTypeMap = new Map<String, RecordType>();
            List<RecordType> recordTypes = [Select Id, DeveloperName, sObjectType From RecordType];
            for(RecordType recordType : recordTypes)
            {
                recordTypeMap.put(recordType.DeveloperName + recordType.sObjectType, recordType);
            }
        }

        System.debug('Brightgen recordTypeMap.get(developerName + sObjectType) [' + recordTypeMap.get(developerName + sObjectType) + ']');
        return recordTypeMap.get(developerName + sObjectType);
    }
}