/**
 *  @Class Name:    QuoteTriggerHandlerTest
 *  @Description:   This is a test class for QuoteTriggerHandler
 *  @Company:       dQuotient
 *  CreatedDate:    08/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           08/12/2017                  Original Version
 */
@isTest
private class QuoteTriggerHandlerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook; 
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Contact contact = bg_Test_Data_Utils.createContact('1', acc.Id);
        insert contact;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        opp.Pricebook2Id = standardPricebook.Id;
        insert opp;
        
        Product2 productObj = bg_Test_Data_Utils.createaProduct2('Test Product');
        insert productObj;
        
        Quote testQuote = bg_Test_Data_Utils.createQuote('Test Quote');
        testQuote.OpportunityId = opp.Id;
        testQuote.Pricebook2Id = standardPricebook.Id;
        insert testQuote;
        
        PricebookEntry pbe = bg_Test_Data_Utils.createPricebookEntry(productObj.Id, standardPricebook.Id);
        pbe.IsActive = true;
        insert pbe;
        
        OpportunityLineItem testOppProduct = bg_Test_Data_Utils.createOpportunityLineItem(opp);
        testOppProduct.Product2Id = productObj.Id;
        testOppProduct.OpportunityId = opp.Id;
        testOppProduct.PricebookEntryId = pbe.Id;
        testOppProduct.Quantity__c = 1;
        testOppProduct.Quantity = 1.00;
        testOppProduct.UnitPrice = 20;
        testOppProduct.Description = 'Test Description';
        insert testOppProduct;
    }
    
    private static testMethod void testUpdatePriceBook() {
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        List<Quote> quoteList = [Select Id, OpportunityId From Quote Where Name = 'Test Quote' Limit 1];
        QuoteTriggerHandler.isInSalesforce1 = true;
        System.runAs(u){
             test.startTest();  
            //  system.assert(UserInfo.getUiTheme()=='Theme4d');
             QuoteTriggerHandler.UpdatePriceBook(quoteList);
             test.stopTest();
        }
    }
    
    private static testMethod void testUpdateQuoteLineItems() {
        Opportunity opp = [Select id,Name,Pricebook2Id from Opportunity LIMIT 1];
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        QuoteTriggerHandler.isInSalesforce1 = true;
        System.runAs(u){
            test.startTest();
            Quote testQuote = bg_Test_Data_Utils.createQuote('Test Quote insert');
            testQuote.OpportunityId = opp.Id;
            testQuote.Pricebook2Id = opp.Pricebook2Id;
            insert testQuote;
               
             //QuoteTriggerHandler.UpdateQuoteLineItems(quoteList);
             test.stopTest();
        }
    }
}