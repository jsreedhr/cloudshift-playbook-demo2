/*********************************************************************************
 * bg_UserHelper
 *
 * Helper method for the User Object
 * 
 * Author: Kash Hussain (BrightGen)
 * Created: 21/12/16
 * Updated: 
 *
 *********************************************************************************/
public with sharing class bg_UserHelper
{
    /*
        Method to build a map of Employee Number to User Id for a given set of Empoyee Numbers
    */
    public static Map<String, User> getEmployeeUserMap(Set<String> employeeNumbers)
    {
        Map<String, User> employeeNumberUserMap =  new Map<String, User>();
        List<User> relevantUsers = [SELECT Id, EmployeeNumber, UserRole.Name FROM User WHERE EmployeeNumber IN : employeeNumbers];

        for (User employee : relevantUsers)
        {
            if (!employeeNumberUserMap.containsKey(employee.EmployeeNumber))
            {
                employeeNumberUserMap.put(employee.EmployeeNumber, employee);
            }
        }

        return employeeNumberUserMap;
    }
}