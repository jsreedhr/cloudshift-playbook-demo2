@isTest(SeeAllData=true)
private class phs_ProductHelper_Tests {
	
    static testMethod void testNewProductPriceBookEntryCreation() {
        // TO DO: implement unit test
        test.StartTest();
        Product2 newProd = new Product2(Name = 'TestProd',IsActive = true, Target_Price__c = 10);           
	    insert newProd;
            
		List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
			                                where product2id = :newProd.Id]; 

        //System.assert(results.size() > 0);
        test.stoptest();
    }
    
        static testMethod void testUpdateProductPriceBookSync() {
        // TO DO: implement unit test
        test.StartTest();
        Product2 newProd = new Product2(Name = 'TestProd',IsActive = false, Target_Price__c = 10);           
	    insert newProd;
	    
	    newProd.IsActive = true;
	    newProd.Target_Price__c = 11;
	    
	    update newProd;
            
		List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
			                                where product2id = :newProd.Id]; 

       // System.assert(results.size() > 0);
        test.stoptest();
    }
    
         static testMethod void testUpdateProductCustomPriceBooksSync() {
        // TO DO: implement unit test
        test.StartTest();
        Product2 newProd = new Product2(Name = 'TestProd',IsActive = true, Target_Price__c = 10);           
	    insert newProd;
	    
	    newProd.IsActive = true;
	    newProd.Target_Price__c = 11;
	    
	    update newProd;
            
		List<PriceBookEntry> results = [SELECT Id,Product2Id FROM PricebookEntry 
			                                where product2id = :newProd.Id]; 

//System.assert(results.size() > 0);
        test.stoptest();
    }
}