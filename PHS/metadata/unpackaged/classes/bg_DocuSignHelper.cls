/*************************************************
bg_DocuSignHelper

Helper class for the DocuSign Processes

Author: Kash Hussain (BrightGen)
Created Date: 02/11/2016
Updates: TM - Added methods: setContractSentDateOnAgreement
                             setContractCompletedDateOnAgreement
                             invokeDocuSign
**************************************************/
global class bg_DocuSignHelper
{
    /*
        The Contract Sent Date is set on the Agreement and Opportunity realted to the DocuSign Status record
    */
    public static void setContractSentDateOnAgreement(List<dsfs__DocuSign_Status__c> docuSignStatuses)
    {
        List<Order> agreementsToUpdate = new List<Order>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses)
        {

            if (docuSignStatus.dsfs__Envelope_Status__c == bg_Constants.SENT && docuSignStatus.Agreement__c != null && docuSignStatus.Opportunity_ID__c != null)
            {
                Order relatedAgreement = new Order();
                relatedAgreement.Id = docuSignStatus.Agreement__c;
                relatedAgreement.Agreement_Sent_Date__c = Date.today();
                agreementsToUpdate.add(relatedAgreement);

                Opportunity relatedOpp = new Opportunity();
                relatedOpp.Id = docuSignStatus.Opportunity_ID__c;
                relatedOpp.StageName = 'Contract Sent';
                oppsToUpdate.add(relatedOpp);
            }
        }
        if (!agreementsToUpdate.isEmpty() && !oppsToUpdate.isEmpty())
        {
            update agreementsToUpdate;
            update oppsToUpdate;
        }
    }

    /*
        The Completed Date is set on the Agreement record realted to the DocuSign Status record and flags are set to invoke the Approval Process (Process Builder)
    */
    public static void setContractCompletedDateOnAgreement(List<dsfs__DocuSign_Status__c> docuSignStatuses, Map<Id, dsfs__DocuSign_Status__c> oldDocuSignStatuses)
    {
        List<Order> agreementsToUpdate = new List<Order>();

        for (dsfs__DocuSign_Status__c docuSignStatus : docuSignStatuses)
        {
            dsfs__DocuSign_Status__c oldDocuSignStatus = oldDocuSignStatuses.get(docuSignStatus.Id);

            if (docuSignStatus.dsfs__Envelope_Status__c == bg_Constants.COMPLETED && docuSignStatus.dsfs__Envelope_Status__c != oldDocuSignStatus.dsfs__Envelope_Status__c)
            {
                Order relatedAgreement = new Order();
                relatedAgreement.Id = docuSignStatus.Agreement__c;
                relatedAgreement.Signature_Date__c = Date.today();
                relatedAgreement.Agreement_Signed_and_Returned__c = TRUE;
                relatedAgreement.Signature_Approval_Required__c = TRUE;
                agreementsToUpdate.add(relatedAgreement);   
            }
        }

        if (!agreementsToUpdate.isEmpty())
        {
            update agreementsToUpdate;
        }
    }

    /*
        Method used to build the Recipient Detail string for DocuSign (used by invokeDocuSign)
    */
    private static String buildRecipientDetail(String fName, String lName, String email, String role, Integer routingOrder, String recipientNote)
    {
        String recipientDetail = '';

        recipientDetail += 'FirstName~' + fName + ';';
        recipientDetail += 'LastName~' + lName + ';';
        recipientDetail += 'Email~' + email + ';';
        recipientDetail += 'Role~' + role + ';';
        recipientDetail += 'RoutingOrder~' + routingOrder + ';';
        recipientDetail += 'RecipientNote~' + recipientNote + '';

        return recipientDetail;
    }

    /*
        Method used by the 'Send Agreement' JS button on the Agreement record
    */
    webservice static String invokeDocuSign(String agreementId, String senderFirstName, String senderLastName, String senderEmail, String signerFirstName, String signerLastName, String signerEmail)
    {
        String sender = buildRecipientDetail(senderFirstName, senderLastName, senderEmail, 'Sender', 1, '');
        String signer = buildRecipientDetail(signerFirstName, signerLastName, signerEmail, 'Signer', 2, '');

        // Related Content (default no related content)
        String RC = '';

        // Recipient Signer Limit (default no limit)
        String RSL = ''; 

        // Recipient Starting Routing Order (default 1)
        String RSRO = '';

        // Recipient Routing Order Sequential (default not sequential)
        String RROS = '';

        // Custom Contact Role Map (default config role)
        String CCRM = 'Sender~Sender;Signer~Signer';

        // Custom Contact Type Map (default Signer)
        String CCTM = 'Sender~Signer;Signer~Signer';

        // Custom Contact Note Map (default no note)
        String CCNM = '';

        // Custom Related Contact List (default object contact)
        String CRCL = '';

        // Custom Recipient List
        String CRL = '';

        CRL += sender + ',' + signer;

        // One Click Option (default edit envelope screen)
        String OCO = '0'; 

        // DocuSign Template ID (default no template)
        //String DST = 'C64DF065-6C1D-4895-8C85-3E409C552CB1';
        String DST = '';

        // Load Attachments (default on)
        String LA = '1'; 

        // Custom Email Message (default in config)
        String CEM = 'Thank you for accepting PHS\'s offer. Your business is important to us and we look forward to your being a PHS customer.';

        // Custom Email Subject (default in config)
        String CES = 'Please DocuSign:  PHS Contract Agreement';

        // Show Tag Button (default in config)
        String STB = '1'; 

        // Show Send Button (default in config)
        String SSB = '0'; 

        // Show Email Subject (default in config)
        String SES = '1'; 

        // Show Email Message (default in config)
        String SEM = '1'; 

        // Show Reminder/Expire (default in config)
        String SRS = '0'; 

        // Show Chatter (default in config)
        String SCS = '0'; 

        // Reminder and Expiration Settings
        String RES = '1,3,2,1,120,14'; 

        String url = 'apex/dsfs__DocuSign_CreateEnvelope?DSEID=0&SourceID=' + agreementId + '&RC='+RC+'&RSL='+RSL+'&RSRO='+RSRO+'&RROS='+RROS+'&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+DST+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+ RES;

        return url;     
    }
}