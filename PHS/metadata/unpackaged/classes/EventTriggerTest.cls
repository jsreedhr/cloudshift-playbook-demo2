@isTest
public class EventTriggerTest {
    
    private static testMethod void method1() {
        
        UserRole uRole = new UserRole(DeveloperName = 'Teleappointer', Name = 'Teleappointing');
        insert uRole;
        
      
        
        User u = new User(
      //  ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
      ProfileId = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id,
        LastName = 'last',
        Email = 'test@testEvent.com',
        Username = 'test@testEvent.com' + System.currentTimeMillis(),
        CompanyName = 'TEST',
        Title = 'title',
        Alias = 'alias',
        TimeZoneSidKey = 'America/Los_Angeles',
        EmailEncodingKey = 'UTF-8',
        LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_US',
        UserRoleId = uRole.Id
        );
        
        insert u;
        
        
        System.runAs(u) {
            Lead l = new Lead();
            l.lastName = 'Test';
            l.Company = 'Test Company';
            insert l;
            Event e = new Event();
            e.whoId = l.id;
            e.Subject = 'Test';
            e.startDateTime =System.now();
            e.endDateTime =System.now().addHours(2);
            insert e;
          
            
        }
        
        
       
        
    }
}