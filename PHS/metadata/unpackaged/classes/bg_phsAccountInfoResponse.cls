/*
    Wrapper Class for the Response coming back from Salesforce
*/
public class bg_phsAccountInfoResponse
{
    public Object DebtProfile;
    public List<bg_ContractLine> ContractLines;
    public Object SaleOrderLines;
    public Object RenegotiationDate;
    public Integer ResultSize;
    public Object Visits;
    public Object Message;
    public Boolean Success;
}