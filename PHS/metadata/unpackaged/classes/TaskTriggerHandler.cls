/**
*  Class Name: TaskTriggerHandler
*  Description: This is a TriggerHandler for Task Trigger.

*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  dquotient             17-11-2017                 Last Modified

*/
public without sharing class TaskTriggerHandler {
    
     public static Boolean isAfterUpdate = true;
     public static Boolean isBeforeUpdate = true;
    
    public static void updateParentFields(List<Task> taskList)
    {
        Boolean oppFlg = false;
        Boolean leadFlg = false;
        
        String lead_prefix = Schema.SObjectType.Lead.getKeyPrefix();
        String opportunity_prefix = Schema.SObjectType.Opportunity.getKeyPrefix();
        System.debug(lead_prefix+'    chekc1    '+opportunity_prefix);
        map<Task,Id> mapTaskLead = new Map<Task,Id>();
        map<Task,Id> mapTaskOpp = new Map<Task,Id>();
        Map<String, Opportunity> mapOpportunityRecord = new Map<String, Opportunity>();
        Map<String, Lead> mapLeadtunityRecord = new Map<String, Lead>();
        for(Task taskRecord1 :taskList){
            if(taskRecord1.WhoId!= NULL)
            {
                if(((String)taskRecord1.WhoId).startsWith(lead_prefix))
                    mapTaskLead.put(taskRecord1,taskRecord1.WhoId);
            }
            if(taskRecord1.WhatId!= NULL)
            {
                if(((String)taskRecord1.WhatId).startsWith(opportunity_prefix))
                    mapTaskOpp.put(taskRecord1,taskRecord1.WhatId);
            }
        }
        
        if(mapTaskLead!=NULL)
        {
            System.debug('map lead ids'+mapTaskLead.Values());
            List<Lead> leadList = [Select id, Status,Lead_Score_Total__c,Campaigns__c,LeadSource,Postal_District__c,FLT_Postal_Code__r.name from Lead where Id in: mapTaskLead.Values()];
            for(Lead leadRec :leadList){
                mapLeadtunityRecord.put(leadRec.Id, leadRec);
            }
            System.debug('llist'+leadList);
        }
                
        if(mapTaskOpp!=NULL)
        {
            System.debug('chekc4');
            
            List<Opportunity> oppList = [Select id, LeadSource, StageName,Amount ,Campaigns__c, DM_Postal_District__r.Name from Opportunity where Id in: mapTaskOpp.Values()];
            for(Opportunity oppoRecord :oppList){
                mapOpportunityRecord.put(oppoRecord.Id, oppoRecord);
            }
            System.debug('opplist'+oppList);
        }
        
        for(Task taskRecord :taskList){
            if(mapOpportunityRecord.containsKey(taskRecord.WhatId)){
                system.debug('Opp----->' +mapOpportunityRecord.get(taskRecord.WhatId).LeadSource);
                taskRecord.Stage__c = mapOpportunityRecord.get(taskRecord.WhatId).StageName;
                taskRecord.Amount__c = mapOpportunityRecord.get(taskRecord.WhatId).Amount;
                taskRecord.Source__c = mapOpportunityRecord.get(taskRecord.WhatId).LeadSource;
                taskRecord.Campaigns__c = mapOpportunityRecord.get(taskRecord.WhatId).Campaigns__c;
                taskRecord.Postal_District__c = mapOpportunityRecord.get(taskRecord.WhatId).DM_Postal_District__r.Name;
               // taskRecord.Postal_District__c = mapOpportunityRecord.get(taskRecord.WhatId).Postal_District__c;
            }
        }
        
        for(Task taskRecord :taskList){
            if(mapLeadtunityRecord.containsKey(taskRecord.WhoId)){
                taskRecord.Status__c = mapLeadtunityRecord.get(taskRecord.WhoId).Status;
                taskRecord.Score__c = mapLeadtunityRecord.get(taskRecord.WhoId).Lead_Score_Total__c;
                taskRecord.Source__c = mapLeadtunityRecord.get(taskRecord.WhoId).LeadSource;
                taskRecord.Campaigns__c = mapLeadtunityRecord.get(taskRecord.WhoId).Campaigns__c;
              //  taskRecord.Postal_District__c = mapLeadtunityRecord.get(taskRecord.WhoId).Postal_District__c;
               taskRecord.Postal_District__c = mapLeadtunityRecord.get(taskRecord.WhoId).FLT_Postal_Code__r.name;
            }
        }
        
    }
}