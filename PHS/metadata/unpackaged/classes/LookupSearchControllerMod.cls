/**
 *  Class Name: LookupSearchController  
 *  Description: This is a Controller for LookupSearch page.
 *  Company: Standav
 *  CreatedDate:22/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             22/03/2018                 Original version
 */
public class LookupSearchControllerMod {
    
    public List<FieldSetUtility.FieldSetMemberWrapper>  fieldSetWrapper;
    public list<DisplayObjectWrapper> displayObjWrapperList {get;set;}
    public String lookupApi {get;set;}
    public String searchTerm;
    public String getSearchTerm() {
        return this.searchTerm;
    }
    public void setSearchTerm (String searchTerm) {
        this.searchTerm = searchTerm;
    }
    public Boolean showSearchList {get; set;}
    public String uniqueId {get; set;}

    public LookupSearchControllerMod() {
        lookupApi = Apexpages.currentPage().getParameters().get('lookupApi');
        System.debug('inside constructor.....>'+lookupApi);
        System.debug('field name.....>'+GlobalConstants.CUSTOM_SETTINGS_SEARCH_LOOKUP_MAP);
        if(lookupApi != null) {
            System.debug('lookup api not null>>>>>'+lookupApi);
            fieldSetWrapper = FieldSetUtility.getFieldSetDetails(lookupApi, GlobalConstants.CUSTOM_SETTINGS_SEARCH_LOOKUP_MAP.get(lookupApi).Fieldset_API__c);    
        }
        searchTerm = Apexpages.currentPage().getParameters().get('searchTerm');
        System.debug('seachTerm.......>'+getSearchTerm());
        System.debug('fieldSetWrapper.......>'+fieldSetWrapper);
        showSearchList = false;
    }
    
    //method to get the records
    public void getLookupSearchList() {
        uniqueId = Apexpages.currentPage().getParameters().get('uniqueId');
        System.debug('uniqueId param..........>'+uniqueId);
        system.debug('enter search method with ----->'+searchTerm);
        String searchquery='FIND \''+searchTerm+'\' IN ALL FIELDS RETURNING PHS_Account__c(Id, Account_Name__c, Billing_Country__c, Phone__c, Type_of_Business__c)';
        List<List<sObject>> sObjectList = Search.query(searchQuery);
        // List<List<sObject>> sObjectList = [FIND {searchTerm} IN ALL FIELDS RETURNING PHS_Account__c(Id, Account_Name__c, Billing_Country__c, Phone__c, Type_of_Business__c)];
        this.displayObjWrapperList = New List<DisplayObjectWrapper>();
        for(List<sObject> sObjList : sObjectList)
        {
            system.debug('sObjList---->'+sObjList);
            for(sObject sObjectVar : sObjList)
            {
                DisplayObjectWrapper objWrapper = New DisplayObjectWrapper();
                objWrapper.objId = String.valueOf(sObjectVar.get('Id'));
                objWrapper.Name = String.valueOf(sObjectVar.get('Account_Name__c'));
                objWrapper.Country = String.valueOf(sObjectVar.get('Billing_Country__c'));
                objWrapper.Phone = String.valueOf(sObjectVar.get('Phone__c'));
                objWrapper.BusinessType = String.valueOf(sObjectVar.get('Type_of_Business__c'));
                displayObjWrapperList.add(objWrapper);
            }    
        }
        showSearchList = true;
        system.debug('displayObjWrapperList---->'+displayObjWrapperList);
        return;
    }
    
    // Wrapper for the records
    public class DisplayObjectWrapper
    {
        public String objId{get;set;}
        public String Name{get;set;}
        public String Country{get;set;}
        public String Phone{get;set;}
        public String BusinessType{get;set;}
    }
    
}