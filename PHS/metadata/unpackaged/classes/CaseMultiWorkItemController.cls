public without sharing class CaseMultiWorkItemController {
    public string errOnPage {
        get;
        set;
    }
    public list <Case> lst_childworkitems {
        get;
        set;
    }
    public list <Case> lst_parentcases {
        get;
        set;
    }
    public list <RecordType> lst_recordtypes {
        get;
        set;
    }
    public string rowNo {
        get;
        set;
    }
    Public String parentcaseId;
    public integer noOfRows {
        get;
        set;
    }
    public List < booleanProjectWrapper > lstOfbooleanProjectWrapper {
        get;
        set;
    }
    public boolean selectAll {
        get;
        set;
    }
    public Boolean showerror {
        get;
        set;
    }
    public integer errorPos {
        get;
        set;
    }
    //constructor to grt the records

    public CaseMultiWorkItemController(apexpages.standardController stdController) {
        errOnPage = '';
        lst_parentcases = new list <Case>();
        lst_childworkitems = new list <Case>();
        lstOfbooleanProjectWrapper = new List <booleanProjectWrapper> ();
        selectAll = false;
        noOfRows = 1;
        parentcaseId = apexpages.currentpage().getparameters().get('id');
        lst_parentcases = [select ID, CaseNumber, Diarised_Date_Time__c, Due_Date__c, Subject, Status, Description, Type, Case_Reason__c, ParentId, Parent_Case__c, Priority, OwnerId From Case where id =: parentcaseId limit 1];
        lst_childworkitems = [select ID, CaseNumber, Diarised_Date_Time__c, Due_Date__c, Subject, Status, Description, Type, Case_Reason__c, ParentId, Parent_Case__c, Priority, OwnerId From Case where Parentid =: parentcaseId limit 1];
        lst_recordtypes = [SELECT ID, sObjectType, DeveloperName FROM RecordType WHERE sObjectType = 'Case' AND DeveloperName = 'Work_Item' LIMIT 1];
        
        for (Case objcase: lst_childworkitems) {
            Case c = new Case();
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objcase));
        }
    }

    // To Create a new case record on click of Add row button

    public Void addRow() {
        selectAll = false;
        for (integer i = 0; i < noOfRows; i++) {
            Case objportlink = new Case();
            objportlink.Parent_Case__c = parentcaseId;
            objportlink.Status = 'New';
            objportLink.RecordTypeId = lst_recordtypes[0].id;
            
            objportlink.OwnerId = lst_parentcases[0].OwnerId;

            lst_childworkitems.add(objportlink);
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink));
        }
    }

    public void selectAll() {

        if (selectAll == true) {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = false;
            }
        }
    }

    Public void deleteSelectedRows() {
            selectAll = false;
            list <Case> toDeleteRows = new list <Case> ();
            for (integer j = (lstOfbooleanProjectWrapper.size() - 1); j >= 0; j--) {
                if (lstOfbooleanProjectWrapper[j].isSelected == true) {
                    if (lstOfbooleanProjectWrapper[j].objport.id != null) {
                        toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                    }

                    lstOfbooleanProjectWrapper.remove(j);

                }


            }
            delete toDeleteRows;
        }
        /**
        *   Method Name:    DelRow
        *   Description:    To delete the record by passing the row no.
        *   Param:  RowNo

        */

    public Void delRow() {

            selectAll = false;
            system.debug('--------' + RowNo);
            list <Case> todelete = new list <Case> ();

            if (lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null) {
                todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);

            }


            lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
            delete todelete;


        }
        /**
         *   Method Name:    saveWorkItem
         *   Description:    To save the records and then redirect it to respective opportunity
         */

    public void saveWorkItem() {
        if (showerror != TRUE) {
            List <Case> listToUpsert = new List <Case> ();
            Boolean isBlank = FALSE;

            for (booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper) {
                if (objportlink.objport.Status == NULL || objportlink.objport.Type == NULL) {
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'Please ensure the fields: Status and Type have been completed when creating new items'));

                } else {
                    errOnPage = '';
                    listToUpsert.add(objportlink.objport);
                }
            }
            if (listToUpsert.size() > 0) {
                try {
                    Upsert listToUpsert;
                    PageReference acctPage = new PageReference('/' + ParentCaseId);
                    acctPage.setRedirect(true);


                } catch (Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'There is missing information on the case, please edit and save the case record to identify the missing information.'));
                }
            }
        }

    }

    public class booleanProjectWrapper {
        public Boolean isSelected {
            get;
            set;
        }
        public Case objport {
            get;
            set;
        }
        public integer ValuetoList {
            get;
            set;
        }

        public booleanProjectWrapper(boolean isSelect, Case objports) {
            objport = objports;
            isSelected = isSelect;
            ValuetoList = 1;

        }
    }

}