/*********************************************************************************
 * bg_PHSAccountSelectionQuickCtr_Tests
 *
 * Test methods for the bg_PHSAccountSelectionQuickController class
 *
 * Author: Jamie Wooley
 * Created: 06-05-2016
 *
 *********************************************************************************/

@isTest
private class bg_PHSAccountSelectionQuickCtr_Tests {

    static testMethod void test_NoAccount() {

    	Account controllerAccount = new Account();

    	PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.goToCaseCreationScreen();

        system.assert(phsAccountSelectionController.errors.size()>0);

    }

    static testMethod void test_ForceRefresh() {

    	Account controllerAccount = new Account();

    	PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.forceRefresh();

    }

    static testMethod void test_PHSAccountOptions(){
		Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Admin_Team_Name__c = 'test';
        phsAccount.Location_AVI_Value__c = 123;
        insert phsAccount;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;

        List<SelectOption> phsAccounts = phsAccountSelectionController.PHSAccounts;

    }

    static testMethod void test_AccountWithoutPHSAccounts() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;

        phsAccountSelectionController.goToCaseCreationScreen();

        system.assert(phsAccountSelectionController.errors.size()>0);

    }

    static testMethod void test_AccountWithPHSAccountNoService() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Service_Enabled__c = false;
        insert phsAccount;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        phsAccountSelectionController.goToCaseCreationScreen();

        system.assert(phsAccountSelectionController.errors.size()>0);

    }

    static testMethod void test_AccountWithPHSAccountNoRef() {
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        phsAccount.Wildebeest_Ref__c = null;
        insert phsAccount;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;

        phsAccountSelectionController.goToCaseCreationScreen();

        system.assert(phsAccountSelectionController.errors.size()>0);

    }

    static testMethod void test_AccountWithPHSAccount_Positive() {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();

        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        insert phsAccount;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
    	Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;
        phsAccountSelectionController.theCase.Origin = 'Email';
        phsAccountSelectionController.theCase.Type = 'Service Problem';
        phsAccountSelectionController.theCase.Case_Reason__c = 'Service driver can\'t get on site';

        PageReference casePageRef = phsAccountSelectionController.goToCaseCreationScreen();

        List<Case> theCreatedCase = [select Id from Case];

        system.assertEquals(1, theCreatedCase.size());

    }

    static testMethod void test_AccountWithPHSAccount_Negative() {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        
        Account a = bg_Test_Data_Utils.createAccount('test');
        insert a;

        PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('12345', a.Id);
        insert phsAccount;

        Account controllerAccount = new Account();

        PageReference pageRef = Page.QuickCreateCase;
        Test.setCurrentPage(pageRef);

        bg_PHSAccountSelectionQuickController phsAccountSelectionController = new bg_PHSAccountSelectionQuickController(new ApexPages.StandardController(controllerAccount));
        phsAccountSelectionController.theCase.accountId = a.Id;
        phsAccountSelectionController.theCase.PHS_Account__c = phsAccount.Id;
        phsAccountSelectionController.theCase.Origin = null;
        phsAccountSelectionController.theCase.Type = 'Service Problem';
        phsAccountSelectionController.theCase.Case_Reason__c = 'Service driver can\'t get on site';

        PageReference casePageRef = phsAccountSelectionController.goToCaseCreationScreen();

        List<Case> theCreatedCase = [select Id from Case];

        system.assertEquals(0, theCreatedCase.size());

    }

}