/***************************************************
* Class Name    : AgreementLocationTriggerFunction
* Date Created  : 13 - Jul - 2018
* Date Modified : 16 - Jul - 2018
* Purpose       : This is the helper class for 
*                 the agreement location trigger AgreementLocationTrigger  
* *************************************************/
public without sharing class AgreementLocationTriggerFunction {
    
    //Method to create Default AccessTimes for newly created account on agreement location creation
    public static void createDefaultAccessTime(List<Agreement_Location__c> listAgrLocation){
        Set<Id> locAccount = new Set<Id>();
        Map<Id,Agreement_Location__c> agrLocMap = new Map<Id,Agreement_Location__c>();
        for(Agreement_Location__c agrLocObj : listAgrLocation){
            if(agrLocObj.Location_Salesforce_Account__c!=null){
                agrLocMap.put(agrLocObj.id,agrLocObj);
                locAccount.add(agrLocObj.Location_Salesforce_Account__c);
            }            
        }       
        
      // List<account> newAccounts = [select id, Name, CreatedDate from Account where SystemModstamp >= :Datetime.now().addMinutes(-15) AND id in:locAccount];
      List<account> newAccounts = [select id, Name, CreatedDate, (Select id from Access_Times__r) from Account where createdDate <:DateTime.now() AND createdDate >:Datetime.now().addMinutes(-15) AND id in:locAccount];
        
        IF(!newAccounts.isEmpty()){

         List <Access_Time__c> lst_ATstocreate = new List <Access_Time__c>();
         
         //Insert Access Times
         for(Account a: newAccounts){
            if(a.Access_Times__r.size() == 0)
            {
            Access_Time__c ATMon = new Access_Time__c();
            ATMon.Account__c = a.id;
            ATMon.Day__c = '1. Monday';
            ATMon.From__c = '06:00';
            ATMon.To__c = '18:00';
            lst_ATstocreate.add(ATMon);
            Access_Time__c ATTue = new Access_Time__c();
            ATTue.Account__c = a.id;
            ATTue.Day__c = '2. Tuesday';
            ATTue.From__c = '06:00';
            ATTue.To__c = '18:00';
            lst_ATstocreate.add(ATTue);
            Access_Time__c ATWed = new Access_Time__c();
            ATWed.Account__c = a.id;
            ATWed.Day__c = '3. Wednesday';
            ATWed.From__c = '06:00';
            ATWed.To__c = '18:00';
            lst_ATstocreate.add(ATWed);
            Access_Time__c ATThu = new Access_Time__c();
            ATThu.Account__c = a.id;
            ATThu.Day__c = '4. Thursday';
            ATThu.From__c = '06:00';
            ATThu.To__c = '18:00';
            lst_ATstocreate.add(ATThu);
            Access_Time__c ATFri = new Access_Time__c();
            ATFri.Account__c = a.id;
            ATFri.Day__c = '5. Friday';
            ATFri.From__c = '06:00';
            ATFri.To__c = '18:00';
            lst_ATstocreate.add(ATFri);
            }
        }
        insert lst_ATstocreate;
        }        
    }
    
    //Populate Account Information from the account linked to the agreement
    public static void PopulateLocationInformation(List<Agreement_Location__c> listAgrLocation){
        Set<Id> locAccount = new Set<Id>();
        List <Account> lst_accs = new List <Account>();
        Map<Id,Agreement_Location__c> agrLocMap = new Map<Id,Agreement_Location__c>();
        Map<Id, Id> mapLocAgreementMap = new Map <Id, Id>();
        for(Agreement_Location__c agrLocObj : listAgrLocation){
            if(agrLocObj.Location_Salesforce_Account__c!=null){
                agrLocMap.put(agrLocObj.id,agrLocObj);
                locAccount.add(agrLocObj.Location_Salesforce_Account__c);
                mapLocAgreementMap.put(agrLocObj.Location_Salesforce_Account__c, agrLocObj.Agreement__c);
            }            
        }
        
        //Query all of the agreements linked to the agreement locations
        List <Order> lst_orders = new List <Order>([SELECT ID, Accountid, Account.Sales_Tier__c, Account.OwnerId, Account.Sic, Account.SicDesc, Account.SIC_2007__c, Account.SIC_Group__c, Account.Sic_Temp__c, Account.Industry, Account.Greenleaf_Key_Account__c, 
                                                   Account.Greenleaf_Key_Account_Manager__c, Account.Key_Account__c, Account.Key_Account_Director__c, Account.Key_Account_Executive__c, Account.Key_Account_Manager__c, Account.Key_Account_Role__c, Account.Key_Account_Sales_Coordinator__c, Account.Key_Account_Support_Manager__c, Account.Key_Account_Type__c, 
                                                   Account.HasLocationResponsibility__c, Account.Entity_Type__c, Account.ParentId, Account.Is_Limited_Company__c, Account.Company_Status__c FROM Order WHERE Id in:mapLocAgreementMap.values()]);
                
       //	List<account> newAccounts = [select id, Name, CreatedDate from Account where SystemModstamp >= :Datetime.now().addMinutes(-15) AND id in:locAccount];
        List<account> newAccounts = [select id, Name, CreatedDate from Account where createdDate <:DateTime.now() AND createdDate >:Datetime.now().addMinutes(-15) AND id in:locAccount];
        
        System.debug('--newAccount---'+newAccounts);
        IF(!newAccounts.isEmpty()){
            
            System.debug('Inner Loop');
            
            List <Account> lst_accstoupdate = new List <Account>();
            
            //Set the newly created account details to be the same as the details from the account the agreement is linked to
            for(Account a: newAccounts){
                for(Order o: lst_orders){
                    IF(mapLocAgreementMap.get(a.id)==o.Id){
                    	a.Sales_Tier__c = o.Account.Sales_Tier__c;
                        a.OwnerId = o.Account.OwnerId;
                        a.Sic = o.Account.Sic;
                        a.SicDesc = o.Account.SicDesc;
                        a.SIC_2007__c =  o.Account.SIC_2007__c;
                        a.SIC_Group__c = o.Account.SIC_Group__c;
                        a.SIC_Temp__c = o.Account.SIC_Temp__c;
                        a.Industry = o.Account.Industry;
                        a.Greenleaf_Key_Account__c = o.Account.Greenleaf_Key_Account__c;
                        a.Greenleaf_Key_Account_Manager__c = o.Account.Greenleaf_Key_Account_Manager__c;
                        a.Key_Account__c = o.Account.Key_Account__c;
                        a.Key_Account_Director__c = o.Account.Key_Account_Director__c;
                        a.Key_Account_Manager__c = o.Account.Key_Account_Manager__c;
                        a.Key_Account_Role__c = o.Account.Key_Account_Role__c;
                        a.Key_Account_Sales_Coordinator__c = o.Account.Key_Account_Sales_Coordinator__c;
                        a.Key_Account_Support_Manager__c = o.Account.Key_Account_Support_Manager__c;
                        a.Key_Account_Type__c = o.Account.Key_Account_Type__c;
						a.HasLocationResponsibility__c = TRUE;
						a.Entity_Type__c = o.Account.Entity_Type__c;
						a.ParentId = o.Account.ParentId;
						a.Company_Status__c = 'Prospect';
                        a.Is_Limited_Company__c = o.Account.Is_Limited_Company__c;
                        lst_accstoupdate.add(a);
                    }
                }
            }
            System.debug(lst_accstoupdate.size());
            Update lst_accstoupdate;
        }
    }



    /**
     *  Method Name: populateSalesforceAccountNamesonAgreement
     *  Description:  method to populate Salesforce Account Names on Agreement
     *  Param:  Map<id, Agreement_Location__c> mapIdOldAgreementLocations -> Trigger.OldMap
     *  Param:  Map<id, Agreement_Location__c> mapIdNewAgreementLocations -> Trigger.newMap
     *  Return: NA
     */
     
   /* public static void populateSalesforceAccountNamesonAgreement(Map<id, Agreement_Location__c> mapIdOldAgreementLocations, Map<id, Agreement_Location__c> mapIdNewAgreementLocations){
        List<Agreement_Location__c> lstNewAgreementLocations = new List<Agreement_Location__c>();
        for(Agreement_Location__c agreementLocation : mapIdNewAgreementLocations.values()){
            // Check if to see the Salesforce Account has changed on the Agreement Location
            if(mapIdOldAgreementLocations.containsKey(agreementLocation.id) && (mapIdOldAgreementLocations.get(agreementLocation.id).Location_Salesforce_Account__c != agreementLocation.Location_Salesforce_Account__c)){
                lstNewAgreementLocations.add(agreementLocation);
            }
        }
        if(!lstNewAgreementLocations.isEmpty()){
            populateSalesforceAccountNamesonAgreement(lstNewAgreementLocations);
        }
    }*/

     /**
     *  Method Name: populateSalesforceAccountNamesonAgreement
     *  Description:  method to populate Salesforce Account Names on Agreement
     *  Param:  List<Agreement_Location__c> lstAgreementLocations -> Trigger.new
     *  Return: NA
     */
     
  /*  public static void populateSalesforceAccountNamesonAgreement(List<Agreement_Location__c> lstNewAgreementLocations){
        if(lstNewAgreementLocations != null && !lstNewAgreementLocations.isEmpty()){
            // Set of Agreement Ids
            set<id> setAgreementIds = new set<id>();
            for(Agreement_Location__c agreementLocation: lstNewAgreementLocations){
                // Collection of Agreement Ids
                setAgreementIds.add(agreementLocation.Agreement__c);
            }
            if(!setAgreementIds.isEmpty()){
                // Fetching the Account Names in the ascending order of Name
                List<Agreement_Location__c> lstTotalAgreementLocations = new List<Agreement_Location__c>();
                // List of Agreements to be updates
                Map<id, Order> mapIdAgreements = new Map<id, Order>();
                mapIdAgreements = constructMapOfAgreementsWithAccountNames(setAgreementIds);
                try{
                    // Updation of Agreement records with the Account Names populated
                    update mapIdAgreements.values();
                }catch(exception e){
                    System.debug('The exception occured'+e.getMessage());
                }
            }
        }
    }*/

    /**
     *  Method Name: constructMapOfAgreementsWithAccountNames
     *  Description:  method to construct map of Agreement id and Agreements with the Salesforce_Account_Names__c field populated 
     *  Param:  set<Id> setAgreementIds -> Set of Agreement Ids concerned
     *  Return: Map<id, Order>
     */
     
  /*  public static Map<id, Order> constructMapOfAgreementsWithAccountNames(set<Id> setAgreementIds){
        Map<id, Order> mapIdAgreements = new Map<id, Order>();
        if(!setAgreementIds.isEmpty()){
            // Fetching the Account Names in the ascending order of Name
            List<Agreement_Location__c> lstTotalAgreementLocations = new List<Agreement_Location__c>();
            // Map of Agreement with Unique Account Ids
            map<id, set<id>> mapAgreementAccountIds = new map<id, set<id>>();
            lstTotalAgreementLocations = [Select id, 
                                            Agreement__c,
                                            Location_Salesforce_Account__c, 
                                            Location_Salesforce_Account__r.Name 
                                            From Agreement_Location__c 
                                            where Agreement__c in: setAgreementIds 
                                            order by Location_Salesforce_Account__r.Name asc];
            for(Agreement_Location__c agreementLocation: lstTotalAgreementLocations){
                // Null Check to make sure that the Account field populated at Agreement Location level
                if(agreementLocation.Location_Salesforce_Account__c != null){
                    set<Id> setAccountIds = new set<Id>();
                    // Getting the current Account Ids associated with the agreement
                    if(mapAgreementAccountIds.containsKey(agreementLocation.Agreement__c)){
                        setAccountIds = mapAgreementAccountIds.get(agreementLocation.Agreement__c);
                    }
                    if(!setAccountIds.contains(agreementLocation.Location_Salesforce_Account__c)){
                        // Population of the Account Names at Order Level in the ascending order of Account Names
                        Order agreement = new Order();
                        if(mapIdAgreements.containsKey(agreementLocation.Agreement__c)){
                            agreement = mapIdAgreements.get(agreementLocation.Agreement__c);
                            agreement.Salesforce_Account_Names__c += ',\'' + agreementLocation.Location_Salesforce_Account__c + '\'';
                        }else{
                            agreement.id = agreementLocation.Agreement__c;
                            agreement.Salesforce_Account_Names__c = '\''+agreementLocation.Location_Salesforce_Account__c +  '\'';
                        }
                        mapIdAgreements.put(agreement.id, agreement);
                        setAccountIds.add(agreementLocation.Location_Salesforce_Account__c);
                        mapAgreementAccountIds.put(agreementLocation.Agreement__c, setAccountIds);
                    }
                }
            }
        }
        return mapIdAgreements;
    } */

}