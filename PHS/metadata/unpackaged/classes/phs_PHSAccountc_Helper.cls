public with sharing class phs_PHSAccountc_Helper {
       public static void maintainAccountToPHSAccountRelationship(Map<Id,PHS_Account__c> oldMap,List<PHS_Account__c> newMap) {
        PHS_Account__c newObj = newMap[0];
        PHS_Account__c oldObj = oldMap.get(newObj.Id);
            
		String oldAccId = oldObj.Salesforce_Account__c;
	    String newAccId = newObj.Salesforce_Account__c;
			
		if(oldAccId != newAccId) {
			String objRef = newObj.Wildebeest_Ref__c;			
	    	Account oldAcct = [Select Id, Wildebeest_Ref__c From Account where Id =: oldAccId];
            Account newAcct = [Select Id, Wildebeest_Ref__c From Account where Id =: newAccId];       
			String oldWideRef = oldAcct.Wildebeest_Ref__c; 
		    String newWideRef = newAcct.Wildebeest_Ref__c; 
		    String oldRefs;
		    
			If(oldWideRef.contains(objRef)){
				List<String> refs = oldWideRef.split(',');
	 			String[] tmp1 = New String[]{};                
				for(String r : refs){
             		If(r != objRef){
      					tmp1.add(r);   
               		 }
				}
				oldRefs = string.join(tmp1,',');
				oldAcct.Wildebeest_Ref__c = oldRefs;
				update oldAcct;				 	
			}
			if(String.isBlank(newAcct.Wildebeest_Ref__c)){
			 	newAcct.Wildebeest_Ref__c = objRef;
			 	        	update newAcct;	
			}
			else{
				String checkLen  = newWideRef + ',' + objRef;
        	if(checkLen.Length() <= bg_Constants.ACCOUNT_WILDE_REF_MAXLENGTH)
        		{
        		newAcct.Wildebeest_Ref__c = newWideRef + ',' + objRef;
        	 	update newAcct;
        		}			 		
			}					        	
        }
	}
	
	 public static void addNewPHSAccountToAccount(List<PHS_Account__c> newMap) {
    	PHS_Account__c obj = newMap[0];
		String accId = obj.Salesforce_Account__c;
        Account a = [Select Id, Wildebeest_Ref__c From Account where Id =: accId];
        String currentRef = a.Wildebeest_Ref__c;
        
       	if(String.isBlank(a.Wildebeest_Ref__c)){
        	a.Wildebeest_Ref__c = obj.Wildebeest_Ref__c;    
        	       update a;    
        }
        else{
        	String checkLen  = currentRef + ',' + obj.Wildebeest_Ref__c;
        	if(checkLen.Length() <= bg_Constants.ACCOUNT_WILDE_REF_MAXLENGTH)
        	{
        	a.Wildebeest_Ref__c = currentRef + ',' + obj.Wildebeest_Ref__c;
        	       update a;
        	}
        }
	}
}