/**********************************************************************
* bg_DeleteIntegrationLogs_Tests:
*
* Test class for the Scheduled Batch job to delete Integration Logs older 
* than N days
* 
* Created By: KH
* Created Date: 16/11/16
*
* Changes: 
***********************************************************************/
@isTest
private class bg_DeleteIntegrationLogs_Tests
{    
	// CRON expression: midnight on March 15.
   	// Because this is a test, job executes
   	// immediately after Test.stopTest().
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testMethod void testMethod1()
    {
   		Wildebeest_Integration__c settings = new Wildebeest_Integration__c();
   		settings.Name = 'Host';
   		settings.Integration_Log_Purge_Limit__c = 5;
   		insert settings;

   		Integration_Log__c logA = new Integration_Log__c();
   		logA.Callout_Type__c = 'Login';
   		logA.Success__c = true;
   		insert logA;

		Datetime tenDaysAgo = Datetime.now().addDays(-10);
		Test.setCreatedDate(logA.Id, tenDaysAgo);

		Test.startTest();

		// Schedule the test job
		String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new bg_DeleteIntegrationLogs());
		 
		// Get the information from the CronTrigger API object
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		system.debug('**ct before**: ' + ct);

		// Verify the expressions are the same
		System.assertEquals(CRON_EXP, ct.CronExpression);

		// Verify the job has not run
		System.assertEquals(0, ct.TimesTriggered);

		// Verify the next time the job will run
		System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

		// Verify the scheduled job hasn't run yet.
		List<Integration_Log__c> logs = [SELECT Id FROM Integration_Log__c];

		System.assertEquals(logs.size(), 1);

		Test.stopTest();
	}
}