/**********************************************************************
* bg_OpportunityHelper:
*
* Tests for the Opportunity object
* Created By: Tom Morris - BrightGen Ltd
* Created Date: 01/08/2016
*
* Changes: KH 27/09/16 - Simplified process by removing dependency on DM Postal Code on Account level.
*          KH 22/12/16 - Added setOriginatingEmployee and checkOriginatingEmployee
***********************************************************************/

public without sharing class bg_OpportunityHelper
{
    public static boolean isBeforeInsert = true;
    public static boolean isBeforeUpdate = true;
    
    private static boolean run = true;
     public static boolean runOnce(){
    if(run){
     run=false;
  return true;
    }else{
    return run;
    }
    
    }
    /*
        Method to find the FLT record related to the Account record based on it's postcode/outcode. 
    */
    /*
    public static void populateFLTFields(List<Opportunity> oppRecords)
    {
       
       List<Opportunity> oppRecordsToUpdate = new List<Opportunity>();

        List<Opportunity> oppsForProcessing = [SELECT Id, Name, Account.FLT_Postal_District__c, DM_Postal_District__c FROM Opportunity WHERE Id IN:oppRecords];
        for (Opportunity oppRecord : oppsForProcessing)
        {
            system.debug(oppRecord.Account.FLT_Postal_District__c+'updatepostal'+oppRecord.DM_Postal_District__c);
            if(oppRecord.Account.FLT_Postal_District__c != null && oppRecord.DM_Postal_District__c == null)
            {
              
               ID fltRecord = oppRecord.Account.FLT_Postal_District__c;
                Opportunity opp = new Opportunity();
                opp.Id = oppRecord.Id;
                opp.DM_Postal_District__c = oppRecord.Account.FLT_Postal_District__c;
                oppRecordsToUpdate.add(opp);
            }
        }
        system.debug('============46====');
        if(!oppRecordsToUpdate.isEmpty())
        {   
           update oppRecordsToUpdate; 
        } 
     
    }
    */
    
    public static void populateFLTFields(List<Opportunity> lstNewOpportunity)
    {
        set<Id> setAccIds = new set<Id>();
        for(Opportunity objOpp: lstNewOpportunity)
        {
            if(objOpp.AccountId != null) setAccIds.add(objOpp.AccountId);
        }
        map<id, Account> mapAccount = new map<id, Account>([SELECT id, FLT_Postal_District__c  FROM Account WHERE id IN: setAccIds AND FLT_Postal_District__c != null]);
        for(Opportunity objOpp: lstNewOpportunity)
        {
            if(objOpp.DM_Postal_District__c == null && objOpp.AccountId != null && mapAccount.containsKey(objOpp.AccountId) )
            {
                objOpp.DM_Postal_District__c = mapAccount.get(objOpp.AccountId).FLT_Postal_District__c;
            }
        }
    }


    /*
        Method used to set the Orginating Employee On the Opportunity (before insert). The employee number on the Opportunity
        is used to get the associated User record. A check is made to see if the Employee Number has changed on the Opportunity.
    */
    public static void setOriginatingEmployee(List<Opportunity> oppRecords)
    {
        Set<String> employeeNumbers = new Set<String>();

        for(Opportunity oppRecord : oppRecords)
        {
            if (oppRecord.Originating_Employee__c != null)
            {
                employeeNumbers.add(oppRecord.Originating_Employee__c);
            }
        }

        if (!employeeNumbers.isEmpty())
        {
            Map<String, User> employeeNumberUserMap = bg_UserHelper.getEmployeeUserMap(employeeNumbers);
            for (Opportunity oppRecord : oppRecords)
            {
                if (employeeNumberUserMap.containsKey(oppRecord.Originating_Employee__c))
                {
                    oppRecord.Originating_Employee_User__c = employeeNumberUserMap.get(oppRecord.Originating_Employee__c).Id;
                    oppRecord.Originating_Employee_Role__c = employeeNumberUserMap.get(oppRecord.Originating_Employee__c).UserRole.Name;
                }
            }
        }
    }

    
    /*
        Method used to set the Orginating Employee On the Opportunity (before update). The employee number on the Opportunity
        is used to get the associated User record.
    */
    public static void checkOriginatingEmployee(List<Opportunity> oppRecords, Map<Id, Opportunity> oldoppRecords)
    {
        List<Opportunity> oppsToProcess = new List<Opportunity>();

        for (Opportunity oppRecord : oppRecords)
        {
            Opportunity oldoppRecord = oldoppRecords.get(oppRecord.Id);

            if (oppRecord.Originating_Employee__c != null && oppRecord.Originating_Employee__c != oldoppRecord.Originating_Employee__c)
            {
                oppsToProcess.add(oppRecord);
            }
        }

        if (!oppsToProcess.isEmpty())
        {
            setOriginatingEmployee(oppsToProcess);
        }
    }
    
    
    
}