/**********************************************************************
* TaskWrapper:
*
* Wrapper class for tasks to send to Wildebeest dates in a 
* different format
* Created By: BrightGen Ltd (AL)
* Created Date: 18/01/2016
*
* Changes: 
***********************************************************************/
public with sharing class TaskWrapper
{
	public Map<String, Object> WrapperMap;

    public TaskWrapper(SObject sObj)
    {
    	WrapperMap = new Map<String, Object>();
        String dateFormat = Wildebeest_Integration__c.getValues('Host').Wildebeest_Date_Format__c != null ? 
                                Wildebeest_Integration__c.getValues('Host').Wildebeest_Date_Format__c : 'dd/MM/yyyy HH:mm:ss';

        if (sObj.getSObjectType() == Schema.Task.getSObjectType())
        {
            Task tsk = (Task)sObj;
            String lastModifiedDate = tsk.LastModifiedDate != null ? tsk.LastModifiedDate.format(dateFormat) : '';
            String reminderDateTime = tsk.ReminderDateTime != null ? tsk.ReminderDateTime.format(dateFormat) : '';
            String systemModstamp = tsk.SystemModstamp != null ? tsk.SystemModstamp.format(dateFormat) : '';
            String createdDate = tsk.CreatedDate != null ? tsk.CreatedDate.format(dateFormat) : '';
            String dueDateTime = tsk.Due_Date_and_Time__c != null ? tsk.Due_Date_and_Time__c.format(dateFormat) : '';

            WrapperMap.put('LastModifiedById', tsk.LastModifiedById);
			WrapperMap.put('InSituDtm__InSitu_TCN_Flag__c', tsk.InSituDtm__InSitu_TCN_Flag__c);
			WrapperMap.put('Id', tsk.Id);
			WrapperMap.put('Assigned_User_Details__c', tsk.Assigned_User_Details__c);
			WrapperMap.put('CreatedDate', createdDate);
			WrapperMap.put('Event_isActive__c', tsk.Event_isActive__c);
			WrapperMap.put('TaskSubtype', tsk.TaskSubtype);
			WrapperMap.put('InSituDtm__InSitu_EN_Hidden_Flag__c', tsk.InSituDtm__InSitu_EN_Hidden_Flag__c);
			WrapperMap.put('IsArchived', tsk.Subject);
			WrapperMap.put('SystemModstamp', systemModstamp);
			WrapperMap.put('Subject', tsk.Subject);
			WrapperMap.put('IsRecurrence', tsk.IsRecurrence);
			WrapperMap.put('Task_isActive__c', tsk.Task_isActive__c);            
			WrapperMap.put('Priority', tsk.Priority);
			WrapperMap.put('WhoId', tsk.WhoId);
			WrapperMap.put('IsDeleted', tsk.IsDeleted);
			WrapperMap.put('InSituDtm__InSitu_Owner_Not_Responsible__c', tsk.InSituDtm__InSitu_Owner_Not_Responsible__c);
			WrapperMap.put('Status', tsk.Status);
			WrapperMap.put('IsReminderSet', tsk.IsReminderSet);
			WrapperMap.put('InSituDtm__InSitu_Contact_Task__c', tsk.InSituDtm__InSitu_Contact_Task__c);
			WrapperMap.put('OwnerId', tsk.OwnerId);
			WrapperMap.put('CreatedById', tsk.CreatedById);
			WrapperMap.put('Last_Modified_User_Details__c', tsk.Last_Modified_User_Details__c);
			WrapperMap.put('InSituDtm__InSitu_EN_Visible_Flag__c', tsk.InSituDtm__InSitu_EN_Visible_Flag__c);
			WrapperMap.put('WhoCount', tsk.WhoCount);
			WrapperMap.put('InSituDtm__InSitu_Send_Email_Reminder__c', tsk.InSituDtm__InSitu_Send_Email_Reminder__c);
			WrapperMap.put('IsClosed', tsk.IsClosed);
			WrapperMap.put('InSituDtm__InSitu_ONTracker__c', tsk.InSituDtm__InSitu_ONTracker__c);
			WrapperMap.put('IsHighPriority', tsk.IsHighPriority);
			WrapperMap.put('ReminderDateTime', reminderDateTime);
			WrapperMap.put('Activities_Created_In_The_Last_Week__c', tsk.Activities_Created_In_The_Last_Week__c);
			WrapperMap.put('WhatCount', tsk.WhatCount);
			WrapperMap.put('Description', tsk.Description);
			WrapperMap.put('WhatId', tsk.WhatId);
			WrapperMap.put('AccountId', tsk.AccountId);
			WrapperMap.put('Due_Date_and_Time__c', dueDateTime);
			WrapperMap.put('LastModifiedDate', lastModifiedDate);
        }
    }
}