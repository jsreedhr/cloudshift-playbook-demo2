/**
 *  @Class Name:    LeadConvertPageControllerTest
 *  @Description:   This is a test class for LeadConvertPageController
 *  @Company:       dQuotient
 *  CreatedDate:    07/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           07/12/2017                  Original Version
 */
@isTest
private class LeadConvertPageControllerTest {
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Wildebeest_Ref__c = 'swq12333';
        insert acc;
        
        Contact contact = bg_Test_Data_Utils.createContact('1', acc.Id);
        contact.AccountId = acc.id;
        insert contact;
        
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook;  
        
        List<Lead> leadList = new List<Lead>();
        Lead lead = bg_Test_Data_Utils.createLead('1');
        lead.OwnerId = u.id;
        lead.FLT_Postal_Code__c = flt.Id;
        lead.Status = 'Ready for Conversion';
        lead.Existing_Contact__c = contact.Id;
        // lead.Existing_Contact__c = contact.Id;
        lead.Key_Account__c =true;
        leadList.add(lead);
        
        Lead lead2 = bg_Test_Data_Utils.createLead('2');
        lead2.OwnerId = u.id;
        lead2.FLT_Postal_Code__c = flt.Id;
        Lead2.Status = 'New';
        lead2.Existing_Contact__c = contact.Id;
        leadList.add(lead2);
        insert leadList;
        
        System.debug('Lead list with status------>'+leadList);
    }
    
    static testMethod void testReadyToConvertLead() {
        
        Lead testLead = [Select Id, Status from Lead Limit 1];
        System.debug('Leads from Test classes '+testLead);
        testLead.Originating_Employee__c = '112233';
        testLead.Status = 'Ready for Conversion';
        testLead.Timescale__c = 'Immediately';
        testLead.Entity_Type__c = 'CCA';
        testLead.Title = 'Title';
        testLead.Originating_Employee__c = '112233';
        update testLead;
        test.startTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadConvertPageController ext = new LeadConvertPageController(sc); 
            ext.loadAction();
            System.assertEquals(testLead.Status, 'Ready for Conversion');
        test.stopTest();
    }
    
    private static testMethod void testNewLead() {
        
        Lead testLead = [Select Id, Status from Lead Limit 1];
        testLead.Status = 'New';
        update testLead;
        test.startTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadConvertPageController ext = new LeadConvertPageController(sc); 
            ext.loadAction();
            System.assertNotEquals(testLead.Status, 'Converted To Opportunity');
        test.stopTest();
    }
    
    private static testMethod void testconvertLead() {
       
        
        Contact c = [select id , accountid from contact limit 1];
        Lead testLead = [Select Id, Status from Lead  Limit 1]; 
        testLead.Status = 'Ready for Conversion';
        update testLead;
        test.startTest();
            PageReference pageRef = Page.Lead_Conversion_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',testLead.id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadConvertPageController ext = new LeadConvertPageController(sc); 
                 ext.oppName='OppName';
                  ext.cont2 = c;
          System.assertNotEquals(null,ext.convertLead());
        test.stopTest();
    }
    
    private static testMethod void testcancelaction() {
        
        Lead testLead = [Select Id, Status from Lead Limit 1];
        testLead.Status = 'Ready for Conversion';
        update testLead;
        test.startTest();
            PageReference pageRef = Page.Lead_Conversion_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',testLead.id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadConvertPageController ext = new LeadConvertPageController(sc); 
            ext.oppName='OppName';
            System.assertNotEquals(null,ext.cancelaction());
        test.stopTest();
    }
    
    private static testMethod void testAccountOwner() {
        User userObj = [select id from user limit 1];
        System.debug('check user id--->'+userObj.Id);
        Lead testLead = [Select Id, Status, Existing_Contact__c,Existing_Contact__r.AccountId, OwnerId from Lead Limit 1];
        System.debug('Owner id---->'+userObj.Id);
        testLead.OwnerId = userObj.Id;
        Account acc = [Select Id, OwnerId From Account Where Name = 'Test Account N'];
        acc.OwnerId = userObj.Id;
        update acc;
        test.startTest();
            PageReference pageRef = Page.Lead_Conversion_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',testLead.id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadConvertPageController ext = new LeadConvertPageController(sc); 
            ext.accnt = acc;
            ext.convertedStatus = null;
            ext.redirectToContact = null;
            ext.loadAction();
            System.assertNotEquals(testLead.Status, 'Converted To Opportunity');
        test.stopTest();
    }


}