/*********************************************************************************
 * bg_PHSAccountSelectionController
 *
 * Controler class for the PHS account selection vf page.
 * Contains methods to build a list of PHS Accounts based on a parent account id,
 * and a method to pass the selected information over to the creation of a new case.
 *
 * Author: Jamie Wooley
 * Created: 05-05-2016
 *
 *********************************************************************************/

public with sharing class bg_PHSAccountSelectionController {
    public Case theCase {get; set;}
    public ApexPages.StandardSetController m_sc {get; set;}
    public PageReference cancelURL {get; set;}

    public bg_PHSAccountSelectionController(ApexPages.StandardSetController stdController) {
        m_sc = stdController;
        cancelURL = m_sc.cancel();

        theCase = bg_PHSAccountSelection_Helper.generateComponentStartPoint();
    }
}