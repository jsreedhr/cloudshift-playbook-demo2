public without sharing class ContentNoteTriggerFunctions {
    
    public static void StampContentNoteDate(List <ContentDocumentLink> lst_notes){
        
        User u = [select Id from user where id=:userinfo.getuserid()];
        
        Set <ID> set_Parentids = new Set <ID>();
        
        for (ContentDocumentLink n : lst_notes){
            IF(n.LinkedEntityId !=null){
                set_Parentids.add(n.LinkedEntityId);
            }
        }
        
        List <Lead> lst_leads = new List <Lead>([Select id, Last_Note_Creation_Date__c, Last_Note_Created_By__c from Lead where Id in: set_Parentids]);
        
        List <Opportunity> lst_opps = new List <Opportunity>([Select id, Last_Note_Creation_Date__c, Last_Note_Created_By__c from Opportunity where Id in: set_Parentids]);
        
        IF(!lst_Leads.isEmpty()){
            for(Lead l: lst_leads){
                l.Last_Note_Creation_Date__c = System.Now();
                l.Last_Note_Created_By__c = u.id;
            }
            Update lst_leads;
        }
        
        IF(!lst_opps.isEmpty()){
            for(Opportunity o: lst_opps){
                o.Last_Note_Creation_Date__c = System.Now();
                o.Last_Note_Created_By__c = u.id;
            }
            Update lst_opps;
        }  
        
        //Event Note Fields
         
        List <Event> lst_events = new List <Event>([Select id, Last_Note_Creation_Date__c, Last_Note_Created_By__c from Event where Id in: set_Parentids]);
         IF(!lst_events.isEmpty()){
            for(Event e: lst_events){
                e.Last_Note_Creation_Date__c = System.Now();
                e.Last_Note_Created_By__c = u.id;
            }
            Update lst_events;
        }
    }
}