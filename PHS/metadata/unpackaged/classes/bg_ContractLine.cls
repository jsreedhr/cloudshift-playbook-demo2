/*
    Class to define the structure of the Contract Line records returned,
    used by the responseWrapper
*/
public class bg_ContractLine
{
	public Integer PHSAccountReference      {get; set;}
    public String PHSAccountName {get; set;}
    public String CurrencySymbol {get; set;}
    public String AgreementTypeName {get; set;}
    public String ProductName {get; set;}
    public String CommencementDate {get; set;}
    public String ExpiryDate {get; set;}
    public String ContractTermName {get; set;}
    public Decimal UnitPrice {get; set;}
    public String ProductRef {get; set;}
    public Integer ContractLineId {get; set;}
    public Decimal ContractLineValue {get; set;}
    public Integer Quantity {get; set;}
    public String ServiceFrequencyName {get; set;}
    public Integer ContractFrequency {get; set;}
    public String DivisionName;
    public Boolean IsCancelled;
    public String PHSAccountFullAddress;
    public Integer PHSLocationAccountReference;
    public String PHSLocationAccountName;
    public String FullSiteDescription;
    public Integer SitedQuantity;

    public String CommencementDateDisplay
    {
        get
        {
            return Date.valueOf(CommencementDate).format();    
        }
    }

    public String ExpiryDateDisplay
    {
        get
        {
            return Date.valueOf(ExpiryDate).format();
        }
    }
}