public class CPQQuoteTriggerFunctions {
    
    public static Boolean isAfterUpdate = true;
    public static Boolean isBeforeUpdate = true;
    public static void SendtoHoSApproval(List <SBQQ__Quote__c> lst_quotes){
        
        for(SBQQ__Quote__c q: lst_quotes){
            
            IF(q.SBQQ__Status__c == 'RSM Approved' && q.Management_Approval_Required__c == TRUE && q.Management_Approved__c == FALSE && q.In_Management_Approval__c == FALSE){
                // create the new approval request to submit
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(q.Id);
                // submit the approval request for processing
                Approval.ProcessResult result = Approval.process(req);
                // display if the reqeust was successful
                System.debug('Submitted for approval successfully: '+result.isSuccess());
                //CPQQuoteTriggerFunctions.setAlreadyCreatedFollowUpTasks();
                System.debug('Locked ?: '+Approval.isLocked(q.id));
                
            }
        }
    }
       
    public static void CreateContractFromAcceptedQuote(List <SBQQ__Quote__c> lst_newquotes, List <SBQQ__Quote__c> lst_oldquotes){
        
        //Declare the variables
        List <Order> lst_orderstocreate = new List <Order>();
        Set <ID> set_quoteids = new Set <ID>();
        Map <Id, Id> map_QuoteId_OrderId = new Map <Id, Id>();
        List <Copy_Quote_Line_Item__c> lst_cpqlstocreate = new List <Copy_Quote_Line_Item__c>();
        List <Access_Time__c> lst_ATstocreate = new List <Access_Time__c>();
        set<Id> setOppIds = new set<Id>();
        for(SBQQ__Quote__c nq: lst_newquotes)
        {
            if(nq.SBQQ__Opportunity2__c != null)
                setOppIds.add(nq.SBQQ__Opportunity2__c);
        }
        map<Id, Opportunity> mapOpp = new map<Id, Opportunity>([SELECT Id, Pricebook2Id from Opportunity where Id IN: setOppIds]);
        
        //Only to be run in an update context
        for(SBQQ__Quote__c nq: lst_newquotes){
            for(SBQQ__Quote__c oq: lst_oldquotes){
                IF(nq.id == oq.id && nq.SBQQ__Status__c == 'Accepted' && oq.SBQQ__Status__c != 'Accepted'){
                    //Create new Order
                    Order o = new Order();
                    o.AccountId = nq.SBQQ__Account__c;
                    o.BillingStreet = nq.SBQQ__BillingStreet__c;
                    o.BillingCity = nq.SBQQ__BillingCity__c;
                    o.BillingState = nq.SBQQ__BillingState__c;
                    o.BillingCountry = nq.SBQQ__BillingCountry__c;
                    o.BillingPostalCode = nq.SBQQ__BillingPostalCode__c;
                    o.Buyer__c = nq.SBQQ__PrimaryContact__c;
                    o.Buyer_PHS_Account__c = nq.Buyer_PHS_Account__c;
                    o.Method_of_Payment__c = nq.Method_of_Payment__c;
                    o.Original_Method_of_Payment__c = nq.Method_of_Payment__c;
                    o.Payment_Terms_Other__c = nq.Payment_Terms_Other__c;
                    o.Payment_Terms__c = nq.SBQQ__PaymentTerms__c;
                    o.Original_Payment_Terms__c = nq.SBQQ__PaymentTerms__c;
                    o.OpportunityId = nq.SBQQ__Opportunity2__c;
                    o.SBQQ__Quote__c = nq.id;
                    o.Status = 'Draft';
                    o.EffectiveDate = System.today() + 10;
                    o.OwnerId = nq.SBQQ__SalesRep__c;
                    o.Installation_Total__c = nq.Quoted_Installation_Total__c;
                    o.Carriage_Total__c = nq.Quoted_Carriage_Total__c;
                    o.DOC_Total__c = nq.Quoted_DoC_Total__c;
                    o.HW_Total__c = nq.Quoted_HW_Total__c;
                    o.Sales_Rep__c = nq.SBQQ__SalesRep__c;
                    
                    if(mapOpp.containsKey(nq.SBQQ__Opportunity2__c)) 
                        o.Pricebook2Id  = mapOpp.get(nq.SBQQ__Opportunity2__c).Pricebook2Id;
                    
                    lst_orderstocreate.add(o);
                    set_quoteids.add(nq.id);
                }
            }
        }
        
        System.debug('Quote Set List is' + set_quoteids.size());
        System.debug('Quotes to Create List is' + lst_orderstocreate.size());
        
        insert lst_orderstocreate;
        
        for(Order o: lst_orderstocreate){
            map_QuoteId_OrderId.put(o.SBQQ__Quote__c, o.id);
        }
         
        
        //Create Line Items 
        List <SBQQ__QuoteLine__c> lst_quotelines = new List <SBQQ__QuoteLine__c>([SELECT Id, SBQQ__Quote__c, Agreement_Type__c, Contract_Type__c, PHSCPQ_Standard_Contract_Term__c, Maximum_Price__c, Maximum_Total__c, Target_Price__c, Target_Total__c, Minimum_Price__c, Minimum_Total__c, SBQQ__NetPrice__c, SBQQ__NetTotal__c, SBQQ__ProductName__c, SBQQ__Product__c, SBQQ__ProductCode__c, PHSCPQ_Product_Range__c, SBQQ__Quantity__c, PHSCPQ_Standard_Service_Frequency__c, Trial_Product__c, Trial_Length__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c in: set_quoteids]);
        
        System.debug(lst_quotelines.size());
        
        IF(!lst_quotelines.isEmpty()){
            for(SBQQ__QuoteLine__c ql: lst_quotelines){
                Copy_Quote_Line_Item__c cpql = new Copy_Quote_Line_Item__c();
                cpql.Agreement__c =  map_QuoteId_OrderId.get(ql.SBQQ__Quote__c);
                cpql.Agreement_Type__c = ql.Agreement_Type__c;
                cpql.Business_Type__c = ql.Contract_Type__c;
                cpql.Contract_Term__c = ql.PHSCPQ_Standard_Contract_Term__c;
                cpql.Maximum_Price__c = ql.Maximum_Price__c;
                cpql.Maximum_Total__c = ql.Maximum_Total__c;
                cpql.Target_Price__c = ql.Target_Price__c;
                cpql.Target_Total__c = ql.Target_Total__c;
                cpql.Minimum_Price__c = ql.Minimum_Price__c;
                cpql.Minimum_Total__c = ql.Minimum_Total__c;
                cpql.Net_Price__c = ql.SBQQ__NetPrice__c;
                cpql.Net_Total__c = ql.SBQQ__NetTotal__c;
                cpql.Name = ql.SBQQ__ProductName__c;
                cpql.Product__c = ql.SBQQ__Product__c;
                cpql.Product_Code__c = ql.SBQQ__ProductCode__c;
                cpql.Product_Range__c = ql.PHSCPQ_Product_Range__c;
                cpql.Quantity__c = ql.SBQQ__Quantity__c;
                cpql.Quote__c = ql.SBQQ__Quote__c;
                cpql.Service_Frequency__c = ql.PHSCPQ_Standard_Service_Frequency__c;
                cpql.Trial_Product__c = ql.Trial_Product__c;
                cpql.Trial_Days__c = ql.Trial_Length__c;
                lst_cpqlstocreate.add(cpql);
            }
            insert lst_cpqlstocreate;
        }
        
    }
}