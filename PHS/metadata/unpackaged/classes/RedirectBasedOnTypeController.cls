public with sharing class RedirectBasedOnTypeController
{
    public Id recordId{get;set;}
    public String QuoteType{get;set;}
    public RedirectBasedOnTypeController()
    {
        recordId =  apexpages.currentpage().getparameters().get('recordId');
        System.Debug('Le Record Id: '+recordId);
        Order orderObj = [Select id, SBQQ__Quote__c,SBQQ__Quote__r.Quote_Type__c FROM Order WHERE Id=:recordId LIMIT 1];
        System.Debug('Le Order Object: '+orderObj);
        if(orderObj!=NULL && orderObj.SBQQ__Quote__c!=NULL && orderObj.SBQQ__Quote__r.Quote_Type__c!=NULL)
        {
            if(orderObj.SBQQ__Quote__r.Quote_Type__c.contains('Hygiene'))
                QuoteType = 'ManageContractLineItemsHygiene';
            else if(orderObj.SBQQ__Quote__r.Quote_Type__c.contains('Greenleaf'))
                QuoteType = 'ManageContractLineItemsGreenleaf';
            else if(orderObj.SBQQ__Quote__r.Quote_Type__c.contains('Wastekit'))
                QuoteType = 'ManageContractLineItemsWastekit';
        }
        else
            QuoteType = 'ManageContractLineItemsHygiene';
    }
    public PageReference redirectAction()
    {
        PageReference pageRef;
        pageRef= new PageReference('/apex/'+QuoteType+'?scontrolCaching=1&id='+recordId);
        System.Debug('Redirect Link: '+'/apex/'+QuoteType+'?scontrolCaching=1&id='+recordId);
        pageRef.setRedirect(True);
        return pageRef;
    }


}