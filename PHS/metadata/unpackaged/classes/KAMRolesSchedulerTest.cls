/*
*  Class Name:  KAMRolesSchedulerTest
*  Description: This class covers KAMRolesBatch class
*  Company: dQuotient
*  CreatedDate: 06-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer      		Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      06-Nov-2017            Orginal Version
*/
@isTest
private class KAMRolesSchedulerTest{
    @isTest
    private static void rolesSchedulerTest(){
        Account account = new Account(
				Name = 'Business Entity',
				BillingPostalCode = 'CF83 1XH',
				Key_Account__c = TRUE,
				Test_Key_Account__c = TRUE,
				HasNegotiatorResponsibility__c = FALSE,
				Phone = '01273464629'
			);
        insert account;
        
        Test.startTest();
       // KAMRolesScheduler scheduler = new KAMRolesScheduler();
       // scheduler.execute(null);
        Test.stopTest();
    }
}