/**
 *  @Class Name:    OpportunityReallocationControllerTest
 *  @Description:   This is a test class for OpportunityReallocationController
 *  @Company:       dQuotient
 *  CreatedDate:    30/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           30/10/2017                  Original Version
 */
@isTest
private class OpportunityReallocationControllerTest {
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Front_Line_Team__c flt = bg_Test_Data_Utils.createFLT('AB10');
        insert flt;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook;    
        
        Account acc = bg_Test_Data_Utils.createAccount('N');
        insert acc;
        
        Campaign camp = bg_Test_Data_Utils.createCampaign('Test Campaign');
        insert camp;
            
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        opp.OwnerId = u.Id;
        insert opp;
    }
    
    // Test reallocateSalesOpportunity method in OpportunityReallocationController
	private static testMethod void testreallocateSalesOpportunity() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.SalesRSM__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        flt.OutboundTSTeamLeader__c = u.id;
        update flt;
        system.debug('flt--->' +flt);
        oppo.DM_Postal_District__c =flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
        test.stopTest();
	}
	
    // Test reallocateOutboundOpportunity method in OpportunityReallocationController
	private static testMethod void testreallocateOutboundOpportunity() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.SalesRSM__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        flt.OutboundTSTeamLeader__c = u.id;
        update flt;
        system.debug('flt--->' +flt);
        oppo.DM_Postal_District__c =flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are not null and Off_Sick_Annual_Leave__c is true;
	private static testMethod void testOpportunitySendtoOutbound() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.SalesRSM__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        flt.OutboundTSTeamLeader__c = u.id;
        update flt;
        system.debug('flt--->' +flt);
        oppo.DM_Postal_District__c =flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
	
	// Test transfer methods when Front_Line_Team__c is null for an opportunity
	private static testMethod void testOpportunitynullFLT() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}

    // Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are null and Off_Sick_Annual_Leave__c is true;
	private static testMethod void testOpportunitynull() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}

    // Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are null and Off_Sick_Annual_Leave__c is false;
	private static testMethod void testOpportunityfalse() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}

    // Test transfer methods when Sales_Rep__c, OutboundTSAgent__c, SalesRSM__c, OutboundTSTeamLeader__c are not null and Off_Sick_Annual_Leave__c is false;
	private static testMethod void testOpportunityfalsenotnullFLT() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.SalesRSM__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        flt.OutboundTSTeamLeader__c = u.id;
        update flt;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c are null , Off_Sick_Annual_Leave__c is false and SalesRSM__c, OutboundTSTeamLeader__c not null;
	private static testMethod void testOpportunitynullcheck() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.SalesRSM__c = u.id;
        flt.OutboundTSTeamLeader__c = u.id;
        update flt;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c not null , Off_Sick_Annual_Leave__c is true and SalesRSM__c, OutboundTSTeamLeader__c are null;
	private static testMethod void testOpportunitynullFLTfields() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=false;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        update flt;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
	
	// Test transfer methods when Sales_Rep__c, OutboundTSAgent__c not null , Off_Sick_Annual_Leave__c is true and SalesRSM__c, OutboundTSTeamLeader__c are null;
    private static testMethod void testOpportunitycheckFLTfieldsnull() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        u.Off_Sick_Annual_Leave__c=true;
        update u;
        Front_Line_Team__c flt = [Select Name, Sales_Rep__c, SalesRSM__c, OutboundTSAgent__c, OutboundTSTeamLeader__c from Front_Line_Team__c where Name = 'AB10'];
        flt.Sales_Rep__c = u.id;
        flt.OutboundTSAgent__c = u.id;
        update flt;
        Opportunity oppo = [Select Id, DM_Postal_District__c, OwnerId from Opportunity where OwnerId= :u.Id];
        oppo.DM_Postal_District__c = flt.id;
        update oppo;
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.SendToFieldSalesOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateSalesOpportunity(oppo.id);
            
            Test.setCurrentPageReference(new PageReference('Page.SendToOutboundOpportunity')); 
            System.currentPageReference().getParameters().put('id', oppo.Id);
            OpportunityReallocationController.reallocateOutboundOpportunity(oppo.Id);
        test.stopTest();
	}
}