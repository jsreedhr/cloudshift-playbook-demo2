public with sharing class ch_Event_Helper{
    
    public static boolean isBeforeUpdate = true;
     private static boolean run = true;
    public static boolean runOnce() {
            if (run) {
                run = false;
                return true;
            } else {
                return run;
            }

        }
    
    public static void EventTrigger(List<Event> events){
        
        List <Lead> leadList = new List <Lead>();
        
        if(userinfo.getUserRoleId()!=NULL && userinfo.getUserRoleId().length()>0){
            UserRole u = [SELECT Name FROM UserRole where Id =: userinfo.getUserRoleId()];
            
            if(u.name !=null && (u.name).containsIgnoreCase('Teleappointing') || u.name !=null && (u.name).containsIgnoreCase('Tele-Appointing')){
                String lead_prefix = Schema.SObjectType.Lead.getKeyPrefix();
                Set<Id> leadIds = new Set<Id>();
                for(Event e : events){
                    if(e.whoID !=null && String.valueof(e.WhoId).startsWith(lead_prefix)){
                        leadIds.add(e.WhoId);
                    }
                }
                
                IF(!leadIds.isEmpty()){
                    leadList = [Select id, Tele_Appointer__c, Tele_Appointer_Email__c from Lead where (Tele_Appointer__c = NULL
                                                                              OR Tele_Appointer__c = '') AND  Id IN: leadIds];
                    
                    if(!leadlist.isEmpty()){
                        for(Lead l : leadList){
                            l.Tele_Appointer__c = userinfo.getName();
                            l.Tele_Appointer_Email__c = userinfo.getUserEmail();
                        }
                        update leadList;
                    }                    
                }              
            }
        }
}
    
    /* Mathod Name : populateLeadOpportunityData
    *  created By : Md Rizwan
    *  created Date : 22-Nov-2017
    *  Description : updates few fields of Event record based on values in related Lead/Opportunity record
    */
    public static void populateLeadOpportunityData(List<Event> eventList){
        
        Set<Id> leadIdSet = new Set<Id>();
        Set<Id> opportunityIdSet = new Set<Id>();
        String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();
        String opportunityPrefix = Schema.SObjectType.Opportunity.getKeyPrefix();
        
        for(Event event:eventList){
            if(event.WhoId!=null && ((String)event.WhoId).startsWith(leadPrefix))
                leadIdset.add(event.WhoId);
            else if(event.WhatId!=null && ((String)event.WhatId).startsWith(opportunityPrefix))
                opportunityIdSet.add(event.WhatId);
        }
        // populate field values from related leads and opportunities
        if(leadIdSet.size()>0){
            Map<Id,Lead> leadMap = new Map<Id,Lead>();
            List<Lead> leadList = [Select Id, Lead_Score_Total__c, LeadSource, Campaigns__c, Status,Postal_District__c,FLT_Postal_Code__r.name From Lead Where Id IN: leadIdSet];
            for(Lead lead:leadList){
                if(!leadMap.containsKey(lead.id))
                    leadMap.put(lead.Id, lead);
            }
            for(Event event:eventList){
                if(event.WhoId != NULL && leadMap.containsKey(event.WhoId)){
                    Lead lead = leadMap.get(event.WhoId);
                    event.Score__c = lead.Lead_Score_Total__c;
                    event.Source__c = lead.LeadSource;
                    event.Campaigns__c = lead.Campaigns__c;
                    event.Status__c = lead.Status;
                  //  event.Postal_District__c = lead.Postal_District__c;
                  event.Postal_District__c = lead.FLT_Postal_Code__r.name;
                }
            }
        }
        if(opportunityIdSet.size()>0){
            Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>();
            List<Opportunity> opportunityList = [Select Id, LeadSource,StageName, Campaigns__c, Amount,DM_Postal_District__r.Name From Opportunity Where Id IN: opportunityIdSet];
            for(Opportunity opp:opportunityList){
                if(!opportunityMap.containsKey(opp.id))
                    opportunityMap.put(opp.Id, opp);
            }
            for(Event event:eventList){
             //    if (!event.isChild){
                     
                if(event.WhatId != NULL && opportunityMap.containsKey(event.WhatId)){
                    Opportunity opp = opportunityMap.get(event.WhatId);
                    event.Source__c = opp.LeadSource;
                    event.Stage__c = opp.StageName;
                    event.Amount__c = opp.Amount;
                    event.Campaigns__c = opp.Campaigns__c;
                    event.Postal_District__c = opp.DM_Postal_District__r.Name;
                }
                
                // }
            }
        }
        
    }
}