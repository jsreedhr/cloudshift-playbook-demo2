/********************************************************************************
* bg_Wildebeest_Helper :
*
* Helper class to synchronise Case and Case related objects to Widebeest
*
* Created By: SA
* Created Date: 23-03-2016 
*
* Changes: KH 16-11-16 Updated synchronise for Task Updates to check for specific
*                      fields changing
*          KH 12-12-16 Updated synchronise for Case Updates to check for specific
*                      fields changing
*          KH 31-06-17 Updated to only sync Case Comments and Tasks if the parent 
*                      Case is sycned and also cleaned up synchronise for Case
*********************************************************************************/
public with sharing class bg_Wildebeest_Helper
{
    private static Set<Id> casesBeingProcessed = new Set<Id>();
    private static Set<Id> tasksBeingProcessed = new Set<Id>();

    /*
        Called from after insert trigger.
        Send to WB if Sync_Wildebeest__c is True
    */
    public static void synchronise(List<Case> caseRecords, List<Case> caseRecordsToUpdate)
    {    
        Map<Id, Case> caseRecordsToSyncMap = new Map<Id,Case>();

        for (Case caseRecord : caseRecords) 
        {
            if(caseRecord.Sync_Wildebeest__c) 
            {
                caseRecordsToSyncMap.put(caseRecord.id, caseRecord);
            }
        }


        if(!caseRecordsToSyncMap.isEmpty() || !caseRecordsToUpdate.isEmpty()) 
        {
            bg_QueueableJob.enqueueJob(new bg_QueueableJob.QueueableJobClass(
                new bg_QueueableJob.QueueableCalloutJob('Case', caseRecordsToSyncMap, null), 
                new bg_QueueableJob.QueueableUpdateJob(caseRecordsToUpdate)));
        }
    }

    /*
        Called from after update trigger.
        Send to WB if Sync_Wildebeest__c is True
    */
    public static void synchronise(List<Case> caseRecords, Map<Id, Case> oldCaseRecordsMap)
    {   
        List<Case> caseRecordsToSynchronise = new List<Case>();
        List<Case> caseRecordsToUpdate = new List<Case>();

        for (Case caseRecord : caseRecords) 
        {
            if (!casesBeingProcessed.contains(caseRecord.Id))
            {
                casesBeingProcessed.add(caseRecord.Id);
                Case oldCase = oldCaseRecordsMap.get(caseRecord.Id);

                Boolean caseRecordIsReadyToSync = caseRecord.Sync_Wildebeest__c;
                Boolean caseRecordHasTheSameCaseRef = oldCaseRecordsMap.get(caseRecord.Id).Case_Ref__c == caseRecord.Case_Ref__c;
                //Boolean caseSyncMessageHasChanged = oldCaseRecordsMap.get(caseRecord.Id).Sync_Wildebeest_Message__c == caseRecord.Sync_Wildebeest_Message__c;
                Boolean relevantFieldsChanged = (caseRecord.Subject != oldCase.Subject || caseRecord.Status != oldCase.Status || caseRecord.Type != oldCase.Type || caseRecord.Case_Reason__c != oldCase.Case_Reason__c || caseRecord.Description != oldCase.Description || caseRecord.PHS_Account__c != oldCase.PHS_Account__c);
                
                if (caseRecordIsReadyToSync && caseRecordHasTheSameCaseRef && relevantFieldsChanged) 
                {
                    caseRecordsToSynchronise.add(caseRecord);
                }
            }
            else
            {
                 system.debug('AG - skipped case, already in processed list: ' + caseRecord.Id);
            }
        }

        if (!caseRecordsToSynchronise.isEmpty() || !caseRecordsToUpdate.isEmpty())
        {
            synchronise(caseRecordsToSynchronise, caseRecordsToUpdate);
        }
        else
        {
             system.debug('AG - skipped sync since lists are empty');
        }
    }


    /*
        Called from after insert trigger.
        Send to WB if Case.Sync_Wildebeest__c is True
    */
    public static void synchronise(Map<Id, CaseComment> caseCommentRecords)
    {
        /*
         * Generate a set of parent case ids
         */
        Set<Id> parentCaseIds = new Set<Id>();
        for (CaseComment caseCommentRecord : caseCommentRecords.values()) 
        {
            parentCaseIds.add(caseCommentRecord.ParentId);
        }

        Map<Id, Case> caseRecordsMap = new Map<Id,Case>([SELECT Id, Case_Ref__c, Sync_Wildebeest__c FROM Case WHERE Id IN :parentCaseIds]);

        /*
         * Generate the case comment and case maps
         */
        Map<Id, Case> caseCommentToCaseMap = new Map<Id,Case>();
        Map<Id,CaseComment> caseCommentMap = new Map<Id,CaseComment>();
        for (CaseComment caseCommentRecord :  [SELECT ParentId, LastModifiedDate, LastModifiedById, CreatedById, CommentBody, CreatedDate, Id
                                                FROM CaseComment
                                                WHERE Id IN: caseCommentRecords.keySet()]) //Get details including friendly names of created/modified
        {
            Boolean caseRecordIsReadyToSync = caseRecordsMap.get(caseCommentRecord.ParentId).Sync_Wildebeest__c;
            Boolean caseHasWildebeestRef = caseRecordsMap.get(caseCommentRecord.ParentId).Case_Ref__c != null && caseRecordsMap.get(caseCommentRecord.ParentId).Case_Ref__c != '';
            if(caseRecordIsReadyToSync && caseHasWildebeestRef) 
            {
                caseCommentMap.put(caseCommentRecord.Id, caseCommentRecord);
                caseCommentToCaseMap.put(caseCommentRecord.Id, caseRecordsMap.get(caseCommentRecord.ParentId));
            }
        }

        /*
         * Send the records to be updated in Wildebeest
         */
        if(!caseCommentMap.isEmpty()) 
        {
            bg_QueueableJob.enqueueJob(new bg_QueueableJob.QueueableJobClass(
                new bg_QueueableJob.QueueableCalloutJob('CaseComment', caseCommentMap, caseCommentToCaseMap), null));
        }
    }

    /*
        Called from after update trigger.
        Send to WB if Case.Sync_Wildebeest__c is True
    */
    public static void synchronise(Map<Id, CaseComment> caseComments, Map<Id, CaseComment> oldMap)
    {
        synchronise(caseComments);
    }


    /*
        Called from after insert trigger.
        Send to WB if Case.Sync_Wildebeest__c is True
    */
    public static void synchronise(List<Task> taskRecords)
    {
        /*
         * Generate a set of parent case ids
         */
        Set<Id> parentCaseIds = new Set<Id>();
        Boolean parentObjectIsACase;
        for (Task taskRecord : taskRecords) 
        {
            parentObjectIsACase = taskRecord.WhatId != null && bg_WildebeestCallout.getType(taskRecord.WhatId)  == 'Case';
            if(parentObjectIsACase)
            {
                parentCaseIds.add(taskRecord.WhatId);
            }
        }

        /*
         * Generate the task map
         */
        Map<Id, Case> caseRecordsMap = new Map<Id,Case>([select id, Case_Ref__c, Sync_Wildebeest__c from Case where Id in :parentCaseIds]);
        Map<Id,Task> taskRecordsMap = new Map<Id,Task>();
        Map<Id, Case> taskIdToCaseMap = new Map<Id,Case>();
        for (Task taskRecord : taskRecords) 
        {
            Boolean caseRecordIsReadyToSync;
            Boolean caseHasWildebeestRef;
            if (parentObjectIsACase)
            {
                caseRecordIsReadyToSync = caseRecordsMap.containsKey(taskRecord.WhatId) && caseRecordsMap.get(taskRecord.WhatId).Sync_Wildebeest__c;
                caseHasWildebeestRef = caseRecordsMap.get(taskRecord.WhatId).Case_Ref__c != null && caseRecordsMap.get(taskRecord.WhatId).Case_Ref__c != '';

                if(caseRecordIsReadyToSync && caseHasWildebeestRef) 
                {
                    taskRecordsMap.put(taskRecord.Id, taskRecord);
                    taskIdToCaseMap.put(taskRecord.Id, caseRecordsMap.get(taskRecord.WhatId));
                }
            }
        }

        if(!taskRecordsMap.isEmpty()) 
        {
            bg_QueueableJob.enqueueJob(new bg_QueueableJob.QueueableJobClass(
                new bg_QueueableJob.QueueableCalloutJob('Task', taskRecordsMap, taskIdToCaseMap), null));
        }

    }


    /*
        Called from after update trigger.
        Send to WB if Case.Sync_Wildebeest__c is True
    */
    public static void synchronise(List<Task> Tasks, Map<Id, Task> oldMap)
    {
        List<Task> updatedTasks = new List<Task>();

        for (Task newTask : Tasks)
        {
            if (!tasksBeingProcessed.contains(newTask.Id))
            {
                tasksBeingProcessed.add(newTask.Id);
                Task oldTask = oldMap.get(newTask.Id);
                
                if (newTask.Priority != oldTask.Priority || newTask.Subject != oldTask.Subject || newTask.Status != oldTask.Status || newTask.Description != oldTask.Description || newTask.OwnerId != oldTask.OwnerId || newTask.Due_Date_and_Time__c != oldTask.Due_Date_and_Time__c)
                {
                    updatedTasks.add(newTask);
                }
            }
        }
        synchronise(updatedTasks);
    }

    public static Map<Id, Case> updateCaseCommentDate(List<CaseComment> caseCommentRecords, Map<Id, SObject> rCaseRecordsToUpdate)
    {
        Set<Id> caseIds = new Set<Id>();
        Map<Id, Case> caseRecordUpdates = new Map<Id, Case>();

        for(CaseComment caseCommentRecord : caseCommentRecords)
        {
            caseIds.add(caseCommentRecord.ParentId);
        }

        List<Case> caseRecords = [Select Id From Case Where Id IN :caseIds];

        for(Case caseRecord : caseRecords)
        {
            Case caseRecordToProcess;
            if(rCaseRecordsToUpdate.containsKey(caseRecord.Id))
            {
                caseRecordToProcess = (case) rCaseRecordsToUpdate.get(caseRecord.Id);
            }
            else
            {
                caseRecordToProcess = caseRecord;
            }

            caseRecordToProcess.Case_Comment_Added_Date__c = datetime.now();

            caseRecordUpdates.put(caseRecord.Id, caseRecordToProcess);
        }

        return caseRecordUpdates;
    }


    /*
        Method to query existing data from the Wildebeest System
        // Test PHS Account References: 288, 13015, 4353727, 129527
    */
    static public bg_phsAccountInfoResponse getExistingContractLines(ID accountID, Set<String> phsAccountReferences, Boolean contractLines, Boolean saleOrderLines, Boolean visits, Boolean debtProfile, String strCalloutMethod)
    {
        bg_phsAccountInfoResponse phsAccountInfo = new bg_phsAccountInfoResponse();
        try 
        {
            Boolean calloutsLessThanLimit = Limits.getCallouts() < Limits.getLimitCallouts();

            if (bg_WildebeestCallout.settingAreValid(null) & calloutsLessThanLimit)
            {   
                String token = bg_WildebeestCallout.login();
                Boolean loginHasWorked = token != null;

                if (loginHasWorked)
                {
                    bg_phsAccountInfoRequest newRequest = new bg_phsAccountInfoRequest();
                    newRequest.PHSAccountIDS = phsAccountReferences;
                    newRequest.ContractLines = contractLines;
                    newRequest.SaleOrderLines = saleOrderLines;
                    newRequest.Visits = visits;
                    newRequest.DebtProfile = debtProfile;
                    newRequest.ContractLineLimit = 1000;
                    String response = bg_WildebeestCallout.send(strCalloutMethod, null, bg_Serializer.toJsonStr(newRequest), token, accountID, null);
                    if (response != null)
                    {
                        system.debug('===response====='+response);//BuyerCustomerDetails
                        phsAccountInfo = bg_WildebeestCallout.parsePHSAccountInfo(response);
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        return phsAccountInfo;
    }
}