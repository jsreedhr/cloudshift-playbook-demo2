/*********************************************************************************
 * bg_FLTHelper_Tests
 *
 * A unit tests for the FLT object.
 * 
 * Author: Kash Hussain (BrightGen Ltd)
 * Created: 04-07-2016
 *
 *********************************************************************************/
@isTest
private class bg_FLTHelper_Tests
{
	/*
		Unit test to ensure that the Lead is updated to the correct FLT record.
	*/
    static testMethod void testFLTchangeOnLead()
    {
    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{
    		Front_Line_Team__c flt = new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02');
	        insert flt;
                            
	        test.startTest();

	        Lead leadRecord = new Lead(LastName = 'Test Lead', Company = 'Test Co.' , PostalCode = 'AB1 1AA');
	        insert leadRecord;

	        test.stopTest();

	        leadRecord = [SELECT Id, FLT_Number__c, FLT_Region_Name__c FROM Lead WHERE Id = :leadRecord.Id LIMIT 1];     

	        System.assertNotEquals(null, leadRecord.FLT_Number__c);
	        System.assertNotEquals(null, leadRecord.FLT_Region_Name__c);
	        System.assertEquals('Scotland & NI', leadRecord.FLT_Region_Name__c);
	        System.assertEquals('FLT 02', leadRecord.FLT_Number__c);

	        flt.FLTNo__c = 'FLT 05';
	        update flt;

			leadRecord = [SELECT Id, FLT_Number__c, FLT_Region_Name__c FROM Lead WHERE Id = :leadRecord.Id LIMIT 1];
			System.assertEquals('FLT 05', leadRecord.FLT_Number__c);				        
    	}
    }


    /*
		Unit test to ensure that the Account is updated to the correct FLT record.
	*/
    static testMethod void testFLTAndRegionOnInsert()
    {

    	User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	System.runAs(userToRunAs)
    	{
    		Front_Line_Team__c flt = new Front_Line_Team__c(Name = 'AB1', Region_Name__c = 'Scotland & NI', FLTNo__c = 'FLT 02');
	        insert flt;

	        test.startTest();

	        Account acct = new Account(Name = 'Test Account', BillingPostalCode = 'AB1 1AA');
	        insert acct;


	        acct = [SELECT Id, FLT_Number__c, FLT_Region_Name__c, BillingPostalCode, Name FROM Account WHERE Id = :acct.Id LIMIT 1];

	        System.assertNotEquals(null, acct.FLT_Number__c);
	        System.assertNotEquals(null, acct.FLT_Region_Name__c);
	        System.assertEquals('Scotland & NI', acct.FLT_Region_Name__c);
	        System.assertEquals('FLT 02', acct.FLT_Number__c);

	        flt.FLTNo__c = 'FLT 05';
	        update flt;
	        test.stopTest();

	        acct = [SELECT Id, FLT_Number__c, FLT_Region_Name__c FROM Account WHERE Id = :acct.Id LIMIT 1];
			System.assertEquals('FLT 05', acct.FLT_Number__c);				        
    	}
    }
    /*
		Unit test to ensure that the Lead is updated to the correct FLT record.
	*/
    static testMethod void testRegionOutput()
    {
  	             
        String [] expectedResult = new List<String>();
        expectedResult.add('M1');
        expectedResult.add('M60');
        expectedResult.add('CR2');
        expectedResult.add('DN55');
        expectedResult.add('W1A');
        expectedResult.add('EC1A');
   
        String [] postCode = new List<String>();
            postCode.add('M1 1AA');
        	postCode.add('M601NW');
        	postCode.add('CR2 6XH');
        	postCode.add('DN55  1PT');
        	postCode.add('W1A 1 H Q');
       		postCode.add('EC1A 1BB');
        
	    test.startTest();
        	for (Integer i = 0; i<postCode.size(); i++) {  			
   				String result = bg_FLTHelper.determineOutcode(postCode[i]);	
                System.assertEquals(result, expectedResult[i]);               
			}			       	     	     	       	         
            test.stopTest();		        
    	}  
}