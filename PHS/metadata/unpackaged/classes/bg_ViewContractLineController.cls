/*********************************************************************************
 * bg_ViewContractLineController:
 *
 * Controller for the Contract Line Visualforce page.
 * 
 * Author: Tom Morris- BrightGen Ltd
 * Created: 03-11-2016
 *
 *********************************************************************************/

public with sharing class bg_ViewContractLineController
{
	private final Account acct;

    public bg_phsAccountInfoResponse response;

	List<PHS_Account__c> phsAccounts = new List<PHS_Account__c>();
	Set<String> phsAccountReferences = new Set<String>();
    public Set<Id> contractLinesDisplay{get;set;}
    public List<bg_ContractLine> contractLines{get;set;}
    public Set<Id> salesLinesDisplay{get;set;}
    public Set<Id> visitsDisplay{get;set;}
    public Boolean isContractLine{get;set;}
    public Boolean isSalesLine{get;set;}
    public Boolean isVisits{get;set;}
    public Boolean rendered{get;set;}
    

    public bg_ViewContractLineController(ApexPages.StandardController stdController)
    {
        this.acct = (Account)stdController.getRecord();
        // Find the PHS Accounts related to the Account
        phsAccounts = [SELECT Id, Wildebeest_Ref__c FROM PHS_Account__c WHERE Salesforce_Account__c =: acct.Id];

        contractLines = new List<bg_ContractLine>();
        rendered = FALSE;
        
        for(PHS_Account__c phsAcct : phsAccounts)
        {
            if (phsAcct.Wildebeest_Ref__c != null && phsAcct.Wildebeest_Ref__c != '')
            {
                phsAccountReferences.add(phsAcct.Wildebeest_Ref__c);
            }
        }
    }

    // Returns the number of PHS Accounts related to the Account
    public Integer getPHSAccountCount()
    {
    	return phsAccounts.size();
    }

    public void setUserinput()
    {
        system.debug('**setUserinput**');
        system.debug('**acct**: ' + acct);
        system.debug('**phsAccounts**: ' + phsAccountReferences);
        // Returns the Contract Line response from WB 
        response = bg_Wildebeest_Helper.getExistingContractLines(acct.Id, phsAccountReferences, true, false, false, false, 'CustomerDetails');
        system.debug('**response**: ' + response);          
        this.isContractLine = isContractLine;
        
        // Checks to see if the Contract Lines checkbox is set
        if (isContractLine==true && response.ResultSize != 0 && response != null)
        {
            // Fill the list on the page
            contractLines = response.contractLines;
            // Make the list visible
            rendered = TRUE;
        }
        else
        {
            // Empty the Contract Lines list
            contractLines.clear();
            // Make the list invisible on the page
            rendered = FALSE;
        }
    } 
}