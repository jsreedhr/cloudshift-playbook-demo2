public with sharing class CreateNewLookupControllerMod {
    
    public String lookupApi {get; set;}
    public String lookupLabel {get; set;}

    public Boolean showModal {get; set;}
    public SObject lookupSObject {get; set;}
    public List<FieldWrapper> fieldListWrapper {get; set;}

    public CreateNewLookupControllerMod() {
        showModal = false;
    }

    private Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

    public void createLookupFields() {
        lookupApi = Apexpages.currentPage().getParameters().get('lookupApi');
        lookupLabel = Apexpages.currentPage().getParameters().get('lookupLabel');
        System.debug('check lookupApi, lookupLabel : '+lookupApi + ','+lookupLabel);

        Schema.SObjectType sot = gd.get(lookupApi);
        if (sot == null) return;

        lookupSObject = sot.newSObject();
        
        Map<String,AutocompleteObjectFieldset__c> customSettingsMap = AutocompleteObjectFieldset__c.getAll();
        Schema.DescribeSObjectResult describeSObject = sot.getDescribe();
        Schema.FieldSet fieldSetObj = describeSObject.FieldSets.getMap().get(customSettingsMap.get(lookupApi).Fieldset_API__c);
        System.debug('check field set------  '+customSettingsMap.get(lookupApi).Fieldset_API__c);
        List<Schema.FieldSetMember> fieldSetFields = new List<Schema.FieldSetMember>();
        if(fieldSetObj != null){
            fieldSetFields = fieldSetObj.getFields();
        }
        fieldListWrapper = new List<FieldWrapper>();
        if(!fieldSetFields.isEmpty()){
            showModal = true;
            for(Schema.FieldSetMember objMember : fieldSetFields){
                FieldWrapper newWrapperObj = new FieldWrapper(objMember.getLabel(),String.valueOf(objMember.getType()),objMember.getDbRequired(),objMember.getFieldPath());
                fieldListWrapper.add(newWrapperObj);
            }
        }
        System.debug('cgeck wrapper-----    '+fieldListWrapper);
        return;
    }

    public void saveLookup(){
       try{
           insert lookupSObject;
       }catch(Exception e){
           System.debug('Error save--->'+e.getMessage());
       }
   }

   public void closeCreateLookup() {
       showModal = false;
   }

    public class FieldWrapper {
        public String label{get;set;}
        public String type{get;set;}
        public Boolean required{get;set;}
        public String fieldAPI{get;set;}

        public FieldWrapper(String label,String type,Boolean required,String fieldAPI){
            this.label = label;
            this.type = type;
            this.required = required;
            this.fieldAPI = fieldAPI;
        }
    }
}