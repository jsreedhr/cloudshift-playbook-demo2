public without sharing class LeadProductsController {
    public string errOnPage {
        get;
        set;
    }
    public list < Lead_Product__c > listPortLinks {
        get;
        set;
    }
    public list < lead > listLead {
        get;
        set;
    }
    public string rowNo {
        get;
        set;
    }
    Public String LeadId;
    public integer noOfRows {
        get;
        set;
    }
    //public integer projectDuration{get; set;}
    //public map<string,integer> mapMonthNo;
    //public map<integer,string> mapNoMonth;
    public List < booleanProjectWrapper > lstOfbooleanProjectWrapper {
        get;
        set;
    }
    public boolean selectAll {
        get;
        set;
    }
    public Boolean showerror {
        get;
        set;
    }
    public integer errorPos {
        get;
        set;
    }
    //constructor to grt the records

    public LeadProductsController(apexpages.standardController stdController) {
        errOnPage = '';
        listLead = new list < lead > ();
        listPortLinks = new list < Lead_Product__c > ();
        lstOfbooleanProjectWrapper = new List < booleanProjectWrapper > ();
        selectAll = false;
        noOfRows = 1;
        LeadId = apexpages.currentpage().getparameters().get('id');
        listLead = [select FirstName, LastName, ID from Lead where id =: LeadId limit 1];
        //	listPortLinks = [select Id, Lead__c, Product__c, Campaign__c, Quantity__c, Service_Frequency__c, Floor_Price__c, Annual_Cost__c, High_Price__c, Trade_Price__c, Contract_Type__c from Lead_Product__c where Lead__c =: LeadId limit 4999];
        listPortLinks = [select Id, Lead__c, Product__c, Campaign__c, Quantity__c, Floor_Price__c, Annual_Cost__c, High_Price__c, Trade_Price__c, Contract_Type__c from Lead_Product__c where Lead__c =: LeadId limit 4999];

        for (Lead_Product__c obj_portlink: listPortLinks) {
            Product2 prod1 = new Product2();
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, obj_portlink, prod1));
        }
    }

    // To Create a new lead product record on click of Add row button


    public Void addRow() {
        selectAll = false;
        for (integer i = 0; i < noOfRows; i++) {
            Lead_Product__c objportlink = new Lead_Product__c();
            objportlink.Lead__c = LeadId;
            objportlink.Quantity__c = 1;
            objportlink.Contract_Type__c = 'New Business';

            listPortLinks.add(objportlink);
            Product2 prodx = new Product2();
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false, objportlink, prodx));
        }
    }

    public void selectAll() {

        if (selectAll == true) {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = true;
            }

        } else {
            for (booleanProjectWrapper wrapperlist: lstOfbooleanProjectWrapper) {
                wrapperlist.isSelected = false;
            }
        }
    }

    Public void deleteSelectedRows() {
            selectAll = false;
            list < Lead_Product__c > toDeleteRows = new list < Lead_Product__c > ();
            for (integer j = (lstOfbooleanProjectWrapper.size() - 1); j >= 0; j--) {
                if (lstOfbooleanProjectWrapper[j].isSelected == true) {
                    if (lstOfbooleanProjectWrapper[j].objport.id != null) {
                        toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                    }

                    lstOfbooleanProjectWrapper.remove(j);

                }


            }
            delete toDeleteRows;
        }
        /**
        *   Method Name:    DelRow
        *   Description:    To delete the record by passing the row no.
        *   Param:  RowNo

        */

    public Void delRow() {

            selectAll = false;
            system.debug('--------' + RowNo);
            list < Lead_Product__c > todelete = new list < Lead_Product__c > ();

            if (lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null) {
                todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);

            }


            lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
            delete todelete;


        }
        /**
         *   Method Name:    saveLeadProduct
         *   Description:    To save the records and then redirect it to respective opportunity
         */

    public void saveLeadProduct() {
        if (showerror != TRUE) {
            List < Lead_Product__c > listToUpsert = new List < Lead_Product__c > ();
            Boolean isBlank = FALSE;
            //system.debug('@@@@@@@@@@'+listProjRev.size());

            for (booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper) {
                if (objportlink.objport.Lead__c == NULL || objportlink.objport.Product__c == NULL || objportlink.objport.Quantity__c == NULL) {
                    errOnPage = 'error';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'Please ensure the fields Lead, Product & Quantity have been completed'));

                } else {
                    errOnPage = '';
                    if(objportlink.objproduct2.PHSCPQ_Standard_Service_Frequency__c!=null)
                    objportlink.objport.Service_Frequency__c = objportlink.objproduct2.PHSCPQ_Standard_Service_Frequency__c;
                    listToUpsert.add(objportlink.objport);
                }
            }
            if (listToUpsert.size() > 0) {
                try {
                    Upsert listToUpsert;
                    PageReference acctPage = new PageReference('/' + LeadId);
                    acctPage.setRedirect(true);


                } catch (Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                        'There is missing information on the lead, please edit and save the lead record to identify the missing information.'));
                }
            }
        }

    }


    public void checkduplicates() {
        Set < ID > dupcheckset = new Set < ID > ();
        showerror = false;
        errorPos = 0;
        Integer i = 0;
        for (booleanProjectWrapper bwrap: lstOfbooleanProjectWrapper) {
            List<Product2> prod = new List<Product2>();

            if (bwrap.objport.Product__c != null && !dupcheckset.contains(bwrap.objport.Product__c)) {
                dupcheckset.add(bwrap.objport.Product__c);

                prod = [select PHSCPQ_Standard_Service_Frequency__c from Product2 where id =: bwrap.objport.Product__c];
               
                if(!prod.isEmpty())
                bwrap.objproduct2 = prod[0];
                system.debug('ProdID--' + bwrap.objport.Product__c);
              // system.debug('Service freq--' + prod[0].PHSCPQ_Standard_Service_Frequency__c);
                System.debug('list modified--' + lstOfbooleanProjectWrapper);

            } else if (bwrap.objport.Product__c != null && dupcheckset.contains(bwrap.objport.Product__c)) {
                showerror = true;
                errorPos = i;
                return;

            }
            i++;


        }

    }
    public class booleanProjectWrapper {
        public Boolean isSelected {
            get;
            set;
        }
        public Lead_Product__c objport {
            get;
            set;
        }
        public Product2 objproduct2 {
            get;
            set;
        }
        public integer ValuetoList {
            get;
            set;
        }

        public booleanProjectWrapper(boolean isSelect, Lead_Product__c objports, Product2 objproduct2) {
            objport = objports;
            isSelected = isSelect;
            ValuetoList = 1;

        }
    }

}