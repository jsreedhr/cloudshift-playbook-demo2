/**********************************************************************
* bg_Case_Tests:
*
* Tests for the Case object
* Created By: BrightGen Ltd
* Created Date: 05/04/2016
*
* Changes: 19-07-2-17: (KH) added unit tests to support assignPHSAccount()
***********************************************************************/
@isTest
private class bg_Case_Tests
{
	@testSetup static void setupDate()
    {
        Glogbal_Settings__c globalSettings = bg_RecordBuilder.generateDefaultGlobalSettings();
        globalSettings.Can_Edit_Closed_Cases__c = TRUE;
        update globalSettings;
	}

    @isTest static void Given_AnOpenCaseWithInFlightActivities_When_ASystemAdministratorTriesToCloseIt_Then_TheUserShouldRecieveAnError()
    {
    	User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
    	Given_AnOpenCaseWithInFlightActivities_When_AUserTriesToCloseIt_Then_TheUserShouldRecieveAnError_Runner(user);
    }

    @isTest static void Given_AnOpenCaseWithInFlightActivities_When_ATaskIsNull_Then_TheUserShouldNotRecieveAnError()
    {
        User user = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
        Given_AnOpenCaseWithInFlightActivities_When_ATaskIsNull_Then_TheUserShouldNotRecieveAnError_Runner(user);
    }

    public static void Given_AnOpenCaseWithInFlightActivities_When_AUserTriesToCloseIt_Then_TheUserShouldRecieveAnError_Runner(User userToRunAs)
    {
    	System.runAs(userToRunAs)
    	{
    		/*
    		 * Set up a case with inflight activities
    		 */

    		Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id);
    		insert caseRecord;

            test.startTest();
    		Task taskRecord = new Task(WhatId = caseRecord.Id, Due_Date_and_Time__c = datetime.now().addDays(1), Task_IsActive__c = true);
    		insert taskRecord;

    		Event eventRecord = new Event(WhatId = caseRecord.Id, ActivityDateTime = datetime.now().addDays(1), DurationInMinutes = 60, Event_IsActive__c = true);
    		insert eventRecord;

            Account testAcct = bg_Test_Data_Utils.createAccount('Test');
            insert testAcct;

            caseRecord.AccountId = testAcct.Id;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('1234', testAcct.Id);
            insert testPHSAccount;

            caseRecord.PHS_Account__c = testPHSAccount.Id;

            update caseRecord;
            test.stopTest();

    		/*
    		 * Try to close the case
    		 */
    		Boolean errorHappened = false;
    		try
    		{
    			caseRecord.Status = bg_Constants.CLOSED_CASE_STATUS;
                update caseRecord;
    		}
    		catch (exception ex)
    		{
    			errorHappened = true;
    		}

    		/*
    		 * Test verification
    		 */
    		System.assertEquals(true, errorHappened, 'The user was able to close a case with inflight tasks');
    	}
    }



    public static void Given_AnOpenCaseWithInFlightActivities_When_ATaskIsNull_Then_TheUserShouldNotRecieveAnError_Runner(User userToRunAs)
    {
        System.runAs(userToRunAs)
        {
            /*
             * Set up a case with inflight activities
             */

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test');
            insert caseRecord;

            test.startTest();
            Task taskRecord = new Task(WhatId = caseRecord.Id, Due_Date_and_Time__c = datetime.now().addDays(1), Task_IsActive__c = true);
            insert taskRecord;

            Event eventRecord = new Event(WhatId = caseRecord.Id, ActivityDateTime = datetime.now().addDays(1), DurationInMinutes = 60, Event_IsActive__c = true);
            insert eventRecord;

            taskRecord.WhatId = null;
            update taskRecord;

            eventRecord.WhatId = null;
            update eventRecord;

            caseRecord.Status = 'Open';
            update caseRecord;

            Account testAcct = bg_Test_Data_Utils.createAccount('Test');
            insert testAcct;

            caseRecord.AccountId = testAcct.Id;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('1234', testAcct.Id);
            insert testPHSAccount;

            caseRecord.PHS_Account__c = testPHSAccount.Id;

            update caseRecord;

            test.stopTest();

            Case updatedCase = [select Id, Status from Case limit 1];

            /*
             * Try to close the case
             */
            Boolean errorHappened = false;
            String exceptionMessage = '';
            try
            {

                caseRecord.Status = bg_Constants.CLOSED_CASE_STATUS;
                update caseRecord;
            }
            catch (exception ex)
            {
                errorHappened = true;
                exceptionMessage = ex.getMessage();
            }

            /*
             * Test verification
             */
            System.assertEquals(false, errorHappened, 'The user was able to close a case with inflight tasks [' + exceptionMessage + ']');
        }
    }

    static testMethod void testOrginatingEmployeeOnInsert()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
        User employee;
        System.runAs(userToRunAs)
        {
            employee = bg_RecordBuilder.createUser('EA', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
            employee.Username = '112233@phs.co.uk';
            insert employee;
            
            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test');
            caseRecord.Originating_Employee__c = '112233';
            insert caseRecord;

            test.stopTest();

            caseRecord = [SELECT Id, Originating_Employee_User__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];

            System.assertEquals(employee.Id, caseRecord.Originating_Employee_User__c);
        }
    }

    static testMethod void testPHSAccountAssignmentOnCasePositive()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
        User employee;
        System.runAs(userToRunAs)
        {
            Account testAcct = bg_Test_Data_Utils.createAccount('TestK');
            insert testAcct;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('K1', testAcct.Id);
            testPHSAccount.Wildebeest_Ref__c = '9876';
            insert testPHSAccount;
            
            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test', Wildebeest_Ref__c = '9876');
            insert caseRecord;

            test.stopTest();

            caseRecord = [SELECT Id, AccountId, PHS_Account__c, PHS_Wildebeest_Ref__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];

            System.assertEquals(testAcct.Id, caseRecord.AccountId);
            System.assertEquals(testPHSAccount.Id, caseRecord.PHS_Account__c);
            System.assertEquals(testPHSAccount.Wildebeest_Ref__c, caseRecord.PHS_Wildebeest_Ref__c);
        }
    }

    static testMethod void testPHSAccountAssignmentOnCaseNegative()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
        User employee;
        System.runAs(userToRunAs)
        {
            Account testAcct = bg_Test_Data_Utils.createAccount('TestK');
            insert testAcct;

            PHS_Account__c testPHSAccount = bg_Test_Data_Utils.createPHSAccount('K1', testAcct.Id);
            testPHSAccount.Wildebeest_Ref__c = '9876';
            insert testPHSAccount;
            
            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test', Wildebeest_Ref__c = '22222');
            insert caseRecord;

            test.stopTest();

            caseRecord = [SELECT Id, AccountId, PHS_Account__c, PHS_Wildebeest_Ref__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];

            System.assertEquals(null, caseRecord.AccountId);
            System.assertEquals(null, caseRecord.PHS_Account__c);
            System.assertEquals(null, caseRecord.PHS_Wildebeest_Ref__c);
        }
    }
    
    static testMethod void testOrginatingEmployeeOnUpdate()
    {
        User userToRunAs = bg_RecordBuilder.createUser('1', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
        System.runAs(userToRunAs)
        {
            List<User> usersToInsert = new List<User>();

            User employeeA = bg_RecordBuilder.createUser('EA', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
            employeeA.Username = '112233@phs.co.uk';
            usersToInsert.add(employeeA);

            User employeeB = bg_RecordBuilder.createUser('EB', [Select Id From Profile Where Name = 'System Administrator' Limit 1].Id);
            employeeB.Username = '999111@phs.co.uk';
            usersToInsert.add(employeeB);
            
            insert usersToInsert;

            test.startTest();

            Case caseRecord = new Case(Status = 'New', RecordTypeId = bg_Constants.getRecordType(bg_Constants.CASE_OBJECT_TYPE, bg_Constants.STANDARD_CASE).Id, Type = 'Test', Case_Reason__c = 'Test');
            caseRecord.Originating_Employee__c = '112233';
            insert caseRecord;

            test.stopTest();

            caseRecord = [SELECT Id, Originating_Employee_User__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];

            System.assertEquals(employeeA.Id, caseRecord.Originating_Employee_User__c);

            caseRecord.Originating_Employee__c = '999111';
            update caseRecord;

            caseRecord = [SELECT Id, Originating_Employee_User__c FROM Case WHERE Id = :caseRecord.Id LIMIT 1];

            System.assertEquals(employeeB.Id, caseRecord.Originating_Employee_User__c);
        }
    }
    
        @isTest static void diarisedAfterInsert(){
            Account accountRecord = new Account(Name = 'Test', BillingPostalCode = 'ab12 3cd');
            insert accountRecord;

            Case caseRecord = new Case(AccountId = accountRecord.Id, Status = 'New', Diarised_Date_Time__c = datetime.now());
            insert caseRecord;

        
        Case c = [SELECT Id, Status FROM Case WHERE Id =: caseRecord.Id Limit 1];
        System.assertEquals('Diarised', c.Status);
    }
    
            @isTest static void diarisedUpdate(){
            Account accountRecord = new Account(Name = 'Test', BillingPostalCode = 'ab12 3cd');
            insert accountRecord;

            Case caseRecord = new Case(AccountId = accountRecord.Id, Status = 'New');
            insert caseRecord;

            Case c = [SELECT Id, Status, Diarised_Date_Time__c FROM Case WHERE Id =: caseRecord.Id Limit 1];
            System.assertEquals('New', c.Status);
                
           	c.Diarised_Date_Time__c = datetime.now();
            update c;
                
            Case cAU = [SELECT Id, Status, Diarised_Date_Time__c FROM Case WHERE Id =: caseRecord.Id Limit 1];   
            System.assertEquals('Diarised', cAU.Status);
    }
}