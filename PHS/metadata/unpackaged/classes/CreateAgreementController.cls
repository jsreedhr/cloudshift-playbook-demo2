/**
 *  @Class Name:    CreateAgreementController
 *  @Description:   This is a controller for CreateAgreementPage
 *  @Company:       dQuotient
 *  CreatedDate:    08/12/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha                 11-12-2017          		Original Version
 */
public class CreateAgreementController {
    
    public Opportunity opp{get;set;}
    public ID agreementID{get;set;}
    public Profile profile{get; set;}
    public  User usr{get; set;}
    public Boolean hasOpenAgreements{get; set;}   
    public  CreateAgreementController()
    {
        profile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1];
        usr = [select ActiveInPilot__c, APIPartnerServerURL290__c,ApiSessionID_F__c from USER where Id = :UserInfo.getUserId() LIMIT 1];
        String oppId=ApexPages.currentPage().getParameters().get('oppId');
        opp= [SELECT Quote_Roll_Up__c ,Has_Synced_Quote__c,Product_Count__c,Is_Quick_Contract__c FROM Opportunity WHERE id =: oppId];
      
    }
    
     public void loadAction() {
         
         if(profile.name=='System Administrator' || usr.ActiveInPilot__c == true)
        {
            hasOpenAgreements =  bg_AgreementHelper.returnResultOfOpenAgreement(opp.id);
            System.debug('hasOpenAgreements:'+hasOpenAgreements);
            if( (opp.Quote_Roll_Up__c > 0 && opp.Has_Synced_Quote__c == true && opp.Product_Count__c > 0 && hasOpenAgreements == false) || (opp.Is_Quick_Contract__c == true && opp.Product_Count__c > 0 && hasOpenAgreements== false) )
            {
             // try{
                  System.debug('Check 1');
              agreementID =  bg_AgreementHelper.createAgreementAndLineItemsFromButtonPress(opp.Id,opp.Product_Count__c.intValue());
                System.debug('agreementID:'+agreementID);
                    Date dt=Date.today();
                    dt.addMonths(1);
                	opp.Agreement_Created_Date__c  = dt;
                   update opp;
                 System.debug('opp:'+opp);
               /* }
                catch(Exception e)
                {
                    System.debug('Exception '+e);
                }*/
                
            }
            
        }
        
     }
}