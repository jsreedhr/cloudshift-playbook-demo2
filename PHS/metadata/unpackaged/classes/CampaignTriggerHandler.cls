/**
 *  Class Name: CampaignTriggerHandler  
 *  Description: This is a TriggerHandler for CampaignTrigger.
 *  Company: Standav
 *  CreatedDate:02/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             02-11-2017                 Original version
 */
 public class CampaignTriggerHandler {
    
    // A method to update activity fields under CampaignMember after insert, update and undelete.
    public static void updateLeadCampaignsField(List<Campaign> camList){
        
        Set<Id> setCampaignId = new Set<Id>();
        for(Campaign camRecord :camList){
            if(camRecord.Id != null && !setCampaignId.contains(camRecord.Id))
                setCampaignId.add(camRecord.Id);
        }

        List<CampaignMember> camMemList = new List<CampaignMember>([Select CampaignId, LeadId, Campaign.Name From CampaignMember Where CampaignId in :setCampaignId]);
        system.debug('camMemList----->' +camMemList);
        Set<String> setLeadId = new Set<String>();
        Map<String, String> mapLeadToCampMember = new Map<String, String>();
        for(CampaignMember objCam : camMemList){
            if(objCam.LeadId != null){
                if(mapLeadToCampMember.containsKey(objCam.LeadId)){
                    String campaignName =mapLeadToCampMember.get(objCam.LeadId);
                    campaignName += objCam.Campaign.Name + ',';   
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
                else{
                    String campaignName = objCam.Campaign.Name + ',';   
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
            }
        }
        system.debug('mapLeadToCampMember----->' +mapLeadToCampMember);
        
        for(CampaignMember camRecord :camMemList){
            if(camRecord.LeadId != null && !setLeadId.contains(camRecord.LeadId)){
                setLeadId.add(camRecord.LeadId);
            }
        }
        List<Lead> leadList = new List<Lead>([Select Campaigns__c From Lead Where Id in :setLeadId]);
        for(Lead leadRecord :leadList){
            if(mapLeadToCampMember.containsKey(leadRecord.Id)){
                leadRecord.Campaigns__c = mapLeadToCampMember.get(leadRecord.Id).removeEnd(',');
            }
        }
        system.debug('leadList----->' +leadList);
        update leadList;
    }
    
    // A method to update Lead Campaigns field after delete of Campaign.
    public static void updateLeadCampaignsFieldDelete(List<Campaign> camList){
        
        Set<Id> setCampaignId = new Set<Id>();
        for(Campaign camRecord :camList){
            if(camRecord.Id != null && !setCampaignId.contains(camRecord.Id))
                setCampaignId.add(camRecord.Id);
        }
        system.debug('setCampaignId----->' +setCampaignId);
        List<CampaignMember> camMemList = new List<CampaignMember>([Select CampaignId, LeadId, Campaign.Name From CampaignMember Where CampaignId in :setCampaignId]);
        delete camMemList;
    }
    
    // A method to update Campaigns field under Opportunity after insert, update and undelete of Campaign.
    public static void updateOpportunityCampaignsField(List<Campaign> camList){
    
        Set<Id> setCampaignId = new Set<Id>();
        for(Campaign camRecord :camList){
            if(camRecord.Id != null && !setCampaignId.contains(camRecord.Id))
                setCampaignId.add(camRecord.Id);
        }
        
        List<Opportunity> oppList = new List<Opportunity>([Select Campaign.Name From Opportunity Where CampaignId in :setCampaignId]);
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([Select Campaign.Name From Opportunity Where CampaignId in :setCampaignId]);
        for(Opportunity oppRecord :oppList){
            if(oppRecord.CampaignId != null){
                oppRecord.Campaigns__c = opportunityMap.get(oppRecord.Id).Campaign.Name;
            }
            else
                oppRecord.Campaigns__c = null;
        }
        update oppList;
    }
    
    // A method to update Campaigns field under Opportunity after delete of Campaign.
    public static void updateOpportunityCampaignsFieldDelete(List<Campaign> camList){
    
        Set<Id> setCampaignId = new Set<Id>();
        for(Campaign camRecord :camList){
            if(camRecord.Id != null && !setCampaignId.contains(camRecord.Id))
                setCampaignId.add(camRecord.Id);
        }
        List<Opportunity> oppList = new List<Opportunity>([Select Campaigns__c, CampaignId From Opportunity Where CampaignId in :setCampaignId]);
        for(Opportunity oppRecord :oppList){
            if(setCampaignId.contains(oppRecord.CampaignId)){
                oppRecord.Campaigns__c = null;
                oppRecord.CampaignId = null;
            }
        }
        update oppList;
        system.debug('oppList----->' +oppList);
    }
}