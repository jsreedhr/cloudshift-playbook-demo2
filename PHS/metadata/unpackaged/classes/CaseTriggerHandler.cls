/**
    * @Name:        CaseTriggerHandler
    * @Description: This class will override all the methods that are needed from the trigger contexts calling the relevant helper methods
    *
    * @author:      Jared Watson
    * @version:     1.0
    * Change Log
    *
    * Date          author              Change Description
    * -----------------------------------------------------------------------------------
    * 24/08/2018  	Jared Watson        Created Class
*/

public class CaseTriggerHandler extends TriggerHandler{

    protected override void beforeInsert(){
        //Auto Create Contact From Email To Case
        CaseTriggerHelper.autoCreateContactFromEmailToCase(Trigger.New);
    }

    protected override void beforeUpdate(){
        //Validate Case before closing to verify Origin and Contact values
        CaseTriggerHelper.validateCaseBeforeClosing(Trigger.NewMap,Trigger.OldMap);
    }

    protected override void beforeDelete(){
        CaseTriggerHelper.createDeletedWIonDelete(Trigger.old);
    }

    protected override void afterInsert(){

    }

    protected override void afterUpdate(){

    }

    protected override void afterDelete(){

    }

}