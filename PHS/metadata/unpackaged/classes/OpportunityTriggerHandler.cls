/**
 *  Class Name: OpportunityTriggerHandler  
 *  Description: This is a TriggerHandler for Opportunity.
 *  Company: Standav
 *  CreatedDate:31/10/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             31-10-2017                 Original version.
 */
public class OpportunityTriggerHandler {

    // A method to update activity fields under Opportunity after insert or update
    public static void updateActivity(List < Opportunity > oppoList) {
        system.debug('Entering updateActivity----->');
        Set < String > setOpportunityId = new Set < String > ();
        Map < String, Opportunity > mapOpportunityRecord = new Map < String, Opportunity > ();
        List < Task > taskList = new List < Task > ();
        List < Event > eventList = new List < Event > ();
        for (Opportunity oppoRecord: oppoList) {

            setOpportunityId.add(oppoRecord.Id);
            mapOpportunityRecord.put(oppoRecord.Id, oppoRecord);
        }
        // List < Opportunity > opplisted = [Select id, Name, DM_Postal_District__r.Name, (Select Id, WhatId, Stage__c, Amount__c, Source__c, Campaigns__c, Postal_District__c FROM Tasks) from Opportunity where Id in: setOpportunityId];
        //system.debug('Entering updateActivity----->' + opplisted);

        try {
            taskList = [Select Id, WhatId, Stage__c, Amount__c, Source__c, Campaigns__c, Postal_District__c FROM Task Where WhatId in: setOpportunityId];
            // List < Event > eventList = [Select Id, WhatId, Stage__c, Amount__c, Source__c, Campaigns__c, Postal_District__c FROM Event Where WhatId in: setOpportunityId];
            eventList = [Select Id, WhatId, Stage__c, Amount__c, Source__c, Campaigns__c, Postal_District__c FROM Event Where WhatId in: setOpportunityId and Ischild = false];
        } catch (Exception e) {

        }
        ID pdID;
        Front_Line_Team__c fltc;

        if (!taskList.isEmpty()) {
            for (Task taskRecord: taskList) {
                if (mapOpportunityRecord.containsKey(taskRecord.WhatId)) {
                    system.debug('Leadsource on task----->' + mapOpportunityRecord.get(taskRecord.WhatId).LeadSource);
                    taskRecord.Stage__c = mapOpportunityRecord.get(taskRecord.WhatId).StageName;
                    taskRecord.Amount__c = mapOpportunityRecord.get(taskRecord.WhatId).Amount;
                    taskRecord.Source__c = mapOpportunityRecord.get(taskRecord.WhatId).LeadSource;
                    taskRecord.Campaigns__c = mapOpportunityRecord.get(taskRecord.WhatId).Campaigns__c;
                    // taskRecord.Postal_District__c = mapOpportunityRecord.get(taskRecord.WhatId).DM_Postal_District__r.Name;

                }
            }
        }
        if (!eventList.isEmpty()) {
            for (Event eventRecord: eventList) {
                if (mapOpportunityRecord.containsKey(eventRecord.WhatId)) {
                    system.debug('Leadsource----->' + mapOpportunityRecord.get(eventRecord.WhatId).LeadSource);
                    eventRecord.Campaigns__c = mapOpportunityRecord.get(eventRecord.WhatId).Campaigns__c;
                    eventRecord.Source__c = mapOpportunityRecord.get(eventRecord.WhatId).LeadSource;
                    eventRecord.Stage__c = mapOpportunityRecord.get(eventRecord.WhatId).StageName;
                    eventRecord.Amount__c = mapOpportunityRecord.get(eventRecord.WhatId).Amount;
                    //  eventRecord.Postal_District__c = mapOpportunityRecord.get(eventRecord.WhatId).DM_Postal_District__r.Name;

                }
            }
        }

        if (!eventList.isEmpty() || !taskList.isEmpty()) {
            try {
                update taskList;
                //update eventList;
                system.debug('Task Updated updateActivity----->');
            } catch (DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
            try {
                // update taskList;
                update eventList;
                system.debug('Event Updated updateActivity----->');
            } catch (DmlException e) {
                System.debug('The following exception has occurred Event Field Updating: ' + e.getMessage());
            }
        }
    }

    // A method to update Campaigns field in Opportunity after insert or update
    public static void updateCampaignsField(List < Opportunity > oppList) {

        system.debug('oppList--->' + oppList);
        Set < String > setOppId = new Set < String > ();

        for (Opportunity oppRecord: oppList) {
            if (oppRecord.Id != null && !setOppId.contains(oppRecord.Id)) {
                setOppId.add(oppRecord.Id);
            }
        }

        List < CampaignInfluence > campList = new List < CampaignInfluence > ([Select CampaignId, OpportunityId, Campaign.Name From CampaignInfluence Where OpportunityId in: setOppId]);
        // Maps leadId to comma seperated Campaign Name
        Map < String, String > mapCampaignInfluence = new Map < String, String > ();

        for (CampaignInfluence objCam: campList) {
            if (objCam.OpportunityId != null) {
                if (mapCampaignInfluence.containsKey(objCam.OpportunityId)) {
                    String campaignName = mapCampaignInfluence.get(objCam.OpportunityId);
                    campaignName += objCam.Campaign.Name + ',';
                    mapCampaignInfluence.put(objCam.OpportunityId, campaignName);
                } else {
                    String campaignName = objCam.Campaign.Name + ',';
                    mapCampaignInfluence.put(objCam.OpportunityId, campaignName);
                }
            }
        }

        for (Opportunity oppRecord: oppList) {
            if (mapCampaignInfluence.containsKey(oppRecord.Id)) {
                oppRecord.Campaigns__c = mapCampaignInfluence.get(oppRecord.Id).removeEnd(',');
            }
        }
    }
    
    //method to update the default opportunity name on before insert and before update
    public static void updateOppoDefaultName(List<Opportunity> lstNewOpportunity)
    {
        set<Id> setAccIds = new set<Id>();
        for(Opportunity objOpp: lstNewOpportunity)
        {
            if(objOpp.AccountId != null) setAccIds.add(objOpp.AccountId);
        }
        map<id, Account> mapAccount = new map<id, Account>([SELECT id, Name, BillingPostalCode  FROM Account WHERE id IN: setAccIds]);
        for(Opportunity objOpp: lstNewOpportunity)
        {
            String strOppDefaultName = (mapAccount.containsKey(objOpp.AccountId) ?  mapAccount.get(objOpp.AccountId).Name: 'Opportunity');
            strOppDefaultName += ( (mapAccount.containsKey(objOpp.AccountId) &&  mapAccount.get(objOpp.AccountId).BillingPostalCode != null) ?  ' - '+mapAccount.get(objOpp.AccountId).BillingPostalCode: '');
            strOppDefaultName += (String.isBlank(objOpp.Priority__c) ? ' ' : ' - '+ objOpp.Priority__c);
            //here createddate will be null in before insert case so using current date 
            strOppDefaultName += (objOpp.CreatedDate == null ?  ' - '+ DateTime.now().day() +'/'+ DateTime.now().month() +'/'+ DateTime.now().year() :  ' - '+ objOpp.CreatedDate.day() +'/'+ objOpp.CreatedDate.month() +'/'+ objOpp.CreatedDate.year() );
            objOpp.Name = strOppDefaultName;
        }
    }
    
    
    public static void updateDateClosed(List<Opportunity> lstNewOpportunity,map<Id, Opportunity> mapOldOpp)
    {
        for(Opportunity objOpp: lstNewOpportunity)
        {
            if(objOpp.StageName != mapOldOpp.get(objOpp.id).StageName )
            {
            if(objOpp.StageName =='Closed Won' ||  objOpp.StageName =='Closed Lost')
            {
                objOpp.Date_Closed__c = System.today();
            }
            else
            {
                 objOpp.Date_Closed__c = null;
            }
            }
        }
        
    }
}