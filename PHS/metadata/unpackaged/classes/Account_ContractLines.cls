/*
    Created By: Ratan Paul 
    Created Date: 31-05-2018
*/
public without sharing class Account_ContractLines
{
    private String wildebeestRef;
    bg_phsAccountInfoResponse response;
    public List<wrapDivision> lstActiveWrapDivision         {get; private set;}
    public List<wrapDivision> lstInActiveWrapDivision       {get; private set;}
    public map<String, Decimal> mapActiveDivisionsToTotal   {get; private set;}
    public map<String, Decimal> mapInActiveDivisionsToTotal {get; private set;}
    
    public Account_ContractLines()
    {
        lstActiveWrapDivision = new List<wrapDivision>();
        lstInActiveWrapDivision = new List<wrapDivision>();
        mapActiveDivisionsToTotal = new map<String, Decimal>();
        mapInActiveDivisionsToTotal= new map<String, Decimal>();
        for(String str:getDivisions())
        {
            mapActiveDivisionsToTotal.put(str, 0);
            mapInActiveDivisionsToTotal.put(str, 0);
        }
    }
    
    public String[] getDivisions() {
        return new String[]{'Hygiene','Greenleaf','Wastekit', 'Other'};
    }
    
    public void calculateDivisiona(List<wrapDivision> lstWrapDivision, map<String, Decimal> mapDivisionsToTotal)
    {
        for(wrapDivision objwrapDivision: lstWrapDivision)
        {
            if(objwrapDivision.strDivisionName == 'Washrooms' || objwrapDivision.strDivisionName == 'Waste Management' || objwrapDivision.strDivisionName == 'Floorcare')
                mapDivisionsToTotal.put('Hygiene', mapDivisionsToTotal.get('Hygiene') + objwrapDivision.getGrandTotalContractValue());
            else if(objwrapDivision.strDivisionName == 'Greenleaf')
                mapDivisionsToTotal.put('Greenleaf', mapDivisionsToTotal.get('Greenleaf') + objwrapDivision.getGrandTotalContractValue());
            else if(objwrapDivision.strDivisionName == 'Wastekit')
                mapDivisionsToTotal.put('Wastekit', mapDivisionsToTotal.get('Wastekit') + objwrapDivision.getGrandTotalContractValue());
            else //(objwrapDivision.strDivisionName == 'Other')
                mapDivisionsToTotal.put('Other', mapDivisionsToTotal.get('Other') + objwrapDivision.getGrandTotalContractValue());
        }
    }
    
    public void init()
    {
        try{
            Id paramaccId = Apexpages.Currentpage().getparameters().get('id');
            if(paramaccId != null) 
            {
                set<String> setWildRef = new set<String>();
                Account account;
                Account acc = [Select Id, Wildebeest_Ref__c,Last_Callout_Date__c, Callout_Eligible__c, Callout_Response__c From Account Where Id =: paramaccId];
                wildebeestRef = acc.Wildebeest_Ref__c.substringBefore(',');
                
                List<Account> acclist= [Select Id From Account Where Wildebeest_Ref__c = :wildebeestRef OR Wildebeest_Ref__c LIKE :wildebeestRef+',%'
                    OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef OR Wildebeest_Ref__c LIKE :'%,'+wildebeestRef+',%'];
                    
                if(acclist!=null && !acclist.isEmpty())
                    account = acclist[0];
                else
                {
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No matching accounts refs found'));
                       return;
                }
                
                if(account != NULL && account.Id != NULL)
                {
                     List<PHS_Account__c> phsAccountList = [Select Id, Name, Wildebeest_Ref__c, Billing_Street__c, Billing_City__c, Billing_Country__c, Billing_Postcode__c, Salesforce_Account__r.Name  
                                                              From PHS_Account__c Where Salesforce_Account__c = :account.Id AND Wildebeest_Ref__c != NULL];
                    if(phsAccountList.size() >0)
                    {
                        set<String> wildebeestReferences = new set<String>(); 
                        for(PHS_Account__c phsAccount : phsAccountList)
                        {
                            wildebeestReferences.add(phsAccount.Wildebeest_Ref__c);
                        }
    
                        if(wildebeestReferences.size()>0)
                        {
                            system.debug('=====wildebeestReferences=='+wildebeestReferences);
                            response = bg_Wildebeest_Helper.getExistingContractLines(account.Id, wildebeestReferences, true, false, false, false, 'BuyerCustomerDetails');
                            if(response != NULL && response.ResultSize>0)
                            {
                                
                                List<bg_ContractLine> activeContractLineList = new List<bg_ContractLine>();
                                List<bg_ContractLine> inActiveContractLineList = new List<bg_ContractLine>();
                                
                                for(bg_ContractLine objbg_ContractLine: response.contractLines)
                                {
                                    if(objbg_ContractLine.IsCancelled)
                                        inActiveContractLineList.add(objbg_ContractLine);
                                    else
                                        activeContractLineList.add(objbg_ContractLine);
                                }
                                prepareContractLineList(activeContractLineList, lstActiveWrapDivision);
                                prepareContractLineList(inActiveContractLineList,  lstInActiveWrapDivision);
                                
                                calculateDivisiona(lstActiveWrapDivision, mapActiveDivisionsToTotal);
                                calculateDivisiona(lstInActiveWrapDivision, mapInActiveDivisionsToTotal);
                            }
                            else
                            {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'There are no contract lines at this address.'));
                            }
                             // following condition is to be checked irrespective of the above if code
                            if(response != NULL)
                            {
                                acc.Callout_Eligible__c = FALSE;
                                acc.Last_Callout_Date__c = System.now();
                                update acc;
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            system.debug(ex.getMessage()+'============ex.getLineNumber()==='+ex.getLineNumber());
        }
    }
    
    private static void prepareContractLineList(List<bg_ContractLine> contractLineList, List<wrapDivision> lstWrapDivision)
    {
        map<String, List<bg_ContractLine>> mapWildebeestRefToContractLines = new map<String, List<bg_ContractLine>>();
        map<String, set<Integer>> mapDivisionNameToWildbeesRef = new map<String, set<Integer>>();
            
        for(bg_ContractLine objbg_ContractLine: contractLineList)
        {
            if(!mapWildebeestRefToContractLines.containskey(objbg_ContractLine.PHSLocationAccountReference +'@@'+objbg_ContractLine.DivisionName))
                mapWildebeestRefToContractLines.put(objbg_ContractLine.PHSLocationAccountReference+'@@'+objbg_ContractLine.DivisionName, new List<bg_ContractLine>());
            mapWildebeestRefToContractLines.get(objbg_ContractLine.PHSLocationAccountReference+'@@'+objbg_ContractLine.DivisionName).add(objbg_ContractLine);
            
            if(String.isNotEmpty(objbg_ContractLine.DivisionName))
            {
                if(!mapDivisionNameToWildbeesRef.containskey(objbg_ContractLine.DivisionName))
                    mapDivisionNameToWildbeesRef.put(objbg_ContractLine.DivisionName, new set<Integer>());
                mapDivisionNameToWildbeesRef.get(objbg_ContractLine.DivisionName).add(objbg_ContractLine.PHSLocationAccountReference);
            }
        }
        
        for(string dvName: mapDivisionNameToWildbeesRef.keySet())
        {
            wrapDivision objwrapDivision = new wrapDivision();
            objwrapDivision.strDivisionName = dvName;
            objwrapDivision.lstphsAccWrapper = new List<phsAccWrapper>();
            for(Integer intWildBeesRef: mapDivisionNameToWildbeesRef.get(dvName))
            {
                phsAccWrapper objphsAccWrapper = new phsAccWrapper();
                objphsAccWrapper.lstContractLines = new List<bg_ContractLine>();
                objphsAccWrapper.lstCLIdToCLs = new List<contractLineIdToLines>();
                if(mapWildebeestRefToContractLines.containskey(intWildBeesRef+'@@'+dvName))
                {
                    map<Integer, List<bg_ContractLine>> mapContractIdToCls = new map<Integer, List<bg_ContractLine>>();
                    for(bg_ContractLine objCline: mapWildebeestRefToContractLines.get(intWildBeesRef+'@@'+dvName))
                    {
                        if(!mapContractIdToCls.containskey(objCline.ContractLineId))
                            mapContractIdToCls.put(objCline.ContractLineId, new List<bg_ContractLine>());
                        mapContractIdToCls.get(objCline.ContractLineId).add(objCline);
                    }
                    for(Integer intContractId: mapContractIdToCls.keyset())
                    {
                        contractLineIdToLines objcontractLineIdToLines = new contractLineIdToLines();
                        objcontractLineIdToLines.intContractLineId = intContractId;
                        objcontractLineIdToLines.lstwrapperSite = new List<wrapperSite>();
                        for(bg_ContractLine objCline:mapContractIdToCls.get(intContractId))
                        {
                            objcontractLineIdToLines.objbg_ContractLine = objCline;
                            wrapperSite objwrapperSite = new wrapperSite();
                            objwrapperSite.strfullsite = objCline.FullSiteDescription;
                            objwrapperSite.intQuantity = objCline.SitedQuantity;
                            objcontractLineIdToLines.lstwrapperSite.add(objwrapperSite);
                        }
                        objphsAccWrapper.lstContractLines.addAll(mapContractIdToCls.get(intContractId));
                        objphsAccWrapper.lstCLIdToCLs.add(objcontractLineIdToLines);
                    }
                    if(objphsAccWrapper.lstContractLines.size() > 0)
                    {
                        objphsAccWrapper.strLocationName = objphsAccWrapper.lstContractLines[0].PHSLocationAccountName + '  '+
                                                            objphsAccWrapper.lstContractLines[0].PHSLocationAccountReference +'  '+
                                                            objphsAccWrapper.lstContractLines[0].PHSAccountFullAddress;
                    }
                    objwrapDivision.lstphsAccWrapper.add(objphsAccWrapper);
                }
            }
            lstWrapDivision.add(objwrapDivision);
        }
    }
    
    public class wrapDivision
    {
        public String strDivisionName               {get; private set;}
        public List<phsAccWrapper> lstphsAccWrapper {get; private set;}
        
        public Decimal getGrandTotalContractValue(){
            Decimal dec1 = 0;
            if(lstphsAccWrapper != null)
            {
                for(phsAccWrapper objphsAccWrapper: lstphsAccWrapper)
                {
                    for(contractLineIdToLines objcontractLineIdToLines:objphsAccWrapper.lstCLIdToCLs)
                        dec1 += (objcontractLineIdToLines.objbg_ContractLine.ContractLineValue != null ? objcontractLineIdToLines.objbg_ContractLine.ContractLineValue :0);
                    
                }
            }
            return dec1;
        }
    }
    public class phsAccWrapper
    {
        public PHS_Account__c objPHS_Account                            {get; private set;}
        public String strLocationName                                   {get; private set;}
        public List<bg_ContractLine> lstContractLines                   {get; private set;}
        public List<contractLineIdToLines> lstCLIdToCLs                 {get; private set;}
        
        public Decimal getTotalContractValue(){
            Decimal dec1 = 0;
            for(contractLineIdToLines objcontractLineIdToLines:lstCLIdToCLs)
                dec1 += (objcontractLineIdToLines.objbg_ContractLine.ContractLineValue != null ? objcontractLineIdToLines.objbg_ContractLine.ContractLineValue :0);
            return dec1;
        } 
    }
    
    public class contractLineIdToLines
    {
        public Integer intContractLineId                {get; private set;}
        public bg_ContractLine objbg_ContractLine       {get; private set;}
        public List<bg_ContractLine> lstContractLines   {get; private set;}
        public List<wrapperSite> lstwrapperSite         {get; private set;}
    }
    public class wrapperSite
    {
        public String strfullsite   {get; private set;}
        public Integer intQuantity  {get; private set;}
    }
    
}