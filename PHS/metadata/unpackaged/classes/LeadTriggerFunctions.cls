/**
*  Class Name: LeadTriggerFunctions
*  Description: This is a TriggerHandler for Lead Triggers.

*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  Hormese             31-10-2017                 Last Modified
*  Md Rizwan           14-11-2017                 Changed the way Wildebeest References were matched
*  Tim Hutchings       16-05-2018                 Added code to retrieve Key_Account_Type__c from Account and store on Lead
*  Paul Jones          29-05-2018                 Retrieve ID from Account to store on Lead
*/
public without sharing class LeadTriggerFunctions {
    
    public static Boolean isAfterUpdate = true;
    // A method to update activity fields under lead after insert or update
    public static void updateActivity(List<Lead> leadList){
        // system.debug('leadList----->' +leadList);
        Set<String> setLeadId = new Set<String>();
        Map<String, Lead> mapLeadRecord = new Map<String, Lead>();
        
        for(Lead leadRecord :leadList){
            //Create set of lead ids
            setLeadId.add(leadRecord.Id);
            // Create a map of lead id and the lead record
            mapLeadRecord.put(leadRecord.Id, leadRecord);
        }
        
        List<Task> taskList = [Select Id, WhoId, Status__c, Score__c, Source__c, Campaigns__c ,Postal_District__c FROM Task Where WhoId in :setLeadId];
        List<Event> eventList = [Select Id, WhoId, Status__c, Score__c, Source__c, Campaigns__c ,Postal_District__c FROM Event Where WhoId in :setLeadId];
        
        
        
        ID pdID;
        Front_Line_Team__c fltc;
        
        for(Task taskRecord :taskList){
            if(mapLeadRecord.containsKey(taskRecord.WhoId)){
                taskRecord.Status__c = mapLeadRecord.get(taskRecord.WhoId).Status;
                taskRecord.Score__c = mapLeadRecord.get(taskRecord.WhoId).Lead_Score_Total__c;
                taskRecord.Source__c = mapLeadRecord.get(taskRecord.WhoId).LeadSource;
                taskRecord.Campaigns__c = mapLeadRecord.get(taskRecord.WhoId).Campaigns__c;
               // System.debug('Check1'+mapLeadRecord.get(taskRecord.WhoId).FLT_Postal_Code__r.name);
              //  taskRecord.Postal_District__c = mapLeadRecord.get(taskRecord.WhoId).FLT_Postal_Code__r.name;
                
                  /*pdID = mapLeadRecord.get(taskRecord.WhoId).FLT_Postal_Code__c;
                  System.debug('Check1'+pdID);
                  if(pdID!=null){
                fltc = [select name from Front_Line_Team__c where ID =: pdID];
                taskRecord.Postal_District__c = fltc.Name;
                  }*/
            }
        }
        
        for(Event eventRecord :eventList){
            if(mapLeadRecord.containsKey(eventRecord.WhoId)){
                eventRecord.Campaigns__c = mapLeadRecord.get(eventRecord.WhoId).Campaigns__c;
                eventRecord.Status__c = mapLeadRecord.get(eventRecord.WhoId).Status;
                eventRecord.Score__c = mapLeadRecord.get(eventRecord.WhoId).Lead_Score_Total__c;
                eventRecord.Source__c = mapLeadRecord.get(eventRecord.WhoId).LeadSource;
              //  System.debug('Check2'+mapLeadRecord.get(eventRecord.WhoId).FLT_Postal_Code__r.name);
             //  eventRecord.Postal_District__c = mapLeadRecord.get(eventRecord.WhoId).FLT_Postal_Code__r.name; 
                
                 /* pdID = mapLeadRecord.get(eventRecord.WhoId).FLT_Postal_Code__c;
                  System.debug('Check2'+pdID);
                   if(pdID!=null){
                fltc = [select name from Front_Line_Team__c where ID =: pdID];
                eventRecord.Postal_District__c = fltc.Name;
                   }*/
            }
        }
        
        update taskList;
        update eventList;
    }
    
    // A method to update Campaigns field in Lead after insert or update
    public static void updateCampaignsField(List<Lead> leadList){
        
        // system.debug('leadRecord--->' +leadList);
        Set<String> setLeadId = new Set<String>();
        
        for(Lead leadRecord :leadList){
            if(leadRecord.Id != null && !setLeadId.contains(leadRecord.Id)){
                setLeadId.add(leadRecord.Id);
            }
        }
        
        List<CampaignMember> camMemList = new List<CampaignMember>([Select CampaignId, LeadId, Campaign.Name From CampaignMember Where LeadId in :setLeadId]);
        // Maps leadId to comma seperated Campaign Name
        Map<String, String> mapLeadToCampMember = new Map<String, String>();
        
        for(CampaignMember objCam : camMemList){
            if(objCam.LeadId != null){
                if(mapLeadToCampMember.containsKey(objCam.LeadId)){
                    String campaignName =mapLeadToCampMember.get(objCam.LeadId);
                    campaignName += objCam.Campaign.Name + ',';
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
                else{
                    String campaignName = objCam.Campaign.Name + ',';
                    mapLeadToCampMember.put(objCam.LeadId, campaignName);
                }
            }
        }
        
        for(Lead leadRecord :leadList){
            if(mapLeadToCampMember.containsKey(leadRecord.Id)){
                leadRecord.Campaigns__c = mapLeadToCampMember.get(leadRecord.Id).removeEnd(',');
            }
        }
    }
    
    public static void getaccountdatainsert(List <Lead> lst_newleads){
        
        Set <ID> set_Leadids = new Set <ID>();
        Map <String, ID> map_LeadWBIds = new Map <String, ID>();
        List <Lead> leads_to_process = new List <Lead>();
        List <Lead> leads_to_update = new List <Lead>();
        
        //We only need to process where the value has changed or been assigned
        for(Lead n: lst_newleads){
            System.debug('Check 1');
            IF(n.Wildebeest_Ref__c != NULL){
                leads_to_process.add(n);
            }
        }
        
        for(Lead n: leads_to_process){
            // whether Wildebeest_Ref__c field has one value or more,
            // substringBefore method will only take first number (of any size)
            //map_LeadWBIds.put(n.Wildebeest_Ref__c.substringBefore(','), n.id);
           if(n.Wildebeest_Ref__c.contains(','))
            map_LeadWBIds.put(n.Wildebeest_Ref__c.substringBefore(','), n.id);
            else
             map_LeadWBIds.put(n.Wildebeest_Ref__c, n.id);
            
        }
        
        System.debug('Check 4'+map_LeadWBIds);
        IF(!map_LeadWBIds.keySet().isEmpty()){
            System.debug('Check 5');
            Set<String> wbidset= new  Set<String>();
            for(String wbid : map_LeadWBIds.keySet())
            {
                /* for every wildebeest number (say, 1234) there are 4 possibilities in Account
*  Account. Wildebeest_Ref__c = 1234 ( = wbid)
*  Account. Wildebeest_Ref__c = 1234,... ( = wbid,% )
*  Account. Wildebeest_Ref__c = ...,1234 ( = %,wbid )
*  Account. Wildebeest_Ref__c = ...,1234,... (= %,wbid,% )
*  we'll add these combination in set
*/
                wbidset.add(wbid);
                wbidset.add(wbid+',%');
                wbidset.add('%,'+wbid);
                wbidset.add('%,'+wbid+',%');
            }
            List <Account> lst_accounts = new List <Account>([SELECT ID, Wildebeest_Ref__c, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Contract_Value_Floorcare__c, Contract_Value_Washrooms__c, Contract_Value_Healthcare_Waste__c, Contract_Value_Greenleaf__c, Contract_Value_Wastekit__c, Entity_Type__c, Key_Account__c, Key_Account_Type__c,Greenleaf_Key_Account__c, Industry, Company_Reg_No__c, Company_Status__c, Website FROM Account WHERE Wildebeest_Ref__c LIKE: wbidset]);
            
            Map <String, String> map_AccName = new Map <String, String>();
            Map <String, Id> map_AccId = new Map <String, Id>();
            Map <String, String> map_BillingStreet = new Map <String, String>();
            Map <String, String> map_BillingCity = new Map <String, String>();
            Map <String, String> map_BillingState = new Map <String, String>();
            Map <String, String> map_BillingPostCode = new Map <String, String>();
            Map <String, Boolean> map_KeyAccount = new Map <String, Boolean>();
            Map <String, String> map_KeyAccountType = new Map <String, String>();
            Map <String, Boolean> map_GLKeyAccount = new Map <String, Boolean>();
            Map <String, String> map_CompRegNo = new Map <String, String>();
            Map <String, String> map_Website = new Map <String, String>();
            Map <String, String> map_Industry = new Map <String, String>();
            Map <String, String> map_CompStatus = new Map <String, String>();
            Map <String, String> map_EntityType = new Map <String, String>();
            Map <String, Double> map_ConValue_Floorcare = new Map <String, Double>();
            Map <String, Double> map_ConValue_Washrooms = new Map <String, Double>();
            Map <String, Double> map_ConValue_HCWaste = new Map <String, Double>();
            Map <String, Double> map_ConValue_Greenleaf = new Map <String, Double>();
            Map <String, Double> map_ConValue_WK = new Map <String, Double>();
            Map <String, ID> map_LeadQueues = new Map <String, ID>();
            
            List <QueueSObject> lst_leadqueues = new List <QueueSObject>([SELECT ID, QueueId, SObjectType, Queue.DeveloperName FROM QueueSObject WHERE SObjectType = 'Lead']);
            
            IF(!lst_leadqueues.isEmpty()){
                for(QueueSObject q: lst_leadqueues){
                map_LeadQueues.put(q.Queue.DeveloperName, q.QueueId);                     
                }
            }
            
            IF(!lst_accounts.isEmpty()){
                for(Account a: lst_accounts){
                    //at this point each account will have one or more wildebeest numbers
                    // we iterate over these numbers and check whether any matching lead exists or not
                    for (String s: a.Wildebeest_Ref__c.split(',')){
                        System.debug('Check 6');
                        Boolean result = map_LeadWBIds.containsKey(s);
                        IF(result == TRUE){
                            System.debug('Check 7');
                            map_AccName.put(s, a.Name);
                            map_AccId.put(s, a.ID);
                            map_BillingStreet.put(s, a.BillingStreet);
                            map_BillingCity.put(s, a.BillingCity);
                            map_BillingState.put(s, a.BillingState);
                            map_BillingPostCode.put(s, a.BillingPostalCode);
                            map_KeyAccount.put(s, a.Key_Account__c);
                            map_KeyAccountType.put(s, a.Key_Account_Type__c);
                            map_GLKeyAccount.put(s, a.Greenleaf_Key_Account__c);
                            map_CompRegNo.put(s, a.Company_Reg_No__c);
                            map_Website.put(s, a.Website);
                            map_Industry.put(s, a.Industry);
                            map_CompStatus.put(s, a.Company_Status__c);
                            map_EntityType.put(s, a.Entity_Type__c);
                            map_ConValue_Floorcare.put(s, a.Contract_Value_Floorcare__c);
                            map_ConValue_Washrooms.put(s, a.Contract_Value_Washrooms__c);
                            map_ConValue_HCWaste.put(s, a.Contract_Value_Healthcare_Waste__c);
                            map_ConValue_Greenleaf.put(s, a.Contract_Value_Greenleaf__c);
                            map_ConValue_WK.put(s, a.Contract_Value_Wastekit__c);                           
                        }
                    }
                }
                System.debug('Check 7.1'+map_AccName);
                for(Lead l: leads_to_process){
                    String wildebeestRef = l.Wildebeest_Ref__c.substringBefore(',');
                    //map_AccName containsKey check for skip required field exception
                    if(map_LeadWBIds.containsKey(wildebeestRef) &&  map_AccName.containsKey(wildebeestRef)){
                        System.debug('wildebeestRef : '+ wildebeestRef);
                        System.debug('Check 8'+ map_AccName.get(wildebeestRef));
                        l.company = map_AccName.get(wildebeestRef);
                        l.Account__c = map_AccId.get(wildebeestRef);
                        l.street = map_BillingStreet.get(wildebeestRef);
                        l.city = map_BillingCity.get(wildebeestRef);
                        l.state = map_BillingState.get(wildebeestRef);
                        l.postalcode = map_BillingPostCode.get(wildebeestRef);
                        IF(map_KeyAccount.containsKey(wildebeestRef) &&  map_KeyAccount.get(wildebeestRef) && map_KeyAccountType.get(wildebeestRef) != 'Public Sector')
                        {
                            l.Key_Account__c = map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false;
                            l.OwnerId = map_LeadQueues.get('Key_Account_Leads');
                        }
                        else if(map_KeyAccount.containsKey(wildebeestRef) &&  map_KeyAccount.get(wildebeestRef) && map_KeyAccountType.get(wildebeestRef) == 'Public Sector')
                        {
                            l.Key_Account__c = map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false;
                            l.OwnerId = userinfo.getUserId();
                        }
                        else if(map_GLKeyAccount.containsKey(wildebeestRef) && map_GLKeyAccount.get(wildebeestRef) ){
                            l.Greenleaf_Key_Account__c = map_GLKeyAccount.containsKey(wildebeestRef) ? map_GLKeyAccount.get(wildebeestRef) : false;
                            l.OwnerId = userInfo.getUserId();                           
                        }
                        else if(map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false == FALSE && map_GLKeyAccount.containsKey(wildebeestRef) ? map_GLKeyAccount.get(wildebeestRef) : false == FALSE){
                            l.Key_Account__c = FALSE;
                            l.Greenleaf_Key_Account__c = FALSE;
                            l.OwnerId = userInfo.getUserId();
                        }
                        l.Key_Account_Type__c = map_KeyAccountType.get(wildebeestRef);
                        l.Company_Reg_No__c = map_CompRegNo.get(wildebeestRef);
                        l.Website = map_Website.get(wildebeestRef);
                        l.Industry = map_Industry.get(wildebeestRef);
                        l.Company_Status__c = map_CompStatus.get(wildebeestRef);
                        l.Entity_Type__c = map_EntityType.get(wildebeestRef);
                        IF(map_ConValue_Floorcare.get(wildebeestRef) == 0){
                            l.Contract_Value_Floorcare__c = 0;
                        }
                        else {
                            l.Contract_Value_Floorcare__c = map_ConValue_Floorcare.get(wildebeestRef);
                        }
                        IF(map_ConValue_Washrooms.get(wildebeestRef) == 0){
                            l.Contract_Value_Washrooms__c = 0;
                        }
                        else{
                            l.Contract_Value_Washrooms__c = map_ConValue_Washrooms.get(wildebeestRef);                            
                        }
                        IF(map_ConValue_HCWaste.get(wildebeestRef) == 0){
                            l.Contract_Value_Healthcare_Waste__c = 0;
                        }
                        else{
                            l.Contract_Value_Healthcare_Waste__c = map_ConValue_HCWaste.get(wildebeestRef);
                        }
                        IF(map_ConValue_Greenleaf.get(wildebeestRef) == 0){
                            l.Contract_Value_Greenleaf__c = 0;
                        }
                        else{
                            l.Contract_Value_Greenleaf__c = map_ConValue_Greenleaf.get(wildebeestRef);
                        }
                        IF(map_ConValue_WK.get(wildebeestRef) == 0){
                            l.Contract_Value_Wastekit__c = 0;
                        }
                        else{
                            l.Contract_Value_Wastekit__c = map_ConValue_WK.get(wildebeestRef);
                        }     
                        leads_to_update.add(l);
                    }
                }
            }
        }
    }
    
    public static void getaccountdataupdate(List <Lead> lst_oldleads, List <Lead> lst_newleads){
        system.debug('=====i am her215e==='+lst_oldleads.size());
        Set <ID> set_Leadids = new Set <ID>();
        Map <String, ID> map_LeadWBIds = new Map <String, ID>();
        List <Lead> leads_to_process = new List <Lead>();
        List <Lead> leads_to_update = new List <Lead>();
        map<Id, Lead> mapNewLead = new map<Id, Lead>(lst_newleads);
        //We only need to process where the value has changed or been assigned
        for(Lead o: lst_oldleads){
            Lead n = mapNewLead.get(o.id);
            IF(o.id == n.id){
                System.debug('Check 1');
                IF((o.Wildebeest_Ref__c == NULL && n.Wildebeest_Ref__c != NULL) || (o.Wildebeest_Ref__c != NULL && n.Wildebeest_Ref__c != NULL && o.Wildebeest_Ref__c != n.Wildebeest_Ref__c)){
                    leads_to_process.add(n);
                }
            }
        }
        
        //We need to handle the fact that there might be multiple values in the lead
        for(Lead n: leads_to_process){
          if(n.Wildebeest_Ref__c.contains(','))
            map_LeadWBIds.put(n.Wildebeest_Ref__c.substringBefore(','), n.id);
            else
             map_LeadWBIds.put(n.Wildebeest_Ref__c, n.id);
        }
        
        System.debug('Check 4'+map_LeadWBIds);
        IF(!map_LeadWBIds.keySet().isEmpty()){
            System.debug('Check 5');
            Set<String> wbidset= new  Set<String>();
            for(String wbid : map_LeadWBIds.keySet()){
                wbidset.add(wbid);
                wbidset.add(wbid+',%');
                wbidset.add('%,'+wbid);
                wbidset.add('%,'+wbid+',%');
            }
            List <Account> lst_accounts = new List <Account>([SELECT ID, Wildebeest_Ref__c, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Contract_Value_Floorcare__c, Contract_Value_Washrooms__c, Contract_Value_Healthcare_Waste__c, Contract_Value_Greenleaf__c, Contract_Value_Wastekit__c, Entity_Type__c, Key_Account__c, Key_Account_Type__c, Greenleaf_Key_Account__c, Industry, Company_Reg_No__c, Company_Status__c, Website FROM Account WHERE Wildebeest_Ref__c LIKE: wbidset]);
            
             
            Map <String, String> map_AccName = new Map <String, String>();
            Map <String, Id> map_AccId = new Map <String, Id>();
            Map <String, String> map_BillingStreet = new Map <String, String>();
            Map <String, String> map_BillingCity = new Map <String, String>();
            Map <String, String> map_BillingState = new Map <String, String>();
            Map <String, String> map_BillingPostCode = new Map <String, String>();
            Map <String, Boolean> map_KeyAccount = new Map <String, Boolean>();
            Map <String, String> map_KeyAccountType = new Map <String, String>();
            Map <String, Boolean> map_GLKeyAccount = new Map <String, Boolean>();
            Map <String, String> map_CompRegNo = new Map <String, String>();
            Map <String, String> map_Website = new Map <String, String>();
            Map <String, String> map_Industry = new Map <String, String>();
            Map <String, String> map_CompStatus = new Map <String, String>();
            Map <String, String> map_EntityType = new Map <String, String>();
            Map <String, Double> map_ConValue_Floorcare = new Map <String, Double>();
            Map <String, Double> map_ConValue_Washrooms = new Map <String, Double>();
            Map <String, Double> map_ConValue_HCWaste = new Map <String, Double>();
            Map <String, Double> map_ConValue_Greenleaf = new Map <String, Double>();
            Map <String, Double> map_ConValue_WK = new Map <String, Double>();
            Map <String, ID> map_LeadQueues = new Map <String, ID>();
            
            List <QueueSObject> lst_leadqueues = new List <QueueSObject>([SELECT ID, QueueId, SObjectType, Queue.DeveloperName FROM QueueSObject WHERE SObjectType = 'Lead']);
            
            IF(!lst_leadqueues.isEmpty()){
                for(QueueSObject q: lst_leadqueues){
                map_LeadQueues.put(q.Queue.DeveloperName, q.QueueId);                     
                }
            }
            
            IF(!lst_accounts.isEmpty()){
                for(Account a: lst_accounts){
                    for (String s: a.Wildebeest_Ref__c.split(',')){
                        System.debug('Check 6');
                        Boolean result=map_LeadWBIds.containsKey(s);
                        IF(result == TRUE){
                            System.debug('Check 7');
                            map_AccName.put(s, a.Name);
                            map_AccId.put(s, a.ID);
                            map_BillingStreet.put(s, a.BillingStreet);
                            map_BillingCity.put(s, a.BillingCity);
                            map_BillingState.put(s, a.BillingState);
                            map_BillingPostCode.put(s, a.BillingPostalCode);
                            map_KeyAccount.put(s, a.Key_Account__c);
                            map_KeyAccountType.put(s, a.Key_Account_Type__c);
                            map_GLKeyAccount.put(s, a.Greenleaf_Key_Account__c);
                            map_CompRegNo.put(s, a.Company_Reg_No__c);
                            map_Website.put(s, a.Website);
                            map_Industry.put(s, a.Industry);
                            map_CompStatus.put(s, a.Company_Status__c);
                            map_EntityType.put(s, a.Entity_Type__c);
                            map_ConValue_Floorcare.put(s, a.Contract_Value_Floorcare__c);
                            map_ConValue_Washrooms.put(s, a.Contract_Value_Washrooms__c);
                            map_ConValue_HCWaste.put(s, a.Contract_Value_Healthcare_Waste__c);
                            map_ConValue_Greenleaf.put(s, a.Contract_Value_Greenleaf__c);
                            map_ConValue_WK.put(s, a.Contract_Value_Wastekit__c);
                        }
                    }
                }
                 
                 
                System.debug('Check 7.1'+map_AccName);
                for(Lead l: leads_to_process){
                    String wildebeestRef = l.Wildebeest_Ref__c.substringBefore(',');
                    //map_AccName containsKey check for skip required field exception
                    if(map_LeadWBIds.containsKey(wildebeestRef) && map_AccName.containsKey(wildebeestRef)){
                        System.debug('Check 8'+l);
                        System.debug('Check 8'+map_AccName.get(wildebeestRef));
                       
                        l.company = map_AccName.get(wildebeestRef);
                        l.Account__c = map_AccId.get(wildebeestRef);
                        l.street = map_BillingStreet.get(wildebeestRef);
                        l.city = map_BillingCity.get(wildebeestRef);
                        l.state = map_BillingState.get(wildebeestRef);
                        l.postalcode = map_BillingPostCode.get(wildebeestRef);
                       
                        IF(map_KeyAccount.containsKey(wildebeestRef) &&  map_KeyAccount.get(wildebeestRef) && map_KeyAccountType.get(wildebeestRef) != 'Public Sector')
                        {
                            l.Key_Account__c = map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false;
                             l.OwnerId = map_LeadQueues.get('Key_Account_Leads');
                        }
                        else if(map_KeyAccount.containsKey(wildebeestRef) &&  map_KeyAccount.get(wildebeestRef) && map_KeyAccountType.get(wildebeestRef) == 'Public Sector')
                       {
                            l.Key_Account__c = map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false;
                            System.debug('=============lead before--'+l.OwnerId);
                            l.OwnerId = userinfo.getUserId();
                       }
                           else if(map_GLKeyAccount.containsKey(wildebeestRef) && map_GLKeyAccount.get(wildebeestRef) ){
                            l.Greenleaf_Key_Account__c = map_GLKeyAccount.containsKey(wildebeestRef) ? map_GLKeyAccount.get(wildebeestRef) : false;
                            l.Key_Account__c = l.Key_Account__c = map_KeyAccount.containsKey(wildebeestRef) ? map_KeyAccount.get(wildebeestRef) : false;
                        }
                        else{
                            l.Key_Account__c = FALSE;
                            l.Greenleaf_Key_Account__c = FALSE;
                            l.OwnerId = userinfo.getUserId();
                        }
                        l.Key_Account_Type__c = map_KeyAccountType.get(wildebeestRef);
                        l.Company_Reg_No__c = map_CompRegNo.get(wildebeestRef);
                        l.Website = map_Website.get(wildebeestRef);
                        l.Industry = map_Industry.get(wildebeestRef);
                        l.Company_Status__c = map_CompStatus.get(wildebeestRef);
                        l.Entity_Type__c = map_EntityType.get(wildebeestRef);
                        IF(map_ConValue_Floorcare.get(wildebeestRef) == 0){
                            l.Contract_Value_Floorcare__c = 0;
                        }
                        else {
                            l.Contract_Value_Floorcare__c = map_ConValue_Floorcare.get(wildebeestRef);
                        }
                        IF(map_ConValue_Washrooms.get(wildebeestRef) == 0){
                            l.Contract_Value_Washrooms__c = 0;
                        }
                        else{
                            l.Contract_Value_Washrooms__c = map_ConValue_Washrooms.get(wildebeestRef);                            
                        }
                        IF(map_ConValue_HCWaste.get(wildebeestRef) == 0){
                            l.Contract_Value_Healthcare_Waste__c = 0;
                        }
                        else{
                            l.Contract_Value_Healthcare_Waste__c = map_ConValue_HCWaste.get(wildebeestRef);
                        }
                        IF(map_ConValue_Greenleaf.get(wildebeestRef) == 0){
                            l.Contract_Value_Greenleaf__c = 0;
                        }
                        else{
                            l.Contract_Value_Greenleaf__c = map_ConValue_Greenleaf.get(wildebeestRef);
                        }
                        IF(map_ConValue_WK.get(wildebeestRef) == 0){
                            l.Contract_Value_Wastekit__c = 0;
                        }
                        else{
                            l.Contract_Value_Wastekit__c = map_ConValue_WK.get(wildebeestRef);
                        }                       
                        leads_to_update.add(l);
                    }
                }
                bg_LeadHelper.populateFLTFields(leads_to_update);
            }
        }
    }
    
    public static void getaccountplanlinksinsert(List <Lead> lst_newleads){
        
        Set <ID> set_Leadids = new Set <ID>();
        Map <String, ID> map_LeadWBIds = new Map <String, ID>();
        List <Lead> leads_to_process = new List <Lead>();
        List <Lead> leads_to_update = new List <Lead>();
        Set <ID> set_Accids = new Set <ID>();
        List <Account_Plan_Link__c> lst_apls = new List <Account_Plan_Link__c>();
        Map <Id, Id> map_APid_Accids = new Map <Id, Id>();
        
        for(Lead n: lst_newleads){
            System.debug('Check 1');
            IF(n.Wildebeest_Ref__c != NULL){
                leads_to_process.add(n);
            }
        }
        
        for(Lead n: leads_to_process){
            // whether Wildebeest_Ref__c field has one value or more,
            // substringBefore method will only take first number (of any size)
            map_LeadWBIds.put(n.Wildebeest_Ref__c.substringBefore(','), n.id);
            
        }
        
        System.debug('Check 4'+map_LeadWBIds);
        IF(!map_LeadWBIds.keySet().isEmpty()){
            System.debug('Check 5');
            Set<String> wbidset= new  Set<String>();
            for(String wbid : map_LeadWBIds.keySet())
            {
                /* for every wildebeest number (say, 1234) there are 4 possibilities in Account
*  Account. Wildebeest_Ref__c = 1234 ( = wbid)
*  Account. Wildebeest_Ref__c = 1234,... ( = wbid,% )
*  Account. Wildebeest_Ref__c = ...,1234 ( = %,wbid )
*  Account. Wildebeest_Ref__c = ...,1234,... (= %,wbid,% )
*  we'll add these combination in set
*/
                wbidset.add(wbid);
                wbidset.add(wbid+',%');
                wbidset.add('%,'+wbid);
                wbidset.add('%,'+wbid+',%');
            }
            List <Account> lst_accounts = new List <Account>([SELECT ID, Wildebeest_Ref__c, Name FROM Account WHERE Wildebeest_Ref__c LIKE: wbidset]);
            
            IF(!lst_accounts.isEmpty()){
                
                for(Account a: lst_accounts){
                    set_Accids.add(a.id);
                }
                
                List <Account_Plan__c> lst_aps = new List <Account_Plan__c>([SELECT ID, Account__c, Account__r.Wildebeest_Ref__c FROM Account_Plan__c WHERE Account__c in: set_Accids]);
                
                IF(!lst_aps.isEmpty()){
                    
                    for(Account_Plan__c ap: lst_aps){
                        map_APid_Accids.put(ap.Account__c, ap.id);
                    }                
                    
                    for(Account a: lst_accounts){
                        //at this point each account will have one or more wildebeest numbers
                        // we iterate over these numbers and check whether any matching lead exists or not
                        for (String s: a.Wildebeest_Ref__c.split(',')){
                            System.debug('Check 6');
                            Boolean result = map_LeadWBIds.containsKey(s);
                            IF(result == TRUE){
                                Account_Plan_Link__c apl = new Account_Plan_Link__c();
                                apl.Lead__c = map_LeadWBIds.get(s);
                                IF(map_APid_Accids.get(a.id) != Null){
                                apl.Account_Plan__c = map_APid_Accids.get(a.id);
                                lst_apls.add(apl);
                                }                                
                            }
                        }
                    }
                    IF(!lst_apls.isEmpty()){
                        insert lst_apls;
                    }
                }
            }
        }
    }
    
    //If the owner of the lead has changed, change the owner of any lead products to match
    public static void ChangeLeadProductsOwner(List <Lead> lst_newleads, List <Lead> lst_oldleads){
        
        Set <ID> set_changedownerleadids = new Set <Id>();
        map<id, Lead> mapOldLead = new map<Id, Lead>(lst_oldleads);//old map
        map<id, Lead> mapNewLead = new map<Id, Lead>(lst_newleads);//new map
        for(Lead nl: lst_newleads){
            IF(nl.OwnerId != mapOldLead.get(nl.id).OwnerId){
                set_changedownerleadids.add(nl.id);
            }
        }
        
        List <Lead_Product__c> lst_leadproducts = new List <Lead_Product__c>([SELECT ID, Lead__c From Lead_Product__c WHERE Lead__c in: set_changedownerleadids]);
        
        IF(!lst_leadproducts.isEmpty()){
            
            for(Lead_Product__c lp: lst_leadproducts){
                if(lp.Lead__c != null && mapNewLead.containsKey(lp.Lead__c)  )
                {
                    lp.OwnerId = mapNewLead.get(lp.Lead__c).OwnerId;
                }
                /*
                for(Lead nll: lst_newleads){
                    IF(lp.Lead__c == nll.id){
                        lp.OwnerId = nll.OwnerId;
                    }
                }*/
            }
            Try{
                Update lst_leadproducts;
            }catch(exception e){
                
            }
        }
    }
}