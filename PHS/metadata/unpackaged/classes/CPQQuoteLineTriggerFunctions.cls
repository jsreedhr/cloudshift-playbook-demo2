public without sharing class CPQQuoteLineTriggerFunctions {
     public static Boolean isBeforeInsert = true;
    public static Boolean isBeforeUpdate = true;
    
    public static void getProductPBEPricing(List <SBQQ__QuoteLine__c> lst_qls){
        //This method should retrieve product pricing from the price book entry records
        Map <ID, ID> mapquotelineIDpricebookID = new Map <ID, ID>();
        Map <ID, ID> mapquotelineIDproductID = new Map <ID, ID>();
        Map <ID, ID> mapquoteIDproductID = new Map <ID, ID>();
        
        for(SBQQ__QuoteLine__c ql : lst_qls){
            System.debug('--QL'+ ql);
            IF(ql.SBQQ__Product__c !=null){
                mapquotelineIDpricebookID.put(ql.Id, ql.Price_Book_Id__c);
                mapquotelineIDproductID.put(ql.Id, ql.SBQQ__Product__c);
            }           
        }
        
        System.debug(mapquotelineIDpricebookID);
        System.debug(mapquotelineIDproductID);
        
        
        IF(mapquotelineIDproductID.values()!=null){
            
            List <PriceBookEntry> lst_pbes = new List <PriceBookEntry>([SELECT ID, Product2Id, Pricebook2id, UnitPrice, Minimum_Price__c, Maximum_Price__c, Target_Price__c FROM PriceBookEntry WHERE Pricebook2id in: mapquotelineIDpricebookID.values() AND Product2Id in: mapquotelineIDproductID.values()]);
            
            System.debug('Size of price book entry list is' + lst_pbes.size());

            IF(!lst_pbes.isEmpty()){
                
                for(SBQQ__QuoteLine__c ql: lst_qls){
                    for(PriceBookEntry pb: lst_pbes){
                        IF(ql.Price_Book_Id__c == pb.Pricebook2id){
                            IF(ql.SBQQ__Product__c == pb.Product2Id){
                                
                                System.debug('--Listprice'+ql.SBQQ__ListPrice__c);
                       
                                IF(pb.Minimum_Price__c == null || pb.Target_Price__c ==  null || pb.Maximum_Price__c == null){
                                    ql.addError('At least one product is missing a minimum or target or maximum price. Please ask your Administrator to add one.');
                                }
                                IF(ql.Standard_Service_Frequency_Formula__c != null){
                                    
                                    //Hygiene Floorcare Mat Product with 26 as std service frequency
                                    IF((ql.SBQQ__ProductFamily__c == 'Floorcare' && ql.PHSCPQ_Product_Range__c == 'Floorcare Entrance Mats' && ql.PHSCPQ_Product_Type__c == 'Loose Lay Mats') || (ql.SBQQ__ProductFamily__c == 'Floorcare' && ql.PHSCPQ_Product_Range__c == 'Floorcare Workplace Mats') || (ql.SBQQ__ProductFamily__c == 'Floorcare' && ql.PHSCPQ_Product_Range__c == 'Floorcare Entrance Mats' && ql.PHSCPQ_Product_Type__c == 'Fitted Mats' && ql.PHSCPQ_Product_Sub_Type__c == 'Fitted Lift & Lay Mats')){
                                        System.debug('--test1--');
                                            double quantityfactor;
                                            IF(ql.SBQQ__Quantity__c == 1){
                                                quantityfactor = 1;                                                
                                            }
                                            IF(ql.SBQQ__Quantity__c >= 2 && ql.SBQQ__Quantity__c <5){
                                                quantityfactor = 0.75;                                                
                                            }
                                            IF(ql.SBQQ__Quantity__c >=5){
                                                quantityfactor = 0.55;                                                
                                            }
                                            double servicefrequencyfactor;
                                            IF(Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c) == 26){
                                                servicefrequencyfactor = 1;                                                
                                            }
                                            //This won't be correct as we will need a percentage grid to differentiate between 1 and 26 etc.
                                            IF(Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c) < 26){
                                                servicefrequencyfactor = 1.2;                                                
                                            }
                                            //This won't be correct as we will need a percentage grid to differentiate between values between 26 & 52 etc.
                                            IF(Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c) > 26){
                                                servicefrequencyfactor = 0.9;                                                
                                            }
                                            System.debug('quantity factor is' + quantityfactor);
                                            System.debug('service frequency factor is' + servicefrequencyfactor);
                                            ql.Minimum_Price__c = (pb.Minimum_Price__c*quantityfactor*servicefrequencyfactor)*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                            ql.Maximum_Total__c = (pb.Maximum_Price__c*quantityfactor*servicefrequencyfactor)*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);  
                                            ql.Target_Price__c = (pb.Target_Price__c*quantityfactor*servicefrequencyfactor)*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                            ql.Target_Total__c = (pb.Target_Price__c*quantityfactor*servicefrequencyfactor)*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);    
                                            ql.Maximum_Price__c = (pb.Maximum_Price__c*quantityfactor*servicefrequencyfactor)*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                            ql.Minimum_Total__c = (pb.Minimum_Price__c*quantityfactor*servicefrequencyfactor)*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                            ql.Minimum_Unit_Price__c = (pb.Minimum_Price__c*quantityfactor*servicefrequencyfactor);
                                            ql.Target_Unit_Price__c = (pb.Target_Price__c*quantityfactor*servicefrequencyfactor);
                                        	ql.Maximum_Unit_Price__c = (pb.Maximum_Price__c*quantityfactor*servicefrequencyfactor);
                                    }
                                    //Must be a different type of Hygiene floorcare mat where the standard freq is 4 & only allowable std frequency will be 4 hence servicefrequencyfactors and quantityfactors are not relevant. There may be another branch needed for WK and GL mats
                                    else If(ql.SBQQ__ProductFamily__c == 'Floorcare'){
                                        System.debug('--test2--');
                                        if(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c != null)
                                        {
                                        ql.Minimum_Price__c = pb.Minimum_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Maximum_Total__c = pb.Maximum_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);  
                                        ql.Target_Price__c = pb.Target_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Target_Total__c = pb.Target_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);    
                                        ql.Maximum_Price__c = pb.Maximum_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Minimum_Total__c = pb.Minimum_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Minimum_Unit_Price__c = pb.Minimum_Price__c;
                                        ql.Maximum_Unit_Price__c = pb.Maximum_Price__c;
                                        ql.Target_Unit_Price__c = pb.Target_Price__c;
                                        }
                                      
                                        else
                                        {
                                        ql.Minimum_Price__c = pb.Minimum_Price__c;
                                        ql.Maximum_Total__c = pb.Maximum_Price__c*ql.SBQQ__Quantity__c;
                                        ql.Target_Price__c = pb.Target_Price__c*ql.SBQQ__Quantity__c;
                                        ql.Target_Total__c = pb.Target_Price__c;
                                        ql.Maximum_Price__c = pb.Maximum_Price__c;
                                        ql.Minimum_Total__c =pb.Minimum_Price__c*ql.SBQQ__Quantity__c;
										ql.Minimum_Unit_Price__c = pb.Minimum_Price__c;
                                        ql.Maximum_Unit_Price__c = pb.Maximum_Price__c;
                                        ql.Target_Unit_Price__c = pb.Target_Price__c;  
                                        }
                                        
                                                                                                                                
                                    }                                     
                                    //Non-Mat Product
                                    else If(pb.Minimum_Price__c!= null && pb.Target_Price__c!= null && pb.Maximum_Price__c!=  null){
                                        System.debug('--test3--');
                                         if(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c != null)
                                        {
                                        ql.Minimum_Price__c = pb.Minimum_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Maximum_Total__c = pb.Maximum_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);  
                                        ql.Target_Price__c = pb.Target_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Target_Total__c = pb.Target_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);    
                                        ql.Maximum_Price__c = pb.Maximum_Price__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Minimum_Total__c = pb.Minimum_Price__c*ql.SBQQ__Quantity__c*Double.valueOf(ql.PHSCPQ_Actual_vs_Seasonal_Formula__c);
                                        ql.Minimum_Unit_Price__c = pb.Minimum_Price__c;
                                        ql.Maximum_Unit_Price__c = pb.Maximum_Price__c;
                                        ql.Target_Unit_Price__c = pb.Target_Price__c;
                                        }
                                        else
                                        {
                                            ql.Minimum_Price__c = pb.Minimum_Price__c;
                                             ql.Maximum_Total__c = pb.Maximum_Price__c*ql.SBQQ__Quantity__c;
                                              ql.Target_Price__c = pb.Target_Price__c;
                                               ql.Target_Total__c = pb.Target_Price__c*ql.SBQQ__Quantity__c;
                                                ql.Maximum_Price__c = pb.Maximum_Price__c;
                                                ql.Minimum_Total__c = pb.Minimum_Price__c*ql.SBQQ__Quantity__c;
                                            	ql.Minimum_Unit_Price__c = pb.Minimum_Price__c;
                                            	ql.Maximum_Unit_Price__c = pb.Maximum_Price__c;
                                            	ql.Target_Unit_Price__c = pb.Target_Price__c;
                                             
                                        }
                                    }
                                }
                                else{
                                    System.debug('--test4--');
                                    //for everything else non visit related, there won't be a service frequency hence the 'price' should just be the unit price * quantity
                                    IF(pb.Minimum_Price__c != null && pb.Target_Price__c !=null && pb.Maximum_Price__c !=null){
                                        ql.Minimum_Price__c = pb.Minimum_Price__c;
                                        ql.Minimum_Total__c = pb.Minimum_Price__c*ql.SBQQ__Quantity__c;    
                                        ql.Target_Price__c = pb.Target_Price__c;
                                        ql.Target_Total__c = pb.Target_Price__c*ql.SBQQ__Quantity__c;
                                        ql.Maximum_Price__c = pb.Maximum_Price__c;
                                        ql.Maximum_Total__c = pb.Maximum_Price__c*ql.SBQQ__Quantity__c;
                                        ql.Minimum_Unit_Price__c = pb.Minimum_Price__c;
                                        ql.Maximum_Unit_Price__c = pb.Maximum_Price__c;
                                        ql.Target_Unit_Price__c = pb.Target_Price__c;
                                    }
                                    IF(pb.Minimum_Price__c == null || pb.Target_Price__c == null || pb.Maximum_Price__c == null){
                                        ql.addError('At least one product is missing a minimum or target or maximum price. Please ask your Administrator to add one.');
                                    }
                                }
                            }                            
                        } 
                    }
                }
            }        
        }
    }
    
    public static void ValidateActualContractLength(List <SBQQ__QuoteLine__c> lst_quotelines, Map<Id, SBQQ__QuoteLine__c> oldLineItems){
        //This method should check that the actual service frequency selected - PHSCPQ_Standard_Contract_Term__c in quote line is allowed in PHSCPQ_Allowed_Contract_Term__c on product.
        // method should check upon save that what has been selected as 'actual' is a valid selection. If not, throw an error message but expand the error message so we can say something like: 'You have chosen an invalid service frequency. Please select from either '5, 10 or 12' because these values are allowable values
        
        for(SBQQ__QuoteLine__c ql : lst_quotelines)
        {
            if(Trigger.isInsert || (!Trigger.isInsert && (ql.PHSCPQ_Standard_Contract_Term__c != oldLineItems.get(ql.id).PHSCPQ_Standard_Contract_Term__c )))
            {
                if(String.isNotBlank(ql.PHSCPQ_Allowed_Contract_Term__c))
                {
                    List<String> allowedContrFreq = new List<String> ();
                    String allowed = ql.PHSCPQ_Allowed_Contract_Term__c;
                    if(allowed.contains(';'))
                        allowedContrFreq=allowed.split(';');
                    else
                        allowedContrFreq.add(allowed);
                    if(!(allowedContrFreq.contains(ql.PHSCPQ_Standard_Contract_Term__c)))
                        ql.addError('You have either chosen an invalid contract term or failed to specify a contract term for at least one product. Please select a valid contract term for all products.');
                }
            }
        }
        
    }
    
    public static void ValidateActualServiceFrequency(List <SBQQ__QuoteLine__c> lst_quotelines, Map<Id, SBQQ__QuoteLine__c> oldLineItems){
        
        //This method should check that the actual service frequency selected - PHSCPQ_Standard_Service_Frequency__c in quote line is allowed in PHSCPQ_Allowed_Service_Frequency__c on product
        // method should check upon save that what has been selected as 'actual' is a valid selection. If not, throw an error message but expand the error message so we can say something like: 'You have chosen an invalid service frequency. Please select from either '5, 10 or 12' because these values are allowable values
        
        for(SBQQ__QuoteLine__c ql : lst_quotelines)
        {
            if(Trigger.isInsert || (!Trigger.isInsert && (ql.PHSCPQ_Standard_Service_Frequency__c != oldLineItems.get(ql.id).PHSCPQ_Standard_Service_Frequency__c )))
            {
                if(String.isNotBlank(ql.PHSCPQ_Allowed_Service_Frequency__c))
                {
                    String allowed = ql.PHSCPQ_Allowed_Service_Frequency__c;
                    List<String> allowedSerFreq = allowed.split(';');
                    if(allowed.contains(';'))
                        allowedSerFreq=allowed.split(';');
                    else
                        allowedSerFreq.add(allowed);
                    if(!(allowedSerFreq.contains(ql.PHSCPQ_Standard_Service_Frequency__c)))
                        ql.addError('You have either omitted or chosen an invalid service frequency for at least one visit based product. Please ensure relevant products have a valid service frequency defined.');
                }
            }
        }
        
        
    }  
    
    //Get allowed contract term on Product 
    /*
public static Set<String> getProductAllowedContracts()
{
Set<String> options = new Set<String>();

Schema.DescribeFieldResult fieldResult = Product2.PHSCPQ_Allowed_Contract_Term__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

for( Schema.PicklistEntry f : ple)
{
options.add(f.getValue());
}      
return options;

}
//Get allowed service frequency on Product 
public static Set<String> getProductAllowedServicefrequency()
{
Set<String> options = new Set<String>();

Schema.DescribeFieldResult fieldResult = Product2.PHSCPQ_Allowed_Service_Frequency__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

for( Schema.PicklistEntry f : ple)
{
options.add(f.getValue());
}      
return options;

}
*/
}