/**
 *  Class Name: ContactMapController  
 *  Description: This is a Controller Class for ContactMap.
 *  Company: Standav
 *  CreatedDate:06/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Hormese             06-11-2017                 Original version
 */
public class ContactMapController {
    
    public String address{
        get;
        set;
    }
     public String streetAddress{
        get;
        set;
    }
    private final Contact Contactrecord;

    public ContactMapController(ApexPages.StandardController stdController){
        this.Contactrecord = (Contact)stdController.getRecord();
        ContactAddressAllocate();
    }
    
    public void ContactAddressAllocate(){
         address='';
        streetAddress='';
        if(Contactrecord.MailingPostalCode != NULL)
            address = Contactrecord.MailingPostalCode;
        else
            address = Contactrecord.MailingStreet + ' ' + Contactrecord.MailingCity;
        system.debug('Address----->'+address);
        if(Contactrecord.MailingStreet!=null)
        {
        streetAddress=Contactrecord.MailingStreet.replace('\n', ' ');
        streetAddress=streetAddress.replace('\r', '  ');
        }
        address =address.replace('\r', '  ').replace('\n', ' ');
        
    }
}