/**
 *  @Class Name:    LeadCompetitorsControllerTest
 *  @Description:   This is a test class for LeadCompetitorsController
 *  @Company:       dQuotient
 *  CreatedDate:    27/11/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S           27/11/2017                  Original Version
 */
@isTest
private class LeadCompetitorsControllerTest {
        /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        User u = bg_Test_Data_Utils.createUserWithEmployeeNumber('1');
        insert u;
        
        Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true,
                  Price_Book_Type__c='Standard'
                  );
        update standardPricebook; 
        
        List<Account> accList= new List<Account>();
        Account acc = bg_Test_Data_Utils.createAccount('N');
        acc.Name = Label.Unknown_Competitor_Name;
        acc.Company_Status__c = 'Competitor';
        accList.add(acc);
        Account acc1 = bg_Test_Data_Utils.createAccount('M');
        // acc1.Name = Label.Unknown_Competitor_Name;
        acc1.Company_Status__c = 'Competitor';
        accList.add(acc1);
        insert accList;
        
        Opportunity opp = bg_Test_Data_Utils.createOpportunity('Test Opp', acc.id);
        insert opp;
        
        List<Lead> leadList = new List<Lead>();
        Lead lead1 = bg_Test_Data_Utils.createLead('1qqwe');
        lead1.OwnerId = u.id;
        leadList.add(lead1);
        
        Lead lead2 = bg_Test_Data_Utils.createLead('2dwwr');
        lead2.OwnerId = u.id;
        leadList.add(lead2);
       // insert leadList;
        
          Lead lead3 = bg_Test_Data_Utils.createLead('2dwwt');
        lead3.OwnerId = u.id;
        leadList.add(lead3);
       
        
         Lead lead4 = bg_Test_Data_Utils.createLead('2dwws');
        lead4.OwnerId = u.id;
        leadList.add(lead4);
        insert leadList;
        
        List<Competitor__c> compList = new List<Competitor__c>();
        Competitor__c competitor1 = bg_Test_Data_Utils.createCompetitor(lead1.Id);
        competitor1.Competitor_Account__c = acc.Id;
        competitor1.Opportunity__c = opp.Id;
        competitor1.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
        compList.add(competitor1);
        
        Competitor__c competitor2 = bg_Test_Data_Utils.createCompetitor(lead2.Id);
       competitor2.Competitor_Account__c = acc1.Id;
        competitor2.Opportunity__c = opp.Id;
        competitor2.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
        compList.add(competitor2);
        
        Competitor__c competitor3 = bg_Test_Data_Utils.createCompetitor(lead3.Id);
       // competitor3.Competitor_Account__c = acc1.Id;
        competitor3.Opportunity__c = opp.Id;
        competitor3.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
         competitor3.Product_Offering__c = 'Test Product_Offering__c';
                compList.add(competitor3);
                
                  Competitor__c cm = bg_Test_Data_Utils.createCompetitor(lead4.Id);
       // competitor3.Competitor_Account__c = acc1.Id;
        cm.Opportunity__c = opp.Id;
        cm.Unknown_Competitor_Details__c = 'Test Unknown_Competitor_Details__c';
       //  competitor3.Product_Offering__c = 'Test Product_Offering__c';
         compList.add(cm);
                
        insert compList;
        
    }
    
    // Test createRecords method in LeadCompetitorsController
	private static testMethod void testcreateRecords() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '1qqwe'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.createRecords();
        }
        test.stopTest();
	}
	
	// Test addRow method in LeadCompetitorsController
	private static testMethod void testaddRow() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '1qqwe'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.addRow();
        }
        test.stopTest();
	}
	
	// Test selectAll method in LeadCompetitorsController
	private static testMethod void testselectAllfalse() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '1qqwe'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.selectAll();
        }
        test.stopTest();
	}
	
	// Test addRow method in LeadCompetitorsController
	private static testMethod void testdeleteSelectedRows() {
	    
        User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '1qqwe'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.selectAll = true;
            ext.selectAll();
            ext.deleteSelectedRows();
        }
        test.stopTest();
	}
	
	// Test delRow method in LeadCompetitorsController
	private static testMethod void testdelRow() {
	    User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '1qqwe'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.RowNo = '0';
            ext.delRow();
        }
        test.stopTest();
	}
	
	// Test checkduplicates method in LeadCompetitorsController
	private static testMethod void testcheckduplicates() {
	    User u = [Select Email, Off_Sick_Annual_Leave__c from User where Alias = 'standt'];
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '2dwwr'];

        test.startTest();
        System.runAs(u) {
            // The following code runs as user 'u'
            Test.setCurrentPageReference(new PageReference('Page.LeadCompetitor')); 
            System.currentPageReference().getParameters().put('id', testLead.Id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
            ext.checkduplicates();
        }
        test.stopTest();
	}
		private static testMethod void testsaveCompetitor() {
        Lead testLead = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '2dwwr'];
        Lead testLead2 = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '2dwwt'];
        Lead testLead3 = [Select Id, FLT_Postal_Code__c, OwnerId from Lead where LastName= '2dwws'];
        
       
        
        test.startTest();
            PageReference pageRef = Page.LeadCompetitor;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', testLead.id);
            Apexpages.StandardController sc = new Apexpages.StandardController(testLead);
            LeadCompetitorsController ext = new  LeadCompetitorsController(sc);
           ext.saveCompetitor();
           
             PageReference pageRef2 = Page.LeadCompetitor;
            Test.setCurrentPage(pageRef2);
            pageRef2.getParameters().put('id', testLead2.id);
            Apexpages.StandardController sc2 = new Apexpages.StandardController(testLead2);
            LeadCompetitorsController ext2 = new  LeadCompetitorsController(sc2);
           ext2.saveCompetitor();
           
            PageReference pageRef3 = Page.LeadCompetitor;
            Test.setCurrentPage(pageRef3);
            pageRef3.getParameters().put('id', testLead3.id);
            Apexpages.StandardController sc3 = new Apexpages.StandardController(testLead3);
            LeadCompetitorsController ext3 = new  LeadCompetitorsController(sc3);
           ext3.saveCompetitor();
           
           
        test.stopTest();
          
        
    }
    	
}