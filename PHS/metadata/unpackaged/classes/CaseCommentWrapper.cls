/**********************************************************************
* bg_CaseCommentWrapper:
*
* Wrapper class for case comments to send to Wildebeest in a 
* different format
* Created By: BrightGen Ltd (AL)
* Created Date: 21/12/2016
*
* Changes: 
***********************************************************************/
public with sharing class CaseCommentWrapper
{
    public CaseCommentWrapper(SObject sObj, Map<id, User> userMap)
    {
        String dateFormat = Wildebeest_Integration__c.getValues('Host').Wildebeest_Date_Format__c != null ? 
                                Wildebeest_Integration__c.getValues('Host').Wildebeest_Date_Format__c : 'dd/MM/yyyy HH:mm:ss';

        if (sObj.getSObjectType() == Schema.CaseComment.getSObjectType())
        {
            CaseComment caseCom = (CaseComment)sObj;
 
            ParentId = caseCom.ParentId;
            LastModifiedDate = caseCom.LastModifiedDate != null ? caseCom.LastModifiedDate.format(dateFormat) : '';
            LastModifiedBy = userMap.containsKey(caseCom.LastModifiedById) ? userMap.get(caseCom.LastModifiedById).Name : '';
            LastModifiedByRole = userMap.containsKey(caseCom.LastModifiedById) ? userMap.get(caseCom.LastModifiedById).UserRole.Name : '';
            LastModifiedByEmployeeNumber = userMap.containsKey(caseCom.LastModifiedById) ? userMap.get(caseCom.LastModifiedById).EmployeeNumber : '';
            CommentBody = caseCom.CommentBody;
            CreatedById = caseCom.CreatedById;
            CreatedBy = userMap.containsKey(caseCom.CreatedById) ? userMap.get(caseCom.CreatedById).Name : '';
            CreatedByRole = userMap.containsKey(caseCom.CreatedById) ? userMap.get(caseCom.CreatedById).UserRole.Name : '';
            CreatedByEmployeeNumber = userMap.containsKey(caseCom.CreatedById) ? userMap.get(caseCom.CreatedById).EmployeeNumber : '';
            CreatedDate = caseCom.CreatedDate != null ? CaseCom.CreatedDate.format(dateFormat) : '';
            Id = caseCom.Id;
        }
    }

    public ID ParentId { get; set; }
    public ID Id { get; set; }
    public String LastModifiedDate { get; set; }
    public String CommentBody { get; set; }
    public String CreatedBy { get; set; }
    public String CreatedByRole { get; set; }
    public String CreatedByEmployeeNumber { get; set; }
    public ID CreatedById { get; set; }
    public String CreatedDate { get; set; }
    public String LastModifiedBy{ get; set; }
    public String LastModifiedByRole { get; set; }
    public String LastModifiedByEmployeeNumber { get; set; }
}