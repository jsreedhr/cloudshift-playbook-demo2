@isTest
public class TaskTeleAppointerTest {
    
    private static testMethod void method1() {
        
        UserRole uRole = new UserRole(DeveloperName = 'Teleappointer', Name = 'Teleappointer');
        insert uRole;
        
        User u = new User(
        ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id,
        LastName = 'last',
        Email = 'test@testTask.com',
        Username = 'test@testTask.com' + System.currentTimeMillis(),
        CompanyName = 'TEST',
        Title = 'title',
        Alias = 'alias',
        TimeZoneSidKey = 'America/Los_Angeles',
        EmailEncodingKey = 'UTF-8',
        LanguageLocaleKey = 'en_US',
        LocaleSidKey = 'en_US',
        UserRoleId = uRole.Id
        );
        
        insert u;
        
        
        System.runAs(u) {
            Lead l = new Lead();
            l.lastName = 'Test';
            l.Company = 'Test Company';
            insert l;
            
            Task t = new Task();
            T.whoId = l.id;
            t.Subject = 'Test';
            
            insert t;
        }
        
    }
}