Global class bg_AccountButtonHelper {

	webservice static Integer returnPHSAccountCount(Id accountId)
    {
        return [SELECT Id FROM PHS_Account__c WHERE Salesforce_Account__c =: accountId].size();
    }
    
}