/*
*  Class Name:  ContractLinesForLeadControllerTest
*  Description: Test class for ContractLinesForLeadController
*  Company: dQuotient
*  CreatedDate: 10-Nov-2017
*
*  Modification Log
*  ----------------------------------------------------------------------------------
*  Developer      		Modification Date      Comments
*  ----------------------------------------------------------------------------------
*  Mohammed Rizwan      10-Nov-2017            Orginal Version
*/
@isTest
private class ContractLinesForLeadControllerTest {
	//sObject2 denotes odata in correct format, SObject1 denotes incomplete data that will cover else/catch blocks
	static Account account;
	static Contact contact;
    static List<PHS_Account__c> phsAccountList;
    static Lead lead1;
	static Lead lead2;
    static PriceBook2 priceBook;

    @isTest
    private static void createData(){
        phsAccountList = new List<PHS_Account__c>();
		// create custom setting Wildebeest_Integration__c
		Wildebeest_Integration__c wildBeest = bg_Test_Data_Utils.createCSWildebeestIntegration();
		insert wildBeest;
		//Insert Account
		account = bg_Test_Data_Utils.createAccount('Test Account');
		account.Wildebeest_Ref__c = '4813792';
		insert account;
		//Insert Contact
		contact = bg_Test_Data_Utils.createContact('Test Contact' , account.Id);
		insert contact;
		//Insert PHS_Account__c
		for(Integer i=0;i<3;i++){
			PHS_Account__c phsAccount = bg_Test_Data_Utils.createPHSAccount('1234', account.Id);
			phsAccountList.add(phsAccount);
		}
		//following three Wildebeest_Ref__c values yields results, don't change
		phsAccountList[0].Wildebeest_Ref__c = '4813792';
		phsAccountList[1].Wildebeest_Ref__c = '3073109';
		phsAccountList[2].Wildebeest_Ref__c = '4505501';
		insert phsAccountList;
        //updating standardPriceBook
        PriceBook2 priceBook = new PriceBook2(
        	Id = Test.getStandardPricebookId(),
            Name = 'Standard Price Book',
            Description = 'Some Text',
            isActive = TRUE,
            Price_Book_Type__c='Standard'
        );
        update priceBook;
		//Insert Lead
		lead1 = bg_Test_Data_Utils.createLead('Test Lead 1');
		lead1.Wildebeest_Ref__c = '';
		lead2 = bg_Test_Data_Utils.createLead('Test Lead 1');
		lead2.Wildebeest_Ref__c = '4813792,12345';
		insert new List<Lead>{lead1,lead2};
    }
    @isTest
    private static void contractLinesTest(){
		createData();
        ContractLinesForLeadController controller1 = new ContractLinesForLeadController();
		ContractLinesForLeadController controller2 = new ContractLinesForLeadController();

		Test.startTest();
		// covers some errors
		controller1.populateContractLines();
		controller1.setLeadId(lead1.Id);
		Id tempId = controller1.getLeadId();
		System.assertEquals(tempId,controller1.leadId);
		controller1.populateContractLines();
		System.assertEquals('stop'+tempId, controller1.str+controller1.leadId);

		//gives positive results
		controller2.setLeadId(lead2.Id);
		tempId = controller2.getLeadId();
		System.assertEquals(tempId,controller2.leadId);
		controller2.populateContractLines();
		System.assertEquals(tempId, controller2.leadId);
		System.assertNotEquals(NULL, controller2.contractLineList);
        //if response is saved in lead Object then showResponseFromLead() will get covered
        controller2.populateContractLines();

		//changing values to cover else/catch blocks
		phsAccountList[0].Wildebeest_Ref__c = '123456789';
		phsAccountList[1].Wildebeest_Ref__c = '12345678';
		phsAccountList[2].Wildebeest_Ref__c = '1234567';
		update phsAccountList;
		controller2.populateContractLines();
        System.assertNOTEquals(NULL, controller2.contractLineList);

		for(PHS_Account__c phsAccount : phsAccountList)
            phsAccount.Wildebeest_Ref__c = '';
		update phsAccountList;
		controller2.populateContractLines();
        System.assertEquals(new List<bg_ContractLine>(), controller2.contractLineList);

		account.Wildebeest_Ref__c = '123456789';
		update account;
		controller2.populateContractLines();
        System.assertEquals(new List<bg_ContractLine>(), controller2.contractLineList);

		account.Wildebeest_Ref__c = '';
		update account;
		controller2.populateContractLines();
        System.assertEquals(0, controller2.contractLineList.size());

		Test.stopTest();

    }
    
    @isTest
    public static void testmethod1()
    {
        bg_ContractLineCallout bgc = new bg_ContractLineCallout();
    }
}