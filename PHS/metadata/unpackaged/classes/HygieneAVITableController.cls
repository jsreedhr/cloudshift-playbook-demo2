public class HygieneAVITableController {
 public List<SBQQ__QuoteLine__c> quoteLineList{get;set;}
    List<SBQQ__QuoteLine__c> quoteLineList2 = new  List<SBQQ__QuoteLine__c>();
    protected Id quoteId;
  public HygieneAVITableController(){
        quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
        System.debug('quoteId: '+quoteId);
        
        if(quoteId!=NULL){
            quoteLineList = [SELECT Id, SBQQ__ProductName__c, PHSCPQ_Standard_AVI_Additional_Charge__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c =:quoteId and (SBQQ__Product__r.Agreement_Type__c='AVI Maintenance' or SBQQ__Product__r.Agreement_Type__c='AVI Callout' or SBQQ__Product__r.Agreement_Type__c='AVI Service' )];
            System.debug('quoteLineList: '+quoteLineList); 
           
        }
            
           
           
    }
}