/**********************************************************************
* bg_CaseCommentsWrapper_Tests:
*
* Test class for the CaseCommentsWrapper class
*
* Created By: BrightGen Ltd (AL)
* Created Date: 18/01/2016
*
* Changes: 
***********************************************************************/

@isTest
private class bg_TaskWrapper_Tests {
    static testMethod void testTaskParsed() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;

    	Account acc = bg_Test_Data_Utils.createAccount('brightGen');
    	insert acc;

    	Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
    	insert cont;

    	Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
    	insert theCase;

    	Task caseTask = bg_Test_Data_Utils.createTask('Test Task', theCase.Id);
        insert caseTask;

        test.startTest();
        caseTask = [SELECT LastModifiedDate, AccountId, WhatId, Description, WhatCount, Activities_Created_In_The_Last_Week__c, ReminderDateTime,
        					IsHighPriority, InSituDtm__InSitu_ONTracker__c, IsClosed, InSituDtm__InSitu_Send_Email_Reminder__c, WhoCount,
        					InSituDtm__InSitu_EN_Visible_Flag__c, Last_Modified_User_Details__c, CreatedById, OwnerId, InSituDtm__InSitu_Contact_Task__c,
        					IsReminderSet, Status, InSituDtm__InSitu_Owner_Not_Responsible__c, IsDeleted, WhoId, Priority, Task_isActive__c,
        					IsRecurrence, Subject, SystemModstamp, IsArchived, InSituDtm__InSitu_EN_Hidden_Flag__c, TaskSubtype, Event_isActive__c,
        					CreatedDate, Due_Date_and_Time__c, Assigned_User_Details__c, Id, InSituDtm__InSitu_TCN_Flag__c, LastModifiedById
                        FROM Task
        				WHERE Id =: caseTask.Id];

        TaskWrapper wrapper = new TaskWrapper((SObject)caseTask);
        test.stopTest();

        system.assertEquals(37, wrapper.WrapperMap.size());
    }

    static testMethod void testSerializerUsesWrapper() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;

    	Account acc = bg_Test_Data_Utils.createAccount('brightGen');
    	insert acc;

    	Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
    	insert cont;

    	Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
    	insert theCase;

    	Task caseTask = bg_Test_Data_Utils.createTask('Test Task', theCase.Id);
        insert caseTask;

        test.startTest();
        caseTask = [SELECT LastModifiedDate, AccountId, WhatId, Description, WhatCount, Activities_Created_In_The_Last_Week__c, ReminderDateTime,
        					IsHighPriority, InSituDtm__InSitu_ONTracker__c, IsClosed, InSituDtm__InSitu_Send_Email_Reminder__c, WhoCount,
        					InSituDtm__InSitu_EN_Visible_Flag__c, Last_Modified_User_Details__c, CreatedById, OwnerId, InSituDtm__InSitu_Contact_Task__c,
        					IsReminderSet, Status, InSituDtm__InSitu_Owner_Not_Responsible__c, IsDeleted, WhoId, Priority, Task_isActive__c,
        					IsRecurrence, Due_Date_and_Time__c, Subject, SystemModstamp, IsArchived, InSituDtm__InSitu_EN_Hidden_Flag__c, TaskSubtype, Event_isActive__c,
        					CreatedDate, Assigned_User_Details__c, Id, InSituDtm__InSitu_TCN_Flag__c, LastModifiedById
                        FROM Task
        				WHERE Id =: caseTask.Id];

        TaskWrapper wrapper = new TaskWrapper((SObject)caseTask);
        test.stopTest();

        system.debug('*** serialized: **** : ' + bg_Serializer.toJsonStr(caseTask, 'Task'));

        system.assertEquals(JSON.serialize(wrapper.WrapperMap), bg_Serializer.toJsonStr(caseTask, 'Task'));
    }

    static testMethod void testDatesAreConverted() {
    	Wildebeest_Integration__c integrationSettings = bg_Test_Data_Utils.setUpWildebeestDates('dd/MM/yyyy HH:mm:ss');
    	insert integrationSettings;

    	Account acc = bg_Test_Data_Utils.createAccount('brightGen');
    	insert acc;

    	Contact cont = bg_Test_Data_Utils.createContact('test', acc.Id);
    	insert cont;

    	Case theCase = bg_Test_Data_Utils.createCase('test case', cont.Id, acc.Id);
    	insert theCase;

    	Task caseTask = bg_Test_Data_Utils.createTask('Test Task', theCase.Id);
        insert caseTask;

        test.startTest();
        caseTask = [SELECT LastModifiedDate, AccountId, WhatId, Description, WhatCount, Activities_Created_In_The_Last_Week__c, ReminderDateTime,
        					IsHighPriority, InSituDtm__InSitu_ONTracker__c, IsClosed, InSituDtm__InSitu_Send_Email_Reminder__c, WhoCount,
        					InSituDtm__InSitu_EN_Visible_Flag__c, Last_Modified_User_Details__c, CreatedById, OwnerId, InSituDtm__InSitu_Contact_Task__c,
        					IsReminderSet, Status, InSituDtm__InSitu_Owner_Not_Responsible__c, IsDeleted, WhoId, Priority, Task_isActive__c,
        					IsRecurrence, Due_Date_and_Time__c, Subject, SystemModstamp, IsArchived, InSituDtm__InSitu_EN_Hidden_Flag__c, TaskSubtype, Event_isActive__c,
        					CreatedDate, Assigned_User_Details__c, Id, InSituDtm__InSitu_TCN_Flag__c, LastModifiedById
                        FROM Task
        				WHERE Id =: caseTask.Id];

        TaskWrapper wrapper = new TaskWrapper((SObject)caseTask);
        test.stopTest();

        system.debug('*** serialized: **** : ' + bg_Serializer.toJsonStr(caseTask, 'Task'));

        system.assertEquals(caseTask.LastModifiedDate.format('dd/MM/yyyy HH:mm:ss'), wrapper.WrapperMap.get('LastModifiedDate'));
        system.assertEquals('', wrapper.WrapperMap.get('ReminderDateTime'));
        system.assertEquals(caseTask.SystemModstamp.format('dd/MM/yyyy HH:mm:ss'), wrapper.WrapperMap.get('SystemModstamp'));
        system.assertEquals(caseTask.CreatedDate.format('dd/MM/yyyy HH:mm:ss'), wrapper.WrapperMap.get('CreatedDate'));
    }
}