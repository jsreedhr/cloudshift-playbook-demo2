/*Controller Extension for EventTaskReallocation
  Created on 20/11/2017
*/
public class EventTaskReallocation {

    public List < wrapperClass > wrapClassList {
        get;
        set;
    }
    public List < Task > taskList;
    public List < Event > eventList;

    public String taskEvent;
    public String status;
    public EventTaskReallocation() {
        assignedUser = new Task();
        assignToUser = new Task();
        showRes = FALSE;
        status = 'Open';
    }

    public void ReAllocate() {

        List < String > strIds = new List < String > ();
        // headerChkFlg = false;
        List < Task > updateTask = new List < Task > ();
        List < Event > updateEvent = new List < Event > ();
        Set < Id > Ids = new Set < id > ();
        Id newUserId = assignToUser.OwnerId;

        if (!wrapClassList.isEmpty()) {
            System.debug('headerChkFlgThisPage value : ----' + headerChkFlgThisPage);
            System.debug('headerFlgParam value: --------' + headerFlgParam);
            System.debug('flagtrue value __' + flagtrue);


            // Select All
            if (headerFlgParam == 'true') {
                for (wrapperClass wc: wrapClassList) {
                    Ids.add(wc.taskEventId);
                }
            }
            //Multiple selections in each page
            else if (!String.isBlank(flagtrue)) {
                strIds = flagtrue.split(',');
                for (String s: strIds) {
                    Ids.add(s);
                }

            }

            //Select This Page
            // else if (headerChkFlgThisPage) {
            else {
                for (wrapperClass wc: wrapClassList) {
                    if (wc.flag)
                        Ids.add(wc.taskEventId);
                }
            }
        }


        // for (wrapperClass wc: wrapClassList) {

        // System.debug('selct hidden FLAGS: -- ' +wc.taskEventId);

        /* if(wc.flag)
               {
                 Ids.add(wc.taskEventId); 
                 System.debug('selct wrpper flgs: -- ' + Ids);
               } 
               
               if(wc.flagtrue!='false'){
                Ids.add(wc.taskEventId);
               // System.debug('selct hidden FLAGS: -- ' +);
            } */

        // }

        System.debug('IDSsss Selected --:' + Ids);
        //}


        for (Task task: [Select id from TASK where id in: Ids]) {
            task.OwnerId = newUserId;
            updateTask.add(task);
        }
        for (Event event: [Select id from Event where id in: Ids]) {
            event.OwnerId = newUserId;
            updateEvent.add(event);
        }

        try {
            update updateTask;
            update updateEvent;
        } catch (Exception e) {
            System.debug('Exception updating ' + e);
        }

        Results();
        headerFlgParam = null;
        flagtrue = null;

    }

    public void Results() {
        flagtrue = null;
        headerChkFlg = false;
        headerFlgParam = null;
        headerChkFlgThisPage = false;
        Id userId = assignedUser.OwnerId;
        Date fromDate = assignedUser.ActivityDate;
        Date toDate = assignToUser.ActivityDate;
        // System.debug(fromDate + '---and----' + toDate);
        wrapClassList = new List < wrapperClass > ();
        taskList = new List < Task > ();
        eventList = new List < Event > ();
        String q;
        String q2;


        if (assignedUser.ActivityDate != NULL && assignToUser.ActivityDate != NULL) {
            if (status == 'Open') {
                q = ' where IsClosed=FALSE and OwnerId  = : userId and (ActivityDate >=: fromDate and ActivityDate <=: toDate ) ';
                q2 = ' where  ActivityDate >= TODAY and OwnerId  = : userId and Ischild = false and ( DAY_ONLY(StartDateTime)>=:fromDate and DAY_ONLY(EndDateTime)<=:toDate) ';
            } else if (status == 'Closed') {
                q = ' where IsClosed=TRUE and OwnerId  = : userId and (ActivityDate >=: fromDate and ActivityDate <=: toDate ) ';
                q2 = ' where   ActivityDate < TODAY and OwnerId  = : userId and Ischild = false and ( DAY_ONLY(StartDateTime)>=:fromDate and DAY_ONLY(EndDateTime)<=:toDate) ';
            } else {
                q = ' where  OwnerId  = : userId and (ActivityDate >=: fromDate and ActivityDate <=: toDate ) ';
                q2 = ' where OwnerId  = : userId and Ischild = false  and ( DAY_ONLY(StartDateTime)>=:fromDate and DAY_ONLY(EndDateTime)<=:toDate) ';

            }

        } else if (assignedUser.ActivityDate != NULL && assignToUser.ActivityDate == NULL) {

            if (status == 'Open') {
                q = ' where IsClosed=FALSE and OwnerId  = : userId and ActivityDate >=: fromDate ';
                q2 = ' where  ActivityDate >= TODAY and OwnerId  = : userId and  DAY_ONLY(StartDateTime)>=:fromDate and Ischild = false  ';
            } else if (status == 'Closed') {
                q = ' where IsClosed=TRUE and OwnerId  = : userId and ActivityDate >=: fromDate ';
                q2 = ' where   ActivityDate < TODAY and OwnerId  = : userId and  DAY_ONLY(StartDateTime)>=:fromDate and Ischild = false  ';
            } else {
                q = ' where  OwnerId  = : userId and ActivityDate >=: fromDate ';
                q2 = ' where OwnerId  = : userId and  DAY_ONLY(StartDateTime)>=:fromDate  and Ischild = false ';

            }

        } else if (assignedUser.ActivityDate == NULL && assignToUser.ActivityDate != NULL) {

            if (status == 'Open') {
                q = ' where IsClosed=FALSE and OwnerId  = : userId  and ActivityDate <=: toDate ';
                q2 = ' where  ActivityDate >= TODAY and OwnerId  = : userId and  DAY_ONLY(EndDateTime)<=:toDate and Ischild = false ';
            } else if (status == 'Closed') {
                q = ' where IsClosed=TRUE and OwnerId  = : userId  and ActivityDate <=: toDate ';
                q2 = ' where   ActivityDate < TODAY and OwnerId  = : userId and DAY_ONLY(EndDateTime)<=:toDate and Ischild = false  ';
            } else {
                q = ' where  OwnerId  = : userId  and ActivityDate <=: toDate ';
                q2 = ' where OwnerId  = : userId and DAY_ONLY(EndDateTime)<=:toDate and Ischild = false ';

            }
        } else if (assignedUser.ActivityDate == NULL && assignToUser.ActivityDate == NULL) {
            if (status == 'Open') {
                q = ' where IsClosed=FALSE and OwnerId  = : userId  ';
                q2 = ' where  ActivityDate >= TODAY and OwnerId  = : userId and Ischild = false   ';
            } else if (status == 'Closed') {
                q = ' where IsClosed=TRUE and OwnerId  = : userId ';
                q2 = ' where   ActivityDate < TODAY and OwnerId  = : userId and Ischild = false  ';
            } else {
                q = ' where  OwnerId  = : userId  ';
                q2 = ' where OwnerId  = : userId and Ischild = false ';

            }

        }

        if (taskEvent == 'Task & Events') {

            taskList = Database.query('Select Id ,Due_Date_and_Time__c,what.type,status ,Subject,ActivityDate,Description,IsClosed,Postal_District__c,WhatId from Task ' + q);
            eventList = Database.query('Select Id ,what.type,Subject,ActivityDateTime, Description,StartDateTime,EndDateTime,Postal_District__c from Event ' + q2);
        } else if (taskEvent == 'Tasks Only') {
            taskList = Database.query('Select Id ,Due_Date_and_Time__c,what.type,status ,Subject,ActivityDate,Description,IsClosed,Postal_District__c ,WhatId from Task ' + q);
        } else if (taskEvent == 'Events Only') {
            eventList = Database.query('Select Id ,what.type,Subject,ActivityDateTime, Description,StartDateTime,EndDateTime,Postal_District__c from Event ' + q2);
        }

        System.debug(taskList);
        Opportunity opty = new Opportunity();
        showRes = TRUE;
        For(Task t: taskList) {
            wc = new wrapperClass();
            wc.taskEventId = t.id;
            wc.flag = false;
            System.debug(eventList);



            if (!taskList.isEmpty()) {
                //   wc.flagtrue = 'false';
                wc.taskOrEvent = 'Task';
                wc.taskEventStatus = t.status;
                wc.relatedTo = t.what.type;
                wc.subject = t.Subject;
                wc.dueDate = t.Due_Date_and_Time__c;
                wc.description = t.Description;
                if (t.Description != null && t.Description.length() > 200)
                    wc.description = t.Description.substring(0, 200);
                // wc.postalDistrict = t.Postal_District__c;
                //Adding opty amount fld instead of postalDistrict
                if (t.what.type == 'Opportunity' && t.WhatId != null)
                {
                    opty = [select id, ExpectedRevenue from Opportunity where id =: t.WhatId];
                if (opty.ExpectedRevenue != null)
                    wc.optyAmt = opty;//.ExpectedRevenue;
                }
                
                wrapClassList.add(wc);
            }
        }

        if (!eventList.isEmpty()) {
            showRes = TRUE;
            For(Event t: eventList) {
                wc = new wrapperClass();
                wc.flag = false;
                wc.taskEventId = t.id;
                wc.taskOrEvent = 'Event';

                if (t.ActivityDateTime >= System.today())
                    wc.taskEventStatus = 'Open';
                else if (t.ActivityDateTime < System.today())
                    wc.taskEventStatus = 'Closed';

                wc.relatedTo = t.what.type;
                wc.subject = t.Subject;
                wc.dueDate = t.ActivityDateTime;
                wc.description = t.Description;

                if (t.Description != null && t.Description.length() > 200)
                    wc.description = t.Description.substring(0, 200);
                //  wc.postalDistrict = t.Postal_District__c;
                 //Adding opty amount fld instead of postalDistrict
                if (t.what.type == 'Opportunity' && t.WhatId != null)
                {
                    opty = [select id, ExpectedRevenue from Opportunity where id =: t.WhatId];
                if (opty.ExpectedRevenue != null)
                    wc.optyAmt = opty;//.ExpectedRevenue;
                }
              
                wc.startDate = date.newinstance(t.StartDateTime.year(), t.StartDateTime.month(), t.StartDateTime.day());
                wc.EndDate = date.newinstance(t.EndDateTime.year(), t.EndDateTime.month(), t.EndDateTime.day());
                wrapClassList.add(wc);
            }
        }


    }

    public PageReference clearValue() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.getParameters().clear();
        newpage.setRedirect(true);
        return newpage;
    }

    //Action for select all checkboxes
    public void ChkAll() {

        System.debug('-Param value--' + headerFlgParam);
        System.debug('---' + headerChkFlg);
        if (!wrapClassList.isEmpty()) {
            for (wrapperClass wc: wrapClassList) {
                if (headerFlgParam == 'true') {
                    wc.flag = TRUE;
                    headerChkFlgThisPage = false;
                } else {
                    wc.flag = FALSE;
                    headerChkFlg = FALSE;
                    headerChkFlgThisPage = FALSE;

                }
            }
        }

    }



    public wrapperClass wc {
        get;
        set;
    }
    public Task assignedUser {
        get;
        set;
    }
    public Task assignToUser {
        get;
        set;
    }
    public Boolean showRes {
        get;
        set;
    }

    //flag for complete page selection 
    public Boolean headerChkFlg {
        get;
        set;
    }


    //flag for current page selection
    public Boolean headerChkFlgThisPage {
        get;
        set;
    }

    //To store multiple selections made in each page on taskEvent table
    public string flagtrue {
        get;
        set;
    }

    public String headerFlgParam {
        get;
        set;
    }

    public List < SelectOption > getItems() {
        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption('Tasks Only', 'Tasks Only'));
        options.add(new SelectOption('Events Only', 'Events Only'));
        options.add(new SelectOption('Task & Events', 'Task & Events'));
        return options;
    }

    public String getTaskEvent() {
        return taskEvent;
    }

    public void setTaskEvent(String taskEvent) {
        this.taskEvent = taskEvent;
    }

    public List < SelectOption > getStatusItems() {
        List < SelectOption > options = new List < SelectOption > ();
        // options.add(new SelectOption('', 'Select'));
        options.add(new SelectOption('Open', 'Open'));
        options.add(new SelectOption('Closed', 'Closed'));
        options.add(new SelectOption('Open & Closed', 'Open & Closed'));
        return options;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class wrapperClass {
        public boolean flag {
            get;
            set;
        }
        public string taskOrEvent {
            get;
            set;
        }
        public string taskEventStatus {
            get;
            set;
        }
        public string relatedTo {
            get;
            set;
        }
        public string subject {
            get;
            set;
        }
        public Datetime dueDate {
            get;
            set;
        }
        public ID taskEventId {
            get;
            set;
        }
        public string description {
            get;
            set;
        }
        public Date startDate {
            get;
            set;
        }
        public Date EndDate {
            get;
            set;
        }
        public Opportunity optyAmt {
            get;
            set;
        }


    }

    /* public void SelectAll() {

         for (wrapperClass wc: wrapClassList) {
             wc.flag = TRUE;
         }

     }*/


}