@isTest
public class Test_ApproveLicense {
    
    private static testMethod void Test_ApproveLicense(){
       
       Test.startTest();
       License_Request__c l = new License_Request__c();
       l.First_Name__c = 'Phil';
       l.Last_Name__c = 'Butson';
       l.Email__c = 'philbutson' + 1 + '@gmail.com';
       l.Timezone__c = 'Europe/London';
       l.Assistant_Email__c = 'barry@gmail.com';
       l.CurrencyIsoCode = 'GBP';
       l.Profile__c = 'System Administrator';
	   insert l;
        
       PageReference Pageref = new PageReference('/' + l.Id);
       Pageref.getParameters().put('id',l.id);

       test.setCurrentPage(Pageref);   
    
       ApexPages.StandardController sc = new ApexPages.standardController(l);

       ApproveLicense rec  = new ApproveLicense(sc);
       rec.loadMessages();
       Test.stopTest();
    }
}