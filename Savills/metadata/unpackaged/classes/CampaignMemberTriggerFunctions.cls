public without sharing class CampaignMemberTriggerFunctions {
    
    //Method to ensure lead campaign members are not set as the exclusive buyer.
    public static void cannotSetLeadExclusiveBuyer(List <CampaignMember> lst_cms){
        
        for(CampaignMember c: lst_cms){
            IF(c.LeadId != null){
                IF(c.status == 'Exclusive Bidder'){
                    c.addError('Leads may not be marked as exclusive bidders, they need to have been converted to contacts.');
                    system.debug('c1--->'+c);
                } 
            } 
        }
    }
    
    //Method to ensure there is only one campaign member set as the exclusive buyer
   // public static void checkoneexclusivebidderinsert(List <CampaignMember> lst_cms){
        
        //Set <ID> set_campaignids = new Set <ID>();
        
       // for(CampaignMember c: lst_cms){
           // IF(c.CampaignId != null){
              //  set_campaignids.add(c.CampaignId);
          //  }
       // }
        
       // Map <ID, CampaignMember> lst_campmembers  =  new Map <Id, CampaignMember>([SELECT ID, Status, CampaignId, ContactId, LeadID FROM CampaignMember WHERE Status = 'Exclusive Bidder' AND ContactId != Null AND CampaignId in: set_campaignids]);
        
       // IF(!lst_campmembers.isEmpty()){
            
           // for(CampaignMember c: lst_cms){
                //IF(c.status == 'Exclusive Bidder'){
                      //  IF(lst_campmembers.containskey(c.id)){
                  
                      //  c.addError('Only one exclusive bidder may be selected for a given campaign.');
                       //   system.debug('c2--->'+c);  
                   // }
              //  }
            //}
        //}
    //}
    
    
     public static void checkoneexclusivebidderinsert(list<CampaignMember> lst_cms)
  {

       
        Set <ID> set_campaignids = new Set <ID>();
        set <ID> lst_cmsids = new set <ID>();
       
        for(CampaignMember c: lst_cms)
        {
            lst_cmsids.add(c.id);
            IF(c.CampaignId != null){
                set_campaignids.add(c.CampaignId);
            }
        }
       

          Map<id,List<CampaignMember>> mapjhd = new    Map<id,List<CampaignMember>>();
          

         for(CampaignMember fjb: [SELECT ID, Status, CampaignId, ContactId, LeadID FROM CampaignMember WHERE Status = 'Exclusive Bidder' AND ContactId != Null AND CampaignId in: set_campaignids ] )
         {

              if (mapjhd.containsKey(fjb.CampaignId))
            {
                mapjhd.get(fjb.CampaignId).add(fjb);
            }
           
            else
            {
                mapjhd.put(fjb.CampaignId, new  List <CampaignMember> { fjb });
            }   
         }

           for(CampaignMember fjb:lst_cms)
           {
               IF(fjb.status == 'Exclusive Bidder')
               {
                 if (mapjhd.containsKey(fjb.CampaignId))
            {
                mapjhd.get(fjb.CampaignId).add(fjb);
            }
           
            else
            {
                mapjhd.put(fjb.CampaignId, new  List <CampaignMember> { fjb });
            }  
               }
              
           }
         for(CampaignMember c: lst_cms)
         {
             iF(c.status == 'Exclusive Bidder')
             {
                IF(mapjhd.containskey(c.CampaignId) && mapjhd.get(c.CampaignId).size()>1)
                {

                  c.addError('Only one exclusive bidder may be selected for a given campaign.');
                }
             }
         }
  

 }  
    
    
    
    
    //Method to ensure there is only one campaign member set as the exclusive buyer
     // public static void checkoneexclusivebidderupdate(List <CampaignMember> lst_cms){
        
       // Set <ID> set_campaignids = new Set <ID>();
        //set <ID> lst_cmsids = new set <ID>();
        
       // for(CampaignMember c: lst_cms)
        //{
          //  lst_cmsids.add(c.id);
          //  IF(c.CampaignId != null){
               // set_campaignids.add(c.CampaignId);
           // }
       // }
        
        //Map <ID, CampaignMember> lst_campmembers = new Map <Id, CampaignMember>([SELECT ID, Status, CampaignId, ContactId, LeadID FROM CampaignMember WHERE Status = 'Exclusive Bidder' AND ContactId != Null AND CampaignId in: set_campaignids AND id NOT in : lst_cmsids]);
        
        //IF(!lst_campmembers.isEmpty()){
            
          //  for(CampaignMember c: lst_cms){
             //   for(CampaignMember q: lst_campmembers){
                    //IF(c.status == 'Exclusive Bidder'){
                      //  IF(lst_campmembers.containskey(c.id)){
                          //  c.addError('Only one exclusive bidder may be selected for a given campaign.');
                       // }
                   // }
              //  }
           // }
       // }
    //}
   
 public static void checkoneexclusivebidderupdate(map<id,CampaignMember> mapOldcam, map<id,CampaignMember> mapNewcam){

    List <CampaignMember> lst_cms = new List <CampaignMember> ();
    for(id m : mapNewcam.keyset())
    {
        if(mapNewcam.get(m).status !=  mapOldcam.get(m).status)
        {
            lst_cms.add(mapNewcam.get(m));
        }
    }
       
        Set <ID> set_campaignids = new Set <ID>();
        set <ID> lst_cmsids = new set <ID>();
       
        for(CampaignMember c: lst_cms)
        {
            lst_cmsids.add(c.id);
            IF(c.CampaignId != null){
                set_campaignids.add(c.CampaignId);
            }
        }
       



          Map<id,List<CampaignMember>> mapjhd = new    Map<id,List<CampaignMember>>();

         for(CampaignMember fjb: [SELECT ID, Status, CampaignId, ContactId, LeadID FROM CampaignMember WHERE Status = 'Exclusive Bidder' AND ContactId != Null AND CampaignId in: set_campaignids AND id NOT in : lst_cmsids] )
         {

              if (mapjhd.containsKey(fjb.CampaignId))
            {
                mapjhd.get(fjb.CampaignId).add(fjb);
            }
           
            else
            {
                mapjhd.put(fjb.CampaignId, new  List <CampaignMember> { fjb });
            }   
         }
          for(CampaignMember fjb:lst_cms)
           {
               IF(fjb.status == 'Exclusive Bidder')
               {
                 if (mapjhd.containsKey(fjb.CampaignId))
            {
                mapjhd.get(fjb.CampaignId).add(fjb);
            }
           
            else
            {
                mapjhd.put(fjb.CampaignId, new  List <CampaignMember> { fjb });
            }  
               }
              
           }

           
         for(CampaignMember c: lst_cms)
         {
             IF(c.status == 'Exclusive Bidder')
             {
                IF(mapjhd.containskey(c.CampaignId) &&  mapjhd.get(c.CampaignId).size()>1)
                {
                     mapNewcam.get(c.id).addError('Only one exclusive bidder may be selected for a given campaign.');
                }
             }
         }
  

 }
    
    public static void updateoppwithexlcusivebidder(List <CampaignMember> lst_cms)
    {
       
       Set<Id> campaignidset = new Set<Id>();
        Set<Id> campaignimembdset = new Set<Id>();
       for(CampaignMember c :lst_cms )
       {
           campaignidset.add(c.campaignid);
           campaignimembdset.add(c.Id);
       }
        system.debug('trigger list---'+campaignimembdset);
        List <Opportunity> lst_opp = new List <Opportunity>([SELECT ID, CampaignId FROM Opportunity where Campaignid in :campaignidset ]);
        
        List<CampaignMember> lst_cmmemb= new List<CampaignMember>([SELECT ID, Status, CampaignId, ContactId, LeadID FROM CampaignMember WHERE Status = 'Exclusive Bidder' AND ContactId != Null AND CampaignId in: campaignidset]);
        system.debug('lst of members--->'+lst_cmmemb);
        Map<Id,List<CampaignMember>> mapCamaign = new  Map<Id,List<CampaignMember>>();
        for(CampaignMember cm : lst_cmmemb)
        {
            if(mapCamaign.containskey(cm.CampaignId)){
            	mapCamaign.get(cm.CampaignId).add(cm);
            }
            else
            {
                List<CampaignMember>  cmtemp = new List<CampaignMember>();
                cmtemp.add(cm);
            	mapCamaign.put(cm.CampaignId, cmtemp);
            }
        }
        
        system.debug('campaign list----'+mapCamaign);
        for(Opportunity opp : lst_opp)
        {
            if(mapCamaign.containskey(opp.CampaignId)){
                system.debug('lst_opp---->'+lst_opp);
            opp.purchaser_contact__c= mapCamaign.get(opp.CampaignId)[0].ContactId;
            }
            else{
             opp.purchaser_contact__c= null;
            }
        }
        
       update lst_opp;
        
    }
}