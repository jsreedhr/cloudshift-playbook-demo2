public without sharing class PortfolioLinksTriggerFunctions {
    
    //Method used to prevent the update of property/portfolio links
    public static void preventpropertychange(List <Portfolio_Link__c> lst_oldportlinks, List <Portfolio_Link__c> lst_newportlinks){
        
        for(Portfolio_Link__c o: lst_oldportlinks){
            for(Portfolio_Link__c n: lst_newportlinks){
                IF(o.id == n.id){
                    IF(o.property__c != n.property__c){
                        n.addError('You cannot change a portfolio property from one to the other. The old one must be deleted and a new one created.');
                    }
                }
            }
        }
    }     
}