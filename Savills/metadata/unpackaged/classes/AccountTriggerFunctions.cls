public without sharing class AccountTriggerFunctions {

    public static void DeleteSavillsAccountError(List <Account> lst_accts){
        
        for(Account a: lst_accts){
            IF(a.id == System.Label.Savills_Account){
                a.adderror('You may not delete the Savills account. It is needed for automated transaction functionality.');
            }
        }
    }    
}