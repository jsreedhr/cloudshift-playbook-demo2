public without sharing class CampaignCustomCloneController{
    
    private ApexPages.StandardController standardController;
    
    public Id campId {get;set;}
    
    public CampaignCustomCloneController(ApexPages.StandardController standardController){
        
        this.standardController = standardController;
        // Fields being cloned
     if(!test.isRunningTest()){   this.standardController.addFields(new List<String> { 'Name', 'Status', 'Type', 'isActive', 'CurrencyIsoCode', 'Description', 'OwnerId'});}
        campId = standardController.getRecord().Id;

    }
    
    public PageReference cloneCampaign(){
        // Record being cloned
        Campaign currentcampaign = (Campaign) standardController.getRecord();
     
        List <CampaignMember> lst_cms = new List <CampaignMember>([SELECT ContactId, LeadId, CampaignId FROM CampaignMember WHERE CampaignId =: campId]);
        
        // Custom clone logic
        Campaign clonedCampaign = new Campaign();
        clonedCampaign.Name = currentcampaign.Name + ' - Clone';
        clonedCampaign.Status = currentcampaign.Status;
        clonedCampaign.Type = currentcampaign.Type;
        clonedCampaign.isActive = currentcampaign.isActive;
        clonedCampaign.CurrencyIsoCode = currentcampaign.CurrencyIsoCode;
        clonedCampaign.Description = currentcampaign.Description;
       // clonedCampaign.Ownerid = currentcampaign.Ownerid;
        insert clonedCampaign;
        
        IF(!lst_cms.isEmpty()){
           List <CampaignMember> clonedCampaignmembers = new List <CampaignMember>();
           for(CampaignMember cmexisting: lst_cms){
                CampaignMember cmnew = new CampaignMember();
                cmnew.ContactId = cmexisting.ContactId;
                cmnew.LeadId = cmexisting.LeadId;
                cmnew.CampaignId = clonedCampaign.Id;
                cmnew.Status = 'To be Contacted';
                clonedCampaignmembers.add(cmnew);
            }
            insert clonedCampaignmembers;
        }
        
        // Redirect to the new cloned record
        return new PageReference('/'+clonedCampaign.Id);
    }
}