public without sharing class LicenseRequestTriggerFunctions{
    
    public static void UserCount(List <License_Request__c> lst_LRs){
        
        List <User> lst_users = new List <User>([SELECT ID FROM User WHERE isActive = TRUE]);
        List <License_Request__c> lst_LRstoUpdate = new List <License_Request__c>();
        
        for(License_Request__c l: lst_LRs){
            l.Number_of_Active_Users__c = lst_users.size();
            lst_LRstoUpdate.add(l);
        }
    }
    
    @future
    public static void createUserfromLicenseRequest(Set <ID> set_LicenseReqs){
        List <User> lst_newusers = new List <User>();
        List <License_Request__c> lst_lrs = new List <License_Request__c>([SELECT ID, Approved__c, First_Name__c, Last_Name__c, Email__c,Phone__c, Mobile__c, Timezone__c, CurrencyIsoCode, Assistant_Email__c, Role__c, Profile__c FROM License_Request__c WHERE ID in: set_LicenseReqs]);
        System.debug('Number of Users Queried = ' + lst_lrs.size());
        
        List <Profile> lst_profiles = new List <Profile>([SELECT ID, Name FROM Profile]);
        System.debug(lst_profiles.size());
        Map <String, ID> map_pNameIds = new Map <String, ID>();
        IF(!lst_profiles.isEmpty()){
            for(Profile p: lst_profiles){
                map_pNameIds.put(p.Name, p.Id);
                System.debug('Number of Matching Profiles =' + lst_profiles.size());
            }
        }
        
        List <UserRole> lst_roles = new List <UserRole>([SELECT ID, Name FROM UserRole]);
        Map <String, ID> map_rNameIds = new Map <String, ID>();
        System.debug(lst_roles.size());
        IF(!lst_roles.isEmpty()){
            for(UserRole r: lst_roles){
                map_rNameIds.put(r.Name, r.Id);
                System.debug('Number of Matching Roles =' + lst_roles.size());
            }
        }
        
        System.debug('Users to loop through' + lst_lrs.size());
        IF(!lst_lrs.isEmpty()){
            System.debug('Going through loop');
            for(License_Request__c lr: lst_lrs){
                System.debug('Entering for loop');
                IF(lr.Approved__c == TRUE){
                    User u = new User();
                    System.debug('Create user?');
                    u.FirstName = lr.First_Name__c;
                    u.LastName = lr.Last_Name__c;
                    u.Email = lr.Email__c;
                    u.Username = lr.Email__c +'.entdev';
                    u.Phone = lr.Phone__c;
                    u.MobilePhone = lr.Mobile__c;
                    u.TimeZoneSidKey = lr.Timezone__c;
                    u.CurrencyIsoCode = lr.CurrencyIsoCode;
                    u.DefaultCurrencyIsoCode = lr.CurrencyIsoCode;
                    u.Assistant_Email__c = lr.Assistant_Email__c;
                    u.UserPermissionsMarketingUser = TRUE;
                    u.Alias = lr.First_Name__c.substring(0,3) + lr.Last_Name__c.substring(0,1);
                    u.EmailEncodingKey = 'UTF-8';
                    u.TimeZoneSidKey = lr.Timezone__c;
                    u.CommunityNickname = lr.First_Name__c + lr.Last_Name__c.substring(0,3);
                    u.LocaleSidKey = 'en_GB';
                    u.LanguageLocaleKey = 'en_US';
                    u.IsActive = TRUE;
                    u.ProfileId = map_pNameIds.get(lr.Profile__c);
                    u.UserRoleId = map_rNameIds.get(lr.Role__c);
                    System.debug('User Loop');
                    lst_newusers.add(u);
                }
            }
            
            System.debug('Come out of User Loop');
            
            try {
                
                insert lst_newusers;
                
            } catch(DmlException e) {
                
                System.debug('There was an error: ' + e.getMessage());
                
            }          
            
            System.debug('Users to create' + lst_newusers.size());
            
            IF(!lst_newusers.isEmpty()){
                for(User u: lst_newusers){
                    System.resetPassword(u.Id, true);
                }
            }
        }
    }
}