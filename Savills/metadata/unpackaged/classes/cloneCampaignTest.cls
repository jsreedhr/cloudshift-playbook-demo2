@isTest
private class cloneCampaignTest {

    private static testMethod void test() {

Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact cont          = new Contact();
        cont.LastName          = 'Test Contact';
        cont.FirstName         = 'jhon';
        cont.Accountid         = a.id ;
        cont.Title             = 'For testing';
         
        insert cont;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.City__c  = 'My city';
        pr.Country_Picklist__c  ='United Kingdom';
       pr.Location_Type__c = 'Rural';
       pr.Description__c = 'testing Agency listing browser property';
       pr.GIA_Sq_Ft__c = 42;
        pr.Sub_Region__c = 'TBC';
        pr.Region__c = 'UK';
       pr.Spa__c = TRUE;
       pr.Conference__c = TRUE;
       pr.Ski__c = TRUE;
      pr.Development__c = TRUE;
       pr.Golf__c = TRUE;
      pr.Parking__c = TRUE;
        pr.Grade_Star_Rating__c = 'LUXURY';
        pr.Room_Count__c = 4;
       // pr.Brand_Lookup__c = a.id;
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        port.Portfolio_Description__c = 'testing Agency listing browser portfolio';
        
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = cont.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.Operating_Structure__c = 'Ground Rent';
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
      
        Task t           = new Task();
        t.OwnerId        = UserInfo.getUserId();
        t.Subject        ='Donni';
        t.Status         ='Not Started';
        t.Priority       ='Normal';
        t.Description    ='test to increase the coverage';
        t.Type           = 'Call';
        t.WhatId         = a.id;
        t.WhoId          = cont.id;
        insert t;
        
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.Description   ='testing is on process';
        eve.WhatId        = a.id;
        eve.WhoId         = cont.id;
        
        insert eve;

      
Test.startTest(); 
     Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        insert cap;
        
        CampaignMember capm              = new CampaignMember();
        capm.Status                      = 'Contacted';
        capm.CampaignId                  =  cap.id;
        capm.Teaser_E_Blast_Sent_Date__c = System.today();
        capm.Data_Room_Access_Date__c    = System.today();
        capm.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);
        capm.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);
        capm.ContactId                   = cont.id;        
        capm.LeadId                      = l.id;
       
        insert capm;
        test.stopTest();

pagereference pageref=page.CloneWithRelatedCampaignMembers;
pageref.getparameters().put('id',cap.id);
test.setcurrentpage(pageref);

ApexPages.StandardController aps=new ApexPages.StandardController(cap);

CampaignCustomCloneController campai=new CampaignCustomCloneController(aps);
campai.cloneCampaign();

    }

}