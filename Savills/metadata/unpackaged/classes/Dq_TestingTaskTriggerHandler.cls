@isTest
private class Dq_TestingTaskTriggerHandler {
    
    static testMethod void validateOpportunity(){
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
        
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      
        Task tsk1           = new Task();
        tsk1.OwnerId        = UserInfo.getUserId();
        tsk1.Subject        ='Donni';
        tsk1.Status         ='Not Started';
        tsk1.Priority       ='Normal';
        tsk1.Description    ='test to increase the coverage';
        tsk1.Type           = 'Call';
        tsk1.WhatId         = cap.id;
        tsk1.WhoId          = c1.id;
        tsk1.ActivityDate   = System.today();
        //insert tsk1;
        
        Task tsk          = new Task();
        tsk.OwnerId        = UserInfo.getUserId();
        tsk.Subject        ='Donni';
        tsk.Status         ='Not Started';
        tsk.Priority       ='Normal';
        tsk.Description    ='test to increase the coverage';
        tsk.Type           = 'Call';
        tsk.WhatId         = cap.id;
        tsk.WhoId          = c2.id;
        tsk.ActivityDate   = System.today();
        //insert tsk;
        list<task> tsk2  = new List<task>();
        tsk2.add(tsk1);
        tsk2.add(tsk);
        insert tsk2;
         Update tsk2;
        delete tsk2;
        
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.ActivityDate   = System.today();
        eve.Description   ='testing is on process';
        eve.WhatId        = cap.id;
        eve.WhoId         = c1.id;
        
         Event eve1         = new Event();
        eve1.OwnerId       = UserInfo.getUserId();
        eve1.Subject       = 'Test';
        eve1.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.ActivityDate   = System.today();
        eve1.Description   ='testing is on process';
        eve1.WhatId        = cap.id;
        eve1.WhoId         = c1.id;
        
        list<Event> eve2  = new List<Event>();
        eve2.add(eve1);
        eve2.add(eve);
        
        Insert eve2;
        
        //insert eve;

      
       
        
        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
        
         CampaignMember capm2              = new CampaignMember();
        capm2.Status                      = 'Contacted';
        capm2.CampaignId                  =  cap.id;
        capm2.Teaser_E_Blast_Sent_Date__c = System.today();
        capm2.Data_Room_Access_Date__c    = System.today();
        capm2.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm2.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm2.ContactId                   = c2.id;        
        capm2.LeadId                      = l.id;
       
        insert capm2;
        
       
        
    }
     static testMethod void validateOpportunity2(){
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
        
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      
        Task tsk1           = new Task();
        tsk1.OwnerId        = UserInfo.getUserId();
        tsk1.Subject        ='Donni';
        tsk1.Status         ='Not Started';
        tsk1.Priority       ='Normal';
        tsk1.Description    ='test to increase the coverage';
        tsk1.Type           = 'Call';
        tsk1.WhatId         = cap.id;
        tsk1.WhoId          = c1.id;
        tsk1.ActivityDate   = System.today();
        //insert tsk1;
        
        Task tsk          = new Task();
        tsk.OwnerId        = UserInfo.getUserId();
        tsk.Subject        ='Donni';
        tsk.Status         ='Not Started';
        tsk.Priority       ='Normal';
        tsk.Description    ='test to increase the coverage';
        tsk.Type           = 'Call';
        tsk.WhatId         = cap.id;
        tsk.WhoId          = c1.id;
        tsk.ActivityDate   = System.today();
        //insert tsk;
        list<task> tsk2  = new List<task>();
        tsk2.add(tsk1);
        tsk2.add(tsk);
        insert tsk2;
         Update tsk2;
        delete tsk2;
         
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.ActivityDate   = System.today();
        eve.Description   ='testing is on process';
        eve.WhatId        = cap.id;
        eve.WhoId         = c1.id;
        
         Event eve1         = new Event();
        eve1.OwnerId       = UserInfo.getUserId();
        eve1.Subject       = 'Test';
        eve1.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.ActivityDate   = System.today();
        eve1.Description   ='testing is on process';
        eve1.WhatId        = cap.id;
        eve1.WhoId         = c1.id;
        
        list<Event> eve2  = new List<Event>();
        eve2.add(eve1);
        eve2.add(eve);
        
        Insert eve2;
        
        //insert eve;

      
       
        
        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
        
         CampaignMember capm2              = new CampaignMember();
        capm2.Status                      = 'Contacted';
        capm2.CampaignId                  =  cap.id;
        capm2.Teaser_E_Blast_Sent_Date__c = System.today();
        capm2.Data_Room_Access_Date__c    = System.today();
        capm2.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm2.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm2.ContactId                   = c2.id;        
        capm2.LeadId                      = l.id;
       
        insert capm2;
       
        
        
        
       
        
    }
    
    
     static testMethod void validateOpportunity3(){
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
        
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      
        Task tsk1           = new Task();
        tsk1.OwnerId        = UserInfo.getUserId();
        tsk1.Subject        ='Donni';
        tsk1.Status         ='Not Started';
        tsk1.Priority       ='Normal';
        tsk1.Description    ='test to increase the coverage';
        tsk1.Type           = 'Call';
        //tsk1.WhatId         = cap.id;
        tsk1.WhoId          = l.id;
        tsk1.ActivityDate   = System.today();
        //insert tsk1;
        
        Task tsk          = new Task();
        tsk.OwnerId        = UserInfo.getUserId();
        tsk.Subject        ='Donni';
        tsk.Status         ='Not Started';
        tsk.Priority       ='Normal';
        tsk.Description    ='test to increase the coverage';
        tsk.Type           = 'Call';
       // tsk.WhatId         = cap.id;
        tsk.WhoId          = l.id;
        tsk.ActivityDate   = System.today();
        //insert tsk;
        list<task> tsk2  = new List<task>();
        tsk2.add(tsk1);
        tsk2.add(tsk);
        insert tsk2;
        Update tsk2;
        delete tsk2;
        
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.ActivityDate   = System.today();
        eve.Description   ='testing is on process';
        //eve.WhatId        = cap.id;
        eve.WhoId         = l.id;
        
         Event eve1         = new Event();
        eve1.OwnerId       = UserInfo.getUserId();
        eve1.Subject       = 'Test';
        eve1.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve1.ActivityDate   = System.today();
        eve1.Description   ='testing is on process';
       // eve1.WhatId        = cap.id;
        eve1.WhoId         = l.id;
        
        list<Event> eve2  = new List<Event>();
        eve2.add(eve1);
        eve2.add(eve);
        
        Insert eve2;
        
        //insert eve;

      
       
        
        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
        
         CampaignMember capm2              = new CampaignMember();
        capm2.Status                      = 'Contacted';
        capm2.CampaignId                  =  cap.id;
        capm2.Teaser_E_Blast_Sent_Date__c = System.today();
        capm2.Data_Room_Access_Date__c    = System.today();
        capm2.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm2.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm2.ContactId                   = c2.id;        
        capm2.LeadId                      = l.id;
       
        insert capm2;
       
       
        
    }

}