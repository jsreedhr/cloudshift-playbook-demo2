@isTest
public class ClonePortfolioTest {
    
    	static testMethod void ClonePortfolio(){
    	Test.startTest(); 
            
        PropertyHOTELS__c pr1    = new PropertyHOTELS__c();
        pr1.Name                 = 'test Property 1';
        pr1.Country_Picklist__c  = 'United Kingdom';
        
        insert pr1;
            
        PropertyHOTELS__c pr2    = new PropertyHOTELS__c();
        pr2.Name                 = 'test Property 2';
        pr2.Country_Picklist__c  = 'United Kingdom';
        
        insert pr2;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolio';
        
        insert port;
            
        Property_Portfolio_Link__c portlink1  = new Property_Portfolio_Link__c();
        portlink1.Portfolio__c = port.id;
        portlink1.Property__c = pr1.id;
        
        insert portlink1;
        
        Property_Portfolio_Link__c portlink2  = new Property_Portfolio_Link__c();
        portlink2.Portfolio__c = port.id;
        portlink2.Property__c = pr2.id;
        
        insert portlink2;
        
        pagereference pageref=page.CloneWithRelatedProperties;
		pageref.getparameters().put('id',port.id);
		test.setcurrentpage(pageref);

		ApexPages.StandardController aps=new ApexPages.StandardController(port);

		CustomClonePortfoliosController sc = new CustomClonePortfoliosController(aps);
		sc.clonePortfolio();
            
        Test.stopTest();            
        }
}