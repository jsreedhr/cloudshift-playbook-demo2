@isTest(SeeAllData=False)
private class OpportunityTriggerHelperTest {

    private static testMethod void test() {

   Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact cont          = new Contact();
        cont.LastName          = 'Test Contact';
        cont.FirstName         = 'jhon';
        cont.Accountid         = a.id ;
        cont.Title             = 'For testing';
         
        insert cont;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.City__c  = 'My city';
        pr.Country_Picklist__c  ='United Kingdom';
       pr.Location_Type__c = 'Rural';
       pr.Description__c = 'testing Agency listing browser property';
       pr.GIA_Sq_Ft__c = 42;
        pr.Sub_Region__c = 'TBC';
        pr.Region__c = 'UK';
       pr.Spa__c = TRUE;
       pr.Conference__c = TRUE;
       pr.Ski__c = TRUE;
      pr.Development__c = TRUE;
       pr.Golf__c = TRUE;
      pr.Parking__c = TRUE;
        pr.Grade_Star_Rating__c = 'LUXURY';
        pr.Room_Count__c = 4;
       // pr.Brand_Lookup__c = a.id;
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        port.Portfolio_Description__c = 'testing Agency listing browser portfolio';
        
        insert port;
        
        list<Opportunity> oppList=new list<Opportunity>();
           Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = cont.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.Operating_Structure__c = 'Ground Rent';
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        oppList.add(opp);
         insert oppList;
        delete oppList;
        
             list<Opportunity> oppList1=new list<Opportunity>();
           Opportunity opp1         = new Opportunity();
                      opp1.recordtypeid= System.Label.Agency_Sell_Side;

        opp1.LeadSource          = 'Client_Relationship__c';
        opp1.Date_Opened__c      = System.today();
        opp1.Name                = 'Test Opportunity';
        opp1.Accountid             = a.id;
        opp1.Primary_Contact__c  = cont.id;
        opp1.StageName           = 'Pitch';
        opp1.Country__c          = 'UK';
        opp1.Property__c         = pr.id;
        opp1.Operating_Structure__c = 'Ground Rent';
        opp1.CloseDate           = System.today();
        opp1.Fee_Share_To_Total__c = 1000;
        opp1.Guide_Price__c              = 2000;
        opp1.Estimated_Sale_Price__c     = 3000;
        oppList1.add(opp1);
        
         insert oppList1;
         
         
           Opportunity opp3         = new Opportunity();
        opp3.LeadSource          = 'Client_Relationship__c';
        opp3.Date_Opened__c      = System.today();
        opp3.Name                = 'Test Opportunity';
        opp3.Accountid             = a.id;
        opp3.Primary_Contact__c  = cont.id;
        opp3.StageName           = 'Pitch';
        opp3.Country__c          = 'UK';
       opp3.Portfolio__c=port.id;
        opp3.Operating_Structure__c = 'Ground Rent';
        opp3.CloseDate           = System.today();
        opp3.Fee_Share_To_Total__c = 1000;
        opp3.Guide_Price__c              = 2000;
        opp3.Estimated_Sale_Price__c     = 3000;
        insert opp3;
    
         
             list<Opportunity> oppList2=new list<Opportunity>();
           Opportunity opp2         = new Opportunity();
           opp2.recordtypeid= System.Label.Agency_Sell_Side;
        opp2.LeadSource          = 'Client_Relationship__c';
        opp2.Date_Opened__c      = System.today();
        opp2.Name                = 'Test Opportunity';
        opp2.Accountid             = a.id;
        opp2.Primary_Contact__c  = cont.id;
        opp2.StageName           = 'Pitch';
        opp2.Country__c          = 'UK';
       // opp2.Property__c         = pr.id;
       opp2.Portfolio__c=port.id;
        opp2.Operating_Structure__c = 'Ground Rent';
        opp2.CloseDate           = System.today();
        opp2.Fee_Share_To_Total__c = 1000;
        opp2.Guide_Price__c              = 2000;
        opp2.Estimated_Sale_Price__c     = 3000;
        oppList2.add(opp2);
        
         insert oppList2;
         
        
          Property_Portfolio_Link__c ppLink     = new Property_Portfolio_Link__c();
        ppLink.Property__c                 = pr.id;
        ppLink.Portfolio__c                = port.id;
    
         Insert ppLink;
         
         
         Portfolio_Link__c pl=new Portfolio_Link__c();
         pl.Opportunity__c=opp1.id;
         pl.Property__c=pr.id;
         insert pl;

Transactions__c tra=new Transactions__c();
tra.name='Test Property - Rockerfella Plaza, New York - 21/3/2017 (Savills)';
tra.Status__c='Expected to Market';
insert tra;
        Test.startTest();
OpportunityTriggerHelper OppTriHelper=new OpportunityTriggerHelper();
OpportunityTriggerHelper.deleteTransactions(oppList);
OpportunityTriggerHelper.deleteValuations(oppList);
OpportunityTriggerHelper.deleteWorkinHand(oppList);
OpportunityTriggerHelper.updateallocations(oppList1);
OpportunityTriggerHelper.deleteportfoliolinks(oppList2,oppList1);
OpportunityTriggerHelper.createportfoliolinks(oppList2);
OpportunityTriggerHelper.deletepropertytransactions(oppList2,oppList1);
OpportunityTriggerHelper.createnewportfoliolinks(oppList2,oppList1);
OpportunityTriggerHelper.deselectallpropertiesofinterest(oppList,oppList);
         Test.StopTest();
    }

}