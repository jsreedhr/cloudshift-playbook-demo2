public class dQ_AgencyListingBuyerClass {
    
    public List<PropertyHOTELS__c> contactList {get;set;}
    public List<PropertyHOTELS__c> contactListSelected {get;set;}
    public SearchWrapper swrap {get;set;}
    public Opportunity opp{get;set;}
    public List<SelectOption> Branding { get; set; }
    public List<SelectOption> Location { get; set; }
    public List<SelectOption> rating { get; set; }
    public List<SelectOption> rooms { get; set; }
    public List <SelectOption> price { get; set; }
    
    
    public List<SelectOption> ukSubRegion { get; set; }
    public List<SelectOption> europeSubRegion { get; set; }
    public List<SelectOption> africaSubRegion { get; set; }
    public List<SelectOption> middleEastSubRegion { get; set; }
    public List <SelectOption> americasSubRegion { get; set; }
    public List <SelectOption> apacSubRegion { get; set; }
    
    public String contIdsStr { get; set; }
    public string descheader{ get; set; }
    public Decimal roomcount{ get; set; }
    public boolean  conference{ get; set; }
     public boolean  spa{ get; set; }
      public boolean  ski{ get; set; }
       public boolean  development{ get; set; }
        public boolean  parking{ get; set; }
         Set<ID> propIdset ;
         
         
         
         
    public dQ_AgencyListingBuyerClass()
    {
        propIdset = new Set<ID> ();
        String strOpportunityID;
         if(ApexPages.currentPage().getParameters().containsKey('id'))
            strOpportunityID = ApexPages.currentPage().getParameters().get('id');
            if(strOpportunityID!=null)
           opp = [Select id,name,
           
                   Primary_Contact__r.UK_Interest__c,Primary_Contact__r.Target_UK_Region_Contact__c,Primary_Contact__r.European_Interest__c,
       Primary_Contact__r.Target_European_Region__c,Primary_Contact__r.Americas_Interest__c,Primary_Contact__r.Target_Americas_Region__c
       ,Primary_Contact__r.Africa_Interest__c,Primary_Contact__r.Target_Africa_Region__c,Primary_Contact__r.Middle_East_Interest__c,
       Primary_Contact__r.Target_ME_Region__c,Primary_Contact__r.APAC_Interest__c,
       Primary_Contact__r.Target_APAC_Region__c,Primary_Contact__r.Target_Spa__c,Primary_Contact__r.Target_Conference__c,Primary_Contact__r.Target_Development__c,
       Primary_Contact__r.Target_Golf__c, Primary_Contact__r.Target_Ski__c,Primary_Contact__r.Target_Location_Type__c,Primary_Contact__r.Target_Hotel_Grade__c,Primary_Contact__r.Size_Rqrd_50_Bedrooms__c,
        Primary_Contact__r.SizeRqrd_51_200_Bedrooms__c,Primary_Contact__r.SizeRqrd_200_Bedrooms__c,Primary_Contact__r.Target_Branded_Unbranded__c
        
           from Opportunity where id=:strOpportunityID Limit 1];
        
        // , Estimated_Sale_Price__c,Primary_Contact__c, Portfolio__c,Guide_Price__c,Portfolio__r.Portfolio_Description__c, Operating_Structure__c, 
        //   Primary_Contact__r.name,
        //   Campaignid,Primary_Contact__r.City__c,Primary_Contact__r.Country__c, Primary_Contact__r.Target_Location_Type__c,
        //   Primary_Contact__r.Description
        //     ,  Primary_Contact__r.Region__c, Primary_Contact__r.Brand__c,
        //     Primary_Contact__r.Spa__c,Primary_Contact__r.Conference__c,Primary_Contact__r.Ski__c,Primary_Contact__r.Development__c,Primary_Contact__r.Golf__c,
        //   Primary_Contact__r.Parking__c,  Primary_Contact__r.Grade_Star_Rating__c,Primary_Contact__r.Room_Count__c
    
    //Primary_Contact__r.GIA_Sq_Ft__c ,, Country__c,Primary_Contact__r.Sub_Region__c
         Branding = new List<SelectOption>();
         Location= new List<SelectOption>();
        rating = new List<SelectOption>();
         rooms= new List<SelectOption>();
         price= new List<SelectOption>();
        
        ukSubRegion = new List<SelectOption>();
        europeSubRegion = new List<SelectOption>();
        africaSubRegion = new List<SelectOption>();
        middleEastSubRegion = new List<SelectOption>();
        americasSubRegion = new List<SelectOption>();  
         apacSubRegion = new List<SelectOption>();  
        
        
         contactList  = new List<PropertyHOTELS__c>();
        swrap = new SearchWrapper();
        List<Schema.Picklistentry>brandingPicklist = Contact.Target_Branded_Unbranded__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>loctionPicklist = Contact.Target_Location_Type__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>ratingPicklist = Contact.Target_Hotel_Grade__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>pricingPicklist = Contact.Target_Price_Band__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>roomsPicklist = Contact.Target_Size__c.getDescribe().getpicklistValues();
        
        List<Schema.Picklistentry>ukPicklist = Contact.Target_UK_Region_Contact__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>africaPicklist = Contact.Target_Africa_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>americaPicklist = Contact.Target_Americas_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>middlEastPicklist = Contact.Target_ME_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>europePicklist = Contact.Target_European_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>apacPicklist = Contact.Target_APAC_Region__c.getDescribe().getpicklistValues();
        
        for(Schema.Picklistentry fld : ukPicklist)
        {
            ukSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : africaPicklist)
        {
            africaSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : americaPicklist)
        {
            americasSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : middlEastPicklist)
        {
            middleEastSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : europePicklist)
        {
            europeSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : apacPicklist)
        {
            apacSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        
        for(Schema.Picklistentry fld : brandingPicklist)
        {
            Branding.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : loctionPicklist)
        {
            Location.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : ratingPicklist)
        {
            rating.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : pricingPicklist)
        {
            price.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : roomsPicklist)
        {
            rooms.add(new SelectOption(fld.value,fld.value));
        }
          List<Properties_of_Interest__c> prop = [Select id , Opportunity__c,Property__C from Properties_of_Interest__c where Opportunity__c=:opp.id];
        if(prop!=null)
        {
            
            for(Properties_of_Interest__c pi : prop)
            {
                propIdset.add(pi.Property__C);
            }
        }
         swrap=new SearchWrapper();
        // if(opp.Portfolio__c==null)
   //  buildInitialwrapper();
       
       
      
        
      //  buildheaderbar();
    }
    public void buildheaderbar()
    {
        //  conference=false;
        //  spa=false;
        //  ski=false;
        //  parking=false;
        //  development=false;
        // if(opp.Portfolio__c==null)
        // {
        //     descheader=opp.Primary_Contact__r.Description;
        //   //  roomcount=opp.Primary_Contact__r.Target_Size__c;
        //     conference=opp.Primary_Contact__r.Target_Conference__c;
        //     spa=opp.Primary_Contact__r.Target_spa__c;
        //     ski=opp.Primary_Contact__r.Target_Ski__c;
        //   // parking=opp.Primary_Contact__r.Target_Parking__c;
        //     development=opp.Primary_Contact__r.Target_Development__c;
        // }
       
    }
    public void buildInitialwrapper()
    {
       
      
         
             if(opp.Primary_Contact__r.UK_Interest__c)
             {
             swrap.uk=true;
             if(opp.Primary_Contact__r.Target_UK_Region_Contact__c!=null)
             {
                //  List<String> tempList = new List<String>();
                //  tempList.add(opp.Primary_Contact__r.Target_UK_Region_Contact__c);
             

              swrap.ukSubregions =opp.Primary_Contact__r.Target_UK_Region_Contact__c.split(';');
             }
             
             }
              if(opp.Primary_Contact__r.apac_Interest__c)
             {
             swrap.apac=true;
             if(opp.Primary_Contact__r.Target_APAC_Region__c!=null)
             {
                //  List<String> tempList = new List<String>();
                //  tempList.add(opp.Primary_Contact__r.Target_UK_Region_Contact__c);
             

              swrap.apacSubregions =opp.Primary_Contact__r.Target_APAC_Region__c.split(';');
             }
             
             }
             if(opp.Primary_Contact__r.European_Interest__c)
             {
             swrap.Europe=true;
             if(opp.Primary_Contact__r.Target_European_Region__c!=null)
             {
                
                swrap.EuropeSubregions =opp.Primary_Contact__r.Target_European_Region__c.split(';');
             }
             
             }
             if(opp.Primary_Contact__r.Americas_Interest__c)
             {
             swrap.Americas=true;
             if(opp.Primary_Contact__r.Target_Americas_Region__c!=null)
             {
              
                swrap.AmericasSubregions =opp.Primary_Contact__r.Target_Americas_Region__c.split(';');
             }
             
             }
            //  if(opp.Primary_Contact__r.Region__c.equalsignorecase('Asia Pacific'))
            //  {
            //  swrap.Asia=true;
            //  if(opp.Primary_Contact__r.Sub_Region__c!=null)
            //  {
            //      List<String> tempList = new List<String>();
            //      tempList.add(opp.Primary_Contact__r.Sub_Region__c);
            //     swrap.ukSubregions =tempList;
            //  }
             
            //  }
          
             if(opp.Primary_Contact__r.Africa_Interest__c)
             {
             swrap.Africa=true;
             if(opp.Primary_Contact__r.Target_Africa_Region__c!=null)
             {
                
                swrap.AfricaSubregions =opp.Primary_Contact__r.Target_Africa_Region__c.split(';');
             }
             
             }
             if(opp.Primary_Contact__r.Middle_East_Interest__c)
             {
             swrap.middleEast=true;
             if(opp.Primary_Contact__r.Target_ME_Region__c!=null)
             {
                 
                swrap.middleEastSubRegions =opp.Primary_Contact__r.Target_ME_Region__c.split(';');
             }
             
             }
             
             if(opp.Primary_Contact__r.Target_Spa__c)
             {
                swrap.spa=true; 
             }
             if(opp.Primary_Contact__r.Target_Conference__c)
             {
                swrap.conference=true; 
             }
             if(opp.Primary_Contact__r.Target_Ski__c)
             {
                swrap.ski=true; 
             }
             
            
        if(opp.Primary_Contact__r.Target_Development__c)
             {
                swrap.development=true; 
             }
             if(opp.Primary_Contact__r.Target_Golf__c)
             {
                swrap.golf=true; 
             }
              if(opp.Primary_Contact__r.Target_Location_Type__c!=null)
             {
                swrap.selectedLocation=opp.Primary_Contact__r.Target_Location_Type__c.split(';');
             }
              if(opp.Primary_Contact__r.Target_Hotel_Grade__c!=null)
             {
                 
                  
                swrap.selectedrating=opp.Primary_Contact__r.Target_Hotel_Grade__c.split(';');
             }
              if(opp.Primary_Contact__r.Size_Rqrd_50_Bedrooms__c!=null)
             {
                 
                 
                     swrap.smallrooms=true;
                      
             }
                  if (opp.Primary_Contact__r.SizeRqrd_51_200_Bedrooms__c)
                 {
                        swrap.mediumrooms=true;
                 }
                 
                  if (opp.Primary_Contact__r.SizeRqrd_200_Bedrooms__c)
                 {
                      swrap.largerooms=true;
                 }
                   
                   if(opp.Primary_Contact__r.Target_Branded_Unbranded__c!=null)
                   {
                       swrap.selectedBranding=opp.Primary_Contact__r.Target_Branded_Unbranded__c.split(';');
                   }
               
             
             
            //  if(opp.Primary_Contact__r.Brand__c!=null)
            //  {
            //       List<String> tempList = new List<String>();
            //      tempList.add('Branded');
            //     swrap.selectedBranding=tempList; 
            //  }
            //  else
            //  {
            //       List<String> tempList = new List<String>();
            //      tempList.add('Unbranded');
            //     swrap.selectedBranding=tempList; 
            //  }
             
            //  if(opp.Estimated_Sale_Price__c!=null)
            //  {
            //      Decimal priceinmil= opp.Estimated_Sale_Price__c/1000000;
                 
            //      if(priceinmil<=5)
            //      {
            //           swrap.zerofive=true;
                      
            //      }
            //      else if(priceinmil<=10)
            //      {
            //          swrap.fiveten=true;
            //      }
            //      else if(priceinmil<=20)
            //      {
            //           swrap.tentwenty=true;
            //      }
            //      else if(priceinmil<=50)
            //      {
            //           swrap.twentyfifty=true;
            //      }
            //      else 
            //      {
            //           swrap.fiftyabove=true;
            //      }
               
            //  }
         
             
       
      
        searchContacts();
          
        
    }
    
    public void searchContacts()
    {
          contactList  = new List<PropertyHOTELS__c>();
        String query =buildquery();
        Database.QueryLocator resultList = Database.getQueryLocator(query);
            // Get an iterator
            Database.QueryLocatorIterator resultIterator =  resultList.iterator();
            
            // Iterate over the records
            while (resultIterator.hasNext())
            {
                PropertyHOTELS__c c = (PropertyHOTELS__c)resultIterator.next();
                contactList.add(c);
            }
       
    }
    public String buildquery()
    {
        system.debug('----'+propIdset);
        String querysTr ='';
        querysTr+='Select id, name, Room_Count__c, Grade_Star_Rating__c, Location_Type__c, City__c, Country_Picklist__c, Sub_Region__c from PropertyHOTELS__c where Buy_side__c != true and id not in :propIdset and ';
      boolean loopFlag=false;
       if(swrap.uk)
       {
           querysTr+='(';
       loopFlag=true;
        querysTr+=' ( Region__c = \'United Kingdom\' ';
        if(swrap.ukSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.ukSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.ukSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.ukSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
       
      
         
       if(swrap.Europe)
       {
     
        if(loopFlag)
        querysTr+=' OR ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
        querysTr+=' ( Region__c = \'Europe\' ';
        if(swrap.EuropeSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.EuropeSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.EuropeSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.EuropeSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
        if(swrap.apac)
       {
     
        if(loopFlag)
        querysTr+=' OR ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
        querysTr+=' ( Region__c = \'Asia Pacific\' ';
        if(swrap.apacSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.apacSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.apacSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.apacSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
       
       if(swrap.Africa)
       {
       
        if(loopFlag)
        querysTr+=' OR ';
        else
         querysTr+=' ( ';
        loopFlag=true;
        
         
        querysTr+=' ( Region__c = \'Africa\' ';
        if(swrap.AfricaSubregions!=null)
        {
            String tempStr='(';
           Integer i=0;
            for(String reg : swrap.AfricaSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.AfricaSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.AfricaSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr ;
        }
        
        
            querysTr+=') ';
       }
       
         
         
       if(swrap.Americas)
       {
           
        if(loopFlag)
        querysTr+=' OR ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
          
       
        querysTr+=' ( Region__c = \'Americas\' ';
        if(swrap.AmericasSubregions!=null)
        {
            String tempStr='(';
          Integer i=0;
            for(String reg : swrap.AmericasSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.AmericasSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.AmericasSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr ;
        }
        
        querysTr+=' ) ';
 
       }
       
       
         
         
       if(swrap.middleEast)
       {
       if(loopFlag)
        querysTr+=' OR ';
        else
         querysTr+='(';
        loopFlag=true;
        querysTr+='( Region__c = \'middleEast\' ';
        if(swrap.middleEastSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.middleEastSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.middleEastSubregions.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
            if(!swrap.middleEastSubregions.isEmpty())
            querysTr+=' and Sub_Region__c in  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
        
      if(loopFlag)
        querysTr+=' )';
        if(swrap.selectedBranding!=null && !swrap.selectedBranding.isEmpty())
        {
            
             if(loopFlag)
             querysTr+=' AND (';
              else
            querysTr+=' ( ';
             
              loopFlag=true;
             String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedBranding)
            {
                
                if(reg=='Branded')
                {
                    If(i!=0)
                tempStr+=' OR ';
                    tempStr+=' Brand_Lookup__c !=null ';
                }
                else if(reg=='Unbranded')
                {
                    If(i!=0)
                tempStr+=' OR ';
                     tempStr+=' Brand_Lookup__c =null ';
                }
                i++;
               
            }
           tempStr+=')';
           
            querysTr+='   ' +tempStr;
             querysTr+=' ) ';
        }
        
        if(swrap.selectedLocation!=null && !swrap.selectedLocation.isEmpty())
        {
             if(loopFlag)
             querysTr+=' AND ( ';
              else
            querysTr+=' ( ';
              loopFlag=true;
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedLocation)
            {
                
                tempStr+='\''+reg+'\'';
                if(i!=(swrap.selectedLocation.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
           
            querysTr+='  Location_Type__c In  ' +tempStr;
               querysTr+=' ) ';
        }
        
          
        if(swrap.selectedrating!=null && !swrap.selectedrating.isEmpty())
        {
             if(loopFlag)
             querysTr+=' AND (';
              else
            querysTr+=' ( ';
              loopFlag=true;
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedrating)
            {
                
                tempStr+='\''+reg+'\'';
                if(i!=(swrap.selectedrating.size()-1))
                tempStr+=',';
                i++;
            }
            tempStr+=')';
           
            querysTr+='  Grade_Star_Rating__c in  ' +tempStr;
               querysTr+=' ) ';
        }
        boolean loopFlagRoom=false;
        // if(swrap.zerofive)
        // {
        //     if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND ( (';
        //      else
        //     querysTr+=' ( (';
        //     loopFlag=true;
        //     loopFlagRoom=true;
        //       querysTr+='  Target_Price0_5m__c = true';
        //       querysTr+=' ) ';
            
        // }
        
        // if(swrap.fiveten)
        // {
             
        //     if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND ( (';
        //       else
        //     querysTr+='( (';
        //     loopFlag=true;
        //       loopFlagRoom=true;
        //       querysTr+='  TargetPrice_5m_10m__c = true';
        //       querysTr+=' ) ';
            
        // }
       
        // if(swrap.tentwenty)
        // {
           
          
        //     if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND ( (';
        //       else
        //     querysTr+='( (';
        //     loopFlag=true;
        //       loopFlagRoom=true;
        //       querysTr+='  TargetPRice10m_20m__c = true';
        //       querysTr+=' ) '; 
        // }
      
        // if(swrap.twentyfifty)
        // {
             
        //     if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND ( (';
        //       else
        //     querysTr+='( (';
        //     loopFlag=true;
        //       loopFlagRoom=true;
        //       querysTr+='  TargetPrice_20m_50m__c = true';
        //       querysTr+=' ) ';
            
        // }
       
        // if(swrap.fiftyabove)
        // {
             
        //     if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND ( (';
        //       else
        //     querysTr+='( (';
        //     loopFlag=true;
        //       loopFlagRoom=true;
        //       querysTr+='  TargetPrice_50m__c = true';
        //       querysTr+=' ) ';
        // }
        
        if(loopFlagRoom)
         querysTr+=' ) ';
        // else
        // {
        //     if(loopFlag)
        //      querysTr+=' AND (';
        //       else
        //     querysTr+='(';
        //     loopFlag=true;
        //       querysTr+='  TargetPrice_50m__c = false';
        //       querysTr+=' ) '; 
        // }
         loopFlagRoom=false;
        if(swrap.smallrooms)
        {
           
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  Room_Count__c <= 50';
               querysTr+=' ) '; 
            
        }
        //  else
        // {
        //      if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND (';
        //       else
        //     querysTr+='(';
        //     loopFlag=true;
        //       querysTr+='  Size_Rqrd_50_Bedrooms__c = false';
        //       querysTr+=' ) '; 
        // }
        if(swrap.mediumrooms)
        {
            
             if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+=' ( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  Room_Count__c <= 200';
               querysTr+=' ) ';
            
        }
         
        if(swrap.largerooms)
        {
            
              if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND  ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  Room_Count__c > 200';
               querysTr+=' ) ';
        }
        
          if(loopFlagRoom)
         querysTr+=' ) ';
       
        
          if(swrap.conference)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Conference__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.development)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Development__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.ski)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Ski__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.spa)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Spa__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.golf)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Golf__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
       
       
      querysTr= querysTr.removeEnd('and ');
      querysTr+=' order by lastmodifieddate desc limit 1000 ';
       system.debug('---query---'+querysTr);
       return querysTr;
    }
    
    public Pagereference saveCampaignMembers()
    {
          system.debug('ExceptionCheck');
        
        List<String> strList   = contIdsStr.split(';;');
        List<Properties_of_Interest__c> insertCampaignmemList   = new List<Properties_of_Interest__c>();
        contactListSelected = new List<PropertyHOTELS__c>();
        Map<ID,PropertyHOTELS__c> mapContact = new Map<ID,PropertyHOTELS__c>();
        for(PropertyHOTELS__c cont : contactList)
        {
            mapContact.put(cont.id,cont);
        }
        for(String str : strList)
        {
            if(mapContact.containsKey(str))
            contactListSelected.add(mapContact.get(str));
            
        }
       propIdset = new Set<ID> ();
        List<Properties_of_Interest__c> prop = [Select id , Opportunity__c,Property__C from Properties_of_Interest__c where Opportunity__c=:opp.id];
        if(prop!=null)
        {
            
            for(Properties_of_Interest__c pi : prop)
            {
                propIdset.add(pi.Property__C);
            }
        }
        for(PropertyHOTELS__c cont : contactListSelected)
        {
            if(!propIdset.contains(cont.Id))
            {
            Properties_of_Interest__c newCM = new Properties_of_Interest__c();
            newCm.Opportunity__c=opp.id;
            newCm.Property__C= cont.Id;
           
                insertCampaignmemList.add(newCm);
            }

        }
        try
        {
            insert insertCampaignmemList;
        }
        catch(Exception e)
        {
            system.debug('ExceptionCheck'+e);
            
            return null;
            
        }
        
         Pagereference pg = new Pagereference('/'+opp.id);
        return pg;
    
        
    }
    
    public void addToCampaignAction()
    {
         List<String> strList   = contIdsStr.split(';;');
        List<Properties_of_Interest__c> insertCampaignmemList   = new List<Properties_of_Interest__c>();
        contactListSelected = new List<PropertyHOTELS__c>();
        Map<ID,PropertyHOTELS__c> mapContact = new Map<ID,PropertyHOTELS__c>();
        for(PropertyHOTELS__c cont : contactList)
        {
            mapContact.put(cont.id,cont);
        }
        for(String str : strList)
        {
            if(mapContact.containsKey(str))
            contactListSelected.add(mapContact.get(str));
            
        }
        
       for(PropertyHOTELS__c cont : contactListSelected)
        {
            Properties_of_Interest__c newCM = new Properties_of_Interest__c();
            newCm.Opportunity__c=opp.id;
            newCm.Property__C= cont.Id;
           
                insertCampaignmemList.add(newCm);

        }
        
    }
    public Pagereference cancelaction()
    {
        Pagereference pg = new Pagereference('/'+opp.id);
        return pg;
    }
    
    public void clearFilter()
    {
        
        swrap.uk=false;
        swrap.Europe=false;
        swrap.Africa=false;
        swrap.middleEast=false;
        swrap.Americas=false;
        swrap.apac=false;
        swrap.conference=false;
        swrap.ski=false;
        swrap.development=false;
        swrap.spa=false;
        swrap.golf=false;
        swrap.selectedBranding=null;
         swrap.selectedLocation=null;
          swrap.selectedrating=null;
           swrap.selectedRooms=null;
            swrap.selectedPrice=null;
             //swrap.selectedBranding=null;
             
         swrap.ukSubregions=null;
          swrap.EuropeSubregions=null;
          swrap.AfricaSubregions=null;
           swrap.middleEastSubregions=null;
            swrap.AmericasSubregions=null;
            swrap.apacSubregions=null;
            contactList= new List<PropertyHOTELS__c>();
             contactListSelected= new List<PropertyHOTELS__c>();
             
                swrap.zerofive=false;
         swrap.fiveten=false;
         swrap.tentwenty=false;
         swrap.twentyfifty=false;
         swrap.fiftyabove=false;
        
         swrap.smallrooms=false;
         swrap.mediumrooms=false;
         swrap.largerooms=false;
    }

    public Class SearchWrapper
    {
        public boolean uk{get;set;}
        public boolean Europe{get;set;}
        public boolean Africa{get;set;}
        public boolean middleEast{get;set;}
        public boolean Americas{get;set;}
         public boolean apac{get;set;}
         
        public boolean zerofive{get;set;}
        public boolean fiveten{get;set;}
        public boolean tentwenty{get;set;}
        public boolean twentyfifty{get;set;}
        public boolean fiftyabove{get;set;}
        
        public boolean smallrooms{get;set;}
        public boolean mediumrooms{get;set;}
        public boolean largerooms{get;set;}
       
        
        public List<String> ukSubregions{get;set;}
        public List<String> EuropeSubregions{get;set;}
        public List<String> AfricaSubregions{get;set;}
        public List<String> middleEastSubregions{get;set;}
        public List<String> AmericasSubregions{get;set;}
        public List<String> ApacSubregions{get;set;}
         
     public List<String> selectedBranding{get;set;}
      public List<String> selectedLocation{get;set;}
      public List<String> selectedrating{get;set;}
      public List<String> selectedRooms{get;set;}
      public List<String> selectedPrice{get;set;}
      
      public boolean conference{get;set;}
       public boolean ski{get;set;}
        public boolean development{get;set;}
         public boolean spa{get;set;}
         public boolean golf{get;set;}
         
       public SearchWrapper()
       {
           uk=false;
           Europe=false;
           Africa=false;
           middleEast=false;
           Americas=false;
            apac=false;
            
           
                zerofive=false;
         fiveten=false;
         tentwenty=false;
         twentyfifty=false;
         fiftyabove=false;
        
         smallrooms=false;
         mediumrooms=false;
         largerooms=false;
           
            conference=false;
           ski=false;
           development=false;
           spa=false;
           golf=false;
           ukSubregions = new List<String>();
            EuropeSubregions = new List<String>();
             AfricaSubregions = new List<String>();
              middleEastSubregions = new List<String>();
               AmericasSubregions = new List<String>();
                ApacSubregions = new List<String>();
                selectedBranding = new List<String>();
                 selectedLocation = new List<String>();
                  selectedrating = new List<String>();
                   selectedRooms = new List<String>();
                    selectedPrice = new List<String>();
       
           
       }
    }
    
   
}


//  if(swrap.selectedPrice!=null && !swrap.selectedPrice.isEmpty())
//         {
//              if(loopFlag)
//              querysTr+=' AND (';
//               else
//             querysTr+='(';
//               loopFlag=true;
//             String tempStr='(';
//             Integer i=0;
            
            
//             for(String reg : swrap.selectedPrice)
//             {
                
                        
       
//                 // if(reg==')
//                 // {
//                 //   Target_Price0_5m__c = true  
//                 // }
//                 // else if()
//                 // {
//                 //     TargetPRice10m_20m__c=true
                    
//                 // }
//                 // else if()
//                 // {
//                 //     TargetPrice_20m_50m__c=true;
                    
//                 // }
//                 // else if()
//                 // {
//                 //     TargetPrice_50m__c= true;
                    
//                 // }
//                 // else if()
//                 // {
//                 //     TargetPrice_5m_10m__c= true;
                    
//                 // }
                
//                 tempStr+='\''+reg+'\'';
//                 if(i!=(swrap.selectedPrice.size()-1))
//                 tempStr+=',';
//                 i++;
//             }
//             tempStr+=')';
           
//             querysTr+='  Target_Price_Band__c includes  ' +tempStr;
//               querysTr+=' ) ';
//         }
        
          
//         if(swrap.selectedRooms!=null && !swrap.selectedRooms.isEmpty())
//         {
//              if(loopFlag)
//              querysTr+=' AND (';
//               else
//                 querysTr+='(';
//               loopFlag=true;
//             String tempStr='(';
//             Integer i=0;
//             for(String reg : swrap.selectedRooms)
//             {
                
//                 tempStr+='\''+reg+'\'';
//                 if(i!=(swrap.selectedRooms.size()-1))
//                 tempStr+=',';
//                 i++;
//             } 
//             tempStr+=')';
           
//             querysTr+='  Target_Size__c includes  ' +tempStr;
//               querysTr+=' ) ';
//         }