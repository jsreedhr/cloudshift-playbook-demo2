public without sharing class CampaignMemberController {

    public list <CampaignMember> listCampaignMember{get; set;}
    public list<Campaign> listCampaign{get; set;}
  
   
    public string rowNo{get;set;}
    Public String CampaignId;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
     public List<SelectOption> Statuslst {get;set;}
    public String selectedStatus {get;set;}
    //constructor to grt the records
    
    public CampaignMemberController(apexpages.standardController stdController){
    
        listCampaign = new list<Campaign>();
        listCampaignMember = new list<CampaignMember>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        CampaignId = apexpages.currentpage().getparameters().get('id');
        listCampaign = [select name, id from Campaign where id=: CampaignId limit 1];
        listCampaignMember = [select Id, CampaignId, FirstName, LastName, ContactId, LeadId, Status, Marketing_Email_Opt_Out__c, Teaser_E_Blast_Sent__c, NDA_Status__c, NDA_Signed_Date__c, NDA_Sent_Date__c, Data_Room_Use__c, Data_Room_Access_Date__c, X1st_Inspection_Date__c, X2nd_Inspection_Date__c, X3rd_Inspection_Date__c FROM CampaignMember where CampaignId =: CampaignId limit 4999];
      
        for(CampaignMember obj_cm :listCampaignMember){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_cm));
            
        }
          Statuslst = new List<SelectOption>();        
    
        Statuslst.add(new SelectOption('To Be Contacted','To Be Contacted'));
        Statuslst.add(new SelectOption('Contacted','Contacted'));
        Statuslst.add(new SelectOption('Engaged','Engaged'));
        Statuslst.add(new SelectOption('Interested','Interested'));
        Statuslst.add(new SelectOption('1st Round Bid Submitted','1st Round Bid Submitted'));
        Statuslst.add(new SelectOption('2nd Round Bid Submitted','2nd Round Bid Submitted'));
        Statuslst.add(new SelectOption('Exclusive Bidder','Exclusive Bidder'));
        Statuslst.add(new SelectOption('Not Interested','Not Interested'));
    }  
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            
        integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        CampaignMember objcm = new CampaignMember();
         system.debug('######------------++++' + l);
     
        objcm.CampaignId = CampaignId;
              
        listCampaignMember.add(objcm);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objcm));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new campaignmember record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        CampaignMember objcm = new CampaignMember();
        objcm.CampaignId = CampaignId;
        listCampaignMember.add(objcm);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objcm));
        }
        
        
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        system.debug('----'+lstOfbooleanProjectWrapper);
        list<CampaignMember> toDeleteRows = new list<CampaignMember>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objcm.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objcm);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
          system.debug('----'+toDeleteRows);
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<CampaignMember> todelete = new list<CampaignMember>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objcm.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objcm);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    saveCampaignMember
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference saveCampaignMember() {
        system.debug('saveCampaignMember'  );
        if(showerror !=true)
        {
        list<CampaignMember> ListtoUpsert= new list<CampaignMember> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        Set<ID> oppId = new Set<ID>();
         for(booleanProjectWrapper objcm: lstOfbooleanProjectWrapper){
         
         
            if(string.valueof(objcm.objcm.CampaignId) != null)
            {
           
                 ListtoUpsert.add(objcm.objcm); 
                 oppId.add(objcm.objcm.CampaignId);
            }
          
                else if(string.valueof(objcm.objcm.CampaignID) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }        
     
                    
               
                     
             
        
        try{
           // Upsert ListtoUpsert;
           list<CampaignMember> listinsert = new list<CampaignMember>();
           list<CampaignMember> listupdate = new list<CampaignMember>();

            for(CampaignMember cm : ListtoUpsert )
             {
            if(cm.id == NULL)
               listinsert.add(cm);
            else
               listupdate.add(cm);
               }

             if(listupdate.size()>0)
          update listupdate;
         if(listinsert.size()>0)
         insert listinsert;
            
        } catch(Exception e) {
            //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
            //                          e.getMessage()));  
            return null; 
        }
                      
                       
          PageReference acctPage = new PageReference('/' + CampaignId);
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
    public boolean checkdupe;
    public pagereference checkduplicates()
    {checkdupe=false;
        Set<ID> dupcheckset = new Set<ID> ();
            showerror = false;
        errorPos= 0;
        Integer i=0;
        for(booleanProjectWrapper bwrap : lstOfbooleanProjectWrapper)
        {   
           
        if(bwrap.objcm.ContactId!=null && !dupcheckset.contains(bwrap.objcm.ContactId))
        {
           dupcheckset.add(bwrap.objcm.ContactId); 
             
        }
        else if(bwrap.objcm.ContactId!=null && dupcheckset.contains(bwrap.objcm.ContactId))
        {
          
            showerror =true;
            checkdupe=true;
            errorPos=i;
             system.debug('dupcheckset' +dupcheckset);
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                      'Duplicate Contact'));
            
            return null;
           
            
        }
         i++;
         
        
    }
        return saveCampaignMember();
    }
    
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public CampaignMember objcm{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, CampaignMember objcms){
          objcm = objcms;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}