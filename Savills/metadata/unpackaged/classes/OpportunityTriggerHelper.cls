public with Sharing class OpportunityTriggerHelper{
     
    public static boolean opportunityStatic;
     public static boolean opprecursionstatic=false; 
    //Method used to delete transactions linked to opportunities when opportunities are deleted. In practice this will only be for Agency.
    public static void deleteTransactions(List <Opportunity> lst_opps){
        
        Set <ID> set_Oppids = new Set <ID>();
       
        for(Opportunity o: lst_opps){
            set_Oppids.add(o.id);                        
        }
        
        IF(!set_Oppids.isEmpty()){
            List <Transactions__c> lst_transactions = new List <Transactions__c>([SELECT ID, Opportunity__c FROM Transactions__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_transactions.isEmpty()){
                delete(lst_transactions); 
            }
        }
    }
    
    //Method used to delete valuations linked to opportunities when opportunities are deleted. In practice this will only be for Valuations.
    public static void deleteValuations(List <Opportunity> lst_opps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Opportunity o: lst_opps){
            set_Oppids.add(o.id);                        
        }
        
        IF(!set_Oppids.isEmpty()){
            List <Valuation__c> lst_valuations = new List <Valuation__c>([SELECT ID, Opportunity__c FROM Valuation__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_valuations.isEmpty()){
                delete lst_valuations;
            }
        }
    }
    
    //Method used to delete work in hand records linked to opportunities when opportunities are deleted. This could affect both Agency and Valuations.
    public static void deleteWorkinHand(List <Opportunity> lst_opps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Opportunity o: lst_opps){
            set_Oppids.add(o.id);                        
        }
        
        IF(!set_Oppids.isEmpty()){
            List <Work_in_Hand__c> lst_wih = new List <Work_in_Hand__c>([SELECT ID, Opportunity__c FROM Work_in_Hand__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_wih.isEmpty()){
                delete lst_wih;
            }
        }
    }
    
    //Method used to update allocations linked to opportunities when an opportunity is updated
    public static void updateallocations(List <Opportunity> lst_opps){
        
        Set <ID> set_OppIds = new Set <ID>();
        
        for(Opportunity o: lst_opps){
            set_OppIds.add(o.id);   
        }
        
        IF(!set_OppIds.isEmpty()){
            
            List <Portfolio_Link__c> lst_portlinks = new List <Portfolio_Link__c>([SELECT ID, Opportunity__c, Transaction__c FROM Portfolio_Link__c WHERE Opportunity__c in: set_OppIds]);
            
            IF(!lst_portlinks.isEmpty()){
                
                for(Opportunity o: lst_opps){
                    for(Portfolio_Link__c lp: lst_portlinks){
                        IF(o.id == lp.Opportunity__c){
                            IF(o.Transaction_ID__c != null){
                                lp.Transaction__c = o.Transaction_ID__c;
                            }
                            IF(o.Transaction_ID__c == null){
                                lp.Transaction__c = null;
                            }
                        }
                    }                
                }
                Update lst_portlinks;
            }
        }
    }
    
    //Method used to delete portfolio links related to opportunities when opportunities are changed from being portfolio relevant to property relevant.
    public static void deleteportfoliolinks(List <Opportunity> lst_oldopps, List <Opportunity> lst_newopps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Opportunity o: lst_oldopps){
            for(Opportunity n: lst_newopps){
                IF(o.id == n.id){
                    IF(o.Portfolio__c != NULL && o.Property__c == Null && n.Portfolio__c == Null && n.Property__c != Null)
                        
                        set_Oppids.add(o.id); 
                }
            }
        }
        
        IF(!set_Oppids.isEmpty()){
            List <Portfolio_Link__c> lst_portfoliolinks = new List <Portfolio_Link__c>([SELECT ID, Opportunity__c FROM Portfolio_Link__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_portfoliolinks.isEmpty()){
                delete(lst_portfoliolinks); 
            }           
        }
    }
    
    //Method used to create opp/property links upon creation of an opportunity
    
    public static void createportfoliolinks(List <Opportunity> lst_opps){
        
        Set <ID> set_Portfolioids = new Set <ID>();
        Map <ID, ID> map_OppPortfolios = new Map <ID, ID>();
        
        for(Opportunity o: lst_opps){
            IF(o.Portfolio__c != null && o.RecordTypeId == System.Label.Agency_Sell_Side){
                set_Portfolioids.add(o.Portfolio__c);
                map_OppPortfolios.put(o.Portfolio__c, o.id);
            }
        }
        
        If(!set_Portfolioids.isEmpty()){
            
            List <Property_Portfolio_Link__c> lst_propportlinks = new List<Property_Portfolio_Link__c>([SELECT ID, Portfolio__c, Property__c FROM Property_Portfolio_Link__c WHERE Portfolio__c in: set_Portfolioids]);
            
            IF(!lst_propportlinks.isEmpty()){
                
                List <Portfolio_Link__c> lst_portlinkstocreate = new List<Portfolio_Link__c>();
                
                for(Property_Portfolio_Link__c ppl: lst_propportlinks){
                    Portfolio_Link__c p = new Portfolio_Link__c();
                    p.Property__c = ppl.Property__c;
                    p.Opportunity__c = map_OppPortfolios.get(ppl.Portfolio__c); 
                    lst_portlinkstocreate.add(p);
                }
                insert lst_portlinkstocreate;                   
            } 
        }
    }
    
    //Method used to delete property transactions when opportunities are changed from being portfolio relevant to property relevant.
    public static void deletepropertytransactions(List <Opportunity> lst_oldopps, List <Opportunity> lst_newopps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Opportunity o: lst_oldopps){
            for(Opportunity n: lst_newopps){
                IF(o.id == n.id){
                    IF(o.RecordTypeId == System.Label.Agency_Sell_Side && o.Portfolio__c != Null && o.Property__c == Null && n.Portfolio__c == Null && n.Property__c != Null)
                        set_Oppids.add(o.id); 
                }
            }
        }
        
        List <Transactions__c> lst_transtocreate = new List <Transactions__c>();
        
        IF(!set_Oppids.isEmpty()){
            List <Transactions__c> lst_transactions = new List <Transactions__c>([SELECT ID, Opportunity__c FROM Transactions__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_transactions.isEmpty()){
                delete(lst_transactions);
            }
            
            for(Opportunity o: lst_oldopps){
                for(Opportunity n: lst_newopps){
                    for (ID i: set_Oppids){
                        If(i == o.id && i == n.id){
                            Transactions__c t = New Transactions__c();
                            t.CurrencyIsoCode = n.CurrencyIsoCode;
                            t.Date_of_Sale__c = n.CloseDate;
                            t.Name = n.Transaction_Name__c;
                            t.Opportunity__c = o.id;
                            t.Guide_Price__c = n.Guide_Price__c;
                            t.Operating_Structure__c = o.Operating_Structure__c;
                            t.Property__c = n.Property__c;
                            t.Status__c = n.Opp_Property_Status_Mapping__c;
                            t.Purchaser__c = n.Purchaser_Account__c;
                            t.Vendor__c = n.AccountId;
                            t.Vendor_s_Agent__c = System.Label.Savills_Account;
                            t.GIA_Area_sale__c = n.Property__r.GIA_Sq_Ft__c;
                            t.Room_Count_at_Sale__c = n.Property__r.Room_Count__c;
                            lst_transtocreate.add(t);
                            
                        }
                    }
                }
                insert lst_transtocreate;
            }
        }
    }
    
    //Method used to create opp/property links upon change from property to portfolio deal
    
    public static void createnewportfoliolinks(List <Opportunity> lst_oldopps, List <Opportunity> lst_newopps){
        
        if(opportunityStatic==null || opportunityStatic==false )
        {
            Set <ID> set_Portfolioids = new Set <ID>();
            Map <ID, ID> map_OppPortfolios = new Map <ID, ID>();
            system.debug('---Hormese1'+lst_oldopps);
            system.debug('---Hormese1'+lst_newopps);
            
            for(Opportunity o: lst_oldopps){
                for(Opportunity n: lst_newopps){
                    IF(o.id == n.id){
                        
                        IF(o.Portfolio__c == Null && o.Property__c != Null && n.Portfolio__c != Null && n.Property__c == Null && o.RecordTypeId == System.Label.Agency_Sell_Side)
                            set_Portfolioids.add(n.Portfolio__c);
                        map_OppPortfolios.put(n.Portfolio__c, n.id);
                        system.debug('---Hormese1'+map_OppPortfolios);
                    }
                }
            }
            
            If(!set_Portfolioids.isEmpty()){
                
                List <Property_Portfolio_Link__c> lst_propportlinks = new List<Property_Portfolio_Link__c>([SELECT ID, Portfolio__c, Property__c FROM Property_Portfolio_Link__c WHERE Portfolio__c in: set_Portfolioids]);
                
                IF(!lst_propportlinks.isEmpty()){
                    
                    List <Portfolio_Link__c> lst_portlinkstocreate = new List<Portfolio_Link__c>();
                    
                    for(Property_Portfolio_Link__c ppl: lst_propportlinks){
                        Portfolio_Link__c p = new Portfolio_Link__c();
                        p.Property__c = ppl.Property__c;
                        p.Opportunity__c = map_OppPortfolios.get(ppl.Portfolio__c); 
                        lst_portlinkstocreate.add(p);
                    }
                    insert lst_portlinkstocreate;    
                    system.debug('---Hormese'+lst_portlinkstocreate);
                } 
            }
            opportunityStatic=true;
        }
    }
    
    //Method used to set all properties of interest Back to 'Exclusive = False' when an opp is marked as portfolio relevant having had an exclusive property marked
    public static void deselectallpropertiesofinterest(List <Opportunity> lst_oldopps, List <Opportunity> lst_newopps){
        
        Set <ID> set_Oppids = new Set <ID>();
        
        for(Opportunity o: lst_oldopps){
            for(Opportunity n: lst_newopps){
                IF(o.id == n.id){
                    IF(o.RecordTypeId == System.Label.Agency_Buy_Side && o.Portfolio__c == NULL && o.Property__c != Null && n.Portfolio__c != Null && n.Property__c == Null)
                        set_Oppids.add(o.id);
                }
            }
        }
        
        IF(!set_Oppids.isEmpty()){
            List <Properties_of_Interest__c> lst_props_of_interest = new List <Properties_of_Interest__c>([SELECT ID, Opportunity__c, Exclusive_Selection__c FROM Properties_of_Interest__c WHERE Opportunity__r.id in: set_Oppids]);
            IF(!lst_props_of_interest.isEmpty()){
                for(Properties_of_Interest__c poi: lst_props_of_interest){
                    poi.Exclusive_Selection__c = FALSE;
                }
                update lst_props_of_interest;
            }           
        }
    }
    
    //Method used to stamp current currency rate on the opportunity
    public static void getcurrentcurrencyrate(List <Opportunity> lst_opps){
        
        List <CurrencyType> lst_currencies = new List <CurrencyType>([SELECT ID, IsoCode, ConversionRate FROM CurrencyType]);
        Map <String, Decimal> maprates = new Map <String, Decimal>();
        
        If(!lst_currencies.isEmpty()){
            for(CurrencyType c: lst_currencies){
                maprates.put(c.IsoCode, c.ConversionRate);
            }
            
            for(Opportunity o: lst_opps){
                IF(o.StageName != 'Completed' && o.StageName != 'Billing Completed' && o.CurrencyIsoCode != NULL){
                    o.Forex_to_GDP_Rate__c = maprates.get(o.CurrencyIsoCode);
                }
            }
           for(Opportunity o: lst_opps){
                IF(o.StageName != 'Completed' && o.StageName != 'Billing Completed' && o.CurrencyIsoCode == 'GBP'){
                    o.Forex_to_USD_Rate__c = maprates.get(o.CurrencyIsoCode) * maprates.get('USD');
                }
            }
            for(Opportunity o: lst_opps){
                IF(o.StageName != 'Completed' && o.StageName != 'Billing Completed' && o.CurrencyIsoCode != NULL){
                    o.Forex_to_USD_Rate__c = 1/maprates.get(o.CurrencyIsoCode) * maprates.get('USD');
                }
            }
        }
    }
}