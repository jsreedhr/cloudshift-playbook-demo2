public class dQ_TaskTriggerHandler{


public static Boolean cmtriggerrecursive= false;
public static void populateCampaignHistory(List<Task> tskList)

    {
        String campPreix= Campaign.sobjecttype.getDescribe().getKeyPrefix();
        String contPreix=  Contact.sobjecttype.getDescribe().getKeyPrefix();
        String LeadPreix=  Lead.sobjecttype.getDescribe().getKeyPrefix();
       
        Map<ID,Set<ID>> LeadContIds = new Map<ID,Set<ID>>();
        
       // Map<CMID
        for(Task tsk : tskList)
        {
            
        if(tsk.whatid!=null && String.valueof(tsk.whatid).startsWith(campPreix) )
        {
            if(tsk.whoid!=null &&(String.valueOf(tsk.whoid).startsWith(contPreix) || String.valueof(tsk.whatid).startsWith(LeadPreix)))
            {
                if(LeadContIds.containskey(tsk.whatid))
                  LeadContIds.get(tsk.whatid).add(tsk.whoid);
                else
                {
                    Set<ID> tempList = new Set<ID>();
                    tempList.add(tsk.whoid);
                    LeadContIds.put(tsk.whatid,tempList);
                }
             
            }
            
        }
        }
        processtaskdates(LeadContIds);
       
    }
    
    
    public static void populateCampaignHistoryEvent(List<Event> tskList)

    {
        String campPreix= Campaign.sobjecttype.getDescribe().getKeyPrefix();
        String contPreix=  Contact.sobjecttype.getDescribe().getKeyPrefix();
        String LeadPreix=  Lead.sobjecttype.getDescribe().getKeyPrefix();
       
        Map<ID,Set<ID>> LeadContIds = new Map<ID,Set<ID>>();
       
       // Map<CMID
        for(Event tsk : tskList)
        {
            
        if(tsk.whatid!=null && String.valueof(tsk.whatid).startsWith(campPreix) )
        {
            if(tsk.whoid!=null &&(String.valueOf(tsk.whoid).startsWith(contPreix) || String.valueof(tsk.whatid).startsWith(LeadPreix)))
            {
                if(LeadContIds.containskey(tsk.whatid))
                  LeadContIds.get(tsk.whatid).add(tsk.whoid);
                else
                {
                    Set<ID> tempList = new Set<ID>();
                    tempList.add(tsk.whoid);
                    LeadContIds.put(tsk.whatid,tempList);
                }
             
            }
            
        }
        }
        
        processtaskdates(LeadContIds);
    }


public static void processtaskdates(  Map<ID,Set<ID>> LeadContIds )
{
    Map<ID,List<CampaignMember>> LeadContCmemberMap = new Map<ID,List<CampaignMember>>();
        Map<ID,Set<ID>> LeadContIdsSelected = new Map<ID,Set<ID>>();
        Set<ID> LeadContactIDSet = new Set<ID>();
        
        
         List<CampaignMember> cmMemberList = new List<CampaignMember> ([Select id,CampaignId,Teaser_E_Blast_Sent_Date__c,Data_Room_Access_Date__c,
        NDA_Sent_Date__c,NDA_Signed_Date__c,contactid, leadid from CampaignMember where campaignid in :LeadContIds.Keyset()]);
        List<CampaignMember> cmMemberListSelect = new List<CampaignMember>();
        for( CampaignMember cm : cmMemberList)
        {
            if(LeadContIds.get(cm.campaignid).contains(cm.contactid)|| LeadContIds.get(cm.campaignid).contains(cm.leadid))
            {
                Id contLeadId ;
                if(cm.contactid!=null)
                contLeadId=cm.contactid;
                else 
                contLeadId=cm.leadid;
                if(LeadContIds.containskey(cm.campaignid))
                  LeadContIds.get(cm.campaignid).add(contLeadId);
                else
                {
                    Set<ID> tempList = new Set<ID>();
                    tempList.add(contLeadId);
                    LeadContIds.put(cm.campaignid,tempList);
                }
                
                 if(LeadContCmemberMap.containskey(contLeadId))
                 {
                  LeadContCmemberMap.get(contLeadId).add(cm);
                 
                    LeadContIdsSelected.get(contLeadId).add(cm.campaignid);
                  
                 }
                else
                {
                    List<CampaignMember> tempList = new List<CampaignMember>();
                    tempList.add(cm);
                    LeadContCmemberMap.put(contLeadId,tempList);
                    Set<ID> tempSet = new Set<ID>();
                    tempSet.add(cm.campaignid);
                    LeadContIdsSelected.put(contLeadId,tempSet);
                }
                
                cmMemberListSelect.add(cm);
                LeadContactIDSet.add(contLeadId);
            }
        }
        system.debug('--hormese--'+cmMemberListSelect);
        List<Task> taskList= new List<Task>();
        List<ActivityHistory> activityList= new List<ActivityHistory>();
        List<Event> eventList= new List<Event>();
        Map<ID,Map<Date,String>> mapContIdDate = new  Map<ID,Map<Date,String>>();
        Map<ID,Map<Date,String>> mapCampIdDate = new  Map<ID,Map<Date,String>>();
        List<Contact> contList = [Select id, (Select id, description,ActivityDate,whoid,WhatId,Type, Owner.FirstName, Owner.LastName  from Tasks   order by ActivityDate),
        (Select id, description,ActivityDate,whoid,WhatId,Type, Owner.FirstName, Owner.LastName from Events order by ActivityDate),(Select id,WhatId,ActivityType, description,ActivityDate,whoid, Owner.Name from ActivityHistories order by ActivityDate)
        from contact where id in :LeadContactIDSet];
        
        List<Lead> LeadList = [Select id, (Select id,WhatId, description,ActivityDate,whoid,Type, Owner.FirstName, Owner.LastName from Tasks order by ActivityDate),
        (Select id, description,ActivityDate,WhatId,whoid,Type, Owner.FirstName, Owner.LastName from Events order by ActivityDate),(Select id,WhatId,ActivityType, description,ActivityDate,whoid, Owner.FirstName, Owner.LastName from ActivityHistories order by ActivityDate)
        from Lead where id in :LeadContactIDSet];
        
        for(Contact cnt :contList)
        {
            for(Task tsk : cnt.Tasks)
            {
                if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
                {
                 system.debug('Hormese3--- '+tsk);
                if(tsk.description==null)
                tsk.description='';
                if(mapContIdDate.containskey(cnt.id))
                {
                   if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate) && mapContIdDate.get(cnt.id).get(tsk.ActivityDate)!=null)
                   {
                   String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);
                   String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1);   
                  if(tsk.type!=null)                   
                   val +=' ;; '+ tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                   else
                    val +=' ;; '+ 'Task' +' - '+tsk.description + ' (' + nameconcat + ')';
                   mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
                   }
                   else
                   {  
                       
                       String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1); 
                       String str=  tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                       if(tsk.type==null)
                        str= 'Task ' +' - '+tsk.description + ' (' + nameconcat + ')';
                       mapContIdDate.get(cnt.id).put(tsk.ActivityDate,str);
                   }
                   
                }
                else
                {
                    Map<Date,String> keymap = new Map<Date,String>();
                    String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1);
                    String str=  tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                    if(tsk.type==null)
                        str= 'Task ' +' - '+tsk.description + ' (' + nameconcat + ')';
                    keymap.put(tsk.ActivityDate,str);
                    mapContIdDate.put(cnt.id,keymap);
                    
                }
                }
            }
            for(Event tsk : cnt.Events)
            {
                  if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
                  {
                 system.debug('Hormese2--- '+tsk);
                 if(tsk.description==null)
                tsk.description='';
                if(mapContIdDate.containskey(cnt.id))
                {
                   if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate)  && mapContIdDate.get(cnt.id).get(tsk.ActivityDate)!=null)
                   {
                   String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);
                   String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1);   
                    if(tsk.type!=null)
                   val +=' ;; '+ tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                   else
                    val +=' ;; '+ 'Meeting' +' - '+tsk.description + ' (' + nameconcat + ')';
                  
                   mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
                   }
                   else
                   {
                         String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1);
                         String str=  tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                           if(tsk.type==null)
                        str= 'Meeting ' +' - '+tsk.description + ' (' + nameconcat + ')';
                       mapContIdDate.get(cnt.id).put(tsk.ActivityDate,str);
                   }
                   
                }
                else
                {
                    Map<Date,String> keymap = new Map<Date,String>();
                    if(tsk.Owner.FirstName!=null && tsk.Owner.LastName!=null)
                    {
                     String nameconcat = tsk.Owner.FirstName.substring(0,1) + tsk.Owner.LastName.substring(0,1);
                     String str=  tsk.type +' - '+tsk.description + ' (' + nameconcat + ')';
                     if(tsk.type==null)
                        str= 'Task ' +' - '+tsk.description + ' (' + nameconcat + ')';
                        keymap.put(tsk.ActivityDate,str);
                        mapContIdDate.put(cnt.id,keymap);
                    }
                    
                }
                  }
            }
            //  for(ActivityHistory tsk : cnt.ActivityHistories)
            // {
            //      if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
            //      {
            //     system.debug('Hormese1--- '+tsk);
            //     String descrip =tsk.description;
            //      if(tsk.description==null)
            //     descrip='';
            //     if(mapContIdDate.containskey(cnt.id))
            //     {
            //       if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate)  && mapContIdDate.get(cnt.id).get(tsk.ActivityDate)!=null)
            //       {
            //       String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);  
            //         if(tsk.ActivityType!=null)
            //       val +=' ;; '+ tsk.ActivityType +' - '+descrip;
            //       else
            //         val +=' ;; '+ 'Task' +' - '+descrip;
                 
            //       // mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
            //       }
            //       else
            //       {
            //              String str=  tsk.ActivityType +' - '+descrip+' ';
            //               if(tsk.ActivityType==null)
            //             str= 'Task  ' +' - '+descrip+' ';
            //          //  mapContIdDate.get(cnt.id).put(tsk.ActivityDate,str);
            //       }
                   
            //     }
            //     else
            //     {
            //         Map<Date,String> keymap = new Map<Date,String>();
            //          String str=  tsk.ActivityType +' - '+descrip+' ';
            //         keymap.put(tsk.ActivityDate,str);
            //       // mapContIdDate.put(cnt.id,keymap);
                    
            //     }
            // }
            
            // }   
        }
        system.debug('Hormese--- '+mapContIdDate);
        //  for(Lead cnt :LeadList)
        // {
        //      for(Task tsk : cnt.Tasks)
        //     {
        //          if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
        //           {
        //          if(tsk.description==null)
        //         tsk.description='';
        //         if(mapContIdDate.containskey(cnt.id))
        //         {
        //           if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate))
        //           {
        //           String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);   
        //           if(tsk.type!=null)
        //           val +=' ;; '+ tsk.type +' - '+tsk.description;
        //           else
        //             val +=' ;; '+ 'Task' +' - '+tsk.description;
        //           mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
        //           }
        //           else
        //           {
        //                 String str=  tsk.type +' - '+tsk.description+' ';
        //                   if(tsk.type==null)
        //                 str= 'Task   ' +' - '+tsk.description+' ';
        //               mapContIdDate.get(cnt.id).put(tsk.ActivityDate,str);
        //           }
                   
        //         }
        //         else
        //         {
        //             Map<Date,String> keymap = new Map<Date,String>();
        //              String str=  tsk.type +' - '+tsk.description+' ';
        //              if(tsk.type==null)
        //                 str= 'Task ' +' - '+tsk.description+' ';
        //             keymap.put(tsk.ActivityDate,str);
        //             mapContIdDate.put(cnt.id,keymap);
                    
        //         }
                
        //           }
        //     }
        //     for(Event tsk : cnt.Events)
        //     {
        //          if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
        //           {
                      
        //          if(tsk.description==null)
        //         tsk.description='';
        //         if(mapContIdDate.containskey(cnt.id))
        //         {
        //           if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate))
        //           {
        //           String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);   
        //           if(tsk.type!=null)
        //           val +=' ;; '+ tsk.type +' - '+tsk.description;
        //           else
        //             val +=' ;; '+ ' Meeting ' +' - '+tsk.description;
        //           mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
        //           }
        //           else
        //           {
        //                 String str=  tsk.type +' - '+tsk.description+' ';
        //                  if(tsk.type==null)
        //                 str= 'Meeting   ' +' - '+tsk.description+' ';
        //               mapContIdDate.get(cnt.id).put(tsk.ActivityDate,tsk.description);
        //           }
                   
        //         }
        //         else
        //         {
        //             Map<Date,String> keymap = new Map<Date,String>();
        //              String str=  tsk.type +' - '+tsk.description+' ';
        //              if(tsk.type==null)
        //                 str= 'Task ' +' - '+tsk.description+' ';
        //             keymap.put(tsk.ActivityDate,str);
        //             mapContIdDate.put(cnt.id,keymap);
                    
        //         }
        //           }
        //     }
        //     //  for(ActivityHistory tsk : cnt.ActivityHistories)
        //     // {
        //     //       if(LeadContIdsSelected.containskey(cnt.id) && LeadContIdsSelected.get(cnt.id).contains(tsk.whatid))
        //     //       {
        //     //      String descrip =tsk.description;
        //     //      if(tsk.description==null)
        //     //     descrip='';
        //     //     if(mapContIdDate.containskey(cnt.id))
        //     //     {
        //     //       if(mapContIdDate.get(cnt.id).containskey(tsk.ActivityDate))
        //     //       {
        //     //       String val= mapContIdDate.get(cnt.id).get(tsk.ActivityDate);   
        //     //         if(tsk.ActivityType!=null)
        //     //       val +=' ;; '+ tsk.ActivityType +' - '+descrip;
        //     //       else
        //     //         val +=' ;; '+ ' Task ' +' - '+descrip;
        //     //       // mapContIdDate.get(cnt.id).put(tsk.ActivityDate,val);
        //     //       }
        //     //       else
        //     //       {
        //     //             String str=  tsk.ActivityType +' - '+descrip+' ';
        //     //              if(tsk.ActivityType==null)
        //     //             str= 'Task  ' +' - '+descrip+' ';
        //     //           // mapContIdDate.get(cnt.id).put(tsk.ActivityDate,str);
        //     //       }
                   
        //     //     }
        //     //     else
        //     //     {
        //     //         Map<Date,String> keymap = new Map<Date,String>();
        //     //          String str=  tsk.ActivityType +' - '+descrip+' ';
        //     //         keymap.put(tsk.ActivityDate,str);
        //     //         //mapContIdDate.put(cnt.id,keymap);
                    
        //     //     }
        //     // }
        //     // } 
        // }
        system.debug('--hormese--'+mapContIdDate);
        for(CampaignMember cm : cmMemberListSelect)
        
        {
            Id contLeadId;
            if(cm.contactid!=null)
            contLeadId=cm.contactid;
            else
            contLeadId=cm.leadid;
            if(mapContIdDate.containskey(contLeadId))
            mapCampIdDate.put(cm.id,mapContIdDate.get(contLeadId)); 
             if(mapContIdDate.containskey(contLeadId))
                {
                    
                     Map<Date,String> keymap = mapContIdDate.get(contLeadId);
                    
                    if(cm.Teaser_E_Blast_Sent_Date__c!=null && !keymap.containsKey(cm.Teaser_E_Blast_Sent_Date__c))
                    keymap.put(cm.Teaser_E_Blast_Sent_Date__c,'Teaser Sent');
                    else if(cm.Teaser_E_Blast_Sent_Date__c!=null && keymap.containsKey(cm.Teaser_E_Blast_Sent_Date__c))
                    {
                         String val =keymap.get(cm.Teaser_E_Blast_Sent_Date__c);
                        val+=';; Teaser Sent ';
                    
                    keymap.put(cm.Teaser_E_Blast_Sent_Date__c,val);
                    }
                    if(cm.Data_Room_Access_Date__c!=null && !keymap.containsKey(cm.Data_Room_Access_Date__c))
                    keymap.put(cm.Data_Room_Access_Date__c,' Data Room Access ');
                    else if(cm.Data_Room_Access_Date__c!=null && keymap.containsKey(cm.Data_Room_Access_Date__c))
                    {
                        String val =keymap.get(cm.Data_Room_Access_Date__c);
                        val+=';; Data Room Access ';
                    
                    keymap.put(cm.Data_Room_Access_Date__c,val);
                    }
                    if(cm.NDA_Sent_Date__c!=null && !keymap.containsKey(cm.NDA_Sent_Date__c))
                    keymap.put(cm.NDA_Sent_Date__c,' NDA Sent ');
                    else if(cm.NDA_Sent_Date__c!=null && keymap.containsKey(cm.NDA_Sent_Date__c))
                    {
                        String val =keymap.get(cm.NDA_Sent_Date__c);
                        val+=';; NDA Sent ';
                    
                    keymap.put(cm.NDA_Sent_Date__c,val);
                    }
                    if(cm.NDA_Signed_Date__c!=null && !keymap.containsKey(cm.NDA_Signed_Date__c))
                    keymap.put(cm.NDA_Signed_Date__c,' NDA Signed ');
                    else if(cm.NDA_Signed_Date__c!=null && keymap.containsKey(cm.NDA_Signed_Date__c))
                    {
                        String val =keymap.get(cm.NDA_Signed_Date__c);
                        val+=';; NDA Signed ';
                    
                    keymap.put(cm.NDA_Signed_Date__c,val);
                    }
                    mapCampIdDate.put(cm.id,keymap);
                    
                }
                else
                {
                  
                     Map<Date,String> keymap = new Map<Date,String>();
                    
                    if(cm.Teaser_E_Blast_Sent_Date__c!=null)
                    keymap.put(cm.Teaser_E_Blast_Sent_Date__c,'Teaser Sent');
                   
                    if(cm.Data_Room_Access_Date__c!=null && !keymap.containsKey(cm.Data_Room_Access_Date__c))
                    keymap.put(cm.Data_Room_Access_Date__c,' Data Room Access ');
                    else if(cm.Data_Room_Access_Date__c!=null && keymap.containsKey(cm.Data_Room_Access_Date__c))
                    {
                        String val =keymap.get(cm.Data_Room_Access_Date__c);
                        val+=';; Data Room Access ';
                    
                    keymap.put(cm.Data_Room_Access_Date__c,val);
                    }
                    if(cm.NDA_Sent_Date__c!=null && !keymap.containsKey(cm.NDA_Sent_Date__c))
                    keymap.put(cm.NDA_Sent_Date__c,' NDA Sent ');
                    else if( cm.NDA_Sent_Date__c!=null && keymap.containsKey(cm.NDA_Sent_Date__c))
                    {
                        String val =keymap.get(cm.NDA_Sent_Date__c);
                        val+=';; NDA Sent ';
                    
                    keymap.put(cm.NDA_Sent_Date__c,val);
                    }
                    if(cm.NDA_Signed_Date__c!=null && !keymap.containsKey(cm.NDA_Signed_Date__c))
                    keymap.put(cm.NDA_Signed_Date__c,' NDA Signed ');
                    else if(cm.NDA_Signed_Date__c!=null && keymap.containsKey(cm.NDA_Signed_Date__c))
                    {
                        String val =keymap.get(cm.NDA_Signed_Date__c);
                        val+=';; NDA Signed ';
                    
                    keymap.put(cm.NDA_Signed_Date__c,val);
                    }
                    
                   
                    
                   
                    mapCampIdDate.put(cm.id,keymap);
                     system.debug('--hormese--'+mapCampIdDate);
                    
                }
        }
        
        for(CampaignMember cm : cmMemberListSelect)
        {
            
            
            if(mapCampIdDate.containsKey(cm.id))
            {
                 Map<Date,String> keymap = mapCampIdDate.get(cm.id);
                 String str='';
                 Set<Date> dateKeySet =  keymap.keyset();
                 List<Date> dateList=  new List< Date> ();
                 for(Date dt :dateKeySet )
                 {
                     dateList.add(dt);
                 }
                 dateList.sort();
                 List<Date> finalList = new List<Date>();

                    for(Integer i = dateList.size()-1; i>=0;i--)
                    
                    {
                    
                        finalList.add(dateList.get(i));
                    
                    }

                 
                for(Date dt : finalList)
                {
                   if(dt!=null &&keymap.containsKey(dt)&& keymap.get(dt)!=''&& keymap.get(dt)!=null )
                   {
                      String tempdt= dt.day() + '/' + dt.month() + '/' + dt.year();
                    
                    List<String> strList = keymap.get(dt).split(';;');
                    for(String stritem : strList)
                    {
                          str+= tempdt+' - '+stritem ;
                            str+='\n';
                        
                    }
                  
                   }
                }
                str.removeEnd(',');
                str= str.replace('00:00:00', '');
                cm.History__c=str;
            }
        }
   //     LeadContCmemberMap
      
        update cmMemberListSelect;
    
    
}

}