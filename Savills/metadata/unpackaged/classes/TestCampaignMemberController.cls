@isTest
private class TestCampaignMemberController {
    
static testMethod void CampaignMember(){
    
        //boolean selectAll= 'false';
        integer noOfRows =1;
       //boolean showerror = 'TRUE';
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact cont          = new Contact();
        cont.LastName          = 'Test Contact';
        cont.FirstName         = 'jhon';
        cont.Accountid         = a.id ;
        cont.Title             = 'For testing';
        cont.UK_Interest__c=true;
        cont.Target_UK_Region_Contact__c='London;Scotland';
        cont.apac_Interest__c=true;
        cont.Target_APAC_Region__c='Australia;India';
        cont.European_Interest__c=true;
        cont.Target_European_Region__c='Ireland;France';
        cont.Americas_Interest__c=true;
        cont.Target_Americas_Region__c='USA;CANADA';
        cont.Middle_East_Interest__c=true;
        cont.Target_ME_Region__c='Middle East';
        cont.Africa_Interest__c=true;
        cont.Target_Africa_Region__c='Northern Africa';
        cont.Target_Spa__c=true;
        cont.Target_Conference__c=true;
        cont.Target_Ski__c=true;
        cont.Target_Development__c=true;
        cont.Target_Ski__c=true;
        cont.Target_Development__c=true;
        cont.Target_Golf__c=true;
        cont.Target_Location_Type__c='urban';
        cont.Target_Hotel_Grade__c='Luxury';
        cont.SizeRqrd_51_200_Bedrooms__c=true;
        cont.Target_Branded_Unbranded__c='Branded';
        cont.SizeRqrd_200_Bedrooms__c=true;
        cont.SizeRqrd_51_200_Bedrooms__c=true;
        insert cont;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.City__c  = 'My city';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.Location_Type__c = 'Rural';
        pr.Description__c = 'testing Agency listing browser property';
        pr.GIA_Sq_Ft__c = 42;
        pr.Sub_Region__c = 'TBC';
        pr.Region__c = 'UK';
        pr.Spa__c = TRUE;
        pr.Conference__c = TRUE;
        pr.Ski__c = TRUE;
        pr.Development__c = TRUE;
        pr.Golf__c = TRUE;
        pr.Parking__c = TRUE;
        pr.Grade_Star_Rating__c = 'LUXURY';
        pr.Room_Count__c = 4;
        insert pr;
    
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        port.Portfolio_Description__c = 'testing Agency listing browser portfolio';
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = cont.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.Primary_Contact__c= cont.id;
        opp.Operating_Structure__c = 'Ground Rent';
        opp.CloseDate           = System.today();
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        opp.purchaser_contact__c         = cont.id;        
        insert opp;
    
       Properties_of_Interest__c p11=new Properties_of_Interest__c();
       p11.Opportunity__c = opp.id;
       p11.Property__C = pr.id;
       insert p11;
    
    
        Task t           = new Task();
        t.OwnerId        = UserInfo.getUserId();
        t.Subject        ='Donni';
        t.Status         ='Not Started';
        t.Priority       ='Normal';
        t.Description    ='test to increase the coverage';
        t.Type           = 'Call';
        t.WhatId         = a.id;
        t.WhoId          = cont.id;
        insert t;
        
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.Description   ='testing is on process';
        eve.WhatId        = a.id;
        eve.WhoId         = cont.id;
        
        insert eve;

      
        Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        insert cap;
    
       list<CampaignMember> capmlist = new  list<CampaignMember>();
    
        CampaignMember capm              = new CampaignMember();
        capm.Status                      = 'Contacted';
        capm.CampaignId                  =  cap.id;
        capm.Teaser_E_Blast_Sent_Date__c = System.today();
        capm.Data_Room_Access_Date__c    = System.today();
        capm.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);
        capm.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);
        capm.ContactId                   = cont.id;        
        capm.LeadId                      = l.id;
    
        capmlist.add(capm);
       insert capm;
    
    PageReference Pageref = page.CampaignMember;
     test.setCurrentPage(Pageref);
    ApexPages.currentPage().getParameters().put('id',capm.id);
    
    ApexPages.StandardController sc = new ApexPages.standardController(capm);
    
 CampaignMemberController obj1  = new CampaignMemberController(SC);  
      

      obj1.createRecords(); 
       obj1.addRow();
       obj1.rowNo = '0';
        obj1.delRow();
       obj1.selectAll();
       obj1.deleteSelectedRows();
       
    Pagereference  ref = obj1.saveCampaignMember();
    Pagereference  ref1 = obj1.checkduplicates();
    
}
    static testMethod void CampaignMember1(){
    
        //boolean selectAll= 'false';
        integer noOfRows =1;
       //boolean showerror = 'TRUE';
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact cont          = new Contact();
        cont.LastName          = 'Test Contact';
        cont.FirstName         = 'jhon';
        cont.Accountid         = a.id ;
        cont.Title             = 'For testing';
        cont.UK_Interest__c=true;
        cont.Target_UK_Region_Contact__c='London;Scotland';
        cont.apac_Interest__c=true;
        cont.Target_APAC_Region__c='Australia;India';
        cont.European_Interest__c=true;
        cont.Target_European_Region__c='Ireland;France';
        cont.Americas_Interest__c=true;
        cont.Target_Americas_Region__c='USA;CANADA';
        cont.Middle_East_Interest__c=true;
        cont.Target_ME_Region__c='Middle East';
        cont.Africa_Interest__c=true;
        cont.Target_Africa_Region__c='Northern Africa';
        cont.Target_Spa__c=true;
        cont.Target_Conference__c=true;
        cont.Target_Ski__c=true;
        cont.Target_Development__c=true;
        cont.Target_Ski__c=true;
        cont.Target_Development__c=true;
        cont.Target_Golf__c=true;
        cont.Target_Location_Type__c='urban';
        cont.Target_Hotel_Grade__c='Luxury';
        cont.SizeRqrd_51_200_Bedrooms__c=true;
        cont.Target_Branded_Unbranded__c='Branded';
        cont.SizeRqrd_200_Bedrooms__c=true;
        cont.SizeRqrd_51_200_Bedrooms__c=true;
        insert cont;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.City__c  = 'My city';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.Location_Type__c = 'Rural';
        pr.Description__c = 'testing Agency listing browser property';
        pr.GIA_Sq_Ft__c = 42;
        pr.Sub_Region__c = 'TBC';
        pr.Region__c = 'UK';
        pr.Spa__c = TRUE;
        pr.Conference__c = TRUE;
        pr.Ski__c = TRUE;
        pr.Development__c = TRUE;
        pr.Golf__c = TRUE;
        pr.Parking__c = TRUE;
        pr.Grade_Star_Rating__c = 'LUXURY';
        pr.Room_Count__c = 4;
        insert pr;
    
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        port.Portfolio_Description__c = 'testing Agency listing browser portfolio';
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = cont.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.Primary_Contact__c= cont.id;
        opp.Operating_Structure__c = 'Ground Rent';
        opp.CloseDate           = System.today();
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        opp.purchaser_contact__c         = cont.id;        
        insert opp;
    
       Properties_of_Interest__c p11=new Properties_of_Interest__c();
       p11.Opportunity__c = opp.id;
       p11.Property__C = pr.id;
       insert p11;
    
    
        Task t           = new Task();
        t.OwnerId        = UserInfo.getUserId();
        t.Subject        ='Donni';
        t.Status         ='Not Started';
        t.Priority       ='Normal';
        t.Description    ='test to increase the coverage';
        t.Type           = 'Call';
        t.WhatId         = a.id;
        t.WhoId          = cont.id;
        insert t;
        
        Event eve         = new Event();
        eve.OwnerId       = UserInfo.getUserId();
        eve.Subject       = 'Test';
        eve.EndDateTime   =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.StartDateTime =  datetime.newInstance(2014, 9, 15, 12, 30, 0);
        eve.Description   ='testing is on process';
        eve.WhatId        = a.id;
        eve.WhoId         = cont.id;
        
        insert eve;

      
        Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        insert cap;
    
       list<CampaignMember> capmlist = new  list<CampaignMember>();
    
        CampaignMember capm              = new CampaignMember();
        capm.Status                      = 'Contacted';
        capm.CampaignId                  =  cap.id;
        capm.Teaser_E_Blast_Sent_Date__c = System.today();
        capm.Data_Room_Access_Date__c    = System.today();
        capm.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);
        capm.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);
        capm.ContactId                   = cont.id;        
        capm.LeadId                      = l.id;
    
        capmlist.add(capm);
       insert capm;
    
    PageReference Pageref = page.CampaignMember;
     test.setCurrentPage(Pageref);
    ApexPages.currentPage().getParameters().put('id',capm.id);
    
    ApexPages.StandardController sc = new ApexPages.standardController(capm);
    
 CampaignMemberController obj1  = new CampaignMemberController(SC);  
      

      obj1.createRecords(); 
       obj1.addRow();
      
       obj1.selectAll();
       obj1.deleteSelectedRows();
       
    Pagereference  ref = obj1.saveCampaignMember();
    Pagereference  ref1 = obj1.checkduplicates();
    
}
}