@isTest
private class Dq_TestingPortfolioHandler {
    
    static testMethod void validateOpportunity(){
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
         
    
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      

        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
   
   
         
    

 ApexPages.StandardController sc = new ApexPages.StandardController(cap);
        
        
            CampaignMemberController instance = new CampaignMemberController(sc);

        CampaignMemberController.booleanProjectWrapper bjwrap = new CampaignMemberController.booleanProjectWrapper(true,capm1);
        bjwrap.isSelected=true;
        bjwrap.objcm=capm1;
        bjwrap.ValuetoList=1;
        instance.lstOfbooleanProjectWrapper = new List<CampaignMemberController.booleanProjectWrapper>();
        instance.lstOfbooleanProjectWrapper.add(bjwrap);
        instance.RowNo='0';
        instance.createRecords();
        instance.addRow();
         instance.selectAll();
         instance.deleteSelectedRows();
         instance.delRow() ;
         instance.saveCampaignMember();
         instance.checkduplicates();
         
         
       
        
    }
     static testMethod void validateOpportunity2(){
        
       
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
        
        Property_Portfolio_Link__c ppc = new Property_Portfolio_Link__c();
        ppc.Property__c=pr.id;
        ppc.Portfolio__c= port.id;
        
        insert ppc;
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
         
    
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      

        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
   
   
         
    

 ApexPages.StandardController sc = new ApexPages.StandardController(port);
        
        
            PropertyPortfolioLinksController instance = new PropertyPortfolioLinksController(sc);

        PropertyPortfolioLinksController.booleanProjectWrapper bjwrap = new PropertyPortfolioLinksController.booleanProjectWrapper(true,ppc);
        bjwrap.isSelected=true;
        bjwrap.objport=ppc;
        bjwrap.ValuetoList=1;
        instance.lstOfbooleanProjectWrapper = new List<PropertyPortfolioLinksController.booleanProjectWrapper>();
        instance.lstOfbooleanProjectWrapper.add(bjwrap);
        instance.RowNo='0';
        instance.createRecords();
        instance.addRow();
         instance.selectAll();
         instance.deleteSelectedRows();
         instance.delRow() ;
         instance.savePortfolioLink();
         instance.checkduplicates();
         
        
        
        
     }
     
       static testMethod void validateOpportunity3(){
        
       
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
      
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
         
           
        Portfolio_Link__c ppc = new Portfolio_Link__c();
        ppc.Opportunity__c=opp.id;
       
        
        insert ppc;
    
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      

        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
   
   
         
    

 ApexPages.StandardController sc = new ApexPages.StandardController(port);
        
        
            PortfolioLinksController instance = new PortfolioLinksController(sc);

        PortfolioLinksController.booleanProjectWrapper bjwrap = new PortfolioLinksController.booleanProjectWrapper(true,ppc);
        bjwrap.isSelected=true;
        bjwrap.objport=ppc;
        bjwrap.ValuetoList=1;
        instance.lstOfbooleanProjectWrapper = new List<PortfolioLinksController.booleanProjectWrapper>();
        instance.lstOfbooleanProjectWrapper.add(bjwrap);
        instance.RowNo='0';
        instance.createRecords();
        instance.addRow();
         instance.selectAll();
         instance.deleteSelectedRows();
         instance.delRow() ;
         instance.savePortfolioLink();
         instance.checkduplicates();
         
        
        
        
     }
     
      static testMethod void validateOpportunity4(){
        
       
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact c1           = new Contact();
        c1.LastName          = 'Test Contact';
        c1.Accountid         = a.id ;
         
        insert c1;
        Contact c2           = new Contact();
        c2.LastName          = 'Test Contact';
        c2.Accountid         = a.id ;
         
        insert c2;
        
        PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        
        insert port;
        
      
        
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = c1.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = pr.id;
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = 3000;
        
         insert opp;
         
           
        Work_In_Hand__c ppc = new Work_In_Hand__c();
        ppc.Opportunity__c=opp.id;
         ppc.Client_Lookup__c=a.id;
         ppc.Client__c='dfgfh';
       
        
        insert ppc;
    
         Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        
        insert cap;
      

        CampaignMember capm1              = new CampaignMember();
        capm1.Status                      = 'Contacted';
        capm1.CampaignId                  =  cap.id;
        capm1.Teaser_E_Blast_Sent_Date__c = System.today();
        capm1.Data_Room_Access_Date__c    = System.today();
        capm1.NDA_Sent_Date__c            = date.newInstance(2017, 04, 15);

        capm1.NDA_Signed_Date__c          = date.newInstance(2017, 04, 16);

        capm1.ContactId                   = c1.id;        
        capm1.LeadId                      = l.id;
       
        insert capm1;
   
   
         
    

 ApexPages.StandardController sc = new ApexPages.StandardController(port);
        
        
            WorkinHandController instance = new WorkinHandController(sc);
             instance.OpportunityId=opp.id;
             instance = new WorkinHandController(sc);

        WorkinHandController.booleanProjectWrapper bjwrap = new WorkinHandController.booleanProjectWrapper(true,ppc);
        bjwrap.isSelected=true;
        bjwrap.objwih=ppc;
        bjwrap.ValuetoList=1;
        instance.listOpportunity.add(opp);
        instance.lstOfbooleanProjectWrapper = new List<WorkinHandController.booleanProjectWrapper>();
        instance.lstOfbooleanProjectWrapper.add(bjwrap);
        instance.RowNo='0';
       
        instance.createRecords();
        instance.addRow();
         instance.selectAll();
         instance.deleteSelectedRows();
         instance.delRow() ;
         instance.saveWorkinHand();
     //    instance.checkduplicates();
         
        
        
        
     }
        
    }