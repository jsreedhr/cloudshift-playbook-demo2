public without sharing class PropertyPortfolioLinksController {

	public list<Property_Portfolio_Link__c> listPortLinks{get; set;}
    public list<Portfolio__c> listPortfolio{get; set;}
    public string rowNo{get;set;}
    Public String PortfolioId;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
    //constructor to grt the records
    
    public PropertyPortfolioLinksController(apexpages.standardController stdController){
        //projectDuration=1;
    
        listPortfolio = new list<Portfolio__c>();
        listPortLinks= new list<Property_Portfolio_Link__c>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        PortfolioId = apexpages.currentpage().getparameters().get('id');
        listPortfolio = [select name ,id from Portfolio__c where id=: PortfolioId limit 1];
        listPortLinks = [select Id, Portfolio__r.id, Property__c, Portfolio__r.name from Property_Portfolio_Link__c where Portfolio__c =: PortfolioId limit 4999];
      
        for(Property_Portfolio_Link__c obj_portlink :listPortLinks){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_portlink));
            
        }
        
    }  
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        Property_Portfolio_Link__c objportlink = new Property_Portfolio_Link__c();
         system.debug('######------------++++' + l);
     
        objportlink.Portfolio__c = PortfolioId;
        objportlink.Property__c = lstOfbooleanProjectWrapper[l].objport.Property__c;
        
        listPortLinks.add(objportlink);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objportlink));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new portfolio link record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        Property_Portfolio_Link__c objportlink = new Property_Portfolio_Link__c();
        objportlink.Portfolio__c = PortfolioId;
       
        listPortLinks.add(objportlink);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objportlink));
        }
        
        
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        list<Property_Portfolio_Link__c> toDeleteRows = new list<Property_Portfolio_Link__c>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objport.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<Property_Portfolio_Link__c> todelete = new list<Property_Portfolio_Link__c>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    savePortfolioLink 
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference savePortfolioLink() {
        if(showerror !=true)
        {
        list<Property_Portfolio_Link__c> ListtoUpsert= new list<Property_Portfolio_Link__c> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        
         for(booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper ){
         
         
            if(objportlink.objport.Property__c != null && string.valueof(objportlink.objport.Portfolio__c) != null)
            {
           
                 ListtoUpsert.add(objportlink.objport);   
            }
          
                else if(objportlink.objport.Property__c != null || string.valueof(objportlink.objport.Portfolio__c) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }
        
    
        
        
        
        
    
        
        
        try{
            Upsert ListtoUpsert;
        } catch(DmlException e) {
        }
    
          PageReference acctPage = new PageReference('/' + PortfolioId );
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
    
    public void checkduplicates()
    {
        Set<ID> dupcheckset = new Set<ID> ();
            showerror = false;
        errorPos= 0;
        Integer i=0;
        for(booleanProjectWrapper bwrap : lstOfbooleanProjectWrapper)
        {   
           
        if(bwrap.objport.Property__c!=null && !dupcheckset.contains(bwrap.objport.Property__c))
        {
           dupcheckset.add(bwrap.objport.Property__c); 
        }
        else if(bwrap.objport.Property__c!=null && dupcheckset.contains(bwrap.objport.Property__c))
        {
            showerror =true;
            errorPos=i;
            return;
            
        }
         i++;
         
        
    }
        
    }
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public Property_Portfolio_Link__c objport{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, Property_Portfolio_Link__c objports){
          objport = objports;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}