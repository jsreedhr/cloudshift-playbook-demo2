public with sharing class PortfolioLinksController{
    public list<Portfolio_Link__c> listPortLinks{get; set;}
    public list<opportunity> listOpportunity{get; set;}
    public string rowNo{get;set;}
    Public String OpportunityId;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
    //constructor to grt the records
    
    public PortfolioLinksController(apexpages.standardController stdController){
        //projectDuration=1;
    
        listOpportunity = new list<opportunity>();
        listPortLinks= new list<Portfolio_Link__c>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        OpportunityId = apexpages.currentpage().getparameters().get('id');
        listOpportunity= [select name ,id, Account.name from opportunity where id=: OpportunityId limit 1];
        listPortLinks = [select Id, Allocation__c, Opportunity__r.id,Property__c, Room_Count__c, Location_Type__c, CurrencyIsoCode, Grade_Star_Rating__c, City__c, Country__c, Opportunity__r.name from Portfolio_Link__c where Opportunity__c =: OpportunityId limit 4999/*order by Year__c,Month__c limit 4999 */];
      
        for(Portfolio_Link__c obj_portlink :listPortLinks){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_portlink));
            
        }
        
    }  
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        Portfolio_Link__c objportlink = new Portfolio_Link__c();
         system.debug('######------------++++' + l);
     
        objportlink.Opportunity__c= OpportunityId;
        objportlink.Property__c = lstOfbooleanProjectWrapper[l].objport.Property__c;
        
        listPortLinks.add(objportlink);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objportlink));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new portfolio link record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        Portfolio_Link__c objportlink = new Portfolio_Link__c();
        objportlink.Opportunity__c= OpportunityId;
       
       //objportlink.Week_Commencing__c = System.today().toStartofWeek().adddays(1);
        listPortLinks.add(objportlink);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objportlink));
        }
        
        
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        list<Portfolio_Link__c> toDeleteRows = new list<Portfolio_Link__c>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objport.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objport);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<Portfolio_Link__c> todelete = new list<Portfolio_Link__c>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objport);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    savePortfolioLink 
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference savePortfolioLink() {
        if(showerror !=true)
        {
        list<Portfolio_Link__c> ListtoUpsert= new list<Portfolio_Link__c> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        
         for(booleanProjectWrapper objportlink: lstOfbooleanProjectWrapper ){
         
         
            if(objportlink.objport.Property__c != null && string.valueof(objportlink.objport.Opportunity__c) != null)
            {
           
                 ListtoUpsert.add(objportlink.objport);   
            }
          
                else if(objportlink.objport.Property__c != null || string.valueof(objportlink.objport.Opportunity__c) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }
        
    
        
        
        
        
    
        
        
        try{
            Upsert ListtoUpsert;
        } catch(DmlException e) {
        }
    
          PageReference acctPage = new PageReference('/' + OpportunityId );
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
    
    public void checkduplicates()
    {
        Set<ID> dupcheckset = new Set<ID> ();
            showerror = false;
        errorPos= 0;
        Integer i=0;
        for(booleanProjectWrapper bwrap : lstOfbooleanProjectWrapper)
        {   
           
        if(bwrap.objport.Property__c!=null && !dupcheckset.contains(bwrap.objport.Property__c))
        {
           dupcheckset.add(bwrap.objport.Property__c); 
        }
        else if(bwrap.objport.Property__c!=null && dupcheckset.contains(bwrap.objport.Property__c))
        {
            showerror =true;
            errorPos=i;
            return;
            
        }
         i++;
         
        
    }
        
    }
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public Portfolio_Link__c objport{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, Portfolio_Link__c objports){
          objport = objports;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}