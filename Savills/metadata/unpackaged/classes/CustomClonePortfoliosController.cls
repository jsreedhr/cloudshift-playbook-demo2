public without sharing class CustomClonePortfoliosController {

    private ApexPages.StandardController standardController;
    
    public Id portid {get;set;}
    
    public CustomClonePortfoliosController(ApexPages.StandardController standardController){
    
        this.standardController = standardController;
        // Fields being cloned
     	if(!test.isRunningTest()){this.standardController.addFields(new List<String> { 'Name', 'id', 'Country_Picklist__c', 'Portfolio_Description__c', 'OwnerId'});}
        portid = standardController.getRecord().Id;

    }
   
    public PageReference clonePortfolio(){
        // Record being cloned
        Portfolio__c currentportfolio = (Portfolio__c) standardController.getRecord();
     
        List <Property_Portfolio_Link__c> lst_pfs = new List <Property_Portfolio_Link__c>([SELECT Portfolio__c, Property__c FROM Property_Portfolio_Link__c WHERE Portfolio__c =: portid]);
        
        // Custom clone logic
        Portfolio__c clonedportfolio = new Portfolio__c();
        clonedportfolio.Name = currentportfolio.Name + ' - Clone';
        clonedportfolio.Country_Picklist__c = currentportfolio.Country_Picklist__c;
        clonedportfolio.Portfolio_Description__c = currentportfolio.Portfolio_Description__c;
        //clonedportfolio.Ownerid = currentportfolio.Ownerid;
        insert clonedportfolio;
        
        IF(!lst_pfs.isEmpty()){
           List <Property_Portfolio_Link__c> clonedLinks = new List <Property_Portfolio_Link__c>();
           for(Property_Portfolio_Link__c pfexisting: lst_pfs){
                Property_Portfolio_Link__c pfnew = new Property_Portfolio_Link__c();
                pfnew.Property__c = pfexisting.Property__c;
                pfnew.Portfolio__c = clonedportfolio.Id;
                clonedLinks.add(pfnew);
            }
            insert clonedLinks;
        }
        
        // Redirect to the new cloned record
        return new PageReference('/'+clonedportfolio.Id);
    }
      
}