@isTest
private class dq_redirectOpportunitytest {

    private static testMethod void test() {
        
        Lead l             = new Lead();
        l.LastName         = 'Swain';
        l.Company          = 'Test Company';
        l.Status           = 'Qualified';
       
        insert l;
        
        Account a           = new Account();
        a.Name              = 'Test Account';
        a.Account_Type__c   = 'Agent';
        
        insert a;
        
        Contact cont          = new Contact();
        cont.LastName          = 'Test Contact';
        cont.FirstName         = 'jhon';
        cont.Accountid         = a.id ;
        cont.Title             = 'For testing';
         
        insert cont;
        
       PropertyHOTELS__c pr    = new PropertyHOTELS__c();
        pr.Name                 = 'test Property';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.City__c  = 'My city';
        pr.Country_Picklist__c  ='United Kingdom';
        pr.Location_Type__c = 'Rural';
        pr.Description__c = 'testing Agency listing browser property';
        pr.GIA_Sq_Ft__c = 42;
        pr.Sub_Region__c = 'TBC';
        pr.Region__c = 'UK';
        pr.Spa__c = TRUE;
        pr.Conference__c = TRUE;
        pr.Ski__c = TRUE;
        pr.Development__c = TRUE;
        pr.Golf__c = TRUE;
        pr.Parking__c = TRUE;
        pr.Grade_Star_Rating__c = 'LUXURY';
        pr.Room_Count__c = 4;
        insert pr;
        
        Portfolio__c port    = new Portfolio__c();
        port.Name          = 'Test Portfolios';
        port.Portfolio_Description__c = 'testing Agency listing browser portfolio';
        
        insert port;
        Campaign cap      = new Campaign();
        cap.Name          ='Test Campaign';
        cap.Type          ='Agency-SellSide';
        cap.Status        ='Aborted';
        insert cap;
    
        Opportunity opp         = new Opportunity();
        opp.LeadSource          = 'Client_Relationship__c';
        opp.Date_Opened__c      = System.today();
        opp.Name                = 'Test Opportunity';
        opp.Accountid             = a.id;
        opp.Primary_Contact__c  = cont.id;
        opp.StageName           = 'Pitch';
        opp.Country__c          = 'UK';
        opp.Property__c         = Null;
        opp.Operating_Structure__c = 'Ground Rent';
        opp.CloseDate           = System.today();
        opp.Fee_Share_To_Total__c = 1000;
        opp.Guide_Price__c              = 2000;
        opp.Estimated_Sale_Price__c     = Null;
        opp.CampaignId                  =  NuLL;
        opp.Portfolio__c                = port.id;
         insert opp;
  
    ApexPages.currentPage().getParameters().put('id',opp.id);
    
    ApexPages.StandardController sc = new ApexPages.standardController(opp);

     dq_redirectOpportunity rec  = new dq_redirectOpportunity(sc);
    
      rec.loadMessages();
    
    
     
       

    }

}