@isTest
public class Test_RejectLicense {
    
    private static testMethod void Test_RejectLicense(){
       
       Test.startTest();
       License_Request__c l = new License_Request__c();
       l.First_Name__c = 'Ben';
       l.Last_Name__c = 'Stokes';
       l.Email__c = 'benstokes' + 1 + '@gmail.com';
       l.Timezone__c = 'Europe/London';
       l.Assistant_Email__c = 'barry@gmail.com';
       l.CurrencyIsoCode = 'GBP';
       l.Profile__c = 'System Administrator';
	   insert l;
        
       PageReference Pageref = new PageReference('/' + l.Id);
       Pageref.getParameters().put('id',l.id);

       test.setCurrentPage(Pageref);   
    
       ApexPages.StandardController sc = new ApexPages.standardController(l);

       RejectLicense rec  = new RejectLicense(sc);
       rec.loadMessages();
       Test.stopTest();
    }
}