public without sharing class ApproveLicense{
    public boolean redirectBoolean {get;set;}
    License_Request__c lr;
    public ApproveLicense(ApexPages.StandardController stdController)
    {
        lr= (License_Request__c)stdController.getRecord();
        
    }
    public void loadMessages()
    {
        
        List<License_Request__c> lst_lrs = [SELECT  id, Approved__c FROM License_Request__c WHERE id = :lr.id ];
        
        try
        {
            if(lst_lrs != null){
                
                for(License_Request__c l : lst_lrs){
                    l.Approved__c=true;
                }
                update lst_lrs;
                redirectBoolean=true;
            }
        }
        catch(Exception e){
            redirectBoolean=false;
        }
    } 
}