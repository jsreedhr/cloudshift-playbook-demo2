public class dQ_AgencyListingBrowserClass {
    
    public List<Contact> contactList {get;set;}
    public List<Contact> contactListSelected {get;set;}
    public SearchWrapper swrap {get;set;}
    public Opportunity opp{get;set;}
     public Campaign cap{get;set;}
    public List<SelectOption> Branding { get; set; }
    public List<SelectOption> Location { get; set; }
    public List<SelectOption> rating { get; set; }
    public List<SelectOption> rooms { get; set; }
    public List <SelectOption> price { get; set; }
    
    
    public List<SelectOption> ukSubRegion { get; set; }
    public List<SelectOption> europeSubRegion { get; set; }
    public List<SelectOption> africaSubRegion { get; set; }
    public List<SelectOption> middleEastSubRegion { get; set; }
    public List <SelectOption> americasSubRegion { get; set; }
    public List <SelectOption> apacSubRegion { get; set; }
    
    public String contIdsStr { get; set; }
    public string descheader{ get; set; }
    public Decimal roomcount{ get; set; }
    public boolean  conference{ get; set; }
     public boolean  spa{ get; set; }
      public boolean  ski{ get; set; }
       public boolean  development{ get; set; }
        public boolean  parking{ get; set; }
        public  decimal convertedAmount{ get; set; }
        public  decimal convertedAmountGuidePrice{ get; set; }
        Set<id> contid ;
    public dQ_AgencyListingBrowserClass()
    {
        String strOpportunityID;
         if(ApexPages.currentPage().getParameters().containsKey('id'))
            strOpportunityID = ApexPages.currentPage().getParameters().get('id');
            if(strOpportunityID!=null)
           opp = [Select id,name,currencyisocode, Estimated_Sale_Price__c, Portfolio__c,Portfolio__r.name,Guide_Price__c,Portfolio__r.Portfolio_Description__c, Operating_Structure__c, Property__r.name,Campaignid,Property__r.City__c,Property__r.Country_Picklist__c, Property__r.Location_Type__c,
           Property__r.Description__c, Property__r.GIA_Sq_Ft__c ,
            Property__r.Sub_Region__c,  Property__r.Region__c, Property__r.Brand_Lookup__c,
            Property__r.Spa__c,Property__r.Conference__c,Property__r.Ski__c,Property__r.Development__c,Property__r.Golf__c,
           Property__r.Parking__c,  Property__r.Grade_Star_Rating__c,Property__r.Room_Count__c
           from Opportunity where id=:strOpportunityID Limit 1];
        
       
    
         Branding = new List<SelectOption>();
        
         Location= new List<SelectOption>();
        rating = new List<SelectOption>();
         rooms= new List<SelectOption>();
         price= new List<SelectOption>();
        
        ukSubRegion = new List<SelectOption>();
        europeSubRegion = new List<SelectOption>();
        africaSubRegion = new List<SelectOption>();
        middleEastSubRegion = new List<SelectOption>();
        americasSubRegion = new List<SelectOption>();  
        apacSubRegion = new List<SelectOption>();  
        
        
         contactList  = new List<Contact>();
        swrap = new SearchWrapper();
        List<Schema.Picklistentry>brandingPicklist = Contact.Target_Branded_Unbranded__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>loctionPicklist = Contact.Target_Location_Type__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>ratingPicklist = Contact.Target_Hotel_Grade__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>pricingPicklist = Contact.Target_Price_Band__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>roomsPicklist = Contact.Target_Size__c.getDescribe().getpicklistValues();
        
        List<Schema.Picklistentry>ukPicklist = Contact.Target_UK_Region_Contact__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>africaPicklist = Contact.Target_Africa_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>americaPicklist = Contact.Target_Americas_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>middlEastPicklist = Contact.Target_ME_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>europePicklist = Contact.Target_European_Region__c.getDescribe().getpicklistValues();
        List<Schema.Picklistentry>apacPicklist = Contact.Target_APAC_Region__c.getDescribe().getpicklistValues();
        
        for(Schema.Picklistentry fld : ukPicklist)
        {
            ukSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : africaPicklist)
        {
            africaSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : americaPicklist)
        {
            americasSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : middlEastPicklist)
        {
            middleEastSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        for(Schema.Picklistentry fld : apacPicklist)
        {
            apacSubRegion.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : europePicklist)
        {
            europeSubRegion.add(new SelectOption(fld.value,fld.value));
        }
        
        for(Schema.Picklistentry fld : brandingPicklist)
        {
            Branding.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : loctionPicklist)
        {
            Location.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : ratingPicklist)
        {
            rating.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : pricingPicklist)
        {
            price.add(new SelectOption(fld.value,fld.value));
        }
         for(Schema.Picklistentry fld : roomsPicklist)
        {
            rooms.add(new SelectOption(fld.value,fld.value));
        }
        
        if(opp.Portfolio__c==null){
        buildInitialwrapper();
        }
        buildheaderbar();
         contactListSelected= new List<Contact>();
          contid = new Set<id>();
         List<CampaignMember> cmMemberList = [Select contactid from CampaignMember where campaignid = : opp.Campaignid ];
         for(CampaignMember cm : cmMemberList )
         {
             contid.add(cm.contactid);
         } 
         if(!contid.isEmpty())
         {
         List<Contact> contList =[Select id, Account.name, FirstName, LastName, title, Owner.Name, Target_UK_Region_Contact__c, Target_European_Region__c, Target_Americas_Region__c, Target_APAC_Region__c, Target_ME_Region__c, Target_Africa_Region__c from contact where id in :contid];
         
             contactListSelected.addall(contList);
         }
    }
    
    public void buildheaderbar()
    {
         conference=false;
         spa=false;
         ski=false;
         parking=false;
         development=false;
        if(opp.Portfolio__c==null)
        {
            descheader=opp.Property__r.Description__c;
            roomcount=opp.Property__r.Room_Count__c;
            conference=opp.Property__r.Conference__c;
            spa=opp.Property__r.Spa__c;
            ski=opp.Property__r.Ski__c;
            parking=opp.Property__r.Parking__c;
            development=opp.Property__r.Development__c;
        }
        else
        {
             List<Property_Portfolio_Link__c> ppLink = [Select id, Property__r.Room_Count__c,Property__r.Conference__c,Property__r.Spa__c,
             Property__r.Ski__c,Property__r.Development__c,Property__r.Parking__c
             from Property_Portfolio_Link__c where Portfolio__c=:opp.Portfolio__c];
            descheader = opp.Portfolio__r.Portfolio_Description__c;
            roomcount=0;
           
            for(Property_Portfolio_Link__c pp : ppLink)
            {
                if(pp.Property__r.Room_Count__c!=null )
                roomcount+=pp.Property__r.Room_Count__c;
                if(pp.Property__r.Conference__c)
                conference =true;
                if(pp.Property__r.Ski__c)
                ski =true;
                if(pp.Property__r.Spa__c)
                spa =true;
                if(pp.Property__r.Development__c)
                development =true;
                if(pp.Property__r.Parking__c)
                parking =true;
                
            }
        }
         if(opp.Estimated_Sale_Price__c!=null)
             {
                 
                
                   convertedAmount=0.0;
                  
                  
                if(!String.isBlank(opp.currencyisocode) && opp.Estimated_Sale_Price__c != null){
                    List<CurrencyType> lstCurrencyType = new List<CurrencyType>();
                    lstCurrencyType = [Select id,DecimalPlaces, ConversionRate,IsoCode From CurrencyType where IsoCode ='USD' limit 1];
                   
                            convertedAmount = opp.Estimated_Sale_Price__c*lstCurrencyType[0].ConversionRate;
                   system.debug(lstCurrencyType[0].ConversionRate+'--'+opp.Estimated_Sale_Price__c);   
                }
                else
                  convertedAmount = opp.Estimated_Sale_Price__c;
                  Decimal priceinmil= convertedAmount/1000000;
                  convertedAmount=convertedAmount.setscale(2);
                  
             }
             
               if(opp.Guide_Price__c!=null)
             {
                 
                
                   convertedAmountGuidePrice=0.0;
                  
                  
                if(!String.isBlank(opp.currencyisocode) && opp.Guide_Price__c != null){
                    List<CurrencyType> lstCurrencyType = new List<CurrencyType>();
                    lstCurrencyType = [Select id,DecimalPlaces, ConversionRate,IsoCode From CurrencyType where IsoCode ='USD' limit 1];
                   
                            convertedAmountGuidePrice = opp.Guide_Price__c*lstCurrencyType[0].ConversionRate;
                   system.debug(lstCurrencyType[0].ConversionRate+'--'+opp.Guide_Price__c);   
                }
                else
                  convertedAmountGuidePrice = opp.Guide_Price__c;
                  Decimal priceinmil= convertedAmountGuidePrice/1000000;
                  convertedAmountGuidePrice=convertedAmountGuidePrice.setscale(2);
             }
        
        
    }
    public void buildInitialwrapper()
    {
        swrap=new SearchWrapper();
       
         if(opp.Property__r.Region__c!=null)
         
         {
             if(opp.Property__r.Region__c.equalsignorecase('UK')||opp.Property__r.Region__c.equalsignorecase('United Kingdom'))
             {
             swrap.uk=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
               swrap.ukSubregions =tempList;
             }
             
             }
             if(opp.Property__r.Region__c.equalsignorecase('Europe'))
             {
             swrap.Europe=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
                swrap.EuropeSubregions =tempList;
             }
             
             }
             if(opp.Property__r.Region__c.equalsignorecase('Americas'))
             {
             swrap.Americas=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
                swrap.AmericasSubregions =tempList;
             }
             
             }
            //  if(opp.Property__r.Region__c.equalsignorecase('Asia Pacific'))
            //  {
            //  swrap.Asia=true;
            //  if(opp.Property__r.Sub_Region__c!=null)
            //  {
            //      List<String> tempList = new List<String>();
            //      tempList.add(opp.Property__r.Sub_Region__c);
            //     swrap.ukSubregions =tempList;
            //  }
             
            //  }
             if(opp.Property__r.Region__c.equalsignorecase('Africa'))
             {
             swrap.Africa=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
                swrap.AfricaSubregions =tempList;
             }
             
             }
             if(opp.Property__r.Region__c.equalsignorecase('Middle East'))
             {
             swrap.middleEast=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
                swrap.middleEastSubRegions =tempList;
             }
             
             }
             if(opp.Property__r.Region__c.equalsignorecase('Asia Pacific'))
             {
             swrap.apac=true;
             if(opp.Property__r.Sub_Region__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Sub_Region__c);
                swrap.apacSubRegions =tempList;
             }
             
             }
             
             
             if(opp.Property__r.Spa__c)
             {
                swrap.spa=true; 
             }
             if(opp.Property__r.Conference__c)
             {
                swrap.conference=true; 
             }
             if(opp.Property__r.Ski__c)
             {
                swrap.ski=true; 
             }
             if(opp.Property__r.Development__c)
             {
                swrap.development=true; 
             }
             if(opp.Property__r.Golf__c)
             {
                swrap.golf=true; 
             }
              if(opp.Property__r.Location_Type__c!=null)
             {
                 List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Location_Type__c);
                swrap.selectedLocation=tempList;
             }
               if(opp.Property__r.Grade_Star_Rating__c!=null)
             {
                 
                    List<String> tempList = new List<String>();
                 tempList.add(opp.Property__r.Grade_Star_Rating__c);
                swrap.selectedrating=tempList; 
             }
               if(opp.Property__r.Room_Count__c!=null)
             {
                 
                 if(opp.Property__r.Room_Count__c<=50)
                 {
                     swrap.smallrooms=true;
                      
                 }
                 else if (opp.Property__r.Room_Count__c<=200)
                 {
                        swrap.mediumrooms=true;
                 }
                 
                 else
                 {
                       swrap.largerooms=true;
                 }
                   
               
               
             }
             
             if(opp.Property__r.Brand_Lookup__c!=null)
             {
                  List<String> tempList = new List<String>();
                 tempList.add('Branded');
                swrap.selectedBranding=tempList; 
             }
             else
             {
                   List<String> tempList = new List<String>();
                 tempList.add('Unbranded');
                swrap.selectedBranding=tempList; 
             }
             
             if(opp.Estimated_Sale_Price__c!=null)
             {
                 
                
                   convertedAmount=0.0;
                  
                  
                if(!String.isBlank(opp.currencyisocode) && opp.Estimated_Sale_Price__c != null){
                    List<CurrencyType> lstCurrencyType = new List<CurrencyType>();
                    lstCurrencyType = [Select id,DecimalPlaces, ConversionRate,IsoCode From CurrencyType where IsoCode ='USD' limit 1];
                   
                            convertedAmount = opp.Estimated_Sale_Price__c*lstCurrencyType[0].ConversionRate;
                   system.debug(lstCurrencyType[0].ConversionRate+'--'+opp.Estimated_Sale_Price__c);   
                }
                else
                  convertedAmount = opp.Estimated_Sale_Price__c;
                  Decimal priceinmil= convertedAmount/1000000;
                  convertedAmount=convertedAmount.setscale(2);
                 
                 if(priceinmil<=5)
                 {
                      swrap.zerofive=true;
                      
                 }
                 else if(priceinmil<=10)
                 {
                     swrap.fiveten=true;
                 }
                 else if(priceinmil<=20)
                 {
                       swrap.tentwenty=true;
                 }
                 else if(priceinmil<=50)
                 {
                      swrap.twentyfifty=true;
                 }
                 else 
                 {
                       swrap.fiftyabove=true;
                 }
               
             }
         
            
              if(opp.Guide_Price__c!=null)
             {
                 
                
                   convertedAmountGuidePrice=0.0;
                  
                  
                if(!String.isBlank(opp.currencyisocode) && opp.Guide_Price__c != null){
                    List<CurrencyType> lstCurrencyType = new List<CurrencyType>();
                    lstCurrencyType = [Select id,DecimalPlaces, ConversionRate,IsoCode From CurrencyType where IsoCode ='USD' limit 1];
                   
                            convertedAmountGuidePrice = opp.Guide_Price__c*lstCurrencyType[0].ConversionRate;
                   system.debug(lstCurrencyType[0].ConversionRate+'--'+opp.Guide_Price__c);   
                }
                else
                  convertedAmountGuidePrice = opp.Guide_Price__c;
                  Decimal priceinmil= convertedAmountGuidePrice/1000000;
                  convertedAmountGuidePrice=convertedAmountGuidePrice.setscale(2);
                 if(priceinmil<=5)
                 {
                      swrap.zerofive=true;
                      
                 }
                 else if(priceinmil<=10)
                 {
                     swrap.fiveten=true;
                 }
                 else if(priceinmil<=20)
                 {
                       swrap.tentwenty=true;
                 }
                 else if(priceinmil<=50)
                 {
                      swrap.twentyfifty=true;
                 }
                 else 
                 {
                       swrap.fiftyabove=true;
                 }
               
             }
         
        
             
         }   
        


        searchContacts();
          
}    
    
    
    public void searchContacts()
    {
          contactList  = new List<Contact>();
        String query =buildquery();
    Database.QueryLocator resultList = Database.getQueryLocator(query);
    System.debug('query------->'+query);
            // Get an iterator
            Database.QueryLocatorIterator resultIterator =  resultList.iterator();
            
            // Iterate over the records
            while (resultIterator.hasNext())
            {
                Contact c = (Contact)resultIterator.next();
                contactList.add(c);
            }
       
    }
    public String buildquery()
    {
        String querysTr ='';
        querysTr+='Select id, Account.name, FirstName, LastName, title, Owner.Name, Target_UK_Region_Contact__c, Target_European_Region__c, Target_Americas_Region__c, Target_APAC_Region__c, Target_ME_Region__c, Target_Africa_Region__c from Contact where Buyer_Contact__c= true and ';
      boolean loopFlag=false;
       if(swrap.uk)
       {
           querysTr+='(';
       loopFlag=true;
        querysTr+=' ( UK_Interest__c = true ';
        if(swrap.ukSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.ukSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.ukSubregions.size()-1))
                tempStr+=' ,';
                i++;
            }
            tempStr+=')';
            if(!swrap.ukSubregions.isEmpty())
            querysTr+=' and Target_UK_Region_Contact__c includes  ' +tempStr;
            
            
        }
        
       
             
         
         
         querysTr+=') ';
    }
       
       
       
      
         
       if(swrap.Europe)
       {
     
        if(loopFlag)
        querysTr+=' AND ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
        querysTr+=' ( European_Interest__c = true ';
        if(swrap.EuropeSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.EuropeSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.EuropeSubregions.size()-1))
                tempStr+=', ';
                i++;
            }
            tempStr+=')';
            if(!swrap.EuropeSubregions.isEmpty())
            querysTr+=' and Target_European_Region__c includes  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
       
       if(swrap.Africa)
       {
       
        if(loopFlag)
        querysTr+=' AND ';
        else
         querysTr+=' ( ';
        loopFlag=true;
        
         
        querysTr+=' ( Africa_Interest__c = true ';
        if(swrap.AfricaSubregions!=null)
        {
            String tempStr='(';
           Integer i=0;
            for(String reg : swrap.AfricaSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.AfricaSubregions.size()-1))
                tempStr+=', ';
                i++;
            }
            tempStr+=')';
            if(!swrap.AfricaSubregions.isEmpty())
            querysTr+=' and Target_Africa_Region__c includes  ' +tempStr ;
        }
        
        
            querysTr+=') ';
       }
       
         
         
       if(swrap.Americas)
       {
           
        if(loopFlag)
        querysTr+=' AND ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
          
       
        querysTr+=' ( Americas_Interest__c = true ';
        if(swrap.AmericasSubregions!=null)
        {
            String tempStr='(';
          Integer i=0;
            for(String reg : swrap.AmericasSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.AmericasSubregions.size()-1))
                tempStr+=', ';
                i++;
            }
            tempStr+=')';
            if(!swrap.AmericasSubregions.isEmpty())
            querysTr+=' and Target_Americas_Region__c includes  ' +tempStr ;
        }
        
        querysTr+=' ) ';
 
       }
       if(swrap.apac)
       { 
           
        if(loopFlag)
        querysTr+=' AND ';
        else
         querysTr+=' ( ';
        loopFlag=true;
         
          
       
        querysTr+=' ( APAC_Interest__c = true ';
        if(swrap.apacSubregions!=null)
        {
            String tempStr='(';
          Integer i=0;
            for(String reg : swrap.apacSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.apacSubregions.size()-1))
                tempStr+=', ';
                i++;
            }
            tempStr+=')';
            if(!swrap.apacSubregions.isEmpty())
            querysTr+=' and Target_APAC_Region__c includes  ' +tempStr ;
        }
        
        querysTr+=' ) ';
 
       }
       
       
         
         
       if(swrap.middleEast)
       {
       if(loopFlag)
        querysTr+=' AND ';
        else
         querysTr+='(';
        loopFlag=true;
        querysTr+='( Middle_East_Interest__c = true ';
        if(swrap.middleEastSubregions!=null)
        {
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.middleEastSubregions)
            {
                tempStr+='\''+reg+'\'';
               if(i!=(swrap.middleEastSubregions.size()-1))
                tempStr+=' ';
                i++;
            }
            tempStr+=')';
            if(!swrap.middleEastSubregions.isEmpty())
            querysTr+=' and Target_ME_Region__c includes  ' +tempStr;
        }
        
        querysTr+=') ';
 
       }
        
      if(loopFlag)
        querysTr+=' )';
        if(swrap.selectedBranding!=null && !swrap.selectedBranding.isEmpty())
        {
             if(loopFlag)
             querysTr+=' AND (';
              else
            querysTr+=' ( ';
             
              loopFlag=true;
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedBranding)
            {
                
                tempStr+='\''+reg+'\'';
                if(i!=(swrap.selectedBranding.size()-1))
                tempStr+=' , ';
                i++;
            }
            tempStr+=')';
           
            querysTr+='  Target_Branded_Unbranded__c in  ' +tempStr;
             querysTr+=' ) ';
        }
        
        if(swrap.selectedLocation!=null && !swrap.selectedLocation.isEmpty())
        {
             if(loopFlag)
             querysTr+=' AND ( ';
              else
            querysTr+=' ( ';
              loopFlag=true;
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedLocation)
            {
                
                tempStr+='\''+reg+'\'';
                if(i!=(swrap.selectedLocation.size()-1))
                tempStr+=' , ';
                i++;
            }
            tempStr+=')';
           
            querysTr+='  Target_Location_Type__c includes  ' +tempStr;
               querysTr+=' ) ';
        }
        
          
        if(swrap.selectedrating!=null && !swrap.selectedrating.isEmpty())
        {
             if(loopFlag)
             querysTr+=' AND (';
              else
            querysTr+=' ( ';
              loopFlag=true;
            String tempStr='(';
            Integer i=0;
            for(String reg : swrap.selectedrating)
            {
                
                tempStr+='\''+reg+'\'';
                if(i!=(swrap.selectedrating.size()-1))
                tempStr+=' , ';
                i++;
            }
            tempStr+=')';
           
            querysTr+='  Target_Hotel_Grade__c includes  ' +tempStr;
               querysTr+=' ) ';
        }
        boolean loopFlagRoom=false;
        if(swrap.zerofive)
        {
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
             else
            querysTr+=' ( (';
            loopFlag=true;
            loopFlagRoom=true;
              querysTr+='  Target_Price0_5m__c = true';
               querysTr+=' ) ';
            
        }
        
        if(swrap.fiveten)
        {
             
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  TargetPrice_5m_10m__c = true';
               querysTr+=' ) ';
            
        }
       
        if(swrap.tentwenty)
        {
           
          
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  TargetPRice10m_20m__c = true';
               querysTr+=' ) '; 
        }
      
        if(swrap.twentyfifty)
        {
             
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  TargetPrice_20m_50m__c = true';
               querysTr+=' ) ';
            
        }
       
        if(swrap.fiftyabove)
        {
             
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  TargetPrice_50m__c = true';
               querysTr+=' ) ';
        }
        
        if(loopFlagRoom)
         querysTr+=' ) ';
        // else
        // {
        //     if(loopFlag)
        //      querysTr+=' AND (';
        //       else
        //     querysTr+='(';
        //     loopFlag=true;
        //       querysTr+='  TargetPrice_50m__c = false';
        //       querysTr+=' ) '; 
        // }
         loopFlagRoom=false;
        if(swrap.smallrooms)
        {
           
            if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  Size_Rqrd_50_Bedrooms__c = true';
               querysTr+=' ) '; 
            
        }
        //  else
        // {
        //      if(loopFlagRoom)
        //     querysTr+=' OR (';
            
        //     else if(loopFlag)
        //      querysTr+=' AND (';
        //       else
        //     querysTr+='(';
        //     loopFlag=true;
        //       querysTr+='  Size_Rqrd_50_Bedrooms__c = false';
        //       querysTr+=' ) '; 
        // }
        if(swrap.mediumrooms)
        {
            
             if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND ( (';
              else
            querysTr+=' ( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  SizeRqrd_51_200_Bedrooms__c = true';
               querysTr+=' ) ';
            
        }
         
        if(swrap.largerooms)
        {
            
              if(loopFlagRoom)
            querysTr+=' OR (';
            
            else if(loopFlag)
             querysTr+=' AND  ( (';
              else
            querysTr+='( (';
            loopFlag=true;
              loopFlagRoom=true;
              querysTr+='  SizeRqrd_200_Bedrooms__c = true';
               querysTr+=' ) ';
        }
        
          if(loopFlagRoom)
         querysTr+=' ) ';
       
        
          if(swrap.conference)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Target_Conference__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.development)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Target_Development__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.ski)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Target_Ski__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.spa)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Target_Spa__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
           if(swrap.golf)
       {
       if(loopFlag)
        querysTr+=' AND ( ';
        else
         querysTr+='(';
        loopFlag=true;
         querysTr+='  Target_Golf__c = true  ' ;
               querysTr+=' ) ';
        
       }
       
       
       
      querysTr= querysTr.removeEnd('and ');
       querysTr+=' order by lastmodifieddate desc limit 1000 ';
       system.debug('---query---'+querysTr);
       return querysTr;
    }
    
    public Pagereference saveCampaignMembers()
    {
        List<String> strList   = contIdsStr.split(';;');
        List<CampaignMember> insertCampaignmemList   = new List<CampaignMember>();
         contactListSelected = new List<Contact>();
        Map<ID,Contact> mapContact = new Map<ID,Contact>();
        for(Contact cont : contactList)
        {
            mapContact.put(cont.id,cont);
            system.debug('mapContact----->'+mapContact);
        }
        for(String str : strList)
        {
            if(mapContact.containsKey(str)  &&  !contid.contains(str))
            contactListSelected.add(mapContact.get(str));
            
        }
        
        for(contact cont : contactListSelected)
        {
            if( !contid.contains(cont.id))
            {
            CampaignMember newCM = new CampaignMember(

                CampaignId = opp.campaignId,
                ContactId = cont.Id,
                status = 'To Be Contacted');
                insertCampaignmemList.add(newCm);
                system.debug('insertCampaignmemList------>'+insertCampaignmemList);
            }

        }
        try
        {
            insert insertCampaignmemList;
        }
        catch(Exception e)
        {
          system.debug('ExceptionCheck'+e); 
            return null;
            
        }
          Pagereference pg = new Pagereference('/'+opp.campaignId);
        return pg;
    
        
     }
     
      Set<String> strSet= new Set<String> ();
    
    public void addToCampaignAction()
    {
         List<String> strList   = contIdsStr.split(';;');
        List<CampaignMember> insertCampaignmemList   = new List<CampaignMember>();
        contactListSelected = new List<Contact>();
     
        Map<ID,Contact> mapContact = new Map<ID,Contact>();
        for(Contact cont : contactList)
        {
            mapContact.put(cont.id,cont);
        }
       
        for(String str : strList)
        {
            if(mapContact.containsKey(str) &&  !contid.contains(str) &&  !strSet.contains(str) )
            {
            contactListSelected.add(mapContact.get(str));
            
            }
            
        }
         for(String str : strList)
        {
         strSet.add(str);   
        }
        
        for(contact cont : contactListSelected)
        {
            CampaignMember newCM = new CampaignMember(

                CampaignId = opp.campaignId,
                ContactId = cont.Id,
                status = 'To Be Contacted');
                insertCampaignmemList.add(newCm);

        }
        
    }
    public Pagereference cancelaction()
    {
        Pagereference pg = new Pagereference('/'+opp.id);
        return pg;
    }
    
    public void clearFilter()
    {
        
        swrap.uk=false;
        swrap.Europe=false;
        swrap.Africa=false;
        swrap.middleEast=false;
        swrap.Americas=false;
        swrap.apac=false;
        swrap.conference=false;
        swrap.ski=false;
        swrap.development=false;
        swrap.spa=false;
        swrap.golf=false;
        swrap.selectedBranding=null;
         swrap.selectedLocation=null;
          swrap.selectedrating=null;
           swrap.selectedRooms=null;
            swrap.selectedPrice=null;
             //swrap.selectedBranding=null;
             
         swrap.ukSubregions=null;
          swrap.EuropeSubregions=null;
          swrap.AfricaSubregions=null;
           swrap.middleEastSubregions=null;
            swrap.AmericasSubregions=null;
            swrap.apacSubregions=null;
            contactList= new List<Contact>();
             contactListSelected= new List<Contact>();
             
                swrap.zerofive=false;
         swrap.fiveten=false;
         swrap.tentwenty=false;
         swrap.twentyfifty=false;
         swrap.fiftyabove=false;
        
         swrap.smallrooms=false;
         swrap.mediumrooms=false;
         swrap.largerooms=false;
           contactListSelected= new List<Contact>();
          contid = new Set<id>();
         List<CampaignMember> cmMemberList = [Select contactid from CampaignMember where campaignid = : opp.Campaignid ];
         for(CampaignMember cm : cmMemberList )
         {
             contid.add(cm.contactid);
         }
         if(!contid.isEmpty())
         {
         List<Contact> contList =[Select id, Account.name, FirstName, LastName, title, Owner.Name, Target_UK_Region_Contact__c, Target_European_Region__c, Target_Americas_Region__c, Target_APAC_Region__c, Target_ME_Region__c, Target_Africa_Region__c from contact where id in :contid];
         
             contactListSelected.addall(contList);
         }
    }

    public Class SearchWrapper
    {
        public boolean uk{get;set;}
        public boolean Europe{get;set;}
        public boolean Africa{get;set;}
        public boolean middleEast{get;set;}
        public boolean Americas{get;set;}
        public boolean apac{get;set;}
        
        public boolean zerofive{get;set;}
        public boolean fiveten{get;set;}
        public boolean tentwenty{get;set;}
        public boolean twentyfifty{get;set;}
        public boolean fiftyabove{get;set;}
        
        public boolean smallrooms{get;set;}
        public boolean mediumrooms{get;set;}
        public boolean largerooms{get;set;}
       
        
        public List<String> ukSubregions{get;set;}
        public List<String> EuropeSubregions{get;set;}
        public List<String> AfricaSubregions{get;set;}
        public List<String> middleEastSubregions{get;set;}
        public List<String> AmericasSubregions{get;set;}
        public List<String> ApacSubregions{get;set;}
        
        
     public List<String> selectedBranding{get;set;}
      public List<String> selectedLocation{get;set;}
      public List<String> selectedrating{get;set;}
      public List<String> selectedRooms{get;set;}
      public List<String> selectedPrice{get;set;}
      
      public boolean conference{get;set;}
       public boolean ski{get;set;}
        public boolean development{get;set;}
         public boolean spa{get;set;}
         public boolean golf{get;set;}
         
       public SearchWrapper()
       {
           uk=false;
           Europe=false;
           Africa=false;
           middleEast=false;
           Americas=false;
           apac=false;
           
                zerofive=false;
         fiveten=false;
         tentwenty=false;
         twentyfifty=false;
         fiftyabove=false;
        
         smallrooms=false;
         mediumrooms=false;
         largerooms=false;
           
            conference=false;
           ski=false;
           development=false;
           spa=false;
           golf=false;
           ukSubregions = new List<String>();
            EuropeSubregions = new List<String>();
             AfricaSubregions = new List<String>();
              middleEastSubregions = new List<String>();
               AmericasSubregions = new List<String>();
                ApacSubregions = new List<String>();
                selectedBranding = new List<String>();
                 selectedLocation = new List<String>();
                  selectedrating = new List<String>();
                   selectedRooms = new List<String>();
                    selectedPrice = new List<String>();
       
           
       }
    }
    
   
}