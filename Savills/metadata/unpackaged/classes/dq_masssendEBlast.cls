public class dq_masssendEBlast {
    public boolean redirectBoolean {get;set;}
    Campaign camp;
    public dq_masssendEBlast(ApexPages.StandardController stdController)
    {
         camp= (Campaign)stdController.getRecord();
        
    }
   public void loadMessages()
   {
        
      List<CampaignMember> cmMember = [SELECT  id,Teaser_E_Blast_Sent__c FROM CampaignMember WHERE status = 'To Be Contacted' and campaignid = :camp.id ];
     try
     {
     if(cmMember!=null)
     {
       for(CampaignMember cm : cmMember)
       {
           cm.Teaser_E_Blast_Sent__c=true;
       }
       update cmMember;
        redirectBoolean=true;
     }
     }
     catch(Exception e)
     {
          redirectBoolean=false;
         
     }
   }

}