public without sharing class WorkinHandController {

    public list <Work_In_Hand__c> listWorkinHand{get; set;}
    public list<Opportunity> listOpportunity{get; set;}
    public string rowNo{get;set;}
    Public String OpportunityId;
    Public String RT;
    Public String Country;  
    Public String SavillsTeam;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
    //constructor to grt the records
    
    public WorkinHandController(apexpages.standardController stdController){
        //projectDuration=1;
    
        listOpportunity = new list<Opportunity>();
        listWorkinHand = new list<Work_In_Hand__c>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        OpportunityId = apexpages.currentpage().getparameters().get('id');
        listOpportunity = [select name,accountid,account.name, id, country__c, savills_team__c, RecordType.DeveloperName from Opportunity where id=: OpportunityId limit 1];
        if(listOpportunity!=null && listOpportunity.size()>0 &&  listOpportunity[0].RecordType!=null)  
        {
        RT = listOpportunity[0].RecordType.DeveloperName;
        Country = listOpportunity[0].Country__c;
        SavillsTeam = listOpportunity[0].Savills_Team__c;
        listWorkinHand = [select Id, Opportunity__r.id,Billing_Date__c, Client_Lookup__c, Client__c, Name, Fee_Type__c, Status__c, CurrencyIsoCode, Invoice_Number__c, Fee__c, Sales_Tax_VAT__c, Team__c, Expenses__c FROM Work_In_Hand__c where Opportunity__c =: OpportunityId limit 4999];
      
        for(Work_In_Hand__c obj_wih :listWorkinHand){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_wih));
            
        }
        }
    }  
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            
        integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        Work_In_Hand__c objwih = new Work_In_Hand__c();
         system.debug('######------------++++' + l);
     
        objwih.Opportunity__c = OpportunityId;
       
        //objwih.Property__c = lstOfbooleanProjectWrapper[l].objport.Property__c;
        
        listWorkinHand.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new Work in Hand record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        Work_In_Hand__c objwih = new Work_In_Hand__c();
        objwih.Opportunity__c = OpportunityId;
        objwih.Status__c = 'Scheduled';
            IF(RT == 'Agency_Buy_Side' || RT == 'Agency_Sell_Side'){
                objwih.Team__c = 'Agency';
            }
            IF(RT == 'Valuations_Core'){
                objwih.Team__c = 'Valuations';
            }
            IF(RT == 'Valuations_Consulting' && Country == 'UK' && SavillsTeam == 'Agency'){
                objwih.Team__c = 'Agency';
            }
            IF(RT == 'Valuations_Consulting' && Country == 'UK' && SavillsTeam == 'Valuations'){
                objwih.Team__c = 'Valuations';
            }
            IF(Country == 'UK'){
                objwih.Sales_Tax_VAT__c = 20;
            }
            IF(Country == 'Australia'){
                objwih.Sales_Tax_VAT__c = 10;
            }
            IF(Country == 'Vietnam'){
                objwih.Sales_Tax_VAT__c = 10;
            }
            IF(Country == 'Japan'){
                objwih.Sales_Tax_VAT__c = 10;
            }
            IF(Country == 'New Zealand'){
                objwih.Sales_Tax_VAT__c = 15;
            }
            IF(Country == 'Singapore'){
                objwih.Sales_Tax_VAT__c = 7;
            } 
        objwih.Client_Lookup__c = listOpportunity[0].accountid;
        listWorkinHand.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
        }      
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        system.debug('----'+lstOfbooleanProjectWrapper);
        list<Work_in_Hand__c> toDeleteRows = new list<Work_in_Hand__c>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objwih.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objwih);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
          system.debug('----'+toDeleteRows);
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<Work_in_Hand__c> todelete = new list<Work_in_Hand__c>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    saveWorkinHand 
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference saveWorkinHand() {
        if(showerror !=true)
        {
        list<Work_in_Hand__c> ListtoUpsert= new list<Work_in_Hand__c> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        Set<ID> oppId = new Set<ID>();
         for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
         
         
            if(string.valueof(objwih.objwih.Opportunity__c) != null)
            {
           
                 ListtoUpsert.add(objwih.objwih); 
                 oppId.add(objwih.objwih.Opportunity__c);
            }
          
                else if(string.valueof(objwih.objwih.Opportunity__c) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }
        
    Map<ID,Opportunity> oppList = new Map<ID,Opportunity>([Select id , CurrencyIsoCode from Opportunity where id in:oppId ]);
        
        
         for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
         
         
            if(string.valueof(objwih.objwih.Opportunity__c) != null && oppList.get(objwih.objwih.Opportunity__c).CurrencyIsoCode!=objwih.objwih.CurrencyIsoCode 
            && objwih.objwih.Fee_Type__c!='Exchange Difference')
            {
           
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'Fee records need to be in the same currency as the parent opportunity. Please ensure the fee is recorded in the same currency as the opportunity.'));  
            return null;  
            }
          
               
                 
        }
        
    
        
        
        try{
            Upsert ListtoUpsert;
        } catch(Exception e) {
            //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
            //                          e.getMessage()));  
            return null; 
        }
    
          PageReference acctPage = new PageReference('/' + OpportunityId);
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public Work_in_Hand__c objwih{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, Work_in_Hand__c objwihs){
          objwih = objwihs;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}