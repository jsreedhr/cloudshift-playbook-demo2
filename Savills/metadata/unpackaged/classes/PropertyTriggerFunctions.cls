public class PropertyTriggerFunctions {
    
    //method to retrieve the sub-region
    public static void getsubregions(List <PropertyHotels__c> lst_prophotels){
        
        Map<String, String> map_country_sr = new Map<String, String>();
        Map<String, String> map_country_region = new Map<String, String>();
        
        List <Location_Mapping__c> lst_locmap = new List <Location_Mapping__c>([SELECT ID, Country__c, Sub_Region__c, Region__c FROM Location_Mapping__c LIMIT 1000]);
        
        IF(!lst_locmap.isEmpty()){
            
            for(Location_Mapping__c l: lst_locmap){
                map_country_sr.put(l.country__c, l.sub_region__c);
                map_country_region.put(l.sub_region__c, l.region__c);
            }
            
            for(PropertyHotels__c p: lst_prophotels){
                IF(p.Country_Picklist__c != null){
                    p.Sub_Region__c = map_country_sr.get(p.Country_Picklist__c);
                    p.Region__c = map_country_region.get(p.Sub_Region__c);
                }     
            }            
        }
    }
}