@isTest
public without sharing class Test_LicenseRequestTriggerFunctions {

   private static testMethod void Test_LicenseRequest(){
       
   Test.startTest();
   Profile p = [SELECT ID, Name FROM Profile WHERE Profile.Name = 'System Administrator'];
   
   List <User> lst_users = new List <User>();
       for(integer i=0; i<3; i++){
           User u = new User();
           u.FirstName = 'Test';
           u.LastName = 'User';
           u.Email = 'savillstestuser' + i + '@testdomain.com';
           u.Username = 'savillstestuser'+ i + '@testdomain.com';
           u.UserPermissionsMarketingUser = TRUE;
           u.TimeZoneSidKey = 'Europe/London';
           u.CurrencyIsoCode = 'GBP';
           u.DefaultCurrencyIsoCode = 'GBP';
           u.Alias = u.FirstName.substring(0,3) + u.LastName.substring(0,1);
           u.EmailEncodingKey = 'UTF-8';
           u.CommunityNickname = u.FirstName + u.LastName.substring(0,3) + i;
           u.LocaleSidKey = 'en_GB';
           u.LanguageLocaleKey = 'en_US';
           u.IsActive = TRUE;
           u.ProfileId = p.id;
           lst_users.add(u);
           }
       insert lst_users;
       
       IF(!lst_users.isEmpty()){
           System.resetPassword(lst_users[0].id, true);
       }   
       
       License_Request__c l = new License_Request__c();
       l.First_Name__c = 'David';
       l.Last_Name__c = 'Kurtanjek';
       l.Email__c = 'testuser' + 1 + '@gmail.com';
       l.Timezone__c = 'Europe/London';
       l.Assistant_Email__c = 'barry@gmail.com';
       l.CurrencyIsoCode = 'GBP';
       l.Profile__c = 'System Administrator';
	   insert l;
		      
       l.Email__c = 'testuser' + 4 + '@gmail.com';
       l.Approved__c = TRUE;
       update l;
       
   Test.stopTest();
   }       
}