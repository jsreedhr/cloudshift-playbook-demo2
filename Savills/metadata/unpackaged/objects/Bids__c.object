<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Bid_Amount__c</fullName>
        <description>The bid amount.</description>
        <externalId>false</externalId>
        <inlineHelpText>The bid amount.</inlineHelpText>
        <label>Bid Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Bid_Date__c</fullName>
        <externalId>false</externalId>
        <label>Bid Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Bidding_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The prospective purchaser company. Built by CloudShift.</description>
        <externalId>false</externalId>
        <inlineHelpText>The prospective purchaser company.</inlineHelpText>
        <label>Bidding (Account)</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Bids</relationshipLabel>
        <relationshipName>Bids</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Bidding_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The prospective purchaser contact. Built by CloudShift.</description>
        <externalId>false</externalId>
        <inlineHelpText>The prospective purchaser contact.</inlineHelpText>
        <label>Bidding (Contact)</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.Bidding_Account__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Bids</relationshipLabel>
        <relationshipName>Bids</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Campaign__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The campaign the bid relates to. Built by CloudShift.</description>
        <externalId>false</externalId>
        <inlineHelpText>The campaign the bid relates to.</inlineHelpText>
        <label>Campaign</label>
        <referenceTo>Campaign</referenceTo>
        <relationshipLabel>Bids</relationshipLabel>
        <relationshipName>Bids</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Conditions_Comments__c</fullName>
        <externalId>false</externalId>
        <label>Conditions &amp; Comments</label>
        <length>10000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <description>The opportunity the bid relates to. Built by CloudShift.</description>
        <externalId>false</externalId>
        <inlineHelpText>The opportunity the bid relates to.</inlineHelpText>
        <label>Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Bids</relationshipLabel>
        <relationshipName>Bids</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>The status of the bid. Built by CloudShift.</description>
        <externalId>false</externalId>
        <inlineHelpText>The status of the bid.</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Verbal Offer</fullName>
                    <default>false</default>
                    <label>Verbal Offer</label>
                </value>
                <value>
                    <fullName>Submitted</fullName>
                    <default>false</default>
                    <label>Submitted</label>
                </value>
                <value>
                    <fullName>Under Review</fullName>
                    <default>false</default>
                    <label>Under Review</label>
                </value>
                <value>
                    <fullName>Accepted</fullName>
                    <default>false</default>
                    <label>Accepted</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Withdrawn</fullName>
                    <default>false</default>
                    <label>Withdrawn</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>The type of bid being made.</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of bid being made.</inlineHelpText>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1st Round</fullName>
                    <default>false</default>
                    <label>1st Round</label>
                </value>
                <value>
                    <fullName>2nd Round</fullName>
                    <default>false</default>
                    <label>2nd Round</label>
                </value>
                <value>
                    <fullName>Exclusive</fullName>
                    <default>false</default>
                    <label>Exclusive</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Bid</label>
    <nameField>
        <displayFormat>BR-{0000}</displayFormat>
        <label>Bid Reference</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Bids</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
