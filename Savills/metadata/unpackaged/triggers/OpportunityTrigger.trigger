trigger OpportunityTrigger on Opportunity(before insert, before update, before delete, after insert, after update){
    IF(Trigger.isBefore){
        If(Trigger.isInsert){
        OpportunityTriggerHelper.getcurrentcurrencyrate(Trigger.new);
        }
        If(Trigger.isUpdate){
        OpportunityTriggerHelper.getcurrentcurrencyrate(Trigger.new);
        }
        If(Trigger.isdelete){
        OpportunityTriggerHelper.deleteTransactions(Trigger.old);
        OpportunityTriggerHelper.deleteValuations(Trigger.old);
        OpportunityTriggerHelper.deleteWorkinHand(Trigger.old);
        }
    }
    IF(Trigger.isAfter){
        IF(Trigger.isInsert){
        OpportunityTriggerHelper.createportfoliolinks(Trigger.new);
        }
        IF(Trigger.isUpdate){
        if(!OpportunityTriggerHelper.opprecursionstatic)
        {
        OpportunityTriggerHelper.deleteportfoliolinks(Trigger.old, Trigger.new);
        OpportunityTriggerHelper.deletepropertytransactions(Trigger.old, Trigger.new);
        OpportunityTriggerHelper.createnewportfoliolinks(Trigger.old, Trigger.new);
        OpportunityTriggerHelper.deselectallpropertiesofinterest(Trigger.old, Trigger.new);  
        OpportunityTriggerHelper.updateallocations(Trigger.new);  
        OpportunityTriggerHelper.opprecursionstatic=true;
        }      
        }
    }
}