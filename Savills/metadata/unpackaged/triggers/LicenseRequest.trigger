trigger LicenseRequest on License_Request__c (before insert, before update) {
    IF(Trigger.isInsert){
        LicenseRequestTriggerFunctions.UserCount(Trigger.new);
        List <User> lst_users = new List <User>([SELECT ID, Email, Username FROM User WHERE isActive = TRUE]);
        for(License_Request__c l: Trigger.new){
            for(User u: lst_users){
                IF(!lst_users.isEmpty() && l.Email__c == u.Email){
                    l.addError('There is already a user with this email');
                }
            }
        }
    }
    IF(Trigger.isUpdate){
        LicenseRequestTriggerFunctions.UserCount(Trigger.new);
        List <User> lst_users = new List <User>([SELECT ID, Email, Username FROM User WHERE isActive = TRUE]);
        
        Set <ID> set_LRs = new Set <ID>();
        for(License_Request__c l: Trigger.new){
            If(integer.valueof(System.label.Number_of_Licenses) - l.Number_of_Active_Users__c <= 0){
                l.addError('There are no more available licenses.');
            }
            else{
                set_LRs.add(l.id);
            }
        }
    LicenseRequestTriggerFunctions.createUserfromLicenseRequest(set_LRs);
    }            
}