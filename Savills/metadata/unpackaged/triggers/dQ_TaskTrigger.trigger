trigger dQ_TaskTrigger on Task (after insert, after update, after delete) {
    
    if(trigger.isdelete)
    {
       dQ_TaskTriggerHandler.populateCampaignHistory(trigger.old);  
    }
    else
   dQ_TaskTriggerHandler.populateCampaignHistory(trigger.new);

}