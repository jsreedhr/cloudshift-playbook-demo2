trigger CampaignMember on CampaignMember (before insert, before update, after insert, after update) {
    If(Trigger.isBefore && !dQ_TaskTriggerHandler.cmtriggerrecursive) {
        If(Trigger.isInsert){
            CampaignMemberTriggerFunctions.cannotSetLeadExclusiveBuyer(Trigger.new);
            CampaignMemberTriggerFunctions.checkoneexclusivebidderinsert(Trigger.new);
        }
        If(Trigger.isUpdate){
            CampaignMemberTriggerFunctions.cannotSetLeadExclusiveBuyer(Trigger.new);
          CampaignMemberTriggerFunctions.checkoneexclusivebidderupdate(Trigger.OLdMap,Trigger.NewMap);

        }
    }
    else If(Trigger.isAfter && !dQ_TaskTriggerHandler.cmtriggerrecursive){
         Map<ID,Set<ID>> LeadContIds = new  Map<ID,Set<ID>> ();
        CampaignMemberTriggerFunctions.updateoppwithexlcusivebidder(Trigger.new); 
        for(CampaignMember cm :Trigger.new )
        {   Id contid;
        if(cm.contactid!=null)
        contid=cm.contactid;
        else
        contid=cm.leadid;
            if(LeadContIds.containskey(cm.campaignid))
            LeadContIds.get(cm.campaignid).add(contid);
            else
            {
               Set<ID> campid = new Set<ID>();
              
               campid.add(contid);
            LeadContIds.put(cm.campaignid,campid);
            }
       
        }
        system.debug('----'+LeadContIds);
         dQ_TaskTriggerHandler.cmtriggerrecursive=true;
         dQ_TaskTriggerHandler.processtaskdates(  LeadContIds );
        
    }
}