trigger PortfolioLinks on Portfolio_Link__c (after update) {
    PortfolioLinksTriggerFunctions.preventpropertychange(Trigger.old, Trigger.new);       
}