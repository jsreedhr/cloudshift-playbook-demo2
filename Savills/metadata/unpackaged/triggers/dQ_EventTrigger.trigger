trigger dQ_EventTrigger on Event (after insert, after update, after delete) {
    
    if(trigger.isdelete)
    {
       dQ_TaskTriggerHandler.populateCampaignHistoryEvent(trigger.old);  
    }
    else
   dQ_TaskTriggerHandler.populateCampaignHistoryEvent(trigger.new);

}