<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_alert_to_contact_campaign_owners_when_private_contact_added_to_campaign</fullName>
        <description>Email alert to contact &amp; campaign owners when private contact added to campaign</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Campaigns_Campaign_Member_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_E_Blast_Status</fullName>
        <description>Updates E_Blast status to contacted when TEASER/E_BLAST is ticked.</description>
        <field>Status</field>
        <literalValue>Contacted</literalValue>
        <name>Update E-Blast Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Campaign - Private Contact Added</fullName>
        <actions>
            <name>Email_alert_to_contact_campaign_owners_when_private_contact_added_to_campaign</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Private_Contact__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress,Planned</value>
        </criteriaItems>
        <description>Workflow rule to send an email when a Private Contact is added.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>E-Blast Status Update</fullName>
        <actions>
            <name>Update_E_Blast_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CampaignMember.Teaser_E_Blast_Sent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Once Mass Send E_Blast complete this will set the Status on the &quot;Add/Remove Campaign Member (From/To):&quot; page to Contacted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
