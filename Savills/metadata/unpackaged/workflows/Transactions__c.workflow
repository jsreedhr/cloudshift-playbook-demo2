<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Transaction_Set_Name</fullName>
        <field>Name</field>
        <formula>IF( 
NOT(ISBLANK(Property__c)), 
Property__r.Name &amp; &quot; - &quot; &amp; TEXT(DAY(Opportunity__r.CloseDate)) &amp; &quot;/&quot; &amp; TEXT(MONTH(Opportunity__r.CloseDate)) &amp; 
&quot;/&quot; &amp; TEXT(YEAR(Opportunity__r.CloseDate)), 

IF( 
NOT(ISBLANK(Portfolio_Transaction__c)), 
Portfolio_Transaction__r.Name &amp; &quot; - &quot; &amp; TEXT(DAY(Opportunity__r.CloseDate)) &amp; &quot;/&quot; &amp; TEXT(MONTH(Opportunity__r.CloseDate)) &amp; 
&quot;/&quot; &amp; TEXT(YEAR(Opportunity__r.CloseDate)), 

IF(AND(ISBLANK(Portfolio_Transaction__c), ISBLANK(Property__c)), 
Opportunity__r.Account.Name &amp; &quot; - &quot; &amp; TEXT(DAY(Opportunity__r.CloseDate)) &amp; &quot;/&quot; &amp; TEXT(MONTH(Opportunity__r.CloseDate)) &amp; 
&quot;/&quot; &amp; TEXT(YEAR(Opportunity__r.CloseDate)), Name)))</formula>
        <name>Transaction - Set Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Transaction - Set Name</fullName>
        <actions>
            <name>Transaction_Set_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the name of a transaction.</description>
        <formula>NOT(ISBLANK(Opportunity__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
