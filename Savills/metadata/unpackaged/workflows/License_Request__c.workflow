<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_License_Request_Email</fullName>
        <description>Approved License Request Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Request_Folder/License_Request_Approved_License_Request</template>
    </alerts>
    <alerts>
        <fullName>License_Request_New_Email_Alert</fullName>
        <description>License Request - New Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Request_Folder/License_Request_New_License_Request</template>
    </alerts>
    <alerts>
        <fullName>Rejected_License_Request_Email</fullName>
        <description>Rejected License Request Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>License_Request_Folder/License_Request_Rejected_License_Request</template>
    </alerts>
    <rules>
        <fullName>License Request - Approved License Request Alert</fullName>
        <actions>
            <name>Approved_License_Request_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Approved__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>License Request - New License Request Alert</fullName>
        <actions>
            <name>License_Request_New_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>License Request - Rejected License Request Alert</fullName>
        <actions>
            <name>Rejected_License_Request_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Rejected__c = TRUE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
