<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contact_Set_Account_Billing_City</fullName>
        <field>MailingCity</field>
        <formula>Account.BillingCity</formula>
        <name>Contact - Set Account Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Account_Billing_Country</fullName>
        <field>MailingCountry</field>
        <formula>Account.BillingCountry</formula>
        <name>Contact - Set Account Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Account_Billing_State</fullName>
        <field>MailingState</field>
        <formula>Account.BillingState</formula>
        <name>Contact - Set Account Billing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Account_Billing_Street</fullName>
        <field>MailingStreet</field>
        <formula>Account.BillingStreet</formula>
        <name>Contact - Set Account Billing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Billing_Post_Code</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.BillingPostalCode</formula>
        <name>Contact - Set Billing Post Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Mailing_City</fullName>
        <field>MailingCity</field>
        <formula>Account.ShippingCity</formula>
        <name>Contact - Set Mailing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Mailing_Country</fullName>
        <field>MailingCountry</field>
        <formula>Account.ShippingCountry</formula>
        <name>Contact - Set Mailing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Mailing_Post_Code</fullName>
        <field>MailingPostalCode</field>
        <formula>Account.ShippingPostalCode</formula>
        <name>Contact - Set Mailing Post Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Mailing_State</fullName>
        <field>MailingState</field>
        <formula>Account.ShippingState</formula>
        <name>Contact - Set Mailing State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Mailing_Street</fullName>
        <field>MailingStreet</field>
        <formula>Account.ShippingStreet</formula>
        <name>Contact - Set Mailing Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Other_City_Address</fullName>
        <field>MailingCity</field>
        <formula>Use_Other_Account_Address__r.Town_City__c</formula>
        <name>Contact - Set Other City Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Other_Country_Address</fullName>
        <field>MailingCountry</field>
        <formula>TEXT(Use_Other_Account_Address__r.Country__c)</formula>
        <name>Contact - Set Other Country Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Other_State_Address</fullName>
        <field>MailingState</field>
        <formula>Use_Other_Account_Address__r.State__c</formula>
        <name>Contact - Set Other State Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Other_Street_Address</fullName>
        <field>MailingStreet</field>
        <formula>Use_Other_Account_Address__r.Name</formula>
        <name>Contact - Set Other Street Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Set_Other_Zip_PC_Address</fullName>
        <field>MailingPostalCode</field>
        <formula>Use_Other_Account_Address__r.Zip_Postal_Code__c</formula>
        <name>Contact - Set Other Zip/PC Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contact - Use Account Billing Address</fullName>
        <actions>
            <name>Contact_Set_Account_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Account_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Account_Billing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Account_Billing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Billing_Post_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to overwrite contact address based on value from account.</description>
        <formula>Use_Billing_Address__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Use Account Mailing Address</fullName>
        <actions>
            <name>Contact_Set_Mailing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Mailing_Post_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Mailing_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Mailing_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to overwrite contact address based on value from account.</description>
        <formula>Use_Account_Address__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contact - Use Additional Account Address</fullName>
        <actions>
            <name>Contact_Set_Other_City_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Other_Country_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Other_State_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Other_Street_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Set_Other_Zip_PC_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to overwrite contact address based on value from account address.</description>
        <formula>NOT(ISBLANK(Use_Other_Account_Address__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
