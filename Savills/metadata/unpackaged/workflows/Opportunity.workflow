<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_Email_to_Owner_s_Assistant_Requesting_Update_of_Valuations_Log</fullName>
        <description>Email - Send Email to Owner&apos;s Assistant Requesting Update of Valuations Log</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Send_Complete_Vals_Log_Request_to_Owner_s_Assistant</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Job_ID_Email_to_Assistant</fullName>
        <description>Email - Send Job ID Email to Assistant</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Send_Job_ID_Request_to_Assistants</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Press_Release_Reminder_Email</fullName>
        <description>Email - Send Press Release Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Send_Press_Release_Reminder_Emails</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_AU_NZ_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - AU/NZ Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Australia</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Agency_Sell_Side_Send_Transaction_Review_Email</fullName>
        <description>Opportunity - Agency Sell Side - Send Transaction Review Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Review_Transaction_Email</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Asia_Pacific_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_tea</fullName>
        <description>Opportunity - Asia Pacific Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ADMIN</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Asia_Pacific</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Asia_Pacific_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - Asia Pacific Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Asia_Pacific</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Australia_NZ_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_tea</fullName>
        <description>Opportunity - Australia/NZ Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ADMIN</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Australia</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Global_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - Global Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mai.kawashima@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Global_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - Global Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mai.kawashima@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Send_Exchanged_Review_Email</fullName>
        <description>Opportunity - Send Exchanged Review Email</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Exchanged_Notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Send_Exclusivity_Info_Review_Email</fullName>
        <description>Opportunity - Send Exclusivity Info Review Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Send_Exclusivity_Reminder_Emails</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Spain_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - Spain Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ADMIN</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Spain</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Spain_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - Spain Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>Spain</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_UK_Ireland_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - UK/Ireland Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ADMIN</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>UK</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_UK_Ireland_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - UK/Ireland Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>UK</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nedwards@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_USA_Valuation_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - USA Valuation - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>USA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Valuation_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_US_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</fullName>
        <description>Opportunity - US Agency Sell Side/Buy Side - Send email to Opportunity team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>ADMIN</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>USA</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>gnicholas@savills.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>smcloughlin@savills.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Opportunity_Agency_Sell_Side_Send_Completed_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Agency_Assistant_Requesting_Update_of_Global_Connect_Portal_Websit</fullName>
        <description>Email - Send Email to Agency Assistant Requesting Update of Global Connect Portal Website</description>
        <protected>false</protected>
        <recipients>
            <field>Owners_Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Opportunity_Send_Global_Connect_Website_Reminder_to_Agency_Assistants</template>
    </alerts>
    <fieldUpdates>
        <fullName>Opp_Set_Exchanged_Email_Flag</fullName>
        <field>Exchanged_Reminder_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opp - Set Exchanged Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Exclusivity_Email_Flag</fullName>
        <field>Exclusivity_Reminder_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opp - Set Exclusivity Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Name</fullName>
        <field>Name</field>
        <formula>IF(NOT(ISBLANK(Property__c)), Property__r.Name &amp; &quot; - &quot; &amp; Savills_Team_Country_Short__c &amp; &quot; - &quot; &amp; Record_Type_Name__c &amp; &quot; - &quot; &amp;  Account.Name &amp; &quot; - &quot; &amp;   Month_of_Close_Date__c &amp; &quot; &quot; &amp; TEXT(YEAR(CloseDate)),

IF(NOT(ISBLANK(Portfolio__c)), &quot;Portfolio&quot; &amp; &quot; (&quot; &amp;  Portfolio__r.Name &amp; &quot;)&quot; &amp; &quot; - &quot; &amp; Savills_Team_Country_Short__c &amp; &quot; - &quot; &amp; Record_Type_Name__c &amp; &quot; - &quot; &amp;  Account.Name &amp; &quot; - &quot; &amp; Month_of_Close_Date__c &amp; &quot; &quot; &amp; TEXT(YEAR(CloseDate)),
Savills_Team_Country_Short__c &amp; &quot; - &quot; &amp; Record_Type_Name__c &amp; &quot; - &quot; &amp; &quot;(&quot; &amp;  Account.Name &amp; &quot;)&quot; &amp; &quot; - &quot; &amp; Month_of_Close_Date__c &amp; &quot; &quot; &amp; TEXT(YEAR(CloseDate))))</formula>
        <name>Opp - Set Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Set_Opp_Stage_Back_to_Instruction</fullName>
        <field>StageName</field>
        <literalValue>Instruction</literalValue>
        <name>Opp - Set Opp Stage Back to Instruction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Tick_Dynamics_Job_Id_Email_Flag</fullName>
        <field>Job_ID_Request_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opp - Tick Dynamics Job Id Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_Untick_Exclusivity_Flag</fullName>
        <field>Exclusivity_Reminder_Email_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Opp - Untick Exclusivity Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Number_of_Rooms</fullName>
        <field>Number_of_Rooms__c</field>
        <formula>Property__r.Room_Count__c</formula>
        <name>Opportunity - Number of Rooms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Buy_Side_Fee_Estimate</fullName>
        <field>Amount</field>
        <formula>IF(AND(INCLUDES(Fee_Type__c, &quot;Retainer&quot;), NOT(INCLUDES(Fee_Type__c, &quot;Success&quot;))),(Monthly_Retainer__c * Term_of_Retainment_Months__c),

IF(AND(INCLUDES(Fee_Type__c, &quot;Retainer&quot;), NOT(ISBLANK(Success_Fee__c))),(Monthly_Retainer__c * Term_of_Retainment_Months__c)+ Success_Fee__c, Success_Fee__c))</formula>
        <name>Opportunity - Set Buy Side Fee Estimate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Estimated_Fee</fullName>
        <field>Amount</field>
        <formula>IF(Value_of_Accepted_Quote__c = 0.00, Value_Accepted_Quote_New__c, 
Value_of_Accepted_Quote__c)</formula>
        <name>Opportunity - Set Estimated Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Opp_to_Exlcusivity</fullName>
        <field>StageName</field>
        <literalValue>Exclusivity</literalValue>
        <name>Opportunity - Set Opp to &apos;Exlcusivity&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Owners_Assistant_Email</fullName>
        <field>Owners_Assistant_Email__c</field>
        <formula>$User.Assistant_Email__c</formula>
        <name>Opportunity - Set Owner&apos;s Assistant Emai</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Stage_to_Instruction</fullName>
        <field>StageName</field>
        <literalValue>Instruction</literalValue>
        <name>Opportunity - Set Stage to Instruction</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Tick_Completion_Email_Flag</fullName>
        <field>Transaction_Review_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Tick Completion Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Tick_GC_Portal_Flag</fullName>
        <field>Update_Global_Connect_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Tick GC Portal Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Tick_Valuations_Log_Flag</fullName>
        <field>Completed_Vals_Log_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Tick Valuations Log Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity - AU%2FNZ Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_AU_NZ_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Australia/New Zealand</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for AU/NZ savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Agency - Calculate Estimated Fee</fullName>
        <actions>
            <name>Opportunity_Set_Buy_Side_Fee_Estimate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow firing on &apos;Agency Buy Side&apos; record type to calculate the estimated fee from the retainment and success figures entered.</description>
        <formula>RecordType.DeveloperName = &quot;Agency_Buy_Side&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Agency Sell Side - Send Final Transaction Review Email</fullName>
        <actions>
            <name>Opportunity_Agency_Sell_Side_Send_Transaction_Review_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Tick_Completion_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send an email requesting any relevant transaction(s) be reviewed.</description>
        <formula>AND(ISPICKVAL(StageName, &quot;Completed&quot;), OR(RecordType.DeveloperName = &quot;Agency_Sell_Side&quot;, 
RecordType.DeveloperName = &quot;Agency_Buy_Side&quot;),
Transaction_Review_Email_Flag__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Asia Pacific Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_Asia_Pacific_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_tea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>China,Japan,Singapore,Thailand,Vietnam</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for Asia Pacific Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Asia Pacific Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_Asia_Pacific_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>China,Japan,Singapore,Thailand,Vietnam</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for Asia Pacific savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Australia Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_Australia_NZ_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_tea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Australia/New Zealand</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for Australia Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Global Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_Global_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Global</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for Global Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Global Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_Global_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Global</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for Global savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Email to Assistant Requesting Dynamics Job Id</fullName>
        <actions>
            <name>Email_Send_Job_ID_Email_to_Assistant</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Tick_Dynamics_Job_Id_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Instruction</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Job_ID_Request_Email_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email to request the opportunity owner&apos;s assistant to create the job in the relevant external system and store the ID in SFDC.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Email to Assistant Requesting Update of Valuations Log</fullName>
        <actions>
            <name>Email_Send_Email_to_Owner_s_Assistant_Requesting_Update_of_Valuations_Log</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Tick_Valuations_Log_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owners_Assistant_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Completed_Vals_Log_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Valuations - Core</value>
        </criteriaItems>
        <description>Email to request the opportunity owner&apos;s assistant to update the valuations log.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Exchanged Reminder Email</fullName>
        <actions>
            <name>Opportunity_Send_Exchanged_Review_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Set_Exchanged_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Exchanged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Exchanged_Reminder_Email_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Workflow to send &apos;Exchanged Reminder&apos; email.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Exclusivity Reminder Email</fullName>
        <actions>
            <name>Opportunity_Send_Exclusivity_Info_Review_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opp_Set_Exclusivity_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Exclusivity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2) Agency - Buy Side,1) Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Exclusivity_Reminder_Email_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Workflow to send &apos;Exclusivity Reminder&apos; email to set expected fee, estimated sale price and billing (close date).</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Instruction Expiration Date 30 Days Before Deadline</fullName>
        <active>true</active>
        <description>Workflow to send reminder to project owner 30 days before instruction expiration date.</description>
        <formula>AND( RecordType.DeveloperName = &quot;Agency_Sell_Side&quot;, NOT(ISPICKVAL(StageName, &quot;Radar&quot;)),   				OR(ISPICKVAL(StageName, &quot;Pitch&quot;),   							ISPICKVAL(StageName, &quot;Instruction&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Instruction_Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity - Send Press Release Reminder Email</fullName>
        <actions>
            <name>Email_Send_Press_Release_Reminder_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Exchanged</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2) Agency - Buy Side,1) Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Press_Release__c</field>
            <operation>equals</operation>
            <value>Applicable</value>
        </criteriaItems>
        <description>Workflow to send &apos;Press Release&apos; reminder email at &apos;Exchanged&apos; when this has been marked as required.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Send Reminder to Agency Assistant to Update Global Connect Website</fullName>
        <actions>
            <name>Send_Email_to_Agency_Assistant_Requesting_Update_of_Global_Connect_Portal_Websit</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opportunity_Tick_GC_Portal_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>1) Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Instruction,Exclusivity,Completed,Billing Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Update_of_Global_Connect_Website__c</field>
            <operation>equals</operation>
            <value>Applicable</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Update_Global_Connect_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Workflow rule to send an email to the Agency Assistant requesting an update to the Global Connect Portal Website.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Accepted Fee</fullName>
        <actions>
            <name>Opportunity_Set_Estimated_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the Amount (Estimated Fee) to be the value of the accepted quote. Built by CloudShift.</description>
        <formula>Number_of_Accepted_Quotes__c &gt; 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Name</fullName>
        <actions>
            <name>Opp_Set_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to apply naming convention to all opportunities.</description>
        <formula>AND(OR( ISNEW(), ISCHANGED(AccountId), ISCHANGED(Country__c), ISCHANGED(Portfolio__c), ISCHANGED(Property__c), ISCHANGED(CloseDate)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Opp Back to %27Instruction%27</fullName>
        <actions>
            <name>Opp_Set_Opp_Stage_Back_to_Instruction</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opp_Untick_Exclusivity_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the opportunity to Instruction if the primary buyer contact is set back to null.</description>
        <formula>AND(RecordType.DeveloperName = &quot;Agency_Sell_Side&quot;, OR(ISPICKVAL(StageName, &quot;Exclusivity&quot;),  ISPICKVAL(StageName, &quot;Exchanged&quot;)), ISBLANK(Purchaser_Contact__c ), NOT(ISBLANK(PRIORVALUE(Purchaser_Contact__c ))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Opp to %27Exclusivity%27</fullName>
        <actions>
            <name>Opportunity_Set_Opp_to_Exlcusivity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the opportunity to Exclusivity once the primary contact has been recorded.</description>
        <formula>AND(RecordType.DeveloperName = &quot;Agency_Sell_Side&quot;, ISPICKVAL(StageName, &quot;Instruction&quot;), NOT(ISBLANK(Job_ID__c)), NOT(ISPICKVAL(Purchaser_AML_Beneficiary_Check__c, &quot;&quot;)), NOT(ISBLANK(Purchaser_Contact__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Set Opportunity Owner%27s Assistant Email</fullName>
        <actions>
            <name>Opportunity_Set_Owners_Assistant_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to record the email of the Opportunity Owner&apos;s Assistant. Built by CloudShift.</description>
        <formula>OR(ISBLANK(Owners_Assistant_Email__c), $User.Assistant_Email__c != Owners_Assistant_Email__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Spain Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_Spain_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Spain</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for Spain Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Spain Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_Spain_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Spain</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for Spain savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - UK%2FIE Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_UK_Ireland_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Ireland,UK</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for UK &amp; Ireland Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - UK%2FIreland Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_UK_Ireland_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>Ireland,UK</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for UK/Ireland savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - US Agency - Opportunity Completion</fullName>
        <actions>
            <name>Opportunity_US_Agency_Sell_Side_Buy_Side_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>2. Agency - Buy Side,1. Agency - Sell Side</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Agency Sell Side/Buy Side&apos; record type when opportunity stage set to &quot;Completed&quot; for US Savills Team countries</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - USA Valuation - Opportunity Completed</fullName>
        <actions>
            <name>Opportunity_USA_Valuation_Send_email_to_Opportunity_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>4. Valuations - Core</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Country__c</field>
            <operation>equals</operation>
            <value>US</value>
        </criteriaItems>
        <description>Workflow firing on &apos;Valuation&apos; record type when opportunity stage set to &quot;Completed&quot; for USA savills team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Vals - Number of Rooms</fullName>
        <actions>
            <name>Opportunity_Number_of_Rooms</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to calculate the number of rooms based on the property connected to the opportunity.</description>
        <formula>AND(NOT(ISBLANK(Property__r.Room_Count__c)),
IsClosed = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Vals - Set Opportunity to Instruction</fullName>
        <actions>
            <name>Opportunity_Set_Stage_to_Instruction</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (4 AND 5 AND 6 AND 7 AND 8)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Number_of_Accepted_Quotes__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>1) Valuations - Core,2) Valuations - Consulting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Category__c</field>
            <operation>notEqual</operation>
            <value>Loan Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Number_of_Accepted_Quotes__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Category__c</field>
            <operation>equals</operation>
            <value>Loan Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Loan_Size__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Term_of_Loan_Months__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>1) Valuations - Core,2) Valuations - Consulting</value>
        </criteriaItems>
        <description>For Vals Opportunities, set the opportunity to &apos;Instruction&apos; when a quote is accepted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
