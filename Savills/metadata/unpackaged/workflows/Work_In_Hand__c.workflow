<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Work_in_Hand_Expenses</fullName>
        <field>Expenses__c</field>
        <formula>IF(AND(OR( 
ISPICKVAL(Fee_Type__c, &quot;Fee Share To&quot;), 
ISPICKVAL(Fee_Type__c, &quot;Credit Note&quot;)), 
Expenses__c&gt;0),(Expenses__c* -1), 

IF(AND(OR( 
ISPICKVAL(Fee_Type__c, &quot;Fee Share Received&quot;), 
ISPICKVAL(Fee_Type__c, &quot;Invoice&quot;)), 
Expenses__c&lt;0), (Expenses__c* -1), Expenses__c))</formula>
        <name>Work in Hand - Expenses</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Work_in_Hand_Set_Sign</fullName>
        <field>Fee__c</field>
        <formula>IF(AND(OR(
ISPICKVAL(Fee_Type__c, &quot;Fee Share To&quot;),
ISPICKVAL(Fee_Type__c, &quot;Credit Note&quot;)),
Fee__c &gt;0),(Fee__c * -1),

IF(AND(OR(
ISPICKVAL(Fee_Type__c, &quot;Fee Share Received&quot;),
ISPICKVAL(Fee_Type__c, &quot;Invoice&quot;)), 
Fee__c &lt;0), (Fee__c * -1), Fee__c))</formula>
        <name>Work in Hand - Set Sign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Work in Hand - Expenses</fullName>
        <actions>
            <name>Work_in_Hand_Expenses</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set the correct sign as per the fee type.</description>
        <formula>NOT(ISBLANK(Expenses__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Work in Hand - Sign</fullName>
        <actions>
            <name>Work_in_Hand_Set_Sign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set the correct sign as per the fee type.</description>
        <formula>NOT(ISPICKVAL(Fee_Type__c, &quot;&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
