<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Allocation_Set_GIA_Sq_Foot</fullName>
        <field>GIA_Sq_Ft__c</field>
        <formula>Property__r.GIA_Sq_Ft__c</formula>
        <name>Allocation - Set GIA Sq Foot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Allocation_Set_Room_Count</fullName>
        <field>Room_Count__c</field>
        <formula>Property__r.Room_Count__c</formula>
        <name>Allocation - Set Room Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allocation - Set GIA Sq Foot</fullName>
        <actions>
            <name>Allocation_Set_GIA_Sq_Foot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the GIA Sq. Foot based on the connected property. Built by CloudShift.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Set Room Count</fullName>
        <actions>
            <name>Allocation_Set_Room_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the room count based on the connected property. Built by CloudShift.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
