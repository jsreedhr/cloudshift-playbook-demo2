<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>BackupTriggeredSendWorkflow</fullName>
        <active>true</active>
        <criteriaItems>
            <field>et4ae5__Automated_Send__c.et4ae5__BackupWorkflow__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>et4ae5__Automated_Send__c.et4ae5__Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>et4ae5__UnpopulateTSBackupWokflow</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>et4ae5__Automated_Send__c.et4ae5__BackupWorkflow__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
