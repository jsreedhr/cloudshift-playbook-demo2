<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-File</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>Transactions__c</tab>
    <tab>PropertyHOTELS__c</tab>
    <tab>Rent_Comparable__c</tab>
    <tab>Portfolio__c</tab>
    <tab>et4ae5__Configuration__c</tab>
</CustomApplication>
