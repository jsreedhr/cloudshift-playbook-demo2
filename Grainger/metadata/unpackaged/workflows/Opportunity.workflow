<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_Approval_Confirmation_Email</fullName>
        <description>Email Alert - Approval Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/IC_Approval_Approved_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Approval_Rejection_Email</fullName>
        <description>Email Alert - Approval Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/IC_Approval_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_DD_Budget_Approval_Confirmation_Email</fullName>
        <description>Email Alert - DD Budget Approval Confirmation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/DD_Budget_Approved_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_DD_Budget_Approval_Rejection_Email</fullName>
        <description>Email Alert - DD Budget Approval Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/DD_Budget_Approval_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_REG_Approval_Confirmation_Email</fullName>
        <description>Email Alert - To notify the authorised appover</description>
        <protected>false</protected>
        <recipients>
            <field>Approved_By__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/REG_Approval_Process_Submission_Email</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Investment_Committee_Approval</fullName>
        <description>Opportunity - Investment Committee Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>david@cloudshiftgroup.com.graingerlive</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michael@cloudshiftgroup.com.graingerlive</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approval_Templates/IC_Approval_Process_Submission_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Acquisition_Set_Naming_Convention</fullName>
        <field>Name</field>
        <formula>Project_Name__c &amp; &quot; (&quot; &amp; City__c  &amp; &quot;)&quot;</formula>
        <name>Acquisition - Set Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DD_Approval_Uncheck_box</fullName>
        <field>DD_Budget_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>DD Approval Uncheck box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DD_Approved_Checkbox</fullName>
        <field>DD_Budget_Approved__c</field>
        <literalValue>1</literalValue>
        <name>DD Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DD_Budget_In_Progress</fullName>
        <field>DD_Budget_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>DD Budget - In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DD_Budget_Untick_Approved</fullName>
        <field>DD_Budget_Approved__c</field>
        <literalValue>0</literalValue>
        <name>DD Budget - Untick Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Approval_In_progress</fullName>
        <field>Investment_Committee_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Investment Approval - In progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Approval_Revert_Stage</fullName>
        <field>StageName</field>
        <literalValue>Awaiting Investment Committee Approval</literalValue>
        <name>Investment Approval - Revert Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Approval_Uncheck_In_Approval</fullName>
        <field>Investment_Committee_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Investment Approval Uncheck In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Approved_Checkbox</fullName>
        <field>Investment_Committee_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Investment Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Committee_Email_Flag</fullName>
        <description>When the approval Email Alert is sent, the Approval Email Flag is set to TRUE.</description>
        <field>Approval_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Investment Committee - Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Committee_In_Approval</fullName>
        <description>When the approval Email Alert is sent, the &quot;Investment Committee - In Approval&quot; checkbox is set to True.</description>
        <field>Investment_Committee_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Investment Committee - In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Investment_Uncheck_box</fullName>
        <field>Investment_Committee_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Investment Uncheck box</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REG_Naming_Convention</fullName>
        <description>Naming convention, sets the Project name to Street, City and Postal Code - Built By Cloudshift.</description>
        <field>Name</field>
        <formula>Street__c  &amp; &quot; - &quot; &amp; 
City__c  &amp; &quot; - &quot; &amp; 
Postal_Code__c</formula>
        <name>REG Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_REG_Email_Alert_Flag_to_True</fullName>
        <description>Sets REG Email Alert Flag to True</description>
        <field>Email_Alert_REG_Approver_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Set REG Email Alert Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Awaiting_Offer_Sub</fullName>
        <field>StageName</field>
        <literalValue>Awaiting Offer Submission</literalValue>
        <name>Set Stage to Awaiting Offer Submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Change_Occurred_Checkbox</fullName>
        <description>Tick Change Occurred Checkbox</description>
        <field>Change_Occurred__c</field>
        <literalValue>1</literalValue>
        <name>Tick Change Occurred Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_In_Approval</fullName>
        <description>If the opportunity has been approved, the Investment Committee - In Approval&apos; checkbox will be unchecked.</description>
        <field>Investment_Committee_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity - Acquisitions Naming Convention</fullName>
        <actions>
            <name>Acquisition_Set_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Acquisition</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.City__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Naming convention for Acquisition Record Type, includes Project Name &amp; City</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Flag Post IC Changes</fullName>
        <actions>
            <name>Tick_Change_Occurred_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tick the Changes Occured checkbox that works in conjugation with the post IC Flag.</description>
        <formula>AND( 
NOT(ISPICKVAL(StageName ,&quot;Pre-qualification&quot;)), 
NOT(ISPICKVAL(StageName ,&quot;Awaiting Sourcing Committee Review&quot;)), 
NOT(ISPICKVAL(StageName ,&quot;Sourcing Committee Approved&quot;)), 
NOT(ISPICKVAL(StageName ,&quot;Awaiting Investment Committee Approval&quot;)), 
NOT(ISPICKVAL(StageName ,&quot;Dead&quot;)), 
RecordType.Name = &quot;Acquisition&quot;, 
Change_Occurred__c = FALSE,

OR (
ISCHANGED(Purchasing_Entity__c),
ISCHANGED( No_of_Units__c),
ISCHANGED(Residential_IV_Per_Square_Foot__c ), 
ISCHANGED(Total_Residential_Rent__c ), 
ISCHANGED(Total_Rent_all_elements__c), 
ISCHANGED(Total_VP__c), 
ISCHANGED(Grainger_Initial_Yield__c), 
ISCHANGED(Grainger_IRR__c ),
ISCHANGED(Amount ), 
ISCHANGED(Grainger_Share_of_Purchase_price__c), 
ISCHANGED(DD_Budget__c ), 
ISCHANGED(Introduction_Fees__c ), 
ISCHANGED(Unlevered_property_level_IRR__c ), 
ISCHANGED(Unlevered_entity_level_IRR__c ), 
ISCHANGED(Total_Capital_Outlay__c ), 
ISCHANGED(Gross_Yield_TCO__c ), 
ISCHANGED(Gross_Yield_Residential__c ), 
ISCHANGED(Gross_Yield_All_Elements__c ), 
ISCHANGED(Total_Residential_VP__c ), 
ISCHANGED(Grainger_Share_of_TCO__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - REG Naming Convention</fullName>
        <actions>
            <name>REG_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>REG Acquisition</value>
        </criteriaItems>
        <description>Naming convention for REG Acquisition Record Type, includes Street, City and Postal Code - Built By Cloudshift</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REG Opportunity - Approver Email Alert</fullName>
        <actions>
            <name>Email_Alert_REG_Approval_Confirmation_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_REG_Email_Alert_Flag_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Email_Alert_REG_Approver_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approved_By__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sends an Email Alert to the opportunity &apos;Approver&apos;.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
