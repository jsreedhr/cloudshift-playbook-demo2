/**
 * Class InvalidContactOwnershipUT : unit test for InvalidContactOwnership
 * @author Eric Wartelle
 * @version 0.1
 */ 
@isTest
public class InvalidContactOwnershipUT {
    public static testMethod void testInvalidContactOwnershipClass(){
       	User currentUser = new User(Username = 'e.wartelle@experis-it.fr', LastName = 'Wartelle', Alias = 'Eric', CommunityNickname = 'TontonLeric',
                                     Email = 'eric.wartelle@experis-it.fr', TimeZoneSidKey = 'Europe/Paris', emailencodingkey = 'UTF-8', localesidkey = 'fr_FR',
                                     languagelocalekey = 'fr', profileId = '00ew00000012I1zAAE', CompanyName = 'Experis');
        
        //Start unit test
        System.Test.startTest();
                
        System.runAs(currentUser) {  
            Contact c = new Contact(LastName = 'Test',email='kirangeorgec@gmail.com',phone='8156886875');
            insert c;
            ApexPages.currentPage().getParameters().put('id', currentUser.ID);
            ApexPages.StandardController myStandardController = new ApexPages.StandardController(c);
            InvalidContactOwnershipClass myInvalidContactOwnershipClass = new InvalidContactOwnershipClass(myStandardController);            
        }

        System.Test.stopTest();
        //Stop unit test
    }
}