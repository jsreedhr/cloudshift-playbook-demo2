/**
 *  @Class Name:    AccountPlanOrgChartControllerExtTest 
 *  @Description:   This is a test class for AccountPlanOrgChartControllerExt
 *  @Company: dQuotient
 *  CreatedDate: 01/09/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aishwarya SK        01/09/2017                 Original Version
 *
 */
 @isTest
 public class AccountPlanOrgChartControllerExtTest{
    
     public static testMethod void AccountPlanOrgChartControllerExtTest() {
        
        Profile pro = [Select id, name From Profile where name = 'System Administrator'];
       // Creation of User
       User user =new User(
                          LastName='LastName_1',
                          Alias= 'User_1',
                          Email='test_@gmail.com',
                          ProfileId=pro.id,
                          UserName='testcaseuser_1@gmail.com',  
                          CommunityNickname= 'NickName_1' ,
                          Department = 'Some Department',
                          EmailEncodingKey = 'ISO-8859-1',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          LocaleSidKey = 'en_US',
                          LanguageLocaleKey = 'en_US',
                          IsActive = true);
        insert user;
        
        //Create Account Record
        List<Account> accountList = new List<Account>();
        Account account = new Account();
        account.Name = 'Test Account';
        account.BillingStreet = 'Old Trafford';
        account.BillingCity = 'Manchester';
        account.BillingState = 'Manchester';
        account.BillingPostalCode = 'PQ123WR';
        accountList.add(account);
        insert accountList;
        system.debug('accountList------>'+accountList);
        
        //Create Contact Record
        List<Contact> contList = new List<Contact>();
        Contact cont = new Contact();
        cont.LastName = 'Test Contact';
        cont.Accountid = accountList[0].id;
        cont.Contact_Role__c = 'Sales Contact';
        cont.Email = 'test@org.com';
        cont.Phone= '111111';
        contList.add(cont);
        insert contList;
        system.debug('contList------>'+contList);
        
        //Create Account_Plan__c Record
        List<Account_Plan__c> accountPlanList = new List<Account_Plan__c>();
        Account_Plan__c accPlanObj = new Account_Plan__c();
        accPlanObj.Name = 'test';
        accPlanObj.Account_Plan__c = accountList[0].id;
        accPlanObj.Type__c = 'Grow';
        accPlanObj.Division__c = 'GLD';
        accPlanObj.Account_Strategy__c = 'demo';
        accPlanObj.CurrencyIsoCode = 'EUR';
        accPlanObj.Status__c = 'Draft';
        accPlanObj.End_Date__c = system.today()+1;
        accPlanObj.Start_Date__c = system.today();
        accPlanObj.VPS_Lead__c = user.id;
        accountPlanList.add(accPlanObj);
        insert accountPlanList;
        system.debug('accountPlanList------>'+accountPlanList);
        
        //Create Account_Plan_Contact__c Record
        List<Account_Plan_Contact__c> accountPlanContList = new List<Account_Plan_Contact__c>();
        Account_Plan_Contact__c accPlanContObj = new Account_Plan_Contact__c();
        accPlanContObj.Account_Plan__c = accountPlanList[0].id;
        accPlanContObj.Account_Plan_Contact__c = contList[0].id;
        accPlanContObj.Parent_Contact__c = contList[0].id;
        accPlanContObj.Position__c = 'CEO';
        accountPlanContList.add(accPlanContObj);
        insert accountPlanContList;
        system.debug('accountPlanContList----->'+accountPlanContList);
    
    test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(accPlanObj);
        AccountPlanOrgChartControllerExt testAccPlanCont = new AccountPlanOrgChartControllerExt(sc);
         
        PageReference pageRef = Page.AccountPlanOrgChart;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', accountPlanList[0].Id);
     test.stopTest();   
    }     
 }