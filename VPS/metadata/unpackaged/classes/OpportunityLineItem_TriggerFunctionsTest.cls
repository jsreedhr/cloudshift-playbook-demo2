@isTest
private class OpportunityLineItem_TriggerFunctionsTest {

    private static testMethod void test() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('UK Account Record Type').getRecordTypeId();
        insert objAccount;


 Pricebook2 standardPricebook = new Pricebook2(
                  Id = Test.getStandardPricebookId(),
                  IsActive = true        
                  );
        update standardPricebook; 
        
          Product2 productObj = new Product2(Name = 'twest');                                                                 
                                 insert productObj;
        
         PricebookEntry pbe = new PricebookEntry(
                                Pricebook2Id = standardPricebook.id, 
                                Product2Id = productObj.id, 
                                UnitPrice = 20
                                );
              pbe.IsActive = true;
              
        insert pbe;

        Id idOppRecordType = [Select Id From RecordType where RecordType.developername = 'Simple'
            limit 1
        ].id;

        Opportunity objOpportunity = new Opportunity();
        objOpportunity.RecordTypeId = idOppRecordType;
        objOpportunity.Name = 'Test Opp -' + 1;
        objOpportunity.StageName = 'Suspect';
        objOpportunity.Amount = 2000 + 1;
        objOpportunity.AccountId = objAccount.id;
        objOpportunity.Type = 'New Business';
        objOpportunity.CloseDate = System.today();
       objOpportunity.Pricebook2Id = standardPricebook.Id;
 objOpportunity.Total_Amount__c=0.0;
                objOpportunity.Total_Recurring_Revenue_Per_Period__c=0.0;
                objOpportunity.Total_One_Off_FR__c=0.0;
        insert objOpportunity;


               
                
              
        OpportunityLineItem LineItem1 = new OpportunityLineItem();
        //LineItem1.PricebookEntryId = pbClone.id;
        
        LineItem1.OpportunityId = objOpportunity.id;
        LineItem1.Quantity = 1;
        LineItem1.UnitPrice = 10000;
        LineItem1.Product2Id = productObj.Id;
        LineItem1.PricebookEntryId = pbe.Id;
        insert LineItem1;

        List < OpportunityLineItem > lstNewOpLi = new List < OpportunityLineItem > ();
        lstNewOpLi.add(LineItem1);
        OpportunityLineItem_TriggerFunctions.PopulateOptyTotalAmount(lstNewOpLi);

    }

}