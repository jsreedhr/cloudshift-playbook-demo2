/*
 * Class InvalidContactOwnershipClass : just for getting the information on the owner of the contact to help the user.
 * @author Eric Wartelle
 * @version 0.1
 */ 
public class InvalidContactOwnershipClass {
    //Properties of the class
    public String userName { get; set; }
    public String userEmail { get; set; }
      
    //Constructor
    public InvalidContactOwnershipClass(ApexPages.StandardController controller){
        PageReference currentPage = ApexPages.currentPage();
        //The ID of the owner is pass by argument from the previous page
        ID ownerId = (ID) currentPage.getParameters().get('id'); 
        User currentUser = [SELECT Name, ID, Email FROM User WHERE ID = :ownerID];
        userName = currentUser.Name;
        userEmail = currentUser.Email;
    }
}