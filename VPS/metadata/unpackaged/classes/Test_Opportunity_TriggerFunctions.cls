/**
 *  Class Name: Test_Opportunity_TriggerFunctions 
 *  Description: This is a  test class for Opportunity_TriggerFunctions 
 *  CreatedDate: 24/05/2016
 *
 */
@isTest
private class Test_Opportunity_TriggerFunctions { 
    
    /*
    * Method name  : createUsers
    * @description Creates a user for testing purposes
    * @return      List<User> The list of test User that have been inserted into the DB
    */
    public static User createUsers() {
        Profile p = [select id from profile where name='System Administrator']; 
        User u = new User(alias = 'standt', email='standarduserTestData@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', username='standarduserTestData@testorg.com');

        return u;
    }
    
    /*
     * Method name  : createOpportunity
     * @description Creates a list of new opportunities for testing purposes
     * @return:     Opportunity
    */ 
        public static List<Opportunity> createOpportunity(Account objAccount){
            Id idOppRecordType= [Select Id From RecordType where RecordType.developername = 'Simple' limit 1].id;
            Opportunity objOpportunity; 
            List<Opportunity> lstOpportunity=new List<Opportunity>();
            for(integer i=0;i<20;i++){
                objOpportunity =new Opportunity();
                objOpportunity.RecordTypeId=idOppRecordType;
                objOpportunity.Name='Test Opp -'+i;
                objOpportunity.StageName='Suspect';
                objOpportunity.Amount=2000+i;
                objOpportunity.AccountId=objAccount.id;
                objOpportunity.Type='New Business';
                objOpportunity.CloseDate=System.today();
                lstOpportunity.add(objOpportunity);
        }           
        return lstOpportunity;
    }
    
    /*
     *  Method name:   createOpportunity
     *  @description   Tests method that tests create 
     */        
    static testMethod void createOpportunity() {
        
        //create User
        User objUser =new User();
        objUser = createUsers();
        insert objUser;
        System.assert(objUser.id != null);
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('UK Account Record Type').getRecordTypeId();
        insert objAccount;
        List<Opportunity> lstOpportunity=new List<Opportunity>();
        
        
        system.runAs(objUser){
            //Call Opportunity trigger. 
            Test.startTest();
                lstOpportunity = createOpportunity(objAccount);
                for(Opportunity objOpportunity: lstOpportunity){
                    objOpportunity.StageName = 'Bidding';
                }
                try{
                    insert lstOpportunity;
                }catch(exception e){
                    System.assertEquals(e.getTypeName(), 'System.DmlException');
                }
            Test.stopTest();
        }
    }
    
    /*
     *  Method name:   updateOpportunity
     *  @description   Tests method that tests create 
     */        
    static testMethod void updateOpportunity() {
        
        //create User
        User objUser =new User();
        objUser = createUsers();
        insert objUser;
        System.assert(objUser.id != null);
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('UK Account Record Type').getRecordTypeId();
        insert objAccount;
        
        List<Contact> lstContact = new List<Contact>(); 
        Contact objContact1 = new Contact();        
        objContact1.LastName = 'Test';
        objContact1.AccountId = objAccount.id;
        objContact1.email = 'test@test.com';
        objContact1.Phone = '123456';
        lstContact.add(objContact1);
        
        Contact objContact2 = new Contact();        
        objContact2.LastName = 'Test2';
        objContact2.AccountId = objAccount.id;
        objContact2.email = 'test2@test.com';
        objContact2.Phone = '123456';
        lstContact.add(objContact2);
        
        insert lstContact;
        List<Opportunity> lstOpportunity=new List<Opportunity>();
        
        system.runAs(objUser){
            //Call Opportunity trigger. 
            Test.startTest();
                lstOpportunity = createOpportunity(objAccount);
                insert lstOpportunity;
                for(Opportunity objOpportunity: lstOpportunity){
                    objOpportunity.StageName = 'Bidding';
                }
                List<OpportunityContactRole> lstOpportunityContactRole = new List<OpportunityContactRole>();
                OpportunityContactRole objOpportunityContactRole1 = new OpportunityContactRole();
                objOpportunityContactRole1.OpportunityId = lstOpportunity[0].id;
                objOpportunityContactRole1.ContactId = lstContact[0].id;
                lstOpportunityContactRole.add(objOpportunityContactRole1);
                
                OpportunityContactRole objOpportunityContactRole2 = new OpportunityContactRole();
                objOpportunityContactRole2.OpportunityId = lstOpportunity[0].id;
                objOpportunityContactRole2.ContactId = lstContact[1].id;
                lstOpportunityContactRole.add(objOpportunityContactRole2);
                
                insert lstOpportunityContactRole;
                
                List<OpportunityCompetitor> lstOpportunityCompetitor = new List<OpportunityCompetitor>();
                OpportunityCompetitor objOpportunityCompetitor1 = new OpportunityCompetitor();
                objOpportunityCompetitor1.OpportunityId = lstOpportunity[0].id;
                objOpportunityCompetitor1.CompetitorName = 'Test';
                lstOpportunityCompetitor.add(objOpportunityCompetitor1);
                
                OpportunityCompetitor objOpportunityCompetitor2 = new OpportunityCompetitor();
                objOpportunityCompetitor2.OpportunityId = lstOpportunity[0].id;
                objOpportunityCompetitor2.CompetitorName = 'Test';
                lstOpportunityCompetitor.add(objOpportunityCompetitor2);
                
                insert lstOpportunityCompetitor;
                CheckRecursive.run  = true;
                try{
                    update lstOpportunity;
                }catch(exception e){
                    System.assertEquals(e.getTypeName(), 'System.DmlException');
                }
            Test.stopTest();
        }
    }
}