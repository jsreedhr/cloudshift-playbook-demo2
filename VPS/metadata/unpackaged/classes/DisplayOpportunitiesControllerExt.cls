/**
 *  @Class Name:    DisplayOpportunitiesController
 *  @Description:   This is the controller for displaying all opportunities of an account
 *  @Company: dQuotient
 *  CreatedDate:  04/07/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Anand              04/07/2016                  Orginal Version
 */
 public with sharing class DisplayOpportunitiesControllerExt{
    
    private Account objAccount;
    public final List<Opportunity> lstOpportunityDisplay;
    public boolean hasOpportunity{get; set;}
    private List<Account> lstAccount;
    private map<id, Account> mapIdAccount;
    public final map<String,List<Opportunity>> mapOppStageType;
    Public string selectedStageType{get;set;}
    Public List<SelectOption> opplist{get;set;}
    Public List<Opportunity> orgList{get;set;}
    /**
     *  Method Name: DisplayOpportunitiesControllerExt
     *  Description: Constructor for DisplayOpportunitiesControllerExt
     *  Param: None
     *  Return: None
    */ 
    public DisplayOpportunitiesControllerExt(ApexPages.StandardController stdController){
       
        orgList=new List<Opportunity>();
        mapOppStageType=new map<String,List<Opportunity>>();
        opplist=new List<SelectOption>();
        opplist.add(new SelectOption('All','All Opportunities'));
        opplist.add(new SelectOption('Won','Won Opportunities'));
        opplist.add(new SelectOption('Lost','Lost Opportunities'));
        opplist.add(new SelectOption('Open','Open Opportunities'));
        selectedStageType = 'All';
        
        
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        set<id> setIds = new set<id>();
        lstOpportunityDisplay = new List<Opportunity>();
        hasOpportunity = false;
        
        if(stdController != null){
            objAccount = (Account)stdController.getRecord();
            setIds.add(objAccount.id);
            integer noOFIterations = 1;
            while(!setIds.isEmpty()){
                lstOpportunity = new List<Opportunity>();
                if(noOFIterations == 1){
                    lstOpportunity =[Select id, name,
                                            isClosed,
                                            isWon,
                                            AccountId, 
                                            Account.Name,
                                            Account.Account_Name_Department__c,
                                            Account.ParentId,
                                            Account.Parent.Name,
                                            Account.Parent.ParentId,
                                            Account.Parent.Parent.Name, 
                                            Account.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Parent.Name,
                                            StageName,
                                            Amount,
                                            CloseDate,
                                            Currency_Symbol__c,
                                            OwnerId,
                                            RecordTypeId,
                                            Type,
                                            Owner.Name
                                            From 
                                            Opportunity
                                            where 
                                            AccountId in: setIds
                                            or Account.ParentId in: setIds
                                            or Account.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.Parent.ParentId in: setIds
                                            order by Account.Parent.Parent.Parent.Parent.Name, Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Name, Account.Parent.Name, Account.Name, Name];
                                        
                }else{
                    lstOpportunity =[Select id, name,
                                            AccountId, 
                                            Account.Name,
                                            Account.Account_Name_Department__c,
                                            Account.ParentId,
                                            Account.Parent.Name,
                                            Account.Parent.ParentId,
                                            Account.Parent.Parent.Name, 
                                            Account.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Parent.ParentId,
                                            OwnerId,
                                            StageName,
                                            Amount,
                                            CloseDate,
                                            Currency_Symbol__c,
                                            RecordTypeId,
                                            Type,
                                            Owner.Name
                                            From 
                                            Opportunity
                                            where 
                                            Account.ParentId in: setIds
                                            or Account.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.Parent.ParentId in: setIds
                                            order by Account.Parent.Parent.Parent.Parent.Name, Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Name, Account.Parent.Name, Account.Name, Name];
                    
                }     
                
                setIds = new set<id>();                     
                for(Opportunity objOpportunity: lstOpportunity){
                    if(objOpportunity.Account.Parent.Parent.Parent.ParentId != null){
                        setIds.add(objOpportunity.AccountId);
                    }
                }
                lstOpportunityDisplay.addAll(lstOpportunity);
                
                
                noOFIterations = noOFIterations+1;
                if(setIds.size() == 0){
                    break;
                }
            }
            
            if(lstOpportunityDisplay!=null && lstOpportunityDisplay.size()>0){
                    
                    orgList.addAll(lstOpportunityDisplay);
                
                    
                    for(Opportunity opp:lstOpportunityDisplay){
                        if(opp.isWon==true){
                               if (mapOppStageType.containskey('Won')){
                                   mapOppStageType.get('Won').add(opp);
                                   
                               }
                               else{
                                   List<Opportunity>wonopp=new List<Opportunity>();
                                   wonopp.add(opp);
                                  mapOppStageType.put('Won',wonopp); 
                               }
                               
                            }
                        
                       else if(opp.isClosed==true &&opp.isWon==false){
                            if (mapOppStageType.containskey('Lost')){
                                   mapOppStageType.get('Lost').add(opp);
                                   
                               }
                               else{
                                   List<Opportunity>lostopp=new List<Opportunity>();
                                   lostopp.add(opp);
                                  mapOppStageType.put('Lost',lostopp); 
                               }
         
                    }
                    else if(opp.isClosed==false){
                            if (mapOppStageType.containskey('Open')){
                                   mapOppStageType.get('Open').add(opp);
                                   
                               }
                               else{
                                   List<Opportunity>openopp=new List<Opportunity>();
                                   openopp.add(opp);
                                   mapOppStageType.put('Open',openopp); 
                               }
         
                    }
                    
                    
                    }   
                }
                
            
            if(!lstOpportunityDisplay.isEmpty())
                hasOpportunity= true;
            System.debug('---ListOfOpportunities---'+lstOpportunityDisplay);
            System.debug('mapvalueOpen'+mapOppStageType.get('Open'));
            System.debug('mapvalueOpen'+mapOppStageType.get('Lost'));
            System.debug('mapvalueOpen'+mapOppStageType.get('Won'));
        }
    }

    public void StageOpp(){
        System.debug('StagenameType'+selectedStageType);
        System.debug('Map---->'+mapOppStageType);
        orgList.clear();
        if(!String.isBlank(selectedStageType)){
            if(selectedStageType.equalsIgnoreCase('All')){
                orgList.addAll(lstOpportunityDisplay);
            }else{
                system.debug('Here---');
                if(mapOppStageType.containsKey(selectedStageType)){
                    orgList.addAll(mapOppStageType.get(selectedStageType));
                }
            }
        }
    }
 }