/**
 *  @Class Name:    AccPlanMonthTriggerHandler 
 *  @Description:   This is a controller for AccPlanMonthTrigger
 *  @Company: dQuotient
 *  CreatedDate: 07/09/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aishwarya SK         07/09/2017                 Original Version
 *  
 */public class AccPlanMonthTriggerHandler{
     
	public static void insertAccPlanCFY(List<Account_Plan__c> Triggernew){
	    
		List<Monthly_Expenditure__c> monthList = new List < Monthly_Expenditure__c > ();
	
        List<Account_Plan_Supplier_Names_CFY__c> accPlanCFY = Account_Plan_Supplier_Names_CFY__c.getall().values();

        List<Account_Plan_Supplier_Names_PFY__c> accPlanPFY = Account_Plan_Supplier_Names_PFY__c.getall().values();
        system.debug('accPlanCFY-->' + accPlanCFY);
        system.debug('accPlanPFY-->' + accPlanPFY);
        
        
        //Triggernew= [SELECT id,name from Account_Plan__c];
        Set <String> setacctPlanID = new Set<String>();
        
        for(Account_Plan__c  accPlan :Triggernew){
            
        	if(!setacctPlanID.contains(accPlan.Id)){
        	       for(Account_Plan_Supplier_Names_CFY__c acc: accPlanCFY){
            			Monthly_Expenditure__c mon = new Monthly_Expenditure__c(name=acc.value__c,
            			Account_Plan__c = accPlan.id,
                        Month_01__c = 0.00,
                        Month_02__c = 0.00,                                                
                        Month_03__c = 0.00,                                                 
                        Month_04__c = 0.00,                                                
                        Month_05__c = 0.00,                                                
                        Month_06__c = 0.00,                                                
                        Month_07__c = 0.00,                                                
                        Month_08__c = 0.00,                                                 
                        Month_09__c = 0.00,                                                
                        Month_10__c = 0.00,                                                
                        Month_11__c = 0.00,                                                 
                        Month_12__c = 0.00,
                        Current_Year__c = true);
                        monthList.add(mon);
                        }
        		
        		
            		for(Account_Plan_Supplier_Names_PFY__c accp: accPlanPFY){
            			Monthly_Expenditure__c mon1 = new Monthly_Expenditure__c(name=accp.value__c,
            			Account_Plan__c = accPlan.id,
                        Month_01__c = 0.00,
                        Month_02__c = 0.00,                                                
                        Month_03__c = 0.00,                                                 
                        Month_04__c = 0.00,                                                
                        Month_05__c = 0.00,                                                
                        Month_06__c = 0.00,                                                
                        Month_07__c = 0.00,                                                
                        Month_08__c = 0.00,                                                 
                        Month_09__c = 0.00,                                                
                        Month_10__c = 0.00,                                                
                        Month_11__c = 0.00,                                                 
                        Month_12__c = 0.00,
                        Current_Year__c = false);
                        monthList.add(mon1);
    
            		}
            		setacctPlanID.add(accPlan.Id);
        	    }
        	
            }
            insert monthList;
	    
	}
}