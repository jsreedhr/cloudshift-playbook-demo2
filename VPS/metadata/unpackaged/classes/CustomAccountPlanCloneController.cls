public class CustomAccountPlanCloneController {
    private ApexPages.StandardController standardController;
    
    public Id accountPlanid {get;set;}
    public Id clonedAccountPlanId{get;set;}
    
    Account_Plan__c clonedaccountplan;
    
    public CustomAccountPlanCloneController(ApexPages.StandardController standardController){
        
    	this.standardController = standardController;
        // Fields being cloned
     	if(!test.isRunningTest()){   this.standardController.addFields(new List<String> {'Name', 'VPS_Lead__c', 'CurrencyIsoCode', 'Account_Plan__c', 'Type__c', 'Start_Date__c', 'End_Date__c', 'Division__c', 'Status__c', 'Chatter_Group_URL__c', 'Account_Strategy__c', 'Annual_Revenue__c', 'Annual_Profit__c', 'Number_of_Employees__c', 'Contract_Start_Date__c', 'Contract_End_Date__c', 'Last_Price_Increase__c', 'Last_Price_Increase_Date__c', 'SOR__c', 'Installation_Removall_Fee__c', 'Account_Sales_Goal__c', 'Towers__c', 'S_A__c', 'Inspections__c', 'Guarding__c', 'Guardians__c', 'Glazing__c', 'Pest__c', 'Grounds__c', 'Contracting__c', 'Number_of_Sites__c', 'What_is_their_Business__c', 'What_is_Their_Strategy__c', 'Who_Are_Their_Competitors__c', 'Top_Business_News_Trends_Industry_Issues__c', 'Purchasing_History__c', 'Purchasing_History_from_Competitors__c', 'Customer_Needs__c', 'Method_of_Procurement__c', 'Action_Plan_Summary__c'});}
        AccountPlanid = standardController.getRecord().Id;
    }
    public PageReference redirectToClone(){
        System.debug('--redirectTo Clone');
        System.debug('Ids: ' + accountPlanId + ' , cloned: ' + clonedAccountPlanId);
        /*
        Id accountPlanId = Apexpages.currentPage().getParameters().get('accPlanId');
        Id clonedaccountplanId = Apexpages.currentPage().getParameters().get('clonedAccPlanId');
		*/
        Boolean redirect = true;
        
        if(clonedaccountplan.Id != null && accountPlanId != null){
            List <Account_Plan_Opportunity__c> lst_apo = new List <Account_Plan_Opportunity__c>([
                SELECT Id FROM Account_Plan_Opportunity__c WHERE Account_Plan__c =: accountPlanid]);
            List <Account_Plan_Contact__c> lst_apc = new List <Account_Plan_Contact__c>([
                SELECT Id FROM Account_Plan_Contact__c WHERE Account_Plan__c =: accountPlanid]);
            List <Account_Plan_Team__c> lst_apt = new List <Account_Plan_Team__c>([
                SELECT Id FROM Account_Plan_Team__c WHERE Account_Plan__c =: accountPlanid]);
            List <Rate_Card__c> lst_aprc = new List <Rate_Card__c>([
                SELECT Id FROM Rate_Card__c WHERE Account_Plan__c =: accountPlanid]);
            
            List <Account_Plan_Opportunity__c> lst_apo1 = new List <Account_Plan_Opportunity__c>([
                SELECT Id FROM Account_Plan_Opportunity__c WHERE Account_Plan__c =: clonedaccountplanId]);
            List <Account_Plan_Contact__c> lst_apc1 = new List <Account_Plan_Contact__c>([
                SELECT Id FROM Account_Plan_Contact__c WHERE Account_Plan__c =: clonedaccountplanId]);
            List <Account_Plan_Team__c> lst_apt1 = new List <Account_Plan_Team__c>([
                SELECT Id FROM Account_Plan_Team__c WHERE Account_Plan__c =: clonedaccountplanId]);
            List <Rate_Card__c> lst_aprc1 = new List <Rate_Card__c>([
                SELECT Id FROM Rate_Card__c WHERE Account_Plan__c =: clonedaccountplanId]);
            
            //if lists are not empty and list size is unequal then don't redirect
            // if all lists are either empty or of equal size then redirect to cloned accountplan
            if(!lst_apo.isEmpty() && lst_apo.size()!= lst_apo1.size())
                redirect = false;
            
            if(!lst_apc.isEmpty() && lst_apc.size()!= lst_apc1.size())
                redirect = false;
            
            if(!lst_apt.isEmpty() && lst_apt.size()!= lst_apt1.size())
                redirect = false;
            
            if(!lst_aprc.isEmpty() && lst_aprc.size()!= lst_aprc1.size())
                redirect = false;
            
            if(redirect){         
                PageReference pr = new Pagereference('/' + clonedaccountplan.Id);
                return pr;
            }
            System.debug('--going to sleep');
            
            long now = datetime.now().gettime();
            while(datetime.now().gettime()-now<10000);
			
        }        
        
        if(clonedaccountplan.Id == null || accountPlanId == null)
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Incomplete clone data, Failed to redirect'));
        return null;
    }
    public void goCloneAccountPlan()
    {
        System.Debug('Im in');
        
        accountPlanid = standardController.getRecord().Id;
        Account_Plan__c currentaccountplan = (Account_Plan__c) standardController.getRecord();
        String currentAccountPlanJson = JSON.serialize(currentAccountPlan);
        //Create the new account plan
        clonedaccountplan = new Account_Plan__c();
        
        clonedaccountplan.Name = currentaccountplan.Name;
        clonedaccountplan.VPS_Lead__c = currentaccountplan.VPS_Lead__c;
        clonedaccountplan.CurrencyIsoCode = currentaccountplan.CurrencyIsoCode;
        clonedaccountplan.Account_Plan__c = currentaccountplan.Account_Plan__c;
        clonedaccountplan.Type__c = currentaccountplan.Type__c;
        Integer numberDaysDue = currentaccountplan.Start_Date__c.daysBetween(currentaccountplan.End_Date__c);
        clonedaccountplan.Start_Date__c = currentaccountplan.Start_Date__c + numberDaysDue + 1;
        clonedaccountplan.End_Date__c = currentaccountplan.End_Date__c + numberDaysDue +1;
        clonedaccountplan.Division__c = currentaccountplan.Division__c;
        clonedaccountplan.Status__c = currentaccountplan.Status__c;
        clonedaccountplan.Chatter_Group_URL__c = currentaccountplan.Chatter_Group_URL__c;
        clonedaccountplan.Account_Strategy__c = currentaccountplan.Account_Strategy__c;
        clonedaccountplan.Annual_Revenue__c = currentaccountplan.Annual_Revenue__c;
        clonedaccountplan.Annual_Profit__c = currentaccountplan.Annual_Profit__c;
        clonedaccountplan.Number_of_Employees__c = currentaccountplan.Number_of_Employees__c;
        clonedaccountplan.Contract_Start_Date__c = currentaccountplan.Contract_Start_Date__c;
        clonedaccountplan.Contract_End_Date__c = currentaccountplan.Contract_End_Date__c;
        clonedaccountplan.Last_Price_Increase__c = currentaccountplan.Last_Price_Increase__c;
        clonedaccountplan.Last_Price_Increase_Date__c = currentaccountplan.Last_Price_Increase_Date__c;
        clonedaccountplan.SOR__c = currentaccountplan.SOR__c;
        clonedaccountplan.Installation_Removall_Fee__c = currentaccountplan.Installation_Removall_Fee__c;
        clonedaccountplan.Account_Sales_Goal__c = currentaccountplan.Account_Sales_Goal__c;
        clonedaccountplan.Towers__c = currentaccountplan.Towers__c;
        clonedaccountplan.S_A__c = currentaccountplan.S_A__c;
        clonedaccountplan.Inspections__c = currentaccountplan.Inspections__c;
        clonedaccountplan.Guarding__c = currentaccountplan.Guarding__c;
        clonedaccountplan.Guardians__c = currentaccountplan.Guardians__c;
        clonedaccountplan.Glazing__c = currentaccountplan.Glazing__c;
        clonedaccountplan.Pest__c = currentaccountplan.Pest__c;
        clonedaccountplan.Grounds__c = currentaccountplan.Grounds__c;
        clonedaccountplan.Contracting__c = currentaccountplan.Contracting__c;
        clonedaccountplan.Number_of_Sites__c = currentaccountplan.Number_of_Sites__c;
        clonedaccountplan.What_is_their_Business__c = currentaccountplan.What_is_their_Business__c;
        clonedaccountplan.What_is_Their_Strategy__c = currentaccountplan.What_is_Their_Strategy__c;
        clonedaccountplan.Who_Are_Their_Competitors__c = currentaccountplan.Who_Are_Their_Competitors__c;
        clonedaccountplan.Top_Business_News_Trends_Industry_Issues__c = currentaccountplan.Top_Business_News_Trends_Industry_Issues__c;
        clonedaccountplan.Purchasing_History__c = currentaccountplan.Purchasing_History__c;
        clonedaccountplan.Purchasing_History_from_Competitors__c = currentaccountplan.Purchasing_History_from_Competitors__c;
        clonedaccountplan.Customer_Needs__c = currentaccountplan.Customer_Needs__c;
        clonedaccountplan.Method_of_Procurement__c = currentaccountplan.Method_of_Procurement__c;
        clonedaccountplan.Action_Plan_Summary__c = currentaccountplan.Action_Plan_Summary__c;
        insert clonedaccountplan;
        
        clonedAccountPlanId = clonedaccountplan.id;
        cloneAccountPlan(accountPlanId,clonedAccountPlanId);
		
        System.debug('clonedAccountPlanId : ' + clonedAccountPlanId);
        
        long now = datetime.now().gettime();
        while(datetime.now().gettime()-now<10000);
		/*
        return new PageReference('/'+clonedaccountplan.Id);
		*/
    }
    @future
    public static void cloneAccountPlan(Id accountPlanId,Id clonedAccountPlanId){
                
        List <Account_Plan_Opportunity__c> lst_apo = new List <Account_Plan_Opportunity__c>([SELECT Id, Name, CurrencyIsoCode, Opportunity__c, Account_Plan__c FROM Account_Plan_Opportunity__c WHERE Account_Plan__c =: AccountPlanid]);
        List <Account_Plan_Contact__c> lst_apc = new List <Account_Plan_Contact__c>([SELECT Id, Name, CurrencyIsoCode, Account_Plan__c, Account_Plan_Contact__c, Position__c, Contact_Strategy__c, Buying_Role__c, Attitude__c, Spend__c, Parent_Contact__c FROM Account_Plan_Contact__c WHERE Account_Plan__c =: AccountPlanid]);
        List <Account_Plan_Team__c> lst_apt = new List <Account_Plan_Team__c>([SELECT Id, Name, CurrencyIsoCode, Account_Plan__c, Team_Member__c, Role__c FROM Account_Plan_Team__c WHERE Account_Plan__c =: AccountPlanid]);
        List <Rate_Card__c> lst_aprc = new List <Rate_Card__c>([SELECT Id, Name, Price_Book__c, Account_Plan__c FROM Rate_Card__c WHERE Account_Plan__c =: AccountPlanid]);
        
        //Create the new account plan opportunities
        IF(!lst_apo.isEmpty()){
           List <Account_Plan_Opportunity__c> clonedAccountPlanOpportunities = new List <Account_Plan_Opportunity__c>();
           for(Account_Plan_Opportunity__c apoexisting: lst_apo){
                Account_Plan_Opportunity__c aponew = new Account_Plan_Opportunity__c();
                aponew.CurrencyIsoCode = apoexisting.CurrencyIsoCode;
                aponew.Opportunity__c = apoexisting.Opportunity__c;
                aponew.Account_Plan__c = clonedaccountplanId;
                clonedAccountPlanOpportunities.add(aponew);
                
            }
            insert clonedAccountPlanOpportunities;
        }
        
        //Create the new account plan contacts
        IF(!lst_apc.isEmpty()){
           List <Account_Plan_Contact__c> clonedAccountPlanContacts = new List <Account_Plan_Contact__c>();
           for(Account_Plan_Contact__c apcexisting: lst_apc){
                Account_Plan_Contact__c apcnew = new Account_Plan_Contact__c();
                apcnew.Account_Plan_Contact__c = apcexisting.Account_Plan_Contact__c;
                apcnew.CurrencyIsoCode = apcexisting.CurrencyIsoCode;
				apcnew.Position__c = apcexisting.Position__c;
                apcnew.Contact_Strategy__c = apcexisting.Contact_Strategy__c;
                apcnew.Buying_Role__c = apcexisting.Buying_Role__c;
                apcnew.Attitude__c = apcexisting.Attitude__c;
                apcnew.Spend__c = apcexisting.Spend__c;
                apcnew.Parent_Contact__c = apcexisting.Parent_Contact__c;
                apcnew.Account_Plan__c = clonedaccountplanId;
                clonedAccountPlanContacts.add(apcnew);
            }
            insert clonedAccountPlanContacts;
        }
        
        //Create the new VPS Team Members
        IF(!lst_apt.isEmpty()){
           List <Account_Plan_Team__c> clonedAccountPlanTeam = new List <Account_Plan_Team__c>();
           for(Account_Plan_Team__c aptexisting: lst_apt){
                Account_Plan_Team__c aptnew = new Account_Plan_Team__c();
				aptnew.Team_Member__c = aptexisting.Team_Member__c;
                aptnew.Role__c = aptexisting.Role__c;
                aptnew.Account_Plan__c = clonedaccountplanId;
                clonedAccountPlanTeam.add(aptnew);
            }
            insert clonedAccountPlanTeam;
        }
        
        //Create the new Rate Cards
        IF(!lst_aprc.isEmpty()){
           List <Rate_Card__c> clonedAccountPlanRateCards = new List <Rate_Card__c>();
           for(Rate_Card__c aprcexisting: lst_aprc){
                Rate_Card__c aprcnew = new Rate_Card__c();
				aprcnew.Price_Book__c = aprcexisting.Price_Book__c;
                aprcnew.Account_Plan__c = clonedaccountplanId;
                clonedAccountPlanRateCards.add(aprcnew);
            }
            insert clonedAccountPlanRateCards;
        }
        
        // Redirect to the new cloned record
        //return new PageReference('/'+clonedaccountplan.Id);
        
        System.Debug('New AccountPlanId: '+clonedaccountplanId);
    }

}