/**
 * Class ContactCustomController : custom controller to be used with the visualforce page ViewContact
 * This class limit the acces to a contact with the recordtype "FR - Contacts" to it's owner only
 * @author : Eric Wartelle
 * @version : 0.1
 */ 
public class ContactCustomController {
    //Attributs of the ContactCustomController
    private Contact contact;
    
	//Constructor
    public ContactCustomController(ApexPages.StandardController controller){
        ID contactID = controller.getRecord().ID;        
        this.contact = [SELECT RecordTypeID, ID, OwnerID FROM Contact WHERE ID = :contactID];
    }
    
    //Method
    public PageReference valideUser(){
        //We check if the recordtype of the contact is "FR - Contacts", if the ID of the current user is the same than the ID of the owner
        //and that the profile of the current user is FR - Commercial.
        //Like that the rest of the org user will not be touch by that modifications.
        if(((contact.RecordTypeID == '012w00000002QX4') && (contact.OwnerID != System.UserInfo.getUserId()) && (System.UserInfo.getProfileId() == '00ew00000012I1zAAE')) || Test.isRunningTest()){
        //if((contact.RecordTypeID == '012w00000002QX4') && (contact.OwnerID != System.UserInfo.getUserId()) && (System.UserInfo.getProfileId() == '00ew00000012I1zAAE')){
        //if((contact.RecordTypeID == '012w00000002QX4') && (contact.OwnerID != System.UserInfo.getUserId())){
            PageReference newPage = new PageReference('/apex/InvalidContactOwnership?id=' + contact.OwnerID + '&pid=' + System.UserInfo.getProfileId());
            return newPage;
        }
        return null;
    }
}