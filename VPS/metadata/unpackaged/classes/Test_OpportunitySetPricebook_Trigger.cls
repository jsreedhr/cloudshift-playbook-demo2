@isTest
private class Test_OpportunitySetPricebook_Trigger {
    testMethod static void testCase() {
        //Needed for VPSitex FR
        //Pricebook2 pb = new Pricebook2(Name='test pricebook1', IsActive=true, Description='test description');
        Pricebook2 pb = new Pricebook2(Name='* VPS FR BTP', IsActive=true, Description='test description');
        insert pb;
                
        Pricebook__c customPb = new Pricebook__c(Active__c=true, Name='test pricebook', Base_Pricebook_Record_Id__c=pb.Id, Description__c='test description');
        insert customPb;

        //Need for VPSitex FR
        //Account acct = new Account(Name='test account', Pricebook__c=customPb.Id);
        Account acct = new Account(Name='test account', Pricebook__c=customPb.Id, SIRET__c = '44916064700037 ');
        insert acct;
        
        Test.startTest();
        Opportunity opp = new Opportunity(Name='test opportunity', AccountId=acct.id, StageName='Prospect', Legacy_Opportunity__c = true, Site_Postcode__c = 'N/A', Site_Name__c = 'Site Name', Site_Address__c = 'Site Address', CloseDate=System.today());
        insert opp;
        Test.stopTest();
        opp = [Select Id, Pricebook2Id from Opportunity where Id=:opp.Id];
        System.assert(opp.Pricebook2Id == pb.Id);
    }
}