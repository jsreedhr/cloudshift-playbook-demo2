@isTest
private class RollingMapControllerExtTest {

	private static testMethod void RollingMapControllerExtTestMethod() 
	{  
	    Account newAccount = new Account(name = 'abc123');
	    insert newAccount;
	    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com'); 
            
        Account_Plan__c newAccountPlan = new Account_Plan__c(name='TestPlanAccount', Account_Plan__c = newAccount.id, Type__c = 'Grow', Division__c ='GLD',
                                                             VPS_Lead__c = u.id, End_Date__c = Date.today(), Status__c = 'Draft');
        Account_Plan_Supplier_Names_CFY__c accPlanCFY = new Account_Plan_Supplier_Names_CFY__c(name='Towers', value__c='Towers');
        Account_Plan_Supplier_Names_CFY__c accPlanCFY1 = new Account_Plan_Supplier_Names_CFY__c(name='S+A', value__c='S+A');
        Account_Plan_Supplier_Names_PFY__c accPlanPFY = new Account_Plan_Supplier_Names_PFY__c(name='Towers', value__c='Towers');
        insert accPlanCFY;
        insert accPlanCFY1;
        insert accPlanPFY;
        system.debug('AccplanCFY--->' + accPlanCFY);
        insert newAccountPlan;
        ApexPages.currentPage().getParameters().put('id',newAccountPlan.id);
        ApexPages.StandardController stdLead = new ApexPages.StandardController(newAccountPlan);
        RollingMapControllerExt objMyLeadsController = new RollingMapControllerExt(stdLead);
        objMyLeadsController.saveCurrent();
        objMyLeadsController.savePrevious();

	}

}