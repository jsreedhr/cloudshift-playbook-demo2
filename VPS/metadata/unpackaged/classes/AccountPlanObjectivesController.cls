public without sharing class AccountPlanObjectivesController {
    
    public list <Account_Plan_Objective__c> listAPObjectives{get; set;}
    public list<Account_Plan__c> listAP{get; set;}
    public string rowNo{get;set;}
    Public String APId;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
    //constructor to grt the records
    
    public AccountPlanObjectivesController(apexpages.standardController stdController){
    
        listAP= new list<Account_Plan__c>();
        listAPObjectives= new list<Account_Plan_Objective__c>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        APId = apexpages.currentpage().getparameters().get('id');
        listAP = [select name, id from Account_Plan__c where id=: APId limit 1];
        if(listAP!=null && listAP.size()>0){
        listAPObjectives = [select Id, Name, Account_Plan__c, Service__c, Status__c, Due_Date__c, Comments__c FROM Account_Plan_Objective__c where Account_Plan__c =: APId limit 4999];
      
        for(Account_Plan_Objective__c obj_wih :listAPObjectives){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_wih));
            
        }
        }
    } 
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            
        integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        Account_Plan_Objective__c objwih = new Account_Plan_Objective__c();
         system.debug('######------------++++' + l);
     
        objwih.Account_Plan__c= APId;
              
        listAPObjectives.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new account plan objective record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        Account_Plan_Objective__c objwih = new Account_Plan_Objective__c();
        objwih.Account_Plan__c = APId;
        listAPObjectives.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
        }      
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        system.debug('----'+lstOfbooleanProjectWrapper);
        list<Account_Plan_Objective__c> toDeleteRows = new list<Account_Plan_Objective__c>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objwih.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objwih);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
          system.debug('----'+toDeleteRows);
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<Account_Plan_Objective__c> todelete = new list<Account_Plan_Objective__c>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    saveWorkinHand 
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference saveWorkinHand() {
        if(showerror !=true)
        {
        list<Account_Plan_Objective__c> ListtoUpsert= new list<Account_Plan_Objective__c> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        Set<ID> oppId = new Set<ID>();
         for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
         
         
            if(string.valueof(objwih.objwih.Account_Plan__c) != null)
            {
           
                 ListtoUpsert.add(objwih.objwih); 
                 oppId.add(objwih.objwih.Account_Plan__c);
            }
          
                else if(string.valueof(objwih.objwih.Account_Plan__c) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }
            
            for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
                    
            if(objwih.objwih.Account_Plan__c==Null || objwih.objwih.Status__c==Null || objwih.objwih.Service__c==Null || objwih.objwih.Due_Date__c==Null || objwih.objwih.Comments__c==Null){
           
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'Please ensure every objective has a defined status, service, due date and description/comments'));  
            return null;  
            }         
                     
        }        
      
        try{
            Upsert ListtoUpsert;
        } 
            catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
            'There is missing information on the plot, please edit and save the plot record to identify the missing information.'));                         
            return null; 
        }
    
          PageReference acctPage = new PageReference('/' + APId);
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public Account_Plan_Objective__c objwih{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, Account_Plan_Objective__c objwihs){
          objwih = objwihs;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}