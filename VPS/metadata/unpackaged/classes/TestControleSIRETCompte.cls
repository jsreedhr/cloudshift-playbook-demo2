@isTest
public class TestControleSIRETCompte
{  

//========================================================
// T.E.S.T M.E.T.H.O.D.S pour trigger ControleSIRETCompte
//========================================================
    public static TestMethod void ControleSIRETCompte_test()
    {
        Account a = new Account();
        Account b = new Account();
        Account c = new Account();

        a.Name = 'TEST SIRET';
        a.SIRET__c ='38333959500027';
        insert a;
    
        b.Name = 'TEST SIRET';
        b.SIRET__c ='37750606800016';
        insert b;
    
        try 
        {
            c.Name = 'TEST SIRET';
            c.SIRET__c ='38333959500027';
            insert c;
        } 
        catch (DmlException e) 
        {    
        }
    }
}