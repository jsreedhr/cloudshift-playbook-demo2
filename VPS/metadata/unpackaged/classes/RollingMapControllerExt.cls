/**
 *  @Class Name:    RollingMapControllerExt 
 *  @Description:   This is a controller for RollingTable_PrevYear and RollingTable_CurrYear
 *  @Company: dQuotient
 *  CreatedDate: 08/09/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Rohith Nair     08/09/2017                  Original Version
 *  
 */ 
public class RollingMapControllerExt {
    
    public String strAccid;
    public String accountplanid;
    public List<Monthly_Expenditure__c> MonthlyExpendituresCurrentYear{get;set;}
    public List<Monthly_Expenditure__c> MonthlyExpendituresBothYears{get;set;}
    public List<Monthly_Expenditure__c> MonthlyExpendituresPreviousYear{get;set;}
    public Decimal TotalMonthlySumCurrent{get;set;}
    public Decimal TotalMonthlySumPrevious{get;set;}
    public Decimal TotalYield {get;set;}
    public List <Decimal> TotalYieldListPrev{get;set;}
    public List <Decimal> TotalYieldListCurr{get;set;}
    public List <Integer> NoOfSitesCurr{get;set;}
    public List <Integer> NoOfSitesPrev{get;set;}
    public List<Decimal> TotalNameWiseListPreviousYear{get;set;}
    public List<Decimal> TotalMonthWiseListPreviousYear{get;set;}
    public List<Decimal> TotalMonthWiseListCurrentYear{get;set;}
    public List<Decimal> TotalNameWiseListCurrentYear{get;set;}
    public List<RollingMapWrapper> CurrentYearMonthlyExpendituresTotal{get;set;}
    public List<RollingMapWrapper> CurrentYearMonthlyExpendituresTotalNew{get;set;}
    public List<RollingMapWrapper> PreviousYearMonthlyExpendituresTotal{get;set;}
    public List<RollingMapWrapper> PreviousYearMonthlyExpendituresTotalNew{get;set;}
    public List<Date> CurrentYearMonths{get;set;}
    public List<Date> PreviousYearMonths{get;set;}
    public List<Date> MonthWiseDateList {get;set;}
    public date AccountPlanCreatedDate{get;set;}
    public date prevYearDate{get;set;}
    public date CurrentDate{get;set;}
    public date nextYear{get;set;}
    public date currYear{get;set;}
    public integer TotalNoOfSites{get;set;}
    public List<Account_Plan__c> AccountPlanList = new List<Account_Plan__c>();
    public Account_Plan__c AccountPlanWithDetails {get;set;}


    
    
     public Account_Plan__c objAccPlan {
        get;
        set;
    }
    
    public RollingMapControllerExt(ApexPages.StandardController stdController)
    {   TotalNameWiseListPreviousYear = new List<Decimal>();
        TotalMonthWiseListPreviousYear = new List<Decimal>();
        TotalMonthWiseListCurrentYear = new List<Decimal>();
        TotalNameWiseListCurrentYear = new List<Decimal>();
        MonthlyExpendituresCurrentYear = new List<Monthly_Expenditure__c>();
        MonthlyExpendituresBothYears = new List<Monthly_Expenditure__c>();
        MonthlyExpendituresPreviousYear = new List<Monthly_Expenditure__c>();
        TotalYieldListPrev  = new List<Decimal>();
        TotalYieldListCurr = new List<Decimal>();
        NoOfSitesCurr = new List<Integer>();
        NoOfSitesPrev = new List<integer>();
        List<Decimal> TotalYieldMonthWiseListCurr = new List<Decimal>();
        List<Decimal> TotalYieldMonthWiseListPrev = new List<Decimal>();
        TotalYield = 0;
        TotalNoOfSites = 0;
        AccountPlanWithDetails = new Account_Plan__c();
        
        CurrentYearMonths = new List<Date>();
            PreviousYearMonths = new List<Date>();
            CurrentYearMonthlyExpendituresTotal = new List<RollingMapWrapper>();
            CurrentYearMonthlyExpendituresTotalNew = new List<RollingMapWrapper>();
            PreviousYearMonthlyExpendituresTotal = new List<RollingMapWrapper>();
            PreviousYearMonthlyExpendituresTotalNew = new List<RollingMapWrapper>();
            for(integer i=0; i<12; i++)
            {
                NoOfSitesPrev.add(0);
                NoOfSitesCurr.add(0);
            }
        
       
        accountplanid = Apexpages.currentPage().getParameters().get('id');
        accountplanid = String.escapeSingleQuotes(accountplanid);
        if (stdController != null)
        { 
            objAccPlan = (Account_Plan__c) stdController.getRecord();
            AccountPlanList = new List < Account_Plan__c > ();
            AccountPlanList = [Select id, CreatedDate, Site_Month_01__c, Site_Month_02__c, Site_Month_03__c, Site_Month_04__c, Site_Month_05__c, Site_Month_06__c, Site_Month_07__c, Site_Month_08__c, Site_Month_09__c, Site_Month_10__c, Site_Month_11__c, Site_Month_12__c from Account_Plan__c where id =: accountplanid];
            AccountPlanWithDetails = AccountPlanList[0];
            AccountPlanCreatedDate = date.newInstance(AccountPlanList[0].CreatedDate.year(), AccountPlanList[0].CreatedDate.month(), AccountPlanList[0].CreatedDate.day());
        }
        system.debug('accountplanid-->' + accountplanid);
        system.debug('AccountPlanList-->' + AccountPlanList);
        
        MonthlyExpendituresBothYears = [Select Name,id, Month_01__c,Month_02__c, Month_03__c, Month_04__c, Month_05__c, Month_06__c, Month_07__c, Month_08__c, Month_09__c, Month_10__c, Month_11__c, Month_12__c, Current_Year__c from Monthly_Expenditure__c where Account_Plan__c = :accountplanid];
        system.debug('MonthlyExpendituresBothYears-->' + MonthlyExpendituresBothYears);
        if(MonthlyExpendituresBothYears!=NULL & MonthlyExpendituresBothYears.size()>0)
        {
            for(Monthly_Expenditure__c newMonthlyExpenditure: MonthlyExpendituresBothYears)
            {
                if(newMonthlyExpenditure.Current_Year__c == true)
                {
                    MonthlyExpendituresCurrentYear.add(newMonthlyExpenditure);
                }
                else
                  MonthlyExpendituresPreviousYear.add(newMonthlyExpenditure);
            }
        }
        system.debug('MonthlyExpendituresPreviousYear-->'+ MonthlyExpendituresPreviousYear);
        system.debug('MonthlyExpendituresCurrentYear-->' + MonthlyExpendituresCurrentYear);
          if(MonthlyExpendituresCurrentYear!=NULL && MonthlyExpendituresCurrentYear.size()>0)
          { Decimal TotalMonth1= 0;
            Decimal TotalMonth2 = 0;
            Decimal TotalMonth3 = 0;
            Decimal TotalMonth4 = 0;
            Decimal TotalMonth5 = 0;
            Decimal TotalMonth6 = 0;
            Decimal TotalMonth7 = 0;
            Decimal TotalMonth8 = 0;
            Decimal TotalMonth9 = 0;
            Decimal TotalMonth10 = 0;
            Decimal TotalMonth11 = 0;
            Decimal TotalMonth12 = 0;
            Decimal TotalYieldMonth1 = 0;
            Decimal TotalYieldMonth2 = 0;
            Decimal TotalYieldMonth3 = 0;
            Decimal TotalYieldMonth4 = 0;
            Decimal TotalYieldMonth5 = 0;
            Decimal TotalYieldMonth6 = 0;
            Decimal TotalYieldMonth7 = 0;
            Decimal TotalYieldMonth8 = 0;
            Decimal TotalYieldMonth9 = 0;
            Decimal TotalYieldMonth10 = 0;
            Decimal TotalYieldMonth11 = 0;
            Decimal TotalYieldMonth12 = 0;
            
              for(Monthly_Expenditure__c newMonthlyExpenditure: MonthlyExpendituresCurrentYear)
              {
                  Decimal TotalName = newMonthlyExpenditure.Month_01__c + newMonthlyExpenditure.Month_02__c + newMonthlyExpenditure.Month_03__c + newMonthlyExpenditure.Month_04__c +
                                      newMonthlyExpenditure.Month_05__c + newMonthlyExpenditure.Month_06__c + newMonthlyExpenditure.Month_07__c + newMonthlyExpenditure.Month_08__c +
                                      newMonthlyExpenditure.Month_09__c + newMonthlyExpenditure.Month_10__c + newMonthlyExpenditure.Month_11__c + newMonthlyExpenditure.Month_12__c;
                  TotalNameWiseListCurrentYear.add(TotalName);
                  
                  TotalMonth1 = TotalMonth1 + newMonthlyExpenditure.Month_01__c;
                  TotalMonth2 = TotalMonth2 + newMonthlyExpenditure.Month_02__c;  
                  TotalMonth3 = TotalMonth3 + newMonthlyExpenditure.Month_03__c;
                  TotalMonth4 = TotalMonth4 + newMonthlyExpenditure.Month_04__c;
                  TotalMonth5 = TotalMonth5 + newMonthlyExpenditure.Month_05__c;
                  TotalMonth6 = TotalMonth6 + newMonthlyExpenditure.Month_06__c;
                  TotalMonth7 = TotalMonth7 + newMonthlyExpenditure.Month_07__c;
                  TotalMonth8 = TotalMonth8 + newMonthlyExpenditure.Month_08__c;
                  TotalMonth9 = TotalMonth9 + newMonthlyExpenditure.Month_09__c;
                  TotalMonth10 = TotalMonth10 + newMonthlyExpenditure.Month_10__c;
                  TotalMonth11 = TotalMonth11 + newMonthlyExpenditure.Month_11__c;
                  TotalMonth12 = TotalMonth12 + newMonthlyExpenditure.Month_12__c;
                  
                  if(newMonthlyExpenditure.name=='S+A' || newMonthlyExpenditure.name == 'Prop Serv Rec' || newMonthlyExpenditure.name=='Inspections')
                  {
                  TotalYieldMonth1 = TotalYieldMonth1 + newMonthlyExpenditure.Month_01__c;
                  TotalYieldMonth2 = TotalYieldMonth2 + newMonthlyExpenditure.Month_02__c;  
                  TotalYieldMonth3 = TotalYieldMonth3 + newMonthlyExpenditure.Month_03__c;
                  TotalYieldMonth4 = TotalYieldMonth4 + newMonthlyExpenditure.Month_04__c;
                  TotalYieldMonth5 = TotalYieldMonth5 + newMonthlyExpenditure.Month_05__c;
                  TotalYieldMonth6 = TotalYieldMonth6 + newMonthlyExpenditure.Month_06__c;
                  TotalYieldMonth7 = TotalYieldMonth7 + newMonthlyExpenditure.Month_07__c;
                  TotalYieldMonth8 = TotalYieldMonth8 + newMonthlyExpenditure.Month_08__c;
                  TotalYieldMonth9 = TotalYieldMonth9 + newMonthlyExpenditure.Month_09__c;
                  TotalYieldMonth10 = TotalYieldMonth10 + newMonthlyExpenditure.Month_10__c;
                  TotalYieldMonth11 = TotalYieldMonth11 + newMonthlyExpenditure.Month_11__c;
                  TotalYieldMonth12 = TotalYieldMonth12 + newMonthlyExpenditure.Month_12__c;
                  }
                  RollingMapWrapper newRollingMapWrapper =new RollingMapWrapper(newMonthlyExpenditure, TotalName);
                  CurrentYearMonthlyExpendituresTotalNew.add(newRollingMapWrapper);
                  
              }
             TotalMonthWiseListCurrentYear.add(TotalMonth1);
             TotalMonthWiseListCurrentYear.add(TotalMonth2);
             TotalMonthWiseListCurrentYear.add(TotalMonth3);
             TotalMonthWiseListCurrentYear.add(TotalMonth4);
             TotalMonthWiseListCurrentYear.add(TotalMonth5);
             TotalMonthWiseListCurrentYear.add(TotalMonth6);
             TotalMonthWiseListCurrentYear.add(TotalMonth7);
             TotalMonthWiseListCurrentYear.add(TotalMonth8);
             TotalMonthWiseListCurrentYear.add(TotalMonth9);
             TotalMonthWiseListCurrentYear.add(TotalMonth10);
             TotalMonthWiseListCurrentYear.add(TotalMonth11);
             TotalMonthWiseListCurrentYear.add(TotalMonth12);
             TotalYield = TotalYieldMonth1 + TotalYieldMonth2 + TotalYieldMonth3 + TotalYieldMonth4 + TotalYieldMonth5 + TotalYieldMonth6 +
                          TotalYieldMonth7 + TotalYieldMonth8 + TotalYieldMonth9 + TotalYieldMonth10 + TotalYieldMonth11 + TotalYieldMonth12;
             TotalNoOfSites = AccountPlanList[0].Site_Month_01__c.intValue() + AccountPlanList[0].Site_Month_02__c.intValue() +AccountPlanList[0].Site_Month_03__c.intValue() + AccountPlanList[0].Site_Month_04__c.intValue() + AccountPlanList[0].Site_Month_05__c.intValue()  + AccountPlanList[0].Site_Month_06__c.intValue()  + AccountPlanList[0].Site_Month_07__c.intValue()  +
                              AccountPlanList[0].Site_Month_08__c.intValue() + AccountPlanList[0].Site_Month_09__c.intValue() + AccountPlanList[0].Site_Month_10__c.intValue() + AccountPlanList[0].Site_Month_11__c.intValue() + AccountPlanList[0].Site_Month_12__c.intValue();
             if(TotalNoOfSites>0)
             TotalYield = TotalYield/ TotalNoOfSites;   
             else
             TotalYield = 0;
             if(AccountPlanList[0].Site_Month_01__c >0)
             TotalYieldListCurr.add(TotalYieldMonth1/AccountPlanList[0].Site_Month_01__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_02__c>0)
             TotalYieldListCurr.add(TotalYieldMonth2/AccountPlanList[0].Site_Month_02__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_03__c>0)
             TotalYieldListCurr.add(TotalYieldMonth3/AccountPlanList[0].Site_Month_03__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_04__c>0)
             TotalYieldListCurr.add(TotalYieldMonth4/AccountPlanList[0].Site_Month_04__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_05__c>0)
             TotalYieldListCurr.add(TotalYieldMonth5/AccountPlanList[0].Site_Month_05__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_06__c>0)
             TotalYieldListCurr.add(TotalYieldMonth6/AccountPlanList[0].Site_Month_06__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_07__c>0)
             TotalYieldListCurr.add(TotalYieldMonth7/AccountPlanList[0].Site_Month_07__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_08__c>0)
             TotalYieldListCurr.add(TotalYieldMonth8/AccountPlanList[0].Site_Month_08__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_09__c>0)
             TotalYieldListCurr.add(TotalYieldMonth9/AccountPlanList[0].Site_Month_09__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_10__c>0)
             TotalYieldListCurr.add(TotalYieldMonth10/AccountPlanList[0].Site_Month_10__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_11__c>0)
             TotalYieldListCurr.add(TotalYieldMonth11/AccountPlanList[0].Site_Month_11__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             if(AccountPlanList[0].Site_Month_12__c>0)
             TotalYieldListCurr.add(TotalYieldMonth12/AccountPlanList[0].Site_Month_12__c.setScale(2));
             else
             TotalYieldListCurr.add(0);
             
             TotalMonthlySumCurrent = TotalMonth1+ TotalMonth2 + TotalMonth3 + TotalMonth4 + TotalMonth5 + TotalMonth6 + TotalMonth7 + TotalMonth8 + TotalMonth9 + TotalMonth10 + TotalMonth11 + TotalMonth12;
          }
          system.debug('CurrentYearMonthlyExpendituresTotal-->' + CurrentYearMonthlyExpendituresTotal);
          system.debug('TotalYieldListCurr-->' + TotalYieldListCurr);
          
        if(MonthlyExpendituresPreviousYear!=NULL && MonthlyExpendituresPreviousYear.size()>0)
          { Decimal TotalMonth1= 0;
            Decimal TotalMonth2 = 0;
            Decimal TotalMonth3 = 0;
            Decimal TotalMonth4 = 0;
            Decimal TotalMonth5 = 0;
            Decimal TotalMonth6 = 0;
            Decimal TotalMonth7 = 0;
            Decimal TotalMonth8 = 0;
            Decimal TotalMonth9 = 0;
            Decimal TotalMonth10 = 0;
            Decimal TotalMonth11 = 0;
            Decimal TotalMonth12 = 0;
            /*Decimal TotalYieldMonth1= 0;
            Decimal TotalYieldMonth2 = 0;
            Decimal TotalYieldMonth3 = 0;
            Decimal TotalYieldMonth4 = 0;
            Decimal TotalYieldMonth5 = 0;
            Decimal TotalYieldMonth6 = 0;
            Decimal TotalYieldMonth7 = 0;
            Decimal TotalYieldMonth8 = 0;
            Decimal TotalYieldMonth9 = 0;
            Decimal TotalYieldMonth10 = 0;
            Decimal TotalYieldMonth11 = 0;
            Decimal TotalYieldMonth12 = 0;*/
            
              for(Monthly_Expenditure__c newMonthlyExpenditure: MonthlyExpendituresPreviousYear)
              {
                  Decimal TotalName = newMonthlyExpenditure.Month_01__c + newMonthlyExpenditure.Month_02__c + newMonthlyExpenditure.Month_03__c + newMonthlyExpenditure.Month_04__c +
                                      newMonthlyExpenditure.Month_05__c + newMonthlyExpenditure.Month_06__c + newMonthlyExpenditure.Month_07__c + newMonthlyExpenditure.Month_08__c +
                                      newMonthlyExpenditure.Month_09__c + newMonthlyExpenditure.Month_10__c + newMonthlyExpenditure.Month_11__c + newMonthlyExpenditure.Month_12__c;
                  TotalNameWiseListPreviousYear.add(TotalName);
                  TotalMonth1 = TotalMonth1 + newMonthlyExpenditure.Month_01__c;
                  TotalMonth2 = TotalMonth2 + newMonthlyExpenditure.Month_02__c;  
                  TotalMonth3 = TotalMonth3 + newMonthlyExpenditure.Month_03__c;
                  TotalMonth4 = TotalMonth4 + newMonthlyExpenditure.Month_04__c;
                  TotalMonth5 = TotalMonth5 + newMonthlyExpenditure.Month_05__c;
                  TotalMonth6 = TotalMonth6 + newMonthlyExpenditure.Month_06__c;
                  TotalMonth7 = TotalMonth7 + newMonthlyExpenditure.Month_07__c;
                  TotalMonth8 = TotalMonth8 + newMonthlyExpenditure.Month_08__c;
                  TotalMonth9 = TotalMonth9 + newMonthlyExpenditure.Month_09__c;
                  TotalMonth10 = TotalMonth10 + newMonthlyExpenditure.Month_10__c;
                  TotalMonth11 = TotalMonth11 + newMonthlyExpenditure.Month_11__c;
                  TotalMonth12 = TotalMonth12 + newMonthlyExpenditure.Month_12__c;
                 /* if(newMonthlyExpenditure.name == 'S+A' || newMonthlyExpenditure.name == 'Prop Serv Rec' || newMonthlyExpenditure.name == 'Inspections')
                  {
                  TotalYieldMonth1 = TotalYieldMonth1 + newMonthlyExpenditure.Month_01__c;
                  TotalYieldMonth2 = TotalYieldMonth2 + newMonthlyExpenditure.Month_02__c;  
                  TotalYieldMonth3 = TotalYieldMonth3 + newMonthlyExpenditure.Month_03__c;
                  TotalYieldMonth4 = TotalYieldMonth4 + newMonthlyExpenditure.Month_04__c;
                  TotalYieldMonth5 = TotalYieldMonth5 + newMonthlyExpenditure.Month_05__c;
                  TotalYieldMonth6 = TotalYieldMonth6 + newMonthlyExpenditure.Month_06__c;
                  TotalYieldMonth7 = TotalYieldMonth7 + newMonthlyExpenditure.Month_07__c;
                  TotalYieldMonth8 = TotalYieldMonth8 + newMonthlyExpenditure.Month_08__c;
                  TotalYieldMonth9 = TotalYieldMonth9 + newMonthlyExpenditure.Month_09__c;
                  TotalYieldMonth10 = TotalYieldMonth10 + newMonthlyExpenditure.Month_10__c;
                  TotalYieldMonth11 = TotalYieldMonth11 + newMonthlyExpenditure.Month_11__c;
                  TotalYieldMonth12 = TotalYieldMonth12 + newMonthlyExpenditure.Month_12__c;
                      
                  }*/
                  
                  
                  RollingMapWrapper newRollingMapWrapper =new RollingMapWrapper(newMonthlyExpenditure, TotalName);
                  PreviousYearMonthlyExpendituresTotalNew.add(newRollingMapWrapper);
              }
             TotalMonthWiseListPreviousYear.add(TotalMonth1);
             TotalMonthWiseListPreviousYear.add(TotalMonth2);
             TotalMonthWiseListPreviousYear.add(TotalMonth3);
             TotalMonthWiseListPreviousYear.add(TotalMonth4);
             TotalMonthWiseListPreviousYear.add(TotalMonth5);
             TotalMonthWiseListPreviousYear.add(TotalMonth6);
             TotalMonthWiseListPreviousYear.add(TotalMonth7);
             TotalMonthWiseListPreviousYear.add(TotalMonth8);
             TotalMonthWiseListPreviousYear.add(TotalMonth9);
             TotalMonthWiseListPreviousYear.add(TotalMonth10);
             TotalMonthWiseListPreviousYear.add(TotalMonth11);
             TotalMonthWiseListPreviousYear.add(TotalMonth12);
             TotalMonthlySumPrevious = TotalMonth1+ TotalMonth2 + TotalMonth3 + TotalMonth4 + TotalMonth5 + TotalMonth6 + TotalMonth7 + TotalMonth8 + TotalMonth9 + TotalMonth10 + TotalMonth11 + TotalMonth12;
             
          }
           
           
            system.debug('PreviousYearMonthlyExpendituresTotal-->' + PreviousYearMonthlyExpendituresTotal);
            for(Integer i = 0; i<12; i++)
            {   Date CurrentDate = Date.newInstance(AccountPlanCreatedDate.year(), AccountPlanCreatedDate.month()+i, AccountPlanCreatedDate.day());
                Date PreviousYearDate = Date.newInstance(AccountPlanCreatedDate.year()-1, AccountPlanCreatedDate.month()+i, AccountPlanCreatedDate.day());
                
                CurrentYearMonths.add(CurrentDate);
                PreviousYearMonths.add(PreviousYearDate);
            }
            system.debug('AccountPlanCreatedDate-->'+ AccountPlanCreatedDate);
            system.debug('PreviousYearMonths-->'+ PreviousYearMonths);
        
        prevYearDate = Date.newInstance(AccountPlanCreatedDate.year()-1, AccountPlanCreatedDate.month(), AccountPlanCreatedDate.day());
        CurrentDate =  Date.newInstance(AccountPlanCreatedDate.year(), AccountPlanCreatedDate.month(), AccountPlanCreatedDate.day());
        nextYear=CurrentDate.addYears(1);
        currYear = prevYearDate.addYears(1);
        Monthly_Expenditure__c newMonthlyExpenditure1 = new Monthly_Expenditure__c();
        RollingMapWrapper newRollingMapWrapper1 = new RollingMapWrapper(newMonthlyExpenditure1, 0);
        for(integer i=0;i<10;i++)
          CurrentYearMonthlyExpendituresTotal.add(newRollingMapWrapper1);
        for(integer i=0; i<9;i++)
          PreviousYearMonthlyExpendituresTotal.add(newRollingMapWrapper1);
        for(RollingMapWrapper newRollingMapWrapper: CurrentYearMonthlyExpendituresTotalNew)
        {
             if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Towers')
               {
               CurrentYearMonthlyExpendituresTotal[0] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'S+A')
                {
               CurrentYearMonthlyExpendituresTotal[1] = newRollingMapWrapper;
               }             
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Inspections')
                {
               CurrentYearMonthlyExpendituresTotal[2] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Prop Serv Rec')
                {
               CurrentYearMonthlyExpendituresTotal[3] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Guarding')
               {
               CurrentYearMonthlyExpendituresTotal[4] = newRollingMapWrapper;
               } 
              
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Guardians')
                {
               CurrentYearMonthlyExpendituresTotal[5] = newRollingMapWrapper;
               } 
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Glazing')
              {
               CurrentYearMonthlyExpendituresTotal[6] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Pest')
              {
               CurrentYearMonthlyExpendituresTotal[7] = newRollingMapWrapper;
               }             
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Grounds')
                {
               CurrentYearMonthlyExpendituresTotal[8] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Contracting')
                {
               CurrentYearMonthlyExpendituresTotal[9] = newRollingMapWrapper;
               }
        }
        
        for(RollingMapWrapper newRollingMapWrapper: PreviousYearMonthlyExpendituresTotalNew)
        {
             if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Towers')
               {
               PreviousYearMonthlyExpendituresTotal[0] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'S+A')
                {
               PreviousYearMonthlyExpendituresTotal[1] = newRollingMapWrapper;
               }             
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Recurring Rev')
                {
               PreviousYearMonthlyExpendituresTotal[2] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Guarding')
               {
               PreviousYearMonthlyExpendituresTotal[3] = newRollingMapWrapper;
               } 
              
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Guardians')
                {
               PreviousYearMonthlyExpendituresTotal[4] = newRollingMapWrapper;
               } 
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Glazing')
              {
               PreviousYearMonthlyExpendituresTotal[5] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Pest')
              {
               PreviousYearMonthlyExpendituresTotal[6] = newRollingMapWrapper;
               }             
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Grounds')
                {
               PreviousYearMonthlyExpendituresTotal[7] = newRollingMapWrapper;
               }
             else if(newRollingMapWrapper.newMonthlyExpenditure.Name == 'Contracting')
                {
               PreviousYearMonthlyExpendituresTotal[8] = newRollingMapWrapper;
               }
        }
        
        system.debug('CurrentYearMonthlyExpendituresTotal[0]-->' +CurrentYearMonthlyExpendituresTotal[0]);
        system.debug('CurrentYearMonthlyExpendituresTotal[1]-->'+CurrentYearMonthlyExpendituresTotal[1]);
    
 }
     
     
     public class RollingMapWrapper
     {
         public Monthly_Expenditure__c newMonthlyExpenditure{get;set;}
         public decimal TotalName {get;set;}
         public RollingMapWrapper(Monthly_Expenditure__c newMonthlyExpenditure, decimal TotalName)
         {
             if(newMonthlyExpenditure!=NULL)
             {
                 this.newMonthlyExpenditure = newMonthlyExpenditure;
             }
             if(TotalName!=NULL)
             {
                 this.TotalName= TotalName;
             }
             else
                 this.TotalName = 0;
         }
     }
     
   public PageReference saveCurrent()
    {   //accountplanid = Apexpages.currentPage().getParameters().get('id');
        //accountplanid = String.escapeSingleQuotes(accountplanid);
        List<Monthly_Expenditure__c> newMonthlyExpenditureList = new List<Monthly_Expenditure__c>();
        for(RollingMapWrapper newRollingMapWrapper: CurrentYearMonthlyExpendituresTotal)
        {   
            newMonthlyExpenditureList.add(newRollingMapWrapper.newMonthlyExpenditure);
            System.Debug(newRollingMapWrapper.newMonthlyExpenditure);
        }
        try{
            System.Debug('Trying Update');
        update newMonthlyExpenditureList;
        update AccountPlanWithDetails;
        }catch(Exception e)
        {
            System.debug('Error on Update:'+e);
        }
        return NULL;
    }
      public PageReference savePrevious()
    {	//accountplanid = Apexpages.currentPage().getParameters().get('id');
        //accountplanid = String.escapeSingleQuotes(accountplanid);
        system.debug('PreviousYearMonthlyExpendituresTotal-->' + PreviousYearMonthlyExpendituresTotal);
       List<Monthly_Expenditure__c> newMonthlyExpenditureList = new List<Monthly_Expenditure__c>();
        for(RollingMapWrapper newRollingMapWrapper: PreviousYearMonthlyExpendituresTotal)
        {   
            newMonthlyExpenditureList.add(newRollingMapWrapper.newMonthlyExpenditure);
            System.Debug(newRollingMapWrapper.newMonthlyExpenditure);
        }
        try{
            System.Debug('Trying Update');
        update newMonthlyExpenditureList;
        }catch(Exception e)
        {
            System.debug('Error on Update:'+e);
        }
        return NULL;
        
    }

}