/**
 *  Class Name: CheckRecursive
 *  Description: This is a class to prevent recursion in Trigger
 *  CreatedDate: 24/05/2016
 *
 */
public Class CheckRecursive{
    public static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
}