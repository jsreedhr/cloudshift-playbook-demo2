@isTest
/**
 *  Class Name: Test_DisplayContactsController
 *  Description: This is a class for testing Test_DisplayContactsController
 *  Company: dQuotient
 *  CreatedDate: 30/06/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran               30/06/2016                  Orginal Version
 *
 */
private class Test_DisplayContactsController  {
    static DisplayContactsControllerExt ext;
    static DisplayContactsControllerExt next;
    
    static PageReference page;
    
    
 /**
 *  Method Name:    runPositiveTestCases
 *  Description:    This is a method is for testing the DisplayContactsController
 */
    static testMethod void runPositiveTestCases() {
        Profile objProfile = [select id from profile where name='System Administrator' limit 1]; 
        User objUser = new User(alias = 'standt', email='standarduserTestData@testorg.com',emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = objProfile.Id, timezonesidkey='America/Los_Angeles', username='standarduser2TestData@testorg.com');

        insert objUser;
        System.RunAs(objUser){
            Test.startTest();
            Account objAccount = new Account();
            objAccount.Name = 'Test Account';
            objAccount.Website='www.test.com';
            insert objAccount;
            Contact objContact = new Contact();
            objContact.AccountId = objAccount.id;
            objContact.LastName = 'Test';
            objContact.Email = 'test@test.com';
            objContact.Phone='982222322';
            insert objContact;
            system.assert(objContact.id!=null);
            page = new PageReference(objAccount.id);
            ApexPages.standardController controller = new ApexPages.StandardController(objAccount);
            ext = new DisplayContactsControllerExt (controller);
            next=new DisplayContactsControllerExt(null);
            Test.StopTest();
         }     
      
        }

}