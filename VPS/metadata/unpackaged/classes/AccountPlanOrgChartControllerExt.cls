/**
 *  @Class Name:    AccountPlanOrgChartControllerExt 
 *  @Description:   This is a controller extension for AccountPlanPage
 *  @Company: dQuotient
 *  CreatedDate: 30/08/2017
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aishwarya SK        30/08/2017                 Original Version
 *
 */
 
public with sharing class AccountPlanOrgChartControllerExt{
    
    //public String accountplanid;
    public Account_Plan__c objAccPlan{get;set;}
    public List<Account_Plan_Contact__c> AccountPlanContList{get;set;}
    public List<AccountPlanOrgChartWrapper> listAccPlanOrg{get;set;}
    
    /**
     *  Method Name: AccountPlanOrgChartControllerExt
     *  Description: This is the constructor for AccountPlanOrgChartControllerExt class
     *  Param: ApexPages.StandardController
     *  Return: None
    */
    
    public AccountPlanOrgChartControllerExt(ApexPages.StandardController stdController){
        
        if(stdController != null){
            
            objAccPlan = (Account_Plan__c)stdController.getRecord();
            AccountPlanContList=new List<Account_Plan_Contact__c>();
            listAccPlanOrg = new List<AccountPlanOrgChartWrapper>();
            
            AccountPlanContList=[select Name,Account_Plan_Contact__c,Account_Plan_Contact__r.Name,Parent_Contact__r.Name,Position__c from Account_Plan_Contact__c where Account_Plan__c =:objAccPlan.id];
            if(AccountPlanContList!=null &&AccountPlanContList.size()>0){
                for(Account_Plan_Contact__c contactObj : AccountPlanContList)
                {
                    String contactName;
                    String contactParentName;
                    String contactPosition;
                    
                    if(contactObj.Account_Plan_Contact__c!=NULL && contactObj.Account_Plan_Contact__r.Name!=NULL)
                        contactName = contactObj.Account_Plan_Contact__r.Name;
                    else 
                        contactName = '';
                    if(contactObj.Account_Plan_Contact__c!=NULL && contactObj.Parent_Contact__r.Name!=NULL)
                        contactParentName = contactObj.Parent_Contact__r.Name;
                    else 
                        contactParentName = '';
                    if(contactObj.Account_Plan_Contact__c!=NULL && contactObj.Position__c!=NULL)
                        contactPosition = contactObj.Position__c;
                    else 
                        contactPosition = '';
                    AccountPlanOrgChartWrapper wrapperObj2 = New AccountPlanOrgChartWrapper(String.escapeSingleQuotes(contactName),String.escapeSingleQuotes(contactParentName),String.escapeSingleQuotes(contactPosition) );
                    listAccPlanOrg.add(wrapperObj2);
                }
            }
        } 
    }
    
     /*
     * Wrapper Class name: AccountPlanOrgChartWrapper
     * Author: Aishwarya SK
     * Description: wrapper class for AccountPlanOrgChartControllerExt 
     * 
     */
    public class  AccountPlanOrgChartWrapper {
        
        public String accPlanContName {get;set;}
        public String accPlanContParentName {get;set;}
        public String accPlanContPosition {get;set;}
        
        
        public AccountPlanOrgChartWrapper(String accPlanContName,String accPlanContParentName, String accPlanContPosition)
        {
            this.accPlanContName = accPlanContName;
            this.accPlanContParentName = accPlanContParentName;
            this.accPlanContPosition = accPlanContPosition;
        }
    }
    
     
}