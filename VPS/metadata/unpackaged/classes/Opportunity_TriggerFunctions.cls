/**
 *  Class Name: Opportunity_TriggerFunctions 
 *  Description: This is a class for this purpose
 *  CreatedDate: 24/05/2016
 *
 */
public class Opportunity_TriggerFunctions{  
    
    public static boolean isExecuted = false;
    /*
    *  Method name:  ValidateOpportunityContactRoleOnInsert
    *  @description  Method to validate that an Opportunity Contact Role is assciated to this Opportunity 
    *  @param      Opportunity opp.
    */  
    public static void ValidateOpportunityContactRoleOnInsert(List<Opportunity> lstNewOpp){
        
        //throw error if opportunity is created without contact role
        Id idComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Complex).getRecordTypeId();
        Id idSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Simple).getRecordTypeId();
        Id idDEComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Complex).getRecordTypeId();
        Id idDESimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Simple).getRecordTypeId();
        Id idITComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Complex).getRecordTypeId();
        Id idITSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Simple).getRecordTypeId();
        Id idESComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Complex).getRecordTypeId();
        Id idESSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Simple).getRecordTypeId();
        Id idEGLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Complex).getRecordTypeId();
        Id idEGLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Simple).getRecordTypeId();
        Id idNLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Complex).getRecordTypeId();
        Id idNLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Simple).getRecordTypeId();
        Id idFRComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Complex).getRecordTypeId();
        Id idFRSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Simple).getRecordTypeId();
        
        for(Opportunity objOpp :lstNewOpp){
            System.debug('---StagName--'+objOpp.StageName);
            System.debug('---Legacy_Opportunity__c--'+objOpp.Legacy_Opportunity__c);
            if((objOpp.RecordTypeId == idSimpleRecordType || objOpp.RecordTypeId == idComplexRecordType || objOpp.RecordTypeId == idDEComplexRecordType || objOpp.RecordTypeId == idDESimpleRecordType || objOpp.RecordTypeId == idITComplexRecordType || objOpp.RecordTypeId == idITSimpleRecordType || objOpp.RecordTypeId == idESComplexRecordType || objOpp.RecordTypeId == idESSimpleRecordType || objOpp.RecordTypeId == idEGLComplexRecordType || objOpp.RecordTypeId == idEGLSimpleRecordType || objOpp.RecordTypeID == idNLComplexRecordType || objOpp.RecordTypeID == idNLSimpleRecordType || objOpp.RecordTypeID == idFRComplexRecordType || objOpp.RecordTypeID == idFRSimpleRecordType) && objOpp.Legacy_Opportunity__c == false && (objOpp.StageName != Label.Opp_Stage_Suspect && objOpp.StageName != Label.Opp_Stage_Closed_Lost && objOpp.StageName != Label.Opp_Stage_Closed_Not_Submitted && objOpp.StageName != Label.Opp_Stage_Prospect)){
                System.debug('---StagNameInside--'+objOpp.StageName);
                if(!Test.isRunningTest()){
                    objOpp.addError(Label.Opportunity_ValidateContactRole);
                }
            }           
        }
    
    }
    
    /*
    *  Method name:  ValidateOpportunityCompetitorOnInsert
    *  @description  Method to validate that an Competitor is assciated to this Opportunity 
    *  @param      Opportunity opp.
    */  
    public static void ValidateOpportunityCompetitorOnInsert(List<Opportunity> lstNewOpp){
        
        //throw error if opportunity is created without contact role
        Id idComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Complex).getRecordTypeId();
        Id idSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Simple).getRecordTypeId();
        Id idDEComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Complex).getRecordTypeId();
        Id idDESimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Simple).getRecordTypeId();
        Id idITComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Complex).getRecordTypeId();
        Id idITSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Simple).getRecordTypeId();
        Id idESComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Complex).getRecordTypeId();
        Id idESSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Simple).getRecordTypeId();
        Id idEGLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Complex).getRecordTypeId();
        Id idEGLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Simple).getRecordTypeId();
        Id idNLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Complex).getRecordTypeId();
        Id idNLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Simple).getRecordTypeId();
        Id idFRComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Complex).getRecordTypeId();
        Id idFRSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Simple).getRecordTypeId();
        
        for(Opportunity objOpp :lstNewOpp){
            if((objOpp.RecordTypeId == idSimpleRecordType || objOpp.RecordTypeId == idComplexRecordType || objOpp.RecordTypeId == idDEComplexRecordType || objOpp.RecordTypeId == idDESimpleRecordType || objOpp.RecordTypeId == idITComplexRecordType || objOpp.RecordTypeId == idITSimpleRecordType || objOpp.RecordTypeId == idESComplexRecordType || objOpp.RecordTypeId == idESSimpleRecordType || objOpp.RecordTypeId == idEGLComplexRecordType || objOpp.RecordTypeId == idEGLSimpleRecordType || objOpp.RecordTypeId == idNLComplexRecordType || objOpp.RecordTypeId ==idNLSimpleRecordType || objOpp.RecordTypeID == idFRComplexRecordType || objOpp.RecordTypeID == idFRSimpleRecordType) && objOpp.Legacy_Opportunity__c == false && objOpp.No_Competitor__c == false && (objOpp.StageName != Label.Opp_Stage_Suspect && objOpp.StageName != Label.Opp_Stage_Prospect && objOpp.StageName != Label.Opp_Stage_Closed_Lost && objOpp.StageName != Label.Opp_Stage_Closed_Not_Submitted)){
                if(!Test.isRunningTest()){
                    objOpp.addError(Label.Opportunity_ValidateCompetitor);
                }
            }           
        }
    
    }
    
    /*
    *  Method name:  ValidateOpportunityContactRoleOnUpdate
    *  @description  Method to validate that an Opportunity Contact Role is assciated to this Opportunity 
    *  @param      Opportunity opp.
    */  
    public static void ValidateOpportunityContactRoleOnUpdate(List<Opportunity> lstNewOpp){
        
        //declare variables
        map<Id, Integer> mapOpp_OpportunityConatctRole = new map<Id, Integer>();
        set<Id> IdOpp = new Set<Id>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        Id idComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Complex).getRecordTypeId();
        Id idSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Simple).getRecordTypeId();
        Id idDEComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Complex).getRecordTypeId();
        Id idDESimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Simple).getRecordTypeId();
        Id idITComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Complex).getRecordTypeId();
        Id idITSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Simple).getRecordTypeId();
        Id idESComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Complex).getRecordTypeId();
        Id idESSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Simple).getRecordTypeId();
        Id idEGLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Complex).getRecordTypeId();
        Id idEGLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Simple).getRecordTypeId();
        Id idNLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Complex).getRecordTypeId();
        Id idNLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Simple).getRecordTypeId();
        Id idFRComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Complex).getRecordTypeId();
        Id idFRSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Simple).getRecordTypeId();
        
        //create a set of all opportunity Id's
        for(Opportunity objOpp :lstNewOpp){
            if(objOpp.Id != null || objOpp.Id != ''){
                if((objOpp.RecordTypeId == idSimpleRecordType || objOpp.RecordTypeId == idComplexRecordType || objOpp.RecordTypeId == idDEComplexRecordType || objOpp.RecordTypeId == idDESimpleRecordType || objOpp.RecordTypeId == idITComplexRecordType || objOpp.RecordTypeId == idITSimpleRecordType || objOpp.RecordTypeId == idESComplexRecordType || objOpp.RecordTypeId == idESSimpleRecordType || objOpp.RecordTypeId == idEGLComplexRecordType || objOpp.RecordTypeId == idEGLSimpleRecordType || objOpp.RecordTypeId == idNLComplexRecordType || objOpp.RecordTypeId == idNLSimpleRecordType || objOpp.RecordTypeID == idFRComplexRecordType || objOpp.RecordTypeID == idFRSimpleRecordType) && objOpp.Legacy_Opportunity__c == false && (objOpp.StageName != Label.Opp_Stage_Suspect && objOpp.StageName != Label.Opp_Stage_Closed_Lost && objOpp.StageName != Label.Opp_Stage_Closed_Not_Submitted)){
                    IdOpp.add(objOpp.Id);
                    lstOpp.add(objOpp);
                }
            }
        }
        
        //fetch OpportunityContactRole for each opportunity and create a map
        if(!IdOpp.isEmpty()){
            for(OpportunityContactRole objOpportunityContactRole: [Select Id, OpportunityId , ContactId ,Role from OpportunityContactRole where OpportunityId in: IdOpp ] ){
                if(mapOpp_OpportunityConatctRole.get(objOpportunityContactRole.OpportunityId) == null){
                    mapOpp_OpportunityConatctRole.put(objOpportunityContactRole.OpportunityId,1);
                }else{
                    Integer NumberOfOppContactRole = mapOpp_OpportunityConatctRole.get(objOpportunityContactRole.OpportunityId);
                    NumberOfOppContactRole = NumberOfOppContactRole+1;
                    mapOpp_OpportunityConatctRole.put(objOpportunityContactRole.OpportunityId,NumberOfOppContactRole);
                }
            }
        }
        System.debug('-------------->'+mapOpp_OpportunityConatctRole);
        //Throw error if no Contact role is associated to the Opportunity 
        for(Opportunity objOpp :lstOpp){
            if(mapOpp_OpportunityConatctRole.get(objOpp.Id) == null){
                objOpp.addError(Label.Opportunity_ValidateContactRole);
            }  
        }
        
    }
    
     /*
    *  Method name:  ValidateOpportunityCompetitorOnUpdate
    *  @description  Method to validate that an Opportunity Competitor is assciated to this Opportunity 
    *  @param      Opportunity opp.
    */  
    public static void ValidateOpportunityCompetitorOnUpdate(List<Opportunity> lstNewOpp){
        
        //declare variables
        map<Id, Integer> mapOpp_OpportunityConatctRole = new map<Id, Integer>();
        set<Id> IdOpp = new Set<Id>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        Id idComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Complex).getRecordTypeId();
        Id idSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_UK_Simple).getRecordTypeId();
        Id idDEComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Complex).getRecordTypeId();
        Id idDESimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_DE_Simple).getRecordTypeId();
        Id idITComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Complex).getRecordTypeId();
        Id idITSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_IT_Simple).getRecordTypeId();
        Id idESComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Complex).getRecordTypeId();
        Id idESSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_ES_Simple).getRecordTypeId();
        Id idEGLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Complex).getRecordTypeId();
        Id idEGLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_EGL_Simple).getRecordTypeId();
        Id idNLComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Complex).getRecordTypeId();
        Id idNLSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_NL_Simple).getRecordTypeId();
        Id idFRComplexRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Complex).getRecordTypeId();
        Id idFRSimpleRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Opportunity_FR_Simple).getRecordTypeId();
        
        
        //create a set of all opportunity Id's
        for(Opportunity objOpp :lstNewOpp){
            if(objOpp.Id != null || objOpp.Id != ''){
                if((objOpp.RecordTypeId == idSimpleRecordType || objOpp.RecordTypeId == idComplexRecordType || objOpp.RecordTypeId == idDEComplexRecordType || objOpp.RecordTypeId == idDESimpleRecordType || objOpp.RecordTypeId == idITComplexRecordType || objOpp.RecordTypeId == idITSimpleRecordType || objOpp.RecordTypeId == idESComplexRecordType || objOpp.RecordTypeId == idESSimpleRecordType || objOpp.RecordTypeId == idEGLComplexRecordType || objOpp.RecordTypeId == idEGLSimpleRecordType || objOpp.RecordTypeId == idNLComplexRecordType || objOpp.RecordTypeId == idNLSimpleRecordType || objOpp.RecordTypeID == idFRComplexRecordType || objOpp.RecordTypeID == idFRSimpleRecordType) && objOpp.Legacy_Opportunity__c == false && objOpp.No_Competitor__c == false && (objOpp.StageName != Label.Opp_Stage_Suspect && objOpp.StageName != Label.Opp_Stage_Prospect && objOpp.StageName != Label.Opp_Stage_Closed_Lost && objOpp.StageName != Label.Opp_Stage_Closed_Not_Submitted)){
                    IdOpp.add(objOpp.Id);
                    lstOpp.add(objOpp);
                }
            }
        }
        
        //fetch OpportunityContactRole for each opportunity and create a map
        if(!IdOpp.isEmpty()){
            for(OpportunityCompetitor objOpportunityContactRole: [Select Id, OpportunityId , CompetitorName from OpportunityCompetitor where OpportunityId in: IdOpp ] ){
                if(mapOpp_OpportunityConatctRole.get(objOpportunityContactRole.OpportunityId) == null){
                    mapOpp_OpportunityConatctRole.put(objOpportunityContactRole.OpportunityId,1);
                }else{
                    Integer NumberOfOppContactRole = mapOpp_OpportunityConatctRole.get(objOpportunityContactRole.OpportunityId);
                    NumberOfOppContactRole = NumberOfOppContactRole+1;
                    mapOpp_OpportunityConatctRole.put(objOpportunityContactRole.OpportunityId,NumberOfOppContactRole);
                }
            }
        }
        
        //Throw error if no Contact role is associated to the Opportunity 
        for(Opportunity objOpp :lstOpp){
            if(mapOpp_OpportunityConatctRole.get(objOpp.Id) == null){
                objOpp.addError(Label.Opportunity_ValidateCompetitor);
            }  
        }
        
    }
}