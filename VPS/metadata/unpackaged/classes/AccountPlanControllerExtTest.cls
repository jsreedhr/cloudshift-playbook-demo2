@isTest
private class AccountPlanControllerExtTest {

	private static testMethod void AccountPlanControllerExtTestMethod() 
	{
      Account newAccount = new Account(name = 'abc123');
	    insert newAccount;
	    Opportunity newOpp = new Opportunity(name='Opp123', Account = newAccount, Service_Centre__c = 'DUBLIN', StageName = 'Suspect', CloseDate= date.today(), 
	    LeadSource = 'Cold Call', Portfolio_Client_Name__c= 'New Client');
	    insert newOpp;
	    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com'); 

        Account_Plan__c newAccountPlan = new Account_Plan__c(name='TestPlanAccount', Account_Plan__c = newAccount.id, Type__c = 'Grow', Division__c ='GLD',
                                                             VPS_Lead__c = u.id, End_Date__c = Date.today(), Status__c = 'Draft');
        insert newAccountPlan;
        Account_Plan_Opportunity__c newAccountPlanOpp = new Account_Plan_Opportunity__c(Account_Plan__c=newAccountPlan.id, Opportunity__c = newOpp.id);
        insert newAccountPlanOpp;
        task t = new task(
        WhatID = newAccountPlanOpp.id,
        Subject='Donni',
        Status='Completed',
        Priority='Normal',
        ActivityDate = Date.today());
        insert t;
        Event e= new Event(WhatId = newAccountPlanOpp.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = system.now());
        insert e;
        Contact cont = new Contact();
        cont.FirstName='Test1';
        cont.LastName='Test1';
        cont.Accountid= newAccount.id;
        insert cont;
        Contact newcont = new Contact();
        newcont.FirstName='Test2';
        newcont.LastName='Test2';
        newcont.Accountid= newAccount.id;
        insert newcont;
        Account_Plan_Contact__c newAccountPlanContact = new Account_Plan_Contact__c(Account_Plan_Contact__c = cont.id, Account_Plan__c = newAccountPlan.id, Buying_Role__c = 'Decision Maker', Attitude__c='Promoter', MapType__c= 'National Procurement');
        insert newAccountPlanContact;
        Account_Plan_Contact__c newAccountPlanContact2 = new Account_Plan_Contact__c(Account_Plan_Contact__c = newcont.id, Account_Plan__c = newAccountPlan.id, Buying_Role__c = 'Decision Maker', Attitude__c='Promoter', MapType__c= 'Local Procurement');
        insert newAccountPlanContact2;
         task t1 = new task(
        WhoID = cont.id,
        Subject='Donni',
        Status='Completed',
        Priority='Normal',
        ActivityDate = Date.today());
        insert t1;
        task t2 = new task(
        WhoID = newcont.id,
        Subject='Donni',
        Status='Completed',
        Priority='Normal',
        ActivityDate = Date.today());
        insert t2;
        Event e1= new Event(WhoId = cont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = system.now());
        Event e2= new Event(WhoId = newcont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = system.now());
        insert e1;
        insert e2;
        DateTime mydate = Datetime.newInstance(system.Today().year(),Date.Today().month()-1,Date.Today().day(), 0, 0, 0);
        Event e3= new Event(WhoId = cont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = mydate);
        DateTime mydate1 = Datetime.newInstance(Date.Today().year(),Date.Today().month()-1,Date.Today().day()-1, 0, 0, 0);
        Event e4= new Event(WhoId = newcont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = mydate);
        insert e3;
        insert e4;
         task t3 = new task(
        WhoID = cont.id,
        Subject='Donni',
        Status='Completed',
        Priority='Normal',
        ActivityDate = Date.newInstance(mydate1.year(), mydate1.month(), mydate1.day()));
        insert t3;
        task t4 = new task(
        WhoID = newcont.id,
        Subject='Donni',
        Status='Completed',
        Priority='Normal',
        ActivityDate = Date.newInstance(mydate.year(), mydate.month(), mydate.day()));
        insert t4;
        
        /*Event e3= new Event(WhId = cont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = system.now());
        Event e4= new Event(WhatId = newcont.id, Subject = 'Donni', DurationInMinutes = 60, ActivityDateTime = system.now());
        insert e3;
        insert e4;*/




        ApexPages.currentPage().getParameters().put('id',newAccountPlan.id);
        system.debug('ApexPages.currentPage().getParameters().get-->' + ApexPages.currentPage().getParameters().get('id'));
        ApexPages.StandardController stdLead = new ApexPages.StandardController(newAccountPlan);
        AccountPlanControllerExt newControllerExt = new AccountPlanControllerExt(stdLead);
	}

}