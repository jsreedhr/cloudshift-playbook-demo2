/**
 *  Class Name: Test_CheckRecursive
 *  Description: This is a class to prevent recursion in Trigger
 *  CreatedDate: 24/05/2016
 *
 */
 @isTest(seeAllData=false)
public Class Test_CheckRecursive{
    
    /*
     *  Method name:   checkRun
     *  @description   Tests method that tests runOnce method 
    */        
    static testMethod void checkRun() {
        Test.startTest();
            boolean result  = CheckRecursive.runOnce();
            System.assertEquals(result, true);
            result = checkRecursive.runOnce();
            System.assertEquals(result, false);
        
        Test.stopTest();
    }
}