/**
 *  @Class Name:    DisplayContactsController
 *  @Description:   This is the controller for displaying all contacts of an account
 *  @Company: dQuotient
 *  CreatedDate:  28/06/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Anand              28/06/2016                  Orginal Version
 */
 public with sharing class DisplayContactsControllerExt{
    
    private Account objAccount;
    public List<Contact> lstContactDisplay{get; set;}
    public boolean hasContacts{get; set;}
    private List<Account> lstAccount;
    private map<id, Account> mapIdAccount;
    
    /**
     *  Method Name: DisplayContactsControllerExt
     *  Description: Constructor for DisplayContactsControllerExt
     *  Param: None
     *  Return: None
    */ 
    public DisplayContactsControllerExt(ApexPages.StandardController stdController){
        
        
        
        List<Contact> lstContact = new List<Contact>();
        set<id> setIds = new set<id>();
        lstContactDisplay = new List<Contact>();
        hasContacts = false;
        
        if(stdController != null){
            objAccount = (Account)stdController.getRecord();
            setIds.add(objAccount.id);
            integer noOFIterations = 1;
            while(!setIds.isEmpty()){
                lstContact = new List<Contact>();
                if(noOFIterations == 1){
                    lstContact =[Select id, name,
                                            FirstName, 
                                            LastName, 
                                            Title, 
                                            Salutation, 
                                            Email, 
                                            AccountId, 
                                            Account.Name,
                                            Account.Account_Name_Department__c,
                                            Account.ParentId,
                                            Account.Parent.Name,
                                            Account.Parent.ParentId,
                                            Account.Parent.Parent.Name, 
                                            Account.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Parent.Name,
                                            Phone, 
                                            MobilePhone,
                                            OwnerId,
                                            Owner.Name
                                            From 
                                            Contact
                                            where 
                                            AccountId in: setIds
                                            or Account.ParentId in: setIds
                                            or Account.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.Parent.ParentId in: setIds
                                            order by Account.Parent.Parent.Parent.Parent.Name, Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Name, Account.Parent.Name, Account.Name, Name];
                                        
                }else{
                    lstContact =[Select id, name,
                                            FirstName, 
                                            LastName, 
                                            Title, 
                                            Salutation, 
                                            Email, 
                                            AccountId, 
                                            Account.Name,
                                            Account.Account_Name_Department__c,
                                            Account.ParentId,
                                            Account.Parent.Name,
                                            Account.Parent.ParentId,
                                            Account.Parent.Parent.Name, 
                                            Account.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Parent.ParentId,
                                            Account.Parent.Parent.Parent.Parent.Name,
                                            Phone, 
                                            MobilePhone,
                                            OwnerId,
                                            Owner.Name
                                            From 
                                            Contact
                                            where 
                                            Account.ParentId in: setIds
                                            or Account.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.ParentId in: setIds
                                            or Account.Parent.Parent.Parent.ParentId in: setIds
                                            order by Account.Parent.Parent.Parent.Parent.Name, Account.Parent.Parent.Parent.Name,
                                            Account.Parent.Parent.Name, Account.Parent.Name, Account.Name, Name];
                    
                }
                                        
                setIds = new set<id>();                     
                for(Contact objContact: lstContact){
                    if(objContact.Account.Parent.Parent.Parent.ParentId != null){
                        setIds.add(objContact.AccountId);
                    }
                }
                lstContactDisplay.addAll(lstContact);
                noOFIterations = noOFIterations+1;
                if(setIds.size() == 0){
                    break;
                }
            }
            if(!lstContactDisplay.isEmpty())
                hasContacts = true;
            System.debug('---ListOfContacts---'+lstContactDisplay);
        }
    }
 }