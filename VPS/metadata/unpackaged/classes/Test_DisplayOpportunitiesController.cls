@isTest
/**
 *  Class Name: Test_DisplayOpportunitiesController
 *  Description: This is a class for testing Test_DisplayOpportunitiesController
 *  Company: dQuotient
 *  CreatedDate: 04/07/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran               04/06/2016                  Orginal Version
 *
 */
private class Test_DisplayOpportunitiesController{
    static DisplayOpportunitiesControllerExt ext;
    static DisplayOpportunitiesControllerExt next;
    
    static PageReference page;
    
    
 /**
 *  Method Name:    runPositiveTestCases
 *  Description:    This is a method is for testing the DisplayOpportunitiesController
 */
    static testMethod void runPositiveTestCases() {
        Profile objProfile = [select id from profile where name='System Administrator' limit 1]; 
        User objUser = new User(alias = 'standt', email='standarduserTestData@testorg.com',emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = objProfile.Id, timezonesidkey='America/Los_Angeles', username='standarduser2TestData@testorg.com');

        insert objUser;
        System.RunAs(objUser){
            Test.startTest();
            Account objAccount = new Account();
            objAccount.Name = 'Test Account';
            objAccount.Website='www.test.com';
            insert objAccount;
            Opportunity objOpp = new Opportunity ();
            objOpp.stageName='Open';
            objOpp.Name='New Opp';
            objOpp.CloseDate=system.today();
            objOpp.AccountId = objAccount.id;
            insert objOpp;
            system.assert(objOpp.id!=null);
            Apexpages.currentPage().getParameters().put('selectedStageType','Won');
            page = new PageReference(objAccount.id);
            ApexPages.standardController controller = new ApexPages.StandardController(objAccount);
            system.assert(objOpp!=null);
            ext = new DisplayOpportunitiesControllerExt (controller);
            ext.StageOpp();
            next=new DisplayOpportunitiesControllerExt(null);
            Test.StopTest();
         }     
      
        }

        /**
 *  Method Name:    runPositiveTestCasesNew
 *  Description:    This is a method is for testing the DisplayOpportunitiesController
 */
    static testMethod void runPositiveTestCasesNew() {
        Profile objProfile = [select id from profile where name='System Administrator' limit 1]; 
        User objUser = new User(alias = 'standt', email='standarduserTestData@testorg.com',emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = objProfile.Id, timezonesidkey='America/Los_Angeles', username='standarduser2TestData@testorg.com');

        insert objUser;
        System.RunAs(objUser){
            Test.startTest();
            Account objAccount = new Account();
            objAccount.Name = 'Test Account';
            objAccount.Website='www.test.com';
            insert objAccount;
            Opportunity objOpp = new Opportunity ();
            objOpp.stageName='Suspect';
            objOpp.Name='New Opp';
            objOpp.CloseDate=system.today().addDays(-11);
            objOpp.AccountId = objAccount.id;
            insert objOpp;
            Competitors__c oppComp = new Competitors__c();
            oppComp.Name = 'Competitor';
            insert oppComp;
            objOpp.StageName = 'Closed Lost';
            objOpp.Comp_Lost_To2__c = oppComp.id;
            objOpp.Lessons_Learned__c='good';
            update objOpp;
            system.assert(objOpp.id!=null);
            Apexpages.currentPage().getParameters().put('selectedStageType','Won');
            page = new PageReference(objAccount.id);
            ApexPages.standardController controller = new ApexPages.StandardController(objAccount);
            system.assert(objAccount!=null);
            ext = new DisplayOpportunitiesControllerExt (controller);
            ext.StageOpp();
            next=new DisplayOpportunitiesControllerExt(null);
            Test.StopTest();
         }     
      
        }


}