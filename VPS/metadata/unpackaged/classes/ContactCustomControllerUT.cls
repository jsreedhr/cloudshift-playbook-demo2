/**
 * Class ContactCustomControllerUT : unit test pour la classe ContactCustomController
 * @author : Eric Wartelle
 * @version : 0.1
 */
@isTest
public class ContactCustomControllerUT {
    public static testMethod void testContactCustomController(){      
        //Start unit test
       	User currentUser = new User(Username = 'e.wartelle@experis-it.fr', LastName = 'Wartelle', Alias = 'Eric', CommunityNickname = 'TontonLeric',
                                     Email = 'eric.wartelle@experis-it.fr', TimeZoneSidKey = 'Europe/Paris', emailencodingkey = 'UTF-8', localesidkey = 'fr_FR',
                                     languagelocalekey = 'fr', profileId = '00ew00000012I1zAAE', CompanyName = 'Experis');       
        System.Test.startTest();
                
        System.runAs(currentUser) {
            Contact c = new Contact(LastName = 'Test',email='test@gmail.com',phone='8233223223');
            insert c;
            ApexPages.StandardController myStandardController = new ApexPages.StandardController(c);
            ContactCustomController myContactCustomController = new ContactCustomController(myStandardController);
            myContactCustomController.valideUser();
        }

        System.Test.stopTest();
        //Stop unit test
    }
}