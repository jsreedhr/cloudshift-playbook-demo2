<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_de_cr_ation_de_compte</fullName>
        <description>Notification de création de compte</description>
        <protected>false</protected>
        <recipients>
            <recipient>FR_Service_ADV_Fact</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FR_Envoi_de_notification_cr_ation_de_compte</template>
    </alerts>
    <alerts>
        <fullName>Notification_de_modification_du</fullName>
        <description>Notification de modification du code métier du compte</description>
        <protected>false</protected>
        <recipients>
            <recipient>FR_Service_ADV_Fact</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_modification</template>
    </alerts>
    <fieldUpdates>
        <fullName>FR_Mise_jour_condition_de_paiement</fullName>
        <field>Conditions_de_reglement__c</field>
        <literalValue>30 jours fin de mois</literalValue>
        <name>FR Mise à jour condition de paiement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_Mise_jour_facturation</fullName>
        <field>Mode_de_reglement__c</field>
        <literalValue>VIREMENT</literalValue>
        <name>FR Mise à jour facturation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Account_Billing_Country</fullName>
        <description>Takes the country picklist value and pushes into the billing country on the account.</description>
        <field>BillingCountry</field>
        <formula>CASE(Country_Picklist__c,
&quot;United Kingdom&quot;, &quot;United Kingdom&quot;,
&quot;United States&quot;, &quot;United States&quot;,
&quot;Italy&quot;, &quot;Italia&quot;,
&quot;Spain&quot;, &quot;España&quot;,
&quot;Germany&quot;, &quot;Deutschland&quot;,
&quot;&quot;)</formula>
        <name>JETCLOUD: Update Account Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Type_New_Customer</fullName>
        <field>Type</field>
        <literalValue>New Customer</literalValue>
        <name>JETCLOUD: Update Type to New Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Type_to_Existing_Custom</fullName>
        <description>Updates the account type to existing customer</description>
        <field>Type</field>
        <literalValue>Existing Customer</literalValue>
        <name>JETCLOUD: Update Type to Existing Custom</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_the_Total_Estimated_Opp</fullName>
        <description>Update the Total Estimated Office Opportunity date field when a change has been made to the estimated Opportunity amount field.</description>
        <field>Total_Estimated_Office_Opportunity_Date__c</field>
        <formula>TODAY()</formula>
        <name>JETCLOUD: Update the Total Estimated Opp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copycity</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>copycity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copycountry</fullName>
        <field>BillingCountry</field>
        <formula>BillingCountry</formula>
        <name>copycountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copypstcode</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>copypstcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copystate</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>copystate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copystreet</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>copystreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>FR Envoi de notification création de compte</fullName>
        <actions>
            <name>Notification_de_cr_ation_de_compte</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Accounts</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>envoi une notification au service ADV Fact lors de la création d&apos;un nouveau compte</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FR Mise à jour condition de paiement</fullName>
        <actions>
            <name>FR_Mise_jour_condition_de_paiement</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FR_Mise_jour_facturation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Accounts</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FR Tâche de de mise à jour de compte</fullName>
        <actions>
            <name>Notification_de_mise_jour_de_compte</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>RGCO09 Envoi un tâche à un propriétaire d&apos;un compte lorsque celui passe de prospect à client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Account - New Customer %28Commercial%29</fullName>
        <actions>
            <name>JETCLOUD_Update_Type_New_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Last_Opportunity_Close_Date__c</field>
            <operation>equals</operation>
            <value>LAST 90 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Commercial__c</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <description>Updates the Type field to reflect New Customer, when the first opportunity has been won on an account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JETCLOUD_Update_Type_to_Existing_Custom</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Last_Opportunity_Close_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Account - New Customer %28Non-Commercial%29</fullName>
        <actions>
            <name>JETCLOUD_Update_Type_New_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Last_Opportunity_Close_Date__c</field>
            <operation>equals</operation>
            <value>LAST 180 DAYS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Commercial__c</field>
            <operation>equals</operation>
            <value>Non-Commercial</value>
        </criteriaItems>
        <description>Updates the Type field to reflect New Customer, when the first opportunity has been won on an account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JETCLOUD_Update_Type_to_Existing_Custom</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Last_Opportunity_Close_Date__c</offsetFromField>
            <timeLength>180</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Account Opportunity Spend Date</fullName>
        <actions>
            <name>JETCLOUD_Update_the_Total_Estimated_Opp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a change is made to the Total Estimated Office Opportunity field, update the date field to highlight when the last review was done.</description>
        <formula>ISCHANGED( Total_Estimated_Office_Opportunity__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Billing Country</fullName>
        <actions>
            <name>JETCLOUD_Update_Account_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Country_Picklist__c</field>
            <operation>equals</operation>
            <value>Ireland,United States,Germany,United Kingdom,Italy,Spain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>notEqual</operation>
            <value>United Kingdom,United States,Italia,España,Deutschland,Eire</value>
        </criteriaItems>
        <description>Update the billing country with the value in the country picklist</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Move address</fullName>
        <actions>
            <name>copycity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copycountry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copypstcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copystate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copystreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Country_Picklist__c</field>
            <operation>equals</operation>
            <value>United Kingdom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingStreet</field>
            <operation>equals</operation>
            <value></value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Notification_de_mise_jour_de_compte</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Haute</priority>
        <protected>false</protected>
        <status>Non commencée</status>
        <subject>Notification de mise à jour de compte</subject>
    </tasks>
</Workflow>
