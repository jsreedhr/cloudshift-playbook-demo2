<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AXA_Leas_Submission</fullName>
        <ccEmails>salesforceadmin@vpsgroup.com</ccEmails>
        <description>AXA Lead Submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>ellie.smith@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.porter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>peter.williams@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>will.poulter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/AXA_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Aviva_Lead_notification</fullName>
        <description>Aviva Lead notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.williams@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_System_Templates/Aviva_lead_received</template>
    </alerts>
    <alerts>
        <fullName>DE_Lead</fullName>
        <description>DE Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>petra.karnatz@vpsitex.de</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>DE_Sales_Templates/DE_Lead</template>
    </alerts>
    <alerts>
        <fullName>Ecclesiastical_Lead_notification</fullName>
        <description>Ecclesiastical Lead notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.williams@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/Ecclesiastical_lead_received</template>
    </alerts>
    <alerts>
        <fullName>FR_Leads</fullName>
        <description>FR Leads - Notification de création d&apos;un nouveau lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>arnaud.soniassy@vpsgroup.fr</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mbrioland@vpsitex.fr</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>FR_Sales_Templates/FR_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>G4s_Lead_notification</fullName>
        <ccEmails>neil.hall@vpsgroup.com</ccEmails>
        <ccEmails>tim.atherton@vpsgroup.com</ccEmails>
        <ccEmails>steven.taylor@uk.g4s.com</ccEmails>
        <description>G4s Lead notification</description>
        <protected>false</protected>
        <senderAddress>quotes@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/G4S_lead_received</template>
    </alerts>
    <alerts>
        <fullName>GEMS_Leas_Submission</fullName>
        <ccEmails>salesforceadmin@vpsgroup.com</ccEmails>
        <description>GEMS Lead Submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>chris.goode@vps-sitesecurity.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.porter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/Select_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Guardian_Wont_Contact</fullName>
        <description>Guardian Wont Contact</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>guardian.interviews@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Guardian_Wont_Contact</template>
    </alerts>
    <alerts>
        <fullName>Guardian_will_contact</fullName>
        <description>Guardian will contact</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>guardian.interviews@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Guardian_Will_Contact</template>
    </alerts>
    <alerts>
        <fullName>Internal_Lead_Alert</fullName>
        <ccEmails>salesforceadmin@vpsgroup.com</ccEmails>
        <description>Internal Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Internal_Lead</template>
    </alerts>
    <alerts>
        <fullName>Lead_VPSGROUP_email_alert</fullName>
        <ccEmails>salesforceadmin@vpsgroup.com</ccEmails>
        <description>Lead - VPSGROUP email alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_System_Templates/VPSGroup_LEad_Alert</template>
    </alerts>
    <alerts>
        <fullName>Lead_not_touched</fullName>
        <ccEmails>Marketing@vpsgroup.com</ccEmails>
        <description>Lead not touched</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_System_Templates/Lead_No_Activity</template>
    </alerts>
    <alerts>
        <fullName>New_Lead</fullName>
        <description>New Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/Lead</template>
    </alerts>
    <alerts>
        <fullName>Opps_Lead_Notification</fullName>
        <description>Opps Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.reid@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/Opps_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>QBE_Lead_notification</fullName>
        <ccEmails>neil.hall@vpsgroup.com</ccEmails>
        <ccEmails>james.fee@vpsgroup.com</ccEmails>
        <ccEmails>jason.denton@vpsgroup.com</ccEmails>
        <description>QBE Lead notification</description>
        <protected>false</protected>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/QBE_lead_received</template>
    </alerts>
    <alerts>
        <fullName>Select_Lead_Notification</fullName>
        <description>Select Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>chris.goode@vps-sitesecurity.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Select_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Spanish_Web_Lead</fullName>
        <description>Spanish Web Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>carlos.figuero@vpsitex.es</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.porter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ES_Sales_Templates/SPANISH_Lead_Notification</template>
    </alerts>
    <alerts>
        <fullName>Standard_Life_Email_Alert</fullName>
        <description>Standard Life Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ellie.smith@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.porter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>peter.williams@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>will.poulter@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/SL_Lead_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>DE_Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>petra.karnatz@vpsitex.de</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>DE Lead - Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>France_Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>arnaud.soniassy@vpsgroup.fr</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>France - Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>French_Leads</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Leads_France</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>France - Lead record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GEMS</fullName>
        <field>LeadSource</field>
        <literalValue>Select Telemarketing Campaign</literalValue>
        <name>GEMS Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insights_demo_leads</fullName>
        <field>OwnerId</field>
        <lookupValue>ross.carroll@vpspecialists.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Insights demo leads</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Description_field</fullName>
        <description>Updates the hidden description field so that the description can be mapped to the lead.</description>
        <field>Description_Hidden__c</field>
        <formula>Description</formula>
        <name>JETCLOUD: Update Description field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Lead_Country</fullName>
        <description>Takes the value from country picklist and pushes into lead country</description>
        <field>Country</field>
        <formula>CASE(Country_Picklist__c,
&quot;United Kingdom&quot;, &quot;United Kingdom&quot;,
&quot;United States&quot;, &quot;United States&quot;,
&quot;Spain&quot;, &quot;España&quot;,
&quot;Italy&quot;, &quot;Italia&quot;,
&quot;Germany&quot;, &quot;Deutschland&quot;,
&quot;Ireland&quot;, &quot;Eire&quot;,
&quot;&quot;)</formula>
        <name>JETCLOUD: Update Lead Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>richard.reid@vpspecialists.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Opps Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Lead_R3ecord_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Operator_Referral</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opps Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opps_Lead_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UK_Leads</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opps Lead Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_DE_Lead</fullName>
        <field>RecordTypeId</field>
        <lookupValue>German_Leads</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type DE Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>chris.goode@vps-sitesecurity.co.uk</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Select - Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Spanish_Lead_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>carlos.figuero@vpsitex.es</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Spanish Lead Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Spanish_Web_Lead</fullName>
        <field>OwnerId</field>
        <lookupValue>Spanish_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Spanish Web Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Bypass_Billing_Rule</fullName>
        <field>Bypass_Billing_Rule__c</field>
        <literalValue>1</literalValue>
        <name>Tick Bypass Billing Rule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Evander_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Web_Lead_Evander</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Evander Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_Grounds_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Web_Lead_Grounds</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to Grounds Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Rating_to_Hot</fullName>
        <field>Rating</field>
        <literalValue>Hot</literalValue>
        <name>Update Rating to Hot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Web_Lead_DE_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Web_Lead_DE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Web Lead DE - Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AXA Lead Notification</fullName>
        <actions>
            <name>AXA_Leas_Submission</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>AXA - web</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert to Avivia</fullName>
        <actions>
            <name>Aviva_Lead_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>AVIVA</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert to Ecclesiastical</fullName>
        <actions>
            <name>Ecclesiastical_Lead_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Ecclesiastical</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert to G4S</fullName>
        <actions>
            <name>G4s_Lead_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>G4S</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Alert to QBE</fullName>
        <actions>
            <name>QBE_Lead_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>QBE</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Bypass Billing Rule when Converted</fullName>
        <actions>
            <name>Tick_Bypass_Billing_Rule</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Converted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DE - Lead Record Type</fullName>
        <actions>
            <name>DE_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Record_Type_DE_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>Web Lead – VPSGroup.de</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>DE Web Lead</fullName>
        <actions>
            <name>Web_Lead_DE_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>vpsgroup.de</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Evander Web Lead</fullName>
        <actions>
            <name>Update_Owner_to_Evander_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead - Evander</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FR - Web to Lead Notification</fullName>
        <actions>
            <name>FR_Leads</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead - VPSGroup.fr</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>France - Lead Owner</fullName>
        <actions>
            <name>France_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead - VPSGroup.fr</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>France - Lead Record Type</fullName>
        <actions>
            <name>French_Leads</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead - VPSGroup.fr</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Grounds Web Lead</fullName>
        <actions>
            <name>Update_Owner_to_Grounds_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web Lead - Grounds</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Guardian Will Contact</fullName>
        <actions>
            <name>Guardian_will_contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Meets_Minimum_criteria__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Guardian Leads</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Guardian Wont Contact</fullName>
        <actions>
            <name>Guardian_Wont_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Meets_Minimum_criteria__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Guardian Leads</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Insights leads</fullName>
        <actions>
            <name>New_Lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Insights_demo_leads</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>pre-launch,smartlockbox,product page,alerttower new business</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Internal Lead Notification</fullName>
        <actions>
            <name>Internal_Lead_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Internal_Lead_Submitted</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Internal_Lead_Submitted2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Lead.Internal_Lead_Form__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>AXA - web</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Copy Lead Description Field</fullName>
        <actions>
            <name>JETCLOUD_Update_Description_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Creates a copy of the lead description field so that it can be mapped to the opportunity as well as the default to map to the contact on conversion.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Country</fullName>
        <actions>
            <name>JETCLOUD_Update_Lead_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Country_Picklist__c</field>
            <operation>equals</operation>
            <value>Ireland,United States,Germany,United Kingdom,Italy,Spain</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Country</field>
            <operation>notEqual</operation>
            <value>United Kingdom,United States,Italia,España,Deutschland,Eire</value>
        </criteriaItems>
        <description>Takes the value from the country picklist and updates the Country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead Lapse Alert</fullName>
        <active>false</active>
        <description>Alert when lead has been assigned by marketing and not actioned</description>
        <formula>AND(   Not(  OR( OwnerId =&quot;00520000001dtuM&quot;, OwnerId =&quot;005w0000006ZRDl&quot;, OwnerId =&quot;005w0000005vSDX&quot;, OwnerId =&quot;00G4E000001C8DV&quot;)),      OR (Ispickval(Status,&quot;Open&quot;), ISpickval(Status, &quot;Pre Appointment&quot;)),   OR (LastActivityDate &lt;  Today() +1, ISBLANK(LastActivityDate))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Lead_not_touched</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.LastModifiedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opps Lead Record Type</fullName>
        <actions>
            <name>Opps_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Opps_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opps_Lead_R3ecord_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Operator Referral</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Select Lead Source</fullName>
        <actions>
            <name>GEMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>select</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Select Telemarketing- Owner</fullName>
        <actions>
            <name>Select_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Select_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>contains</operation>
            <value>select</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Spanish Lead Notification</fullName>
        <actions>
            <name>Spanish_Web_Lead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>web lead - VPSGroup.es</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Spanish Web Lead Owner</fullName>
        <actions>
            <name>Spanish_Lead_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>equals</operation>
            <value>Spanish Web Leads</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Spanish Web Leads</fullName>
        <actions>
            <name>Spanish_Web_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>web lead - VPSGroup.es</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Standard Life Lead Notification</fullName>
        <actions>
            <name>Standard_Life_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Standard Life</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Lead Notification</fullName>
        <actions>
            <name>Lead_VPSGROUP_email_alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Web_Lead_Submitted</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Web_Lead_Submitted3</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Web_Lead_Submitted4</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>web lead - VPSGroup.com,Web Lead - Evander,Web Lead - Grounds</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Internal_Lead_Submitted</fullName>
        <assignedTo>elle-mae.charlesworth@vpsgroup.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Internal Lead Submitted</subject>
    </tasks>
    <tasks>
        <fullName>Internal_Lead_Submitted2</fullName>
        <assignedTo>will.poulter@vpsgroup.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Internal Lead Submitted</subject>
    </tasks>
    <tasks>
        <fullName>Web_Lead_Submitted</fullName>
        <assignedTo>will.poulter@vpsgroup.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Lead Submitted</subject>
    </tasks>
    <tasks>
        <fullName>Web_Lead_Submitted2</fullName>
        <assignedTo>elle-mae.charlesworth@vpsgroup.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Lead Submitted</subject>
    </tasks>
    <tasks>
        <fullName>Web_Lead_Submitted3</fullName>
        <assignedTo>elle-mae.charlesworth@vpsgroup.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Lead Submitted</subject>
    </tasks>
    <tasks>
        <fullName>Web_Lead_Submitted4</fullName>
        <assignedTo>megan.porter@vpspecialists.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Lead Submitted</subject>
    </tasks>
</Workflow>
