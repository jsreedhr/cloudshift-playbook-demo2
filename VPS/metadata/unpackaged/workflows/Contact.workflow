<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Remove_Survey_Request</fullName>
        <field>New_Survey_Requested__c</field>
        <literalValue>0</literalValue>
        <name>Remove Survey Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Initialised_date</fullName>
        <field>Last_Survey_Initiated__c</field>
        <formula>today()</formula>
        <name>Update Initialised date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Survey%3A Update Initialise Date</fullName>
        <actions>
            <name>Update_Initialised_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.New_Survey_Requested__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remove_Survey_Request</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>168</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
