<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Activity_Type</fullName>
        <field>Type</field>
        <literalValue>Other</literalValue>
        <name>Activity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_VPS_Comments</fullName>
        <description>Take first 200 characters of task comments and push to VPS Comments</description>
        <field>VPS_Comments__c</field>
        <formula>LEFT(Description,200)</formula>
        <name>JETCLOUD: Update VPS Comments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_For_Report</fullName>
        <field>Type_for_report__c</field>
        <formula>TEXT(Type)</formula>
        <name>Update Type For Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BM Job ID Required2</fullName>
        <actions>
            <name>Activity_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>equals</operation>
            <value>Enter BM Job ID</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Push task comments into custom field</fullName>
        <actions>
            <name>JETCLOUD_Update_VPS_Comments</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>On saving a task push task comments into a shortened field to allow view in Console/Related Lists</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type for Report</fullName>
        <actions>
            <name>Update_Type_For_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Type_for_report__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
