<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Acceptance_Received</fullName>
        <ccEmails>salesforceadmin@vpsgroup.com</ccEmails>
        <description>Acceptance Received</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>quotes@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/Acceptance_Recieved</template>
    </alerts>
    <alerts>
        <fullName>CAD_upload_key</fullName>
        <description>CAD upload key</description>
        <protected>false</protected>
        <recipients>
            <recipient>VPS_Site_Security_Admin_management</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sales@vps-sitesecurity.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/CAD_Team_Upload</template>
    </alerts>
    <alerts>
        <fullName>CAD_upload_non_key</fullName>
        <description>CAD upload non key</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sales@vps-sitesecurity.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/CAD_Team_Upload</template>
    </alerts>
    <alerts>
        <fullName>Chase_Quotation</fullName>
        <description>Chase Quotation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Follow_Up_Quote</template>
    </alerts>
    <alerts>
        <fullName>Chase_Quotation2</fullName>
        <description>Chase Quotation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Follow_Up_Quote</template>
    </alerts>
    <alerts>
        <fullName>Closed_Won_Opp</fullName>
        <description>Closed Won Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>efoley@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Closed_Won_Opp</template>
    </alerts>
    <alerts>
        <fullName>DE_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (DE) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>dominik.wittenbrink@vpsgroup.de</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>DE_Sales_Templates/DE_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>DE_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (DE) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>laura.otremba@vpsitex.de</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>DE_Sales_Templates/DE_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>DE_Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>DE - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>DE_Sales_Templates/DE_VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>DE_Opportunity_Rejected</fullName>
        <description>DE - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>DE_Sales_Templates/DE_VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>DE_in_approval_queue</fullName>
        <description>DE - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>DE_Sales_Templates/DE_VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>ES_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (ES) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>jose.fuster@vpsitex.es</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ES_Sales_Templates/ES_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>ES_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (ES) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>jose.fuster@vpsitex.es</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ES_Sales_Templates/ES_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>ES_Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>ES - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ES_Sales_Templates/ES_VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>ES_Opportunity_Rejected</fullName>
        <description>ES - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ES_Sales_Templates/ES_VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>ES_in_approval_queue</fullName>
        <description>ES - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>ES_Sales_Templates/ES_VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (UK) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>carlos.rosa@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director_SiteSec</fullName>
        <description>Email - Alert to inform of Decommitted Opportunity - Sales Director Site Security</description>
        <protected>false</protected>
        <recipients>
            <recipient>richard.reid@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (UK) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (FR) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FR_Sales_Templates/FR_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (FR) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FR_Sales_Templates/FR_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>FR - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FR_Sales_Templates/FR_VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_Opportunity_Rejected</fullName>
        <description>FR - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FR_Sales_Templates/FR_VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>FR_in_approval_queue</fullName>
        <description>FR - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>FR_Sales_Templates/FR_VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>IT_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (IT) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>alessandro.verdiani@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IT_Sales_Templates/IT_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>IT_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (IT) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IT_Sales_Templates/IT_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>IT_Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>IT - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IT_Sales_Templates/IT_VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>IT_Opportunity_Rejected</fullName>
        <description>IT - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IT_Sales_Templates/IT_VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>IT_in_approval_queue</fullName>
        <description>IT - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>IT_Sales_Templates/IT_VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>NL_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</fullName>
        <description>Email (NL) - Alert to inform of Decommitted Opportunity - Sales Director</description>
        <protected>false</protected>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NL_Sales_Templates/NL_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>NL_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</fullName>
        <description>Email (NL) - Alert to inform of Decommitted Opportunity - Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales Manager</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NL_Sales_Templates/NL_Email_Decommitted_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>NL_Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>NL - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NL_Sales_Templates/NL_VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>NL_Opportunity_Rejected</fullName>
        <description>NL - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NL_Sales_Templates/NL_VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>NL_in_approval_queue</fullName>
        <description>NL - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>NL_Sales_Templates/NL_VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Notification_devis_signe</fullName>
        <description>Notification devis signé</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_devis_signe</template>
    </alerts>
    <alerts>
        <fullName>Notification_opportunit_AO_en_qualification</fullName>
        <description>Notification opportunité AO en qualification</description>
        <protected>false</protected>
        <recipients>
            <recipient>FR_Service_Client</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Notification_opportunit_AO_en_qualification1</fullName>
        <description>Notification opportunité AO en qualification</description>
        <protected>false</protected>
        <recipients>
            <recipient>FR_Service_Devis</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FR_Notification_service_devis_AO</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Approval_Process_Outcome_Approved_Email_to_Owner</fullName>
        <description>UK - Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/VPS_Standard_Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Approved</fullName>
        <description>Opportunity Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Approved_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Rejected</fullName>
        <description>UK - Opportunity Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>quotes@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/VPS_Standard_Rejected_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_created_against_your_account</fullName>
        <description>Opportunity created against your account</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>salesforceadmin@vpsgroup.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/Opportunity_Created</template>
    </alerts>
    <alerts>
        <fullName>Site_Security_Closed_Won</fullName>
        <description>Site Security Closed Won</description>
        <protected>false</protected>
        <recipients>
            <recipient>diane.webster@vps-sitesecurity.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gemma.schofield@vps-sitesecurity.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>phil.bunting@vps-sitesecurity.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sales@vps-sitesecurity.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_Sales_Templates/VPS_Site_Security_Closed_Won_Notification</template>
    </alerts>
    <alerts>
        <fullName>UK_Alert_Account_Owner_Of_Opportunity</fullName>
        <description>UK - Alert Account Owner Of Opportunity</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/UK_New_Opportunity_Created_on_your_account</template>
    </alerts>
    <alerts>
        <fullName>in_approval_queue</fullName>
        <description>UK - in approval queue</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/VPS_Submit_for_Approval_Confirmation</template>
    </alerts>
    <fieldUpdates>
        <fullName>AH_Checkbox</fullName>
        <field>Advanced_Hire__c</field>
        <literalValue>1</literalValue>
        <name>AH Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Gold_Brochure</fullName>
        <field>Brochure1__c</field>
        <formula>&quot;http://www.vpspecialists.co.uk/ddme_cms/userfiles/images/VPS_quote_HSS_small_red.jpg&quot;</formula>
        <name>Add Gold Brochure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Alert_Tower_Update</fullName>
        <field>Alert_Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Alert Tower Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <description>Built by CloudShift. Ticked by workflow when the opportunity has been approved.</description>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>Pending_Approval_With__c</formula>
        <name>Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_version_number</fullName>
        <field>Approved_number__c</field>
        <formula>Approved_number__c+1</formula>
        <name>Approved version number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Aprroved_Contracting_Page_Layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contracting_Quotation_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Aprroved Contracting Page Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Team_Approved</fullName>
        <field>StageName</field>
        <literalValue>Audit Team - Approved</literalValue>
        <name>Audit Team - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Team_Pending</fullName>
        <field>StageName</field>
        <literalValue>Audit Team - Review Pending</literalValue>
        <name>Audit Team - Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Audit_Team_Rejected</fullName>
        <field>StageName</field>
        <literalValue>Audit Team - Rejected</literalValue>
        <name>Audit Team - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAD_Drawing_Time</fullName>
        <field>CAD_Drawing_Uploaded_Date_Time__c</field>
        <formula>Now()</formula>
        <name>CAD Drawing Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Opportunity_to_Europe_Guarding</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Guarding_Europe</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Opportunity to Europe Guarding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Opportunity_to_Neutralisation</fullName>
        <field>RecordTypeId</field>
        <lookupValue>France_Neutralisation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Opportunity to Neutralisation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Changeover_Checkbox</fullName>
        <field>Changeover__c</field>
        <literalValue>1</literalValue>
        <name>Changeover Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_App_Notes</fullName>
        <field>Current_Approval_Stage_Notes__c</field>
        <name>Clear App Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approcal_Notes_Section</fullName>
        <field>Pending_Approval_With__c</field>
        <name>Clear Approval Notes Section</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Approval_Stage_Notes</fullName>
        <field>Current_Approval_Stage_Notes__c</field>
        <name>Clear Approval Stage Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Closed_date_today</fullName>
        <field>CloseDate</field>
        <formula>today()</formula>
        <name>Closed date today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Conditions_de_facturation</fullName>
        <description>Mise à jour automatique du champs</description>
        <field>Conditions_de_facturation__c</field>
        <formula>&quot;&lt;b&gt;&lt;u&gt;Modalités de facturation&lt;/u&gt;&lt;/b&gt; : &lt;BR&gt;- Pour les locations mensuelles, nos prestations sont facturées mensuellement à partir de la date effective de pose pour un mois plein (date à date), tout mois commencé étant dû dans sa totalité. &lt;BR&gt;- Pour les locations avec un engagement ferme, nos prestations sont facturées en totalité dès le 1er mois de pose pour la durée commandée. &lt;BR&gt;- Pour toute vente de prestations, nos factures sont émises le mois de pose. &lt;BR&gt;- Nos factures sont émises terme à échoir. &lt;BR&gt;- En cas de détérioration ou disparition de nos protections, celles-ci vous seront facturées selon notre tarif en vigueur au moment du constat. &lt;BR&gt;&lt;BR&gt;&lt;b&gt;&lt;u&gt;Mode de règlement&lt;/u&gt;&lt;/b&gt; : Virement à 30 jours à réception de la facture. Pour tout règlement par chèque, merci de libeller le chèque à l’ordre de VPSITEX. &lt;BR&gt;&lt;BR&gt;&lt;b&gt;Toute commande relative à un devis émis par VPS vaut acceptation de nos conditions générales de vente. &lt;/b&gt;&quot;</formula>
        <name>Conditions de facturation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contracting_Approved_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contracting_Quotation_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Contracting Approved Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Credit_Account_Para</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt; Should you wish to proceed with the quotation, VPS will open an account for your organisation with a credit facility of £4,500. Payment terms are 30 days from date of invoice.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Credit Account Para</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cross_Selling</fullName>
        <field>CrossSellingSpecifics__c</field>
        <formula>TEXT(Referring_Division__c ) + &quot; referring work to &quot; +    Owner.Division</formula>
        <name>Cross Selling</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DEVIS_Conditions_de_location_vente</fullName>
        <field>DEVIS_Conditions_de_location_vente__c</field>
        <formula>&quot;&lt;b&gt;&lt;u&gt;Horaires d&apos;intervention sur site &lt;/u&gt;&lt;/b&gt; :  8 heures à 17 heures du lundi au vendredi hors jours fériés. &lt;BR&gt;&lt;b&gt;&lt;u&gt;Délais d&apos;intervention&lt;/u&gt;&lt;/b&gt; : 48 Heures à réception de votre bon de commande. &lt;/b&gt;&quot;</formula>
        <name>DEVIS Conditions de location vente</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DEVIS_Mentions</fullName>
        <field>DEVIS_Mentions__c</field>
        <formula>&quot; A l&apos;exclusion de toute autre prestation, nos prix comprennent la pose, la dépose, la mise en service et la maintenance de nos matériels (hormis en cas de détérioration ou de disparition du matériel).&lt;BR&gt;&lt;BR&gt;&lt;b&gt;Le procédé « protection électronique » VPS comprend :&lt;/b&gt;&lt;BR&gt;- L’installation et la main d’œuvre&lt;BR&gt;- La prise en charge du service GSM&lt;BR&gt;- La prise en charge consignes alarmes&lt;BR&gt;- La télésurveillance 24h/24&lt;BR&gt;- 1 test cyclique toutes les 24 heures&lt;BR&gt;- La maintenance « hors mauvaise utilisation »&lt;BR&gt;- La levée de doute vidéo (pour notre gamme avec levée de doute vidéo).&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>DEVIS Mentions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Submitted</fullName>
        <field>Date_Time_Submitted__c</field>
        <formula>NOW()</formula>
        <name>Date Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_acceptation_approbation</fullName>
        <field>Date_Quote_Sent_To_Customer__c</field>
        <formula>NOW()</formula>
        <name>Date acceptation approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_acceptation_approbationn</fullName>
        <field>Date_Quote_Sent_To_Customer__c</field>
        <formula>NOW()</formula>
        <name>Date acceptation approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_demande_approbation</fullName>
        <field>Date_Quote_Sent_To_Customer__c</field>
        <formula>NOW()</formula>
        <name>Date demande approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_demande_approbationn</fullName>
        <field>Date_Time_Quotation_Recieved__c</field>
        <formula>NOW()</formula>
        <name>Date demande approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Direct_Debit_Para</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt; Should you wish to accept this quotation we will require you to complete the attached direct debit mandate. The completed hard copy of the direct debit mandate will be required prior to any works being carried out.&lt;BR&gt;&lt;BR&gt; The original direct debit should be posted to:VPS (UK) Limited, Broadgate House, Broadway Business Park, Chadderton, Oldham, OL9 9XA, FAO: The Quotes Team.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Direct Debit Para</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_Pas_de_concurrent</fullName>
        <field>No_Competitor__c</field>
        <literalValue>1</literalValue>
        <name>FR - Pas de concurrent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_approved_by</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>FR - approved by</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_numero_de_version</fullName>
        <field>Version__c</field>
        <formula>Version__c+1</formula>
        <name>FR - numero de version</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>G4S_Tim_Partner</fullName>
        <field>Opportunity_Partner__c</field>
        <lookupValue>tim.atherton@vpsgroup.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>G4S - Tim Partner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Approval</fullName>
        <description>Built by CloudShift. Ticks the In Approval box to show that this opportunity is currently in the approval process.</description>
        <field>In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Push_Counter_Field</fullName>
        <description>Increment the Push Counter by 1</description>
        <field>Push_Counter__c</field>
        <formula>IF( 
ISNULL( Push_Counter__c ), 
1, 
Push_Counter__c + 1 
)</formula>
        <name>Increment Push Counter Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Uncheck_Won_in_Last_12_Months</fullName>
        <description>When an opportunity closed more than 12 months ago, uncheck the won in last 12 month checkbox.</description>
        <field>Won_Last_12_Months__c</field>
        <literalValue>0</literalValue>
        <name>JETCLOUD: Uncheck Won in Last 12 Months</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Average_Rental_Term</fullName>
        <description>For open opportunities updates the average rental term from the account record.</description>
        <field>Average_Rental_Term__c</field>
        <formula>Account.Average_Rental_Term__c</formula>
        <name>JETCLOUD: Update Average Rental Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Custom_Amount</fullName>
        <description>Updates the custom amount field with the weighted total based on: 
(Recurring Line items * Weighted average number of weeks rental * Quantity) + One-off total 
NOTE. cannot override standard amount field using workflow when products are in use</description>
        <field>Total_Amount__c</field>
        <formula>IF ( 
AND( 

$RecordType.DeveloperName &lt;&gt;&quot;FR_Simple&quot;, 
$RecordType.DeveloperName &lt;&gt;&quot;FR_Complex&quot;),




IF(Guarding_Recurring_Amount__c + Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Inspections_Recurring_Amount__c +
 Property_Services_One_Off_Amount__c + Property_Services_Recurring_Amount__c +
 Securing_One_Off_Amount__c + Securing_Recurring_Amount__c&gt;0,
 Guarding_Recurring_Amount__c + Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Inspections_Recurring_Amount__c +
 Property_Services_One_Off_Amount__c + Property_Services_Recurring_Amount__c +
 Securing_One_Off_Amount__c + Securing_Recurring_Amount__c ,

IF(NOT(ISBLANK(Estimated_Contract_Term__c)), (Estimated_Contract_Term__c * Total_Recurring_Revenue_Per_Period__c) + Total_One_Off__c, 

(Minumum_Contracted_Term__c * Total_Recurring_Revenue_Per_Period__c) + Total_One_Off__c)),

IF(HasOpportunityLineItem , 
Guarding_Total_Estimated_Rollup__c + Inspections__c + Property_Services_Total_Estimated__c + Securing_Total_Estimated__c + ( Guardian_Recurring_Per_Period__c * Minumum_Contracted_Term__c ), Total_Amount__c )
)</formula>
        <name>JETCLOUD: Update Custom Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Guarding_Recurring</fullName>
        <description>Updates the Guarding total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Guarding_Recurring_Amount__c</field>
        <formula>Guarding_Recurring_Weighted_Rollup__c</formula>
        <name>JETCLOUD: Update Guarding Recurring</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Inspections_Recurring</fullName>
        <description>Updates the Inspections total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Inspections_Recurring_Amount__c</field>
        <formula>Inspections_Recurring_Weighted_Rollup__c</formula>
        <name>JETCLOUD: Update Inspections Recurring</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Opportunity_Name</fullName>
        <description>Account.Name &amp;&quot; - &quot;&amp;  LEFT(site_address__c,50)&amp;&quot; - &quot;&amp;TEXT(Number_of_Properties__c)&amp; &quot; - &quot;&amp;
IF(Number_of_Properties__c =1,&quot; Site&quot;,&quot; Sites&quot;) &amp;&quot;- &quot;&amp;TEXT(Month(CloseDate))&amp;&quot;/&quot;&amp;TEXT(Year(CloseDate))</description>
        <field>Name</field>
        <formula>Account.Name &amp; &quot; &quot; &amp; &quot;-&quot; &amp; &quot; &quot; &amp;  Site_Name__c &amp;&quot; - &quot;&amp;TEXT(Month(CloseDate))&amp;&quot;/&quot;&amp;TEXT(Year(CloseDate))</formula>
        <name>JETCLOUD: Update Opportunity Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Property_Services_One_O</fullName>
        <description>Updates the Property Services total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Property_Services_One_Off_Amount__c</field>
        <formula>Property_Services_One_Off_Weighted_RSF__c</formula>
        <name>JETCLOUD: Update Property Services One-O</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Property_Services_Recur</fullName>
        <description>Updates the Property Services total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Property_Services_Recurring_Amount__c</field>
        <formula>Property_Services_Recurring_Wtd_RSF__c</formula>
        <name>JETCLOUD: Update Property Services Recur</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Securing_One_Off</fullName>
        <description>Updates the Securing total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Securing_One_Off_Amount__c</field>
        <formula>Securing_One_Off_Weighted_Rollup__c</formula>
        <name>JETCLOUD: Update Securing One-Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Securing_Recurring</fullName>
        <description>Updates the Securing total based on weighted rollup once opportunity products have been associated to an opportunity</description>
        <field>Securing_Recurring_Amount__c</field>
        <formula>Securing_Recurring_Weighted_Rollup__c</formula>
        <name>JETCLOUD: Update Securing Recurring</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Standard_Amount</fullName>
        <description>Updates the standard amount with the ballpark figure if products have not been added</description>
        <field>Amount</field>
        <formula>IF(Guarding_Recurring_Amount__c + Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Inspections_Recurring_Amount__c +
Property_Services_One_Off_Amount__c + Property_Services_Recurring_Amount__c +
Securing_One_Off_Amount__c + Securing_Recurring_Amount__c&gt;0,
Guarding_Recurring_Amount__c + Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Inspections_Recurring_Amount__c +
Property_Services_One_Off_Amount__c + Property_Services_Recurring_Amount__c +
Securing_One_Off_Amount__c + Securing_Recurring_Amount__c ,

IF(NOT(ISBLANK(Estimated_Contract_Term__c)), (Estimated_Contract_Term__c * Total_Recurring_Revenue_Per_Period__c) +  Total_One_Off__c,

(Minumum_Contracted_Term__c * Total_Recurring_Revenue_Per_Period__c) +  Total_One_Off__c))</formula>
        <name>JETCLOUD: Update Standard Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Total_One_Off</fullName>
        <description>Sets the value in the Total One-Off field. If products, then it updates the product total. If the product category splits entered by the sales guy, then this updates the field. Otherwise, if the Total-One-off field has been entered by sales, the value rem</description>
        <field>Total_One_Off__c</field>
        <formula>IF(AND(RecordType.DeveloperName != &quot;FR_Simple&quot;, RecordType.DeveloperName != &quot;FR_Complex&quot;, HasOpportunityLineItem), Total_One_Off_Rollup__c, 

IF(AND(RecordType.DeveloperName != &quot;FR_Simple&quot;, RecordType.DeveloperName != &quot;FR_Complex&quot;, (Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Property_Services_One_Off_Amount__c + Securing_One_Off_Amount__c &gt;0)), Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Property_Services_One_Off_Amount__c + Securing_One_Off_Amount__c, 

IF(AND(OR(RecordType.DeveloperName = &quot;FR_Simple&quot;, RecordType.DeveloperName = &quot;FR_Complex&quot;), HasOpportunityLineItem), Total_One_Off_FR__c, 

IF(AND(OR(RecordType.DeveloperName = &quot;FR_Simple&quot;, RecordType.DeveloperName = &quot;FR_Complex&quot;), (Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Property_Services_One_Off_Amount__c + Securing_One_Off_Amount__c &gt;0)), Guarding_One_Off_Amount__c + Inspections_One_Off_Amount__c + Property_Services_One_Off_Amount__c + Securing_One_Off_Amount__c,

IF(AND(OR(RecordType.DeveloperName = &quot;FR_Simple&quot;, RecordType.DeveloperName = &quot;FR_Complex&quot;), Total_One_Off_FR__c &gt;0), Total_One_Off_FR__c,

IF(Total_One_Off__c &gt;0,Total_One_Off__c,0))))))</formula>
        <name>JETCLOUD: Update Total One-Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Total_Recurring</fullName>
        <description>If Products, then roll-total weighted recurring amount. If no products, this should be the Product category values summed</description>
        <field>Total_Recurring__c</field>
        <formula>IF( HasOpportunityLineItem,  Total_Recurring_Rollup__c,  
IF( Guarding_Recurring_Amount__c + Inspections_Recurring_Amount__c + Property_Services_Recurring_Amount__c + Securing_Recurring_Amount__c &gt;0, Guarding_Recurring_Amount__c + Inspections_Recurring_Amount__c + Property_Services_Recurring_Amount__c + Securing_Recurring_Amount__c, 
IF(Total_Recurring__c &gt;0,Total_Recurring__c,0)))</formula>
        <name>JETCLOUD: Update Total Recurring</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Total_Recurring_Rev_PP</fullName>
        <description>Updates the new total revenue per period field. This is a free type field for the user during the early stages of opportunity. Once products are added should be overwritten.</description>
        <field>Total_Recurring_Revenue_Per_Period__c</field>
        <formula>Recurring_Revenue_Per_Period__c</formula>
        <name>JETCLOUD: Update Total Recurring Rev PP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Won_in_Last_12_Months</fullName>
        <description>Check the checkbox to show that opportunity was won in last 12 months. Used to calculate untapped opportunity revenue on account.</description>
        <field>Won_Last_12_Months__c</field>
        <literalValue>1</literalValue>
        <name>JETCLOUD: Update Won in Last 12 Months</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_update_Description_field</fullName>
        <description>Takes the value from the hidden description field that was mapped from the lead, and pushes into the description field.</description>
        <field>Description</field>
        <formula>Description_Hidden__c</formula>
        <name>JETCLOUD: update Description field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Legal_Account_2</fullName>
        <field>Legal_Account__c</field>
        <literalValue>1</literalValue>
        <name>Legal Account 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_a_jour_approbation</fullName>
        <field>StageName</field>
        <literalValue>Envoi Client</literalValue>
        <name>Mise à jour approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_du_champ_Type</fullName>
        <field>Type</field>
        <literalValue>Client</literalValue>
        <name>Mise à jour du champ Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_en_approbation</fullName>
        <field>StageName</field>
        <literalValue>En approbation N+2</literalValue>
        <name>Mise à jour en approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_en_approbation1</fullName>
        <field>StageName</field>
        <literalValue>En approbation N+2</literalValue>
        <name>Mise à jour en approbation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_envoi_client</fullName>
        <field>StageName</field>
        <literalValue>Envoi Client</literalValue>
        <name>Mise à jour envoi client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_envoi_client1</fullName>
        <field>StageName</field>
        <literalValue>Envoi Client</literalValue>
        <name>Mise à jour envoi client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mise_jour_tape</fullName>
        <description>Mise à jour étape négociation</description>
        <field>StageName</field>
        <literalValue>Negotiation</literalValue>
        <name>Mise à jour étape</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Logo_Update</fullName>
        <field>Opportunity_Logo__c</field>
        <formula>Account.Account_Logo__c</formula>
        <name>Opportunity Logo Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Decommit_Flag</fullName>
        <description>Workflow rule to tick the decommit flag when the opportunity reaches an advanced stage.</description>
        <field>Decommit_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Opportunity - Update Decommit Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Team_Approved_By</fullName>
        <field>Quote_Team_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Quote Team Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_ask_for_approval</fullName>
        <field>Rejected__c</field>
        <literalValue>0</literalValue>
        <name>Rejected ask for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Type_to_Europe_Tolage</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Tolage</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp Type to Europe Tolage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_type_Contracting</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contracting_Property_Services</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp type Contracting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_type_Guarding</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Guarding</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp type Guarding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_type_Rental</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Rental</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp type Rental</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Security_Proposal_Text</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussions and site survey I have the pleasure of enclosing our security proposal for your project/site.&lt;BR&gt;&lt;BR&gt;Our system uses motion and heat detection in order to monitor your site. Should an intruder pass through an area of detection whilst the system is armed the camera will instantly and automatically pan, tilt and zoom to the view the intrusion whilst simultaneously alerting our BS5979 monitoring station. Our CCTV operators have the facility to directly address the intruder with a live audio challenge using the inbuilt audio communication system to deter them from your site. In the vast majority of cases when intruders are aware they’ve been caught, are being recorded, and police are on their way, they leave at speed.&lt;BR&gt;&lt;BR&gt; Should you have any questions please do not hesitate to contact me otherwise I look forward to gaining your authorisation to proceed.&quot;</formula>
        <name>Site Security Proposal Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Standard_Life_Free_Text</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation. This quotation is based on the preferential rates provided to all Standard Life customers &lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Standard Life Free Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Prop_Serv_Rec</fullName>
        <field>totalpropservrec__c</field>
        <formula>Property_Services_Recurring_Period__c * Minumum_Contracted_Term__c</formula>
        <name>Total Prop Serv Rec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tower_Opportunity2</fullName>
        <field>Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Tower Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Approved</fullName>
        <description>Built by CloudShift. This is required for circumstances where the opportunity has been approved already but the user wants to submit it again.</description>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_Approval</fullName>
        <description>Built by CloudShift. This workflow will untick the &apos;In Approval&apos; box when the opportunity has been approved.</description>
        <field>In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AH_checkbox</fullName>
        <field>Payment_Terms__c</field>
        <literalValue>Upfront Payment</literalValue>
        <name>Update AH checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Adjusted_Sales_Figure</fullName>
        <description>Update the sales figures for Business Tracker</description>
        <field>Total_Adjusted_Sales_2__c</field>
        <formula>if(Adj_weekly_rental_total__c &gt; 0, Adj_weekly_rental_total__c+Install_Fee_Bonus__c , Install_Fee_Bonus__c)</formula>
        <name>Update Adjusted Sales Figure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount_Figure_Europe</fullName>
        <field>Amount</field>
        <formula>if(RecordTypeId = &quot;01220000000YZR7&quot;,  Total_Premium_Alarm__c +  Total_Rental_Europe_Steel__c +  Total_Alarm_Rental_Europe__c, if(OR(RecordTypeId = &quot;01220000000YZ0K&quot;,RecordTypeId =&quot;01220000000YZAU&quot;),  Charge_Rate__c, if( RecordTypeId =&quot;01220000000YZ0F&quot;,  Total_Hours_per_Week__c *  Hourly_Charge_Rate_to_Client__c *  Number_of_Weeks__c, 0)))</formula>
        <name>Update Amount Figure - Europe</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Client_Services</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to your recent quote request regarding the above site we now have pleasure in submitting our pricing for the required works. This quotation is valid for 30 days from this date.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval and look forward to hearing from you.&lt;BR&gt;&lt;BR&gt;Should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&quot;</formula>
        <name>Update Client Services</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Custom_Total_Amount_for_France</fullName>
        <field>Total_Amount__c</field>
        <formula>(Minumum_Contracted_Term__c * Total_Recurring_Revenue_Per_Period__c) +  Total_One_Off_FR__c</formula>
        <name>Update Custom Total Amount for France</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Legacy_Opportunity_TRUE</fullName>
        <field>Legacy_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Update Legacy Opportunity TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Customer_New_Business</fullName>
        <field>Type</field>
        <literalValue>New Business</literalValue>
        <name>Update New Customer - New Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_to_Rental</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Rental_Europe</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Opportunity to Rental</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Proposal_Free_Text</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within the Social Housing and Public Sectors.  We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Update Proposal Free Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Figure_USA</fullName>
        <field>Amount</field>
        <formula>CASE($RecordType.Name,&quot;Guarding&quot;,Total_Guarding_Value_VPSInc__c,
	&quot;Contracting / Propety Services&quot;, Charge_Rate__c , 
&quot;Rental&quot;, if(Install_Removal_Fee__c&gt;0,Minimum_Hire_Period_months__c *  Monthly_Rental__c +  Install_Removal_Fee__c,Minimum_Hire_Period_months__c *  Monthly_Rental__c),0)</formula>
        <name>Update Total Sales Figure - USA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total_Sales_Figure_UK</fullName>
        <field>Amount</field>
        <formula>CASE($RecordType.Name,&quot;Guarding&quot;,Weekly_Charge_Rate_to_Client__c,
&quot;Contracting / Property Services&quot;,Charge_Rate__c,&quot;Rental&quot;, Weekly_Rental__c ,0)</formula>
        <name>Update Total Sales Figure - UK</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Type_Existing_Business</fullName>
        <field>Type</field>
        <literalValue>Existing Business</literalValue>
        <name>Update Type Existing Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Vpsitex_Free_Text</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPSitex’s Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPSitex has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Update Vpsitex Free Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Conducting_SRA</fullName>
        <field>StageName</field>
        <literalValue>Conducting SRA</literalValue>
        <name>Update to Conducting SRA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Upfront_Payment_Paragraph</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt; Notwithstanding the payment provisions in our standard Terms &amp; Conditions, should you wish to proceed with the attached quotation, we will require payment in full of the amount stipulated in the document in advance of our commencing to provide any service. Upon receipt of cleared funds we will commence the works as detailed.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Upfront Payment Paragraph</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>commercial_free_text</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>commercial free text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copy_company_reg</fullName>
        <field>Portfolio_Client_Reg_Number__c</field>
        <formula>Account.Company_Registration_Number__c</formula>
        <name>copy company reg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copycity</fullName>
        <field>Invoice_City__c</field>
        <formula>Account.BillingCity</formula>
        <name>copycity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copyname</fullName>
        <field>Portfolio_Client_Name__c</field>
        <formula>Account.Name</formula>
        <name>copyname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copynumber</fullName>
        <field>Portfolio_Client_Reg_Number__c</field>
        <formula>Account.Company_Registration_Number__c</formula>
        <name>copynumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copypostcode</fullName>
        <field>Invoice_Postcode__c</field>
        <formula>Account.BillingPostalCode</formula>
        <name>copypostcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copystate</fullName>
        <field>Invoice_County__c</field>
        <formula>Account.BillingState</formula>
        <name>copystate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>copystreet</fullName>
        <field>Invoice_Street__c</field>
        <formula>Account.BillingStreet</formula>
        <name>copystreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>untick_Rejected</fullName>
        <field>Rejected__c</field>
        <literalValue>0</literalValue>
        <name>untick Rejected for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_close_date</fullName>
        <field>CloseDate</field>
        <formula>today() + 7</formula>
        <name>update close date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>qualtrics__Qualtrics_Example_Outbound_Message</fullName>
        <apiVersion>34.0</apiVersion>
        <description>An example of how to setup an outbound message. 
The endpoint url is not valid and needs to be updated to a real out endpoint url.</description>
        <endpointUrl>http://survey.qualtrics.com/WRQualtricsServer/sfApi.php?r=outboundMessage&amp;u=UR_123456789&amp;s=SV_123456789&amp;t=TR_123456789</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>megan.porter@vpsgroup.com</integrationUser>
        <name>Qualtrics Example Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>AH Checkbox</fullName>
        <actions>
            <name>AH_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Payment_Terms__c</field>
            <operation>equals</operation>
            <value>Upfront Payment</value>
        </criteriaItems>
        <description>AH checkbox ticked when the payment terms = upfront</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>AXA Free Text</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Pricebook2.X18_digit_Price_Book_ID__c</field>
            <operation>equals</operation>
            <value>01sw0000001bUBo</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Add Company Reg</fullName>
        <actions>
            <name>copy_company_reg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Company_Registration_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Portfolio_Client_Reg_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Add Invoice details</fullName>
        <actions>
            <name>copycity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copyname</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copynumber</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copypostcode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copystate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>copystreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Country_Picklist__c</field>
            <operation>equals</operation>
            <value>United Kingdom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Invoice_Street__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Alert Tower</fullName>
        <actions>
            <name>Alert_Tower_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Primary_Service_Interest__c</field>
            <operation>includes</operation>
            <value>Alert Tower</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Approval</fullName>
        <actions>
            <name>Approved_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto Notification Of New Opp To Account Owner</fullName>
        <actions>
            <name>Opportunity_created_against_your_account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Rule will auto notify account owner/rep whenever a new opportunity is created.</description>
        <formula>OwnerId  &lt;&gt;  Account.OwnerId</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BM Job ID Required</fullName>
        <active>true</active>
        <description>To create task to remind user to complete BM Job ID after install has taken place</description>
        <formula>AND(
NOT(RecordType.DeveloperName = &quot;FR_Simple&quot;),
NOT(RecordType.DeveloperName = &quot;FR_Complex&quot;),
OR(
$User.ProfileId  &lt;&gt; &apos;00ew00000015ujy&apos;,
$User.ProfileId  &lt;&gt; &apos;00ew0000001tRoI&apos;,
$User.ProfileId  &lt;&gt; &apos;00ew00000012mxC&apos;,
$User.ProfileId  &lt;&gt; &apos;00ew0000001PlAD&apos;,
 RecordTypeId  = &apos;012w0000000V1yB&apos;,
 RecordTypeId  = &apos;012w0000000V1yG&apos;, 
RecordTypeId  = &apos;012w00000002Q6o&apos;,

ISPICKVAL( StageName ,&quot;Closed Won&quot;),
ISBLANK(  BM_Job_ID__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Enter_BM_Job_ID</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Start_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CAD Update</fullName>
        <actions>
            <name>CAD_Drawing_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Final_CAD_Drawing__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Opportunity to France Neutralisation</fullName>
        <actions>
            <name>Change_Opportunity_to_Neutralisation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Neutralisation</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Changeover Checkbox</fullName>
        <actions>
            <name>Changeover_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Opportunity.LeadSource</field>
            <operation>equals</operation>
            <value>Changeover</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client Services Free Text</fullName>
        <actions>
            <name>Update_Client_Services</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Client Services,team leader,UK CS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Proposal_Free_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won Opp</fullName>
        <actions>
            <name>Closed_Won_Opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Owners_Division__c</field>
            <operation>contains</operation>
            <value>ireland</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed date today</fullName>
        <actions>
            <name>Closed_date_today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR - Complex,FR - Simple</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CloudShift%3A Update Product Category Totals</fullName>
        <actions>
            <name>JETCLOUD_Update_Guarding_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Inspections_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Property_Services_One_O</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Property_Services_Recur</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Securing_One_Off</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Securing_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Total_Recurring_Rev_PP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.HasOpportunityLineItem</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex,IT - Simple,IT - Complex,DE - Complex,DE - Simple,EGL - Simple,EGL - Complex,ES - Simple,ES - Complex,NL - Complex,NL - Simple,FR - Complex,FR - Simple,Grounds Services</value>
        </criteriaItems>
        <description>Updates Guarding, Securing, Inspections, Property Services Totals from the Weighted total once opportunity products have been added to the opportunity.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Commercial Free Text</fullName>
        <actions>
            <name>commercial_free_text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>notContain</operation>
            <value>social,VPSitex IE,site security,VPSitex France</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Proposal_Free_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR - Complex,FR - Simple</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>notEqual</operation>
            <value>VPS UK - Standard Life July 2016</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cross Selling</fullName>
        <actions>
            <name>Cross_Selling</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>contains</operation>
            <value>simple,complex</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DEVIS Conditions de location vente</fullName>
        <actions>
            <name>DEVIS_Conditions_de_location_vente</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordType_Name__c</field>
            <operation>contains</operation>
            <value>FR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DEVIS Mentions</fullName>
        <actions>
            <name>DEVIS_Mentions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordType_Name__c</field>
            <operation>contains</operation>
            <value>FR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR - %09DEVIS Conditions de facturation</fullName>
        <actions>
            <name>Conditions_de_facturation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>contains</operation>
            <value>FR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR - Competitor</fullName>
        <actions>
            <name>FR_Pas_de_concurrent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Complex,FR - Simple,FR - Simple - Test v2</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Mise à jour statut client</fullName>
        <actions>
            <name>Mise_jour_du_champ_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Prospect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR Cotation et AO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Devis Signé</value>
        </criteriaItems>
        <description>Met le type de compte en client lors de la signature d&apos;un devis</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity Set Legacy</fullName>
        <actions>
            <name>Update_Legacy_Opportunity_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Complex,FR - Simple</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>G4S - Tim Partner</fullName>
        <actions>
            <name>G4S_Tim_Partner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.OwnerId</field>
            <operation>equals</operation>
            <value>naomi slater</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>contains</operation>
            <value>G4S</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Amount</fullName>
        <actions>
            <name>JETCLOUD_Update_Custom_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Total_One_Off</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Total_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the custom amount field with the weighted total</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Average Rental Term</fullName>
        <actions>
            <name>JETCLOUD_Update_Average_Rental_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Average_Rental_Term__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Updates the Average rental term based on the account average rental term</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Opportunity Description</fullName>
        <actions>
            <name>JETCLOUD_update_Description_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Description</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Description_Hidden__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the opportunity is created from a lead conversion, take the description from the lead and enter it into the opportunity description.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Opportunity Name</fullName>
        <actions>
            <name>JETCLOUD_Update_Opportunity_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>On creating an opportunity update the opportunity name to show the Account Name-No of Sites - Site Address - CloseMonth/CloseYear</description>
        <formula>AND( RecordType.Name != &apos;FR Cotation et AO&apos;, RecordType.Name != &apos;DE - Simple&apos;, RecordType.Name != &apos;DE - Complex&apos;, OR(NOT(CONTAINS(Name, Account.Name)), NOT(CONTAINS(Name,  Site_Name__c )), NOT(CONTAINS(Name,LEFT(TEXT(CloseDate),3)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Product Category Totals</fullName>
        <actions>
            <name>JETCLOUD_Update_Guarding_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Inspections_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Property_Services_One_O</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Property_Services_Recur</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Securing_One_Off</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Securing_Recurring</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Total_Recurring_Rev_PP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.HasOpportunityLineItem</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>,Schedule Of Rates,Contracting Quotation,Contracting Quotation Approved,Contracting / Property Services,FR Cotation et AO,VPS Site Security,Lotus Landscapes,UK - Simple,UK - Complex,IT - Simple,IT - Complex,DE - Simple,DE - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>ES - Simple,ES - Complex,EGL - Complex,EGL - Simple</value>
        </criteriaItems>
        <description>Updates Guarding, Securing, Inspections, Property Services Totals from the Weighted total once opportunity products have been added to the opportunity.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Standard Amount</fullName>
        <actions>
            <name>JETCLOUD_Update_Standard_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.HasOpportunityLineItem</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Lotus Landscapes</value>
        </criteriaItems>
        <description>Updates the standard amount field with the weighted total</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Won in Last 12 Months</fullName>
        <actions>
            <name>JETCLOUD_Update_Won_in_Last_12_Months</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates a field to flag if the opportunity was won in the last 12 months.
Time based workflow</description>
        <formula>AND(IsWon=TRUE, CloseDate&gt;TODAY()-365)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JETCLOUD_Uncheck_Won_in_Last_12_Months</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Won in Last 12 Months - WA</fullName>
        <active>true</active>
        <description>Updates a field to flag if the opportunity was won in the last 12 months.
Time based workflow
Workarund for previously closed opps within last 12 months.</description>
        <formula>AND(Won_Last_12_Months__c, CloseDate&gt;TODAY()-365)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>JETCLOUD_Uncheck_Won_in_Last_12_Months</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Legal Account</fullName>
        <actions>
            <name>Legal_Account_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Legal_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunitiy Customer Logo</fullName>
        <actions>
            <name>Opportunity_Logo_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Logo__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28DE%29 - Negotiation - Sales Process Reminder</fullName>
        <actions>
            <name>Zur_Erinnerung_Aufbau_von_Beziehungen_w_hrend_der_Verhandlung</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DE - Complex,DE - Simple</value>
        </criteriaItems>
        <description>Workflow rule to create a task when the opportunity is saved in the negotiation stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28DE%29 - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>DE_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DE - Simple,DE - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Steffen Bluhm) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28DE%29 - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>DE_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>DE - Simple,DE - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28ES%29 - Negotiation - Sales Process Reminder</fullName>
        <actions>
            <name>Recordatorio_Relacion_de_construccion_durante_la_negociacion</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ES - Complex,ES - Simple</value>
        </criteriaItems>
        <description>Workflow rule to create a task when the opportunity is saved in the negotiation stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28ES%29 - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>ES_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ES - Simple,ES - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Carlos Rosa) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28ES%29 - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>ES_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ES - Simple,ES - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28FR%29 - Negotiation - Sales Process Reminder</fullName>
        <actions>
            <name>FR_Reminder_Relationship_Building_During_Negotiation</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Simple,FR - Complex,FR - Simple - Test v2</value>
        </criteriaItems>
        <description>Workflow rule to create a task when the opportunity is saved in the negotiation stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Opportunity %28FR%29 - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>FR_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Simple,FR - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Jean-Philippe Gindre) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28FR%29 - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>FR_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Simple,FR - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28IT%29 - Negotiation - Sales Process Reminder</fullName>
        <actions>
            <name>Promemoria_costruzione_di_relazioni_durante_la_negoziazione</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IT - Simple,IT - Complex</value>
        </criteriaItems>
        <description>Workflow rule to create a task when the opportunity is saved in the negotiation stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28IT%29 - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>IT_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IT - Simple,IT - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Alessandro Verdiani) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28IT%29 - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>IT_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IT - Simple,IT - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28NL%29 - Negotiation - Sales Process Reminder</fullName>
        <actions>
            <name>NL_Reminder_Relationship_Building_During_Negotiation</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Negotiation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NL - Simple,NL - Complex</value>
        </criteriaItems>
        <description>Workflow rule to create a task when the opportunity is saved in the negotiation stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28NL%29 - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>NL_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NL - Simple,NL - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Jeroen Van Der Poel) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity %28NL%29 - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>NL_Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>NL - Simple,NL - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Simple%2FComplex - Decommit Email Alert - Sales Director</fullName>
        <actions>
            <name>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Carlos Rosa) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Simple%2FComplex - Decommit Email Alert - Sales Director Site Security</fullName>
        <actions>
            <name>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Director_SiteSec</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>Site Security</value>
        </criteriaItems>
        <description>Automated email to be sent to the sales director (Carlos Rosa) when a large opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Simple%2FComplex - Decommit Email Alert - Sales Manager</fullName>
        <actions>
            <name>Email_Alert_to_inform_of_Decommitted_Opportunity_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Decommit_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Automated email to be sent to the sales manager of the opportunity owner when the opportunity has been decomitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - Simple%2FComplex - Set Decommit Flag</fullName>
        <actions>
            <name>Opportunity_Update_Decommit_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex,IT - Simple,IT - Complex,DE - Complex,DE - Simple,ES - Simple,ES - Complex,NL - Simple,NL - Complex,FR - Simple,FR - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Commit,Closed Won</value>
        </criteriaItems>
        <description>Workflow used to tick a box</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project End Task</fullName>
        <actions>
            <name>Project_end_approaching</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Tower_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp Type to Europe Gaurding</fullName>
        <actions>
            <name>Change_Opportunity_to_Europe_Guarding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Guarding - Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR Cotation et AO</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp type Contracting</fullName>
        <actions>
            <name>Set_Opp_type_Contracting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Contracting / Property Services</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>VPS UK,Systems Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR Cotation et AO</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp type Guarding</fullName>
        <actions>
            <name>Set_Opp_type_Guarding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Guarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>VPS UK,Systems Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR - Accounts</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp type Rental</fullName>
        <actions>
            <name>Set_Opp_type_Rental</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>Rental</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notContain</operation>
            <value>VPS UK,Systems Administrator</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Site Security Closed Won</fullName>
        <actions>
            <name>Site_Security_Closed_Won</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>VPS SIte Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Site Security Free Text</fullName>
        <actions>
            <name>Site_Security_Proposal_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Site Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>SiteSecurity Sales Director</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Proposal_Free_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Social Free Text</fullName>
        <actions>
            <name>Update_Proposal_Free_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>social</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Proposal_Free_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Standard Life Text</fullName>
        <actions>
            <name>Standard_Life_Free_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>contains</operation>
            <value>Standard Life</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Total Rec Rev</fullName>
        <actions>
            <name>Total_Prop_Serv_Rec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tower Opportunity</fullName>
        <actions>
            <name>Tower_Opportunity2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Primary_Service_Interest__c</field>
            <operation>includes</operation>
            <value>Towers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Site Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Amount Figure - Europe</fullName>
        <actions>
            <name>Update_Amount_Figure_Europe</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>Italian</value>
        </criteriaItems>
        <description>Update Amount: 
Formula:
Neutralisation: Charge Rate, Tolage: Charge Rate. Location: Total Alarm + Total Premium + Total Steel 
Gardiennage: No. Of Hours * Rate to Charge per Hour * Total Hours Per Week * Estimated Duration (No of. Weeks)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Amount Figure - USA</fullName>
        <actions>
            <name>Update_Sales_Figure_USA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>contains</operation>
            <value>VPS Inc</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Custom Total Amount for France</fullName>
        <actions>
            <name>Update_Custom_Total_Amount_for_France</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Simple,FR - Complex</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Type Existing Business</fullName>
        <actions>
            <name>Update_Type_Existing_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Customer_Type__c</field>
            <operation>equals</operation>
            <value>Existing Customer</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Type New Business</fullName>
        <actions>
            <name>Update_New_Customer_New_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Customer_Type__c</field>
            <operation>equals</operation>
            <value>New Customer</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Upfront Payment Para</fullName>
        <actions>
            <name>Upfront_Payment_Paragraph</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Payment_Terms__c</field>
            <operation>equals</operation>
            <value>Upfront Payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordType_Name__c</field>
            <operation>equals</operation>
            <value>UK - Simple,UK - Complex</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VPSSS - CAD Upload key</fullName>
        <actions>
            <name>CAD_upload_key</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Final_CAD_Drawing__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Key_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VPSSS - CAD Upload non key</fullName>
        <actions>
            <name>CAD_upload_non_key</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Final_CAD_Drawing__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Key_Account__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VPSitex IE Free Text</fullName>
        <actions>
            <name>Update_Vpsitex_Free_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>contains</operation>
            <value>VPSitex IE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Proposal_Free_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>VPS Site Security</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>qualtrics__Qualtrics Example Survey Rule</fullName>
        <actions>
            <name>qualtrics__Qualtrics_Example_Outbound_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>An example of how to setup a rule to trigger a survey using an outbound message. 
In this example when an opportunity is closed we want to email the opportunity and see how their interaction with the sales representative went.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Enter_BM_Job_ID</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please complete the BM job ID field, as your opportunity should now have been installed</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Start_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Enter BM Job ID</subject>
    </tasks>
    <tasks>
        <fullName>FR_Reminder_Relationship_Building_During_Negotiation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>4</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Relance suite à l’envoi du devis</subject>
    </tasks>
    <tasks>
        <fullName>NL_Reminder_Relationship_Building_During_Negotiation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reminder: Relationship Building During Negotiation (NL)</subject>
    </tasks>
    <tasks>
        <fullName>Notification_renouvellement_devis</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>-7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.date_de_renouvellement__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Notification renouvellement devis</subject>
    </tasks>
    <tasks>
        <fullName>Opportunity_Stuck_at_Negotiation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>30</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Date_Quote_Sent_To_Customer__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Opportunity Stuck at Negotiation</subject>
    </tasks>
    <tasks>
        <fullName>Project_end_approaching</fullName>
        <assignedToType>owner</assignedToType>
        <description>Visit the site to re-confirm the approx decommission date, obtain feedback from clients regarding our performance and their satisfaction/issues, and discuss their next project so we can quote for solutions which will meet their new requirements</description>
        <dueDateOffset>-28</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Project_End_Date__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Project end approaching</subject>
    </tasks>
    <tasks>
        <fullName>Promemoria_costruzione_di_relazioni_durante_la_negoziazione</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Promemoria: Costruzione di relazioni durante la negoziazione</subject>
    </tasks>
    <tasks>
        <fullName>Recordatorio_Relacion_de_construccion_durante_la_negociacion</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Recordatorio: Relación de construcción durante la negociación</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_Relationship_Building_During_Negotiation</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reminder: Relationship Building During Negotiation</subject>
    </tasks>
    <tasks>
        <fullName>Zur_Erinnerung_Aufbau_von_Beziehungen_w_hrend_der_Verhandlung</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Zur Erinnerung: Aufbau von Beziehungen während der Verhandlung</subject>
    </tasks>
</Workflow>
