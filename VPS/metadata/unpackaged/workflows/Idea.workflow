<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_to_inform_System_Administrator_of_New_Idea</fullName>
        <description>Email - Alert to inform System Administrator of New Idea</description>
        <protected>false</protected>
        <recipients>
            <recipient>david@cloudshiftgroup.com.live</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>neil.hall@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Ideas_Templates/Ideas_Send_Email_Alert_to_System_Admin_upon_Idea_Creation</template>
    </alerts>
    <rules>
        <fullName>Ideas - Send Email Alert to System Admin upon Idea Creation</fullName>
        <actions>
            <name>Email_Alert_to_inform_System_Administrator_of_New_Idea</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to send an email to Neil Hall to inform him of the creation of a new idea.</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
