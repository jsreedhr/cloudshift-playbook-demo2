<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Plan_Not_touched_for_3_months</fullName>
        <ccEmails>ablackwell@cloudshiftgroup.com</ccEmails>
        <description>Account Plan - Not touched for 3 months</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Account_Plan_Not_touched_for_3_months</template>
    </alerts>
    <rules>
        <fullName>Task for Next Price Increase</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account_Plan__c.Next_Price_Increase_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>To alert the user of any actions that need to be taken before the next price increase</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Next_Price_Increase_in_90_days</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account_Plan__c.Next_Price_Increase_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Next_Price_Increase_in_90_days</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account_Plan__c.Next_Price_Increase_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Next Price Increase in 90 days</subject>
    </tasks>
</Workflow>
