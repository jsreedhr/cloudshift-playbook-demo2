<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_lead_has_been_submitted_by_G4S</fullName>
        <ccEmails>neil.hall@vpspecialists.com</ccEmails>
        <ccEmails>Damien.frost@vpspecialists.com</ccEmails>
        <description>A lead has been submitted by G4S</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/G4S_lead_received</template>
    </alerts>
    <alerts>
        <fullName>A_lead_has_been_submitted_by_QBE</fullName>
        <description>A lead has been submitted by QBE</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.williams@vpsgroup.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ross.carroll@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>UK_Sales_Templates/QBE_lead_received</template>
    </alerts>
    <alerts>
        <fullName>JETCLOUD_Notify_Case_Creator_that_the_Case_has_been_closed</fullName>
        <description>JETCLOUD: Notify Case Creator that the Case has been closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_Child_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>SF_Admin</fullName>
        <description>SF Admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/SF_Admin</template>
    </alerts>
    <fieldUpdates>
        <fullName>AXA_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>AXA_UK_Service</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>AXA Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Co_opperative_Bank_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Co_op</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Co-opperative Bank Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completions_CS_Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Completions - CS</literalValue>
        <name>Completions - CS - Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Emcor_Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CS1</literalValue>
        <name>Emcor Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Upate_Sub_Reason_Client_Name</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Client name incorrect</literalValue>
        <name>FQ - Upate Sub Reason Client Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Addresse_Details</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Addressee details</literalValue>
        <name>FQ - Update Reason Addresse Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Commercial_Discount</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Commercial Discount</literalValue>
        <name>FQ - Update Reason Commercial Discount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Inspection_Reports</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Inspection Reports</literalValue>
        <name>FQ - Update Reason Inspection Reports</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Invoice_Split</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Invoice split / incorrect branch</literalValue>
        <name>FQ - Update Reason Invoice Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Job_Cancelled</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Job cancelled</literalValue>
        <name>FQ Update Reason Job Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Not_Offered_Hired</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Property Not Off Hired On Instruction</literalValue>
        <name>FQ - Update Reason Not Offered Hired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Other</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Other</literalValue>
        <name>FQ - Update Reason Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Ownership_Change</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Ownership change</literalValue>
        <name>FQ - Update Reason Ownership Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_PO_Number</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Purchase Order number</literalValue>
        <name>FQ - Update Reason PO Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Price</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Price</literalValue>
        <name>FQ - Update Reason Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Reason_Work_Carried_Out</fullName>
        <field>Finance_Query_Resaon__c</field>
        <literalValue>Work carried out</literalValue>
        <name>FQ - Update Reason Work Carried Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Address_Incorre</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Address / other details incorrect but correct client</literalValue>
        <name>FQ - Update Sub Reason - Address Incorre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Alarm_Response</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Alarm response credit</literalValue>
        <name>FQ - Update Sub Reason Alarm Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Billing_Period</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Billing Period Incorrect</literalValue>
        <name>FQ - Update Sub Reason Billing Period</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Charged_Twice</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Charged twice</literalValue>
        <name>FQ - Update Sub Reason Charged Twice</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Charges_For_MisKi</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Charges for missing kit in dispute</literalValue>
        <name>FQ - Update Sub Reason Charges For MisKi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Hours_Stock</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Hours/quantity of stock disputed</literalValue>
        <name>FQ - Update Sub Reason Hours/Stock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Inspection_Report</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Customer can&apos;t view inspection report</literalValue>
        <name>FQ - Update Sub Reason Inspection Report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Invoice_Split</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Invoice requires splitting out</literalValue>
        <name>FQ - Update Sub Reason Invoice Split</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Job_Cancelled</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Job cancelled</literalValue>
        <name>FQ Update Sub Reason Job Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Job_Quality_Timin</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Job Quality / Timing</literalValue>
        <name>FQ - Update Sub Reason Job Quality Timin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Missing_Kit</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Charge for missing kit</literalValue>
        <name>FQ - Update Sub Reason Missing Kit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_No_Completion</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>No notification of job completion received</literalValue>
        <name>FQ - Update Sub Reason - No Completion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Other</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Other</literalValue>
        <name>FQ - Update Sub Reason Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Ownership_Change</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Ownership change</literalValue>
        <name>FQ - Update Sub Reason Ownership Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_PO_Missing</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>PO missing</literalValue>
        <name>FQ - Update Sub Reason PO Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_PO_Used_Previousl</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Purchase Order previously used</literalValue>
        <name>FQ - Update Sub Reason PO Used Previousl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Pricing_Incomplet</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Pricing incomplete</literalValue>
        <name>FQ - Update Sub Reason Pricing Incomplet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Property_Not_Off</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Property Not Off Hired On Instruction</literalValue>
        <name>FQ - Update Sub Reason Property Not Off</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Sales_Concession</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Sales Concession</literalValue>
        <name>FQ - Update Sub Reason Sales Concession</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Wrong_Item</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Wrong item being charged/description incorrect</literalValue>
        <name>FQ - Update Sub Reason Wrong Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_Sub_Reason_Wrong_PO</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Wrong Purchase Order</literalValue>
        <name>FQ - Update Sub Reason Wrong PO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_sub_reason_price_list</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Price List Incorrect</literalValue>
        <name>FQ - Update sub reason price list</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_Update_sub_reaspn_FOC_promise</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>FOC promise</literalValue>
        <name>FQ - Update sub reaspn FOC promise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FQ_update_sub_reason_doesnt_match_inst</fullName>
        <field>Finance_Query_Sub_Reason__c</field>
        <literalValue>Doesn&apos;t match contract/instructions</literalValue>
        <name>FQ - update sub reason doesnt match inst</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Case_Priority_NotYetFlag</fullName>
        <description>Update the Case priority to not yet flagged.</description>
        <field>Priority</field>
        <literalValue>Not Yet Flagged</literalValue>
        <name>JETCLOUD: Update Case Priority - Not yet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_AXA</fullName>
        <field>Managing_Team__c</field>
        <literalValue>AXA</literalValue>
        <name>JETCLOUD: Update Managing Team - AXA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Comple</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Completions - CS</literalValue>
        <name>JETCLOUD: Update Managing Team - Complet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Contrac</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Contracting</literalValue>
        <name>JETCLOUD: Update Managing Team - Contrac</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Estates</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Estates</literalValue>
        <name>JETCLOUD: Update Managing Team - Estates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Fusion21</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Fusion 21</literalValue>
        <name>JETCLOUD: Update Managing Team -Fusion21</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_GuardChec</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Guard Checks</literalValue>
        <name>JETCLOUD: Update Managing Team-GuardChec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Guardin</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Guarding</literalValue>
        <name>JETCLOUD: Update Managing Team - Guardin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Insolve</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Insolvency</literalValue>
        <name>JETCLOUD: Update Managing Team - Insolve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_LSH_Sur</fullName>
        <field>Managing_Team__c</field>
        <literalValue>LSH &amp; Survey</literalValue>
        <name>JETCLOUD: Update Managing Team - LSH&amp;Sur</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Liv2020</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Liverpool 2020</literalValue>
        <name>JETCLOUD: Update Managing Team -Liv2020</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_PUBS</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN2</literalValue>
        <name>JETCLOUD: Update Managing Team - Pubs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Propert</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Property Management</literalValue>
        <name>JETCLOUD: Update Managing Team - Propert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Repo</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Repo</literalValue>
        <name>JETCLOUD: Update Managing Team - Repo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_STT</fullName>
        <field>Managing_Team__c</field>
        <literalValue>STT</literalValue>
        <name>JETCLOUD: Update Managing Team - STT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Service</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Services</literalValue>
        <name>JETCLOUD: Update Managing Team - Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_ServiceIm</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Service Improvement</literalValue>
        <name>JETCLOUD: Update Managing Team-ServiceIm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Smithsgo</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Smithsgore</literalValue>
        <name>JETCLOUD: Update Managing Team -Smithsgo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Managing_Team_Squat</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Squatters</literalValue>
        <name>JETCLOUD: Update Managing Team - Squat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Priority_Critical</fullName>
        <field>Priority</field>
        <literalValue>Critical</literalValue>
        <name>JETCLOUD: Update Priority - Critical</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Priority_Inspection</fullName>
        <field>Priority</field>
        <literalValue>Inspection</literalValue>
        <name>JETCLOUD: Update Priority - Inspection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Low_Priority</fullName>
        <field>Priority</field>
        <literalValue>Low</literalValue>
        <name>Low Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>G4S</literalValue>
        <name>Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managing_Team_AXA</fullName>
        <field>Managing_Team__c</field>
        <literalValue>AXA</literalValue>
        <name>Managing Team AXA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managing_Team_Invoicing</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Invoicing</literalValue>
        <name>Managing Team - Invoicing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Managing_team_Contract_amendments</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Contract-Amendments</literalValue>
        <name>Managing team - Contract-amendments</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_Action_Required</fullName>
        <field>Reason</field>
        <literalValue>No Action Required</literalValue>
        <name>No Action Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RBS_Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>RBS</literalValue>
        <name>RBS Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RM_Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Reactive Maintenance</literalValue>
        <name>RM Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Royal_British_Legion</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN1</literalValue>
        <name>Royal British Legion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UK_Tower_Manag8ing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>UK Towers</literalValue>
        <name>UK Tower Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UK_Tower_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>UK_Towers</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>UK Tower Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MT_BarkingSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>BarkingSC</literalValue>
        <name>Update MT BarkingSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MT_CorbySC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CorbySC</literalValue>
        <name>Update MT CorbySC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MT_NortheastSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>NorthEastSC</literalValue>
        <name>Update MT NortheastSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Quotes</literalValue>
        <name>Update Managing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_BristolSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>BristolSC</literalValue>
        <name>Update Managing Team BristolSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Centralenglandco_op</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CentralenglandCo-op</literalValue>
        <name>Update Managing Team Centralenglandco-op</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Enterprise</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Enterprise</literalValue>
        <name>Update Managing Team Enterprise</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_G4S</fullName>
        <field>Managing_Team__c</field>
        <literalValue>G4S</literalValue>
        <name>Update Managing Team G4S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Guardian_Admin</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Guardian Admin</literalValue>
        <name>Update Managing Team - Guardian Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Guarding_Queries</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Guarding Queries</literalValue>
        <name>Update Managing Team Guarding Queries</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_HullSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>HullSC</literalValue>
        <name>Update Managing Team HullSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Invoicing</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Invoicing</literalValue>
        <name>Update Managing Team - Invoicing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_Keys</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Keys</literalValue>
        <name>Update Managing Team - Keys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_KilmarnockSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>KilmarnockSC</literalValue>
        <name>Update Managing Team KilmarnockSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_LiverpoolSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>LiverpoolSC</literalValue>
        <name>Update Managing Team LiverpoolSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_LondonSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>LondonSC</literalValue>
        <name>Update Managing Team LondonSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_ManchesterSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>ManchesterSC</literalValue>
        <name>Update Managing Team ManchesterSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_PortsmouthSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>PortsmouthSC</literalValue>
        <name>Update Managing Team PortsmouthSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_RSA</fullName>
        <field>Managing_Team__c</field>
        <literalValue>RSA</literalValue>
        <name>Update Managing Team RSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_VSG</fullName>
        <field>Managing_Team__c</field>
        <literalValue>VSG</literalValue>
        <name>Update Managing Team - VSG</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_WalesSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>WalesSC</literalValue>
        <name>Update Managing Team WalesSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_WatfordSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>WatfordSC</literalValue>
        <name>Update Managing Team WatfordSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_YHG</fullName>
        <field>Managing_Team__c</field>
        <literalValue>YHG</literalValue>
        <name>Update Managing Team YHG</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_co_op</fullName>
        <field>Managing_Team__c</field>
        <literalValue>COOP</literalValue>
        <name>Update Managing Team co-op</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_Team_to_Inspections_Admi</fullName>
        <field>Managing_Team__c</field>
        <literalValue>inspections admin</literalValue>
        <name>Update Managing Team to Inspections Admi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Managing_team_Admin</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Salesforce Admin</literalValue>
        <name>Update Managing team Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manging_Team_Co_opperative_Bank</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Co-opperative Bank</literalValue>
        <name>Update Manging Team Co-opperative Bank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Sales_Billing</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_Query_Sales</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner - Sales Billing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Reason_Invoicing</fullName>
        <field>Reason</field>
        <literalValue>Invoicing</literalValue>
        <name>Update Reason Invoicing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sub_Reason_Invoice_Query</fullName>
        <field>Sub_Reason__c</field>
        <literalValue>Invoice Query</literalValue>
        <name>Update Sub Reason Invoice Query</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_managing_team_Alarms</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Alarms</literalValue>
        <name>Update managing team - Alarms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_managing_team_to_CS_North</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN1</literalValue>
        <name>Update managing team to CS North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_Billing_Team</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_Query_Billing_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner - Billing Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_owner_CS_finance_query</fullName>
        <field>OwnerId</field>
        <lookupValue>Finance_Query_Customer_Services</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update owner - CS finance query</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>close_case</fullName>
        <field>Status</field>
        <literalValue>Closed - No Action Required</literalValue>
        <name>close case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>cs_south_owner_Managing_team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CS1</literalValue>
        <name>cs south - owner - Managing team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>not_yet_flagged</fullName>
        <field>Priority</field>
        <literalValue>Not Yet Flagged</literalValue>
        <name>not yet flagged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatae_managing_team</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN1</literalValue>
        <name>updatae managing team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_Birmingham</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Birmingham</literalValue>
        <name>update managing team Birmingham</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_CS1</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CS1</literalValue>
        <name>update managing team CS SOUTH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_Commercial_North</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Unallocated - Customer Services</literalValue>
        <name>update managing team Commercial North</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_Fife</fullName>
        <field>Managing_Team__c</field>
        <literalValue>FIFESC</literalValue>
        <name>update managing team Fife</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_Guardians</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Guardians</literalValue>
        <name>update managing team - Guardians</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_Mitie_Orbit</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Mitie Orbit</literalValue>
        <name>update managing team Mitie Orbit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_NottinghamSC</fullName>
        <field>Managing_Team__c</field>
        <literalValue>NottinghamSC</literalValue>
        <name>update managing team NottinghamSC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_STT</fullName>
        <field>Managing_Team__c</field>
        <literalValue>STT</literalValue>
        <name>update managing team - STT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_SmartCCTV</fullName>
        <field>Managing_Team__c</field>
        <literalValue>SmartCCTV</literalValue>
        <name>update managing team SmartCCTV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_VPSitex_ireland</fullName>
        <field>Managing_Team__c</field>
        <literalValue>VPSitex Ireland</literalValue>
        <name>update managing team VPSitex ireland</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_cn2</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN2</literalValue>
        <name>update managing team cn2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_cn3</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN3</literalValue>
        <name>update managing team cn3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_commercial_south</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Unallocated - Customer Services</literalValue>
        <name>update managing team commercial south</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_continuous_improvem</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Continuous Improvement</literalValue>
        <name>update managing team continuous improvem</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_credit_approvals</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Credit Approvals</literalValue>
        <name>update managing team credit approvals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_cs2</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CS2</literalValue>
        <name>update managing team cs2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_cs3</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CS3</literalValue>
        <name>update managing team cs3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_heineken</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Heineken</literalValue>
        <name>update managing team heineken</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_inspection_issues</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Inspection Issues</literalValue>
        <name>update managing team inspection issues</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_key_pubs</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Key Pubs</literalValue>
        <name>update managing team - key pubs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_leedssc</fullName>
        <field>Managing_Team__c</field>
        <literalValue>LeedsSC</literalValue>
        <name>update managing team leedssc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_unallocated</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Unallocated - Customer Services</literalValue>
        <name>update managing team - unallocated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_team_workman</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Workman</literalValue>
        <name>update managing team workman</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_teamleebaron</fullName>
        <field>Managing_Team__c</field>
        <literalValue>CN1</literalValue>
        <name>update managing team lee baron</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_managing_teams_Contract_setups</fullName>
        <field>Managing_Team__c</field>
        <literalValue>Contract-Setups</literalValue>
        <name>update managing teams - Contract-setups</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_owner_Neil_Hall</fullName>
        <field>OwnerId</field>
        <lookupValue>megan.porter@vpspecialists.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>update owner - Neil Hall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AUTOCLOSE - Mailbox too large</fullName>
        <actions>
            <name>No_Action_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_managing_team_STT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_owner_Neil_Hall</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Your mailbox is becoming too large</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Your mailbox is almost full.</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>AXA Managing Team</fullName>
        <actions>
            <name>AXA_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Managing_Team_AXA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>axa@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - CN1</fullName>
        <actions>
            <name>updatae_managing_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>equals</operation>
            <value>cn1</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - CN2</fullName>
        <actions>
            <name>update_managing_team_cn2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>equals</operation>
            <value>cn2</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - CN3</fullName>
        <actions>
            <name>update_managing_team_cn3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>equals</operation>
            <value>cn3</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - Commercial North</fullName>
        <actions>
            <name>update_managing_team_Commercial_North</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>northcs@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - Commercial South</fullName>
        <actions>
            <name>update_managing_team_commercial_south</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>southcs@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS - Unallocated</fullName>
        <actions>
            <name>update_managing_team_unallocated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>notEqual</operation>
            <value>cs1,cs2,cs3,cn1,cn2,cn3</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS NORTH</fullName>
        <actions>
            <name>Update_managing_team_to_CS_North</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>equals</operation>
            <value>CN1,CN2,CN3,CS NORTH</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS SOUTH</fullName>
        <actions>
            <name>update_managing_team_CS1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>customerservices@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Account_Case_Team__c</field>
            <operation>equals</operation>
            <value>CS1,CS2,CS3,CS SOUTH</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CS SOUTH - Owner - Managing Team</fullName>
        <actions>
            <name>cs_south_owner_Managing_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>cs south</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completions - CS</fullName>
        <actions>
            <name>Completions_CS_Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>completions@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Emcor Managing team</fullName>
        <actions>
            <name>Emcor_Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>emcor@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 01 Other</fullName>
        <actions>
            <name>FQ_Update_Reason_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>01</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 03 - Charged Twice</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Charged_Twice</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>03</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 05 Pricing Incomplete</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Pricing_Incomplet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>05</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 06 - Client Name incorrect</fullName>
        <actions>
            <name>FQ_Upate_Sub_Reason_Client_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Reason_Addresse_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>06</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 07 Hours%2Fstock</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Hours_Stock</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>07</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 08 Job Cancelled</fullName>
        <actions>
            <name>FQ_Update_Reason_Job_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Job_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>08</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 10 - FOC Promise</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_sub_reaspn_FOC_promise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>10</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 12 - Missing Kit</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Missing_Kit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>12</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 13 Wrong Item</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Wrong_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>13</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 15 Sales Concession</fullName>
        <actions>
            <name>FQ_Update_Reason_Commercial_Discount</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Sales_Concession</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>15</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 17 - Price List Incorrect</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_sub_reason_price_list</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>17</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 18 Property Not Off Hired</fullName>
        <actions>
            <name>FQ_Update_Reason_Not_Offered_Hired</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Property_Not_Off</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>18</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 19 - Billing Period</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Billing_Period</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>19</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 20 Ownership Changed</fullName>
        <actions>
            <name>FQ_Update_Reason_Ownership_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Ownership_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>20</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 22 - Invoice Split Required</fullName>
        <actions>
            <name>FQ_Update_Reason_Invoice_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Invoice_Split</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>22</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 26 - Address Incorrect</fullName>
        <actions>
            <name>FQ_Update_Reason_Addresse_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Address_Incorre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>26</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 28 Job Quality %2F Timing</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Job_Quality_Timin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>28</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 29 Doesnt Match Instruction</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_update_sub_reason_doesnt_match_inst</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>29</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 44 Charges For Missing Kit In Dispute</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Charges_For_MisKi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>44</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 50 - Alarm Response</fullName>
        <actions>
            <name>FQ_Update_Reason_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Alarm_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>50</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 63 Inspection Reports</fullName>
        <actions>
            <name>FQ_Update_Reason_Inspection_Reports</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Inspection_Report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>63</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 67 - Invoice Split Required</fullName>
        <actions>
            <name>FQ_Update_Reason_Work_Carried_Out</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_No_Completion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>67</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 69 PO Used Previously</fullName>
        <actions>
            <name>FQ_Update_Reason_PO_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_PO_Used_Previousl</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>69</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 74 - PO Missing</fullName>
        <actions>
            <name>FQ_Update_Reason_PO_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_PO_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>74</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - 75 - Wrong PO</fullName>
        <actions>
            <name>FQ_Update_Reason_PO_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FQ_Update_Sub_Reason_Wrong_PO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>75</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - Update Finance query - Customer Services</fullName>
        <actions>
            <name>Update_owner_CS_finance_query</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>19,50,12,03,67,07,13,44,28,74,75,69,08,18</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - Update Finance query - Sales</fullName>
        <actions>
            <name>Update_Owner_Sales_Billing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>17,10,29,05,15</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - Update Finance query - billing team</fullName>
        <actions>
            <name>Update_owner_Billing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK Finance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Finance_Query_Code__c</field>
            <operation>equals</operation>
            <value>26,06,22,20,01</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FQ - Update Standard Reason %2F Sub Reason</fullName>
        <actions>
            <name>Update_Reason_Invoicing</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Sub_Reason_Invoice_Query</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>UK Finance</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>G4S</fullName>
        <actions>
            <name>Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>G4S@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Critical Priority Accounts</fullName>
        <actions>
            <name>JETCLOUD_Update_Priority_Critical</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>contains</operation>
            <value>willmottdixon.co.uk,ovhpropertyservices.co.uk,rcthomes.co.uk,systemalerts@valueworks.co.uk,QMS@enterpriseinns.plc.uk</value>
        </criteriaItems>
        <description>Set the priority flag for Critical Priority Accounts as Critical</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Completions</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_Comple</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>completions@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - GuardChecks</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_GuardChec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>guard.checks@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Guarding</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_Guardin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>guarding@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Guarding Queries</fullName>
        <actions>
            <name>Update_Managing_Team_Guarding_Queries</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>guarding.queries@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Lee Baron</fullName>
        <actions>
            <name>update_managing_teamleebaron</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>leebaron@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Liverpool2020</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_Liv2020</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>ordersliverpool@vpsecurity.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Quotes</fullName>
        <actions>
            <name>Update_Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>quotes@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - STT</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_STT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>securitytransfer@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Managing Team - Service Improvement</fullName>
        <actions>
            <name>JETCLOUD_Update_Managing_Team_ServiceIm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>complaints@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Parent Case Status on Child Close</fullName>
        <actions>
            <name>JETCLOUD_Notify_Case_Creator_that_the_Case_has_been_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.ParentId</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.CreatedById</field>
            <operation>notEqual</operation>
            <value>VPS Service</value>
        </criteriaItems>
        <description>When a Child case gets closed, Send an email to the creator of the child case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Priority to - Not Yet Flagged</fullName>
        <actions>
            <name>JETCLOUD_Update_Case_Priority_NotYetFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED(OwnerId),
OR(OwnerId=&quot;00G200000017Gu4&quot;,OwnerId=&quot;00G200000017Gtz&quot;,OwnerId=&quot;00G200000017Gtu&quot;,OwnerId=&quot;00G200000017FtN&quot;,OwnerId=&quot;00G200000017Gtf&quot;,OwnerId=&quot;00G200000017HEY&quot;,OwnerId=&quot;00G200000017HEd&quot;,OwnerId=&quot;00G200000017FtS&quot;,OwnerId=&quot;00G200000017FtX&quot;,OwnerId=&quot;00G200000017HET&quot;,OwnerId=&quot;00G20000001dx1N&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Low Priority</fullName>
        <actions>
            <name>Low_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>pics@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RBS Managing Team</fullName>
        <actions>
            <name>RBS_Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>iss-rbs@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reactive Maintenance Managing Team</fullName>
        <actions>
            <name>RM_Managing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>reactivemaintenance@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Royal British Legion</fullName>
        <actions>
            <name>Royal_British_Legion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>rbl@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SF Admin</fullName>
        <actions>
            <name>SF_Admin</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Salesforce Admin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK Towers Managing Team and Owner</fullName>
        <actions>
            <name>UK_Tower_Manag8ing_Team</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UK_Tower_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>uktowers@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update MT BarkingSC</fullName>
        <actions>
            <name>Update_MT_BarkingSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>BarkingSC@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update MT CorbySC</fullName>
        <actions>
            <name>Update_MT_CorbySC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>CorbySC@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update MT NortheastSC</fullName>
        <actions>
            <name>Update_MT_NortheastSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>NorthEastSC@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managin Team London</fullName>
        <actions>
            <name>Update_Managing_Team_LondonSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>BarkingSC@VPSpecialists.com,WatfordSC@VPSpecialists.com,londonsc@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - Birmingham</fullName>
        <actions>
            <name>update_managing_team_Birmingham</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>birmingham@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - Continuous Improvement</fullName>
        <actions>
            <name>update_managing_team_continuous_improvem</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Continuousimprovement@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - Contract-Setups</fullName>
        <actions>
            <name>update_managing_teams_Contract_setups</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>contract-setups@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - Guardian Admin</fullName>
        <actions>
            <name>Update_Managing_Team_Guardian_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>guardianadmin@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - HullSC</fullName>
        <actions>
            <name>Update_Managing_Team_HullSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>HullSC@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - Invoicing</fullName>
        <actions>
            <name>Managing_Team_Invoicing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>creditcontrol@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - KilmarnockSC</fullName>
        <actions>
            <name>Update_Managing_Team_KilmarnockSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>KilmarnockSC@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team - ManchesterSC</fullName>
        <actions>
            <name>Update_Managing_Team_ManchesterSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>ManchesterSC@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Alarms</fullName>
        <actions>
            <name>Update_managing_team_Alarms</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>alarms@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team BristolSC</fullName>
        <actions>
            <name>Update_Managing_Team_BristolSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>BristolSC@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Centralenglandco-op</fullName>
        <actions>
            <name>Update_Managing_Team_Centralenglandco_op</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Centralenglandco-op@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Co-opperative Bank</fullName>
        <actions>
            <name>Co_opperative_Bank_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Manging_Team_Co_opperative_Bank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Carillionco-op@vpsgroup.com,co-operative.bank@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Enterprise</fullName>
        <actions>
            <name>JETCLOUD_Update_Case_Priority_NotYetFlag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Managing_Team_Enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Enterprise@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>QMS@Enterpriseinns.plc.uk</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Enterprise QMS</fullName>
        <actions>
            <name>JETCLOUD_Update_Priority_Critical</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Managing_Team_Enterprise</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Enterprise@vpspecialists.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>QMS@Enterpriseinns.plc.uk</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Invoicing</fullName>
        <actions>
            <name>Update_Managing_Team_Invoicing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>queries@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team LeedsSC</fullName>
        <actions>
            <name>update_managing_team_leedssc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>LeedsSC@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team LiverpoolSC</fullName>
        <actions>
            <name>Update_Managing_Team_LiverpoolSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>LiverpoolSC@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team Nottinghamsc</fullName>
        <actions>
            <name>update_managing_team_NottinghamSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Notts.bookings@VPSpecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team PortsmouthSC</fullName>
        <actions>
            <name>Update_Managing_Team_PortsmouthSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>PortsmouthSC@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team WalesSC</fullName>
        <actions>
            <name>Update_Managing_Team_WalesSC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>walessc@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team YHG</fullName>
        <actions>
            <name>Update_Managing_Team_YHG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>YHG@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team co-op</fullName>
        <actions>
            <name>Update_Managing_Team_co_op</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>coop@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing Team to Inspections Admin</fullName>
        <actions>
            <name>Update_Managing_Team_to_Inspections_Admi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>inspections.admin@vpsgroup.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Managing team Admin</fullName>
        <actions>
            <name>Update_Managing_team_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>salesforceadmin@vpsgroup.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>salesforce admin</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update managing team - Keys</fullName>
        <actions>
            <name>Update_Managing_Team_Keys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>keys@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update managing team - Guardians</fullName>
        <actions>
            <name>update_managing_team_Guardians</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>guardian.interviews@vpspecialists.com,guardians@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update managing team - Smartcctv</fullName>
        <actions>
            <name>update_managing_team_SmartCCTV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>SmartCCTV@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update managing team Fife</fullName>
        <actions>
            <name>update_managing_team_Fife</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>vpsfife@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update managing team Mitie Orbit</fullName>
        <actions>
            <name>update_managing_team_Mitie_Orbit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>mitieorbit@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update managing team workman</fullName>
        <actions>
            <name>update_managing_team_workman</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>workmanhansteen@vpspecialists.com</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
