<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Event</fullName>
        <description>New Event</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/New_Event_2</template>
    </alerts>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_VPS_Comments_Event_Desc</fullName>
        <description>Takes first 200 characters of event description and pushes to custom field</description>
        <field>VPS_Comments__c</field>
        <formula>LEFT(Description, 200)</formula>
        <name>JETCLOUD: Update VPS Comments-Event Desc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SS_Customer_meeting_to_site_visit</fullName>
        <field>Type</field>
        <literalValue>Site Visit</literalValue>
        <name>SS Customer meeting to site visit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_event_type_for_report</fullName>
        <field>Type_for_report__c</field>
        <formula>text(Type)</formula>
        <name>Update event type for report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JETCLOUD%3A Push event comments into custom field</fullName>
        <actions>
            <name>JETCLOUD_Update_VPS_Comments_Event_Desc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Takes the first 200characters of the event description and pushes to a custom field for view in console/related lists</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Meeting to site visit</fullName>
        <actions>
            <name>SS_Customer_meeting_to_site_visit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.Type</field>
            <operation>equals</operation>
            <value>Customer Meeting</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Owners_Division__c</field>
            <operation>contains</operation>
            <value>site security</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Event</fullName>
        <actions>
            <name>New_Event</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(Owners_Division__c = &quot;France&quot;,CreatedBy.Email &lt;&gt; Owner:User.Email), true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>update type for report</fullName>
        <actions>
            <name>Update_event_type_for_report</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Text(Type) &lt;&gt;  Type_for_report__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
