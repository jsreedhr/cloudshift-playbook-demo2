<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Due For Renewal</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Tender__c.Renewal_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This creates a task for tenders due</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Due_For_Renewal_90</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Tender__c.Renewal_Date__c</offsetFromField>
            <timeLength>-90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Contract_Due_For_Renewal</fullName>
        <assignedTo>UK_Head_of_Bid_Management</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Tender__c.Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Due For Renewal</subject>
    </tasks>
    <tasks>
        <fullName>Contract_Due_For_Renewal_90</fullName>
        <assignedTo>UK_Head_of_Bid_Management</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Tender__c.Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Due For Renewal</subject>
    </tasks>
    <tasks>
        <fullName>Contract_Due_For_Renewal_task</fullName>
        <assignedTo>UK_Head_of_Bid_Management</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>-30</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Tender__c.Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Contract Due For Renewal</subject>
    </tasks>
</Workflow>
