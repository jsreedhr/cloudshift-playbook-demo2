<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Contractor_Response_Received</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Contractor Response Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Case_to_In_Progress</fullName>
        <description>Update the status to In Progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>JETCLOUD: Update Case to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>JETCLOUD%3A Reopen Closed Case</fullName>
        <actions>
            <name>JETCLOUD_Update_Case_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If an email is received associated to an existing Closed case. Update the Case status to In Progress so it appears back in the queue/view for the team to manage.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Status to In Progress - With other user</fullName>
        <actions>
            <name>JETCLOUD_Update_Case_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>,Escalated,With BDM,With Customer,With Operations,With Insurers,With Finance,With Internal Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an email is received and the case is set to with any of the other teams, update the status to In Progress so that it appears in the users view/queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Status to In Progress - Quote Received</fullName>
        <actions>
            <name>Contractor_Response_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>With Contractor</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an email is received and the case is set to with any of the other teams, update the status to In Progress so that it appears in the users view/queue.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
