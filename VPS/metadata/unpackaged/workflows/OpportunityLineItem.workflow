<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SS_One_Off_Product_Alert</fullName>
        <description>SS One -Off Product Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_Sales_Templates/Alert_of_one_off_SS</template>
    </alerts>
    <alerts>
        <fullName>TEST</fullName>
        <ccEmails>lamyaa.naim@experis-it.fr</ccEmails>
        <description>TEST</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FR_Notification_service_devis_AO</template>
    </alerts>
    <alerts>
        <fullName>X25_Discount_Alert_Email</fullName>
        <description>25% Discount Alert Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>dominik.wittenbrink@vpsgroup.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.porter@vpspecialists.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mike.juergens@vpsgroup.de</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DE_Sales_Templates/DE_25_Discount</template>
    </alerts>
    <fieldUpdates>
        <fullName>AXA</fullName>
        <field>Insurer__c</field>
        <literalValue>AXA Insurance</literalValue>
        <name>AXA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Adjusted_Price_2</fullName>
        <description>Update Adjusted Price for FRANCE</description>
        <field>Adjusted_Price__c</field>
        <formula>IF(Product_Family3__c = &quot;Recurring&quot;,TotalPrice, TotalPrice)</formula>
        <name>Adjusted Price 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Alert_Tower_Update2</fullName>
        <field>Alert_Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Alert Tower Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FR_MAJ_Opportunity_Family_product</fullName>
        <description>MAJ Ligne Opportunité Family product</description>
        <field>Product_Family__c</field>
        <formula>TEXT(PricebookEntry.Product2.Family)</formula>
        <name>FR MAJ Opportunity Family product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INSURER_UPDATE</fullName>
        <field>Insurer__c</field>
        <literalValue>AXA Insurance</literalValue>
        <name>INSURER UPDATE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Install_Fee</fullName>
        <field>Install_Fee_Charged__c</field>
        <literalValue>1</literalValue>
        <name>Install Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Insurer_Checkbox</fullName>
        <field>Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Insurer Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Estimated_Total</fullName>
        <description>Sets the estimated total = Quantity * Sales price * Estimated no. of weeks</description>
        <field>Estimated_Total_Price__c</field>
        <formula>IF(OR(ISPICKVAL(Product2.Family,&quot;Recurring&quot;), ISPICKVAL(Product2.Family,&quot;Guarding&quot;)), UnitPrice *  Minimum_Contracted_Term__c  * Quantity, UnitPrice *Quantity)</formula>
        <name>JETCLOUD: Update Estimated Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Minimum_Contracted_Term</fullName>
        <description>Takes the value from the opportunity Minimum Contracted Term field to allow for calculations and roll-ups</description>
        <field>Minimum_Contracted_Term__c</field>
        <formula>If(Product_Family__c=&quot;One-Off&quot;, 1, Minimum_Contracted_Term_Hidden__c)</formula>
        <name>JETCLOUD: Update Minimum Contracted Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Product_Category</fullName>
        <description>Takes the product splits from the product table to allow for rollups to the Opportunity Header</description>
        <field>Product_Category__c</field>
        <formula>CASE( Product2.Product_Category__c , 
&quot;Guarding&quot;,&quot;Guarding&quot;, 
&quot;Inspections&quot;,&quot;Inspections&quot;, 
&quot;Securing&quot;,&quot;Securing&quot;, 
&quot;Property Services&quot;,&quot;Property Services&quot;,
&quot;Guardian Services&quot;,&quot;Guardian Services&quot;, 
&quot;Towers&quot;, &quot;Towers&quot;,
&quot;Evander&quot;, &quot;Evander&quot;,
&quot;&quot;)</formula>
        <name>JETCLOUD: Update Product Category</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Product_Family</fullName>
        <field>Product_Family__c</field>
        <formula>CASE(Product2.Family, 
&quot;One-Off&quot;,&quot;One-Off&quot;, 
&quot;Recurring&quot;,&quot;Recurring&quot;,
&quot;Guarding&quot;,&quot;Guarding&quot;,
&quot;Guardian Services&quot;, &quot;Guardian Services&quot;, 
&quot;&quot;)</formula>
        <name>JETCLOUD: Update Product Family</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JETCLOUD_Update_Weighted_Total</fullName>
        <description>Calculates the Total for the line item based on Quantity * Average Contract Term * Sales Price
IF( Product_Family__c =&quot;Recurring&quot;, 
UnitPrice * Average_Rental_Term__c  * Quantity, 
UnitPrice *Quantity)</description>
        <field>Weighted_Total_Price__c</field>
        <formula>IF( Product2.Name =&quot;Guarding - Short Term&quot;,UnitPrice * 1 * Quantity,
IF( Product2.Name =&quot;Guarding - Long Term&quot;,UnitPrice * 8 * Quantity,

IF( Product_Family__c =&quot;Recurring&quot;, 
UnitPrice * Average_Rental_Term__c  * Quantity, 
UnitPrice *Quantity)))</formula>
        <name>JETCLOUD: Update Weighted Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Property_Insurer</fullName>
        <description>Auto populates AXA insurer when AXA product selected</description>
        <field>Insurer__c</field>
        <literalValue>AXA Insurance</literalValue>
        <name>Property Insurer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposal_Free_Text_SS_update</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Our system uses motion and heat detection in order to monitor your site. Should an intruder pass through an area of detection whilst the system is armed the camera will instantly and automatically pan, tilt and zoom to the view the intrusion whilst simultaneously alerting our BS5979 monitoring station.&lt;BR&gt;&lt;BR&gt;Our CCTV operators have the facility to directly address the intruder with a live audio challenge using the inbuilt audio communication system to deter them from your site. In the vast majority of cases when intruders are aware they’ve been caught, are being recorded, and police are on their way, they leave at speed.  &lt;BR&gt;&lt;BR&gt;VPS Site Security is obliged by the telecommunications suppliers to comply with an acceptable usage policy both for the individual line and for the network as a whole.  Therefore the customer access to the video stream will be limited in duration.  VPS Site Security reserves the right to manage the data streams at its absolute discretion. &lt;BR&gt;&lt;BR&gt;It will also not be possible to have more than a proportion of customers on the network at any one time.  Therefore it is to be clearly understood that if the customer cannot gain access to their cameras at a particular time, or if access is interrupted, it does not mean that the cameras have stopped functioning.  The customer should not call the control room in this situation.  Also to note when the customer is viewing the cameras VPS Site Security are unable to record/monitor at the same time.On the next pages you will find the technical drawing for the solution and the investment details. Should you have any questions please do not hesitate to contact me otherwise I look forward to gaining your authorisation to proceed.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Proposal Free Text SS update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposal_Free_Text_UK_update</fullName>
        <field>Proposal_Free_Text__c</field>
        <formula>&quot;Further to our recent discussion/meeting regarding the above site we now have pleasure in submitting our pricing for the hire of VPS’ Void Property Security Products and Services, as per the enclosed quotation.&lt;BR&gt;&lt;BR&gt;With over 12 years experience within the vacant property management industry, VPS has a proven track record of consistently delivering best value with innovative and immediate solutions within various industries such as construction, property management, retail, leisure, and insolvency practitioners. We have developed a deep understanding of our customer requirements and wider objectives, offering an extensive range of products and services within the vacant property lifecycle to help reduce the true costs of vacancy and minimise the effects on their surrounding communities. &lt;BR&gt;&lt;BR&gt;VPS is obliged by the telecommunications suppliers to comply with an acceptable usage policy both for the individual line and for the network as a whole.  Therefore the customer access to the video stream will be limited in duration.  VPS reserves the right to manage the data streams at its absolute discretion.&lt;BR&gt;&lt;BR&gt;It will also not be possible to have more than a proportion of customers on the network at any one time.  Therefore it is to be clearly understood that if the customer cannot gain access to their cameras at a particular time, or if access is interrupted, it does not mean that the cameras have stopped functioning.  The customer should not call the control room in this situation.  Also to note when the customer is viewing the cameras VPS are unable to record/monitor at the same time.&lt;BR&gt;&lt;BR&gt;We trust the enclosed meets with your approval, however should you have any queries or wish to discuss matters in more detail then please do not hesitate to contact us.&lt;BR&gt;&lt;BR&gt;We look forward to hearing from you in due course.&lt;BR&gt;&lt;BR&gt;&quot;</formula>
        <name>Proposal Free Text UK update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Total_Weighted_Amount_for_FR</fullName>
        <field>Weighted_Total_Price__c</field>
        <formula>Adjusted_Price__c</formula>
        <name>Total Weighted Amount for FR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tower</fullName>
        <field>Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Tower</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tower_Checkbox</fullName>
        <field>Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Tower Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tower_Opportunity</fullName>
        <field>Tower_Opportunity__c</field>
        <literalValue>1</literalValue>
        <name>Tower Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tranche2</fullName>
        <field>Opp_Tranche_2__c</field>
        <formula>PricebookEntry.Tranche2__c - (PricebookEntry.Tranche2__c * Discount)</formula>
        <name>Tranche2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tranche3</fullName>
        <field>Opp_Tranche_3__c</field>
        <formula>PricebookEntry.Tranche3__c - (PricebookEntry.Tranche3__c * Discount)</formula>
        <name>Tranche3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tranche4</fullName>
        <field>Opp_Tranche_4__c</field>
        <formula>PricebookEntry.Tranche4__c- (PricebookEntry.Tranche4__c * Discount)</formula>
        <name>Tranche4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tranche5</fullName>
        <field>Opp_Tranche_5__c</field>
        <formula>PricebookEntry.Tranche5__c- (PricebookEntry.Tranche5__c * Discount)</formula>
        <name>Tranche5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tranche6</fullName>
        <field>Opp_Tranche_6__c</field>
        <formula>PricebookEntry.Tranche6__c - (PricebookEntry.Tranche6__c * Discount)</formula>
        <name>Tranche6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Adjust_Total</fullName>
        <field>Adjusted_Price__c</field>
        <formula>if(contains(PricebookEntry.Product2.Name,&quot;Guardian&quot;), 
 TotalPrice/4.33,TotalPrice)</formula>
        <name>Update Adjust Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Check</fullName>
        <field>Check__c</field>
        <formula>IF(  Opportunity.Check__c= 0, 1,0)</formula>
        <name>Update Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Core_Non_Core</fullName>
        <field>Core_Non__c</field>
        <formula>TEXT(Product2.Core_Product__c)</formula>
        <name>Update Core/Non Core</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Standard_Description</fullName>
        <field>Standard_Description__c</field>
        <formula>PricebookEntry.Product2.Description</formula>
        <name>Update Standard Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total</fullName>
        <field>Adjusted_Price__c</field>
        <formula>if(contains(PricebookEntry.Product2.Name,&quot;Grounds&quot;), 
TotalPrice/4.33,TotalPrice)</formula>
        <name>Update Total</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Total_Recurring_Revenue</fullName>
        <field>Total_Recurring_Revenue_Per_Period__c</field>
        <formula>if(contains(PricebookEntry.Product2.Name,&quot;Grounds&quot;), 
TotalPrice/4.33,TotalPrice)</formula>
        <name>Update Total Recurring Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>0 Value  Site Security Alert</fullName>
        <active>true</active>
        <formula>AND(
  UnitPrice  =  0  ,
OR(
$User.UserRoleId = &apos;00Ew0000000uLwm&apos;,$User.UserRoleId = &apos;00E20000001KAje&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>DE - 25%25 Discount</fullName>
        <actions>
            <name>X25_Discount_Alert_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Discount__c</field>
            <operation>greaterThan</operation>
            <value>25</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordType_Name__c</field>
            <operation>contains</operation>
            <value>DE - Simple,DE - Complex</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>VPS - German User</value>
        </criteriaItems>
        <description>Notification when Sales price is over 25%</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Discount by Site Security</fullName>
        <active>true</active>
        <formula>AND(
 UnitPrice &lt;  ListPrice ,
OR(
$User.UserRoleId = &apos;00Ew0000000uLwm&apos;,$User.UserRoleId = &apos;00E20000001KAje&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Opportunity MAJ Product Family</fullName>
        <actions>
            <name>FR_MAJ_Opportunity_Family_product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>MAJ product family à la création et modification des postes opportunités</description>
        <formula>Opportunity.RecordType.Id =&apos;012w00000002RYH&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FR Tranche Update</fullName>
        <actions>
            <name>Tranche2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tranche3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tranche4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tranche5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Tranche6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Opp_Tranche_2__c</field>
            <operation>equals</operation>
            <value>EUR 0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Grounds Monthly Value</fullName>
        <actions>
            <name>Update_Total_Recurring_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Product2.Product_Category__c</field>
            <operation>equals</operation>
            <value>Evander</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Install Fee</fullName>
        <actions>
            <name>Install_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(((1 AND 2 ) OR 3) OR 4)</booleanFilter>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>Installation/Removal Fee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>equals</operation>
            <value>VPS UK - Corporate 2017</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Category__c</field>
            <operation>equals</operation>
            <value>Property Services,Guarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>notEqual</operation>
            <value>VPS UK - Corporate 2017</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Insurer Bundle - Property Insurer</fullName>
        <actions>
            <name>AXA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Insurer_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>equals</operation>
            <value>AXA Inspections Bundle</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JETCLOUD%3A Update Product Family</fullName>
        <actions>
            <name>JETCLOUD_Update_Estimated_Total</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Minimum_Contracted_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Product_Category</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Product_Family</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>JETCLOUD_Update_Weighted_Total</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Adjust_Total</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Core_Non_Core</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Total</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>FR Cotation et AO</value>
        </criteriaItems>
        <description>Pulls the details of product family and product category from the product when saving a new line item. Also calculates the weighted and estimated total revenues for each product based on minimum term and average rental term.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>One Off Site Security Alert</fullName>
        <actions>
            <name>SS_One_Off_Product_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND(
 Product_Family__c  = &apos;One-Off&apos;  ,
OR(
$User.UserRoleId = &apos;00Ew0000000uLwm&apos;,$User.UserRoleId = &apos;00E20000001KAje&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remote Viewing Text SS</fullName>
        <actions>
            <name>Proposal_Free_Text_SS_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>contains</operation>
            <value>VPS Site Security</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Remote viewing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Remote Viewing Text UK</fullName>
        <actions>
            <name>Proposal_Free_Text_UK_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Pricebook__c</field>
            <operation>contains</operation>
            <value>VPS UK,Social</value>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Name</field>
            <operation>contains</operation>
            <value>Remote viewing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tower Opportunity 2</fullName>
        <actions>
            <name>Tower_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>OpportunityLineItem.Name</field>
            <operation>contains</operation>
            <value>JCB Smart</value>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Category__c</field>
            <operation>contains</operation>
            <value>tower</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Adjusted Price for France</fullName>
        <actions>
            <name>Adjusted_Price_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>FR - Complex,FR - Simple</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Standard Line Desription</fullName>
        <actions>
            <name>Update_Standard_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Standard_Description__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
