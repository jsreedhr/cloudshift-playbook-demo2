/**
  Performs 'before insert' related initialization activities.
    - populates pricebook id on opportunity record based on custom pricebook lookup field
*/
trigger Opportunity_SetPricebook on Opportunity (before insert, before Update) {
    
    if(checkRecursive.runOnce()){
        if(trigger.isBefore){
            if(Trigger.isInsert){
                Set<Id> acctIds = new Set<Id>();
                for (Opportunity opp : Trigger.new) {
                    if(opp.AccountId != null){
                        acctIds.add(opp.AccountId);
                    }
                }
                if(!acctIds.isEmpty()){
                    Map<Id, Account> acctMap = new Map<Id, Account>([Select Id, Pricebook__c,Pricebook__r.Base_Pricebook_Record_Id__c from Account where Id in :acctIds]);
                  
                    for (Opportunity opp : Trigger.new) {
                        System.debug('opportunity: ' + opp);
                        if(acctMap.containsKey(opp.AccountId)){
                            Account acct = acctMap.get(opp.AccountId);
                            if(acct != null){
                                if(acct.Pricebook__c!=null){
                                    //System.debug('assigning pricebook id ' + acct.Pricebook__r.Base_Pricebook_Record_Id__c + ' to opportunity');
                                    opp.Pricebook2Id = acct.Pricebook__r.Base_Pricebook_Record_Id__c;
                                }
                            }
                        }
                    }
                }
                // Method to check that atleast one oppContactRole is associated to these opp before
                Opportunity_TriggerFunctions.ValidateOpportunityContactRoleOnInsert(trigger.New);
                Opportunity_TriggerFunctions.ValidateOpportunityCompetitorOnInsert(trigger.New);
                
            }
            if(trigger.isUpdate){
                 // Method to check that atleast one oppContactRole is associated to these opp before
                Opportunity_TriggerFunctions.ValidateOpportunityContactRoleOnUpdate(trigger.New);
                Opportunity_TriggerFunctions.ValidateOpportunityCompetitorOnUpdate(trigger.New);
            }
        }
    }
}