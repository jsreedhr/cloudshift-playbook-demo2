trigger AccPlanMonthTrigger on Account_Plan__c (after insert) {
 if(Trigger.isAfter)
    {
        if(Trigger.IsInsert)
        {
            AccPlanMonthTriggerHandler.insertAccPlanCFY(Trigger.new);
        }
    }
}