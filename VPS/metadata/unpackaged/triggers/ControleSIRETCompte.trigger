/**
* Trigger ControleSIRETCompte
* Description : Vérifie que le SIRET n'existe pas déjà sur un autre compte
* Author : Mathieu PERRIN
*/
trigger ControleSIRETCompte on Account (before insert, before update) {
    // Récupération des informations de l'utilisateur connecté
    //GeneralSettings__c gs = GeneralSettings__c.getInstance();
    
    // Exécute le trigger seulement si l'utilisateur appartient à Proservia ou Experis et les triggers sont actifs
        List<String> sirets = new List<String>();
        // Création de la liste des SIRET
        for (Account a : System.Trigger.new) {
            if(a.D_bloquer_SIRET__c == false) {
                sirets.add(a.SIRET__c);
            }
        }
        List<Account> accounts = [SELECT Id, SIRET__c, Name FROM Account WHERE SIRET__c IN :sirets];
        
        // Parcourt de la liste des comptes
        for (Account au : System.Trigger.new) {
            for (Account a : accounts) {     
                // Vérification si le SIRET existe déjà
                if (a.SIRET__c == au.SIRET__c && a.Id != au.Id && a.SIRET__c !=null) {
                    au.SIRET__c.addError('Ce SIRET existe déjà. Il correspond au compte '+a.Name+'.');
                }
            }
        }
}